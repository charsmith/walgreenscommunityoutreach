﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensDistrictUsersHome.aspx.cs" Inherits="walgreensDistrictUsersHome" MaintainScrollPositionOnPostback="true"  %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register Src="controls/ActionItems.ascx" TagName="walgreensActionItems" TagPrefix="ucWActionItem" %>
<%@ Register Src="controls/MetricsReports.ascx" TagName="walgreensMetricsReports" TagPrefix="ucWMetricsReports" %>
<%@ Register Src="controls/CorporateClinics.ascx" TagName="walgreensCorporateClinics" TagPrefix="ucWCorporateClinics" %>
<%@ Register Src="controls/DirectB2BCampaigns.ascx" TagName="walgreensDirectB2BCampaigns" TagPrefix="ucWDirectB2BCampaigns" %>
<%@ Register Src="controls/LocalLeads.ascx" TagName="walgreensLocalLeads" TagPrefix="ucWLocalLeads" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>    
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>  
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
     <script src="javaScript/jquery.printelement.min.js" type="text/javascript"></script> 
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <link href="css/calendarStyle.css" rel="stylesheet" />
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function getFilterData() {
            $("input[id$='txtUnassignedClinicsFilter'],input[id$='txtAssignedDistrictClinicsFilter'],input[id$='txtAssignedStoreClinicsFilter'],input[id$='txtUnassignedLocalLeadsFilter'],input[id$='txtAssignedDistrictLocalLeadsFilter'],input[id$='txtAssignedStoreLocalLeadsFilter'],input[id$='txtUnassignedDirectB2BMailBusinessFilter'],input[id$='txtAssignedDistrictDirectB2BMailFilter'],input[id$='txtAssignedStoreDirectB2BMailFilters']").autocomplete({
                source: function (request, response) {
                    var filter_type = $(this.element).parent().parent().find('select').val().toString();

                    $.ajax({
                        type: "POST",
                        url: "walgreensDistrictUsersHome.aspx/getFilterData",
                        data: '{}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var user_filter_data = [];
                            var filter_data = [];

                            if (response.d != "")
                                user_filter_data = $.parseJSON(data.d);

                            $.grep(user_filter_data, function (n) {                                
                                if ((filter_type == "" || n.locationType == filter_type) && n.locationId.toString().substr(0, request.term.toString().length) == request.term.toString()) {
                                    filter_data.push({
                                        label: n.address,
                                        value: n.locationId,
                                        type: n.locationType
                                    });
                                }
                            });
                            response(filter_data);                        
                        },
                        failure: function (error) {
                            alert(error.responseText);
                        },
                        error: function (error) {
                            alert(error.responseText);
                        }
                    });
                },
                select: function (event, ui) {
                    this.value = ui.item.value;

                    var filter_type = $(this).parent().parent().find('select');
                    if (filter_type.val() == "") {
                        filter_type.val(ui.item.type);
                    }

                    if (this.name.indexOf('CorporateClinics') > -1) {
                        if (this.name.indexOf('txtUnassignedClinicsFilter') > -1) {
                            $("input[id$='btnUnassignedCoporateClinics']").click();
                        }
                        else if (this.name.indexOf('txtAssignedDistrictClinicsFilter') > -1) {
                            $("input[id$='btnAssignedToDistCoporateClinics']").click();
                        }
                        else if (this.name.indexOf('txtAssignedStoreClinicsFilter') > -1) {
                            $("input[id$='btnAssignedCoporateClinics']").click();
                        }
                    }
                    else if (this.name.indexOf('DirectB2B') > -1) {
                        if (this.name.indexOf('txtUnassignedDirectB2BMailBusinessFilter') > -1) {
                            $("input[id$='btnUnassignedDirectB2BMailBusiness']").click();
                        }
                        else if (this.name.indexOf('txtAssignedDistrictDirectB2BMailFilter') > -1) {
                            $("input[id$='btnAssignedToDistrictDirectB2BMail']").click();
                        }
                        else if (this.name.indexOf('txtAssignedStoreDirectB2BMailFilters') > -1) {
                            $("input[id$='btnAssignedStoreDirectB2BMail']").click();
                        }
                    }
                    else if (this.name.indexOf('LocalLeads') > -1) {
                        if (this.name.indexOf('txtUnassignedLocalLeadsFilter') > -1) {
                            $("input[id$='btnUnassignedLocalLeads']").click();
                        }
                        else if (this.name.indexOf('txtAssignedDistrictLocalLeadsFilter') > -1) {
                            $("input[id$='btnAssignedDistrictLocalLeads']").click();
                        }
                        else if (this.name.indexOf('txtAssignedStoreLocalLeadsFilter') > -1) {
                            $("input[id$='btnAssignedStoreLocalLeads']").click();
                        }
                    }
                },
                autoFocus: true,
                minLength: 1
            })
            .keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }

        function showHCSUsersList() {
            $("#txtSelectedUser").autocomplete({
                source: function (request, response) {
                    var users_list = [];
                    $.ajax({
                        type: "POST",
                        url: "walgreensDistrictUsersHome.aspx/getHCSUsersList",
                        data: '{}',
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                users_list = [];
                                if (item.name.toString().substr(0, request.term.toString().length) == request.term.toString()) {
                                    users_list.push({
                                        label: item.name,
                                        value: item.id
                                    });
                                    return users_list;
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (event, ui) {
                    $("#txtSelectedUser").val(ui.item.label);
                    $("#txtSelectedUserId").val(ui.item.value);
                    $("#btnRefreshActionDropdown").click();
                },
                autoFocus: true,
                minLength: 1
            });
        }

        $(document).ready(function () {
            getFilterData();
            showHCSUsersList();

            //******************* Clearing filter textbox when filter change Start *********************
            (function () {
                var previous;
                $("#walgreensLocalLeads_ddlUnassignedLocalLeadsFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtUnassignedLocalLeadsFilter']").val('');
                        previous = this.value;
                    }
                });
            })();

            (function () {
                var previous;
                $("#walgreensLocalLeads_ddlAssignedDistrictLocalLeadsFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtAssignedDistrictLocalLeadsFilter']").val('');
                        previous = this.value;
                    }
                });
            })();

            (function () {
                var previous;
                $("#walgreensLocalLeads_ddlAssignedStoreLocalLeadsFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtAssignedStoreLocalLeadsFilter']").val('');
                        previous = this.value;
                    }
                });
            })();

            (function () {
                var previous;
                $("#walgreensDirectB2BCampaigns_ddlUnassignedDirectB2BMailBusinessFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtUnassignedDirectB2BMailBusinessFilter']").val('');
                        previous = this.value;
                    }
                });
            })();

            (function () {
                var previous;
                $("#walgreensDirectB2BCampaigns_ddlAssignedDistrictDirectB2BMailFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtAssignedDistrictDirectB2BMailFilter']").val('');
                        previous = this.value;
                    }
                });
            })();

            (function () {
                var previous;
                $("#walgreensDirectB2BCampaigns_ddlAssignedStoreDirectB2BMailFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtAssignedStoreDirectB2BMailFilters']").val('');
                        previous = this.value;
                    }
                });
            })();

            (function () {
                var previous;
                $("#walgreensCorporateClinics_ddlUnassignedClinicsFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtUnassignedClinicsFilter']").val('');
                        previous = this.value;
                    }
                });
            })();

            (function () {
                var previous;
                $("#walgreensCorporateClinics_ddlAssignedDistrictClinicsFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtAssignedDistrictClinicsFilter']").val('');
                        previous = this.value;
                    }
                });
            })();
            (function () {
                var previous;
                $("#walgreensCorporateClinics_ddlAssignedStoreClinicsFilters").focus(function () {
                    previous = this.value;
                }).change(function () {
                    if (typeof (previous) !== "undefined") {
                        $("input[id$='txtAssignedStoreClinicsFilter']").val('');
                        previous = this.value;
                    }
                });
            })();

            //******************* Clearing filter textbox when filter change Start *********************
            var infoLabelsJSON = {
                "corporateClinicsInstr": [
                    { "Button": "imgBtnUnassignedClinicsInstr", "Label": "ltlUnassignedClinicsNote", "flag": "hfUnassignedClinicsflag" },
                    { "Button": "imgBtnAssignedDistrictClinicsInstr", "Label": "ltlAssignedDistrictClinicsNote", "flag": "hfAssignedDistrictClinicsflag" },
                    { "Button": "imgBtnAssignedClinicsInstr", "Label": "ltlAssignedClinicsNote", "flag": "hfAssignedStoreClinicsflag"}],
                "directB2BMailInstr": [
                    { "Button": "imgBtnUnassignedDirectB2BMailInstr", "Label": "ltlUnassignedDirectB2BMailBusinessNote", "flag": "_UnAssignB2Bflag" },
                    { "Button": "imgBtnAssignedDistrictDirectB2BMailInstr", "Label": "ltlAssignedDistrictDirectB2BMailNote", "flag": "_AssignDistrictB2Bflag" },
                    { "Button": "imgBtnAssignedStoreDirectB2BMailInstr", "Label": "ltlAssignedB2BNote", "flag": "_AssignedB2Bflag" }],
                "localLeadsInstr": [
                    { "Button": "imgBtnUnassignedLocalLeadsInstr", "Label": "ltlUnassignedLocalLeadsNote", "flag": "hfUnassignedLocalLeadsflag" },
                    { "Button": "imgBtnAssignedDistrictLocalLeadsInstr", "Label": "ltlAssignedDistrictLocalLeadsNote", "flag": "hfAssignedDistrictLocalLeadsflag" },
                    { "Button": "imgBtnAssignedStoreLocalLeadsInstr", "Label": "ltlAssignedStoreLocalLeadsNote", "flag": "hfAssignedStoreLocalLeadsflag"}]
            };

            initTab(infoLabelsJSON.corporateClinicsInstr);
            initTab(infoLabelsJSON.directB2BMailInstr);
            initTab(infoLabelsJSON.localLeadsInstr);

            var chk_unassigned_clinics = $("input[id$='chkSelectAllUnassignedClinics']");
            var chk_assigned_clinics = $("input[id$='chkSelectAllAssignedClinics']");
            var chk_assigned_district_clinics = $("input[id$='chkSelectAllAssignedDistrictClinics']");

            chk_unassigned_clinics.click(function () {
                selectAllUnassigned();
            });

            $("input[id$='chkSelectAllUnassignedClinicsInDistrict']").click(function () {
                chk_unassigned_clinics.attr('checked', $(this).attr('checked') == 'checked');
                selectAllUnassigned();
            });
            function selectAllUnassigned() {
                $("table[id$='grdNcStoreAssignment'] INPUT[type='checkbox']").each(function () {
                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_unassigned_clinics.is(':checked'));
                    }
                });
            }
            chk_assigned_district_clinics.click(function () {
                selectAllAssignedDistricts();
            });
            $("input[id$='chkSelectAllAssignedDistrictClinicsInDistrict']").click(function () {
                chk_assigned_district_clinics.attr('checked', $(this).attr('checked') == 'checked');
                selectAllAssignedDistricts();
            });
            function selectAllAssignedDistricts() {
                $("table[id$='grdAssignedDistrictClinics'] INPUT[type='checkbox']").each(function () {

                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_assigned_district_clinics.is(':checked'));
                    }
                });
            }
            chk_assigned_clinics.click(function () {
                $("table[id$='grdAssignedClinics'] INPUT[type='checkbox']").each(function () {

                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_assigned_clinics.is(':checked'));
                    }
                });
            });

            $('#walgreensCorporateClinics_txtUnassignedClinicsSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $('#walgreensCorporateClinics_txtAssignedStoreDirectB2BMailSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $('#walgreensCorporateClinics_txtAssignedDistrictClinicsSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $("table[id$='grdNcStoreAssignment'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdNcStoreAssignment'] INPUT[id*='chkSelectUnassignedClinic']").length == $("table[id$='grdNcStoreAssignment'] INPUT[id*='chkSelectUnassignedClinic']:checked").length) {
                    chk_unassigned_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_unassigned_clinics.attr('checked', false);
                    }
                }
            });
            $("table[id$='grdAssignedDistrictClinics'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdAssignedDistrictClinics'] INPUT[id*='chkSelectAssignedDistrictClinic']").length == $("table[id$='grdAssignedDistrictClinics'] INPUT[id*='chkSelectAssignedDistrictClinic']:checked").length) {
                    chk_assigned_district_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_assigned_district_clinics.attr('checked', false);
                    }
                }
            });
            $("table[id$='grdAssignedClinics'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdAssignedClinics'] INPUT[id*='chkSelectAssignedClinics']").length == $("table[id$='grdAssignedClinics'] INPUT[id*='chkSelectAssignedClinics']:checked").length) {
                    chk_assigned_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_assigned_clinics.attr('checked', false);
                    }
                }
            });

            //*************B2B Script Start*************
            var chk_b2bunassigned_clinics = $("input[id$='chkAllUnassignedDirectB2BMailBusiness']");
            var chk_b2bassigned_district_clinics = $("input[id$='chkSelectAllDirectB2BBusiness']");
            var chk_b2bassigned_clinics = $("input[id$='chkSelectAllAssignedBusiness']");

            chk_b2bunassigned_clinics.click(function () {
                selectAllUnAssignedB2BDistricts();
            });
            $("input[id$='chkSelectAllUnassignedDirectB2BMailBusinessInDistrict']").click(function () {
                chk_b2bunassigned_clinics.attr('checked', $(this).attr('checked') == 'checked');
                selectAllUnAssignedB2BDistricts();
            });
            function selectAllUnAssignedB2BDistricts() {
                $("table[id$='grdUnassignedDirectB2BMailBusiness'] INPUT[type='checkbox']").each(function () {
                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_b2bunassigned_clinics.is(':checked'));
                    }
                });
            }
            chk_b2bassigned_district_clinics.click(function () {
                selectAllAssignedB2BDistricts();
            });
            $("input[id$='chkSelectAllAssignedDistrictDirectB2BMailInDistrict']").click(function () {
                chk_b2bassigned_district_clinics.attr('checked', $(this).attr('checked') == 'checked');
                selectAllAssignedB2BDistricts();
            });
            function selectAllAssignedB2BDistricts() {
                $("table[id$='grdAssignedDistrictDirectB2BMail'] INPUT[type='checkbox']").each(function () {

                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_b2bassigned_district_clinics.is(':checked'));
                    }
                });
            }
            chk_b2bassigned_clinics.click(function () {
                $("table[id$='grdAssignedStoreDirectB2BMail'] INPUT[type='checkbox']").each(function () {

                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_b2bassigned_clinics.is(':checked'));
                    }
                });
            });

            $('#walgreensDirectB2BCampaigns_txtUnassignedB2BBusinessSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $('#walgreensDirectB2BCampaigns_txtAssignedDistrictDirectB2BMailSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $('#walgreensDirectB2BCampaigns_txtAssignedStoreDirectB2BMailSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $("table[id$='grdUnassignedDirectB2BMailBusiness'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdUnassignedDirectB2BMailBusiness'] INPUT[id*='chkUnassignedDirectB2BMailBusiness']").length == $("table[id$='grdUnassignedDirectB2BMailBusiness'] INPUT[id*='chkUnassignedDirectB2BMailBusiness']:checked").length) {
                    chk_b2bunassigned_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_b2bunassigned_clinics.attr('checked', false);
                    }
                }
            });
            $("table[id$='grdAssignedDistrictDirectB2BMail'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdAssignedDistrictDirectB2BMail'] INPUT[id*='chkSelectDirectB2BBusiness']").length == $("table[id$='grdAssignedDistrictDirectB2BMail'] INPUT[id*='chkSelectDirectB2BBusiness']:checked").length) {
                    chk_b2bassigned_district_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_b2bassigned_district_clinics.attr('checked', false);
                    }
                }
            });
            $("table[id$='grdAssignedStoreDirectB2BMail'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdAssignedStoreDirectB2BMail'] INPUT[id*='chkSelectAssignedBusiness']").length == $("table[id$='grdAssignedStoreDirectB2BMail'] INPUT[id*='chkSelectAssignedBusiness']:checked").length) {
                    chk_b2bassigned_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_b2bassigned_clinics.attr('checked', false);
                    }
                }
            });
            //************B2B Script End**************

            //*************Local Leads Script Start*************
            var chk_localleadunassigned_clinics = $("input[id$='chkSelectAllUnassignedLocalLeads']");
            var chk_localleadassigned_district_clinics = $("input[id$='chkSelectAllDistrictLocalLeads']");
            var chk_localleadassigned_clinics = $("input[id$='chkSelectAllAssignedToStoreLocalLeads']");

            chk_localleadunassigned_clinics.click(function () {
                selectAllUnAssignedLocDistricts();
            });
            $("input[id$='chkSelectAllUnassignedLocalLeadsInDistrict']").click(function () {
                chk_localleadunassigned_clinics.attr('checked', $(this).attr('checked') == 'checked');
                selectAllUnAssignedLocDistricts();
            });
            function selectAllUnAssignedLocDistricts() {
                $("table[id$='grdUnassignedLocalLeads'] INPUT[type='checkbox']").each(function () {
                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_localleadunassigned_clinics.is(':checked'));
                    }
                });
            }

            chk_localleadassigned_district_clinics.click(function () {
                selectAllAssignedTDLocDistricts();
            });
            $("input[id$='chkSelectAllAssignedDistrictLocalLeadsInDistrict']").click(function () {
                chk_localleadassigned_district_clinics.attr('checked', $(this).attr('checked') == 'checked');
                selectAllAssignedTDLocDistricts();
            });
            function selectAllAssignedTDLocDistricts() {
                $("table[id$='grdAssignedToDistrictLocalLeads'] INPUT[type='checkbox']").each(function () {

                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_localleadassigned_district_clinics.is(':checked'));
                    }
                });
            }

            chk_localleadassigned_clinics.click(function () {
                $("table[id$='grdAssignedToStoreLocalLeads'] INPUT[type='checkbox']").each(function () {

                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_localleadassigned_clinics.is(':checked'));
                    }
                });
            });

            $('#walgreensLocalLeads_txtUnassignedLocalLeadsSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $('#walgreensLocalLeads_txtAssignedDistrictLocalLeadsSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $('#walgreensLocalLeads_txtAssignedStoreLocalLeadsSearch').keyup(function () {
                if (this.value.match(/[<>?=~`$%()@]/g)) {
                    this.value = this.value.replace(/[<>?=~`$%()@]/g, '');
                    return false;
                }
            });

            $("table[id$='grdUnassignedLocalLeads'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdUnassignedLocalLeads'] INPUT[id*='chkSelectUnassignedLocalLeads']:not(:disabled)").length == $("table[id$='grdUnassignedLocalLeads'] INPUT[id*='chkSelectUnassignedLocalLeads']:checked").length) {
                    chk_localleadunassigned_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_localleadunassigned_clinics.attr('checked', false);
                    }
                }
            });
            $("table[id$='grdAssignedToDistrictLocalLeads'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdAssignedToDistrictLocalLeads'] INPUT[id*='chkSelectDistrictLocalLeads']:not(:disabled)").length == $("table[id$='grdAssignedToDistrictLocalLeads'] INPUT[id*='chkSelectDistrictLocalLeads']:checked").length) {
                    chk_localleadassigned_district_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_localleadassigned_district_clinics.attr('checked', false);
                    }
                }
            });
            $("table[id$='grdAssignedToStoreLocalLeads'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdAssignedToStoreLocalLeads'] INPUT[id*='chkSelectAssignedToStoreLocalLeads']").length == $("table[id$='grdAssignedToStoreLocalLeads'] INPUT[id*='chkSelectAssignedToStoreLocalLeads']:checked").length) {
                    chk_localleadassigned_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_localleadassigned_clinics.attr('checked', false);
                    }
                }
            });
            //************Local Leads Script End**************

            $('.ddlFilter').change(function () {
                var checkBox = null;
                var txtBox = $(this).parent().next().next()[0].firstChild.nextSibling;
                //to hide Select All in Dist check if no id is empty  
                switch (true) {
                    case $(this).attr("id").indexOf("ddlUnassignedClinicsFilters") > -1: checkBox = $("INPUT[id$='chkSelectAllUnassignedClinicsInDistrict']");
                        break;
                    case $(this).attr("id").indexOf("ddlAssignedDistrictClinicsFilters") > -1: checkBox = $("INPUT[id$='chkSelectAllAssignedDistrictClinicsInDistrict']");
                        break;
                    case $(this).attr("id").indexOf("ddlUnassignedDirectB2BMailBusinessFilters") > -1: checkBox = $("INPUT[id$='chkSelectAllUnassignedDirectB2BMailBusinessInDistrict']");
                        break;
                    case $(this).attr("id").indexOf("ddlAssignedDistrictDirectB2BMailFilters") > -1: checkBox = $("INPUT[id$='chkSelectAllAssignedDistrictDirectB2BMailInDistrict']");
                        break;
                    case $(this).attr("id").indexOf("ddlUnassignedLocalLeadsFilters") > -1: checkBox = $("INPUT[id$='chkSelectAllUnassignedLocalLeadsInDistrict']");
                        break;
                    case $(this).attr("id").indexOf("ddlAssignedDistrictLocalLeadsFilters") > -1: checkBox = $("INPUT[id$='chkSelectAllAssignedDistrictLocalLeadsInDistrict']");
                        break;
                    default:
                        break;
                }
                $(this).find("option:selected").each(function () {
                    if ($(this).attr("value") == "District") {
                        if ($(txtBox).val().length > 0)
                            checkBox.parent().show();
                        else {
                            checkBox[0].checked = false;
                            checkBox.parent().hide();
                        }

                    }
                    else {
                        checkBox[0].checked = false;
                        checkBox.parent().hide();
                    }
                });
            }).change();
            $("input[id$='txtUnassignedClinicsFilter'],input[id$='txtAssignedDistrictClinicsFilter'],input[id$='txtUnassignedLocalLeadsFilter'],input[id$='txtAssignedDistrictLocalLeadsFilter'],input[id$='txtUnassignedDirectB2BMailBusinessFilter'],input[id$='txtAssignedDistrictDirectB2BMailFilter']").keyup(function () {
                //to hide Select All in Dist check if no id is empty 
                if ($(this).val().length == 0) {
                    var checkbox = $(this).parent().next()[0].firstChild.nextSibling;
                    $(checkbox).hide();
                }
            });

            $("span[id$='lblStatus']").each(function () {
                if ($(this).text().indexOf("No Client Interest") > -1)
                    $(this).closest('tr')[0].style.color = "gray";
            });
        });

        function initTab(tabType) {
     
            if (!Object.keys) { //Object.keys will not work with IE6. Below code is a workaround.
                Object.keys = function (obj) {
                    var keys = [];

                    for (var i in obj) {
                        if (obj.hasOwnProperty(i)) {
                            keys.push(i);
                        }
                    }

                    return keys;
                };
            }

            for (i = 0; i < Object.keys(tabType).length; i++) (function (i) {
                if (i < Object.keys(tabType).length) {
                    var button = $("input[id$='" + tabType[i].Button + "']");
                    var label = $("span[id$='" + tabType[i].Label + "']");
                    var flag = $("input[id$='" + tabType[i].flag + "']");
                    if ((typeof button !== 'undefined') && (typeof label[0] !== 'undefined')) {
                        Setlabels(flag, button, label);
                        button.on("click", function () {
                            if (label[0].style.display == '') {
                                flag.val("false");
                                Setlabels(flag, button, label);
                            }
                            else {
                                flag.val("true");
                                Setlabels(flag, button, label);
                            }
                            return false;
                        })
                    }
                }
            })(i);
        }

        //Show or hide info
        function Setlabels(flag, button, label) {
     
            if (flag.val() == "true") {
                label[0].style.display = '';
                button.src = "images/btn_hide_info.png";
                button.onmouseover = MouseOverImage(button.attr('id'), 'images/btn_hide_info_lit.png');
                button.onmouseout = MouseOutImage(button.attr('id'), 'images/btn_hide_info.png');
            }
            else {
                label[0].style.display = 'none';
                button.src = "images/btn_info.png";
                button.onmouseover = MouseOverImage(button.attr('id'), 'images/btn_info_lit.png');
                button.onmouseout = MouseOutImage(button.attr('id'), 'images/btn_info.png');
            }
        }

        function setTabs(grid_type) {
            var selected_tab;
            var selected_tab_id;
            var tab_id;

            switch (grid_type) {
                case "CorporateClinics":
                    selected_tab = '<%= commonAppSession.SelectedStoreSession.SelectedCorporateClinicsTabId %>';
                    tab_id = "tabsCorporateClinics";
                    break;
                case "DirectB2BMailBusiness":
                    selected_tab = '<%= commonAppSession.SelectedStoreSession.SelectedDirectB2BMailTabId %>';
                    tab_id = "tabsDirectB2BMailBusiness";
                    break;
                case "LocalLeads":
                    selected_tab = '<%= commonAppSession.SelectedStoreSession.SelectedLocalLeadsTabId %>';
                    tab_id = "tabsLocalLeads";
                    break;
            }

            switch (selected_tab) {
                case "UnassignedClinics":
                case "UnassignedDirectB2BMail":
                case "UnassignedLocalLeads":
                    selected_tab_id = 0;
                    break;
                case "AssignedDistrictClinics":
                case "AssignedToDistrictDirectB2BMail":
                case "AssignedToDistrictLocalLeads":
                    selected_tab_id = 1;
                    break;
                case "AssignedStoreClinics":
                case "AssignedToStoresDirectB2BMail":
                case "AssignedToStoresLocalLeads":
                    selected_tab_id = 2;
                    break;
                case "CancelledClinics":
                    selected_tab_id = 3;
                    break;
                case "ResolveDupBusiness":
                    selected_tab_id = 4;
                    break;
                default:
                    selected_tab_id = 0;
                    break;
            }

            var $tabs = $('#' + tab_id).tabs();
            if ($('#' + tab_id).tabs("length") == 2) { //For DM User
                if (selected_tab_id >= 2)
                    selected_tab_id -= 1;
            }
            $tabs.tabs('select', selected_tab_id);
        }

        function setSelectedTab(tab_id) {
            var grid_type;

            if (tab_id.indexOf("Clinics") > -1)
                grid_type = "CorporateClinics";
            else if (tab_id.indexOf("LocalLeads") > -1)
                grid_type = "LocalLeads";
            else 
                grid_type = "DirectB2BMailBusiness";

            $.ajax({
                url: "walgreensDistrictUsersHome.aspx/setSelectedTabId",
                type: "POST",
                data: "{'tab_id':'" + tab_id + "', 'grid_type':'" + grid_type + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                }
            });
        }

        function CloseDialog() {
            $('#dupBusinesses').dialog('close');
            $("#cdt_shadowbox").dialog('close');
            return false;
        }

        function showUnscheduleClinicWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 170,
                    width: 450,
                    title: "Reschedule Appointment confirmation",
                    buttons: {
                        'Continue': function () {                            
                            __doPostBack("rescheduleAppts", 1);
                            $(this).dialog('close');
                        },
                        'Cancel': function () {                            
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function showConfirmationDialog(title, postback_type, postback_param, warning_msg) {
            $(function () {
                warning_msg.replace("\n", "<br />");
                $("#divConfirmDialog").html(warning_msg);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: title,
                    buttons: {
                        'Continue': function () {
                            __doPostBack(postback_type, postback_param);
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
            return false;
        }

 
        //For clearing the search boxes
        function tog(v) {
            return v ? 'addClass' : 'removeClass';
        }

        $(document).on('hover input', '.clearable', function () {
            $(this)[tog(this.value)]('x');
        }).on('mousemove', '.x', function (e) {
            $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
        }).on('click', '.onX', function () {
            $(this).removeClass('x onX').val('').change();
            //$(this).trigger($.Event("keypress", { keyCode: 13, which: 13 })); 
            $(this).next().click();
        });
    </script>
    
    <style type="text/css">
        .ui-widget
        {
            font-size: 11px;
        }
        
        .ui-widget-header
        {
            border-bottom: 0px none #FFFFFF;
        }
        
        .ui-tabs .ui-tabs-panel
        {
            padding: 0px;
        }
        
        .ui-state-active, .ui-state-active, .ui-widget-header .ui-state-active
        {
            background: url("images/tab_gradient.jpg") repeat-x scroll 50% 50%;
            background-repeat: repeat-x;
            background-position: bottom;
        }
        .ui-tabs .ui-tabs-nav li a, .ui-tabs.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-selected a
        {
            cursor: pointer;
            text-align: left;
        }
        .ui-tabs .ui-tabs-nav li
        {
            text-align: center;
        }
        .ui-tabs .ui-tabs-nav li a
        {
            padding: 0.25em 1em;
        }
        
        /* ui-tabs .ui-tabs-nav li a 
	    {
		    padding-top:5px;
		    padding-left:0px;
		    padding-right:0px;		
		    float:none;
		    height:30px;
	    }*/
	    
        .clearable
        {
            background: none;
            border: 1px solid #999;
            padding: 3px 18px 3px 4px;
            border-radius: 3px;
        }
        
        .clearable:hover, .clearable.x
        {
            background: #fff url('images/btn_clear_search.png') no-repeat right center;
        }
        
        .clearable.onX
        {
            cursor: pointer;
        }
        
        .clearable::-ms-clear
        {
            display: none;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient" onload="javascript:setTabs('CorporateClinics'); setTabs('DirectB2BMailBusiness'); setTabs('LocalLeads');">
    <form id="form1" runat="server" defaultbutton="btnRefreshActionDropdown">
    <asp:ImageButton ID="btnRefreshActionDropdown" runat="server" Visible="true" style="display:none" OnClick="btnRefreshActionDropdown_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />
    <%--<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" Style="display: none" OnClick="btnRefresh_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />
    <asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" Style="display: none"></asp:TextBox>--%>
    <input type="hidden" id="_ispostback" value="<%=Page.IsPostBack.ToString()%>" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="center"  style="padding-top:24px; padding-bottom:24px">
	          <table width="888" border="0" cellspacing="0" cellpadding="0">
                <tr runat="server" id="rowImmunizationSnapshot">
                  <td style="padding-bottom: 20px;" >
		            <table width="100%" cellpadding="0" cellspacing="0">
                      <tbody>
                        <tr>
                          <td class="snapshotHCSTextHead" align="left">Total Immunization Outreach Snapshot</td>
                        </tr>                    
                        <tr>
                          <td class="snapshotHCSMonitor">
                            <table width="100%" cellpadding="0" cellspacing="15px">
                              <tr runat="server" id="rowActionItemsHCSUsers">
                                <td align="left" colspan="2">
                                  <table border="0" cellspacing="0" cellpadding="0">
                                    <%--<tr>
                                        <td>
                                            <asp:Label ID="lblDataSize" CssClass="noteText" runat="server"></asp:Label>                                            
                                        </td>
                                    </tr>--%>
                                    <tr>
                                      <td><span class="logSubTitles">HCS Users: </span>
                                      <%--<asp:DropDownList CssClass="formFields" ID="ddlHCSUsers" runat="server" EnableViewState="false" Width="340px"></asp:DropDownList>--%>
                                      <asp:TextBox CssClass="formFields"  ID="txtSelectedUserId" runat="server" Width="340px" Style="display: none"></asp:TextBox>
                                      <asp:TextBox CssClass="formFields"  ID="txtSelectedUser" runat="server" Width="340px"></asp:TextBox>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td width="50%" valign="top"><ucWActionItem:walgreensActionItems ID="walgreensActionItems" runat="server" /></td>
                                <td width="50%" valign="top"><ucWMetricsReports:walgreensMetricsReports ID="walgreensMetricsReports" runat="server" /></td>
                              </tr>
                            </table>
	                      </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 20px;" >
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                          <tr>
                            <td class="corporateHCSTextHead" align="left">Corporate/Uploaded Clinics</td>
                          </tr>
                          <tr>
                            <td colspan="2" bgcolor="#FFFFFF" class="corporateHCSMonitor">
                              <ucWCorporateClinics:walgreensCorporateClinics ID="walgreensCorporateClinics" runat="server" />
                              <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 13px; text-align:center; font-weight:bold;"></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                </tr>
                <tr runat="server" id="rowDirectB2BMailBusinessSection">
                    <td style="padding-bottom: 20px;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                          <tr>
                            <td class="mailHCSTextHead" align="left">Direct B2B Campaigns</td>
                          </tr>
                          <tr>
                            <td bgcolor="#FFFFFF" class="mailHCSMonitor">&nbsp;
                                <ucWDirectB2BCampaigns:walgreensDirectB2BCampaigns ID="walgreensDirectB2BCampaigns" runat="server" />
                                <div id="div1" style="display: none; font-family:Arial; font-size: 13px; text-align:center; font-weight:bold;"></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                </tr>
                <tr runat="server" id="rowLocalLeadsSection">
                    <td style="padding-bottom: 20px;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                          <tr>
                            <td class="localLeadsHCSTextHead" align="left">Local Leads</td>
                          </tr>
                          <tr>
                            <td bgcolor="#FFFFFF" class="localLeadsHCSMonitor">&nbsp;                                
                                <ucWLocalLeads:walgreensLocalLeads ID="walgreensLocalLeads" runat="server" />
                                <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 13px; text-align:center; font-weight:bold;"></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                </tr>
            </table>
          </td>
        </tr>        
    </table>
	<ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
