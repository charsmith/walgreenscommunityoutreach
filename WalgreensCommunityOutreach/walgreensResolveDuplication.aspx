﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensResolveDuplication.aspx.cs" Inherits="walgreensResolveDuplication" %>
<%@ Register src="controls/walgreensHeader.ascx" tagname="walgreensHeader" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Walgreens Community Outreach</title>
<link href="css/wags.css" rel="stylesheet" type="text/css" />    
<script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
<link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />     
<link href="css/theme.css" rel="stylesheet" type="text/css" />
<script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
<script type="text/javascript"  src="javaScript/jquery-ui.js"></script>
<script src="javaScript/commonFunctions.js" type="text/javascript"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnRefresh">
<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" style="display:none" OnClick="btnRefresh_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />  
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>  
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <uc2:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
              <td><table width="935" border="0" cellspacing="22" cellpadding="0">
                <tr>
                    <td valign="top" class="pageTitle">Resolve Duplicate Businesses</td>
                  </tr>
                  <tr>
                    <td valign="top" ><span class="formFields">The following businesses have one or more fields that have duplicates in the Outreach Program. To reduce the occurrence of duplicate efforts being made, please choose whether to Keep or Remove the records below. You will not be able to remove businesses that already have contacts logged.</span></td>
                  </tr>
                  <tr>
                    <td align="center" valign="top" class="formFields2" ><asp:GridView ID="grdDuplicateBusiness" runat="server"  AutoGenerateColumns="False" CellPadding="1" 
                    CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10" OnSorting="grdDuplicateBusiness_sorting" OnPageIndexChanging ="grdDuplicateBusiness_PageIndexChanging" OnRowDataBound="grdDuplicateBusiness_RowDataBound"
                                                    GridLines="None"  Width="100%" DataKeyNames="businessAddress,phone,duplicateType">
                                                <FooterStyle CssClass="footerGrey" />
                                                <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true"  />
                                                <RowStyle BorderWidth="1px" BorderColor= "#CCCCCC" />                                               
                                              <Columns>                                                
                                                    <asp:TemplateField HeaderText="businessPk" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Store Id" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Height="25px" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Business Name" SortExpression="businessName" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>' width="100px" class="wrapword"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                    
                                                    <asp:TemplateField HeaderText="Address" SortExpression="businessAddress" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("businessAddress") %>' width="150px" class="wrapword"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                    
                                                    <asp:TemplateField HeaderText="Phone" SortExpression="phone" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("phone") %>' width="85px" class="wrapword"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Duplication" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblDuplication" runat="server" Text='<%# Bind("duplicateType") %>' Width="100" class="wrapword" CssClass="wrapword" ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                 
                                                     <asp:TemplateField HeaderText="Contacts" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                        <ItemTemplate>
                                                        <asp:Label ID="lblContacts" runat="server" Text='<%# Bind("contacts") %>' Width="50" class="wrapword" CssClass="wrapword" ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                   
                                                    <asp:TemplateField HeaderText="Resolution" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblKeepBusiness" runat="server" Text='<%# Bind("keepBusiness") %>' Visible="false"></asp:Label>
                                                            <asp:RadioButtonList ID="rblResolution" runat="server" CssClass="formFields" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2">
                                                            <asp:ListItem Value="1" Text="Keep"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="Remove"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                    
                                              </Columns>
                                              <EmptyDataTemplate>
						                        <center>
						                          <b>
						                            <asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields"  runat="server"></asp:Label>
						                          </b>
						                        </center>
						                      </EmptyDataTemplate>
						                      <SelectedRowStyle BackColor="LightBlue" />
						                    </asp:GridView></td>
						                  </tr>
						                  <tr>
						                    <td align="right" valign="top" ><span style="padding-top:0px">
						                      <asp:ImageButton ID="btnSkip" ImageUrl="~/images/btn_skip.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_skip.png');" CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_skip_lit.png');" runat="server" PostBackUrl="~/walgreensHome.aspx"  AlternateText="Skip" />                      
						                      &nbsp;
						                      <asp:ImageButton ID="btnSubmit" ImageUrl="~/images/btn_submit_event.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_event.png');" CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_event_lit.png');" runat="server" onclick="btnSubmit_Click" AlternateText="Process duplicate business" />                      
						                    </span></td>
							                </tr>
							                </table></td>
  </tr>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>
