﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensClinicAgreementPrevSeason_Sp.aspx.cs"   Inherits="walgreensClinicAgreementPrevSeason_Sp" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css" />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css" />
    <link href="css/calendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/jquery.printelement.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />

    <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>

    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>


    <style type="text/css">
        .ui-timepicker-list li
        {
            color: #000000;
            font-family: "Times New Roman",Times,serif;
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            // for part of the page
            $("#lnkPrintContract").click(function () {
                $("#trContractBodyText").printElement({ printMode: 'popup' });
            });
        });
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfContactLogPk" runat="server" />
    <asp:HiddenField ID="hfUserEmail" runat="server" />
    <asp:HiddenField ID="hfUserType" runat="server" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="left">
              <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td colspan="2" valign="top" class="pageTitle">Acuerdo Para Clínica Externa En La Comunidad</td>
                    </tr>
                    <tr>
                      <td valign="top" width="599" ><table width="599" border="0" cellpadding="0">
                        <tr>
                          <td>
                            <table width="612" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td  class="contractBodyText" style="padding-bottom:6px">
                           <table  align="right">
                               <tr>
                                   <td>
                                        <asp:LinkButton ID="lnkChangeCulture" runat="server" Visible="true" onclick="lnkChangeCulture_Click">English Version</asp:LinkButton> 
                                   </td>
                                   <td>
                                        <asp:LinkButton ID="lnkPrintContract" runat="server" Visible="false">Imprimir el Contrato</asp:LinkButton>
                                   </td>
                               </tr>
                           </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="612" border="0" cellspacing="0" cellpadding="36" id="trContractBodyText" style="border:#CCC solid 1px" >
                                <tr>
                                    <td class="contractBodyText">
                                      <p align="center">
                                            <b>
                                                <img src="images/contract_logo.gif" width="191" height="37" alt="Walgreens" /><br />
                                                ACUERDO PARA CLÍNICA EXTERNA EN LA COMUNIDAD</b></p>
                                        <p align="justify">
                                            Este <strong><em>ACUERDO PARA CLÍNICA EXTERNA EN LA COMUNIDAD</em></strong> (el“<b>Acuerdo</b>”) por y entre la parte indicada adelante (el “<b>Cliente</b>”), y Walgreen Co., en nombre propio y en el de todas sus subsidiarias y afiliadas (“<b>Walgreens</b>”) acuerdan y entra en vigor en la fecha en la cual sea firmado electrónicamente por un representante autorizado, tanto del Cliente como de Walgreens (la “<b>Fecha de Vigencia</b>”).</p>
                                        <p align="justify">
                                            A cambio de una buena y valiosa consideración, el recibido y la suficiencia de la cual por la presente es reconocida, el Cliente y Walgreens, mediante firmar adelante electrónicamente, acuerdan que (i) Walgreens les proveerá el dispensado y el administrado de ciertas inmunizaciones o vacunas, según listadas adelante (“<b>Vacuna(s)</b>”) a los participantes (los “<b>Participantes</b>”) en fechas y horas mutuamente acordadas en la(s) facilidad(es) del Cliente listada(s) adelante (“<b>Servicios Cubiertos para Vacunas</b>” - “Covered Vaccine Services”); y (ii) que cumplirá con los términos y las condiciones contenidas en este Acuerdo, según descritas en la siguientes páginas.</p>
                                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="5" id="tblBusinesses" runat="server">
                                            <tr>
                                                <td  align="left">
                                                    <b>Ubicación de la(s) Facilidad(es) del Cliente*:</b>
                                                </td>
                                                <td  align="right">
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:GridView ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" DataKeyNames="state,clinicDate" >
                                                        <Columns>
                                                            <asp:BoundField DataField="state" Visible="false" />
                                                            <asp:BoundField DataField="clinicDate" Visible="false" />
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="100%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                <table width="100%"  style="border-style:solid; border-width:0px; border-color:Black">
                                                                  <tr>
                                                                    <td colspan="2" align="left">
                                                                        <asp:Label ID="lblClinicLocation" Text='<%# Bind("clinicLocation") %>' runat="server" style="font-weight:bold"></asp:Label>
                                                                    </td>
                                                                   <%-- <td colspan="3" align="right">
                                                                        <asp:Label ID="lblIsReassignClinic" runat="server" Text='<%# Bind("isReassign") %>' Visible="false" ></asp:Label>
                                                                        <asp:CheckBox ID="chkReassignClinic" runat="server" Text="Reasignar basado en la geografía*" Checked='<%# Convert.ToInt32(Eval("isReassign")) == 1 ? true : false %>'/>
                                                                    </td>--%>
                                                                  </tr>
                                                                </table>                                                                    
                                                                <table width="100%"  style="border-style:solid; border-width:1px; border-color:Black">
                                                                     <tr>
                                                                        <td><b>Nombre del Contacto Local</b></td>
                                                                        <td><b>Teléfono del Contacto Local</b></td>
                                                                        <td colspan="3"><b>Dirección Electrónica del Contacto Local</b></td>                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="lblLocalContactName" runat="server" Text='<%# Bind("localContactName") %>' CssClass="contractBodyText"  Width="100px" ></asp:Label></td>
                                                                        <td><asp:Label ID="lblLocalContactPhone" runat="server" Text='<%# Bind("LocalContactPhone") %>' CssClass="contractBodyText" Width="100px" ></asp:Label></td>
                                                                        <td colspan="3"><asp:Label ID="lblLocalContactEmail" runat="server" Text='<%# Bind("LocalContactEmail") %>'  CssClass="contractBodyText" Width="100px" ></asp:Label></td>         
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Dirección 1</b></td>
                                                                        <td><b>Dirección 2</b></td>
                                                                        <td><b>Ciudad</b></td>
                                                                        <td><b>Estado</b></td>
                                                                        <td><b>Código Postal</b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="lblAddress1" runat="server" Text='<%# Bind("Address1") %>' CssClass="contractBodyText" Width="100px" ></asp:Label></td>
                                                                        <td><asp:Label ID="lblAddress2" runat="server" Text='<%# Bind("Address2") %>' CssClass="contractBodyText" Width="100px" ></asp:Label></td>
                                                                        <td><asp:Label ID="lblCity" runat="server" Text='<%# Bind("city") %>'  CssClass="contractBodyText" Width="100px" MaxLength="100" ></asp:Label></td>
                                                                        <td><asp:Label ID="lblState" runat="server"  CssClass="contractBodyText" Width="95%" Text='<%# Bind("state") %>' ></asp:Label></td>
                                                                        <td><asp:Label ID="lblZipCode" runat="server" CssClass="contractBodyText" Width="95%" Text='<%# Bind("zipCode") %>' ></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Fecha de la Clínica</b></td>
                                                                        <td><b>Comenzando a la(s)</b></td>
                                                                        <td><b>Terminando a la(s)</b></td>
                                                                        <td><b>No. Estimado de vacunas:</b></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:Label ID="lblClinicDateTime" runat="server" Text='<%# Bind("clinicDate") %>' CssClass="contractBodyText" ></asp:Label></td>
                                                                        <td><asp:Label ID="lblStartTime" runat="server"  Text='<%# Bind("startTime") %>' CssClass="contractBodyText" Width="55px" ></asp:Label></td>
                                                                        <td><asp:Label ID="lblEndTime" runat="server" Text='<%# Bind("endTime") %>' CssClass="contractBodyText" Width="55px" ></asp:Label></td>
                                                                        <td><asp:Label ID="lblEstShots" runat="server" Text='<%# Bind("EstShots") %>' CssClass="contractBodyText" Width="55px" ></asp:Label></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                                <br />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr id="rowReassignClinicStore" runat="server">
                                                <td colspan="2">*Clínicas serán reasignados a otra tienda si la ubicación de la clínica es más de 10 millas de la tienda de contraer o cruza las fronteras estatales.</td>
                                            </tr>
                                        </table>
                                        <table id="tblAgreement" runat="server" width="100%" border="0"><tr><td>
                                        <p align="center">
                                            <b>EN FE DE LO CUAL</b>, el Cliente y Walgreens han firmado electrónicamente este Acuerdo, a partir de la Fecha de Vigencia.</p>
                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
                                            <tr>
                                                <td width="46" align="left" valign="middle">
                                                    <b>CLIENTE:</b>
                                                </td>
                                                <td width="210" align="left" valign="middle">
                                                    <asp:Label ID="lblClient" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                </td>
                                                <td colspan="2" align="left" valign="middle">
                                                    <b><u>WALGREEN CO.</u></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="middle">
                                                    NOMBRE:
                                                </td>
                                                <td align="left" valign="middle">
                                                    <asp:Label ID="lblClientName" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                </td>
                                                <td width="46" align="left" valign="middle">
                                                    NOMBRE:
                                                </td>
                                                <td width="210" align="left" valign="middle">
                                                    <asp:Label ID="lblWalgreenName" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>                                                  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="middle">
                                                    TÍTULO:
                                                </td>
                                                <td align="left" valign="middle">
                                                    <asp:Label ID="lblClientTitle" runat="server" Width="200px" CssClass="contractBodyText"></asp:Label>
                                                </td>
                                                <td align="left" valign="middle">
                                                    TÍTULO:
                                                </td>
                                                <td align="left" valign="middle">
                                                    <asp:Label ID="lblWalgreenTitle" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="middle">
                                                    FECHA:
                                                </td>
                                                <td align="left" valign="middle">
                                                    <asp:Label ID="lblClientDate" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                </td>
                                                <td align="left" valign="middle">
                                                    FECHA:
                                                </td>
                                                <td align="left" valign="middle">
                                                    <asp:Label ID="lblWalgreenDate" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="middle">
                                                    <u>Envíele Avisos Legales al Cliente A:</u>
                                                </td>
                                                <td colspan="2" align="left" valign="middle">
                                                    DISTRITO No:&nbsp;<asp:Label ID="lblDistrictNum" runat="server" Width="103px" CssClass="contractBodyText" ></asp:Label>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td colspan="2" align="left" valign="middle" rowspan="3" >
                                                <table width="100%" border="0">
                                                <tr>
                                               <td>Atención:</td>
                                                    <td align="left"><asp:Label ID="lblLegalAttentionTo" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                   </td>
                                                  </tr>
                                                        <tr>
                                                    <td>Dirección 1:</td>
                                                     <td  align="left">
                                                            <asp:Label ID="lblLegalAddress1" runat="server" CssClass="contractBodyText" ></asp:Label>
                                                     </td>
                                                 </tr>
                                                      
                                                     <tr>
                                                       <td>Dirección 2:</td>
                                                       <td align="left">
                                                    <asp:Label ID="lblLegalAddress2" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                       </td></tr>
                                                        <tr>
                                                        <td>Ciudad:</td>
                                                   <td align="left"> <asp:Label ID="lblLegalCity" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                         <td>Estado:</td> 
                                                 <td  align="left"><asp:Label ID="lblLegalState" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                 </td> 
                                                       </tr>
                                                        <tr> <td>Código Postal:</td>
                                                    <td  align="left"><asp:Label ID="lblLegalZipCode" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                        </td> </tr>  </table>
                                                </td>
                                          </tr>
                                           <tr>
                                                <td colspan="2" align="left" valign="middle">
                                                    <u>Envíele Avisos Legales al Walgreens A:</u>
                                                </td>
                                          </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top">
                                                  Healthcare Innovations Group<br />
                                                  200 Wilmot Rd<br />
                                                  MS2222<br />
                                                  Deerfield, IL 60015<br />
                                                  Attn: Health Law – Divisional Vice President<br />
                                                  cc: clinicalcontracts@walgreens.com</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top">
                                                    <asp:GridView ID="grdImmunizationChecks" runat="server" AutoGenerateColumns="False" GridLines="None" onrowdatabound="grdImmunizationChecks_RowDataBound" Width="100%">
                                                            <Columns>
                                                                <asp:BoundField DataField="ImmunizationName" Visible="false" />
                                                                <asp:TemplateField HeaderText="<u>Vacuna</u>" HeaderStyle-Width="70%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblImmunizationCheck" runat="server" Text='<%# Bind("spanishName") %>'/>
                                                                         <asp:Label ID="lblImmunizationPk" runat="server" Text='<%# Bind("pk") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>el Precio*</u>" HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" >
                                                                    <ItemTemplate>                                                                        
                                                                        <asp:Label  style="text-align:left" ID="lblValue"  runat="server" Text='<%# Bind("price") %>' Width="50px" ></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                    </asp:GridView>
                                                    <br />
                                                    <table border="0" cellpadding="0" cellspacing="0" width="95%">
                                                        <tr>
                                                            <td><asp:Label ID="lblImmunizationDisclaimer" runat="server" Text="*El costo incluye la vacuna y el administrado."></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>**El costo para la influenza está basado sobre el siguiente número mínimo a ser facturado. El precio no cambiará aunque el número de vacunas exceda el mínimo</td>
                                                        </tr>
                                                        <tr><td><asp:Label ID="lblMinimumInvoiced" runat="server" Visible="false"></asp:Label></td></tr>
                                                    </table>                                                    
                                                </td>                                                 
                                                <td colspan="2" align="left" valign="top">
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="grdPaymentTypes" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" onrowdatabound="grdPaymentTypes_RowDataBound"  >
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="90%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>                                           
                                                                        <table id="Table1" width="100%" runat="server" border="0" >
                                                                            <tr>
                                                                                <td colspan="2" valign="top"><b>TIPO DE PAGO: </b>
                                                                                    <asp:Label ID="lblPaymentType" CssClass="contractBodyText" runat="server" Text='<%# Bind("paymentType") %>' ></asp:Label>
                                                                                    <asp:Label ID="lblPaymentTypeId" Text='<%# Bind("paymentId") %>' runat="server" Visible="false"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <table width="100%" id="tblPaymentInfo" runat="server" border="0" visible="false" >
                                                                                        <tr><td colspan="2">Envíele la Factura a:</td></tr>
                                                                                        <tr>
                                                                                            <td>Nombre:</td>
                                                                                            <td><asp:Label ID="lblPmtName" CssClass="contractBodyText" runat="server" Text='<%# Bind("name") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Dirección 1:</td>
                                                                                            <td><asp:Label ID="lblPmtAddress1" CssClass="contractBodyText" runat="server" Text='<%# Bind("address1") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Dirección 2:</td>
                                                                                            <td><asp:Label ID="lblPmtAddress2" CssClass="contractBodyText" runat="server" Text='<%# Bind("address2") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Ciudad:</td>
                                                                                            <td><asp:Label ID="lblPmtCity" CssClass="contractBodyText" runat="server" Text='<%# Bind("city") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Estado:</td>
                                                                                            <td><asp:Label ID="lblPmtState" CssClass="contractBodyText" runat="server" Text='<%# Bind("state") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Código Postal:</td>
                                                                                            <td><asp:Label ID="lblPmtZipCode" CssClass="contractBodyText" runat="server" Text='<%# Bind("zip") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Teléfono:</td>
                                                                                            <td><asp:Label ID="lblPmtPhone" CssClass="contractBodyText" runat="server" Text='<%# Bind("phone") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Dirección Electrónica:</td>
                                                                                            <td><asp:Label ID="lblPmtEmail" CssClass="contractBodyText" runat="server" Text='<%# Bind("email") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">¿Está Exento de Impuestos el patrono/empleador?<asp:Label ID="lblPmtTaxExempt" CssClass="contractBodyText" runat="server" Text='<%# Bind("tax") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">¿Pagará el paciente una parte del costo – hay in copago?<br />
                                                                                                <asp:Label ID="lblIsCopay" CssClass="contractBodyText" runat="server" Text='<%# Bind("copay") %>'></asp:Label>
                                                                                                <asp:Label ID="lblDollar" runat="server" Text="$"></asp:Label>
                                                                                                <asp:Label CssClass="contractBodyText" ID="lblCopy" runat="server" Text='<%# Bind("copayValue") %>'></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">Necesita un vale:<asp:Label ID="lblVocher" CssClass="contractBodyText" runat="server" Text='<%# Bind("isVoucherNeeded") %>'></asp:Label></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>                                                                    
                                                                        </table>
                                                                        <br />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                            
                                                                </Columns>
                                                            </asp:GridView>
                                                            </td>
                                                        </tr>
                                                     </table>                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="middle">&nbsp;</td>
                                                <td colspan="2" align="left" valign="middle">&nbsp;</td>
                                            </tr>
                                        </table>
                                        <p align="center"><b>ACUERDO DE WALGREENS PARA CLÍNICA EXTERNA EN LA COMUNIDAD <br />TÉRMINOS Y CONDICIONES</b></p>
                                        <table width="540" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="47%" align="left" valign="top">
                                                    <p align="justify">
                                                        <b>I. Responsabilidades de Walgreens</b><br />
                                                        <u>Servicios Cubiertos para las Vacunas.</u> Sujeto a los límites o las restricciones impuestas bajo contratos federales y estatales, las leyes y los reglamentos federales y estatales y a la disponibilidad de las Vacunas apropiadas, Walgreens les proveerá a los Participantes los Servicios Cubiertos para las Vacunas. Respecto a dichos Servicios Cubiertos, las partes cumplirán con los procedimientos aquí establecidos.
                                                    </p>
                                                    <p align="justify">
                                                        <u>Provisto de Profesionales en el Cuidado de la Salud.</u> Walgreens le proveerá al Cliente el número apropiado de profesionales y técnicos en el cuidado de la salud para poder proveer los Servicios Cubiertos para las  Vacunas.<br />
                                                        <br />
                                                        <u>Opinión Profesional.</u> Walgreens puede negarle a un Participante los  Servicios Cubiertos para las Vacunas debido a razones válidas, incluyendo pero no necesariamente limitado a que un Participante no pague por los Servicios Cubiertos para las Vacunas; pedidos por un Participante que sean inconsistentes con los requisitos legales y regulatorios; o donde, en la opinión profesional del profesional en el cuidado de la salud, los servicios no deberían ser prestados.</p>
                                                    <p align="justify">
                                                        <b>II. Responsabilidades del Cliente</b><br />
                                                        <u>Coordinación.</u> El Cliente les notificará a los Participantes cuándo y dónde los Servicios Cubiertos para las Vacunas serán provistos y les proveerá al personal de Walgreens y a los Participantes un sitio privado, limpio y equipado con mesas y sillas. Si  aplicable, el Cliente les proveerá a los Participantes con vales aprobados por Walgreens, los cuales los Participantes podrán redimir en una farmacia participante de Walgreens.</p>
                                                    <p align="justify">
                                                        <u>Acceso.</u> Por la presente el Cliente le concede a Walgreens, y no a ninguna otra persona o entidad, acceso a su área designada para el prestado de los Servicios Cubiertos para <u>las Vacunas</u> durante las horas y en las fechas mutuamente acordadas entre las partes, según  estipulado en este Acuerdo.</p>
                                                    <p align="justify">
                                                        <u>Pagos.</u> Antes de la prestación de los Servicios de Vacunación Cubiertos, el Participante debe de proporcionar evidencia de cubierta bajo un seguro de terceros o un programa del gobierno (v.gr.: Medicare). Si tal evidencia es presentada por el Participante y Walgreens se contrata con un seguro de terceros o un programa del gobierno, Walgreens presentará a su vez la reclamación de reembolso por dicho Participante y el monto a pagar por concepto de copago, coaseguro y deducible incurrido por el Participante se cobrará en una fecha posterior. Si dichas pruebas no se proporcionan en el momento de la prestación de los servicios, el Cliente o el Participante asumirá la responsabilidad de compensar a Walgreens con el menor de los precios establecidos o el Cargo Usual y Acostumbrado de la Vacuna. Los pagos a ser efectuados por el Cliente vencen para el pago a los treinta (30) días siguientes a la recepción de la factura mensual y deben remitirse a la dirección de envío indicada en la factura. La factura incluirá los siguientes datos y ninguna otra información será provista: número de identificación de grupo, número de la tienda, número de la receta, nombre del paciente, número del destinatario, nombre del médico, costo, cargos por servicio, monto del copago, IVU, importe total, fecha del servicio y nombre del medicamento/Código Nacional de Fármacos (NDC, por sus siglas en inglés). Como se usa en el presente Acuerdo, el “Cargo Usual y Acostumbrado” se refiere a la cantidad que la farmacia que administra los servicios cobra a un cliente que paga en efectivo, excluyente de importes reclamados y de IVU.</p>
                                                    <p align="justify">
                                                        <b>III. Término y Terminación</b><br />
                                                        <u>Término y Terminación.</u> Este Acuerdo comenzará en la Fecha de Vigencia y continuará por un año.  Cualquiera de las partes puede terminar este Acuerdo mediante notificación escrita a la otra parte.</p>
                                                    <p align="justify">
                                                        <u>Efecto de la Terminación.</u> La Terminación no tendrá ningún efecto sobre los derechos o las obligaciones de las partes que hayan surgido de las transacciones que hayan ocurrido antes de la fecha de dicha terminación.</p>
                                                    <p align="justify">
                                                        <b>IV. Indemnización</b><br />
                                                        <u>Indemnización.</u> Hasta el punto permitido bajo la ley, cada parte indemnizará, defenderá y eximirá a la otra parte, incluyendo a sus empleados y agentes, de y contra toda y cualquier reclamación o responsabilidad que surja como resultado de negligencia o de acto ilegal de la parte que indemniza, a sus empleados, o a sus agentes, en el llevado a cabo de sus deberes y obligaciones bajo los términos de este Acuerdo. Esta sección sobrevivirá la terminación de este Acuerdo.</p>
                                                </td>
                                                <td width="6%" align="left" valign="middle">&nbsp;
                                                    
                                                </td>
                                                <td width="47%" colspan="2" align="left" valign="top">
                                                    <p align="justify">
                                                        <b>V. Seguro</b><br />
                                                        <u>Seguro.</u> Cada parte se auto asegurará o mantendrá, exclusivamente a expensas propias y en los montos consistentes con los estándares de la industria, Seguro de acuerdo con los términos de la póliza de seguro de Walgreens. Evidencia de dicho seguro puede ser obtenida mediante descargar/bajar el “Walgreens Memorandum of Liability Insurance and Memorandum of Professional Liability Insurance” y otra información pertinente sobre el programa de seguro de Walgreens en <a href="http://www.walgreens.com/Insurance">www.walgreens.com/Insurance</a>.
                                                        <br />
                                                        <br />
                                                        <b>VI. Términos Generales</b><br />
                                                        <u>Confidencialidad de la PHI.</u> Ambas partes garantizan que mantendrán y protegerán la confidencialidad de toda la información de la salud de los Participantes que pueda ser identificada individualmente (“Protected Health Information,” o “PHI”) de acuerdo con la Ley para la Responsabilidad y la Portabilidad de los Seguros para la Salud del 1996 (“the Health Insurance Portability and Accountability Act of 1996”) y todas las leyes y los reglamentos federales y estatales aplicables. No obstante, nada contenido aquí limitará el uso por una de las partes de información agregada sobre los Participantes que no contenga PHI. Esta sección sobrevivirá la terminación de este Acuerdo.</p>
                                                    <p align="justify">
                                                        <u>Publicidad.</u> Ninguna parte puede darle publicidad ni usar las marcas registradas, marcas para servicio o los símbolos de la otra parte, sin antes haber recibido el permiso escrito de la parte dueña de la marca registrada, marca de servicio y/o el/los símbolo(s), excepto en los siguientes casos: el Cliente puede hacer uso del nombre y de las direcciones de las farmacias de Walgreens en los materiales que les informen a los Participantes y al público en general que Walgreens provee los Servicios Cubiertos para las Vacunas. Toda otra referencia a Walgreens en cualquier material del Cliente tiene que ser aprobada antes de mano, por escrito, por Walgreens.</p>
                                                    <p align="justify">
                                                        <u>Fuerza Mayor.</u> El cumplimiento por cada parte bajo este acuerdo será excusado hasta el punto que circunstancias fuera del control razonable de la parte, tales como inundaciones, tornado/huracán, terremoto u otro desastre natural, epidemia, guerra, destrucción material de las facilidades, fuego, actos de terrorismo, actos de Dios, etc. En dichos casos, las partes harán lo mejor que puedan para resumir las funciones tan pronto como razonablemente posible bajo las circunstancias que causaron que la parte interrumpiese las funciones.</p>
                                                    <p align="justify">
                                                        <u>Cumplimiento.</u> Las partes cumplirán con todas las leyes, las reglas y los reglamentos aplicables en cada jurisdicción en la cual se presten los Servicios Cubiertos para las Vacunas bajo este Acuerdo. Cada parte cooperará con los pedidos razonables de la otra parte para información que sea necesaria para ella poder cumplir con las leyes, las reglas y/o los reglamentos aplicables.</p>
                                                    <p align="justify">
                                                        <u>Notificaciones.</u> Todas las notificaciones aquí provistas tienen que ser hechas por escrito y ser enviadas por correo americano certificado, acuse de recibo pedido, franqueo pagado, o mediante servicio de entrega de la noche a la mañana que provea evidencia de entrega a las direcciones que aparecen adelante bajo los encasillados para las firmas. Las notificaciones serán consideradas como entregadas al ser recibidas, o a la entrega ser rehusada.</p>
                                                    <p align="justify">
                                                        <u>Acuerdo en su Totalidad.</u> Este Acuerdo, y todo adjunto, documento, aditamento o documento al cual se le haga referencia aquí, constituyen el acuerdo total entre las partes en lo relacionado con el asunto aquí descrito y remplaza todo contrato anterior, y ningún cambio, enmienda o alterado será efectivo a menos que sea por escrito y firmado por un representante de cada parte. Los acuerdos, documentos, entendimientos o las representaciones anteriores relacionadas con el tema de este Acuerdo que no hayan sido  expresamente expuestas aquí, no serán válidas, ni tendrán efecto.
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">&nbsp;
                                                    
                                                </td>
                                                <td align="left" valign="middle">&nbsp;
                                                    
                                                </td>
                                              <%--  <td colspan="2" align="right" valign="top">
                                                    <b>Customer Initials</b>
                                                    <asp:TextBox ID="txtInitials2" runat="server" Width="50px" CssClass="contractBodyText" style="background-color: #FF0"  MaxLength="50" ></asp:TextBox>
                                                    
                                                </td>--%>
                                            </tr>
                                        </table>
                                        <p align="right">
                                            <span style="font-size: 10px;">©2015 Walgreen Co. Reservados todos los Derechos.</span>
                                        </p>
                                        </td></tr></table>
                                       
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                          </td>
                        </tr>
                      </table></td>
                      <td valign="top" width="268" style="padding-top:20px"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                            <tr>
                              <td>
                                 <table border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                         <td  align="left" valign="top">
                                             <asp:Label ID="lblErrorMessage" runat="server" class="bestPracticesText" ForeColor="Red"></asp:Label>
                                         </td>
                                    </tr>
                                    <tr>
                                    <td colspan="2" align="left" valign="top">
                                        <table runat="server" id="tblEmails" visible="false"><tr><td><span class="bestPracticesText">
                                            <b>Envíe el acuerdo por correo electrónico a:
                                                <asp:TextBox ID="txtEmails" runat="server" CssClass="bestPracticesText" ValidationGroup="WalgreensUser" Width="205px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtEmails" ID="txtEmailsReqFV" runat="server" Display="None" ErrorMessage="Introduzca una dirección de correo electrónico en el mensaje de correo electrónico Acuerdo campo para:" ValidationGroup="WalgreensUser" ></asp:RequiredFieldValidator>
                                                <%--<asp:RegularExpressionValidator ControlToValidate="txtEmails" ID="txtEmailsRegEV" runat="server" Display="None" ErrorMessage="The 'Email Agreement to:' email is Invalid." ValidationGroup="WalgreensUser" ValidationExpression="^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([,])*)*$" ></asp:RegularExpressionValidator>--%>
                                            </b></span><br />
                                            <span class="noteText">Puede enviar a direcciones múltiples mediante separar las direcciones con una coma.</span>
                                            </td></tr>
                                        </table>
                                        <table border="0" width="100%" runat="server" id="tblBusinessUserMsg">
                                            <tr>
                                              <td colspan="2" align="left" valign="top" style="font-size:12px; color:#333; font-family:Arial, Helvetica, sans-serif">
                                                Examine cuidadosamente el Acuerdo para Clínica Externa en la Comunidad. Si está de acuerdo con las condiciones del contrato, favor de marcar "Aprobado" adelante y entre su nombre en el campo Firma Electrónica. Si hubiese alguna discrepancia, rechace el Acuerdo y provea las correcciones en el campo Apuntes.
                                              </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px">
                                        <table border="0" cellspacing="0" cellpadding="1" id="tblApproval" runat="server" visible="false">
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:RadioButton ID="rbtnApprove" CssClass="formFields" runat="server" Text="Aprobar" GroupName="agreement" />
                                                    </b>
                                                    <label for="checkbox11">
                                                    </label>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td valign="top" id="tdSign">Firma Electrónica &nbsp;
                                                    <asp:TextBox ID="txtElectronicSign" runat="server" MaxLength="500" CssClass="bestPracticesText" Style="background-color: #FF0" Width="90%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:RadioButton ID="rbtnReject" CssClass="formFields" runat="server" Text="Rechazar" GroupName="agreement" />
                                                    </b>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trNotes" runat="server" visible="false">
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px">
                                        Notes:<br />
                                        <label for="textarea">
                                        </label>
                                        <asp:TextBox ID="txtNotes" class="formFields" runat="server" Columns="75" Rows="5" TextMode="MultiLine" Width="210px"></asp:TextBox>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center"  class="bestPracticesText">
                                    <asp:ImageButton ID="btnSendMail" ImageUrl="images/btn_send_email_SP.png" CausesValidation="true"
                                            runat="server" AlternateText="Enviar Correo Electrónico" onmouseout="javascript:MouseOutImage(this.id,'images/btn_send_email_SP.png');"
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_send_email_SP_lit.png');" 
                                            onclick="btnSendMail_Click" ValidationGroup="WalgreensUser" Visible="false" />
                                     </td>
                                </tr>
                                 <%--<tr>
                                    <td colspan="2" align="center"  class="bestPracticesText">
                                     <asp:ImageButton ID="btnSendLater" ImageUrl="images/btn_save_send_later_SP.png" CausesValidation="true"
                                            runat="server" AlternateText="Guardar y Enviar más Tarde" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_send_later_SP.png');"
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_send_later_SP_lit.png');" onclick="btnSendLater_Click"  ValidationGroup="SendLater" />
                                    </td>
                                </tr>--%>
                                  <tr>
                                   <td colspan="2" align="center"  class="bestPracticesText">
                                       <asp:ImageButton ID="btnCancel" ImageUrl="images/btn_cancel_SP.png" CausesValidation="false"
                                            runat="server" AlternateText="Cancelar" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_SP.png');"
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_SP_lit.png');" onclick="btnCancel_Click" Visible="false" />
                                    </td>
                                  </tr>
                                  <tr  runat="server" id="trNoteText" visible="false">
                                    <td colspan="2" align="left" valign="top"><span class="noteText">* Tenga en cuenta, una vez que el contrato ha sido firmado por el cliente, no puede ser editada. Tendrá que borrar y volver a iniciado. Para borrar un contrato / clínica abrir los detalles Clínica y haga clic en "Eliminar Acuerdo Clínica".</span>
                                    </td>
                                </tr>
                               </table>
                              </td>
                            </tr>
                      </table></td>
                    </tr>                   
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
