﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;

public partial class encryptedLoginLink : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        EncryptedQueryString args1 = new EncryptedQueryString();
        args1["arg1"] = txtUserEmail.Text.Trim();
        args1["arg2"] = ApplicationSettings.walgreensAppPwd();

        lblUserLoginLink.Text = ApplicationSettings.emailPath() + "walgreensLandingPage.aspx?args=" + args1.ToString();
    }
}