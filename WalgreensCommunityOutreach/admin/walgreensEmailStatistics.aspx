﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensEmailStatistics.aspx.cs" Inherits="walgreensEmailStatistics" %>
<%@ Register src="../controls/walgreensHeader.ascx" tagname="walgreensHeader" tagprefix="uc2" %>

<%@ Register Src="~/controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="~/controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>    
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />  
    <link href="../css/gridstyles.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>   
     <script language="javascript" type="text/javascript">
       function sendEmail() {
           var boolValue = confirm('Do you want to send an email?')
           if (boolValue == true)
               return true;
           else
               return false;
       } 
     </script> 

    <style type="text/css">       
        .style1
        {
            height: 350px;
        }
        </style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:Label ID="lblNewWindow" runat="server" style="display:none"></asp:Label>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
      <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td colspan="2" class="pageTitle2" style="border-bottom: 1px solid #999999; padding-top: 0px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top" class="pageTitle2" >Email & Site Status</td>
                                <td align="right" valign="middle" class="formFields"><span class="class2">
                                    <asp:LinkButton runat="server" 
                                        CssClass="class2" ID="saveButton"  
                                        Text="Send Email" OnClientClick="javascript:return sendEmail();" 
                                        onclick="saveButton_Click" ></asp:LinkButton></span> | <span class="class2"><asp:LinkButton runat="server" CssClass="class2" ID="cancelButton" CommandName="doCancel" OnCommand="doCancel" Text="Cancel" CausesValidation="false" /></span></td>
                              </tr>
                              </table></td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center" valign="top" nowrap="nowrap" class="style1">
                          <table width = "100%">
                              <tr>
                                  <td>
                                        <asp:GridView ID="grdEmailStatistics" runat="server" DataKeyNames="" AutoGenerateColumns="false" CellPadding="4" 
                                                    GridLines="None" Width="75%" CssClass="logSubTitles" SelectedRowStyle-BackColor="lightblue" 
                                                    AllowPaging="True" PageSize="15">
                                                      <AlternatingRowStyle BackColor="#f0f0f0" />
                                                <FooterStyle CssClass="footerGrey" />
                                                <PagerStyle ForeColor="#333333"  Font-Names="Arial, Helvetica, Sans-Serif" Font-Size="11px" BackColor="#FFFFFF" />
                                                <HeaderStyle ForeColor ="#FFFFFF" Font-Names="Arial, Helvetica, sans-serif" Font-Size="11px" BackColor="#B2B2B2" />
                                                <RowStyle Font-Names="Arial, Helvetica, sans-serif" Font-Size="10px" BackColor="#FFFFFF"/>    
                                                <Columns>
                                                     <asp:TemplateField HeaderText="Title" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("UserRole") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Users Count" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmailSent" runat="server" Text='<%# Bind("UserCount") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No. Users<br/> Accessed<br/> Website" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAccessedWebsite" runat="server" Text='<%# Bind("accessedWebsite") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="% Accessed" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPercentAccessedWebsite" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "percentAccessed","{0:0.00}") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Email Sent" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmailSent" runat="server" Text='<%# Bind("EmailCount") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="% Email Sent" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPercentEmailSent" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PercentEmailSent","{0:0.00}") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                </Columns>                                       
                                              
                                      </asp:GridView>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                  <br />
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:GridView ID="grdHighLevelReport" runat="server" DataKeyNames="" AutoGenerateColumns="false" CellPadding="4" 
                                                    GridLines="None" Width="75%" CssClass="logSubTitles" SelectedRowStyle-BackColor="lightblue" 
                                                    AllowPaging="True" PageSize="15">
                                                     <AlternatingRowStyle BackColor="#f0f0f0" />
                                                 <FooterStyle CssClass="footerGrey" />
                                                <PagerStyle ForeColor="#333333"  Font-Names="Arial, Helvetica, Sans-Serif" Font-Size="11px" BackColor="#FFFFFF" />
                                                <HeaderStyle ForeColor ="#FFFFFF" Font-Names="Arial, Helvetica, sans-serif" Font-Size="11px" BackColor="#B2B2B2" />
                                                <RowStyle Font-Names="Arial, Helvetica, sans-serif" Font-Size="10px" BackColor="#FFFFFF"/>   
                                                 <Columns>
                                                     <asp:TemplateField HeaderText="Compliant/Non-Compliant" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCompliant" runat="server" Text='<%# Bind("compliant") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Store Count" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTotalCount" runat="server" Text='<%# Bind("totalCount") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="% On Store Count" HeaderStyle-HorizontalAlign="Left" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPercentage" runat="server" Text='<%# Bind("percentage") %>'></asp:Label>
                                                         </ItemTemplate>
                                                        </asp:TemplateField>
                                                </Columns>                                 
                                      </asp:GridView>
                                  </td>
                              </tr>
                          </table>
                      </td>
                    </tr>
                </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
</form>
</body>
</html>