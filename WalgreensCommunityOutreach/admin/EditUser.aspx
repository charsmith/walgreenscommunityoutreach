﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditUser.aspx.cs" Inherits="EditUser" %>

<%@ Register Src="~/controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="~/controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />    
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
     <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css" />
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script> 
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        function nospaces(t) {
            if (t.value.match(/\s/g)) {
                alert('Sorry, you are not allowed to enter any spaces');
                t.value = t.value.replace(/\s/g, '');
            }
        } 
    </script>
    <script type="text/javascript" language="javascript">
    function chkRoles(sender, args) {
        var chkControlId = 'chkRoles'
        var options = document.getElementById(chkControlId).getElementsByTagName('input');
        var ischecked = false;
        args.IsValid = false;

        for (i = 0; i < options.length; i++) {
            var opt = options[i];
            //if(opt.type=="checkbox")
            if (opt.type == "radio") {
                if (opt.checked) {
                    ischecked = true;
                    args.IsValid = true;
                }
            }
        }
    }

    function validateMarket(sender, args) {
        var market = document.getElementById('ddlMarkets');
        if (market != null) {
            if (market.options[market.selectedIndex].value.length == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function validateArea(sender, args) {
        var area = document.getElementById('ddlAreas');
        if (area != null) {
            if (area.options[area.selectedIndex].value.length == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function validateDistrict(sender, args) {
        var district = document.getElementById('ddlDistricts');
        if (district != null) {
            if (district.options[district.selectedIndex].value.length == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function validateStore(sender, args) {
        var store = document.getElementById('ddlStores');
        if (store != null) {
            if (store.options[store.selectedIndex].value.length == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function CustomValidatorSelectedStore(source, arguments) {
        if (document.getElementById('lstSelectedStore').length > 0) {
            arguments.IsValid = true;
        }
        else {
            arguments.IsValid = false;
        }
    }

    function CustomValidatorSelectedDistrict(source, arguments) {

        if (document.getElementById('lstSelectedDistricts').length > 0) {
            arguments.IsValid = true;
        }
        else {
            arguments.IsValid = false;
        }
    }

    function showListBox(boxId) {
        //if(boxId > 0)
        //{
        boxId.style.display = 'block';
        //}
        return false;
    }

    </script> 
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" >
<asp:HiddenField ID="hfUserPk" runat="server" />
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
    <td colspan="2">        
        <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />        
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td  colspan="2" align="left" valign="top">
    <table width="935"  border="0" cellpadding="0" cellspacing="0" >
        <tr>
            <td align="center" style="background-color:#FFFFFF;">
                <table width="900"  border="0" cellpadding="0" cellspacing="0"  style="border:solid #cccccc 1px;">
                          <tr>
      <td align="left" valign="middle" class="innerHeadTitle"><asp:Label runat="server" ID="lblpageHead" Text ="Edit User"></asp:Label> 
      Profile</td>
      <td width="166"  align="right" valign="middle" class="innerHeadNav" ><span class="class23">
        <asp:LinkButton runat="server" Cssclass="class23" ID="saveButton" ToolTip= "Click to save the changes" CommandName="doSave" OnCommand="doSave" Text="Save" />|<asp:LinkButton runat="server" ToolTip="Click to visit previous screen" Cssclass="class23" ID="cancelButton" CommandName="doCancel" OnCommand="doCancel" Text="Cancel" CausesValidation="false" />        
      </span></td>
      </tr>
      <tr>
    <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF" class="profilePagePad">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" 
                                ShowSummary="false" />
      <table width="734" border="0" cellspacing="10">        
        <tr>
          <td width="356" align="left" valign="top" ><table width="463" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="163" align="center" valign="middle" nowrap="nowrap" class="profileTabs">User Information</td>
                <td width="300" class="profileBorderTop"><img src="../images/spacer.gif" width="300" height="1" alt=""/></td>
              </tr>
              <tr>
                <td colspan="2" align="center" class="profileBorder"><table width="98%" border="0" cellpadding="0" cellspacing="8" >
                    <tr  id="rowMessage">
                      <td colspan="2" class="auto-style1"><asp:Label ID="lblRoleMessage" runat="server" Text="" ForeColor="Red" Visible="false" CssClass="formFields"></asp:Label></td>
                    </tr>
                  <tr id="rowMarkets" runat="server" visible="false">
                    <td width="20%" align="right" nowrap="nowrap" class="profileTitles"><asp:Label ID="lblRegionText" runat="server" Text="*Region:"></asp:Label></td>
                    <td width="80%" align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:DropDownList ID="ddlMarkets" runat="server" Visible="false" CssClass="profileFormObject" TabIndex="1" AutoPostBack="true" onselectedindexchanged="ddlMarket_SelectedIndexChanged"></asp:DropDownList>
                            <asp:CustomValidator ClientValidationFunction="validateMarket" ID="ddlMarketCV" runat="server" ErrorMessage="Region is required." Display="None"/>
                        <asp:Label ID="lblRegion" runat="server" Visible="false"></asp:Label> 
                    </span></td>
                  </tr>
                  <tr id="rowAreas" runat="server" visible="false">
                    <td width="20%" align="right" nowrap="nowrap" class="profileTitles"><asp:Label ID="lblAreaText" runat="server" Text="*Area:"></asp:Label></td>
                    <td width="80%" align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:DropDownList ID="ddlAreas" runat="server" Visible="false" CssClass="profileFormObject" TabIndex="1" AutoPostBack="true" onselectedindexchanged="ddlArea_SelectedIndexChanged"></asp:DropDownList>
                            <asp:CustomValidator ClientValidationFunction="validateArea" ID="ddlAreaCV" runat="server" ErrorMessage="Area is required." Display="None"/>
                        <asp:Label ID="lblArea" runat="server" Visible="false"></asp:Label>
                    </span></td>
                  </tr>
                  <tr id="rowDistricts" runat="server" visible="false">
                    <td width="20%" align="right" nowrap="nowrap" class="profileTitles"><asp:Label ID="lblDistrictText" runat="server" Text="*District:"></asp:Label></td>
                    <td width="80%" align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:DropDownList ID="ddlDistricts" runat="server" CssClass="profileFormObject" TabIndex="1" 
                            AutoPostBack="true" Visible="false" onselectedindexchanged="ddlDistricts_SelectedIndexChanged"></asp:DropDownList>
                            <asp:CustomValidator ClientValidationFunction="validateDistrict" ID="ddlDistrictsCV" runat="server" ErrorMessage="District is required." Display="None"/>
                        <asp:Label ID="lblDistrict" runat="server" Visible="false"></asp:Label>
                    </span></td>
                  </tr>
                  <tr id="rowStores" runat="server" visible="false">
                      <td width="20%" align="right" nowrap="nowrap" class="profileTitles"><asp:Label ID="lblStoreText" runat="server" Text="*Store:"></asp:Label></td>
                      <td width="80%" align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:DropDownList ID="ddlStores" runat="server" CssClass="profileFormObject" TabIndex="1" AutoPostBack="true" Visible="false" onselectedindexchanged="ddlStore_SelectedIndexChanged"></asp:DropDownList>
                            <asp:CustomValidator ClientValidationFunction="validateStore" ID="ddlStoresCV" runat="server" ErrorMessage="Store is required." Display="None"/>
                          <asp:Label ID="lblStore" runat="server" Visible="false"></asp:Label>
                    </span></td>
                  </tr>
                  <tr>
                    <td width="20%" align="right" nowrap="nowrap" class="profileTitles">*First Name:</td>
                    <td width="80%" align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:TextBox ID="txtFirstName" runat="server"  CssClass="profileFormObject" maxlength="25" TabIndex="2"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="txtFirstNameReqFV" runat="server" Display="None" ControlToValidate="txtfirstName" ErrorMessage="First name is required"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="txtFirstNameReqEV" ControlToValidate="txtFirstName" Display="None" ValidationExpression="[^,&lt;&gt;]+" runat="server" ErrorMessage="First Name: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </span></td>
                  </tr>
                  <tr>
                    <td align="right" nowrap="nowrap" class="profileTitles">*Last Name:</td>
                    <td align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:TextBox ID="txtLastName" runat="server"  CssClass="profileFormObject" maxlength="35" TabIndex="3"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="txtLastNameReqFV" runat="server" ControlToValidate="txtlastName" Display="None" ErrorMessage="Last name is required"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="txtLastNameReqEV" ControlToValidate="txtLastName" Display="None" ValidationExpression="[^,&lt;&gt;]+" runat="server" ErrorMessage="Last Name: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </span></td>
                  </tr>                  
                <%--  <tr>
                    <td align="right" nowrap="nowrap"  class="profileTitles">Address1:</td>
                    <td align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:TextBox ID="txtAddress1" runat="server"  CssClass="profileFormObject" size="32" 
                                                    maxlength="40" TabIndex="3"></asp:TextBox>
                      <asp:RegularExpressionValidator ID="txtAddress1RegEV" ControlToValidate="txtAddress1" Display="None" ValidationExpression="[^&lt;&gt;]+" runat="server" ErrorMessage="Address1: &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </span></td>
                  </tr>
                  <tr>
                    <td align="right" nowrap="nowrap" class="profileTitles" >Address2:</td>
                    <td align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:TextBox ID="txtAddress2" runat="server"  CssClass="profileFormObject" size="32" 
                                                    maxlength="40" TabIndex="4"></asp:TextBox>
                      <asp:RegularExpressionValidator ID="txtAddress2RegEV" ControlToValidate="txtAddress2" Display="None" ValidationExpression="[^&lt;&gt;]+" runat="server" ErrorMessage="Address2: &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </span></td>
                  </tr>
                  <tr>
                    <td align="right" nowrap="nowrap" class="profileTitles" >City:</td>
                    <td align="left" class="profileTitles">
                        <span class="whiteFormText">
                            <asp:TextBox ID="txtCity" runat="server"  CssClass="profileFormObject" size="32" maxlength="25" TabIndex="5"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="txtCityRegEV" ControlToValidate="txtCity" Display="None" ValidationExpression="[^,&lt;&gt;]+" runat="server" ErrorMessage="City: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>                      
                        </span>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" nowrap="nowrap" class="profileTitles" >State:</td>
                    <td align="left" class="style2"><asp:DropDownList ID="ddlState" runat="server"  CssClass="profileFormObject" TabIndex="6"></asp:DropDownList>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" nowrap="nowrap" class="profileTitles">Zip:</td>
                    <td align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:TextBox ID="txtZip" runat="server"  CssClass="profileFormObject" Width="53" 
                                                            MaxLength="5" TabIndex="7"></asp:TextBox>
                      <asp:CustomValidator ID="txtZipCV" runat="server" ErrorMessage="Valid Zip Code is required (ex: #####)" ControlToValidate="txtZip" Display="None" ClientValidationFunction="validateZipCode" OnServerValidate="validateZipCode"></asp:CustomValidator>
                    </span></td>
                  </tr>  --%>               
                  <tr>
                    <td align="right" valign="middle" nowrap="nowrap" class="profileTitles">Phone:</td>
                    <td align="left" class="style2"><span class="whiteFormText">
                      <asp:TextBox ID="txtPhone" runat="server" CssClass="formFields" Width="112" maxlength="14" TabIndex="8" onblur="textBoxOnBlur(this);"></asp:TextBox>
                      <asp:CustomValidator ID="txtPhoneCV" ControlToValidate="txtPhone" runat="server" SetFocusOnError="true" Display="None" ErrorMessage="Valid Phone number is required(ex: ###-###-####)" ClientValidationFunction="ValidateCompanyPhoneFax" OnServerValidate="ValidateCompanyPhoneFax"></asp:CustomValidator>
                    </span></td>
                    </tr>
                  <%--<tr>
                    <td align="right" valign="middle" nowrap="nowrap" class="profileTitles">Fax:</td>
                    <td align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:TextBox ID="txtFax" runat="server" CssClass="formFields" Width="112" MaxLength="14" TabIndex="9" onblur="textBoxOnBlur(this);"></asp:TextBox>
                      <asp:CustomValidator ID="txtFaxCV" ControlToValidate="txtFax" runat="server" Display="None" ErrorMessage="Valid Fax number is required(ex: ###-###-####)" ClientValidationFunction="ValidateCompanyPhoneFax" OnServerValidate="ValidateCompanyPhoneFax"></asp:CustomValidator>
                    </span></td>
                  </tr>--%>
                  <%--<tr id="tblStoresRow" runat="server" visible="false">
                    <td align="right" valign="middle" nowrap="nowrap" class="profileTitles">
                        Store Id:</td>
                    <td align="left" class="profileTitles">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" rowspan="2" width="109">
                                    <asp:ListBox ID="lstStoreID" runat="server" CssClass="formFields" SelectionMode="Multiple" TabIndex="10" Width="109px"></asp:ListBox>
                                </td>
                                <td align="center" width="98">
                                    <asp:ImageButton ID="btnAddStore" runat="server" AlternateText="Add Store" CausesValidation="false" CssClass="buttonPad" ImageAlign="AbsMiddle" 
                                        ImageUrl="~/images/btn_assign_store.png" onmouseout="javascript:MouseOutImage(this.id,'../images/btn_assign_store.png');" 
                                        onmouseover="javascript:MouseOverImage(this.id,'../images/btn_assign_store_lit.png');" onclick="btnAddStore_Click" TabIndex="13" />
                                </td>
                                <td align="left" rowspan="2" width="109">
                                    <asp:ListBox ID="lstSelectedStore" runat="server" CssClass="formFields" SelectionMode="Multiple" TabIndex="11" Width="109px"></asp:ListBox>
                                    <asp:CustomValidator ID="lstSelectedStoreCV" runat="server" ClientValidationFunction="CustomValidatorSelectedStore" Display="None" ErrorMessage="Store ID is required." />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="98">
                                    <asp:ImageButton ID="btnRemoveStore" runat="server" 
                                        AlternateText="Remove Store" CausesValidation="false" CssClass="buttonPad" ImageAlign="AbsMiddle" 
                                        ImageUrl="~/images/btn_remove_store.png" onmouseout="javascript:MouseOutImage(this.id,'../images/btn_remove_store.png');" 
                                        onmouseover="javascript:MouseOverImage(this.id,'../images/btn_remove_store_lit.png');" onclick="btnRemoveStore_Click" TabIndex="14" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    </tr>--%>
                  <%--<tr id="tblDistrictsRow" runat="server" visible="false">
                    <td align="right" valign="middle" nowrap="nowrap" class="profileTitles">District Id:</td>
                    <td align="left" class="profileTitles">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" width="109" rowspan="2">
                                    <asp:ListBox ID="lstAvailableDistricts" runat="server" CssClass="formFields" SelectionMode="Multiple" TabIndex="10" Width="109px" Height="75px"></asp:ListBox>
                                </td>
                                <td align="center" width="108">
                                    <asp:ImageButton ID="btnAddDistrict" runat="server" AlternateText="Add District" CausesValidation="false" CssClass="buttonPad" ImageAlign="AbsMiddle" 
                                    ImageUrl="~/images/btn_assign_district.png" onmouseout="javascript:MouseOutImage(this.id,'../images/btn_assign_district.png');" 
                                    onmouseover="javascript:MouseOverImage(this.id,'../images/btn_assign_district_lit.png');" onclick="btnAddDistrict_Click" TabIndex="13" />
                                </td>
                                <td align="left" rowspan="2" width="109">
                                    <asp:ListBox ID="lstSelectedDistricts" runat="server" CssClass="formFields" SelectionMode="Multiple" TabIndex="11" Width="109px" Height="75px"></asp:ListBox>
                                    <asp:CustomValidator ID="lstSelectedDistrictCV" runat="server" ClientValidationFunction="CustomValidatorSelectedDistrict" Display="None" ErrorMessage="At least one district is required." />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="108">
                                    <asp:ImageButton ID="btnRemoveDistrict" runat="server" AlternateText="Remove District" CausesValidation="false" CssClass="buttonPad" ImageAlign="AbsMiddle" 
                                    ImageUrl="~/images/btn_remove_district.png" onmouseout="javascript:MouseOutImage(this.id,'../images/btn_remove_district.png');" 
                                    onmouseover="javascript:MouseOverImage(this.id,'../images/btn_remove_district_lit.png');" onclick="btnRemoveDistrict_Click" TabIndex="14" /></td>
                            </tr>
                        </table>
                    </td>
                  </tr>--%>
                  <tr>
                    <td align="right" nowrap="nowrap" class="profileTitles">*Email:</td>
                    <td align="left" class="profileTitles"><span class="whiteFormText">
                      <asp:TextBox ID="txtUsername" runat="server"  CssClass="profileFormObject" maxlength="256" TabIndex="12" Width="220"></asp:TextBox>
                      <asp:RequiredFieldValidator ControlToValidate="txtUsername" ID="txtemailReqFV" runat="server" Display="None" ErrorMessage="Email is required"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ControlToValidate="txtUsername" ID="txtemailRegEV" runat="server" Display="None" ErrorMessage="Invalid email" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                    </span></td>
                  </tr>
                    <tr id="rowstoreIds" runat="server" visible="false">
                       <td width="20%" align="right" nowrap="nowrap" class="profileTitles">District Stores:
                       </td>
                         <td align="left" width="80%">
                             <asp:ListBox ID="lstStoreID" runat="server" CssClass="formFields" SelectionMode="Single" Height="220px" TabIndex="10" Width="250px"></asp:ListBox>
                        </td>
                    </tr>
                </table>
              </tr>
            </table>
              <img src="../images/spacer.gif" height="20" width="1" /></td>
          <td width="359" align="right" valign="top" ><table width="313" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="163" align="center" valign="middle" nowrap="nowrap" class="profileTabs">User Assignments</td>
                <td width="100" align="right" valign="middle" class="profileBorderTop"><img src="../images/spacer.gif" width="150" height="1" /></td>
              </tr>
              <tr>
                <td colspan="2" align="center" class="profileBorder"><table width="250"  border="0" cellpadding="0" cellspacing="8">
                <tr>
                    <td class="profileTitles" align="left">*Assign User Role</td>
                  </tr>
                  <tr>
                    <td align="left" >
                        <table width="275" cellpadding="5" cellspacing="0" bgcolor="#eeeeee" style="width: 275px;border: 1px solid #CCCCCC;">
                            <tr>
                              <td align="left" valign="top" >
                                <asp:RadioButtonList BorderStyle="Solid" BorderColor="#666666" BorderWidth="0" ID="chkRoles" runat="server" RepeatColumns="1" Width="195" RepeatDirection="Horizontal" 
                                CssClass="profileTitles" TabIndex="15" AutoPostBack="true" OnSelectedIndexChanged="chkRoles_Click" ></asp:RadioButtonList>
                                <asp:CustomValidator ClientValidationFunction="chkRoles" ID="chkRolesCV" runat="server" ErrorMessage="User role is required." Display="None"/>
                              </td>
                            </tr>
                        </table>
                    </td>
                  </tr>
<%--                  <tr runat="server" id="trOutreachEfforts" visible="false">
                     <td>
                         <table border="1">
                          <tr>
                            <td class="profileTitles" align="left"><img src="../images/spacer.gif"  width="1" height="10" alt="spacer" /><br />Assign Outreach Effort(s) </td>
                          </tr>
                          <tr>
                            <td align="left"><table width="275" cellpadding="5" cellspacing="0" bgcolor="#eeeeee" 
                                                            style="width: 275px;border: 1px solid #CCCCCC;">
                                <tr>
                                  <td align="left" valign="top" >
                                  <asp:CheckBoxList ID="chkLstOutreachEfforts" CssClass="profileTitles" runat="server" TabIndex="16" RepeatDirection="Vertical" ></asp:CheckBoxList>
                                  <asp:CustomValidator ClientValidationFunction="validateOutreachEfforts" ID="chkLstOutreachEffortsCV" runat="server" ErrorMessage="Outreach Effort(s) is required." Display="None"/>
                              </td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>
                     </td>
                </tr>--%> 
                </table></td>
              </tr>
            </table>
              <img src="../images/spacer.gif" height="20" width="1" alt="spacer" /><img src="../images/spacer.gif" height="20" width="1" alt="spacer" /></td>
        </tr>
      </table></td>
      </tr>
      <tr>
      	<td colspan="2" class="footerGrey">
            <img src="../images/spacer.gif"  width="1" height="10" alt="spacer" />
        </td>
      </tr>
                </table>
            </td>
        </tr>
    </table>
    </td>
    </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
