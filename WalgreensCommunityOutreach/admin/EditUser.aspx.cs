﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using TdApplicationLib;
using tdEmailLib;
using TdWalgreens;

public partial class EditUser : Page
{
    #region ---------- PROTECTED EVENTS ----------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.bindUserRoles();

            if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "editText"))
            {
                this.lblpageHead.Text = "Edit User";

                this.txtUsername.Enabled = false;
                this.doEditExistingUser();
            }
            else if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "addText"))
            {
                this.lblpageHead.Text = (string)GetGlobalResourceObject("errorMessages", "addUser");
            }
        }
        this.txtFirstName.Focus();
    }

    protected void chkRoles_Click(object sender, EventArgs e)
    {
        this.setControlsBasedOnUserRole();
        this.txtUsername.Text = string.Empty;
        this.txtUsername.Enabled = true;
        switch (this.chkRoles.SelectedValue.Trim().ToLower())
        {
            case "district manager":
                this.ddlDistricts.ClearSelection();
                this.txtUsername.Enabled = false;
                this.bindDropdownLists();
                break;
            case "regional vice president":
            case "regional healthcare director":
                this.ddlMarkets.ClearSelection();
                this.bindDropdownLists();
                break;
            case "healthcare supervisor":
            case "director – rx & retail ops":
                this.ddlAreas.ClearSelection();
                this.bindDropdownLists();
                break;
            case "store manager":
            case "pharmacy manager":
                this.ddlStores.ClearSelection();
                this.txtUsername.Enabled = false;
                this.bindDropdownLists();
                break;
        }
    }

    protected void ddlMarket_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.setControlsBasedOnUserRole();
        this.rowMarkets.Visible = true;
        if (!string.IsNullOrEmpty(this.ddlMarkets.SelectedValue))
        {
            this.lblRoleMessage.Visible = true;
            this.lblRoleMessage.Text = String.Format((string)GetGlobalResourceObject("errorMessages", "allMarketStoresAssignedToUser"), this.chkRoles.SelectedValue, this.ddlMarkets.SelectedItem.Text);
        }
        else
        {
            this.lblRoleMessage.Visible = false;
            this.lblRoleMessage.Text = "";
        }
    }

    protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.setControlsBasedOnUserRole();
        this.rowAreas.Visible = true;
        if (!string.IsNullOrEmpty(this.ddlAreas.SelectedValue))
        {
            this.lblRoleMessage.Visible = true;
            this.lblRoleMessage.Text = String.Format((string)GetGlobalResourceObject("errorMessages", "allAreaStoresAssignedToUser"), this.chkRoles.SelectedValue, this.ddlAreas.SelectedItem.Text);
        }
        else
        {
            this.lblRoleMessage.Visible = false;
            this.lblRoleMessage.Text = "";
        }
    }

    protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.createDefaultUserEmail();
    }

    protected void ddlDistricts_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.setControlsBasedOnUserRole();
        if (!string.IsNullOrEmpty(this.ddlDistricts.SelectedValue) && this.chkRoles.SelectedValue.Trim().ToLower() == "district manager")
        {
            this.rowDistricts.Visible = true;
            this.lblRoleMessage.Visible = true;
            this.lblRoleMessage.Text = String.Format((string)GetGlobalResourceObject("errorMessages", "allStoresAssignedToUser"), this.chkRoles.SelectedValue, this.ddlDistricts.SelectedItem.Text);
            this.createDefaultUserEmail();
        }
        else
        {
            this.lblRoleMessage.Visible = false;
            this.lblRoleMessage.Text = "";
        }
    }

    protected void doSave(object sender, CommandEventArgs e)
    {
        int return_value_from_sp;
        MembershipCreateStatus result;

        if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "addText"))
        {
            #region ---------- ADDING USER-----------
            string password_question = null;
            string password_answer = null;

            Membership.CreateUser(txtUsername.Text.ToString().Trim(), ApplicationSettings.walgreensAppPwd(), txtUsername.Text.ToString().Trim(), password_question, password_answer, true, out result);

            switch (result)
            {
                case MembershipCreateStatus.Success:
                    this.isDuplicate = false;
                    return_value_from_sp = this.dbOperation.addUpdateMembershipUser(this.setRegisMembershipUser(), (string)GetGlobalResourceObject("errorMessages", "addText"));
                    if (return_value_from_sp != -1)
                    {
                        this.sendUserAccessLinkEmail(this.txtUsername.Text.ToString().Trim(), this.chkRoles.SelectedValue.Trim());
                        this.setControlsBasedOnUserRole();
                        this.cancelButton.Text = " Return";
                        this.doClearFields();
                        this.rowDistricts.Visible = false;
                        this.lblRoleMessage.Visible = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userCreated") + "');", true);
                    }
                    break;
                case MembershipCreateStatus.DuplicateUserName:
                    {
                        this.isDuplicate = true;
                        return_value_from_sp = this.dbOperation.addUpdateMembershipUser(this.setRegisMembershipUser(), (string)GetGlobalResourceObject("errorMessages", "addText"));
                        if (return_value_from_sp == 0)
                        {
                            this.sendUserAccessLinkEmail(this.txtUsername.Text.ToString().Trim(), this.chkRoles.SelectedValue.Trim());
                            this.cancelButton.Text = " Return";
                            this.chkRoles.ClearSelection();
                            this.setControlsBasedOnUserRole();
                            this.doClearFields();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userCreated") + "');", true);
                        }
                        else
                        {
                            if (return_value_from_sp == -2)
                            {
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userAlreadyExists") + "');", true);
                                txtUsername.Focus();
                            }
                            else
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Problem while saving user. Please contact administrator');", true);
                        }
                    }
                    break;
            }
            #endregion
        }
        else
        {
            #region ---------- EDITING EXISTING USER-----------
            //bool is_user_updated = false;
            //selected_user = Membership.GetUser(this.txtUsername.Text.ToString().Trim(), false);
            //selected_user.Email = this.txtUsername.Text.ToString().Trim();
            //Membership.UpdateUser(selected_user);
            //is_user_updated = true;

            //if (is_user_updated)
            //{
            this.isDuplicate = true;
            return_value_from_sp = this.dbOperation.addUpdateMembershipUser(this.setRegisMembershipUser(), (string)GetGlobalResourceObject("errorMessages", "editText"));

            if (return_value_from_sp != -1)
            {
                this.sendUserAccessLinkEmail(this.txtUsername.Text.ToString().Trim(), this.chkRoles.SelectedValue.Trim());

                this.setControlsBasedOnUserRole();
                this.cancelButton.Text = " Return";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "dataUpdated") + "');", true);
            }
            //}
            //else
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userUpdationError") + "');", true);
            #endregion
        }
    }

    protected void doRevert(object sender, CommandEventArgs e)
    {
        Response.Redirect("UserEditor.aspx", false);
    }

    protected void doCancel(object sender, CommandEventArgs e)
    {
        Response.Redirect("UserEditor.aspx", false);
    }

    protected void ValidateCompanyPhoneFax(object sender, ServerValidateEventArgs e)
    {
        string value = e.Value.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();

        string regex_pattern_1 = @"^[0-9]+$";

        if (!Regex.IsMatch(value, regex_pattern_1)) return;
        if (string.IsNullOrEmpty(value)) return;
        e.IsValid = Convert.ToInt64(value) > 0 && value.Length == 10;
    }

    protected void validateZipCode(object sender, ServerValidateEventArgs e)
    {
        string value = e.Value;

        string regex_pattern_1 = @"^[0-9]+$";
        if (!Regex.IsMatch(value, regex_pattern_1)) return;
        if (string.IsNullOrEmpty(value)) return;
        e.IsValid = (Convert.ToInt32(value) > 0 && value.Length == 5);
    }
    #endregion

    #region ---------- PRIVATE METHODS -----------
    /// <summary>
    /// Binds user roles to user roles list
    /// </summary>
    private void bindUserRoles()
    {
        DataTable dt_user_roles = this.dbOperation.getUserRoles;
        if (dt_user_roles.Rows.Count > 0)
        {
            this.chkRoles.DataSource = dt_user_roles;
            this.chkRoles.DataTextField = "RoleName";
            this.chkRoles.DataValueField = "RoleName";
            this.chkRoles.DataBind();
        }

        if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "editText"))
        {
            foreach (ListItem item in this.chkRoles.Items)
                item.Enabled = false;
        }
    }

    /// <summary>
    /// Displays edit user information
    /// </summary>
    private void doEditExistingUser()
    {
        DataTable dt_user_profile = this.dbOperation.getUserprofileInfo(this.commonAppSession.SelectedUser);

        if (dt_user_profile != null)
        {
            if (dt_user_profile.Rows.Count > 0)
            {
                this.hfUserPk.Value = dt_user_profile.Rows[0]["UserPk"].ToString();
                this.txtFirstName.Text = dt_user_profile.Rows[0]["firstName"].ToString().Trim();
                this.txtLastName.Text = dt_user_profile.Rows[0]["lastName"].ToString().Trim();
                this.txtUsername.Text = dt_user_profile.Rows[0]["UserName"].ToString().Trim();
                this.txtPhone.Text = dt_user_profile.Rows[0]["phone"].ToString().Trim();
                this.makeSelectedUserRoles(dt_user_profile.Rows[0]["RoleName"].ToString());
                if (dt_user_profile.Rows[0]["RoleName"].ToString().ToLower() != "admin")
                    this.bindDropdownLists();

                this.lblDistrict.Text = dt_user_profile.Rows[0]["districtNumber"].ToString().ToString();
                this.lblArea.Text = dt_user_profile.Rows[0]["areaNumber"].ToString().ToString();
                this.lblRegion.Text = dt_user_profile.Rows[0]["regionNumber"].ToString().ToString();
                this.lblStore.Text = dt_user_profile.Rows[0]["storeId"].ToString().ToString();

                this.setControlsBasedOnUserRole();

                if (this.chkRoles.SelectedValue.Trim().ToLower() == "district manager")
                    this.bindDistrictUserStores(Convert.ToInt32(dt_user_profile.Rows[0]["districtNumber"].ToString()), Convert.ToInt32(dt_user_profile.Rows[0]["UserPk"].ToString()));
            }
        }
    }

    /// <summary>
    /// Creates default username for the selected user role
    /// </summary>
    private void createDefaultUserEmail()
    {
        if (!string.IsNullOrEmpty(this.chkRoles.SelectedValue))
        {
            if (this.chkRoles.SelectedValue.ToLower() == "store manager" && !string.IsNullOrEmpty(this.ddlStores.SelectedValue))
                this.txtUsername.Text = "mgr." + ddlStores.SelectedValue.PadLeft(5, '0') + "@store.walgreens.com";
            else if (this.chkRoles.SelectedValue.ToLower() == "pharmacy manager" && !string.IsNullOrEmpty(this.ddlStores.SelectedValue))
                this.txtUsername.Text = "rxm." + ddlStores.SelectedValue.PadLeft(5, '0') + "@store.walgreens.com";
            else if (this.chkRoles.SelectedValue.ToLower() == "district manager" && !string.IsNullOrEmpty(this.ddlDistricts.SelectedValue))
                this.txtUsername.Text = "d" + this.ddlDistricts.SelectedValue.PadLeft(3, '0') + ".dm@walgreens.com";
        }
    }

    /// <summary>
    /// Display store selection controls bases on selected user role
    /// </summary>
    private void setControlsBasedOnUserRole()
    {
        bool is_edit_mode = (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "editText")) ? true : false;
        this.lblRoleMessage.Visible = false;
        this.lblRoleMessage.Text = "";
        this.rowDistricts.Visible = false;
        this.rowMarkets.Visible = false;
        this.rowAreas.Visible = false;
        this.rowStores.Visible = false;
        this.lblDistrict.Visible = false;
        this.lblRegion.Visible = false;
        this.lblArea.Visible = false;
        this.lblStore.Visible = false;

        switch (this.chkRoles.SelectedValue.Trim().ToLower())
        {
            case "admin":
                this.lblRoleMessage.Visible = true;
                this.lblRoleMessage.Text = (string)GetGlobalResourceObject("errorMessages", "allStoresAssigned");
                break;
            case "district manager":
                this.lblDistrictText.Text = is_edit_mode ? this.lblDistrictText.Text.Replace('*', ' ') : this.lblDistrictText.Text;
                this.rowDistricts.Visible = true;
                this.lblDistrict.Visible = is_edit_mode;
                this.ddlDistricts.Visible = !is_edit_mode;
                break;
            case "regional vice president":
            case "regional healthcare director":
                this.lblRegionText.Text = is_edit_mode ? this.lblRegionText.Text.Replace('*', ' ') : this.lblRegionText.Text;
                this.rowMarkets.Visible = true;
                this.lblRegion.Visible = is_edit_mode;
                this.ddlMarkets.Visible = !is_edit_mode;
                break;
            case "healthcare supervisor":
            case "director – rx & retail ops":
                this.lblAreaText.Text = is_edit_mode ? this.lblAreaText.Text.Replace('*', ' ') : this.lblAreaText.Text;
                this.rowAreas.Visible = true;
                this.lblArea.Visible = is_edit_mode;
                this.ddlAreas.Visible = !is_edit_mode;
                break;
            case "store manager":
            case "pharmacy manager":
                this.lblStoreText.Text = is_edit_mode ? this.lblStoreText.Text.Replace('*', ' ') : this.lblStoreText.Text;
                this.rowStores.Visible = true;
                this.rowDistricts.Visible = false;
                this.lblStore.Visible = is_edit_mode;
                this.ddlStores.Visible = !is_edit_mode;
                break;
        }
    }

    /// <summary>
    /// Binds Region, Area and districts to dropdown lists
    /// </summary>
    private void bindDropdownLists()
    {
        string location_type = string.Empty;
        DataTable dt_available_locations = new DataTable();
        dt_available_locations = this.dbOperation.getAvailableLocations(this.chkRoles.SelectedValue.Trim(), (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "addText")) ? "Add" : "Edit");

        switch (this.chkRoles.SelectedValue.Trim().ToLower())
        {
            case "district manager":
                this.ddlDistricts.DataSource = dt_available_locations;
                this.ddlDistricts.DataTextField = "districtName";
                this.ddlDistricts.DataValueField = "districtNumber";
                this.ddlDistricts.DataBind();
                this.ddlDistricts.Items.Insert(0, new ListItem("--Select District--", ""));
                location_type = "district";

                break;
            case "regional vice president":
            case "regional healthcare director":
                this.ddlMarkets.DataSource = dt_available_locations;
                this.ddlMarkets.DataTextField = "marketName";
                this.ddlMarkets.DataValueField = "marketNumber";
                this.ddlMarkets.DataBind();
                this.ddlMarkets.Items.Insert(0, new ListItem("--Select Region--", ""));
                location_type = "region";

                break;
            case "healthcare supervisor":
            case "director – rx & retail ops":
                this.ddlAreas.DataSource = dt_available_locations;
                this.ddlAreas.DataTextField = "areaName";
                this.ddlAreas.DataValueField = "areaNumber";
                this.ddlAreas.DataBind();
                this.ddlAreas.Items.Insert(0, new ListItem("--Select Area--", ""));
                location_type = "area";

                break;
            case "store manager":
            case "pharmacy manager":
                this.ddlStores.DataSource = dt_available_locations;
                this.ddlStores.DataTextField = "storeId";
                this.ddlStores.DataValueField = "storeId";
                this.ddlStores.DataBind();
                this.ddlStores.Items.Insert(0, new ListItem("--Select Store--", ""));
                location_type = "store";
                break;
        }

        if (dt_available_locations.Rows.Count == 0)
        {
            this.lblRoleMessage.Visible = true;
            this.lblRoleMessage.Text = "No " + location_type + " available to create a " + this.chkRoles.SelectedValue;
        }
    }

    /// <summary>
    /// bind district user stores along with district stores which are not assigned to user 
    /// </summary>
    /// <param name="district_number"></param>
    /// <param name="user_pk"></param>
    private void bindDistrictUserStores(int district_number, int user_pk)
    {
        rowstoreIds.Visible = true;
        lstStoreID.DataSource = dbOperation.getDistrictUserStores(district_number, user_pk);
        lstStoreID.DataValueField = "SPStoreId";
        lstStoreID.DataTextField = "SPStoreId";
        lstStoreID.DataBind();
    }

    /// <summary>
    /// Sends portal access login link
    /// </summary>
    /// <param name="user_name"></param>
    /// <param name="user_type"></param>
    private void sendUserAccessLinkEmail(string user_name, string user_type)
    {
        string subject_line = string.Empty, mail_link = string.Empty, email_faq_link = string.Empty, body_file_name = string.Empty;

        //Send email to the configured recipient in web.config if not null 
        mail_link = ApplicationSettings.encryptedLink(user_name, "walgreensLandingPage.aspx");

        //Email templates are common for both programs (IP and SO)
        if (this.walgreensEmail != null)
        {
            email_faq_link = ApplicationSettings.encryptedLinkWithThreeArgs(user_name, "walgreensLandingPage.aspx", "walgreensFAQsIP.aspx");
            subject_line = ((string)GetGlobalResourceObject("errorMessages", "userAccessLinkEmailSubjectLineIP"));

            if (user_type.ToLower() == "regional healthcare director" || user_type.ToLower() == "regional vice president")
                body_file_name = Server.MapPath("~/emailTemplates/statusEmailTemplate_IP_region.htm");
            else if (user_type.ToLower() == "healthcare supervisor" || user_type.ToLower() == "director – rx & retail ops")
                body_file_name = Server.MapPath("~/emailTemplates/statusEmailTemplate_IP_area.htm");
            else if (user_type.ToLower() == "district manager")
                body_file_name = Server.MapPath("~/emailTemplates/statusEmailTemplate_IP_district.htm");
            else
                body_file_name = Server.MapPath("~/emailTemplates/statusEmailTemplate_IP_store.htm");

            if (!string.IsNullOrEmpty(body_file_name))
            {
                this.walgreensEmail.sendListPendingMail(user_type, mail_link, body_file_name, subject_line, mail_link, email_faq_link, (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString())) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : user_name, true, "");
            }
        }
    }

    /// <summary>
    /// Get selected user role.
    /// </summary>
    private void makeSelectedUserRoles(string user_role)
    {
        for (int counter = 0; counter < chkRoles.Items.Count; counter++)
        {
            if (user_role == chkRoles.Items[counter].Text)
                chkRoles.Items[counter].Selected = true;
        }
    }

    /// <summary>
    /// Clear all the date existing in the controls.
    /// </summary>
    private void doClearFields()
    {
        foreach (Control ctrl in from Control c in Page.Controls from Control ctrl in c.Controls select ctrl)
            if (ctrl is TextBox)
                ((TextBox)ctrl).Text = "";

            else if (ctrl is DropDownList)
                ((DropDownList)ctrl).SelectedIndex = 0;

            else if (ctrl is CheckBox)
                ((CheckBox)ctrl).Checked = false;

            else if (ctrl is CheckBoxList)
                ((CheckBoxList)ctrl).ClearSelection();
            else if (ctrl is RadioButtonList)
                ((RadioButtonList)ctrl).ClearSelection();

        // this.lstSelectedStore.Items.Clear();
        rowAreas.Visible = false;
        rowDistricts.Visible = false;
        rowMarkets.Visible = false;
        rowStores.Visible = false;
        ddlStores.ClearSelection();
        ddlDistricts.ClearSelection();
        ddlAreas.ClearSelection();
        ddlMarkets.ClearSelection();

    }

    /// <summary>
    /// This function returns the xml structure to DB for creating/updating the user profile
    /// </summary>
    /// <returns></returns>
    private XmlDocument setRegisMembershipUser()
    {
        XmlDocument profile_doc = new XmlDocument();
        XmlElement store_profile_element = store_profile_element = profile_doc.CreateElement("storeprofile");
        XmlElement fields_element = profile_doc.CreateElement("fields");

        this.userProfileElement("pk", ((this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "addText")) ? string.Empty : this.hfUserPk.Value), ((this.isDuplicate) ? true : false), fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("userID", string.Empty, ((this.isDuplicate) ? false : true), fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("active", "1", true, fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("isDeleted", "0", true, fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("updatedBy", this.commonAppSession.LoginUserInfoSession.UserName.ToString().Replace("'", "''"), true, fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("updatedOn", DateTime.Now.ToString(), true, fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("firstName", this.txtFirstName.Text.Trim().Replace("'", "''"), true, fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("lastName", this.txtLastName.Text.Trim().Replace("'", "''"), true, fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("phone", this.txtPhone.Text.Trim(), true, fields_element, profile_doc, out fields_element, out profile_doc);
        this.userProfileElement("email", this.txtUsername.Text.Trim().Replace("'", "''"), true, fields_element, profile_doc, out fields_element, out profile_doc);
        if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "addText"))
            this.userProfileElement("createdBy", this.txtUsername.Text.Trim().Replace("'", "''"), true, fields_element, profile_doc, out fields_element, out profile_doc);

        if (string.IsNullOrEmpty(this.commonAppSession.SelectedUser))
            this.userProfileElement("isFirstTimeLogin", "1", true, fields_element, profile_doc, out fields_element, out profile_doc);

        switch (this.chkRoles.SelectedValue.ToLower())
        {
            case "store manager":
            case "pharmacy manager":
                //this.userProfileElement("storeList", this.lblpageHead.Text == "Edit User" ? lblStore.Text : this.ddlStores.SelectedValue, false, fields_element, profile_doc, out fields_element, out profile_doc);
                this.userProfileElement("assignedLocation", this.lblpageHead.Text == "Edit User" ? this.lblStore.Text : this.ddlStores.SelectedValue, true, fields_element, profile_doc, out fields_element, out profile_doc);
                break;
            case "district manager":
                this.userProfileElement("assignedLocation", this.lblpageHead.Text == "Edit User" ? this.lblDistrict.Text : this.ddlDistricts.SelectedValue, true, fields_element, profile_doc, out fields_element, out profile_doc);
                break;
            case "healthcare supervisor":
            case "director – rx & retail ops":
                this.userProfileElement("assignedLocation", this.lblpageHead.Text == "Edit User" ? this.lblArea.Text : this.ddlAreas.SelectedValue, true, fields_element, profile_doc, out fields_element, out profile_doc);
                break;
            case "regional vice president":
            case "regional healthcare director":
                this.userProfileElement("assignedLocation", this.lblpageHead.Text == "Edit User" ? this.lblRegion.Text : this.ddlMarkets.SelectedValue, true, fields_element, profile_doc, out fields_element, out profile_doc);
                break;
            default:
                this.userProfileElement("assignedLocation", "", true, fields_element, profile_doc, out fields_element, out profile_doc);
                break;
        }

        this.userProfileElement("roleName", this.chkRoles.SelectedValue, false, fields_element, profile_doc, out fields_element, out profile_doc);
        //if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "addText"))
        this.userProfileElement("outreachXml", this.createUserOutreachEfforts(), true, fields_element, profile_doc, out fields_element, out profile_doc);
        //else
        //    this.userProfileElement("outreachXml", string.Empty, false, fields_element, profile_doc, out fields_element, out profile_doc);

        store_profile_element.AppendChild(fields_element);
        profile_doc.AppendChild(store_profile_element);

        return profile_doc;
    }

    /// <summary>
    /// Creates the User information field element for profile XML document, to insert/update the tblUserProfiles table
    /// </summary>
    /// <param name="field_name"></param>
    /// <param name="field_value"></param>
    /// <param name="is_table_column"></param>
    /// <param name="fields_element"></param>
    /// <param name="profile_doc"></param>
    protected void userProfileElement(string field_name, string field_value, bool is_table_column, XmlElement fields_element, IXPathNavigable profile_doc, out XmlElement fields_element_create, out XmlDocument profile_doc_create)
    {
        XmlElement create_user;
        create_user = ((XmlDocument)profile_doc).CreateElement("field");
        create_user.SetAttribute("name", field_name);
        create_user.SetAttribute("value", field_value);
        if (!is_table_column)
            create_user.SetAttribute("isTableColumn", "0");
        fields_element.AppendChild(create_user);

        fields_element_create = fields_element;
        profile_doc_create = (XmlDocument)profile_doc;

    }

    /// <summary>
    /// creates the user approval XML
    /// </summary>
    /// <returns></returns>
    protected string createUserOutreachEfforts()
    {
        XmlDocument x_doc = new XmlDocument();
        XmlElement approvals = x_doc.CreateElement("outreachEfforts");

        DataTable dt_outreach_efforts = this.dbOperation.getOutreachEffortsList;

        foreach (DataRow row in dt_outreach_efforts.Rows)
        {
            XmlElement approval = x_doc.CreateElement("outreachEffort");
            approval.SetAttribute("id", row["fieldValue"].ToString());
            approval.SetAttribute("type", row["outreachEffort"].ToString());
            approval.SetAttribute("outreachShortCode", row["outreachEffortCode"].ToString());
            approvals.AppendChild(approval);
        }

        if (!approvals.HasChildNodes)
            return string.Empty;
        else
        {
            x_doc.AppendChild(approvals);
            return x_doc.InnerXml;
        }
    }

    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
            this.bindUserRoles();
            if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "editText"))
            {
                this.lblpageHead.Text = "Edit User";

                this.txtUsername.Enabled = false;
                this.doEditExistingUser();
            }
            else if (this.commonAppSession.CommandName == (string)GetGlobalResourceObject("errorMessages", "addText"))
            {
                this.lblpageHead.Text = (string)GetGlobalResourceObject("errorMessages", "addUser");
            }
        this.txtFirstName.Focus();
    }

    #endregion

    #region ---------- PRIVATE VARIABLES ---------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private WalgreenEmail walgreensEmail = null;
    private bool isDuplicate;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion
}