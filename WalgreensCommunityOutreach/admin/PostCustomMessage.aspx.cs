﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using TdApplicationLib;
using TdWalgreens;

public partial class PostCustomMessage : Page
{
    #region ---------- PROTECTED EVENTS ----------
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblMessage.Text = "";
        if (!IsPostBack || this.hfCustomValue.Value.Trim() != "btnSave")
        {
            this.bindCustomMessage();
        }
        else
        {
            if (string.IsNullOrEmpty(Server.UrlDecode(this.hfCustom.Value)))
                this.bindCustomMessage();
        }
        if (!string.IsNullOrEmpty(this.txtFromDate.Text))
        {
            if ((Convert.ToDateTime(this.txtFromDate.Text) <= Convert.ToDateTime(this.outreachStartDate)))
            {
                this.outreachStartDate = Convert.ToDateTime(this.txtFromDate.Text).ToString();
            }
        }
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        int return_value = 0;
        if (!string.IsNullOrEmpty(this.hfCustom.Value))
        {
            string decodeString = Server.UrlDecode(this.hfCustom.Value);
            if (this.chkSchedule.Checked)
            {
                selectedFromDate = Convert.ToDateTime(this.txtFromDate.Text.ToString());
                selectedToDate = Convert.ToDateTime(this.txtToDate.Text.ToString());
            }
            return_value = this.dbOperation.postCustomMessage(decodeString, selectedFromDate, selectedToDate, (this.chkMessageOn.Checked ? true : false), (this.chkSchedule.Checked ? true : false), this.commonAppSession.LoginUserInfoSession.UserID);
            if (return_value > 0)
            {
                this.lblMessage.Text = (string)GetGlobalResourceObject("errorMessages", "customeMessageInserted");
                this.hfCustomValue.Value = "";
            }
            else
            {
                this.lblMessage.Text = (string)GetGlobalResourceObject("errorMessages", "customeMessageFailed");
            }
        }
        this.bindCustomMessage();
    }
    #endregion

    #region ---------- PRIVATE METHODS -----------
    /// <summary>
    /// Get Custom Message
    /// </summary>
    private void bindCustomMessage()
    {
        DataTable dt_custom_msg = this.dbOperation.getCustomMessages();
        if (dt_custom_msg != null && dt_custom_msg.Rows.Count > 0)
        {
            this.hfCustom.Value = Server.HtmlDecode(dt_custom_msg.Rows[0]["customMessage"].ToString());
            if (Convert.ToBoolean(dt_custom_msg.Rows[0]["messageOn"]) == true)
                this.chkMessageOn.Checked = true;
            else
                this.chkMessageOff.Checked = true;

            this.chkSchedule.Checked = Convert.ToBoolean(dt_custom_msg.Rows[0]["isScheduled"]);
            if (this.chkSchedule.Checked)
            {
                this.txtFromDate.Text = Convert.ToDateTime(dt_custom_msg.Rows[0]["startDate"]).ToString();
                this.txtToDate.Text = Convert.ToDateTime(dt_custom_msg.Rows[0]["endDate"]).ToString();
                this.chkMessageOn.Checked = false;
                this.chkMessageOff.Checked = false;
            }
            else
            {
                this.txtFromDate.Text = "";
                this.txtToDate.Text = "";
                this.txtFromDate.Enabled = false;
                this.txtToDate.Enabled = false;
            }
        }
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private DateTime? selectedFromDate = (DateTime?)null;
    private DateTime? selectedToDate = (DateTime?)null;
    public string outreachStartDate = string.Empty;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.outreachStartDate = DateTime.Now.ToString("MM/dd/yyyy");
    }
    #endregion
}