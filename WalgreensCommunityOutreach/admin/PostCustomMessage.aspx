﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PostCustomMessage.aspx.cs" Inherits="PostCustomMessage" %>

<%@ Register Src="~/controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="~/controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="../css/jquery-te-1.4.0.css" rel="stylesheet" />
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/calendarStyle.css" type="text/css" rel="stylesheet" />
    <link href="../css/theme.css" type="text/css" rel="stylesheet" />
    <link href="../css/gridstyles.css" rel="stylesheet" type="text/css" />
    <link href="../themes/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-te-1.4.0.css" rel="stylesheet" type="text/css" />

    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-te-1.4.0.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>

    <style type="text/css">
        #chkMessageOn {
            display: inline;
        }

        #chkMessageOff {
            display: inline;
        }

        .auto-style1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: bold;
            color: #333;
            width: 348px;
        }

        tr.spaceUnder > td {
            padding-bottom: 1em;
        }

        td {
            padding-bottom: .1em;
        }

        .customSaveButton {
            background-color: #82c949;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: arial;
            font-size: 16px;
            font-weight: bold;
            padding: 12px 24px;
            text-decoration: none;
            position: relative;
            behavior: url(PIE.htc);
            -moz-box-shadow: 0px 1px 1px rgba(000,000,000,0.1), inset 0px 1px 1px rgba(255,255,255,0.7);
            -webkit-box-shadow: 0px 1px 1px rgba(000,000,000,0.1), inset 0px 1px 1px rgba(255,255,255,0.7);
        }

            .customSaveButton:hover {
                position: relative;
                background-color: #87d646;
            }

            .customSaveButton:active {
                position: relative;
                top: 1px;
            }
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {

            $('#chkMessageOn').change(function () {
                if (this.checked) {
                    $('#chkMessageOff').prop('checked', false);
                    $('#chkSchedule').prop('checked', false);
                    $("#txtFromDate").datepicker('disable');
                    $("#txtToDate").datepicker('disable');
                    $("#lblMessage").text('');
                }
            });
            $('#chkMessageOff').change(function () {
                if (this.checked) {
                    $('#chkMessageOn').prop('checked', false);
                    $('#chkSchedule').prop('checked', false);
                    $("#txtFromDate").datepicker('disable');
                    $("#txtToDate").datepicker('disable');
                    $("#lblMessage").text('');
                }
            });

            $('#chkSchedule').change(function () {
                if (this.checked) {
                    $('#chkMessageOn').prop('checked', false);
                    $('#chkMessageOff').prop('checked', false);
                    $("#txtFromDate").datepicker('enable');
                    $("#txtToDate").datepicker('enable');
                    $("#lblMessage").text('');
                }
            });

            $("#txtFromDate").attr("readonly", "readonly");
            $("#txtToDate").attr("readonly", "readonly");

            createDateCalendar();

            if ($("#chkSchedule").prop('checked') == true) {
                $('#chkMessageOn').prop('checked', false);
                $('#chkMessageOff').prop('checked', false);
                $("#txtFromDate").datepicker('enable');
                $("#txtToDate").datepicker('enable');
            }

            setCustomText();
            $(".jqte_editor").change(function () {
                updateHiddenField();
            });
        });
        function setCustomText() {
            var input = $("#hfCustom").val();
            $(".jqte_editor").html(input);
            $("#hfCustom").val('');
        }
        function updateHiddenField() {
            var currentText = $(".jqte_editor").html();
            currentText = encodeURIComponent(currentText); // Escapes the HTML including quotations, etc
            $("#hfCustom").val(currentText); // Set the hidden field
        }
        var minOutreachDate = new Date("<%=outreachStartDate %>");
        var maxOutreachDate = new Date();
        function createDateCalendar() {

            $("#txtFromDate").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy'
            });
            $("#txtFromDate").datepicker("option", "minDate", minOutreachDate);

            $("#txtToDate").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy'
            });
            $("#txtToDate").datepicker("option", "minDate", minOutreachDate);

            $(".ui-datepicker-trigger").css("margin-bottom", "-6px");
            $(".ui-datepicker-trigger").css("padding-left", "5px");
            $("div[id^=VisibleReportContentvaccinePurchasingReport]").css("height", "450px");
        }
        function saveData() {
            var customMessage = $("#txtCustom").text();
            if (customMessage == null) {
                alert("Please Enter Message");
                return false;
            }
            else {
                //  $("#txtCustom").html($("#txtCustom").text());
                // $(".jqte_editor").val("");
                $("#hfCustomValue").val("btnSave");
                updateHiddenField();
            }

            if ($("#chkSchedule").prop('checked') == true) {
                var startDate = $("#txtFromDate").val();
                var endDate = $("#txtToDate").val();
                if (startDate != "" && endDate != "" && startDate != undefined && endDate != undefined) {
                    if (startDate > endDate) {
                        alert("Start date should be less than end date");
                        $("#hfCustomValue").val("");
                        return false;
                    }
                }
                else {
                    alert("Please select schedule dates");
                    $("#hfCustomValue").val("");
                    return false;
                }
            }
        }
    </script>
     <style type="text/css">
        .ui-widget
        {
            font-size: 11px;
        }
       .cart-calendar-month
        {
            font-size: 11px;
        }
        </style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
            <tr>
                <td colspan="2">
                    <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr>

                <td colspan="2" align="center" style="background-color: #FFFFFF;">
                    <div style="text-align: left; padding-left: 67px;">
                        <asp:Label ID="lblMessage" runat="server" Text="" class="formFields" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </div>
                    <br/>
                    <table width="800" border="0" cellspacing="8" style="border: 1px solid #999; padding-bottom: 2em">
                        <tr>
                            <td align="left" valign="top" class="subTitle">Admin Message To Users
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="5%" class="logSubTitles">Message:</td>
                                        <td id="rowBusinessType" style="vertical-align: central" runat="server" class="logSubTitles">
                                            <asp:RadioButton ID="chkMessageOn" runat="server" Text="On" class="logSubTitles" />
                                            <asp:RadioButton ID="chkMessageOff" runat="server" Text="Off" class="logSubTitles" />
                                            <asp:RadioButton ID="chkSchedule" runat="server" Text="Schedule" class="logSubTitles" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            Start Date
                                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="formFields" Width="100px"></asp:TextBox>
                                            End Date:
                                            <asp:TextBox ID="txtToDate" runat="server" CssClass="formFields" Width="100px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <table width="70%">
                                                <td align="left" valign="top">
                                                    <%--<asp:TextBox ID="txtCustom" CssClass="textEditor1" runat="server" TextMode="MultiLine" Height="52px" Width="100px"></asp:TextBox>--%>
                                                    <textarea id="txtCustom" class="textEditor1" wrap="hard"></textarea>
                                                    <asp:HiddenField ID="hfCustom" runat="server" />
                                                    <asp:HiddenField ID="hfCustomValue" runat="server" />

                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" nowrap="nowrap" class="auto-style1">
                                <%--<asp:Button ID="btnSave" runat="server" OnClientClick="javascript:return saveData();" OnClick="btnSave_Click" Text="Save" class="logSubTitles" />--%>
                                <asp:LinkButton ID="lnkSave" runat="server" CssClass="customSaveButton" OnClientClick="javascript:return saveData();" OnClick="btnSave_Click" Text="Save"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../images/spacer.gif" width="1" height="10" alt="spacer" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>

<%--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-te/1.4.0/jquery-te.min.js" type="text/javascript"></script>--%>
<script language="javascript" type="text/javascript">

    $('textarea').jqte({
        'format': false,
        'outdent': false,
        'indent': false,
        'left': false,
        'center': false,
        'right': false,
        'rule': false,
        'sub': false, 'sup': false,
        'strike': false, 'link': false, 'unlink': false,
        'remove': false,
        'source': false
    });

</script>
</html>
