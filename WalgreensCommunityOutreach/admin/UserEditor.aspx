﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserEditor.aspx.cs" Inherits="UserEditor" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="WFooter" TagPrefix="ucFooter" %>
<%@ Register src="../controls/walgreensHeader.ascx" tagname="WHeader" tagprefix="ucHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>

    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <link href="../themes/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/gridstyles.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/grid.locale-en.js" type="text/javascript"></script>
    <script src="../javaScript/jquery.jqGrid.min.js?a=1" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../themes/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css" />
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script> 
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
         function ltrim(s) {
             var l = 0;
             while (l < s.length && s[l] == ' ')
             { l++; }
             return s.substring(l, s.length);
         }

         function rtrim(s) {
             var r = s.length - 1;
             while (r > 0 && s[r] == ' ')
             { r -= 1; }
             return s.substring(0, r + 1);
         }

         function trim(s) {
             return rtrim(ltrim(s));
         }

         function editUser(){
             var user_id = document.getElementById('lblTempUserName').value;
             var login_username = '<%=this.loginUserName%>';
              if (user_id == "") {
                  alert('<%=(string)GetGlobalResourceObject("errorMessages", "userSelectionError") %>', '');
                 return false;
             }
         }

         function deleteUser() {
             var user_id = document.getElementById('lblTempUserName').value;
             var login_username = '<%=this.loginUserName%>';
             if (user_id == "") {
                 alert('<%=(string)GetGlobalResourceObject("errorMessages", "removeSelectedUser") %>', '');
                 return false;
             }
             else {
                 if (trim(user_id).toLocaleLowerCase() != trim(login_username).toLocaleLowerCase()) {
                     return confirm('<%=(string)GetGlobalResourceObject("errorMessages", "removeUser") %>: ' + user_id + '? ');
                 }
                 else {
                     alert('<%=(string)GetGlobalResourceObject("errorMessages", "loggedInUser") %>', '');
                     return false;
                 }
             }
         }

         function bindUserProfiles() {
            $("#gridUserProfiles").jqGrid("GridUnload");

             var grid_cols = [
                { name: 'pk', index: 'pk', hidden: true, width: 5 },
                { name: 'firstname', index: 'firstname', width: 120, sorttype: "string" },
                { name: 'lastname', index: 'lastname', width: 120, sorttype: "string" },
                { name: 'username', index: 'username', width: 205, sorttype: "string" },
                { name: 'rolename', index: 'rolename', width: 155, sorttype: "string" },
                { name: 'regionId', index: 'regionId', width: 65, sorttype: "int" },
                { name: 'areaId', index: 'areaId', width: 65, sorttype: "int" },
                { name: 'districtId', index: 'districtId', width: 65, sorttype: "int" },
                { name: 'storeId', index: 'storeId', width: 65, sorttype: "int" }
             ];

             jQuery("#gridUserProfiles").jqGrid({
                    datatype: "local",
                    data: <%=this.userProfiles %>,
                    gridview: true,
                    viewrecords: true,
                    pager: '#pagernav',
                    emptyrecords: "No records found",
                    sortname: 'firstname, lastname, username, rolename',
                    rowNum: 20,
                    sortorder: "desc",
                    multiSort: true,
                    sortable: true,
                    toppager: true,
                    scrollOffset: 0,
                    columnReordering: false,
                    height: 'auto',
                    width: 885,
                    colNames: ['pk', 'First Name', 'Last Name', 'Username', 'User Type', 'Region Id', 'Area Id', 'District Id', 'Store Id'],
                    colModel: grid_cols,
                    onPaging: function (nav_button) {
                        $("#lblTempUserName").val("");
                        $("#lblTempUserId").val("");
                    },
                    beforeSelectRow: function(rowid, e) {
                        var sel_row = $("#gridUserProfiles").jqGrid("getGridParam", "selrow");
                        var trElement = jQuery("#" + sel_row, jQuery('#gridUserProfiles'));

                        if(sel_row != "" && sel_row != null) {
                            trElement.removeClass('highlight-selected-row');
                            trElement.addClass('ui-widget-content');
                        }

                        return true;
                    },
                    onSelectRow: function (id) {
                        var trElement = jQuery("#" + id, jQuery('#gridUserProfiles'));
                        trElement.removeClass('ui-widget-content ui-state-highlight');
                        trElement.addClass('highlight-selected-row');

                        $("#lblTempUserName").val($("#gridUserProfiles").getRowData(id).username);
                        $("#lblTempUserId").val($("#gridUserProfiles").getRowData(id).pk);
                    }
                });

                jQuery("#gridUserProfiles").jqGrid('navGrid', '#pagernav', { add: false, edit: false, del: false, search: false, refresh: false, cloneToTop:false });
         }

         $(document).ready(function(){
            bindUserProfiles();
         });
    </script>
  <style type="text/css" >
    .ui-widget
    {
        font-size:11px;
    }
       
    /*.ui-widget-content
    {
        height:25px;
        border:1px none #FFFFFF;
        /*border-left:1px none #FFFFFF;
        border-right:1px none #FFFFFF;*/
    }*/ 
        
    .highlight-selected-row
    {
        border: 1px solid #cccccc/*{borderColorHighlight}*/;
        background: #A4A4A4;
        color: #363636/*{fcHighlight}*/;
    }
        
    .ui-widget-header
    {
        border-bottom:0px none #FFFFFF;        
    }
    
    .ui-jqgrid-bdiv
    {
        border-left: 1px solid #aaaaaa;
    }
    
    .ui-jqgrid tr.jqgrow
    {
        border: 1px solid #aaaaaa;
    }
    
    .ui-jqgrid-hdiv
    {
        font-size:12px;
        font-weight:bold;
        height: 25px;
    }
    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnSearchUser">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" ShowSummary="false" />
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucHeader:WHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
      <table width="910" border="0" cellspacing="0" cellpadding="0" style="padding-left:25px;">
        <tr>
            <td valign="bottom" style="padding-top:6px; padding-bottom:6px; vertical-align:middle">
                <asp:Label style="font-family: Trebuchet MS, Arial, Helvetica, sans-serif;font-size: 14px;font-weight: normal;color: #333333;" ID="lblUserType" runat="server" Text="User Type "></asp:Label>
                <asp:DropDownList ID="ddlUserRoles" CssClass="profileFormObject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserRoles_SelectedIndexChanged"></asp:DropDownList>
            </td>
            <td align="right" valign="bottom" style="padding-top:6px; padding-bottom:6px">
                <table  border="0" cellspacing="2" cellpadding="0">
                    <tr>
                        <td align="right" valign="middle">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="formFields"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="txtSearchRV" ControlToValidate="txtSearch" Display="None" ValidationExpression="[^'<>]+" runat="server" ErrorMessage ="Search: '< > Characters are not allowed."></asp:RegularExpressionValidator>
                        </td>
                        <td valign="top">
                            <asp:ImageButton ID="btnSearchUser" AlternateText="Search" ImageUrl="../images/btn_search.png" onmouseout="javascript:MouseOutImage(this.id,'../images/btn_search.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'../images/btn_search_lit.png');" runat="server" OnCommand="cmdProcessUser_Click" CommandArgument="Search" />
                        </td>
                        <td valign="top">
                            <asp:ImageButton ID="btnResetUser" AlternateText="Reset" ImageUrl="../images/btn_show_all.png" onmouseout="javascript:MouseOutImage(this.id,'../images/btn_show_all.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'../images/btn_show_all_lit.png');" runat="server" OnCommand="cmdProcessUser_Click" CommandArgument="Reset"/>
                        </td>
                      </tr>
                </table>
            </td>
        </tr>
        <tr class="gridHeadGradient">
            <td class="categoryNavTitle">
                User Profiles
            </td>
            <td align="right" valign="middle" class="categoryNav">
                <%--<span class="class14">
                    <asp:LinkButton runat="server" CssClass="class2" ID="lnkBtnEmailStatus"  PostBackUrl="~/admin/walgreensEmailStatistics.aspx" Text="Email & Site Status" />
                </span>| --%>    
                <span class="class14">
                    <asp:LinkButton runat="server" CssClass="class14" ID="lnkBtnAddUser" CommandArgument="Add" OnCommand="cmdProcessUser_Click" Text="Add User" 
                    ToolTip="Click to add new user profile" /> |
                    <asp:LinkButton runat="server" CssClass="class14" ID="lnkBtnEditUser" CommandArgument="Edit" OnCommand="cmdProcessUser_Click" Text="Edit User" 
                    ToolTip="Click to edit existing user profile" OnClientClick ="javascript:return editUser();" /> |
                    <asp:LinkButton runat="server" CssClass="class14" ID="lnkBtnRemoveUser" CommandArgument="Remove" OnCommand="cmdProcessUser_Click" Text="Remove User" 
                    ToolTip="Click to remove existing user profile" OnClientClick="javascript:return deleteUser();"/>
                </span>
            </td>
            </tr>
        <tr>
            <td colspan="2" valign="top" nowrap="nowrap" height="590" style="padding-bottom:15px;">
                <input type ="hidden" ID="lblTempUserName" runat="server" />
                <input type ="hidden" ID="lblTempUserId" runat="server" />
                <table runat="server" name="gridUserProfiles"  ID="gridUserProfiles" width="100%"></table>
                <div id="pagernav"></div>

                <%--<asp:GridView ID="mainGrid" runat="server" DataKeyNames="" AutoGenerateColumns="False" CellPadding="4" GridLines="None" Width="100%"  SelectedRowStyle-BackColor="lightblue" 
                OnRowDataBound="mainGrid_RowDataBound" OnPageIndexChanging="mainGrid_PageIndexChanging" AllowPaging="True" PageSize="15" PagerSettings-Mode="NumericFirstLast">
                <AlternatingRowStyle BackColor="#f0f0f0" />
                <PagerSettings FirstPageText="First" LastPageText="Last" />
                <FooterStyle CssClass="footerGrey" />
                <PagerStyle CssClass="GridFooterText" />
                <HeaderStyle CssClass="historyGridColTitle" />
                <RowStyle CssClass="Row" />
                <Columns>
                    <asp:TemplateField HeaderText="UserID" HeaderStyle-HorizontalAlign="Left" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblUserID" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                        <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("firstName") %>' width="75" class="wrapword"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                        <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("lastName") %>' width="75" class="wrapword"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Username" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                        <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("userName") %>' width="150" class="wrapword"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="User Type" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                        <asp:Label ID="lblUserType" runat="server" Text='<%# Bind("UserRole") %>' Width="100" class="wrapword" CssClass="wrapword" ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Region Id" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblRegionNumber" runat="server" Text='<%# Bind("RegionNumber") %>' Width="40" class="wrapword" CssClass="wrapword" ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText="Area Id" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblAreaNumber" runat="server" Text='<%# Bind("AreaNumber") %>'  Width="40" class="wrapword" CssClass="wrapword" ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText="District Id" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                        <asp:Label ID="lblDistrictNumber" runat="server" Text='<%# Bind("DistrictNumber") %>' Width="40" class="wrapword" CssClass="wrapword" ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>  
                    <asp:TemplateField HeaderText="Store Id" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("StoreId") %>' Width="40" class="wrapword" CssClass="wrapword" ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--  <asp:TemplateField HeaderText="Address1" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                        <asp:Label ID="lblAddress1" runat="server" Text='<%# Bind("Address1") %>' Width="90" class="wrapword"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address2" HeaderStyle-HorizontalAlign="Left" Visible="false">
                        <ItemTemplate>
                        <asp:Label ID="lblAddress2" runat="server" Text='<%# Bind("Address2") %>' Width="100" class="wrapword"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%# Bind("City") %>' width="65" class="wrapword"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%# Bind("State") %>' Width="30"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>  --%>
                    <%--<asp:TemplateField HeaderText="Active">
                        <ItemTemplate>
                            <asp:CheckBox Enabled="false" ID="checkDelete" runat="server" Checked='<%# Bind("active") %>' /></ItemTemplate>
                    </asp:TemplateField>--%>
                <%--</Columns>
                <EmptyDataTemplate>
                    <center>
                        <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields"  runat="server"></asp:Label></b>
                    </center>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="LightBlue" />
                </asp:GridView>--%>
            </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<ucFooter:WFooter id="walgreensFooter" runat="server" />
</form>
</body>
</html>
