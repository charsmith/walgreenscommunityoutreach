﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using TdApplicationLib;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Security;
using TdWalgreens;
using System.Data;

public partial class UserEditor : Page
{
    #region --------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Session.Remove("UserProfiles");
            this.bindUserRoles();
            this.makeMainGrid();
        }
    }

    protected void ddlUserRoles_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.txtSearch.Text = string.Empty;
        this.lblTempUserName.Value = string.Empty;
        Session.Remove("UserProfiles");
        this.makeMainGrid();
    }

    protected void cmdProcessUser_Click(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLower())
        {
            case "add":
                Session.Remove("UserProfiles");
                this.commonAppSession.CommandName = (string)GetGlobalResourceObject("errorMessages", "addText");
                Response.Redirect("EditUser.aspx", false);
                break;
            case "edit":
                Session.Remove("UserProfiles");
                this.doEditUser();
                break;
            case "remove":
                this.doRemoveUser();
                break;
            case "search":
                this.makeMainGrid();
                break;
            case "reset":
                this.txtSearch.Text = string.Empty;
                this.makeMainGrid();
                break;
        }
    }
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        Session.Remove("UserProfiles");
        this.bindUserRoles();
        this.makeMainGrid();
    }

    #endregion

    #region --------------- PRIVATE METHODS ---------------
    /// <summary>
    /// Binds user roles to dropdown
    /// </summary>
    private void bindUserRoles()
    {
        this.ddlUserRoles.DataSource = this.dbOperation.getUserRoles;
        this.ddlUserRoles.DataValueField = "RoleName";
        this.ddlUserRoles.DataTextField = "RoleName";
        this.ddlUserRoles.DataBind();
        this.ddlUserRoles.Items.Insert(0, new ListItem("All", ""));
    }

    /// <summary>
    /// Binds user profiles to grid
    /// </summary>
    private void makeMainGrid()
    {
        DataTable dt_user_profiles = new DataTable();
        if ((DataTable)Session["UserProfiles"] != null && ((DataTable)Session["UserProfiles"]).Rows.Count > 0)
            dt_user_profiles = (DataTable)Session["UserProfiles"];
        else
        {
            dt_user_profiles = this.dbOperation.getUserprofiles(this.ddlUserRoles.SelectedValue.ToLower().Trim());
            Session["UserProfiles"] = dt_user_profiles;
        }

        if (!string.IsNullOrEmpty(this.txtSearch.Text.Trim()))
        {
            IEnumerable<DataRow> filtered_users = dt_user_profiles.AsEnumerable().Where(user => user.Field<string>("username").ToLower().Contains(this.txtSearch.Text.Trim().ToLower()) ||
                                                                                        (!string.IsNullOrEmpty(user.Field<string>("firstname")) && user.Field<string>("firstname").ToLower().Contains(this.txtSearch.Text.Trim().ToLower())) ||
                                                                                        (!string.IsNullOrEmpty(user.Field<string>("lastname")) && user.Field<string>("lastname").ToLower().Contains(this.txtSearch.Text.Trim().ToLower())) ||
                                                                                        user.Field<string>("RoleName").ToLower().Contains(this.txtSearch.Text.Trim().ToLower()) ||
                                                                                        user.Field<object>("storeId").ToString().ToLower().Contains(this.txtSearch.Text.Trim().ToLower()) ||
                                                                                        user.Field<object>("districtId").ToString().Contains(this.txtSearch.Text.Trim()) ||
                                                                                        user.Field<object>("areaId").ToString().Contains(this.txtSearch.Text.Trim()) ||
                                                                                        user.Field<object>("regionId").ToString().Contains(this.txtSearch.Text.Trim()));

            if (filtered_users.Count() > 0)
                this.userProfiles = filtered_users.CopyToDataTable().getJSONObject();
        }
        else
            this.userProfiles = dt_user_profiles.getJSONObject();
    }

    /// <summary>
    /// Removes selected user
    /// </summary>
    private void doRemoveUser()
    {
        Label user_name = new Label();
        user_name.Text = this.lblTempUserName.Value.Trim();
        bool is_deleted = false;
        try
        {
            if (user_name.Text.Trim().Length > 0)
            {
                if (this.commonAppSession.LoginUserInfoSession.IsAdmin && (user_name.Text.ToString().Trim() == this.commonAppSession.LoginUserInfoSession.UserName))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "invalidUserSelection") + "');", true);
                    return;
                }

                if (!string.IsNullOrEmpty(user_name.Text) && user_name.Text.ToString().Trim() != this.commonAppSession.LoginUserInfoSession.UserName)
                {
                    MembershipUser selected_user;
                    selected_user = Membership.GetUser(user_name.Text.Trim());
                    selected_user.IsApproved = false;
                    Membership.UpdateUser(selected_user);

                    is_deleted = this.dbOperation.doRemoveUser(user_name.Text.Trim());
                    if (is_deleted)
                    {
                        this.lblTempUserName.Value = "";

                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userDeleted") + "');", true);
                    }

                    Session.Remove("UserProfiles");
                    this.makeMainGrid();
                }
                else if (string.IsNullOrEmpty(user_name.Text))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userSelectionError") + "');", true);
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userSelectionError") + "');", true);
                return;
            }

            if (!is_deleted)
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "logedUser") + "');", true);
        }
        catch (Exception ex)
        {
            this.dbOperation.logError(-1, ex.Message, ex.StackTrace);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userDeletionError") + "');", true);
        }
    }

    /// <summary>
    /// Gets existing user data to update
    /// </summary>
    private void doEditUser()
    {
        try
        {
            string user_name = this.lblTempUserName.Value.Trim();

            if (user_name.Trim().Length > 0)
            {
                if (this.commonAppSession.LoginUserInfoSession.IsPowerUser && this.commonAppSession.LoginUserInfoSession.IsAdmin)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "adminUsersCannotEdit") + "');", true);
                    this.lblTempUserName.Value = "";
                }
                else
                {
                    this.commonAppSession.SelectedUser = user_name.Trim();
                    this.commonAppSession.CommandName = (string)GetGlobalResourceObject("errorMessages", "editText");
                    Response.Redirect("EditUser.aspx", false);
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userSelectionError") + "');", true);
            }
        }
        catch (Exception ex)
        {
            this.dbOperation.logError(-1, ex.Message, ex.StackTrace);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "userDeletionError") + "');", true);
        }
    }

    #endregion

    #region --------------- PRIVATE VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    protected string userProfiles = "[]";
    #endregion

    #region --------------- PUBLIC VARIABLES --------------
    public string loginUserName;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();

        this.loginUserName = this.commonAppSession.LoginUserInfoSession.UserName;
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
   
    #endregion
}
