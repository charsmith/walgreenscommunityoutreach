using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using TdApplicationLib;
using System.Web.Configuration;
using System.Configuration;
using tdEmailLib;
using System.Net.Configuration;
using TdWalgreens;


public partial class walgreensEmailStatistics : Page
{
    #region -------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.bindGrid();
        }
    }
    #endregion

    #region ---------- PROTECTED ACCESSORS-----------

    protected void saveButton_Click(object sender, EventArgs e)
    {
        string strEmailBody = string.Empty;
        strEmailBody += "<p>";
        strEmailBody += getHTML(this.grdEmailStatistics);
        strEmailBody += "</p><br /><p>";
        strEmailBody += getHTML(this.grdHighLevelReport);
        strEmailBody += "</p><p>";

        //strEmailBody = strEmailBody.Replace("75%", "100%");

        EncryptedQueryString args = new EncryptedQueryString();
        args["arg1"] = this.commonAppSession.LoginUserInfoSession.UserName;
        args["arg2"] = ApplicationSettings.walgreensAppPwd();
        
        string image_path = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
        Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
        string password = ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Password != null ? ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Password : "";

        if (((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Host != null)
        {
            WalgreenEmail walgreen_mail = new WalgreenEmail(((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Host, ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.UserName, password, ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Port, ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.From);
            walgreen_mail.sendUsersStatisticsEmail(args.ToString(), "NewUser", "Attention Administrators", "Admin_Outreach_Status", image_path, Server.MapPath("~/emailTemplates/corporateEmailTemplate.htm"), ((string)GetGlobalResourceObject("errorMessages", "emailStatistics")), strEmailBody, "walgreensLandingPage.aspx?args=", this.getEmailFromConfig(), true);
        }
    }

    protected void doCancel(object sender, CommandEventArgs e)
    {
        Response.Redirect("UserEditor.aspx", false);
    }
    #endregion

    #region --------- PRIVATE METHODS--------------
    private string getEmailFromConfig()
    {
        string to_email = this.commonAppSession.LoginUserInfoSession.Email.Trim();
        if (!string.IsNullOrEmpty(to_email))
            to_email += "," + ApplicationSettings.getEmailInfoFromGroup("corporateUsersEmailGroup", false).Trim();
        else
            to_email += ApplicationSettings.getEmailInfoFromGroup("corporateUsersEmailGroup", false).Trim();

        return to_email;
    }

    /// <summary>
    /// Binding Data grid
    /// </summary>
    private void bindGrid()
    {
        this.grdEmailStatistics.DataSource = this.dbOperation.getEmailSentStatistics(this.commonAppSession.SelectedStoreSession.OutreachProgramSelected);
        this.grdEmailStatistics.DataBind();

        this.grdHighLevelReport.DataSource = this.dbOperation.getEmailSentHighLevelReport(this.commonAppSession.SelectedStoreSession.OutreachProgramSelected);
        this.grdHighLevelReport.DataBind();
    }

    /// <summary>
    /// get HTML from Grid.
    /// </summary>
    /// <param name="gv"></param>
    /// <returns></returns>
    private string getHTML(GridView gv)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        gv.RenderControl(hw);
        return sb.ToString();
    }
    #endregion

    #region --------- PRIVATE VARIABLES ------------------
    DBOperations dbOperation = null;
    AppCommonSession commonAppSession = null;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
}
