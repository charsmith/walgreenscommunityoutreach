﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensStoreBusiness.aspx.cs" Inherits="walgreensStoreBusiness" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>    
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="css/wags.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />   

    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="javaScript/jquery-ui.js"></script>    
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddlStoreList").change(function () {
                $("#txtStoreId").val($("#ddlStoreList option:selected").val());
                $("#btnRefresh").click();
            });

            $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
            dropdownRepleceText("ddlStoreList", "txtStoreList");

            $('#chkHhsProgram').change(function () {
                if (this.checked) {
               $('#chkCOProgram').prop('checked', false);
                    $('#hdSelectedBusinessType').val('charity');
                }
                else {
                    $('#hdSelectedBusinessType').val('');
                }
            });
            $('#chkCOProgram').change(function () {
                if (this.checked) {
                    $('#chkHhsProgram').prop('checked', false);
                    $('#hdSelectedBusinessType').val('CommunityOutreach');
                }
                else {
                    $('#hdSelectedBusinessType').val('');
                }
            });
        });

        function validateOutreachEfforts(sender, args) {
            args.IsValid = false;

            var chkLstUserOutreachEfforts = document.getElementById('<%=chkLstOutreachEfforts.ClientID %>');
            var chkList = chkLstUserOutreachEfforts.getElementsByTagName("input");
            for (var i = 0; i < chkList.length; i++) {
                if (chkList[i].checked) {
                    args.IsValid = true;
                }
            }
        }
              
        function SelectDuplicateBusiness(business_id) {
            var select_business_id = document.getElementById(business_id);
            var radio_buttons = document.getElementsByTagName("input");
            var selected_storeId_ctrl = business_id.replace("rbSelectBusiness", "lblStoreId");
            var selected_outreach_ctrl = business_id.replace("rbSelectBusiness", "lblOutreachEffort");
            for (i = 0; i < radio_buttons.length; i++) {
                if (radio_buttons[i].type == "radio" && radio_buttons[i].id != select_business_id.id) {
                    radio_buttons[i].checked = false;
                }
            }

            var out_reach_message = "";
            if ($("#" + selected_outreach_ctrl).text() == "Senior Outreach") {
                out_reach_message = "Immunization Program"
                $("#hdOutreachEffort").val(3);
            }
            else {
                out_reach_message = "Senior Outreach"
                $("#hdOutreachEffort").val(1);
            }

            $("#hdOutreach").val("");
            $("#lblOutreachMessage").hide();

            $.each($('#chkLstOutreachEfforts').find('span label'), function (key, val) {
                if ($('#' + val.previousSibling.id)[0].checked != true) {
                    $('#' + val.previousSibling.id)[0].checked = true;
                }
            });

            $.each($('#chkLstOutreachEfforts').find('span label'), function (key, val) {
                if (val.innerHTML == $("#" + selected_outreach_ctrl).text() && $('#' + val.previousSibling.id)[0].checked) {
                    $('#' + val.previousSibling.id)[0].checked = false;
                    $("#lblOutreachMessage").html(" This business is only available to add for " + out_reach_message);
                    $("#lblOutreachMessage").show();
                }
            });
        }

        function displayConfirmation(alert_message, business_pk) {
            var msg = confirm(alert_message);
            if (msg) {
                __doPostBack('deleteContactLog', business_pk);
            }
        }

        function showDNCMatchWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 250,
                    width: 350,
                    title: "Business matches DNC Client",
                    buttons: {
                        'Add Business': function () {
                            __doPostBack("AddBusiness", 1);
                            $(this).dialog('close');
                        },
                        "Do Not Add Business" : function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }
    </script>
    <style type="text/css" >
        .ui-widget {
            font-size: 11px;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" >
<asp:HiddenField ID="hdOutreachEffort" runat="server" />
<asp:HiddenField ID="hdSelectedBusinessType" runat="server" Value="" />
<%--<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" style="display:none" OnClick="btnRefresh_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />--%>  
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>    
<asp:Label ID="lblNewWindow" runat="server" style="display:none"></asp:Label>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
       <table width="935" border="0" cellspacing="22" cellpadding="0">
        <tr>
            <td colspan="2" class="pageTitle2" style="border-bottom: 1px solid #999999; padding-top: 0px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top" class="pageTitle2" >Search/Add A Business</td>
                        <td align="right" valign="middle" class="formFields">
                          <span class="class2"><asp:LinkButton runat="server" CssClass="class2" ID="saveButton"  CommandName="doSave" OnCommand="doSave" Text="Submit"></asp:LinkButton></span> | 
                          <span class="class2"><asp:LinkButton runat="server" CssClass="class2" ID="cancelButton" CommandName="doCancel" OnCommand="doCancel" Text="Cancel" CausesValidation="false" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server"  DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true"  ShowSummary="false" />
                        </td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" valign="top" nowrap="nowrap" height="350">                           
                <table width="750px">
                    <tr>
                        <td colspan="4" align="left" class="formFields" style="padding-bottom: 10px;">
                            To add a Business to your Outreach Contact Log, complete the form below as fully
                            as possible. If the businesses is already assigned to another Walgreens locations,
                            you may not be able to add the business to your own Contact Log.
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="4" align="left" nowrap="nowrap" class="profileTitles" style="padding-bottom: 10px;">
                            *Add Business to Walgreens Store:
                            <%--<table width="100%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores">
                                <tr>
                                    <td align="left" valign="top">
                                        <span class="logSubTitles">Store Search: </span>
                                    </td>
                                    <td>
                                        <uc3:getStoreControl ID="getStoreControl1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <span class="logSubTitles">Store Name: </span>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px"></asp:Label>
                                    </td>
                                </tr>
                            </table>--%>
                           <%-- <table border="0" cellspacing="5" cellpadding="0" runat="server" id="tdStoreSelectionDropDown" width="100%">
                                <tr>
                                    <td align="left" valign="top" nowrap="nowrap" width="107px">
                                        <span class="logSubTitles">Store Name: </span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList CssClass="formFields" ID="ddlStoreList" runat="server" EnableViewState="false" Width="440px"></asp:DropDownList>
                                        <asp:TextBox CssClass="formFields"  ID="txtStoreList" runat="server" Width="438px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>--%>
                        </td>
                    </tr>
                </table>
                <table width="750" border="0" cellspacing="0" cellpadding="15" class="wagsRoundedCorners">
                  <tr>
                    <td>
                        <table width="100%">
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles">*Business Name: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtBusinessName" runat="server" CssClass="formFields" MaxLength="150" Width="165px" TabIndex="1"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="txtBusinessNameReqEV" ControlToValidate="txtBusinessName" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Business Name:: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="txtBusinessNameReqFV" runat="server" ControlToValidate="txtBusinessName" Display="None" ErrorMessage="Business Name is required"></asp:RequiredFieldValidator>
                            </td>
                            <td align="right" class="profileTitles">*Phone #: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtPhone" runat="server" CssClass="formFields" Width="120" onblur="textBoxOnBlur(this);" MaxLength="14" TabIndex="11"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtPhoneReqFV" ControlToValidate="txtPhone" Display="None" runat="server" ErrorMessage="Valid Phone Number is required(ex:(###) ###-####)" ></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="txtPhoneCV" ControlToValidate="txtPhone" runat="server" Display="None" ErrorMessage="Valid Phone Number is required (ex: ###-###-####)" ClientValidationFunction="ValidateCompanyPhoneFax" OnServerValidate="validatePhone"></asp:CustomValidator>
                            </td>
                          </tr>
                          <tr>
                            <td width="18%" align="right" nowrap="nowrap" class="profileTitles"> Contact First Name: </td>
                            <td width="31%" align="left" class="profileTitles">
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="formFields" MaxLength="25" TabIndex="2"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="txtFirstNameRegEV" ControlToValidate="txtFirstName" Display="None" ValidationExpression="[^<>@#&]+" runat="server" ErrorMessage="First Name: < > @ # & Characters are not allowed."></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="right" class="profileTitles">Toll Free #:</td>
                            <td width="31%" align="left" class="profileTitles">
                                <asp:TextBox ID="txtTollFree" runat="server" CssClass="formFields" Width="120" MaxLength="15" TabIndex="12"></asp:TextBox>
                            </td>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> Contact Last Name: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="formFields" MaxLength="35" TabIndex="3"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="txtLastNameRegEV" ControlToValidate="txtLastName" Display="None" ValidationExpression="[^<>@#&]+" runat="server" ErrorMessage="Last Name: < > @ # & Characters are not allowed."></asp:RegularExpressionValidator></td>
                            <td align="right" class="profileTitles">*Industry: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtIndustry" runat="server" CssClass="formFields" MaxLength="49" TabIndex="13" size="32"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtIndustryReqFV" ControlToValidate="txtIndustry" Display="None" runat="server" ErrorMessage="Industry is required."></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="txtIndustryRegEV" ControlToValidate="txtIndustry" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Industry Name:: < > Characters are not allowed."></asp:RegularExpressionValidator>
                            </td>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> Job Title: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="formFields" MaxLength="100" TabIndex="4"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="txtTitleRegEV" ControlToValidate="txtTitle" Display="None" ValidationExpression="[^<>@#&]+" runat="server" ErrorMessage="Title: < > @ # & Characters are not allowed."></asp:RegularExpressionValidator>
                            </td>
                            <td align="right" class="profileTitles">Website URL:</td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtWebUrl" runat="server" CssClass="formFields" MaxLength="200" TabIndex="14" size="32"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="txtWebUrlRegEV" ControlToValidate="txtWebUrl" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Web URL: < > Characters are not allowed."></asp:RegularExpressionValidator>
                            </td>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> *Address1:<br /></td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtAddress1" runat="server" CssClass="formFields"  size="32" MaxLength="450" TabIndex="5"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtAddress1ReqFV" ControlToValidate="txtAddress1" Display="None" runat="server" ErrorMessage="Address is required."></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="txtAddress1RegEV" ControlToValidate="txtAddress1" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Address: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                <asp:CustomValidator ID="txtAddressCV" runat="server" ErrorMessage="<%$ resources:errorMessages, BusinessLocationPOBoxAlert %>" Display="None" ClientValidationFunction="validateAddress"  OnServerValidate="validateAddress"></asp:CustomValidator>
                            </td>
                            <td align="right" nowrap="nowrap" class="profileTitles"> &nbsp;Employment Size:</td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtEmpSize" runat="server" CssClass="formFields" MaxLength="6" TabIndex="15"></asp:TextBox>
                                <asp:RegularExpressionValidator ControlToValidate="txtEmpSize" ID="txtEmpSizeRegEV" runat="server" Display="None" ErrorMessage="Enter a valid Employment Size" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                            </td>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> Address2:</td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtAddress2" runat="server" CssClass="formFields" size="32" MaxLength="40" TabIndex="5"></asp:TextBox>           
                            </td>
                            <td align="right" class="profileTitles">Community Outreach:</td>
                            <td align="left" class="profileTitles">
                                <asp:CheckBox CssClass="formFields" ID="chkCOProgram" runat="server" groupname="ProgramName" CausesValidation="false" Visible="true" Enabled="true" />
                                <%--<asp:RadioButton CssClass="formFields" ID="chkCOProgram" AutoPostBack="true" runat="server"  />--%>
                                  </td>
                          </tr>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> County: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtCounty" runat="server" CssClass="formFields" size="32" MaxLength="25" TabIndex="6"></asp:TextBox>          
                                <asp:RegularExpressionValidator ID="txtCountyRegEV" runat="server" ControlToValidate="txtCounty" Display="None" ErrorMessage="County: < > Characters are not allowed." ValidationExpression="[^<>]+"></asp:RegularExpressionValidator>
                            </td>
                          <td align="right" class="profileTitles"><%--Charity (HHS Voucher): --%></td>
                            <td align="left" class="profileTitles">*Required Fields
                                <%--<asp:CheckBox CssClass="formFields" ID="chkHhsProgram" runat="server" groupname="ProgramName" CausesValidation="false" Visible="true" Enabled="true" />--%>
                                <%--<asp:RadioButton CssClass="formFields" ID="chkHhsProgram" AutoPostBack="true" runat="server"  />--%>
                            </td>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> *City: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtCity" runat="server" CssClass="formFields" size="32" MaxLength="25" TabIndex="7"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtCityReqFV" ControlToValidate="txtCity" Display="None" runat="server" ErrorMessage="City is required."></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="txtCityRegEV" ControlToValidate="txtCity" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="City: < > Characters are not allowed."></asp:RegularExpressionValidator>
                            </td>
                            <td align="right" width="200px" class="profileTitles">&nbsp;</td>
                            <td align="left" class="formFields2" valign="top"></td>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> *State: </td>
                            <td align="left" class="profileTitles">
                                <asp:DropDownList ID="ddlState" runat="server"  CssClass="profileFormObject" TabIndex="8"> </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="ddlStateReqFV" ControlToValidate="ddlState" Display="None" runat="server" ErrorMessage="State is required."></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="profileTitles">&nbsp;</td>
                            <td align="left" valign="middle" class="formFields2">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="right" nowrap="nowrap" class="profileTitles"> *Zip Code: </td>
                            <td align="left" class="profileTitles">
                                <asp:TextBox ID="txtZip" runat="server" CssClass="formFields" Width="53" MaxLength="5" TabIndex="9"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtZipReqFV" ControlToValidate="txtZip" Display="None" runat="server" ErrorMessage="Zip Code is required (ex:#####)."></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="txtZipCV" runat="server" ErrorMessage="Valid Zip Code is required (ex: #####)" ControlToValidate="txtZip" Display="None" ClientValidationFunction="validateZipCode" OnServerValidate="validateZipCode"></asp:CustomValidator>
                                <asp:TextBox ID="txtZip4" runat="server" CssClass="formFields" Width="53" MaxLength="4" TabIndex="10"></asp:TextBox>
                                <asp:RegularExpressionValidator ControlToValidate="txtZip4" ID="txtZip4RegEV" runat="server" Display="None" ErrorMessage="Valid Sub Zip code is required (ex: ####)" ValidationExpression="\d{4}$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left" class="profileTitles">&nbsp;</td>
                            <td align="left" class="profileTitles">&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="4" align="right" nowrap="nowrap" class="pageTitle">&nbsp;</td>
                          </tr>
                          <tr>
                            <td valign="top" align="right" nowrap="nowrap" class="profileTitles">*Outreach Campaigns:</td>
                            <td valign="top" align="left" nowrap="nowrap" class="formFields" colspan="3">
                                <asp:CheckBoxList ID="chkLstOutreachEfforts" CssClass="profileTitles" runat="server" TabIndex="16" RepeatDirection="Vertical" ></asp:CheckBoxList>
                                <asp:CustomValidator ClientValidationFunction="validateOutreachEfforts" ID="chkLstOutreachEffortsCV" runat="server" ErrorMessage="At least one Outreach Campaign is required to be selected." Display="None"/>
                            </td>        
                          </tr>
                          <tr>
                            <td colspan="4" align="right" nowrap="nowrap" class="profileTitles">&nbsp;</td>
                          </tr>
                          <tr id="rowAddBusiness" runat="server">
                            <td colspan="4" align="center" nowrap="nowrap" class="profileTitles">
                                <asp:ImageButton ID="btnCancelButton" ImageUrl="~/images/btn_cancel_event.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_event.png');" CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_event_lit.png');" runat="server"  AlternateText="Cancel" CommandName="doCancel" OnCommand="doCancel" TabIndex="18"/>&nbsp;
                                <asp:ImageButton ID="btnSaveButton" ImageUrl="~/images/btn_submit_event.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_event.png');" CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_event_lit.png');" runat="server"  CommandName="doSave" OnCommand="doSave" AlternateText="Save Store Business" TabIndex="17" />
                            </td>
                          </tr>
                          <tr id="rowDupBusinessesGrid" runat="server" visible="false">
                            <td colspan="4">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td colspan="4" align="left" class="formFields" style="padding-bottom: 10px;">
                                        <p><asp:Label ID="lblDuplicatebusinessAlert" CssClass="formFields" runat="server"></asp:Label><br /><asp:Label ID="lblOutreachMessage" CssClass="formFields" ForeColor="Red" runat="server"></asp:Label></p>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="grdDuplicateBusiness" runat="server"  AutoGenerateColumns="False" CellPadding="1" CellSpacing="0" AllowPaging="false" AllowSorting="false" PageSize="10" GridLines="None" CssClass="formFields" Width="100%">
                                            <FooterStyle CssClass="footerGrey" />
                                            <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true"  />
                                            <RowStyle BorderWidth="1px" BorderColor= "#CCCCCC" />                                               
                                            <Columns>                                                                                                
                                                <asp:TemplateField HeaderText="businessPk" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#3096D8" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" >
                                                    <ItemTemplate>                            
                                                        <asp:RadioButton ID="rbSelectBusiness" runat="server" name="DuplicateBusiness" GroupName="DuplicateBusiness" CssClass="formFields" OnClick="javascript:SelectDuplicateBusiness(this.id)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Store Id" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Height="25px" HeaderStyle-BorderColor="#3096D8" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Business Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#3096D8" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                    <ItemTemplate>
                                                    <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>' width="100px" class="wrapword"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                    
                                                <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#3096D8" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                    <ItemTemplate>
                                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("businessAddress") %>' width="150px" class="wrapword"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                    
                                                <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#3096D8" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                    <ItemTemplate>
                                                    <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("phoneNumber") %>' width="85px" class="wrapword"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                             
                                                <asp:TemplateField HeaderText="Outreach Effort" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#3096D8" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOutreachEffort" runat="server" class="wrapword" Text='<%# Bind("outreachEffort") %>' width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                    
                                            </Columns>
                                            <SelectedRowStyle BackColor="LightBlue" />
                                        </asp:GridView>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="4" align="center" nowrap="nowrap" class="profileTitles" style="height:60px; vertical-align:middle;">
                                        <asp:ImageButton ID="btnCancelAdd" ImageUrl="~/images/btn_cancel_event.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_event.png');" CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_event_lit.png');" runat="server"  AlternateText="Cancel" CommandArgument="Cancel" OnCommand="doAssignBusiness" />&nbsp;
                                        <asp:ImageButton ID="btnAddBusiness" ImageUrl="~/images/btn_add.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_add.png');" CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_lit.png');" runat="server" CommandArgument="Add" OnCommand="doAssignBusiness" AlternateText="Add Business" />
                                    </td>
                                  </tr>
                                </table>
                            </td>
                          </tr>
                        </table>
                    </td>
                  </tr>
                </table>
                <p>&nbsp;</p>
            </td>
        </tr>
       </table>
       <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 12px; text-align:center; font-weight:normal;"></div>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooter" runat="server" />
</form>
</body>
</html>