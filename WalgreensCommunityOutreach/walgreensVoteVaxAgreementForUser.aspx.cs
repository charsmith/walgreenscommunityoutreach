﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using tdEmailLib;
using System.IO;
using TdWalgreens;
using System.Threading;
using System.Globalization;
using System.Collections.Generic;

public partial class walgreensVoteVaxAgreementForUser : Page
{
    #region ------------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.ddlLegalState.bindStates();
            this.bindContractAgreement();
        }
        this.setImageButtons();
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        Validate("BusinessUser");
        if (Page.IsValid)
        {
            if (!string.IsNullOrEmpty(this.hfBusinessUserEmail.Value) && !string.IsNullOrEmpty(this.hfContactLogPk.Value) && Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value] != null)
            {
                int contact_log_pk = 0;
                Int32.TryParse(hfContactLogPk.Value, out contact_log_pk);
                DataTable dt_approved_contract_details;
                int return_value = this.dbOperation.insertClinicAgreementApproval(contact_log_pk, rbtnApprove.Checked == true ? 1 : 0, this.hfBusinessUserEmail.Value, this.txtElectronicSign.Text, "", "", this.txtNotes.Text, this.prepareBusinessUserXml(), Convert.ToDateTime(this.hfLastUpdatedDate.Value), out dt_approved_contract_details);

                if (return_value == -2)
                {
                    if (dt_approved_contract_details.Rows.Count > 0)
                    {
                        Session.Remove(this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "approvedContractAlreadyExists"), dt_approved_contract_details.Rows[0]["electronicSign"].ToString(), dt_approved_contract_details.Rows[0]["email"].ToString()) + "');window.location.href='thankYou.aspx?key=fromUserPage';", true);
                    }
                }
                else if (return_value == -6)
                {
                    Session.Remove(this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "AgreementStatusChanged") + "');window.location.href='thankYou.aspx?key=fromUserPage';", true);
                    return;
                }
                else if (return_value != -1)
                {
                    EmailOperations email_operations = new EmailOperations();
                    DataTable dt_user_emails = dbOperation.getStoreUsersEmails(Convert.ToInt32(this.hfBusinessStoreId.Value));
                    if (dt_user_emails.Rows.Count > 0)
                        email_operations.sendClinicAgreementToWalgreensUser(dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString(), dt_user_emails.Rows[0]["storeManagerEmail"].ToString(), this.hfBusinessName.Value, Convert.ToInt32(hfContactLogPk.Value), "VoteVax");

                    //Sends clinic store re-assignment emails
                    if (return_value == -3)
                    {
                        if (dt_approved_contract_details != null && dt_approved_contract_details.Rows.Count > 0)
                        {
                            //Sends clinic store re-assigned email to contracted store user
                            email_operations.sendClinicStoreAutoGeoCodeReassignmentEmail(dt_user_emails, dt_approved_contract_details,"VoteVax");

                            //Sends clinic assigned email to the assigned store user
                            email_operations.sendLocalClinicStoreAssignmentEmail(dt_approved_contract_details, hfContactLogPk.Value, "VoteVax");
                        }
                    }

                    if (rbtnApprove.Checked)
                    {
                        DataSet ds_clinic_agreement = (DataSet)Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value];
                        if (ds_clinic_agreement.Tables[0].Rows[0]["storeState"].ToString() == "PR")
                        {
                            this.hfThankYouPageReqValues.Value += "PR|";
                        }
                        else if (ds_clinic_agreement.Tables[0].Rows[0]["storeRegion"].ToString() == "35")
                        {
                            this.hfThankYouPageReqValues.Value += "DR|";
                        }
                        else
                        {
                            this.hfThankYouPageReqValues.Value += "OtherState|";
                        }
                        //checking for Corporate to invoice
                        this.xmlContractAgreement.LoadXml(ds_clinic_agreement.Tables[0].Rows[0]["clinicAgreementXml"].ToString());
                        DataSet ds_clinic_details = new DataSet();
                        //Bind immunizations and payment methods
                        StringReader immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Immunizations").OuterXml);
                        ds_clinic_details.ReadXml(immunizations_reader);
                        foreach (DataRow row in ds_clinic_details.Tables[0].Rows)
                        {
                            if (row["paymentTypeName"].ToString() == "Corporate to Invoice Employer Directly" && !hfThankYouPageReqValues.Value.Contains("CorpToInvoice"))
                            {
                                this.hfThankYouPageReqValues.Value += "CorpToInvoice|";
                            }
                            else if ((row["paymentTypeName"].ToString() == "Submit Claims to Pharmacy Insurance" || row["paymentTypeName"].ToString() == "Submit Claims to Medical Insurance") && !hfThankYouPageReqValues.Value.Contains("Insurance"))
                            {
                                this.hfThankYouPageReqValues.Value += "Insurance|";
                            }
                        }
                    }
                    Session.Remove(this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value);
                    Server.Transfer("thankYou.aspx?key=fromUserPage");
                }
            }
        }
    }

    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationPk");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");
            if (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR")
            {
                Label lbl_Immunization = (Label)e.Row.FindControl("lblImmunizationCheck");
                Label lbl_payment_type = (Label)e.Row.FindControl("lblPaymentType");
                lbl_Immunization.Text = ((DataRowView)(e.Row.DataItem)).Row["immunizationSpanishName"].ToString();
                lbl_payment_type.Text = ((DataRowView)(e.Row.DataItem)).Row["paymentTypeSpanishName"].ToString();
            }

            if (Convert.ToInt32(lbl_payment_type_id.Text) == 6)
            {
                System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice_to = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowSendInvoiceTo");
                System.Web.UI.HtmlControls.HtmlTableRow row_voucher_needed = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowVoucherNeeded");
                System.Web.UI.HtmlControls.HtmlTableRow row_voucher_exp_date = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowExpirationDate");
                DropDownList ddl_tax_exempt = (DropDownList)e.Row.FindControl("ddlTaxExempt");
                DropDownList ddl_iscopay = (DropDownList)e.Row.FindControl("ddlIsCopay");
                DropDownList ddl_voucher = (DropDownList)e.Row.FindControl("ddlVoucher");
                var voucher_exp_date = (PickerAndCalendar)e.Row.FindControl("pcVaccineExpirationDate");

                if (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR")
                {
                    List<ListItem> items_voucher = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };
                    List<ListItem> items_iscopayr = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };
                    List<ListItem> items_tax_exempt = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };

                    ddl_voucher.Items.Clear();
                    ddl_voucher.Items.AddRange(items_voucher.ToArray());
                    ddl_iscopay.Items.Clear();
                    ddl_iscopay.Items.AddRange(items_iscopayr.ToArray());
                    ddl_tax_exempt.Items.Clear();
                    ddl_tax_exempt.Items.AddRange(items_tax_exempt.ToArray());
                }

                row_send_invoice_to.Visible = true;
                row_voucher_needed.Visible = true;

                XmlElement immunization_ele = (XmlElement)this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + lbl_immunization_id.Text.Trim() + "' and @paymentTypeId='" + lbl_payment_type_id.Text.Trim() + "']");
                if (ddl_tax_exempt.Items.FindByValue("" + immunization_ele.Attributes["tax"].Value + "") != null)
                    ddl_tax_exempt.Items.FindByValue("" + immunization_ele.Attributes["tax"].Value + "").Selected = true;

                if (ddl_iscopay.Items.FindByValue("" + immunization_ele.Attributes["copay"].Value + "") != null)
                    ddl_iscopay.Items.FindByValue("" + immunization_ele.Attributes["copay"].Value + "").Selected = true;

                if (ddl_voucher.Items.FindByValue("" + immunization_ele.Attributes["isVoucherNeeded"].Value + "") != null)
                    ddl_voucher.Items.FindByValue("" + immunization_ele.Attributes["isVoucherNeeded"].Value + "").Selected = true;

                //Set default max and min voucher expiration dates
                if (!string.IsNullOrEmpty(immunization_ele.Attributes["isVoucherNeeded"].Value) && immunization_ele.Attributes["isVoucherNeeded"].Value == "Yes")
                {
                    ((PickerAndCalendar)voucher_exp_date).getSelectedDate = Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value);
                    ((TextBox)e.Row.FindControl("txtVaccineExpirationDate")).Text = Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value).ToString("MM/dd/yyyy");

                    if (DateTime.Now < Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value))
                        ((PickerAndCalendar)voucher_exp_date).MinDate = DateTime.Now.AddDays(-1);
                    else
                        ((PickerAndCalendar)voucher_exp_date).MinDate = Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value).AddDays(-1);
                }
                else
                {
                    ((PickerAndCalendar)voucher_exp_date).getSelectedDate = (immunization_ele.Attributes["immunizationName"].Value.Contains("Influenza")) ? ApplicationSettings.getVoucherMaxExpDate : this.contractCreatedDate.AddYears(1);
                    ((PickerAndCalendar)voucher_exp_date).MinDate = DateTime.Now.AddDays(-1);
                }

                ((PickerAndCalendar)voucher_exp_date).MaxDate = (immunization_ele.Attributes["immunizationName"].Value.Contains("Influenza")) ? ApplicationSettings.getVoucherMaxExpDate.AddDays(1) : this.contractCreatedDate.AddYears(1).AddDays(1);

                if (ddl_voucher.SelectedValue == "Yes")
                    row_voucher_exp_date.Attributes.CssStyle.Add("display", "block");
                else
                    row_voucher_exp_date.Attributes.CssStyle.Add("display", "none");

               // lbl_price.Text = "$ " + lbl_price.Text;
            }
            //else
            //    lbl_price.Text = (string.IsNullOrEmpty(lbl_price.Text)) ? "N/A" : "$ " + lbl_price.Text;
            lbl_price.Text = "N/A";
        }
    }

    protected void grdLocationsRowdatabound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_clinic_datetime = (Label)e.Row.FindControl("lblClinicDateTime");
            GridView grd_clinic_immunizations = (GridView)e.Row.FindControl("grdClinicImmunizations");

            if (!string.IsNullOrEmpty(lbl_clinic_datetime.Text))
                lbl_clinic_datetime.Text = Convert.ToDateTime(lbl_clinic_datetime.Text).ToString("MM/dd/yyyy");


            this.bindClinicImmunizations(grd_clinic_immunizations, e.Row.RowIndex);
        }
    }

    protected void grdClinicImmunizations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string immunization_key = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "immunizationSpanishName" : "immunizationName";
            string payment_type_key = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "paymentTypeSpanishName" : "paymentTypeName";
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationId");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");
            Label lbl_clinic_immunization = (Label)e.Row.FindControl("lblClinicImmunization");
            Label lbl_clinic_payment_type = (Label)e.Row.FindControl("lblClinicPaymentType");

            XmlElement immunization_ele = (XmlElement)this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + lbl_immunization_id.Text.Trim() + "' and @paymentTypeId='" + lbl_payment_type_id.Text.Trim() + "']");
            lbl_clinic_immunization.Text = immunization_ele.Attributes[immunization_key].Value;
            lbl_clinic_payment_type.Text = immunization_ele.Attributes[payment_type_key].Value;
        }
    }

    protected void ValidateData(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = true;
        string value = string.Empty;
        // txtClient
        value = txtClient.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClient"), "textbox", "Enter a Business Name for the Client field.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClient"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtClient.Style.Add("border", "1px solid gray");
            txtClient.ToolTip = "";
        }
        //txtClientName
        value = txtClientName.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientName"), "textbox", "Enter a first and last name in the Client's Name field.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientName"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtClientName.Style.Add("border", "1px solid gray");
            txtClientName.ToolTip = "";
        }
        //txtClientTitle
        value = txtClientTitle.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientTitle"), "textbox", "Enter the Title for the Client's representative named in the Name field.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientTitle"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtClientTitle.Style.Add("border", "1px solid gray");
            txtClientTitle.ToolTip = "";
        }
        //txtAttentionTo
        value = txtAttentionTo.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtAttentionTo"), "textbox", "Enter value in the Attention To field of the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtAttentionTo"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtAttentionTo.Style.Add("border", "1px solid gray");
            txtAttentionTo.ToolTip = "";
        }
        //txtLegalAddress1
        value = txtLegalAddress1.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalAddress1"), "textbox", "Enter a street address in the Address1 field of the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalAddress1"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtLegalAddress1.Style.Add("border", "1px solid gray");
            txtLegalAddress1.ToolTip = "";
        }
        //txtLegalAddress2
        value = txtLegalAddress2.Text;
        if (!string.IsNullOrEmpty(value))
        {
            if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtLegalAddress2"), "textbox", "Invalid Data");
                e.IsValid = false;
            }
            else
            {
                txtLegalAddress2.Style.Add("border", "1px solid gray");
                txtLegalAddress2.ToolTip = "";
            }
        }
        //txtLegalCity
        value = txtLegalCity.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalCity"), "textbox", "Enter a City in the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalCity"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtLegalCity.Style.Add("border", "1px solid gray");
            txtLegalCity.ToolTip = "";
        }
        //ddlLegalState
        value = ddlLegalState.SelectedValue;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("ddlLegalState"), "ddl", "Select a State in the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else
        {
            ddlLegalState.Style.Add("border", "1px solid gray");
            ddlLegalState.ToolTip = "";
        }
        //txtLegalZip
        value = txtLegalZip.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalZip"), "textbox", "Enter a 5 digit Zip Code in the Send Legal Notices To Client at section. (ex:#####).");
            e.IsValid = false;
        }
        else if (!value.validateZipCode() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalZip"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtLegalZip.Style.Add("border", "1px solid gray");
            txtLegalZip.ToolTip = "";
        }
        //txtElectronicSign
        if (rbtnApprove.Checked)
        {
            value = txtElectronicSign.Text;
            if (string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtElectronicSign"), "textbox", "Your Electronic Signature is required for Approval");
                e.IsValid = false;
            }
            else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtElectronicSign"), "textbox", "Invalid Data");
                e.IsValid = false;
            }
            else
            {
                txtElectronicSign.Style.Add("border", "1px solid gray");
                txtElectronicSign.ToolTip = "";
            }
        }
        else
        {
            txtElectronicSign.Style.Add("border", "1px solid gray");
            txtElectronicSign.ToolTip = "";
        }
        //txtNotes
        if (rbtnReject.Checked)
        {
            value = txtNotes.Text;
            if (string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtNotes"), "textbox", "Please enter Notes.");
                e.IsValid = false;
            }
            else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtNotes"), "textbox", "Invalid Data");
                e.IsValid = false;
            }
            else
            {
                txtNotes.Style.Add("border", "1px solid gray");
                txtNotes.ToolTip = "";
            }
        }
        else
        {
            txtNotes.Style.Add("border", "1px solid gray");
            txtNotes.ToolTip = "";
        }
    }

    protected void lnkChangeCulture_Click(object sender, EventArgs e)
    {
        if (Request.QueryString.Count == 0 || Request.QueryString == null || Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value] == null)
        {
            Response.Redirect("walgreensNoAccess.htm");
        }
        else
        {
            DataSet ds_contract_agreement = (DataSet)Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value];
            ds_contract_agreement.Tables[0].Rows[0]["clinicAgreementXml"] = this.prepareBusinessUserXml();
            Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value] = ds_contract_agreement;
            bindContractAgreement();
        }
    }
    #endregion

    #region -------------------- PRIVATE METHODS --------------
    /// <summary>
    /// To set control border red
    /// </summary>
    /// <param name="control"></param>
    /// <param name="cont_type"></param>
    /// <param name="tool_tip"></param>
    private void setControlProperty(Control control, string cont_type, string tool_tip)
    {
        switch (cont_type)
        {
            case "textbox":
                ((TextBox)control).Style.Add("border", "1px solid red");
                ((TextBox)control).ToolTip = tool_tip;
                break;
            case "ddl":
                ((DropDownList)control).Style.Add("border", "1px solid red");
                ((DropDownList)control).ToolTip = tool_tip;
                break;
        }
    }
    /// <summary>
    /// Gets and binds contract agreement for business user
    /// </summary>
    private void bindContractAgreement()
    {
        if (Request.QueryString["args"] != null && Request.QueryString.Count > 0)
        {
            EncryptedQueryString args = new EncryptedQueryString(Request.QueryString["args"]);
            if (args.Count() == 2)
            {
                this.hfBusinessUserEmail.Value = args["arg1"].ToString();
                this.hfContactLogPk.Value = args["arg2"].ToString();
				this.lblAgreementTitle.Text = "Walgreens Vote & Vax Community Off-Site Agreement";
                DataSet ds_contract_agreement = new DataSet();

                if (Session[args["arg1"].ToString() + args["arg2"].ToString()] != null)
                    ds_contract_agreement = (DataSet)Session[args["arg1"].ToString() + args["arg2"].ToString()];
                else
                    ds_contract_agreement = this.dbOperation.getClinicAgreement("Business", args["arg2"].ToString(), args["arg1"].ToString(), 2015, false);

                if ((ds_contract_agreement == null) || (ds_contract_agreement != null && ds_contract_agreement.Tables.Count == 0))
                    Response.Redirect("walgreensNoAccess.htm");
                else
                    this.dtContractAgreement = ds_contract_agreement.Tables[0];

                if (this.dtContractAgreement != null && this.dtContractAgreement.Rows.Count > 0)
                {
                    this.xmlContractAgreement.LoadXml(this.dtContractAgreement.Rows[0]["clinicAgreementXml"].ToString());

                    //Update immunization price in the contract agreements created before price update and not yet approved
                    if (this.dtContractAgreement.Rows[0]["isApproved"].ToString().Trim().Length == 0 || (this.dtContractAgreement.Rows[0]["isApproved"].ToString().Trim().Length != 0 && !((bool)this.dtContractAgreement.Rows[0]["isApproved"])))
                    {
                        DataTable dt_immunizations = this.dbOperation.getImmunizationCheck(2016, "Vote & Vax");

                        if (this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization") != null)
                        {
                            XmlNodeList immunization_checks = this.xmlContractAgreement.SelectNodes("//Immunizations/Immunization");
                            foreach (XmlNode immunization_check in immunization_checks)
                            {
                                DataRow[] dr_immunization_check = dt_immunizations.Select("immunizationId = '" + immunization_check.Attributes["pk"].Value + "' AND paymentTypeId = '" + immunization_check.Attributes["paymentTypeId"].Value + "'");
                                immunization_check.Attributes["price"].Value = (immunization_check.Attributes["paymentTypeId"].Value == "6") ? dr_immunization_check[0]["directBillPrice"].ToString() : dr_immunization_check[0]["price"].ToString();
                            }

                            this.dtContractAgreement.Rows[0]["clinicAgreementXml"] = this.xmlContractAgreement.OuterXml;
                        }
                    }

                    Session[args["arg1"].ToString() + args["arg2"].ToString()] = ds_contract_agreement;

                    this.txtElectronicSign.Text = this.dtContractAgreement.Rows[0]["signature"].ToString();
                    this.hfBusinessStoreId.Value = this.dtContractAgreement.Rows[0]["storeId"].ToString();
                    this.hfBusinessName.Value = this.dtContractAgreement.Rows[0]["businessName"].ToString();
                    this.contractCreatedDate = Convert.ToDateTime(this.dtContractAgreement.Rows[0]["dateCreated"].ToString());
                    this.hfLastUpdatedDate.Value = this.dtContractAgreement.Rows[0]["updatedDate"].ToString();
                    this.hfStoreState.Value = this.dtContractAgreement.Rows[0]["storeState"].ToString();
                    if (hfStoreState.Value == "PR" && hfLanguge.Value == "es-MX")
                        hfLanguge.Value = "es-PR";

                    //Bind Client details
                    this.txtClient.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@header").Value.ToString();
                    this.txtClientName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@name").Value.ToString();
                    this.txtClientTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@title").Value.ToString();
                    //this.lblClientDate.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString();
                    //if (!string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value))
                        //this.lblClientDate.Text = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value).ToString("MM/dd/yyyy");
                    this.lblClientDate.Text = DateTime.Today.ToString("MM/dd/yyyy");

                    //Binds Walgreen CO. details
                    this.lblWalgreenName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@name").Value.ToString();
                    this.lblWalgreenTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@title").Value.ToString();
                    if (!string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value))
                        this.lblWalgreenDate.Text = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value).ToString("MM/dd/yyyy");
                    this.lblDistrictNum.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@districtNumber").Value.ToString();

                    //Binds Legal Notice details
                    this.txtAttentionTo.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@attentionTo").Value.ToString();
                    this.txtLegalAddress1.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address1").Value.ToString();
                    this.txtLegalAddress2.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address2").Value.ToString();
                    this.txtLegalCity.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@city").Value.ToString();
                    this.ddlLegalState.ClearSelection();
                    if (this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString().Trim().Length != 0)
                        this.ddlLegalState.Items.FindByValue(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString()).Selected = true;
                    this.txtLegalZip.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@zipCode").Value.ToString();

                    DataSet ds_clinic_details = new DataSet();
                    //Bind immunizations and payment methods
                    StringReader immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Immunizations").OuterXml);
                    ds_clinic_details.ReadXml(immunizations_reader);
                    if (ds_clinic_details != null && ds_clinic_details.Tables.Count > 0)
                    {
                        this.grdImmunizationChecks.DataSource = ds_clinic_details.Tables[0];
                        this.grdImmunizationChecks.DataBind();
                    }
                    else
                    {
                        Session.Remove(args["arg1"].ToString() + args["arg2"].ToString());
                        Response.Redirect("walgreensNoAccess.htm");
                    }

                    //Bind clinic locations
                    StringReader clinics_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").OuterXml);
                    ds_clinic_details = new DataSet();
                    ds_clinic_details.ReadXml(clinics_reader);
                    if (ds_clinic_details.Tables.Count > 0 && (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR"))
                    {
                        foreach (DataRow row in ds_clinic_details.Tables[0].Rows)
                        {
                            row["ClinicLocation"] = row["ClinicLocation"].ToString().Replace("CLINIC LOCATION", "UBICACIÓN DE LA CLÍNICA");
                        }
                    }

                    this.grdLocations.DataSource = ds_clinic_details.Tables[0];
                    this.grdLocations.DataBind();

                    if (this.dtContractAgreement.Rows[0]["approvedOrRejectedBy"] != DBNull.Value)
                    {
                        this.lblErrorMessage.Text = "The Walgreens Community Off-Site Agreement has already been submitted by " + this.dtContractAgreement.Rows[0]["approvedOrRejectedBy"].ToString();
                    }
                    else
                    {
                        if (ds_contract_agreement.Tables[1] != null)
                        {
                            DataTable dt_approved_contract_details = ds_contract_agreement.Tables[1];
                            if (dt_approved_contract_details != null && dt_approved_contract_details.Rows.Count > 0)
                            {
                                this.lblErrorMessage.Text = String.Format((string)GetGlobalResourceObject("errorMessages", "approvedContractAlreadyExists"), dt_approved_contract_details.Rows[0]["electronicSign"].ToString(), dt_approved_contract_details.Rows[0]["email"].ToString());
                                this.disableBusinessUserControls();
                            }
                        }
                    }

                    if (this.dtContractAgreement.Rows[0]["isApproved"].ToString().Trim().Length != 0)
                    {
                        if (!((bool)this.dtContractAgreement.Rows[0]["isApproved"]))
                        {
                            this.rbtnReject.Checked = true;
                            this.trNotes.Attributes.Add("style", "display:block;");
                            this.txtNotes.Text = this.dtContractAgreement.Rows[0]["notes"].ToString();
                        }
                        else
                        {
                            this.rbtnApprove.Checked = true;
                        }
                        this.disableBusinessUserControls();
                    }
                    this.tblApproval.Visible = true;

                    this.disableControls(true);
                }
                else
                    Response.Redirect("walgreensNoAccess.htm");
            }
            else
                Response.Redirect("walgreensNoAccess.htm");
        }
        else
            Response.Redirect("walgreensNoAccess.htm");
    }

    /// <summary>
    /// Binds immunizations to clinic location grid
    /// </summary>
    /// <param name="grd_clinic_immunizations"></param>
    /// <param name="clinic_number"></param>
    private void bindClinicImmunizations(GridView grd_clinic_immunizations, int clinic_number)
    {
        StringReader clinic_immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").ChildNodes[clinic_number].OuterXml);
        DataSet ds_clinic_immunizations = new DataSet();
        ds_clinic_immunizations.ReadXml(clinic_immunizations_reader);

        if (ds_clinic_immunizations.Tables.Count > 1 && ds_clinic_immunizations.Tables[1].Rows.Count > 0)
        {
            grd_clinic_immunizations.DataSource = ds_clinic_immunizations.Tables[1];
            grd_clinic_immunizations.DataBind();
        }
    }

    /// <summary>
    /// Creates Contract Agreement XML document with business user information
    /// </summary>
    /// <returns></returns>
    private string prepareBusinessUserXml()
    {
        DataSet ds_clinic_agreement = (DataSet)Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value];

        this.xmlContractAgreement.InnerXml = ds_clinic_agreement.Tables[0].Rows[0]["clinicAgreementXml"].ToString();

        //Set clinic location lats and longs        
        foreach (GridViewRow row in grdLocations.Rows)
        {
            string clinic_location = ((Label)row.FindControl("lblClinicLocation")).Text.Trim().Contains("UBICACIÓN DE LA CLÍNICA") ? ((Label)row.FindControl("lblClinicLocation")).Text.Trim().Replace("UBICACIÓN DE LA CLÍNICA", "CLINIC LOCATION") : ((Label)row.FindControl("lblClinicLocation")).Text.Trim();
            XmlNode business_node = this.xmlContractAgreement.SelectSingleNode("//Clinics/clinic[@clinicLocation='" + clinic_location + "']");
            ((XmlElement)business_node).SetAttribute("naClinicLatitude", ((HiddenField)row.FindControl("hfClinicLatitude")).Value.Trim());
            ((XmlElement)business_node).SetAttribute("naClinicLongitude", ((HiddenField)row.FindControl("hfClinicLongitude")).Value.Trim());
        }

        //Update voucher expiration date if changed by user
        string voucher_needed = string.Empty;
        string is_copay = string.Empty;
        string txtCoPay = string.Empty;
        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            voucher_needed = ((DropDownList)row.FindControl("ddlVoucher")).SelectedItem.Value;
            if (voucher_needed == "Yes")
            {
                if (((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                    this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + ((Label)row.FindControl("lblImmunizationPk")).Text.Trim() + "' and @paymentTypeId='" + ((Label)row.FindControl("lblPaymentTypeId")).Text.Trim() + "']").Attributes["voucherExpirationDate"].Value = ((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy");
            }
            is_copay = ((DropDownList)row.FindControl("ddlIsCopay")).SelectedItem.Value;
            txtCoPay = ((TextBox)row.FindControl("txtCoPay")).Text;
            if (is_copay == "" || is_copay == "Select" || is_copay == "0")
                this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + ((Label)row.FindControl("lblImmunizationPk")).Text.Trim() + "' and @paymentTypeId='" + ((Label)row.FindControl("lblPaymentTypeId")).Text.Trim() + "']").Attributes["copay"].Value = "No";
            if (is_copay == "Yes" && string.IsNullOrEmpty(txtCoPay))
                this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + ((Label)row.FindControl("lblImmunizationPk")).Text.Trim() + "' and @paymentTypeId='" + ((Label)row.FindControl("lblPaymentTypeId")).Text.Trim() + "']").Attributes["copay"].Value = "No";

        }

        XmlNode client_node = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client");
        ((XmlElement)client_node).SetAttribute("header", this.txtClient.Text.Trim());
        ((XmlElement)client_node).SetAttribute("name", this.txtClientName.Text.Trim());
        ((XmlElement)client_node).SetAttribute("title", this.txtClientTitle.Text.Trim());
        ((XmlElement)client_node).SetAttribute("date", DateTime.Now.ToString("MM/dd/yyyy"));

        XmlNode legal_notic_address = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress");
        ((XmlElement)legal_notic_address).SetAttribute("attentionTo", this.txtAttentionTo.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("address1", this.txtLegalAddress1.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("address2", this.txtLegalAddress2.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("city", this.txtLegalCity.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("state", this.ddlLegalState.SelectedValue.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("zipCode", this.txtLegalZip.Text.Trim());

        return this.xmlContractAgreement.OuterXml;
    }

    /// <summary>
    /// Disables Business user controls for approved contracts
    /// </summary>
    private void disableBusinessUserControls()
    {
        this.tblApproval.Disabled = true;
        this.trNotes.Disabled = true;
        this.txtElectronicSign.Enabled = false;
        this.txtNotes.Enabled = false;
        this.btnSubmit.Visible = false;
        this.txtClient.Enabled = false;
        this.txtClientName.Enabled = false;
        this.txtClientTitle.Enabled = false;
        this.txtAttentionTo.Enabled = false;
        this.txtLegalAddress1.Enabled = false;
        this.txtLegalAddress2.Enabled = false;
        this.txtLegalCity.Enabled = false;
        this.ddlLegalState.Enabled = false;
        this.txtLegalZip.Enabled = false;
    }

    /// <summary>
    /// Disables controls
    /// </summary>
    /// <param name="disable"></param>
    private void disableControls(bool disable)
    {
        this.disableGridControls(disable, this.grdLocations);
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            GridView grd_view = (GridView)row.FindControl("grdClinicImmunizations");
            this.disableGridControls(disable, grd_view);
        }

        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice = (System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowSendInvoiceTo");
            this.disableCommunityAgreement(disable, row_send_invoice.Controls);
            System.Web.UI.HtmlControls.HtmlTableRow row_voucher_needed = (System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowVoucherNeeded");
            this.disableCommunityAgreement(disable, row_voucher_needed.Controls);
        }
    }

    /// <summary>
    /// Disables grid controls
    /// </summary>
    /// <param name="disable"></param>
    /// <param name="grd_ctrl"></param>
    private void disableGridControls(bool disable, GridView grd_ctrl)
    {
        foreach (GridViewRow row in grd_ctrl.Rows)
        {
            foreach (Control ctrl in row.Controls)
            {
                foreach (Control ctrl1 in ctrl.Controls)
                {
                    if (ctrl1 is TextBox)
                        ((TextBox)ctrl1).Enabled = !disable;

                    if (ctrl1 is DropDownList)
                        ((DropDownList)ctrl1).Enabled = !disable;

                    if (ctrl1 is CheckBox)
                        ((CheckBox)ctrl1).Enabled = !disable;
                }
            }
        }
    }

    /// <summary>
    /// Disables all input controls in the page
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableCommunityAgreement(bool disable, ControlCollection ctrl_main)
    {
        foreach (Control page_ctrl in ctrl_main)
        {
            foreach (Control ctrl1 in page_ctrl.Controls)
            {
                if (ctrl1 is TextBox)
                    ((TextBox)ctrl1).Enabled = !disable;

                if (ctrl1 is RadioButton)
                    ((RadioButton)ctrl1).Enabled = !disable;

                if (ctrl1 is DropDownList)
                    ((DropDownList)ctrl1).Enabled = !disable;

                if (ctrl1 is PickerAndCalendar)
                    ((PickerAndCalendar)ctrl1).Visible = false;

                foreach (Control ctrl in ctrl1.Controls)
                {
                    if (page_ctrl is System.Web.UI.HtmlControls.HtmlTableCell)
                    {
                        if (ctrl1 is TextBox)
                            ((TextBox)ctrl1).Enabled = !disable;

                        if (ctrl1 is DropDownList)
                            ((DropDownList)ctrl1).Enabled = !disable;

                        if (ctrl1 is CheckBox)
                            ((CheckBox)ctrl1).Enabled = !disable;

                        if (ctrl1 is ImageButton)
                            ((ImageButton)ctrl1).Visible = !disable;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Setting image urls based on culture
    /// </summary>
    private void setImageButtons()
    {
        if (!String.IsNullOrEmpty(hfLanguge.Value))
        {
            btnSubmit.ImageUrl = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "images/btn_submit_SP.png" : "images/btn_submit_event.png";

            if (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR")
            {
                btnSubmit.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_submit_SP.png');");
                btnSubmit.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_submit_SP_lit.png');");
                btnSubmit.AlternateText = "Enviar Correo Electrónico";
            }
            else
            {
                btnSubmit.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_submit_event.png');");
                btnSubmit.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_submit_event_lit.png');");
                btnSubmit.AlternateText = "Send email";
            }
        }
    }
    #endregion

    #region ---------------- PRIVATE VARIABLES ----------------
    private DBOperations dbOperation = null;
    private DataTable dtContractAgreement = null;
    private XmlDocument xmlContractAgreement;
    private WalgreenEmail walgreensEmail = null;
    private DateTime contractCreatedDate;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        //ApplicationSettings.validateSession();
        this.dbOperation = new DBOperations();
        this.xmlContractAgreement = new XmlDocument();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        contractCreatedDate = DateTime.Now;
    }

    protected override void InitializeCulture()
    {
        if (Request.Form["hfLanguge"] != null)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["hfLanguge"]);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["hfLanguge"]);
            Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo("en-US").DateTimeFormat;
        }
        base.InitializeCulture();
    }
}