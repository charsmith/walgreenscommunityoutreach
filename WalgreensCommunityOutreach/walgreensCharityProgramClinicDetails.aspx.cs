﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using TdApplicationLib;
using tdEmailLib;
using TdWalgreens;
using System.Linq;
using System.Web;

public partial class walgreensCharityProgramClinicDetails : System.Web.UI.Page
{
    #region --------------- PROTECTED EVENTS -----------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserName))
        {
            //Enable store assignment access to Admin, DM, HCS, DPR only
            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "admin":
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                    this.isStoreEditable = true;
                    break;
                default: this.isStoreEditable = false;
                    break;
            }

            if (this.isStoreEditable)
                this.txtDefaultClinicStoreId.Enabled = true;
        }

        if (!Page.IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk > 0)
            {
                this.hfBusinessClinicPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk.ToString();
                this.hfBusinessStoreId.Value = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.txtDefaultClinicStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.isDisableEditClinicDetails = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails");

                this.bindClinicLocationDetails();
                this.controlAccess();
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
            }
            else
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                Response.Redirect("walgreensHome.aspx");
            }
        }
        else
        {
            string event_args = Request["__EVENTTARGET"];
            if (event_args.ToLower() == "maintaincontactstatus")
            {
                bool maintain_log = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                if (!maintain_log)
                {
                    int business_clinic_pk;
                    Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);

                    if (business_clinic_pk > 0)
                        this.dbOperation.removeClinicContactLogs(business_clinic_pk, "Charity");
                }

                Session.Remove(this.hfBusinessClinicPk.Value);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
            else if (event_args.ToLower() == "clinicdetailsupdated")
            {
                bool save_changes = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                if (save_changes)
                {
                    this.updatedAction = "Submit";
                    if (ValidateLocations())
                    {
                        this.doProcess(true, true);
                        if (!this.showAlertMessage)
                        {
                            Session.Remove(this.hfBusinessClinicPk.Value);
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
                        return;
                    }
                }
                else
                {
                    Session.Remove(this.hfBusinessClinicPk.Value);
                    Response.Redirect("walgreensHome.aspx");
                }
            }
            else if (event_args.ToLower() == "deletecharityprogram")
            {
                int business_clinic_pk;
                int return_value = 0;
                Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);

                if (business_clinic_pk > 0)
                {
                    return_value = this.dbOperation.deleteAgreement(business_clinic_pk, this.commonAppSession.LoginUserInfoSession.UserID, "Charity");
                    if (return_value == 0)
                    {
                        this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;

                        //Send delete charity program notification email to admin users
                        string to_email = string.Empty;
                        to_email += ApplicationSettings.getEmailInfoFromGroup("deleteCharityProgramNotificationGroup", true).Trim();
                        this.walgreensEmail.sendCommunityOffsiteAgreementDeletedNotificationEmail("Admin", this.commonAppSession.LoginUserInfoSession.UserName, "Clinic " + ((Label)this.grdLocations.Rows[0].FindControl("lblClinicLocation")).Text, Server.MapPath("~/emailTemplates/charityProgramDeletionTemplate.htm"), String.Format((string)GetGlobalResourceObject("errorMessages", "deleteCharityProgramNotifyEmailSubject"), this.lblClientName.Text), this.hfBusinessStoreId.Value, this.lblClientName.Text, this.hfBusinessStoreId.Value, (string)GetGlobalResourceObject("errorMessages", "nlaThankYouLine"), (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : to_email), "", true);

                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "deletedCharityProgram") + "'); window.location.href = 'walgreensHome.aspx';", true);
                    }
                }
            }
            else if (event_args.ToLower().Contains("continuesaving"))
            {
                string save_clinic = Request["__EVENTARGUMENT"].ToString();
                if (save_clinic == "1")
                {
                    this.updatedAction = event_args.ToLower().Split(',')[1];
                    this.doProcess(false, false);
                }
                else if (save_clinic == "isValid")
                {
                    this.updatedAction = event_args.ToLower().Split(',')[1];
                    this.doProcess(false, false, true);
                }
                else
                {
                    this.prepareClinicDetailsXml(-1);
                    this.bindClinicLocations();
                }
            }
            else if (event_args.ToLower().Contains("continuesubmitting"))
            {
                this.updatedAction = event_args.ToLower().Split(',')[1];
                this.doProcess(false, false, true, true);
            }
            this.setMinMaxDates();
        }

        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        this.prepareClinicDetailsXml(-1);

        XmlNode clinic_locations = this.xmlClinicDetails.SelectSingleNode("//clinicLocations");
        XmlElement clinic_location = this.xmlClinicDetails.CreateElement("clinicLocation");

        int max_clinic_number = 0;
        double locations_count = 0;

        Int32.TryParse(this.hdMaxClinicNumber.Value, out max_clinic_number);
        locations_count = max_clinic_number + (this.grdLocations.Rows.Count - 1);

        clinic_location.SetAttribute("naClinicLocation", ((locations_count >= 26) ? ((Char)(65 + (locations_count % 26 == 0 ? Math.Ceiling(locations_count / 26) - 1 : Math.Ceiling(locations_count / 26) - 2))).ToString() + "" + ((Char)(65 + locations_count % 26)).ToString() : ((Char)(65 + locations_count % 26)).ToString()));
        clinic_location.SetAttribute("naContactFirstName", string.Empty);
        clinic_location.SetAttribute("naContactLastName", string.Empty);
        clinic_location.SetAttribute("naClinicContactPhone", string.Empty);
        clinic_location.SetAttribute("naContactEmail", string.Empty);
        if (this.isRestrictedStoreState)
        {
            //DataTable dt_business_contact = dbOperation.getBusinessContactDetails(Convert.ToInt32(this.hfContactLogPk.Value));
            clinic_location.SetAttribute("naClinicAddress1", (this.businessLocationAddress.ContainsKey("naClinicAddress1")) ? this.businessLocationAddress["naClinicAddress1"] : string.Empty);
            clinic_location.SetAttribute("naClinicAddress2", (this.businessLocationAddress.ContainsKey("naClinicAddress2")) ? this.businessLocationAddress["naClinicAddress2"] : string.Empty);
            clinic_location.SetAttribute("naClinicCity", (this.businessLocationAddress.ContainsKey("naClinicCity")) ? this.businessLocationAddress["naClinicCity"] : string.Empty);
            clinic_location.SetAttribute("naClinicState", (this.businessLocationAddress.ContainsKey("naClinicState")) ? this.businessLocationAddress["naClinicState"] : string.Empty);
            clinic_location.SetAttribute("naClinicZip", (this.businessLocationAddress.ContainsKey("naClinicZip")) ? this.businessLocationAddress["naClinicZip"] : string.Empty);
            this.isAddressDisabled = true;
        }
        else
        {
            clinic_location.SetAttribute("naClinicAddress1", string.Empty);
            clinic_location.SetAttribute("naClinicAddress2", string.Empty);
            clinic_location.SetAttribute("naClinicCity", string.Empty);
            clinic_location.SetAttribute("naClinicState", string.Empty);
            clinic_location.SetAttribute("naClinicZip", string.Empty);
        }
        clinic_location.SetAttribute("clinicDate", string.Empty);
        clinic_location.SetAttribute("naClinicStartTime", string.Empty);
        clinic_location.SetAttribute("naClinicEndTime", string.Empty);
        clinic_location.SetAttribute("clinicScheduledOn", string.Empty);
        clinic_location.SetAttribute("naClinicEstimatedVolume", string.Empty);
        clinic_location.SetAttribute("naClinicVouchersDist", string.Empty);
        clinic_location.SetAttribute("naClinicTotalImmAdministered", string.Empty);
        clinic_location.SetAttribute("clinicStoreId", this.hfBusinessStoreId.Value);
        clinic_location.SetAttribute("confirmedClientName", string.Empty);
        clinic_location.SetAttribute("isNoClinic", "0");
        clinic_locations.AppendChild(clinic_location);

        this.bindClinicLocations();
    }

    protected void imgBtnRemoveLocation_Click(object sender, EventArgs e)
    {
        ImageButton img_btn_remove = (ImageButton)sender;
        GridViewRow grd_row = (GridViewRow)img_btn_remove.NamingContainer;

        this.prepareClinicDetailsXml(grd_row.RowIndex);
        this.bindClinicLocations();
    }

    protected void grdLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_add_location = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowAddClinicLocation");
            CheckBox chk_no_clinic = (CheckBox)e.Row.FindControl("chkNoClinic");

            DropDownList ddl_states = (DropDownList)e.Row.FindControl("ddlState");
            string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate");
            string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate");
            if (Convert.ToDateTime(restriction_start_date) < DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
            {
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString(), ApplicationSettings.getStoreStateRestrictions("storeState"));
            }
            else
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString(), "");
            if (ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString()) != null)
                ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString()).Selected = true;

            string clinic_date = "";
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
                clinic_date = grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString();

            if (e.Row.RowIndex == 0)
            {
                ImageButton remove_clinic = (ImageButton)e.Row.FindControl("imgBtnRemoveLocation");
                System.Web.UI.HtmlControls.HtmlTableRow row_estimated_shots = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowEstimatedShots");
                System.Web.UI.HtmlControls.HtmlTable tbl_voucher_distributed = (System.Web.UI.HtmlControls.HtmlTable)e.Row.FindControl("tblVouchersDistributed");

                remove_clinic.Visible = false;
                tbl_voucher_distributed.Visible = false;
                row_estimated_shots.Visible = false;

                if (this.isDisableEditClinicDetails)
                {
                    row_add_location.Visible = false;
                    ddl_states.Enabled = false;
                }

                //Show clinic date reminder above Billing & Vaccine Information for default clinic location
                DateTime clinic_scheduled_on = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicScheduledOn"].ToString());
                if (!chk_no_clinic.Checked && clinic_date.Length > 0 && Convert.ToDateTime(clinic_date).Date < clinic_scheduled_on.Date.AddDays(14))
                {
                    this.lblClinicDateAlert.Text = (string)GetGlobalResourceObject("errorMessages", "clinicApprovedBefore2Weeks");
                    this.lblClinicDateAlert.Visible = true;
                }
                else
                    this.lblClinicDateAlert.Visible = false;
            }
            else
            {
                ImageButton add_clinic = (ImageButton)e.Row.FindControl("imgBtnAddLocation");
                row_add_location.Visible = false;
                add_clinic.Visible = false;

                //Displays store assignment to specific users only
                if (this.isStoreEditable)
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row_store_assignment = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowStoreAssignment");
                    row_store_assignment.Visible = true;
                }

                if (!chk_no_clinic.Checked && clinic_date.Length > 0 && Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14))
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row_clinic_date_alert = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowClinicDateAlert");
                    Label lbl_clinic_date_alert = (Label)e.Row.FindControl("lblClinicDateAlert");
                    row_clinic_date_alert.Visible = true;
                    lbl_clinic_date_alert.Text = (string)GetGlobalResourceObject("errorMessages", "clinicCreatedBefore2Weeks");
                }
            }

            this.setClinicDates(e.Row, true);

            if (isAddressDisabled)
            {
                ((TextBox)e.Row.FindControl("txtAddress1")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtAddress2")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtCity")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtZipCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlState")).Enabled = false;
            }
        }
    }

    protected void grdClinicUpdatesHistory_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_update_action = (Label)e.Row.FindControl("lblAction");
            if (lbl_update_action.Text.ToLower() == "updated")
            {
                System.Web.UI.HtmlControls.HtmlTableCell update_field = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateField");
                update_field.ColSpan = 2;
                System.Web.UI.HtmlControls.HtmlTableCell update_value = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateValue");
                update_value.Visible = false;
            }
        }
    }

    protected bool ValidateLocations()
    {
        bool is_page_valid = true;

        is_page_valid = txtFirstContactName.validateControls("textbox", "string", false, "Contact First Name is required", "Contact First Name: , < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = txtLastContactName.validateControls("textbox", "string", false, "Contact Last Name is required", "Contact Last Name: , < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = txtContactJobTitle.validateControls("textbox", "string", true, "", "Contact Job Title: , < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = txtPlanId.validateControls("textbox", "string", false, "Plan ID is required", "Plan ID: < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = txtGroupId.validateControls("textbox", "string", false, "Group ID is required", "Group ID: < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = txtIdRecipient.validateControls("textbox", "string", false, "ID Recipient is required", "ID Recipient: < > characters are not allowed", this.Page) && is_page_valid;
        //Validate clinic locations data on server side
        foreach (GridViewRow row in grdLocations.Rows)
        {
            bool allow_empty = ((CheckBox)row.FindControl("chkNoClinic")).Checked;
            if (row.RowIndex != 0)
            {
                if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtVouchersDistributed")).Text))
                {
                    is_page_valid = ((TextBox)row.FindControl("txtVouchersDistributed")).validateControls("textbox", "number", false, "Please enter the number of Vouchers Distributed", "Please enter the valid number of Vouchers Distributed", this.Page) && is_page_valid;
                    is_page_valid = ((TextBox)row.FindControl("txtEstShots")).validateControls("textbox", "zero", allow_empty, "Estimated Shots is required (ex: #####)", "Please enter valid Estimated Shots (ex: #####)", this.Page) && is_page_valid;
                    is_page_valid = ((TextBox)row.FindControl("txtTotalImm")).validateControls("textbox", "zero", true, "Total Immunizations Administered is required", "Please enter valid number of Total Immunizations Administered (ex: #####)", this.Page) && is_page_valid;
                }
                else
                    is_page_valid = ((TextBox)row.FindControl("txtVouchersDistributed")).validateControls("textbox", "number", false, "Please enter the number of Vouchers Distributed", "Please enter the valid number of Vouchers Distributed", this.Page) && is_page_valid;
            }
            else
            {
                is_page_valid = this.txtDefaultVouchersDistributed.validateControls("textbox", "number", false, "Please enter the number of Vouchers Distributed", "Please enter the valid number of Vouchers Distributed", this.Page) && is_page_valid;
                is_page_valid = this.txtDefaultEstShots.validateControls("textbox", "zero", allow_empty, "Estimated Shots is required (ex: #####)", "Please enter valid Estimated Shots (ex: #####)", this.Page) && is_page_valid;
                is_page_valid = this.txtDefaultTotalImm.validateControls("textbox", "zero", true, "Total Immunizations Administered is required", "Please enter valid number of Total Immunizations Administered (ex: #####)", this.Page) && is_page_valid;
                is_page_valid = this.txtDefaultClinicStoreId.validateControls("textbox", "number", false, "Store Id is required", "Please enter valid Store Id", this.Page) && is_page_valid;
            }

            is_page_valid = ((TextBox)row.FindControl("txtContactFirstName")).validateControls("textbox", "string", allow_empty, "Contact First Name is required", "Contact First Name: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtContactLastName")).validateControls("textbox", "string", allow_empty, "Contact Last Name is required", "Contact Last Name: < > characters are not allowed", this.Page) && is_page_valid;

            is_page_valid = ((TextBox)row.FindControl("txtAddress1")).validateControls("textbox", "address", !(this.isStoreState) && allow_empty, "Address1 is required", "Clinic Address1: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtAddress2")).validateControls("textbox", "address", true, "", "Clinic Address2: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtCity")).validateControls("textbox", "string", !(this.isStoreState) && allow_empty, "City is required", "Clinic City: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtZipCode")).validateControls("textbox", "zip", !(this.isStoreState) && allow_empty, "Zip Code is required", "Please enter a valid Zip Code (ex: #####)", this.Page) && is_page_valid;

            is_page_valid = ((TextBox)row.FindControl("txtStartTime")).validateControls("textbox", "string", allow_empty, "Start Time is required", "Clinic Start Time: < > Characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtEndTime")).validateControls("textbox", "string", allow_empty, "End Time is required", "Clinic End Time: < > Characters are not allowed", this.Page) && is_page_valid;

            is_page_valid = ((TextBox)row.FindControl("txtLocalContactPhone")).validateControls("textbox", "phone", allow_empty, "Phone number is required", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtLocalContactEmail")).validateControls("textbox", "email", allow_empty, "Email is required", "Please enter a valid email address", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtClinicStore")).validateControls("textbox", "number", true, "Store Id is required", "Please enter valid Store Id", this.Page) && is_page_valid;
        }
        is_page_valid = txtNameOfRxHost.validateControls("textbox", "string", true, "", "Name Of Rx Host: < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = txtRxHostPhone.validateControls("textbox", "phone", true, "", "Valid Phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        is_page_valid = txtTotalHoursClinicHeld.validateControls("textbox", "decimal", true, "", "Please enter the valid Total Hours Clinic Held", this.Page) && is_page_valid;
        is_page_valid = txtFeedBack.validateControls("textbox", "string", true, "", "Feed Back: < > characters are not allowed", this.Page) && is_page_valid;

        return is_page_valid;
    }

    /// <summary>
    /// Displays Time picker at server side
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void displayTime_Picker(object sender, EventArgs e)
    {
        StringBuilder script_text = new StringBuilder();
        GridViewRow gvr = (GridViewRow)((sender as TextBox).NamingContainer);
        TextBox txt_end_time = new TextBox();
        txt_end_time = (TextBox)gvr.FindControl("txtEndTime");

        string start_time_client_id = (sender as TextBox).ClientID;
        string end_time_client_id = txt_end_time.ClientID;
        script_text = ApplicationSettings.displayTimePicker(start_time_client_id, end_time_client_id);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DateScript" + gvr.RowIndex, script_text.ToString(), true);
    }

    protected void doProcess_Click(object sender, CommandEventArgs e)
    {
        this.updatedAction = e.CommandArgument.ToString();
        if ((this.updatedAction.ToLower() == "cancel"))
        {
            if (!ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails"))
                this.doProcess(false, true);
            else
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                Response.Redirect("~/walgreensHome.aspx");
            }
        }
        else
        {
            if (ValidateLocations())
                this.doProcess(false, true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
        }
    }

    protected void ValidateCompanyPhoneFax(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validatePhone();
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        // to clear the date in picker if we select MO or DC state by non admin user
        //calling bind clinic to set min dates dates for date pickers
        DropDownList ddl_state_clinic_location = (DropDownList)sender;
        if (ddl_state_clinic_location != null)
        {
            GridViewRow row = (GridViewRow)ddl_state_clinic_location.Parent.Parent;
            if (row != null)
            {
                var date_control = row.FindControl("PickerAndCalendarFrom");
                DateTime seleted_date = ((PickerAndCalendar)date_control).getSelectedDate;

                if ((ddl_state_clinic_location.SelectedValue == "MO" || ddl_state_clinic_location.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin && seleted_date < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Parse("01/01/0001");
                }
            }
            this.prepareClinicDetailsXml(-1);
            this.bindClinicLocations();
        }
    }
    #endregion

    #region --------------- PRIVATE METHODS ------------------
    /// <summary>
    /// Binds selected clinic client details and location information
    /// </summary>
    private void bindClinicLocationDetails()
    {
        int business_clinic_pk;
        Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);
        if (!string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserRole))
        {
            DataSet ds_clinic_details = this.dbOperation.getClinicLocationDetails(business_clinic_pk, 2015, 1, this.commonAppSession.LoginUserInfoSession.UserRole);
            if (ds_clinic_details != null && ds_clinic_details.Tables.Count > 0)
            {
                if (ds_clinic_details.Tables[0].Rows.Count > 0)
                {
                    this.isRestrictedStoreState = ApplicationSettings.isRestrictedStoreState(this.commonAppSession.SelectedStoreSession.storeState, this.commonAppSession.LoginUserInfoSession.UserRole);
                    this.isAddressDisabled = this.isRestrictedStoreState;
                    if (this.isRestrictedStoreState)
                    {
                        Dictionary<string, string> business_address = new Dictionary<string, string>();
                        business_address.Add("naClinicAddress1", ds_clinic_details.Tables[0].Rows[0]["businessAddress"].ToString());
                        business_address.Add("naClinicAddress2", ds_clinic_details.Tables[0].Rows[0]["businessAddress2"].ToString());
                        business_address.Add("naClinicCity", ds_clinic_details.Tables[0].Rows[0]["businessCity"].ToString());
                        business_address.Add("naClinicState", ds_clinic_details.Tables[0].Rows[0]["businessState"].ToString());
                        business_address.Add("naClinicZip", ds_clinic_details.Tables[0].Rows[0]["businessZip"].ToString());
                        this.businessLocationAddress = business_address;
                    }

                    this.hdMaxClinicNumber.Value = ds_clinic_details.Tables[0].Rows[0]["maxClinicLocationId"].ToString();

                    //Business Information
                    this.lblClientName.Text = ds_clinic_details.Tables[0].Rows[0]["businessName"].ToString();
                    this.txtFirstContactName.Text = ds_clinic_details.Tables[0].Rows[0]["firstName"].ToString();
                    this.txtLastContactName.Text = ds_clinic_details.Tables[0].Rows[0]["lastName"].ToString();
                    this.txtContactJobTitle.Text = ds_clinic_details.Tables[0].Rows[0]["jobTitle"].ToString();
                    this.lblLocalContactEmailPrimary.Text = ds_clinic_details.Tables[0].Rows[0]["businessContactEmail"].ToString();
                    this.lblLocalContactPhonePrimary.Text = ds_clinic_details.Tables[0].Rows[0]["phone"].ToString();
                    this.txtPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                    this.lblPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                    this.txtGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();
                    this.lblGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();
                    this.txtIdRecipient.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();
                    this.lblIdRecipient.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();

                    //Billing & Vaccine Information
                    this.txtDefaultEstShots.Text = ds_clinic_details.Tables[1].Rows[0]["estimatedQuantity"].ToString();
                    this.txtDefaultVouchersDistributed.Text = ds_clinic_details.Tables[1].Rows[0]["vouchersDistributed"].ToString();
                    this.txtDefaultTotalImm.Text = ds_clinic_details.Tables[1].Rows[0]["totalImmAdministered"].ToString();

                    //Bind default clinic details
                    this.grdLocations.DataSource = ds_clinic_details.Tables[0];
                    this.grdLocations.DataBind();

                    //Bind Pharmacist & Post Clinic Information                
                    this.txtNameOfRxHost.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistName"].ToString();
                    this.txtRxHostPhone.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistPhone"].ToString();
                    this.txtTotalHoursClinicHeld.Text = ds_clinic_details.Tables[0].Rows[0]["totalHours"].ToString();

                    //Show/hide outreach contact status buttons
                    this.btnCancelClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnCancelClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnConfirmedClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnConfirmedClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnClinicCompleted.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnClinicCompletedDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());

                    this.hfIsCancelled.Value = ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString();

                    //disable edit clinic details facility to specific clients
                    if (this.isDisableEditClinicDetails)
                    {
                        this.btnCancelClinic.Visible = false;
                        this.btnConfirmedClinic.Visible = false;
                        this.btnClinicCompleted.Visible = false;
                        this.btnSubmit.Visible = false;
                        this.btnCancelClinicDim.Visible = true;
                        this.btnConfirmedClinicDim.Visible = true;
                        this.btnClinicCompletedDim.Visible = true;
                        this.btnSubmitDim.Visible = true;
                    }

                    //Bind clinic udpates history log
                    this.grdClinicUpdatesHistory.DataSource = ApplicationSettings.getClinicUpdateHistory(ds_clinic_details.Tables[2], "Charity");
                    this.grdClinicUpdatesHistory.DataBind();

                    //Making address fields mandatory for MO state store clinics
                    //Making address fields mandatory for DC state store clinics - 02/05/2018
                    if (ds_clinic_details.Tables[0].Rows[0]["storeState"].ToString() == "MO" || ds_clinic_details.Tables[0].Rows[0]["storeState"].ToString() == "DC")
                        this.hfisMOState.Value = "true";
                }

                this.prepareClinicDetailsXml(-1);
                Session[this.hfBusinessClinicPk.Value] = this.xmlClinicDetails;
            }
        }
        else
            Response.Redirect("auth/sessionTimeout.aspx");
    }

    /// <summary>
    /// Binds clinic locations to Grid
    /// </summary>
    private void bindClinicLocations()
    {
        StringReader location_reader = new StringReader(this.xmlClinicDetails.SelectSingleNode("//clinicLocations").OuterXml.ToString());
        DataSet location_dataset = new DataSet();
        location_dataset.ReadXml(location_reader);

        grdLocations.DataSource = location_dataset.Tables[0];
        grdLocations.DataBind();
    }

    /// <summary>
    /// Prepares clinic locations XML document
    /// </summary>
    /// <param name="remove_location_id"></param>
    private void prepareClinicDetailsXml(int remove_location_id)
    {
        XmlElement clinic_details_ele = this.xmlClinicDetails.CreateElement("clinicDetails");
        int max_clinic_number = 0, new_store_id = 0, estimated_Shots = 0;

        Int32.TryParse(this.hdMaxClinicNumber.Value, out max_clinic_number);
        double location_count = max_clinic_number - 1;

        //Clinic client details
        XmlElement client_info = this.xmlClinicDetails.CreateElement("clientInformation");
        client_info.SetAttribute("clientName", this.lblClientName.Text.Trim());
        client_info.SetAttribute("contactFirstName", this.txtFirstContactName.Text.Trim());
        client_info.SetAttribute("contactLastName", this.txtLastContactName.Text.Trim());
        client_info.SetAttribute("contactPhone", this.lblLocalContactPhonePrimary.Text.Trim());
        client_info.SetAttribute("contactEmail", this.lblLocalContactEmailPrimary.Text.Trim());
        client_info.SetAttribute("jobTitle", this.txtContactJobTitle.Text.Trim());
        client_info.SetAttribute("naClinicPlanId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtPlanId.Text.Trim() : this.lblPlanId.Text.Trim()));
        client_info.SetAttribute("naClinicGroupId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtGroupId.Text.Trim() : this.lblGroupId.Text.Trim()));
        client_info.SetAttribute("recipientId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtIdRecipient.Text.Trim() : this.lblIdRecipient.Text.Trim()));
        client_info.SetAttribute("comments", "");
        client_info.SetAttribute("feedback", this.txtFeedBack.Text);
        clinic_details_ele.AppendChild(client_info);

        //Clinic locations details
        XmlElement clinic_locations = this.xmlClinicDetails.CreateElement("clinicLocations");
        foreach (GridViewRow row in grdLocations.Rows)
        {
            if (row.RowIndex != remove_location_id)
            {
                XmlElement location_node = this.xmlClinicDetails.CreateElement("clinicLocation");
                location_node.SetAttribute("naClinicLocation", ((row.RowIndex == 0) ? ((Label)row.FindControl("lblClinicLocation")).Text.Trim() : (location_count >= 26 ? ((Char)(65 + (location_count % 26 == 0 ? Math.Ceiling(location_count / 26) - 1 : Math.Ceiling(location_count / 26) - 2))).ToString() + "" + ((char)(65 + location_count % 26)).ToString() : ((char)(65 + location_count % 26)).ToString())));
                location_node.SetAttribute("naContactFirstName", ((TextBox)row.FindControl("txtContactFirstName")).Text);
                location_node.SetAttribute("naContactLastName", ((TextBox)row.FindControl("txtContactLastName")).Text);
                location_node.SetAttribute("naClinicContactPhone", ((TextBox)row.FindControl("txtLocalContactPhone")).Text);
                location_node.SetAttribute("naContactEmail", ((TextBox)row.FindControl("txtLocalContactEmail")).Text);
                location_node.SetAttribute("naClinicAddress1", ((TextBox)row.FindControl("txtAddress1")).Text);
                location_node.SetAttribute("naClinicAddress2", ((TextBox)row.FindControl("txtAddress2")).Text);
                location_node.SetAttribute("naClinicCity", ((TextBox)row.FindControl("txtCity")).Text);
                location_node.SetAttribute("naClinicState", ((DropDownList)row.FindControl("ddlState")).SelectedValue);
                location_node.SetAttribute("naClinicZip", ((TextBox)row.FindControl("txtZipCode")).Text);
                if (((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                    location_node.SetAttribute("clinicDate", ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString());
                else
                    location_node.SetAttribute("clinicDate", "");
                location_node.SetAttribute("naClinicStartTime", ((TextBox)row.FindControl("txtStartTime")).Text);
                location_node.SetAttribute("naClinicEndTime", ((TextBox)row.FindControl("txtEndTime")).Text);
                location_node.SetAttribute("clinicScheduledOn", grdLocations.DataKeys[row.RowIndex].Values["clinicScheduledOn"].ToString());
                location_node.SetAttribute("isNoClinic", ((CheckBox)row.FindControl("chkNoClinic")).Checked ? "1" : "0");
                if (row.RowIndex == 0)
                {
                    Int32.TryParse(this.txtDefaultClinicStoreId.Text, out new_store_id);
                    Int32.TryParse(this.txtDefaultEstShots.Text, out estimated_Shots);

                    location_node.SetAttribute("naClinicEstimatedVolume", (this.txtDefaultEstShots.Text.Trim().Length > 0 ? estimated_Shots.ToString() : ""));
                    location_node.SetAttribute("naClinicVouchersDist", this.txtDefaultVouchersDistributed.Text);
                    location_node.SetAttribute("naClinicTotalImmAdministered", this.txtDefaultTotalImm.Text);
                }
                else
                {
                    Int32.TryParse(((TextBox)row.FindControl("txtClinicStore")).Text, out new_store_id);
                    Int32.TryParse(((TextBox)row.FindControl("txtEstShots")).Text, out estimated_Shots);

                    location_node.SetAttribute("naClinicEstimatedVolume", (((TextBox)row.FindControl("txtEstShots")).Text.Trim().Length > 0 ? estimated_Shots.ToString() : ""));
                    location_node.SetAttribute("naClinicVouchersDist", ((TextBox)row.FindControl("txtVouchersDistributed")).Text);
                    location_node.SetAttribute("naClinicTotalImmAdministered", ((TextBox)row.FindControl("txtTotalImm")).Text);
                }

                if (isStoreEditable)
                    location_node.SetAttribute("clinicStoreId", (new_store_id > 0) ? new_store_id.ToString() : "");
                else
                    location_node.SetAttribute("clinicStoreId", "");

                location_node.SetAttribute("confirmedClientName", "");

                clinic_locations.AppendChild(location_node);
                location_count++;
            }
        }
        clinic_details_ele.AppendChild(clinic_locations);

        XmlElement post_clinic_info = this.xmlClinicDetails.CreateElement("postClinicInformation");
        post_clinic_info.SetAttribute("pharmacistName", this.txtNameOfRxHost.Text);
        post_clinic_info.SetAttribute("totalHours", this.txtTotalHoursClinicHeld.Text);
        post_clinic_info.SetAttribute("pharmacistPhone", this.txtRxHostPhone.Text);
        clinic_details_ele.AppendChild(post_clinic_info);

        this.xmlClinicDetails.AppendChild(clinic_details_ele);
    }

    /// <summary>
    /// Sends store reassignment email
    /// </summary>
    /// <param name="store_id"></param>
    /// <param name="xml_doc"></param>
    /// <param name="business_clinic_pk"></param>
    /// <param name="dt_reassigned_store_users"></param>
    private void sendRevisedStoreAssignments(int store_id, XmlDocument xml_doc, int business_clinic_pk, DataTable dt_reassigned_store_users)
    {
        string email_body, clinic_full_address, clinic_date, clinic_time, clinic_name, plan_id = "", group_id = "";
        string store_manager_email, pharmacy_manager_email, subject_line, email_to, email_cc, salutation;

        if (this.walgreensEmail != null)
        {
            XmlNode default_clinic_location = xml_doc.SelectSingleNode("//clinicLocations").FirstChild;
            XmlNode client_information = xml_doc.SelectSingleNode("//clientInformation");

            clinic_name = this.appSettings.removeLineBreaks(this.lblClientName.Text.Trim() + " - " + default_clinic_location.Attributes["naClinicLocation"].Value);
            clinic_full_address = default_clinic_location.Attributes["naClinicAddress1"].Value.Trim() + " " + default_clinic_location.Attributes["naClinicAddress2"].Value.Trim() + default_clinic_location.Attributes["naClinicCity"].Value.Trim();
            clinic_full_address = (string.IsNullOrEmpty(clinic_full_address.Trim())) ? clinic_full_address + default_clinic_location.Attributes["naClinicState"].Value.Trim() + " " + default_clinic_location.Attributes["naClinicZip"].Value.Trim() : clinic_full_address + ", " + default_clinic_location.Attributes["naClinicState"].Value.Trim() + " " + default_clinic_location.Attributes["naClinicZip"].Value.Trim();
            clinic_date = (!string.IsNullOrEmpty(default_clinic_location.Attributes["clinicDate"].Value.Trim())) ? Convert.ToDateTime(default_clinic_location.Attributes["clinicDate"].Value).ToString("MM/dd/yyyy") : "";

            clinic_time = default_clinic_location.Attributes["naClinicStartTime"].Value.Trim();
            clinic_time = (!string.IsNullOrEmpty(clinic_time)) ? clinic_time + ((!string.IsNullOrEmpty(default_clinic_location.Attributes["naClinicEndTime"].Value)) ? " - " + default_clinic_location.Attributes["naClinicEndTime"].Value.Trim() : "") : clinic_time + default_clinic_location.Attributes["naClinicEndTime"].Value.Trim();

            plan_id = client_information.Attributes["naClinicPlanId"].Value.Trim();
            group_id = client_information.Attributes["naClinicGroupId"].Value.Trim();

            email_body = "<p class='assignment' align='left'><b>Client: </b>" + clinic_name + "</p>";
            email_body += "<p class='assignment' align='left'><b>Clinic Date: </b>" + clinic_date + "</p>";
            email_body += "<p class='assignment' align='left'><b>Plan ID: </b>" + plan_id + "</p>";
            email_body += "<p class='assignment' align='left'><b>Group ID: </b>" + group_id + "</p>";

            email_body += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

            email_body += "<p class='assignment' align='left'><b>Clinic Time: </b>" + clinic_time + "</p>";
            email_body += "<p class='assignment' align='left'><b>Clinic Location: </b>" + clinic_full_address + "</p>";

            if (dt_reassigned_store_users.Rows.Count > 0)
            {
                store_manager_email = ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_reassigned_store_users.Rows[0]["storeManagerEmail"].ToString();
                pharmacy_manager_email = ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_reassigned_store_users.Rows[0]["pharmacyManagerEmail"].ToString();

                email_to = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                salutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";
                email_cc = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";

                subject_line = String.Format((string)GetGlobalResourceObject("errorMessages", "nlaSubjectLine"), clinic_name);
                if (!string.IsNullOrEmpty(email_to))
                    this.walgreensEmail.sendLocalClinicAssignmentEmailFromWeb(salutation, ApplicationSettings.encryptedLinkWithSixArgs(email_to, "walgreensLandingPage.aspx", "walgreensCharityProgramClinicDetails.aspx", business_clinic_pk.ToString(), "0", store_id.ToString()), String.Format((string)GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), Server.MapPath("~/emailTemplates/localAssignmentsTemplate.htm"), ApplicationSettings.encryptedLinkWithThreeArgs(email_to, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), subject_line, email_body, "", email_to, email_cc.TrimStart(','), true);
            }
        }
    }

    /// <summary>
    /// Do process for sending mails, updating history logs and contact logs
    /// </summary>
    /// <param name="is_return_home"></param>
    /// <param name="is_check_clinic_dates"></param>
    private void doProcess(bool is_return_home, bool is_check_clinic_dates, bool is_override = false, bool is_continue = false)
    {
        int business_clinic_pk, store_id, new_store_id;
        bool clinic_date_changed_2weeks = false, est_qnt_increased = false;
        Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);
        Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);
        Int32.TryParse(this.txtDefaultClinicStoreId.Text, out new_store_id);
        if (new_store_id == 0 || new_store_id == store_id)
            new_store_id = 0;

        if (Session[this.hfBusinessClinicPk.Value] != null && business_clinic_pk > 0 && this.commonAppSession.LoginUserInfoSession != null)
        {
            int return_value = 0;
            string error_message = string.Empty;
            string email_body = string.Empty;
            //string billing_email = string.Empty;
            //bool send_emailto_clinical_contract = false;
            XmlDocument old_clinic_details_xml = ((XmlDocument)Session[this.hfBusinessClinicPk.Value]);
            int clinic_count = 1;
            string max_qty_error = string.Empty;
            bool has_max_qty = false;
            this.prepareClinicDetailsXml(-1);
            bool is_equal = ApplicationSettings.compareXMLDocuments(this.xmlClinicDetails, old_clinic_details_xml);

            //Warning to save changes before returning to home page
            switch (this.updatedAction.ToLower())
            {
                case "cancel":
                    if (!is_equal)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showUpdateClinicWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "clinicDetailsChanged")) + "');", true);
                        return;
                    }
                    else
                    {
                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Response.Redirect("~/walgreensHome.aspx");
                    }
                    break;
                case "submit":
                    if (is_equal)
                    {
                        this.bindClinicLocations();
                        return;
                    }
                    break;
                case "confirmed":
                case "completed":
                case "cancelled":
                    if (new_store_id > 0)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showStoreChangeWithContactStatusWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "storeChangedWithContactStatusUpdate")) + "');", true);
                        return;
                    }
                    break;
            }

            //Check duration between clinic scheduled on and clinic date
            if (is_check_clinic_dates)
            {
                if (!this.validateClinicDates())
                {
                    this.showAlertMessage = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + string.Format((string)GetGlobalResourceObject("errorMessages", "clinicCreatedBefore2Weeks")) + "','" + this.updatedAction + "');", true);
                    return;
                }
            }

            XmlNodeList clinic_location_nodes = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation[@clinicDate!='' and @naClinicStartTime!='' and  @naClinicEndTime!='' ]");
            string failed_location = "";
            if (!Convert.ToBoolean(this.hfIsCancelled.Value))
            {
                ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "details", this.lblClientName.Text, string.Empty);
                if (!String.IsNullOrEmpty(failed_location))
                {
                    this.showAlertMessage = true;
                    //calling this methos due to set min date css for picker 
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", failed_location, true);
                    return;
                }
            }
            var clinic_locations = (from locations in XDocument.Parse(this.xmlClinicDetails.InnerXml).Descendants("clinicLocation")
                                    where locations.Attribute("isNoClinic").Value != "1"
                                    select locations).ToList();
            var old_imm_list = old_clinic_details_xml.SelectNodes("/clinicDetails/clinicLocations/clinicLocation/@naClinicEstimatedVolume").Cast<XmlNode>().ToList();

            foreach (var clinic_location in clinic_locations)
            {
                string clinic_date = clinic_location.Attribute("clinicDate").Value;
                int old_est_shots;
                int.TryParse(old_imm_list[0].Value, out old_est_shots);
                int new_est_shots;
                int.TryParse(clinic_location.Attribute("naClinicEstimatedVolume").Value, out new_est_shots);
                if (clinic_count == 1)
                {
                    if (Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14) && !is_override)
                    {
                        if (old_clinic_details_xml != null && !string.IsNullOrEmpty(old_clinic_details_xml.SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicDate").Value))
                        {
                            DateTime previous_clinic_date = Convert.ToDateTime(old_clinic_details_xml.SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicDate").Value);

                            if (previous_clinic_date != Convert.ToDateTime(clinic_date))
                            {
                                clinic_date_changed_2weeks = true;
                            }
                        }
                    }
                    if (new_est_shots != old_est_shots)
                    {
                        if (new_est_shots > old_est_shots && Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14) && !is_override)
                            est_qnt_increased = true;

                        if (new_est_shots > 250 && !est_qnt_increased)
                        {
                            has_max_qty = true;
                            max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), new_est_shots, "Standard – Trivalent", clinic_location.Attribute("naClinicLocation").Value);
                        }
                    }
                }
                else
                {
                    if (new_est_shots > 250)
                    {
                        has_max_qty = true;
                        max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), new_est_shots, "Standard – Trivalent", clinic_location.Attribute("naClinicLocation").Value);
                    }
                }
                clinic_count++;
            }

            if (this.updatedAction.ToLower() != "completed")
            {
                string validation_summary = string.Empty;
                List<string> error_list = new List<string>();
                bool is_having_override = true;
                if (clinic_date_changed_2weeks)
                {
                    error_list.Add((string)GetGlobalResourceObject("errorMessages", "clinicChangedBefore2Weeks"));

                    if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                        is_having_override = false;
                }
                if (est_qnt_increased)
                {
                    error_list.Add("<b style=\"color: red;\">" + (string)GetGlobalResourceObject("errorMessages", "immQtyIncreasedBefore2Weeks") + "</b>");
                    if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
                        is_having_override = false;
                }
                if (error_list.Count > 0)
                {
                    validation_summary = "<ul>";
                    foreach (var item in error_list)
                    {
                        validation_summary += "<li style=\"text-align:left\">" + item.Replace("\n", "<br />").Trim() + "</li><br/>";
                    }
                    validation_summary += "</ul>";
                    this.showAlertMessage = true;
                    if (is_having_override)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryWarning('" + validation_summary + "','continuesaving," + this.updatedAction + "');", true);
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryAlert('" + validation_summary + "','continuesaving');", true);
                    validation_summary = "";
                    error_list.Clear();
                    return;
                }
                if (has_max_qty && !is_continue)
                {
                    this.showAlertMessage = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showWarning('WARNING','" + max_qty_error.Substring(6) + "','continuesubmitting," + this.updatedAction + "');", true);
                    return;
                }
            }

            DataTable dt_reassigned_clinics = new DataTable();
            Dictionary<string, string> updated_values = ApplicationSettings.getUpdatedClinicDetails(this.xmlClinicDetails, ((XmlDocument)Session[this.hfBusinessClinicPk.Value]), "Charity");
            email_body = updated_values["emailBody"];
            //billing_email = updated_values["billingEmail"];

            if (!string.IsNullOrEmpty(updated_values["historyLog"]))
            {
                //Prepare updated fields xml and append to clinic details xml document
                XmlDocumentFragment xml_frag = this.xmlClinicDetails.CreateDocumentFragment();
                xml_frag.InnerXml = updated_values["historyLog"];
                this.xmlClinicDetails.DocumentElement.AppendChild(xml_frag);

                //send_emailto_clinical_contract = CommonExtensionsMethods.sendEmailToClinicalContract(updated_values["historyLog"]);
            }

            //reassign clinic if store state is not restricted.
            //bool can_reassign_clinic = (!ApplicationSettings.isRestrictedStoreState("MO", this.commonAppSession.LoginUserInfoSession.UserRole));
            return_value = this.dbOperation.updateClinicLocationDetails(business_clinic_pk, "Charity", this.commonAppSession.LoginUserInfoSession.UserID, this.updatedAction, this.xmlClinicDetails.InnerXml, false, out error_message, out dt_reassigned_clinics);

            if (return_value == -2 && !string.IsNullOrEmpty(error_message))
            {
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message.Substring(4) + "');", true);
                return;
            }
            if (return_value == -4 && !string.IsNullOrEmpty(error_message))
            {
                string validation_message = string.Empty;
                ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "details", this.lblClientName.Text, error_message);

                this.showAlertMessage = true;
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
                return;
            }
            if (return_value == -5 && !string.IsNullOrEmpty(error_message))//Assigned to Restricted store state
            {
                this.bindClinicLocations();
                this.showAlertMessage = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
                return;
            }

            DataTable dt_default_clinic_user = dbOperation.getStoreUsersEmails(new_store_id > 0 ? new_store_id : store_id);
            EmailOperations email_operations = new EmailOperations();

            //Sending clinic details changed email 'Business Information'...
            //if ((!string.IsNullOrEmpty(email_body) || !string.IsNullOrEmpty(billing_email)) && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
            if (!string.IsNullOrEmpty(email_body) && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
            {
                //email_operations.sendClinicDetailsChangedEmail(dt_default_clinic_user, "Charity", email_body, billing_email, this.lblClientName.Text, business_clinic_pk.ToString(), "0", (new_store_id > 0 ? new_store_id : store_id).ToString(), send_emailto_clinical_contract);
                email_operations.sendClinicDetailsChangedEmail(dt_default_clinic_user, "Charity", email_body, string.Empty, this.lblClientName.Text, business_clinic_pk.ToString(), "0", (new_store_id > 0 ? new_store_id : store_id).ToString(), false);

                //if (!string.IsNullOrEmpty(updated_values["billingEmailToAllClinics"]))
                //    email_operations.sendUpdateBillingInfoEmailToAllClinics("Charity", email_body, updated_values["billingEmailToAllClinics"], this.lblClientName.Text, business_clinic_pk, "0", send_emailto_clinical_contract);
            }

            //Sending store reassignment & revised store assignment email...
            if (new_store_id > 0 && dt_reassigned_clinics != null && dt_reassigned_clinics.Rows.Count > 0 && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
            {
                email_operations.sendLocalClinicStoreReassignmentEmail(dt_default_clinic_user, dt_reassigned_clinics);

                this.sendRevisedStoreAssignments(new_store_id, this.xmlClinicDetails, business_clinic_pk, dt_reassigned_clinics);
            }

            if (return_value == -3 && !string.IsNullOrEmpty(error_message))
            {
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showMaintainContactLogWarning('" + error_message + "');", true);
                return;
            }
            else if (!is_return_home)
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;

                if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "confirmed")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "confirmedClinic") + "'); window.location.href = 'walgreensCharityProgramClinicDetails.aspx';", true);
                else if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "completed")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicCompleted") + "'); window.location.href = 'walgreensCharityProgramClinicDetails.aspx';", true);
                else if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "cancelled")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicCancelled") + "'); window.location.href = 'walgreensCharityProgramClinicDetails.aspx';", true);
                else
                {
                    if (new_store_id > 0)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensCharityProgramClinicDetails.aspx';", true);
                }
            }
        }
        else
        {
            Session.Remove(this.hfBusinessClinicPk.Value);
            Response.Redirect("~/walgreensHome.aspx");
        }
    }

    /// <summary>
    /// Displays controls based on user login
    /// </summary>
    private void controlAccess()
    {
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.txtPlanId.Visible = true;
            this.txtGroupId.Visible = true;
            this.txtIdRecipient.Visible = true;
            this.lblGroupId.Visible = false;
            this.lblPlanId.Visible = false;
            this.lblIdRecipient.Visible = false;
        }
    }

    /// <summary>
    /// Validates clinic duration between clinic date and scheduled date
    /// </summary>
    /// <returns></returns>
    private bool validateClinicDates()
    {
        bool is_date_valid = true;
        bool is_no_clinic = true;
        int clinic_count = 1;
        var clinic_details_xml = XDocument.Parse(this.xmlClinicDetails.InnerXml);

        var clinic_dates = from dates in clinic_details_xml.Descendants("clinicLocation")
                           select dates.Attribute("clinicDate").Value;

        foreach (var clinic_date in clinic_dates)
        {
            is_no_clinic = Convert.ToBoolean(Convert.ToInt32(this.xmlClinicDetails.SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation[" + clinic_count + "]/@isNoClinic").Value));

            if (!is_no_clinic)
            {
                if (clinic_count == 1)
                {
                    bool is_prev_no_clinic = Convert.ToBoolean(Convert.ToInt32(((XmlDocument)Session[this.hfBusinessClinicPk.Value]).SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@isNoClinic").Value));
                    if (is_prev_no_clinic)
                    {
                        if (Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14))
                        {
                            is_date_valid = false;
                            break;
                        }
                    }
                }
                else
                {
                    if (Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14))
                    {
                        is_date_valid = false;
                        break;
                    }
                }
            }

            clinic_count++;
        }

        return is_date_valid;
    }

    /// <summary>
    /// Sets min/max dates for clinics
    /// </summary>
    /// <param name="row"></param>
    private void setClinicDates(GridViewRow row, bool is_from_grid_load)
    {
        var date_control = row.FindControl("PickerAndCalendarFrom");
        DropDownList ddl_states = (DropDownList)row.FindControl("ddlState");
        if (grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString().Length > 0)
        {
            string clinic_date = grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString();
            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                ((PickerAndCalendar)date_control).MinDate = ApplicationSettings.getOutreachStartDate;
            else
            {
                if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    if (Convert.ToDateTime(clinic_date) >= DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                        ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                    else if (Convert.ToDateTime(clinic_date) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                        ((PickerAndCalendar)date_control).SetMinDate = Convert.ToDateTime(clinic_date);
                }
                else if (Convert.ToDateTime(clinic_date) > DateTime.Today.Date)
                {
                    if (DateTime.Now.AddDays(14) < Convert.ToDateTime(clinic_date)
                        && !ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                        ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(13);
                    else if (ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                        ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                    else
                        ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
                }
                else
                {
                    ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
                }
            }
            if (is_from_grid_load)
            {
                ((PickerAndCalendar)date_control).getSelectedDate = Convert.ToDateTime(clinic_date);
                ((TextBox)row.FindControl("txtCalenderFrom")).Text = Convert.ToDateTime(clinic_date).ToString("MM/dd/yyyy");
            }
        }
        else
        {
            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                ((PickerAndCalendar)date_control).MinDate = ApplicationSettings.getOutreachStartDate;
            else if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
            else
            {
                if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Now.AddDays(14);
                else
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
            }
        }
    }
    /// <summary>
    /// Set minimum and maximum dates for date controls.
    /// </summary>
    private void setMinMaxDates()
    {
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            var clinic_date = row.FindControl("PickerAndCalendarFrom");
            if (clinic_date != null)
            {
                this.setClinicDates(row, false);
            }
        }
    }
    #endregion

    #region --------------- PRIVATE VARIABLES ----------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private WalgreenEmail walgreensEmail = null;
    private string updatedAction;
    private XmlDocument xmlClinicDetails;
    private ApplicationSettings appSettings = null;
    private bool showAlertMessage = false;
    private bool isStoreState
    {
        get { return Convert.ToBoolean(this.hfisMOState.Value); }
        set { this.hfisMOState.Value = value.ToString(); }
    }
    private bool isDisableEditClinicDetails = false;
    private bool isStoreEditable = false;
    private bool isAddressDisabled
    {
        get
        {
            bool value = false;
            if (ViewState["isAddressDisabled"] != null)
                value = (bool)ViewState["isAddressDisabled"];
            return value;
        }
        set
        {
            ViewState["isAddressDisabled"] = value;
        }
    }
    private bool isRestrictedStoreState
    {
        get
        {
            bool value = false;
            if (ViewState["isRestrictedStoreState"] != null)
                value = (bool)ViewState["isRestrictedStoreState"];
            return value;
        }
        set
        {
            ViewState["isRestrictedStoreState"] = value;
        }
    }
    private Dictionary<string, string> businessLocationAddress
    {
        get
        {
            Dictionary<string, string> address = new Dictionary<string, string>();
            if (ViewState["businessLocationAddress"] != null)
                address = (Dictionary<string, string>)ViewState["businessLocationAddress"];
            return address;
        }
        set
        {
            ViewState["businessLocationAddress"] = value;
        }
    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        this.updatedAction = string.Empty;
        this.xmlClinicDetails = new XmlDocument();
    }
}