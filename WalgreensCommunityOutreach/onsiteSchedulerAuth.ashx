﻿<%@ WebHandler Language="C#" Class="onsiteSchedulerAuth" %>

using System;
using System.Web;

public class onsiteSchedulerAuth : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {

        TdWalgreens.EncryptQueryStringAES args = new TdWalgreens.EncryptQueryStringAES();
        args["arg1"] = context.Request.QueryString[0];
        string registration_link = new TdApplicationLib.DBOperations().getSchedulerSiteLink(args.ToString());

        if (registration_link.Length > 0)
        {
            context.Response.Redirect(registration_link);
            context.Response.StatusCode = 301;
            context.Response.End();
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}