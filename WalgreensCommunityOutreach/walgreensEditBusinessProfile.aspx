﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensEditBusinessProfile.aspx.cs"
    Inherits="walgreensEditBusinessProfile" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            if (($("#txtSOStore").val().length == 0) && $('#chkbSOEffort').checked) {
                $("#txtSOStore").attr("disabled", "disabled");
            }

            if (($("#txtIPStore").val().length == 0) && $('#chkbIPEffort').checked) {
                $("#txtIPStore").attr("disabled", "disabled");
            }

            dropdownRepleceText("ddlLocalBusinesses", "txtLocalBusinesses");
            dropdownRepleceText("ddlEditInformation", "txtEditInformation");

            $("#txtSOStore").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#txtIPStore").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $('input[type=checkbox]').click(function () {
                var groupName = $(this).attr('groupname');
                if (!groupName)
                    return;

                var checked = $(this).is(':checked');

                $("input[groupname='" + groupName + "']:checked").each(function () {
                    $(this).prop('checked', '');
                });

                if (checked)
                    $(this).prop('checked', 'checked');
            });

            $('#chkbxHhsProgram').change(function () {
                if (this.checked) {
                    $('#chkCOProgram').prop('checked', false);
                    $('#hdSelectedBusinessType').val('charity');
                }
                else {
                    $('#hdSelectedBusinessType').val('');
                }
            });
            $('#chkCOProgram').change(function () {
                if (this.checked) {
                    $('#chkbxHhsProgram').prop('checked', false);
                    $('#hdSelectedBusinessType').val('CommunityOutreach');
                }
                else {
                    $('#hdSelectedBusinessType').val('');
                }
            });
       });

        function validateRequired(is_checked) {
            if (is_checked.checked) {
                switch (is_checked.id) {
                    case 'chkbSOEffort':
                        $("#txtSOStore").val($("#hdSOStore").val());
                        setControlValidation('txtSOStore', true);
                        $("#txtSOStore").removeAttr("disabled");
                        break;
                    case "chkbIPEffort":
                        $("#txtIPStore").val($("#hdIPStore").val());
                        setControlValidation('txtIPStore', true);
                        $("#txtIPStore").removeAttr("disabled");
                        break;
                }
            }
            else {
                switch (is_checked.id) {
                    case 'chkbSOEffort':
                        $("#txtSOStore").val('');
                        setControlValidation('txtSOStore', false);
                        $("#txtSOStore").attr("disabled", "disabled");
                        break;
                    case "chkbIPEffort":
                        $("#txtIPStore").val('');
                        setControlValidation('txtIPStore', false);
                        $("#txtIPStore").attr("disabled", "disabled");
                        break;
                }
            }
        }

        function setControlValidation(txt_control, is_enable) {
            if (typeof (Page_Validators) != 'undefined') {
                for (i = 0; i <= Page_Validators.length; i++) {
                    if (Page_Validators[i] != null) {
                        if (Page_Validators[i].ControlToValidate == txt_control || Page_Validators[i].controltovalidate == txt_control)
                            ValidatorEnable(Page_Validators[i], is_enable);
                    }
                }
            }
        }

        function displayConfirmation(alert_message, outreach_pk) {
            var msg = confirm(alert_message);
            if (msg) {
                $("#hdImmunizationRemove").val('');
                $("#hdSeniorOutreachRemove").val('');

                if (outreach_pk == '1#3') {
                    $("#hdImmunizationRemove").val('3');
                    $("#hdSeniorOutreachRemove").val('1');
                }
                else if (outreach_pk == '1') {
                    $("#hdSeniorOutreachRemove").val('1');
                }
                else if (outreach_pk == '3') {
                    $("#hdImmunizationRemove").val('3');
                }
                $("#btnRefresh").click();
            }
            else {

                if (outreach_pk == '1#3') {
                    $("#txtIPStore").val($("#hdIPStore").val());
                    $("#txtSOStore").val($("#hdSOStore").val());
                    $("#chkbSOEffort").prop('checked', true);
                    $("#chkbIPEffort").prop('checked', true);
                }
                else if (outreach_pk == '1') {
                    $("#txtSOStore").val($("#hdSOStore").val());
                    $("#chkbSOEffort").prop('checked', true);
                }
                else if (outreach_pk == '3') {
                    $("#txtIPStore").val($("#hdIPStore").val());
                    $("#chkbIPEffort").prop('checked', true);
                }
            }
        }

        function showAddBusinessDisableToMOstateAlert(alert_message) {
            $(function () {
                $("#divConfirmDialog").html(alert_message);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "Add A Business disabled to State Store",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("canReassignBusiness");
                            $(this).dialog('close');
                        },
                        'Cancel': function () {                            
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function showDNCMatchWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 250,
                    width: 350,
                    title: "Business matches DNC Client",
                    buttons: {
                        'Add Business': function () {
                            __doPostBack("AddBusiness", 1);
                            $(this).dialog('close');
                        },
                        "Do Not Add Business": function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }
    </script>
    <style type="text/css">
        .noclose .ui-dialog-titlebar-close { display:none; }
        .ui-icon ui-icon-closethick { display:none; }
        .ui-widget { font-family: Arial; font-size: 13px; }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdImmunizationRemove" runat="server" />
    <asp:HiddenField ID="hdSeniorOutreachRemove" runat="server" />
    <asp:HiddenField ID="hdSelectedBusinessType" runat="server" Value="" />
    <asp:Button ID="btnRefresh" runat="server" CommandArgument="Save" Visible="true"
        Style="display: none" OnCommand="doProcess" CausesValidation="false" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td colspan="2" class="pageTitle2" style="border-bottom: 1px solid #999999; padding-top: 0px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top" class="pageTitle2">
                                        Business Profiles
                                    </td>
                                    <td align="right" valign="middle" class="formFields">
                                        <span class="class2"><asp:LinkButton runat="server" CssClass="class2" ID="lnkBtnSave" CommandArgument="Save" OnCommand="doProcess" Text="Submit"></asp:LinkButton></span> | 
                                        <span class="class2"><asp:LinkButton runat="server" CssClass="class2" ID="lnkBtnCancel" CommandArgument="Cancel" OnCommand="doProcess" Text="Cancel" CausesValidation="false" /></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="formFields">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" ShowSummary="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" valign="top" nowrap="nowrap" height="350">
                            <table width="750px">
                                <tr>
                                    <td width="100%" colspan="4" align="left" nowrap="nowrap" class="profileTitles" style="padding-bottom: 10px;">
                                        <table width="100%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores">
                                            <tr>
                                                <td align="right" valign="top">
                                                    <span class="logSubTitles">Local Businesses: </span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLocalBusinesses" runat="server" CssClass="formFields" AutoPostBack="true" CausesValidation="false" Width="340px" OnSelectedIndexChanged="ddlLocalBusinesses_SelectedIndexChanged"></asp:DropDownList>
                                                    <asp:TextBox CssClass="formFields" ID="txtLocalBusinesses" runat="server" Width="338px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="ddlLocalBusinessesReqFV" ControlToValidate="ddlLocalBusinesses" Display="None" runat="server" ErrorMessage="Select a local business to edit"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <span style="font-size: 8pt; color: #57a1d3;">Select a local business to view/edit profile</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">
                                                    <span class="logSubTitles">Edit: </span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEditInformation" runat="server" CssClass="formFields" AutoPostBack="true" CausesValidation="false" Width="340px" OnSelectedIndexChanged="ddlEditInformation_SelectedIndexChanged">
                                                        <asp:ListItem Value="1">Business Information</asp:ListItem>
                                                        <asp:ListItem Value="2">Outreach Campaigns</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:TextBox CssClass="formFields" ID="txtEditInformation" runat="server" Width="338px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlEditInformation" Display="None" runat="server" ErrorMessage="Select a edit information"></asp:RequiredFieldValidator>
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="750" border="0" cellspacing="0" cellpadding="15" class="wagsRoundedCorners">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    *Business Name:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtBusinessName" runat="server" CssClass="formFields" MaxLength="150" Width="165px" TabIndex="1"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtBusinessNameReqEV" ControlToValidate="txtBusinessName" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Business Name:: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="txtBusinessNameReqFV" runat="server" ControlToValidate="txtBusinessName" Display="None" ErrorMessage="Business Name is required"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="right" class="profileTitles">
                                                    *Phone #:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="formFields" Width="120" onblur="textBoxOnBlur(this);" MaxLength="14" TabIndex="12"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="txtPhoneReqFV" ControlToValidate="txtPhone" Display="None" runat="server" ErrorMessage="Valid Phone Number is required(ex:(###) ###-####)"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="txtPhoneCV" ControlToValidate="txtPhone" runat="server" Display="None" ErrorMessage="Valid Phone Number is required (ex: ###-###-####)" ClientValidationFunction="ValidateCompanyPhoneFax" OnServerValidate="validatePhone"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="18%" align="right" nowrap="nowrap" class="profileTitles">
                                                    Contact First Name:
                                                </td>
                                                <td width="31%" align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="formFields" MaxLength="25" TabIndex="2"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtFirstNameRegEV" ControlToValidate="txtFirstName" Display="None" ValidationExpression="[^<>@#&]+" runat="server" ErrorMessage="First Name: < > @ # & Characters are not allowed."></asp:RegularExpressionValidator>
                                                </td>
                                                <td width="20%" align="right" class="profileTitles">
                                                    Toll Free #:
                                                </td>
                                                <td width="31%" align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtTollFree" runat="server" CssClass="formFields" Width="120" MaxLength="15" TabIndex="13"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    Contact Last Name:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="formFields" MaxLength="35" TabIndex="3"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtLastNameRegEV" ControlToValidate="txtLastName" Display="None" ValidationExpression="[^<>@#&]+" runat="server" ErrorMessage="Last Name: < > @ # & Characters are not allowed."></asp:RegularExpressionValidator>
                                                </td>
                                                <td align="right" class="profileTitles">
                                                    *Industry:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtIndustry" runat="server" CssClass="formFields" MaxLength="49" TabIndex="14" size="32"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="txtIndustryReqFV" ControlToValidate="txtIndustry" Display="None" runat="server" ErrorMessage="Industry is required."></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="txtIndustryRegEV" ControlToValidate="txtIndustry" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Industry Name:: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    Job Title:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtTitle" runat="server" CssClass="formFields" MaxLength="100" TabIndex="4"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtTitleRegEV" ControlToValidate="txtTitle" Display="None" ValidationExpression="[^<>@#&]+" runat="server" ErrorMessage="Title: < > @ # & Characters are not allowed."></asp:RegularExpressionValidator>
                                                </td>
                                                <td align="right" class="profileTitles">
                                                    Website URL:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtWebUrl" runat="server" CssClass="formFields" MaxLength="200" TabIndex="15" size="32"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtWebUrlRegEV" ControlToValidate="txtWebUrl" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Website URL: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    *Address 1:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="formFields" size="32" MaxLength="450" TabIndex="5"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="txtAddress1ReqFV" ControlToValidate="txtAddress1" Display="None" runat="server" ErrorMessage="Address is required."></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="txtAddress1RegEV" ControlToValidate="txtAddress1" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Address: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="txtAddressCV" runat="server" ErrorMessage="<%$ resources:errorMessages, BusinessLocationPOBoxAlert %>"  Display="None" ClientValidationFunction="validateAddress"  OnServerValidate="validateAddress"></asp:CustomValidator>
                                                </td>
                                                <td align="right" class="profileTitles">
                                                    Employment Size:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtEmpSize" runat="server" CssClass="formFields" MaxLength="6" TabIndex="16"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ControlToValidate="txtEmpSize" ID="txtEmpSizeRegEV" runat="server" Display="None" ErrorMessage="Enter a valid Employment Size" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="profileTitles">
                                                    Address 2:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="formFields" size="32" MaxLength="39" TabIndex="6"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtAddress2RegEV" ControlToValidate="txtAddress2" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="Address 2: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                                </td>
                                                <td align="right" class="profileTitles">
                                                    Charity (HHS Voucher):
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:CheckBox CssClass="formFields" ID="chkbxHhsProgram" runat="server" groupname="ProgramName" CausesValidation="false" Visible="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    County:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtCounty" runat="server" CssClass="formFields" size="32" MaxLength="25" TabIndex="7"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtCountyRegEV" runat="server" ControlToValidate="txtCounty" Display="None" ErrorMessage="County: < > Characters are not allowed." ValidationExpression="[^<>]+"></asp:RegularExpressionValidator>
                                                </td>
                                               
                                                <td align="right" class="profileTitles">
                                                    Community Outreach:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                     <asp:CheckBox CssClass="formFields" ID="chkCOProgram"  groupname="ProgramName"  runat="server" CausesValidation="false" Visible="true" Enabled="true" />
                                                </td>
                                                 
                                            </tr>
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    *City:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtCity" runat="server" CssClass="formFields" size="32" MaxLength="25" TabIndex="8"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="txtCityReqFV" ControlToValidate="txtCity" Display="None" runat="server" ErrorMessage="City is required."></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="txtCityRegEV" ControlToValidate="txtCity" Display="None" ValidationExpression="[^<>]+" runat="server" ErrorMessage="City: < > Characters are not allowed."></asp:RegularExpressionValidator>
                                                </td>
                                                <td align="right" width="200px" class="profileTitles">&nbsp;</td>
                                                 <td align="left" class="formFields2" valign="top">*Required Fields</td>
                                            </tr>
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    *State:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:DropDownList ID="ddlState" runat="server" CssClass="profileFormObject" TabIndex="9"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddlStateReqFV" ControlToValidate="ddlState" Display="None" runat="server" ErrorMessage="State is required."></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" class="profileTitles">&nbsp;</td>
                                                <td align="left" class="formFields2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right" nowrap="nowrap" class="profileTitles">
                                                    *Zip Code:
                                                </td>
                                                <td align="left" class="profileTitles">
                                                    <asp:TextBox ID="txtZip" runat="server" CssClass="formFields" Width="53" MaxLength="5" TabIndex="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="txtZipReqFV" ControlToValidate="txtZip" Display="None" runat="server" ErrorMessage="Zip Code is required (ex:#####)."></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="txtZipCV" runat="server" ErrorMessage="Valid Zip Code is required (ex: #####)" ControlToValidate="txtZip" Display="None" ClientValidationFunction="validateZipCode" OnServerValidate="validateZipCode"></asp:CustomValidator>
                                                    <asp:TextBox ID="txtZip4" runat="server" CssClass="formFields" Width="53" MaxLength="4" TabIndex="11"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ControlToValidate="txtZip4" ID="txtZip4RegEV" runat="server" Display="None" ErrorMessage="Valid Sub Zip code is required (ex: ####)" ValidationExpression="\d{4}$"></asp:RegularExpressionValidator>
                                                </td>
                                                <td align="left" class="profileTitles">&nbsp;</td>
                                                <td align="left" class="profileTitles">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right" nowrap="nowrap" class="pageTitle">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top" nowrap="nowrap" class="profileTitles">
                                                    Outreach Campaigns:
                                                </td>
                                                <td colspan="3">
                                                    <table border="0">
                                                        <tr>
                                                            <td align="left" valign="top" nowrap="nowrap" class="profileTitles">
                                                                <asp:CheckBox ID="chkbSOEffort" CssClass="profileTitles" runat="server" AutoPostBack="false" onclick="javascript:validateRequired(this);"></asp:CheckBox>Senior Outreach
                                                            </td>
                                                            <td align="left" valign="top" nowrap="nowrap" class="profileTitles">
                                                                &nbsp;&nbsp;&nbsp;&nbsp; Store:
                                                                <asp:TextBox ID="txtSOStore" runat="server" CssClass="formFields" size="16" MaxLength="6"></asp:TextBox>
                                                                <asp:HiddenField ID="hdSOStore" runat="server" />
                                                                <asp:RequiredFieldValidator ID="txtSOStoreReqFV" ControlToValidate="txtSOStore" Display="None" Enabled="false" runat="server" ErrorMessage="Please enter the store Id for Senior Outreach."></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" nowrap="nowrap" class="profileTitles">
                                                                <asp:CheckBox ID="chkbIPEffort" CssClass="profileTitles" runat="server" AutoPostBack="false" onclick="javascript:validateRequired(this);"></asp:CheckBox>Immunization Program
                                                            </td>
                                                            <td align="left" valign="top" nowrap="nowrap" class="profileTitles">
                                                                &nbsp;&nbsp;&nbsp;&nbsp; Store:
                                                                <asp:TextBox ID="txtIPStore" runat="server" CssClass="formFields" size="16" MaxLength="6"></asp:TextBox>
                                                                <asp:HiddenField ID="hdIPStore" runat="server" />
                                                                <asp:RequiredFieldValidator ID="txtIPStoreReqFV" ControlToValidate="txtIPStore" Display="None" runat="server" Enabled="false" ErrorMessage="Please enter the store Id for Immunization Program."></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" class="formFields2">
                                                 <p>  <asp:Label ID="lblnonEditMessage" runat="server" Text="Once a business is added to an Outreach Campaign it may only be removed by an Admin." Visible="false"></asp:Label>
                                                     </p>
                                                </td>
                                            </tr>
                                            <tr id="rowAddBusiness">
                                                <td colspan="4" align="center" nowrap="nowrap" class="profileTitles">
                                                    <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_event.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_event.png');"
                                                        CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_event_lit.png');"
                                                        runat="server" AlternateText="Cancel" CommandArgument="Cancel" OnCommand="doProcess" />&nbsp;
                                                    <asp:ImageButton ID="imgBtnSave" ImageUrl="~/images/btn_submit_event.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_event.png');"
                                                        CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_event_lit.png');"
                                                        runat="server" CommandArgument="Save" OnCommand="doProcess" AlternateText="Save Business Profile"
                                                        TabIndex="15" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <p>
                                &nbsp;</p>
                        </td>
                    </tr>
                </table>
                <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 13px; text-align:center; font-weight: normal;"></div>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
