﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TdApplicationLib;
using System.Web.Security;
using System.Configuration;
using System.Text;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Web.Services;
using tdEmailLib;
using TdWalgreens;
using NLog;

public partial class walgreensHome : Page
{
    #region -------------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserName))
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return;
        }

        if (!Page.IsPostBack)
        {
            walgreensHeaderCtrl.storeId = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;

            this.bindOutreachStatusDropdown();
            this.PickerAndCalendar1.getSelectedDate = DateTime.Today.Date;
            this.assignedBusiness();
            this.displayOutreachTabs();
        }
        else
        {
            string contact_args = Request["__EVENTARGUMENT"];
            string log_contact = Request["__EVENTTARGET"];

            if (!string.IsNullOrEmpty(log_contact) && log_contact.ToLower() == "showcontract")
            {
                //string[] args = contact_args.Split(',');
                //string account_type = string.Empty;
                int contact_log_pk;
                Int32.TryParse(contact_args.Trim(), out contact_log_pk);
                //if (args.Length > 1)
                //    account_type = args[1].Trim();
                if (contact_log_pk > 0)
                {
                    this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = contact_log_pk;
                    //if (account_type.ToLower() == "vote & vax")
                    //    Response.Redirect("walgreensVoteVaxAgreement.aspx", false);
                    //else
                    Response.Redirect("walgreensClinicAgreement.aspx", false);
                }
                else
                {
                    this.lblMessage.Text = (string)GetGlobalResourceObject("errorMessages", "logBusinessContactInserted");
                    this.bindOutreachStatusDropdown();
                }
            }
            else if (!string.IsNullOrEmpty(log_contact) && log_contact.ToLower() == "getdncclientslist")
            {
                this.createDNCClientsJson();
            }
            else if (!string.IsNullOrEmpty(log_contact) && log_contact.ToLower() == "deleteddncclient")
            {
                if (contact_args.Length > 0)
                {
                    this.dbOperation.deleteNationalContractClients(contact_args, this.commonAppSession.LoginUserInfoSession.UserID);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Selected Do Not Call National Contract Clients removed successfully'); window.location.href = 'walgreensHome.aspx';", true);
                }
                else
                {
                    this.createDNCClientsJson();
                }
            }
            else
                this.nationalContracts = "[]";
        }

        if (this.outreachStatuses == "[]")
        {
            this.dataTable = new DataTable();
            this.dataTable = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("isActive = 'True' AND category <> 'AC'").CopyToDataTable();

            //outreach status json serialization
            this.outreachStatuses = this.dataTable.getJSONObject();
        }

        //this.getStoreControl1.FilePath = "search.aspx";
        //this.displayStoreSelectionToUser();
        this.PickerAndCalendar1.MaxDate = DateTime.Today.Date.AddDays(1);
        this.lblMessage.Text = "";
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId.ToLower() == "3")
            this.hdDeploymentDate.Value = ConfigurationManager.AppSettings["outreachStartDateIP"].ToString().Trim().Length == 0 ? DateTime.Now.ToShortDateString() : ConfigurationManager.AppSettings["outreachStartDateIP"].ToString();
        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId.ToLower() == "1")
            this.hdDeploymentDate.Value = ConfigurationManager.AppSettings["outreachStartDateSO"].ToString().Trim().Length == 0 ? DateTime.Now.ToShortDateString() : ConfigurationManager.AppSettings["outreachStartDateSO"].ToString();
        lnkAddBusiness.Attributes.Add("onclick", "return disableAddBusiness();");
        if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.lnkRemoveClients.Visible = false;
            this.btnRemoveClients.Visible = false;
        }
    }

    protected void imgBtnAddBusiness_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("walgreensStoreBusiness.aspx", false);
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (this.ddlOutreach.SelectedItem.Value == @"0")
            return;
        string store_state = this.commonAppSession.SelectedStoreSession.storeState;
        bool is_restricted_store_state = ApplicationSettings.isRestrictedStoreState(store_state, this.commonAppSession.LoginUserInfoSession.UserRole);
        string contact_log_pk = "";
        string error_message = "";
        int return_value = 0;
        this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = 0;

        //Do not log Event Scheduled contact outreach status here. Log the contact status along with the Scheduled event details in event scheduled form - CR(02/20/2015-Vishnu)
        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" && this.ddlOutreach.SelectedItem.Value == @"3")
        {
            Dictionary<string, string> event_contact_log = new Dictionary<string, string>();
            event_contact_log.Add("storeId", this.commonAppSession.SelectedStoreSession.storeID.ToString());
            event_contact_log.Add("businessPk", this.ddlAssignedBusiness.SelectedValue);
            event_contact_log.Add("firstName", this.txtContactFirstName.Text);
            event_contact_log.Add("lastName", this.txtContactLastName.Text);
            event_contact_log.Add("contactDate", this.PickerAndCalendar1.getSelectedDate.ToString());
            event_contact_log.Add("feedback", this.txtFeedBack.Text);
            event_contact_log.Add("createdBy", this.commonAppSession.LoginUserInfoSession.UserID.ToString());

            Session["eventContactLog_" + this.commonAppSession.SelectedStoreSession.storeID + "_" + this.commonAppSession.LoginUserInfoSession.UserID.ToString()] = event_contact_log;
            Response.Redirect("scheduleEventForm.aspx", true);
        }
        else
            return_value = this.dbOperation.insertContactLog(this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId, this.commonAppSession.SelectedStoreSession.storeID, Convert.ToInt32(this.ddlAssignedBusiness.SelectedValue), this.txtContactFirstName.Text, this.txtContactLastName.Text, this.PickerAndCalendar1.getSelectedDate, Convert.ToInt32(this.ddlOutreach.SelectedItem.Value), this.txtFeedBack.Text, this.commonAppSession.LoginUserInfoSession.UserID.ToString(), (this.txtTitle.Visible && this.txtTitle.Text.Length > 0) ? this.txtTitle.Text : null, is_restricted_store_state, out contact_log_pk, out error_message);

        if (return_value == -2)
        {
            this.assignedBusiness();
            this.displayOutreachTabs();

            if (!string.IsNullOrEmpty(contact_log_pk) && Convert.ToInt32(this.ddlOutreach.SelectedValue) == 3)
            {
                //string account_type = Convert.ToInt32(this.ddlOutreach.SelectedValue) == 3 ? "Local" : "Vote & Vax";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showContractInitiatedWarning('" + error_message + "', '" + contact_log_pk + "');", true);
            }
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);

            this.clearFields();
            this.bindOutreachStatusDropdown();
            return;
        }
        else if (return_value == -1)
        {
            this.clearFields();
            this.assignedBusiness();
            this.displayOutreachTabs();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Error: Could not log the Business Contact!');", true);
            return;
        }

        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.ddlOutreach.SelectedItem.Value == @"3")
        {
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
            Response.Redirect("walgreensClinicAgreement.aspx", false);
        }
        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" && this.ddlOutreach.SelectedItem.Value == @"3")
        {
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
            Response.Redirect("scheduleEventForm.aspx", false);
        }
        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.ddlOutreach.SelectedItem.Value == @"2")
        {
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
            Response.Redirect("walgreensFollowupEmail.aspx", false);
        }
        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.ddlOutreach.SelectedItem.Value == @"8")
        {
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
            Response.Redirect("walgreensCharityProgram.aspx", false);
        }
        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.ddlOutreach.SelectedItem.Value == @"12")
        {
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
            Response.Redirect("walgreensCommunityOutreachProgram.aspx", false);
        }

        this.clearFields();
        this.lblMessage.Text = (string)GetGlobalResourceObject("errorMessages", "logBusinessContactInserted");

        this.bindOutreachStatusDropdown();
        this.assignedBusiness();
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
        {
            this.workflowMonitor.setWorkFlowData();
        }

        this.displayOutreachTabs();
    }

    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.txtContactFirstName.Text = "";
        this.txtContactLastName.Text = "";
        this.lblBusinsessContactMsg.Visible = false;

        //if (walgreensHeaderCtrl.isddlTdVisible)
        //    this.txtStoreProfiles.Text = walgreensHeaderCtrl.selectedStoreOnNonAdmin;

        this.bindOutreachStatusDropdown();
        this.assignedBusiness();
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
        {
            this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses = this.dbOperation.getAssistedClinicBusinesses(this.commonAppSession.SelectedStoreSession.storeID);
            if (this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses != null && this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses.Rows.Count > 0)
            {
                this.assistedClinics = CommonExtensionsMethods.getJSONObject(this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses);
                ((LinkButton)this.walgreensHeaderCtrl.FindControl("lnkAssistedClinics")).Visible = true;
            }

            this.workflowMonitor.setWorkFlowData();

            if (this.hfSelectedBusinessTypeTabId.Value == "3")
                this.createDNCClientsJson();
        }
        this.displayOutreachTabs();
    }

    protected void btnHome_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("walgreensHome.aspx");
    }
    #endregion

    #region ----------------- PRIVATE METHODS ------------------
    /// <summary>
    /// This method will be used to display outreach tabs
    /// </summary>
    private void displayOutreachTabs()
    {
        StringBuilder sb = new StringBuilder();
        this.idNationalContracts.Visible = false;
        this.idAssignedClinics.Visible = false;
        this.dataTable = ApplicationSettings.getOutreachBusinessTypes(this.commonAppSession.SelectedStoreSession.OutreachProgramSelected);
        if (this.dataTable != null && this.dataTable.Rows.Count > 0)
        {
            foreach (DataRow row in this.dataTable.Rows)
            {
                if (row["businessType"].ToString().ToLower() == "national contracts")
                {
                    sb.Append("<li class='profileTabs' style='width: 150px; height: 40px; vertical-align:middle; text-align:center; '><a id='" + row["businessType"].ToString().Replace(" ", "") + "'  href='#id" + row["businessType"].ToString().Replace(" ", "") + "' runat='server' onclick=setDropDowns('" + row["category"].ToString().Trim() + "'," + row["fieldValue"].ToString().Trim() + ")>" + row["title"].ToString().Trim() + "</a></li>");
                    this.idNationalContracts.Visible = true;
                }
                else if (row["businessType"].ToString().ToLower() == "assigned clinics" && this.commonAppSession.SelectedStoreSession.AssignedClinicsJson != "[]")
                {
                    sb.Append("<li class='profileTabs' style='width: 150px; height: 40px; vertical-align:middle; text-align:center; '><a id='" + row["businessType"].ToString().Replace(" ", "") + "'  href='#id" + row["businessType"].ToString().Replace(" ", "") + "' runat='server' onclick=setDropDowns('" + row["category"].ToString().Trim() + "'," + row["fieldValue"].ToString().Trim() + ")>" + row["title"].ToString().Trim() + "</a></li>");
                    this.idAssignedClinics.Visible = true;
                }
                else if (row["businessType"].ToString().ToLower() == "outreach business")
                    sb.Append("<li class='profileTabs' style='width: 150px; height: 40px; vertical-align:middle; text-align:center; '><a id='" + row["businessType"].ToString().Replace(" ", "") + "'  href='#id" + row["businessType"].ToString().Replace(" ", "") + "' runat='server' onclick=setDropDowns('" + row["category"].ToString().Trim() + "'," + row["fieldValue"].ToString().Trim() + ")>" + row["title"].ToString().Trim() + "</a></li>");
            }
        }

        this.lblTabLinks.Text = sb.ToString();
    }

    /// <summary>
    /// Creates JSON to bind National Contract Clients to grid
    /// </summary>
    private void createDNCClientsJson()
    {
        this.nationalContracts = CommonExtensionsMethods.getJSONObject(this.dbOperation.getNationalContractClients);
    }

    /// <summary>
    /// Binding Store dropdown with out view state
    /// </summary>
    //private void storeDropDown()
    //{
    //  DataTable user_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
    //  this.ddlStoreList.DataSource = user_stores;
    // this.ddlStoreList.DataTextField = "address";
    // this.ddlStoreList.DataValueField = "storeid";
    // this.ddlStoreList.DataBind();
    //}

    /// <summary>
    /// Binds Outreach statuses to dropdown
    /// </summary>
    private void bindOutreachStatusDropdown()
    {
        this.dataTable = new DataTable();
        this.dataTable = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("isActive = 'True' AND category <> 'AC'").CopyToDataTable();

        //outreach status json serialization
        this.outreachStatuses = this.dataTable.getJSONObject();

        if (this.dataTable.Rows.Count > 0)
        {
            this.ddlOutreach.DataSource = (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1") ? this.dataTable.Select("category='SO'").CopyToDataTable() : this.dataTable.Select("category<>'SO' AND pk NOT IN (9, 10)").CopyToDataTable();
            this.ddlOutreach.DataTextField = "outreachStatus";
            this.ddlOutreach.DataValueField = "pk";
            this.ddlOutreach.DataBind();
            this.ddlOutreach.Items.RemoveAt(0);
            this.ddlOutreach.Items.Insert(0, new ListItem("-- Select Outreach Status --", "0"));
            this.commonAppSession.SelectedStoreSession.SelectedContactOutreachStatusId = 0;
        }
    }

    /// <summary>
    /// Displays selected stores compliance and completion information
    /// </summary>
    private void lastContact()
    {
        //string last_access = string.Empty;
        //string to_goal = string.Empty;
        //string to_complete = string.Empty;

        //this.dbOperation.getHeaderValues(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, ApplicationSettings.getOutreachStartDate, ApplicationSettings.getCurrentQuarter.ToString(), this.commonAppSession.LoginUserInfoSession.UserID, out last_access, out to_goal, out to_complete); //set the goal message here   

        //if (!string.IsNullOrEmpty(txtStoreProfiles.Text) && !this.txtStoreProfiles.Text.Contains("Nothing"))
        //    this.commonAppSession.SelectedStoreSession.storeName = txtStoreProfiles.Text;

        //if (!string.IsNullOrEmpty(last_access) && !this.txtStoreProfiles.Text.Contains("Nothing"))
        //    this.commonAppSession.SelectedStoreSession.lastStatusProvided = (string)GetGlobalResourceObject("errorMessages", "lastStatusProvided") + " " + last_access;
        //else
        //    this.commonAppSession.SelectedStoreSession.lastStatusProvided = "";

        //this.walgreensHeaderCtrl.strStoreName = this.commonAppSession.SelectedStoreSession.storeName;
        //this.walgreensHeaderCtrl.strLastStatusProvided = this.commonAppSession.SelectedStoreSession.lastStatusProvided;


        //if (string.IsNullOrEmpty(to_goal)) to_goal = "0";
        //if (string.IsNullOrEmpty(to_complete)) to_complete = "0";
        //Literal lblHeaderDetails = ((Literal)walgreensHeaderCtrl.FindControl("lblHeaderDetails"));
        //Literal lblLastStatusProvided = ((Literal)walgreensHeaderCtrl.FindControl("lblLastStatusProvided"));

        //if (!this.txtStoreProfiles.Text.Contains("Nothing"))
        //{
        //    if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
        //    {
        //        this.walgreensHeaderCtrl.strStoreGoalMsg += String.Format((string)GetGlobalResourceObject("errorMessages", "goalMessage"), to_goal, to_complete, "Q" + ApplicationSettings.getCurrentQuarter.ToString());
        //    }

        //    switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
        //    {
        //        case "district manager":
        //        case "healthcare supervisor":
        //        case "director – rx & retail ops":
        //        case "regional vice president":
        //        case "regional healthcare director":
        //            lblHeaderDetails.Text = "<span class='wagsAddress'> " + this.commonAppSession.LoginUserInfoSession.LocationType + " " + this.commonAppSession.LoginUserInfoSession.AssignedLocations + "</span><br />";
        //            if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
        //                lblLastStatusProvided.Text = (!string.IsNullOrEmpty(this.walgreensHeaderCtrl.strStoreGoalMsg) ? "<span class='statusLine'>" + this.walgreensHeaderCtrl.strStoreGoalMsg + "</span>" : "");
        //            break;
        //        default:
        //            lblHeaderDetails.Text = "<span class='wagsAddress'>" + this.walgreensHeaderCtrl.strStoreName + "</span><br />";
        //            lblLastStatusProvided.Text = "";
        //            lblLastStatusProvided.Text = "<span class='statusLine' width='200'>" + (!string.IsNullOrEmpty(this.walgreensHeaderCtrl.strStoreGoalMsg) ? this.walgreensHeaderCtrl.strStoreGoalMsg + "<br />" : "") + (!string.IsNullOrEmpty(this.walgreensHeaderCtrl.strLastStatusProvided) ? "<b>" + this.walgreensHeaderCtrl.strLastStatusProvided + "</b>" : (string.IsNullOrEmpty(this.walgreensHeaderCtrl.strStoreGoalMsg)) ? lblLastStatusProvided.Text : "") + "</span>";
        //            break;
        //    }
        //}
        //else
        //{
        //    //this.commonAppSession.SelectedStoreSession.storeGoalMsg = "";
        //    this.walgreensHeaderCtrl.strStoreGoalMsg = "";
        //    lblHeaderDetails.Text = "<span class='wagsAddress'>" + this.walgreensHeaderCtrl.strStoreName + "</span><br />";
        //}
        //this.commonAppSession.SelectedStoreSession.HeaderDetails = "";
        //this.commonAppSession.SelectedStoreSession.HeaderDetails = lblHeaderDetails.Text + "|" + lblLastStatusProvided.Text;
    }


    /// <summary>
    /// Binding businesses assigned to store to dropdown    
    /// </summary>
    private void assignedBusiness()
    {
        string store_state;
        int store_id = 0;
        Int32.TryParse(this.walgreensHeaderCtrl.storeId, out store_id);

        DataTable dt_filtered_business = new DataTable();
        DataTable dt_businesses = this.dbOperation.getAssignedBusinesses(store_id, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), out store_state);

        DataRow[] businesses = dt_businesses.Select("outreachEffort LIKE '%" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "%'");
        if (businesses.Count() > 0)
            dt_businesses = businesses.CopyToDataTable();
        else
            dt_businesses.Clear();

        DataView dv_businesses = dt_businesses.DefaultView;

        System.Web.UI.HtmlControls.HtmlTableRow assigned_clinic_row = (System.Web.UI.HtmlControls.HtmlTableRow)this.FindControl("toAssignedRow");
        assigned_clinic_row.Visible = false;
        System.Web.UI.HtmlControls.HtmlTableRow clinic_lead_row = (System.Web.UI.HtmlControls.HtmlTableRow)this.FindControl("toLeadRow");
        clinic_lead_row.Visible = false;

        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
            dv_businesses.Sort = "isExistingBusiness DESC";
        else
            dv_businesses.Sort = "sicPriority DESC";
        //dv_businesses.Sort = "isStandardized DESC";

        dv_businesses.RowFilter = "outreachStatusTitle NOT LIKE '%Business Closed%'";
        dt_filtered_business = dv_businesses.Table.Clone();
        foreach (DataRowView drowview in dv_businesses)
            dt_filtered_business.ImportRow(drowview.Row);

        DataView dv_business_closed = dt_businesses.DefaultView;
        dv_business_closed.RowFilter = "outreachStatusTitle LIKE '%Business Closed%'";

        foreach (DataRowView drowview in dv_business_closed)
            dt_filtered_business.ImportRow(drowview.Row);

        SelectedStoreInfo selected_store_info = new SelectedStoreInfo();
        selected_store_info.storeName = this.txtStoreProfiles.Text;
        selected_store_info.storeID = store_id;
        if (!this.txtStoreProfiles.Text.Contains("Nothing"))
        {
            selected_store_info.storeName = this.commonAppSession.SelectedStoreSession.storeName;
            selected_store_info.storeState = this.commonAppSession.SelectedStoreSession.storeState;
        }

        selected_store_info.getAssignedBusinesses = dt_filtered_business;
        selected_store_info.OutreachProgramSelectedId = this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId;
        selected_store_info.OutreachProgramSelected = this.commonAppSession.SelectedStoreSession.OutreachProgramSelected;
        selected_store_info.OutreachBusinessesJson = this.commonAppSession.SelectedStoreSession.OutreachBusinessesJson;
        selected_store_info.getAssistedClinicBusinesses = this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses;

        if (selected_store_info.getAssistedClinicBusinesses != null && selected_store_info.getAssistedClinicBusinesses.Rows.Count > 0)
            this.assistedClinics = CommonExtensionsMethods.getJSONObject(selected_store_info.getAssistedClinicBusinesses);

        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 3)
            this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;
        selected_store_info.SelectedBusinessTypeId = this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId;
        selected_store_info.SelectedContactLogPk = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk;
        selected_store_info.SelectedContactBusinessPk = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk;
        selected_store_info.SelectedBusinessAccountType = this.commonAppSession.SelectedStoreSession.SelectedBusinessAccountType;
        selected_store_info.SelectedContactOutreachEffort = this.commonAppSession.SelectedStoreSession.SelectedContactOutreachEffort;
        selected_store_info.SelectedContactOutreachStatusId = this.commonAppSession.SelectedStoreSession.SelectedContactOutreachStatusId;
        selected_store_info.HeaderDetails = this.commonAppSession.SelectedStoreSession.HeaderDetails;
        this.commonAppSession.SelectedStoreSession = selected_store_info;

        dt_businesses.Clear();
        dv_businesses = new DataView();
        dv_businesses = dt_filtered_business.DefaultView;
        //dv_businesses.Sort = "businessName ASC";
        dv_businesses.RowFilter = "outreachEffort LIKE '%" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "%'";
        dt_businesses = dv_businesses.Table.Clone();
        foreach (DataRowView drowview in dv_businesses)
            dt_businesses.ImportRow(drowview.Row);
        //dt_businesses.Select("outreachEffort LIKE '%" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "%'").CopyToDataTable();
        this.ddlAssignedBusiness.DataTextField = "businessName";
        this.ddlAssignedBusiness.DataValueField = "businessPk";
        this.ddlAssignedBusiness.DataSource = dt_businesses;
        this.ddlAssignedBusiness.DataBind();
        this.ddlAssignedBusiness.Items.Insert(0, new ListItem(" -- Select Business -- ", "0"));
        this.ddlAssignedBusiness = (DropDownList)ApplicationSettings.setItemToolTip(this.ddlAssignedBusiness);

        //Disabling "Add A Business" facility to restricted state stores
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == "IP" && ApplicationSettings.isRestrictedStoreState(store_state, this.commonAppSession.LoginUserInfoSession.UserRole))
            this.hfDisableAddBusinessToStoreState.Value = String.Format((string)GetGlobalResourceObject("errorMessages", "disableAddBusinessToStore"), (store_state == "MO" ? "Missouri" : "District of Columbia"), (store_state == "MO" ? "20" : "15"));
        else
            this.hfDisableAddBusinessToStoreState.Value = "";

        //Create Outreach Business JSON object serialization
        var json_serializer = new JavaScriptSerializer();
        List<Dictionary<string, object>> outreach_rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> outreach_row = null;

        List<Dictionary<string, object>> clinic_rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> clinic_row = null;

        int count = 0;
        string outreach_status_id = "";
        string contacts = "";
        string last_contact_grid = "";
        string str_contact_output = "";
        string outreach_status_id_grid = "";
        string outreach_statusTitle_grid = "";
        string outreach_status_title = "";
        string contacts_tool_tip = "";
        string last_contact_tool_tip = "";
        string outreach_status_grid_tool_tip = "";
        string outreach_contact_date = "";
        string account_Type = "";

        foreach (DataRow dt_row in dt_businesses.Rows)
        {
            if (Convert.ToInt32(dt_row["businessType"].ToString()) == 1)
            {
                outreach_row = new Dictionary<string, object>();

                foreach (DataColumn dt_column in dt_businesses.Columns)
                {
                    if (dt_column.ColumnName.ToLower() == "businesspk")
                        outreach_row.Add("businessId", dt_row["businessPk"].ToString().Trim());
                    if (dt_column.ColumnName.ToLower() == "businessname")
                        outreach_row.Add(dt_column.ColumnName, "<u style='cursor:hand'>" + ((Convert.ToInt32(dt_row["isStandardized"]) == 0) ? this.appSettings.removeLineBreaks(dt_row[dt_column].ToString().Replace("\"", "&quot;")) + " *" : this.appSettings.removeLineBreaks(dt_row[dt_column].ToString().Replace("\"", "&quot;"))) + "</u>".Trim());
                    else if (dt_column.ColumnName.ToLower() == "employmentsize")
                        outreach_row.Add(dt_column.ColumnName, (dt_row["employmentSize"] is DBNull || dt_row["employmentSize"].ToString().Equals("0")) ? "N/A" : dt_row["employmentSize"].ToString().Trim());
                    else if (dt_column.ColumnName.ToLower() == "outreacheffort")
                    {
                        count = 0;
                        outreach_status_id = "";
                        contacts = "";
                        last_contact_grid = "";
                        outreach_status_id_grid = "";
                        outreach_statusTitle_grid = "";
                        outreach_status_title = "";
                        contacts_tool_tip = "";
                        last_contact_tool_tip = "";
                        outreach_status_grid_tool_tip = "";
                        foreach (string str_outreach_program in dt_row["outreachEffort"].ToString().TrimStart(',').Split(','))
                        {
                            str_contact_output = dt_row["lastContact"].ToString().Split(',').Count() > 1 ? dt_row["lastContact"].ToString().Split(',')[count + 1].ToString() : "";

                            if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == str_outreach_program)
                            {
                                //contacts = contacts + "<a href='walgreensContactLog.aspx?businessId=" + dt_row["businessPk"] + "&outreachStatus=" + dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString() + "&outreachEffort=" + str_outreach_program + "'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";

                                last_contact_grid = last_contact_grid + "<span>" + str_contact_output + "</span><br/>";
                                outreach_status_id_grid = dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString();

                                outreach_status_id = dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString();
                                outreach_status_title = dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString();
                                contacts = contacts + "<a onClick='javascript:showBusinessContactLog(" + dt_row["businessPk"] + "," + outreach_status_id + ",3)' href='#'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Replace("IP", "IMZ") + "</a><br/>";

                                if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" && outreach_status_id == "3")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(1, " + contact_log_pk + ")' href='#' >" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (outreach_status_id == "2")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(2, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "8")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(4, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "3")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(3, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "11")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(5, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "12")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(6, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<span>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</span><br/>";

                                outreach_contact_date = str_contact_output;
                            }
                            else
                            {
                                outreach_status_id = dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString();
                                //contacts = contacts + "<a href='walgreensContactLog.aspx?businessId=" + dt_row["businessPk"] + "&outreachStatus=" + dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString() + "&outreachEffort=" + str_outreach_program + "'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                contacts = contacts + "<a onClick='javascript:showBusinessContactLog(" + dt_row["businessPk"] + "," + outreach_status_id + ",1)' href='#'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Replace("IP", "IMZ") + "</a><br/>";
                                last_contact_grid = last_contact_grid + "<span style='color:#999999'>" + str_contact_output + "</span><br/>";
                                outreach_statusTitle_grid = outreach_statusTitle_grid + "<span style='color:#999999'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</span><br/>";
                            }

                            if (str_outreach_program.ToLower() == "ip")
                            {
                                contacts_tool_tip = "Immunization Program-" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Split(' ')[1];
                                if (str_contact_output.Trim().Length != 0) last_contact_tool_tip = "Immunization Program-" + str_contact_output;
                                outreach_status_grid_tool_tip = "Immunization Program-" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString();
                            }
                            else if (str_outreach_program.ToLower() == "so")
                            {
                                if (contacts_tool_tip.Trim().Length != 0) contacts_tool_tip += "|";
                                contacts_tool_tip = contacts_tool_tip + "Senior Outreach-" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Split(' ')[1];

                                if (last_contact_tool_tip.Trim().Length != 0) last_contact_tool_tip += "|";
                                if (str_contact_output.Trim().Length != 0) last_contact_tool_tip = last_contact_tool_tip + "Senior Outreach-" + str_contact_output;

                                if (outreach_status_grid_tool_tip.Trim().Length != 0) outreach_status_grid_tool_tip += "|";
                                outreach_status_grid_tool_tip = outreach_status_grid_tool_tip + "Senior Outreach-" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString();
                            }
                            count++;
                        }
                        outreach_row.Add("contacts", contacts.Trim());
                        outreach_row.Add("lastContactGrid", last_contact_grid.Trim());
                        outreach_row.Add("lastContact", str_contact_output.Trim());
                        outreach_row.Add("outreachStatus", outreach_status_title.Trim());
                        outreach_row.Add("outreachStatusIdGrid", outreach_status_id_grid.Trim());
                        outreach_row.Add("outreachStatusTitleGrid", outreach_statusTitle_grid.Trim());
                        outreach_row.Add("outreachStatusId", outreach_status_id_grid.Trim());
                        outreach_row.Add("contactsToolTip", contacts_tool_tip.Trim());
                        outreach_row.Add("lastContactToolTip", last_contact_tool_tip.Trim());
                        outreach_row.Add("statusToolTip", outreach_status_grid_tool_tip.Trim());
                        outreach_row.Add("outreachContactDate", outreach_contact_date.Trim());
                    }
                    else if (dt_column.ColumnName.ToLower() != "contacts" && dt_column.ColumnName.ToLower() != "lastcontact" && dt_column.ColumnName.ToLower() != "outreachstatus" && dt_column.ColumnName.ToLower() != "businessPk")
                        outreach_row.Add(dt_column.ColumnName, dt_row[dt_column].ToString().Trim().Replace("\\", "&quot;"));
                }

                outreach_rows.Add(outreach_row);
            }
            else if (Convert.ToInt32(dt_row["businessType"].ToString()) == 2)
            {
                clinic_row = new Dictionary<string, object>();
                foreach (DataColumn dt_column in dt_businesses.Columns)
                {
                    if (dt_column.ColumnName.ToLower() == "businessname")
                        clinic_row.Add(dt_column.ColumnName, this.appSettings.removeLineBreaks(dt_row[dt_column].ToString().Replace("\"", "&quot;")).Trim());
                    else if (dt_column.ColumnName.ToLower() == "phone")
                        clinic_row.Add("contactPhone", dt_row["phone"]);
                    else if (dt_column.ColumnName.ToLower() == "contacts")
                        clinic_row.Add(dt_column.ColumnName, dt_row["contacts"].ToString().TrimStart(','));
                    else if (dt_column.ColumnName.ToLower() == "lastcontact")
                        clinic_row.Add(dt_column.ColumnName, dt_row["lastContact"].ToString().TrimStart(','));
                    else if (dt_column.ColumnName.ToLower() == "outreachstatustitle")
                    {
                        account_Type = dt_row["accountType"].ToString();
                        if (!string.IsNullOrEmpty(dt_row["leadStoreId"].ToString()))
                        {
                            if ((dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0].ToLower() == "contact client") && Convert.ToInt32(dt_row["leadStoreId"].ToString()) != Convert.ToInt32(dt_row["storeId"].ToString()))
                            {
                                if (Convert.ToInt32(dt_row["leadStoreId"].ToString()) != store_id)
                                {
                                    clinic_row.Add("contactStatus", "<span style='color:#5B9EC6'><b>*" + dt_row["leadStoreId"].ToString() + ":</b> </span>" + dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                                    assigned_clinic_row.Visible = true;
                                }
                                else
                                {
                                    clinic_row.Add("contactStatus", "<span style='color:#5B9EC6'><b>*" + dt_row["storeId"].ToString() + ":</b> </span>" + dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                                    clinic_lead_row.Visible = true;
                                }
                            }
                            else
                            {
                                clinic_row.Add("contactStatus", dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                            }
                        }
                        else
                        {
                            clinic_row.Add("contactStatus", dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                        }
                    }
                    else if (dt_column.ColumnName.ToLower() == "storeid" && Convert.ToInt32(dt_row["storeid"]) != store_id && Convert.ToInt32(dt_row["leadStoreId"]) == store_id)
                        clinic_row.Add("isClinicUnderLeadStore", "true");
                    else if (dt_column.ColumnName.ToLower() == "outreachstatus")
                        clinic_row.Add("outreachStatusId", dt_row["outreachstatus"].ToString().TrimStart(','));
                    //else if (dt_column.ColumnName.ToLower() == "naclinictotalimmadministered")
                    //    clinic_row.Add("totalImmAdministered", dt_row["naClinicTotalImmAdministered"].ToString().TrimStart(','));
                    else
                        clinic_row.Add(dt_column.ColumnName, dt_row[dt_column]);
                }
                clinic_rows.Add(clinic_row);

            }
        }
        this.commonAppSession.SelectedStoreSession.OutreachBusinessesJson = json_serializer.Serialize(outreach_rows).Replace(@"\""", @"&quot;").Replace("\''", "&#39;");
        this.commonAppSession.SelectedStoreSession.AssignedClinicsJson = json_serializer.Serialize(clinic_rows).Replace("\r\n", "").Replace("\n", "").Replace("\r", "").Replace(@"\""", @"&quot;").Replace("\''", "&#39;");

        this.lastContact();
        this.lblBusinsessContactMsg.Visible = this.commonAppSession.SelectedStoreSession.getAssignedBusinesses.Select("isStandardized = 0").Count() > 0;
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
        {
            this.imgPreviousClient.Src = "~/images/so_outreach_color_key_high_priority.gif";
            this.imgPreviousClient.Width = 172;
            this.imgPreviousClient.Alt = "High Priority";
            this.rowMessageSO.Visible = true;
            this.ltlMessageSO.Text = (string)GetGlobalResourceObject("errorMessages", "headerTitleSO");
            this.imgOutreachColorKey.Src = "~/images/so_outreach_color_key.gif";
            this.lblBusinsessContactMsg.Text = "* Business contacts added in addition to the assigned businesses.";
        }
        else
        {
            this.imgPreviousClient.Src = "~/images/outreach_color_key_previous_client.gif";
            this.imgPreviousClient.Alt = "Previous Client";
        }
        this.commonAppSession.SelectedStoreSession.referrerPath = "walgreensHome.aspx";
    }

    /// <summary>
    /// Clearing all fields once the Contact log information entered.
    /// </summary>
    private void clearFields()
    {
        this.ddlAssignedBusiness.ClearSelection();
        this.txtFeedBack.Text = "";
        this.txtContactFirstName.Text = "";
        this.txtContactLastName.Text = "";
        this.PickerAndCalendar1.getSelectedDate = DateTime.Now;
        this.ddlOutreach.ClearSelection();
    }

    /// <summary>
    /// This function will bind outreach data to dropdown
    /// </summary>
    private void bindOutreachDropDownOnRefreshWorkflow()
    {
        this.bindOutreachStatusDropdown();

        if (this.hfSelectedBusinessTypeTabId.Value == "3")
            this.createDNCClientsJson();
    }

    /// <summary>
    /// sets tool tip for each item in drop down list
    /// </summary>
    /// <param name="ddl_list"></param>
    public void setItemToolTip()
    {
        foreach (ListItem list_item in this.ddlAssignedBusiness.Items)
        {
            list_item.Attributes.Add("title", list_item.Text);
        }
    }

    #endregion

    [WebMethod]
    public static void setSelectedBusinessId(string business_id)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        int selected_business_id = 1;
        Int32.TryParse(business_id, out selected_business_id);
        if (Convert.ToInt32(selected_business_id) != 3)
            common_app_session.SelectedStoreSession.SelectedBusinessTypeId = selected_business_id;
    }

    [WebMethod]
    public static void setSelectedContactLogPk(string contact_log_pk)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        int selected_contact_log_pk = 0;
        Int32.TryParse(contact_log_pk, out selected_contact_log_pk);
        common_app_session.SelectedStoreSession.SelectedContactLogPk = selected_contact_log_pk;
    }

    [WebMethod]
    public static void setSelectedBusinessPk(string business_clinic_pk, string account_type, string outreach_status_id, string is_clinic_under_leadstore)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        int selected_business_pk = 0;
        try
        {
            Int32.TryParse(business_clinic_pk, out selected_business_pk);
            common_app_session.SelectedStoreSession.SelectedContactBusinessPk = selected_business_pk;
            common_app_session.SelectedStoreSession.SelectedBusinessAccountType = account_type;
            common_app_session.SelectedStoreSession.SelectedContactOutreachStatusId = Convert.ToInt32(outreach_status_id);
            //common_app_session.SelectedStoreSession.IsClinicUnderLeadStore= is_clinic_under_leadstore == "true" ? true : false;
        }
        catch (Exception ex)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Error("Error message: " + ex.Message + " Stack Trace: " + ex.StackTrace);
        }
    }

    [WebMethod]
    public static void setSelectedBusinessContactLog(string business_pk, string outreach_status_id, string outreach_effort_id)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        int selected_business_pk = 0;
        Int32.TryParse(business_pk, out selected_business_pk);
        common_app_session.SelectedStoreSession.SelectedContactBusinessPk = selected_business_pk;
        common_app_session.SelectedStoreSession.SelectedContactOutreachStatusId = Convert.ToInt32(outreach_status_id);
        if (Convert.ToInt32(outreach_status_id) > 0)
            common_app_session.SelectedStoreSession.SelectedContactOutreachEffort = (outreach_effort_id == "1" ? "SO" : "IP");
    }

    #region ----------------- PRIVATE VARIABLES -----------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    private DataTable dataTable;
    private WalgreenEmail walgreensEmail = null;
    protected string nationalContracts = "[]";
    protected string outreachStatuses = "[]";
    protected string assistedClinics = "[]";
    #endregion

    #region ----------------- Web Form Designer generated code -----------------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        this.workflowMonitor.btnRefreshHandler += new WorkflowMonitor.OnButtonClick(bindOutreachDropDownOnRefreshWorkflow);
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion
}