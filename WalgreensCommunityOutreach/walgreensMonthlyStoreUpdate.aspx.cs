﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using TdWalgreens;
using System.Web.Script.Serialization;
using System.Xml;
using System.Globalization;
using System.Web;

public partial class walgreensMonthlyStoreUpdate : System.Web.UI.Page
{
    #region --------------- PROTECTED EVENTS ----------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.resetValues();
            string storeupdate_files = Server.MapPath("~/storeUpdateFiles/");
            DirectoryInfo storeupdate_files_directory = new DirectoryInfo(storeupdate_files);
            IEnumerable<string> files = new[] { "--Select--" };
            IEnumerable<string> file_names = storeupdate_files_directory.GetFiles()
              .Where(f => f.Extension == ".txt" &&
                  (DateTime.ParseExact(f.Name.Split('_')[1].Split('.')[0].ToString(), "MMddyyyyHHmmss", CultureInfo.InvariantCulture)).Month == DateTime.Today.Month &&
                  (DateTime.ParseExact(f.Name.Split('_')[1].Split('.')[0].ToString(), "MMddyyyyHHmmss", CultureInfo.InvariantCulture)).Year == DateTime.Today.Year)
              .OrderBy(f => DateTime.ParseExact(f.Name.Split('_')[1].Split('.')[0].ToString(), "MMddyyyyHHmmss", CultureInfo.InvariantCulture))
              .Select(f => f.Name);
            if (file_names.Any())
                files = files.Concat(file_names);
            this.gridMainStoreData.Visible = false;
            this.lblUpdateCounts.Visible = false;

            this.ddlUploadedFiles.DataSource = files;
            this.ddlUploadedFiles.DataBind();
        }
        else
        {
            this.lblErrorNote.Text = "";
            string event_args = Request["__EVENTTARGET"];
            if (event_args.ToLower() == "storeupdate")
            {
                this.submitStoreUpdate();
            }
            else if (event_args.ToLower() == "updateinprogress")
            {
                this.updateStoreProfiles(Convert.ToInt32(Request["__EVENTARGUMENT"]));
            }
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (this.storeProfileUploadControl.HasFile)
        {
            this.resetValues();
            this.labelUploadedfile.Text = storeProfileUploadControl.FileName;
            this.hfUploadedTime.Value = DateTime.Now.ToString("MM-dd-yyyy").Replace("-", "") + DateTime.Now.ToString("HH-mm-ss").Replace("-", "");

            string file_path = Path.Combine(Server.MapPath("~/storeUpdateFiles/"), "TribuneDirect1_" + this.hfUploadedTime.Value);
            this.hfUploadedFile.Value = file_path + ".txt";
            this.storeProfileUploadControl.SaveAs(this.hfUploadedFile.Value);

            DataTable uploaded_data = this.uploadFile(this.hfUploadedFile.Value);
            if (uploaded_data.Rows.Count == 0)
            {
                File.Delete(this.hfUploadedFile.Value);
                this.resetValues();
                this.lblErrorNote.Text = "No Store Details Found.";
            }
            else
            {
                this.dumpNormalStoresToDatabase(uploaded_data);
                this.dtStoreUploadData = uploaded_data;

                this.gridMainStoreData.Visible = false;
                this.lblUpdateCounts.Visible = false;
                this.btnGeneratexlsReport.Visible = false;
                this.btnGeneratepdfReport.Visible = false;
                this.bindUploadedData();
            }
        }
        else
        {
            this.resetValues();
            this.lblErrorNote.Text = "Unknown Error. Please upload feed again or select from uploaded feeds";
        }
    }

    protected void btnSubmitReport_Command(object sender, CommandEventArgs e)
    {
        if (this.grdStoreUploadData.EditIndex == -1)
        {
            if (!string.IsNullOrEmpty(this.labelUploadedfile.Text))
            {
                this.hfJsonFileName.Value = this.labelUploadedfile.Text;

                if (e.CommandArgument.ToString() == "Submit")
                {
                    if ((this.grdStoreUploadData.Visible && this.grdStoreUploadData.Rows.Count > 0) || (this.gridMainStoreData.Visible && !string.IsNullOrEmpty(this.storeUpdateJsonPath)))
                    {
                        if (this.grdStoreUploadData.Visible && rbnIsPartial.SelectedIndex == 0)
                        {
                            this.lblErrorNote.Text = "Please View the report before submitting the feed for Full Update.";
                        }
                        else
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "submitStoreUpdateWarning('All the Store profiles will be modified. Press continue to submit the changes.');", true);
                    }
                    else
                        this.lblErrorNote.Text = "Unknown Error. Please upload feed again or select from uploaded feeds";
                }
                else
                {
                    this.loadStoreListReport();
                    this.btnViewReport.Visible = false;
                }
            }
            else
                this.lblErrorNote.Text = "Please upload feed or select from uploaded feeds";
        }
        else
            this.lblErrorNote.Text = "Please save the pending changes before submit.";
    }

    protected void ddlUploadedFiles_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.labelUploadedfile.Text = "";
        if (!ddlUploadedFiles.SelectedItem.Value.Contains("Select"))
        {
            string table_name = ddlUploadedFiles.SelectedItem.Value;
            this.labelUploadedfile.Text = table_name;
            this.hfUploadedTime.Value = (table_name.Split('.')[0]).Split('_')[1];
            DataTable dt_uploaded_data = this.uploadFile(Path.Combine(Server.MapPath("~/storeUpdateFiles/"), table_name));
            this.dtStoreUploadData = dt_uploaded_data;

            string json_path = Path.Combine(Server.MapPath("~/storeUpdateFiles/"), "StoreUpdateLive_" + this.hfUploadedTime.Value + ".json");

            if (!File.Exists(json_path))
            {
                this.rbnIsPartial.Enabled = true;
                this.gridMainStoreData.Visible = false;
                this.lblUpdateCounts.Visible = false;
                this.btnGeneratepdfReport.Visible = false;
                this.btnGeneratexlsReport.Visible = false;
                this.bindUploadedData();
                this.btnViewReport.Visible = true;
            }
            else
            {
                this.rbnIsPartial.Enabled = false;
                this.rbnIsPartial.SelectedIndex = 0;
                this.loadStoreListReport();
                this.btnViewReport.Visible = false;
            }
        }
        else
            this.resetValues();
    }

    protected void grdStoreUploadData_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.grdStoreUploadData.EditIndex = e.NewEditIndex;
        this.bindUploadedData();
    }

    protected void grdStoreUploadData_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DataTable dt_uploaded_data = this.dtStoreUploadData;

        //Update the values.
        GridViewRow row = this.grdStoreUploadData.Rows[e.RowIndex + (grdStoreUploadData.PageIndex * grdStoreUploadData.PageSize)];
        dt_uploaded_data.Rows[row.DataItemIndex]["location_number"] = ((TextBox)row.FindControl("txtLocationNumber")).Text;
        dt_uploaded_data.Rows[row.DataItemIndex]["address"] = ((TextBox)row.FindControl("txtAddress")).Text;
        dt_uploaded_data.Rows[row.DataItemIndex]["city"] = ((TextBox)row.FindControl("txtCity")).Text;
        dt_uploaded_data.Rows[row.DataItemIndex]["state"] = ((TextBox)row.FindControl("txtState")).Text;
        dt_uploaded_data.Rows[row.DataItemIndex]["zip"] = ((TextBox)row.FindControl("txtZip")).Text;
        // dt_uploaded_data.Rows[row.DataItemIndex]["community"] = ((TextBox)row.FindControl("txtCommunity")).Text;
        // dt_uploaded_data.Rows[row.DataItemIndex]["cl_name"] = ((TextBox)row.FindControl("txtClName")).Text;
        // dt_uploaded_data.Rows[row.DataItemIndex]["cl_home_store"] = ((TextBox)row.FindControl("txtClHomeStore")).Text;
        dt_uploaded_data.Rows[row.DataItemIndex]["district_number"] = ((TextBox)row.FindControl("txtDistrictNumber")).Text;
        //  dt_uploaded_data.Rows[row.DataItemIndex]["district_name"] = ((TextBox)row.FindControl("txtDistrictName")).Text;
        dt_uploaded_data.Rows[row.DataItemIndex]["region_number"] = ((TextBox)row.FindControl("txtRegionNumber")).Text;
        // dt_uploaded_data.Rows[row.DataItemIndex]["region_name"] = ((TextBox)row.FindControl("txtRegionName")).Text;
        dt_uploaded_data.Rows[row.DataItemIndex]["area_number"] = ((TextBox)row.FindControl("txtAreaNumber")).Text;
        // dt_uploaded_data.Rows[row.DataItemIndex]["area_name"] = ((TextBox)row.FindControl("txtAreaName")).Text;

        //Reset the edit index.
        this.grdStoreUploadData.EditIndex = -1;
        this.bindUploadedData();
        this.dumpNormalStoresToDatabase(dt_uploaded_data.Copy());
    }

    protected void grdStoreUploadData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdStoreUploadData.PageIndex = e.NewPageIndex;
        this.grdStoreUploadData.EditIndex = -1;
        this.bindUploadedData();
    }

    protected void grdStoreUploadData_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindUploadedData();
    }

    protected void grdStoreUploadData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Visible = (rbnIsPartial.SelectedItem.Text == "Partial");
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Visible = (rbnIsPartial.SelectedItem.Text == "Partial");
            string item = e.Row.Cells[0].Text;

            foreach (Button button in e.Row.Controls.OfType<Button>())
            {

                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete selected row?')){ return false; };";
                }
            }
        }
    }

    protected void grdStoreUploadData_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int index = Convert.ToInt32(e.RowIndex + (grdStoreUploadData.PageIndex * grdStoreUploadData.PageSize));
        DataTable dt_uploaded_data = this.dtStoreUploadData;
        dt_uploaded_data.Rows[index].Delete();
        this.dtStoreUploadData = dt_uploaded_data;
        this.bindUploadedData();
        this.dumpNormalStoresToDatabase(dt_uploaded_data.Copy());
    }

    protected void grdStoreUploadData_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdStoreUploadData.EditIndex = -1;
        this.bindUploadedData();
    }

    protected void PageDropDownList_SelectedIndexChanged(Object sender, EventArgs e)
    {
        // Retrieve the pager row.
        GridViewRow pager_row = this.grdStoreUploadData.BottomPagerRow;

        // Retrieve the PageDropDownList DropDownList from the bottom pager row.
        DropDownList page_list = (DropDownList)pager_row.Cells[0].FindControl("PageDropDownList");

        // Set the PageIndex property to display that page selected by the user.
        this.grdStoreUploadData.PageIndex = page_list.SelectedIndex;
    }

    protected void btnGenerateReport_Command(object sender, CommandEventArgs e)
    {
        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        DataSet ds_data_updates = new DataSet();
        //save json to database.
        if (!string.IsNullOrEmpty(this.storeUpdateJsonPath))
            this.storeUpdateReportData = System.IO.File.ReadAllText(this.storeUpdateJsonPath);

        if (this.storeUpdateReportData.Length < json_serializer.MaxJsonLength)
        {
            IEnumerable<StoreProfile> obj_store_collection = json_serializer.Deserialize<IEnumerable<StoreProfile>>(this.storeUpdateReportData);
            DataTable dt_data_updates = ApplicationSettings.CreateDataTable<StoreProfile>(obj_store_collection);
            dt_data_updates = dt_data_updates.Select("isCurrent = true").CopyToDataTable();
            dt_data_updates.Columns.Remove("id");
            dt_data_updates.Columns.Remove("isCurrent");
            dt_data_updates.Columns.Remove("isStoreUpdated");
            //Custom Sorting the Report
            string sort_expn = "IIF([type] = 'New', 1, " +
                               "IIF([type] LIKE 'Drop%', 2, " +
                               "IIF([type] LIKE 'Changed %', 3, " +
                               "4)))";

            dt_data_updates.Columns.Add("sortOrder", typeof(int), sort_expn);
            dt_data_updates.DefaultView.Sort = "sortOrder, type, storeId";
            dt_data_updates = dt_data_updates.DefaultView.ToTable();

            DataTable dt_percentage_changes = this.calculatePercentages(dt_data_updates);
            ds_data_updates.Tables.Add(dt_percentage_changes);
            ds_data_updates.Tables.Add(dt_data_updates);
        }

        if (e.CommandArgument.ToString() == "excel")
            this.generateExcelReport(ds_data_updates);
        else if (e.CommandArgument.ToString() == "pdf")
            this.generatePdfReport(ds_data_updates);
    }

    protected void rbnIsPartial_SelectedIndexChanged(object sender, EventArgs e)
    {
        string json_path = Path.Combine(Server.MapPath("~/storeUpdateFiles/"), "StoreUpdateLive_" + this.hfUploadedTime.Value + ".json");

        if (!File.Exists(json_path))
        {
            this.btnViewReport.Visible = (rbnIsPartial.SelectedItem.Text != "Partial");
            this.bindUploadedData();
        }
        else
        {
            this.loadStoreListReport();
            this.btnViewReport.Visible = false;
        }
    }

    #endregion

    #region --------------- PRIVATE METHODS ----------------
    /// <summary>
    /// Reset all values to initial state
    /// </summary>
    private void resetValues()
    {
        this.lblErrorNote.Text = "";
        this.hfJsonFileName.Value = "";
        this.hfUploadedFile.Value = "";
        this.hfUploadedTime.Value = "";
        this.ddlUploadedFiles.SelectedIndex = 0;
        this.grdStoreUploadData.Visible = false;
        this.gridMainStoreData.Visible = false;
        this.lblUpdateCounts.Visible = false;
        this.btnGeneratepdfReport.Visible = false;
        this.btnGeneratexlsReport.Visible = false;
        this.btnViewReport.Visible = (this.rbnIsPartial.SelectedItem.Text != "Partial");
        this.storeUpdateJsonPath = string.Empty;
        this.dtStoreUploadData = null;
        this.hfIsStoresUpdated.Value = "false";
        this.rbnIsPartial.Enabled = true;
    }

    /// <summary>
    /// Upload new feed or existing feed
    /// </summary>
    /// <param name="selected_file"></param>
    /// <returns></returns>
    private DataTable uploadFile(string selected_file)
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(selected_file) && File.Exists(selected_file))
        {
            string csv_data = File.ReadAllText(selected_file);
            string[] split_char = new string[] { "\r", "\n" };

            string[] rows;
            rows = csv_data.Split(split_char, StringSplitOptions.None);

            foreach (string row in rows)
            {
                if (!string.IsNullOrEmpty(row))
                {
                    if (row == rows[0])
                    {
                        string[] columns = row.Split('|');
                        foreach (string column in columns)
                        {
                            DataColumn dc = new DataColumn(column.Trim(), typeof(string));
                            dt.Columns.Add(dc);
                        }
                    }
                    else
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (string cell in row.Split('|'))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell.Trim();
                            i++;
                        }
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                DataColumn col_uploaded_on = new DataColumn("uploadedOn", typeof(System.String));
                col_uploaded_on.DefaultValue = this.hfUploadedTime.Value;
                dt.Columns.Add(col_uploaded_on);

                if (!dt.Columns.Contains("isCurrent"))
                {
                    DataColumn is_current = new DataColumn("isCurrent", typeof(System.Boolean));
                    is_current.DefaultValue = true;
                    dt.Columns.Add(is_current);
                }
                if (!dt.Columns.Contains("isStoreUpdated"))
                {
                    DataColumn is_store_updated = new DataColumn("isStoreUpdated", typeof(System.Boolean));
                    is_store_updated.DefaultValue = false;
                    dt.Columns.Add(is_store_updated);
                }
            }
        }
        return dt;
    }

    /// <summary>
    /// Binds uploaded Data to grid
    /// </summary>
    private void bindUploadedData()
    {
        DataTable uploaded_data = this.dtStoreUploadData;
        if ((uploaded_data != null) && (uploaded_data.Rows.Count > 0))
        {
            if (rbnIsPartial.SelectedItem.Text == "Partial" && !uploaded_data.Columns.Contains("isOnsite"))
            {
                uploaded_data.Columns.Add("isOnsite", typeof(bool));
                this.dtStoreUploadData = uploaded_data;
            }
            this.grdStoreUploadData.Visible = true;
            this.gridMainStoreData.Visible = false;
            this.lblUpdateCounts.Visible = false;
            this.btnGeneratepdfReport.Visible = false;
            this.btnGeneratexlsReport.Visible = false;
            this.grdStoreUploadData.DataSource = uploaded_data.Select("isCurrent=1").CopyToDataTable();
            this.grdStoreUploadData.DataBind();
        }
        else
        {
            this.grdStoreUploadData.Visible = false;
        }
    }

    /// <summary>
    /// Dumps uploaded store info to DB
    /// </summary>
    /// <param name="dt"></param>
    private void dumpNormalStoresToDatabase(DataTable dt, bool is_uploaded_data = true)
    {
        if (dt.Columns.Contains("COMMUNITY"))
            dt.Columns.Remove("COMMUNITY");
        if (dt.Columns.Contains("CL_NAME"))
            dt.Columns.Remove("CL_NAME");
        if (dt.Columns.Contains("CL_HOME_STORE"))
            dt.Columns.Remove("CL_HOME_STORE");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Select("LEN(TRIM(zip)) < 5"))
            {
                row["zip"] = row["zip"].ToString().PadLeft(5, '0');
            }
            dt.TableName = "StoreProfileUpdates";

            this.dbOperation.dumpStoresToDatabase(dt, this.hfUploadedTime.Value, is_uploaded_data);
        }
        else
        {
            File.Delete(this.hfUploadedFile.Value);
            this.resetValues();
            this.lblErrorNote.Text = "No Store Details Found.";
        }
    }

    /// <summary>
    /// Convert Datattypes to match onsite store types
    /// </summary>
    /// <param name="dt"></param>
    private void dumpOnsiteStoresToDatabase(DataTable dt)
    {
        if (dt.Columns.Contains("isOnsite"))
            dt.Columns.Remove("isOnsite");

        DataTable dt_onsite = dt.Clone();
        dt_onsite.Columns["LOCATION_NUMBER"].DataType = typeof(Int32);
        dt_onsite.Columns["DISTRICT_NUMBER"].DataType = typeof(Int32);
        dt_onsite.Columns["REGION_NUMBER"].DataType = typeof(Int32);
        dt_onsite.Columns["AREA_NUMBER"].DataType = typeof(Int32);
        foreach (DataRow row in dt.Rows)
        {
            foreach (DataColumn dc in dt.Columns)
            {
                if (string.IsNullOrEmpty(row[dc.ToString()].ToString()))
                    row[dc.ToString()] = null;
            }
            dt_onsite.ImportRow(row);
        }

        //string db_name = getXMLValues(this.commonAppSession.LoginUserInfoSession.UserName, "onsitestoreDB");
        //this.dbOperation.dumpToOnsiteStores(dt_onsite, db_name);
        XmlDocument onsite_doc = this.prepareOnsiteStoresXML(dt_onsite);
        int return_value = this.dbOperation.insertUpdateOnsiteStores(onsite_doc);
        if (return_value == 0)
            this.lblErrorNote.Text = "Onsite Stores Were Loaded Successfully.";
        else
            this.lblErrorNote.Text = "Unable to insert Onsite Pharmacies";
    }

    /// <summary>
    /// Generate Store Update Report
    /// </summary>
    private void loadStoreListReport()
    {
        this.gridMainStoreData.Visible = true;
        this.lblUpdateCounts.Visible = true;
        this.grdStoreUploadData.Visible = false;
        this.btnGeneratepdfReport.Visible = true;
        this.btnGeneratexlsReport.Visible = true;
        DataTable dt_storeupdate_changes = new DataTable();
        string json_path = Path.Combine(Server.MapPath("~/storeUpdateFiles/"), "StoreUpdateLive_" + this.hfUploadedTime.Value + ".json");
        int max_row_count;
        int.TryParse(this.getXMLValues(this.commonAppSession.LoginUserInfoSession.UserName, "storeupdatelimit"), out max_row_count);
        if (!string.IsNullOrEmpty(hfUploadedTime.Value))
        {
            if (!File.Exists(json_path))
            {
                dt_storeupdate_changes = this.dbOperation.getStoreListLoadReport(this.hfUploadedTime.Value, Convert.ToBoolean(this.rbnIsPartial.SelectedItem.Value));

                if (dt_storeupdate_changes != null && dt_storeupdate_changes.Rows.Count <= max_row_count)
                {
                    this.storeUpdateReportData = dt_storeupdate_changes.getJSONObject();
                    System.IO.File.WriteAllText(json_path, this.storeUpdateReportData);
                }
                else
                {
                    this.lblErrorNote.Text = "Maximum Store Update Limit Reached.";
                    this.btnGeneratexlsReport.Visible = false;
                    this.btnGeneratepdfReport.Visible = false;
                    this.deleteStoreFeed();
                    return;
                }
            }
            else
            {
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                this.storeUpdateReportData = System.IO.File.ReadAllText(json_path);
                if (!string.IsNullOrEmpty(this.storeUpdateReportData) && this.storeUpdateReportData.Length < json_serializer.MaxJsonLength)
                {
                    IEnumerable<StoreProfile> objStoreCollection = json_serializer.Deserialize<IEnumerable<StoreProfile>>(this.storeUpdateReportData);
                    dt_storeupdate_changes = ApplicationSettings.CreateDataTable<StoreProfile>(objStoreCollection);
                }
            }
            this.storeUpdateJsonPath = json_path;
        }

        DataTable dt_percentage_changes = this.calculatePercentages(dt_storeupdate_changes);
        if (dt_percentage_changes.Rows.Count == 0 || dt_storeupdate_changes.Rows.Count == 0 || this.hfIsStoresUpdated.Value == "true")
        {
            this.deleteStoreFeed();
            this.resetValues();
            this.lblErrorNote.Text = "No Store Updates Found.";
            this.btnGeneratexlsReport.Visible = false;
            this.btnGeneratepdfReport.Visible = false;
        }
        else
        {
            this.lblErrorNote.Text = "Store Profile Update Report Generated Successfully. Press Submit to apply these changes.";
            this.btnGeneratexlsReport.Visible = true;
            this.btnGeneratepdfReport.Visible = true;
        }
    }

    /// <summary>
    /// Sets onsite stores
    /// </summary>
    private void setStoreChecked()
    {
        DataTable dt_uploaded_data = this.dtStoreUploadData;
        if (rbnIsPartial.SelectedItem.Text == "Partial" && !dt_uploaded_data.Columns.Contains("isOnsite"))
        {
            dt_uploaded_data.Columns.Add("isOnsite", typeof(bool));
            this.dtStoreUploadData = dt_uploaded_data;
        }
        GridView grd_source = this.grdStoreUploadData;
        if (dt_uploaded_data.Rows.Count > 0 && grd_source.Rows.Count > 0)
        {
            foreach (GridViewRow row in grd_source.Rows)
            {
                CheckBox chk_select_onsite_store = (CheckBox)(row.FindControl("chkSelectOnsiteStore"));
                dt_uploaded_data.Rows[row.RowIndex]["isOnsite"] = chk_select_onsite_store.Checked ? 1 : 0;
            }
        }
    }

    /// <summary>
    /// Calculate percentage changes in store profiles
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    private DataTable calculatePercentages(DataTable dt)
    {
        DataTable dt_percentage_changes = new DataTable();
        dt_percentage_changes.Columns.Add("percentNew", typeof(double));
        dt_percentage_changes.Columns.Add("percentDropped", typeof(double));
        dt_percentage_changes.Columns.Add("percentChanged", typeof(double));
        if (dt.Rows.Count > 0)
        {
            int new_stores_count = dt.Select("type = 'New'").Length;
            int dropped_stores_count = dt.Select("type LIKE 'Drop%'").Length;
            int updated_stores_count = dt.Select("type LIKE 'Change%'").Length;
            int total_stores_count = Convert.ToInt32(this.dbOperation.getDeleteStoreProfileDetails(this.hfUploadedTime.Value, false, true).Rows[0][0]);

            this.lblUpdateCounts.Text = (((new_stores_count > 0) ? "New Stores : " + new_stores_count.ToString() + " <br/> " : "") +
                ((dropped_stores_count > 0) ? "Dropped Stores : " + dropped_stores_count.ToString() + " <br/> " : "") +
                ((updated_stores_count > 0) ? "Updated Stores : " + updated_stores_count.ToString() : ""));

            DataRow drPercentageChanges = dt_percentage_changes.NewRow();
            drPercentageChanges["percentNew"] = Math.Round((Decimal.Divide(new_stores_count, total_stores_count) * 100), 2);
            drPercentageChanges["percentDropped"] = Math.Round((Decimal.Divide(dropped_stores_count, total_stores_count) * 100), 2);
            drPercentageChanges["percentChanged"] = Math.Round((Decimal.Divide(updated_stores_count, total_stores_count) * 100), 2);

            dt_percentage_changes.Rows.Add(drPercentageChanges);
        }
        return dt_percentage_changes;
    }

    /// <summary>
    /// All the store profiles will be updated
    /// </summary>
    private void submitStoreUpdate()
    {
        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        if (this.rbnIsPartial.SelectedItem.Text == "Partial")
        {
            this.setStoreChecked();
            DataTable dt_uploaded_data = this.dtStoreUploadData;
            DataTable dt_onsite_stores_data = new DataTable();
            if (dt_uploaded_data.Rows.Count > 0 && dt_uploaded_data.Select("isOnsite = 1").Count() > 0)
            {
                dt_onsite_stores_data = dt_uploaded_data.Select("isOnsite = 1").CopyToDataTable();
                this.dumpOnsiteStoresToDatabase(dt_onsite_stores_data);
            }
            this.loadStoreListReport();
        }
        if (!string.IsNullOrEmpty(this.storeUpdateJsonPath))
            this.storeUpdateReportData = System.IO.File.ReadAllText(this.storeUpdateJsonPath);
        else
            this.loadStoreListReport();

        IEnumerable<StoreProfile> obj_store_collection = json_serializer.Deserialize<IEnumerable<StoreProfile>>(this.storeUpdateReportData);
        DataTable updated_report = ApplicationSettings.CreateDataTable<StoreProfile>(obj_store_collection);
        updated_report.Columns.Remove("id");
        if (!updated_report.Columns.Contains("uploadedOn"))
        {
            DataColumn col_uploaded_on = new DataColumn("uploadedOn", typeof(System.String));
            col_uploaded_on.DefaultValue = this.hfUploadedTime.Value;
            updated_report.Columns.Add(col_uploaded_on);
        }

        if (this.rbnIsPartial.SelectedItem.Text != "Partial")
        {
            DataTable original_report = this.dbOperation.getDeleteStoreProfileDetails(this.hfUploadedTime.Value, false, false);
            DataTable final_report = original_report.Copy();
            if (final_report.Rows.Count > 0 || updated_report.Rows.Count > 0)
            {
                for (int i = 0; i < updated_report.Rows.Count; i++)
                {
                    if (original_report.Select("storeId=" + updated_report.Rows[i]["storeId"].ToString() + " AND type = '" + updated_report.Rows[i]["type"].ToString() + "'").Count() == 0)
                        final_report.ImportRow(updated_report.Rows[i]);
                }
                foreach (DataRow row in updated_report.Select("isCurrent=false"))
                {
                    DataRow dr = final_report.Select("storeId=" + row["storeId"].ToString() +
                        " AND type = '" + row["type"].ToString() + "'").FirstOrDefault();
                    dr["isCurrent"] = false;

                }
                this.dumpNormalStoresToDatabase(final_report, false);
            }
        }
        else
        {
            if (File.Exists(this.hfUploadedFile.Value))
                File.Delete(this.hfUploadedFile.Value);
        }

        if (this.hfIsStoresUpdated.Value == "false" && updated_report.Rows.Count > 0)
            updateStoreProfiles(1);
        else
            this.hfIsStoresUpdated.Value = "true";
    }

    /// <summary>
    /// Processes Store Profiles Updates
    /// </summary>
    /// <param name="update_operation"></param>
    private void updateStoreProfiles(int update_operation)
    {
        try
        {
            int return_value = this.dbOperation.updateStoreProfiles(hfUploadedTime.Value, Convert.ToBoolean(this.rbnIsPartial.SelectedItem.Value), update_operation);

            if (return_value == 0)
            {
                if (update_operation == 1)
                {
                    this.lblErrorNote.Text = "Store Profiles were updated. Assigning Managers to the stores..";
                    this.hfUpdateProgress.Value = "2";
                }
                else if (update_operation == 2)
                {
                    this.lblErrorNote.Text = "All the Managers were updated successfully. GeoCoding the Stores..";
                    this.hfUpdateProgress.Value = "3";
                }
                else if (update_operation == 3)
                {
                    this.lblErrorNote.Text = "All the Store Profiles updates were loaded Successfully.";
                    this.hfUpdateProgress.Value = "0";
                    this.hfIsStoresUpdated.Value = "true";
                    this.dtStoreUploadData = null;
                    this.deleteStoreFeed();
                }
            }
            else
            {
                this.lblErrorNote.Text = "Unknown Error. Please Contact Administrator.";
                this.dtStoreUploadData = null;
                return;
            }
        }
        catch (Exception ex)
        {
            this.hfUpdateProgress.Value = "0";
            if (ex.Message.Contains("The wait operation timed out") || ex.Message.Contains("Timeout expired"))
            {
                if (update_operation == 3)
                    this.lblErrorNote.Text = "Stores were loaded Successfully, but GeoCoding is taking longer than expected. Please run the procedure manually.";
                else
                    this.lblErrorNote.Text = "The Store Update is in progress. Please Login after few minutes to check the updates.";
                this.hfIsStoresUpdated.Value = "true";
            }
            else
                throw ex;

            this.deleteStoreFeed();
        }
    }

    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }

    /// <summary>
    /// Generate excel from rdlc
    /// </summary>
    /// <param name="ds_data_updates"></param>
    private void generateExcelReport(DataSet ds_data_updates)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string filenameExtension;

        if (ds_data_updates.Tables.Count > 0)
        {
            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = "percentUpdateDataSet";
            rds1.Value = ds_data_updates.Tables[0];

            ReportDataSource rds2 = new ReportDataSource();
            rds2.Name = "storeUpdateDataSet";
            rds2.Value = ds_data_updates.Tables[1];

            ReportViewer report_viewer = new ReportViewer();
            report_viewer.ProcessingMode = ProcessingMode.Local;
            report_viewer.LocalReport.ReportPath = Server.MapPath("~/reports/StoreUpdateReport.rdlc");
            report_viewer.LocalReport.DataSources.Clear();
            report_viewer.LocalReport.DataSources.Add(rds2);
            report_viewer.LocalReport.DataSources.Add(rds1);

            byte[] bytes = report_viewer.LocalReport.Render("Excel", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            if (bytes != null)
            {
                Response.ContentType = "application/vnd.xls";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "TribuneDirect1_" + this.hfUploadedTime.Value + ".xls");
                Response.AddHeader("Content-Type", "application/Excel");
                Response.AddHeader("content-length", bytes.Length.ToString());
                Response.AddHeader("Connection", "Keep-Alive");
                Response.AddHeader("Cache-Control", "max-age=1");
                Response.BinaryWrite(bytes);
                bytes = null;
                GC.Collect();
                Response.End();
            }
        }
    }

    /// <summary>
    /// Generate pdf from rdlc
    /// </summary>
    /// <param name="ds_data_updates"></param>
    private void generatePdfReport(DataSet ds_data_updates)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string filenameExtension;
        if (ds_data_updates.Tables.Count > 0)
        {
            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = "percentUpdateDataSet";
            rds1.Value = ds_data_updates.Tables[0];

            ReportDataSource rds2 = new ReportDataSource();
            rds2.Name = "storeUpdateDataSet";
            rds2.Value = ds_data_updates.Tables[1];

            ReportViewer report_viewer = new ReportViewer();
            report_viewer.ProcessingMode = ProcessingMode.Local;
            report_viewer.LocalReport.ReportPath = Server.MapPath("~/reports/StoreUpdateReport.rdlc");
            report_viewer.LocalReport.DataSources.Clear();
            report_viewer.LocalReport.DataSources.Add(rds2);
            report_viewer.LocalReport.DataSources.Add(rds1);

            byte[] bytes = report_viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            if (bytes != null)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "TribuneDirect1_" + this.hfUploadedTime.Value + ".pdf");
                Response.AddHeader("Content-Transfer-Encoding", "BINARY");
                Response.AddHeader("content-length", bytes.Length.ToString());
                Response.AddHeader("Connection", "Keep-Alive");
                Response.AddHeader("Cache-Control", "max-age=1");
                Response.BinaryWrite(bytes);
                bytes = null;
                GC.Collect();
                Response.End();
            }
        }
    }

    /// <summary>
    /// Gets the values from xml config
    /// </summary>
    /// <param name="user_name"></param>
    /// <param name="xml_key"></param>
    /// <returns></returns>
    private string getXMLValues(string user_name, string xml_key)
    {
        XmlDocument client_services_xdoc = new XmlDocument();
        string return_val = string.Empty;
        client_services_xdoc.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml"));

        if (client_services_xdoc.SelectNodes("//clientAction[@name='StoreProfileUpdate']") != null)
        {
            XmlNodeList node_list = client_services_xdoc.SelectNodes("//clientAction[@name='StoreProfileUpdate']/client");

            if (!string.IsNullOrEmpty(user_name))
                node_list = client_services_xdoc.SelectNodes("//clientAction[@name='StoreProfileUpdate']/client[@username = '" + user_name + "']");

            foreach (XmlNode node in node_list)
            {
                if (node.Attributes[xml_key] != null)
                    return_val = node.Attributes[xml_key].Value;
            }
        }
        return return_val;
    }

    /// <summary>
    /// Prepares Oniste Stores Data
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    private XmlDocument prepareOnsiteStoresXML(DataTable dt)
    {
        XmlDocument profile_doc = new XmlDocument();
        XmlElement store_profile_element = store_profile_element = profile_doc.CreateElement("onsitestoreprofiles");
        foreach (DataRow row in dt.Rows)
        {
            XmlElement store_profile = profile_doc.CreateElement("store");
            store_profile.SetAttribute("LOCATION_NUMBER", row["LOCATION_NUMBER"].ToString());
            store_profile.SetAttribute("ADDRESS", row["ADDRESS"].ToString());
            store_profile.SetAttribute("CITY", row["CITY"].ToString());
            store_profile.SetAttribute("STATE", row["STATE"].ToString());
            store_profile.SetAttribute("ZIP", row["ZIP"].ToString());
            if (dt.Columns.Contains("COMMUNITY"))
                store_profile.SetAttribute("COMMUNITY", row["COMMUNITY"].ToString());
            if (dt.Columns.Contains("CL_NAME"))
                store_profile.SetAttribute("CL_NAME", row["CL_NAME"].ToString());
            if (dt.Columns.Contains("CL_HOME_STORE"))
                store_profile.SetAttribute("CL_HOME_STORE", row["CL_HOME_STORE"].ToString());

            store_profile.SetAttribute("DISTRICT_NUMBER", row["DISTRICT_NUMBER"].ToString());
            store_profile.SetAttribute("DISTRICT_NAME", row["DISTRICT_NAME"].ToString());
            store_profile.SetAttribute("REGION_NUMBER", row["REGION_NUMBER"].ToString());
            store_profile.SetAttribute("REGION_NAME", row["REGION_NAME"].ToString());
            store_profile.SetAttribute("AREA_NUMBER", row["AREA_NUMBER"].ToString());
            store_profile.SetAttribute("AREA_NAME", row["AREA_NAME"].ToString());
            store_profile_element.AppendChild(store_profile);
        }
        profile_doc.AppendChild(store_profile_element);
        return profile_doc;
    }

    /// <summary>
    /// Deletes the uploaded feed
    /// </summary>
    private void deleteStoreFeed()
    {
        string storeupdate_files = Server.MapPath("~/storeUpdateFiles/");
        DirectoryInfo storeupdate_files_directory = new DirectoryInfo(storeupdate_files);
        if (!string.IsNullOrEmpty(this.hfUploadedTime.Value))
        {
            var store_files = storeupdate_files_directory.GetFiles().Where(f => f.Name.Contains(this.hfUploadedTime.Value));
            if (store_files.Count() > 0)
                store_files.AsParallel().Where(f => f.Extension == ".txt").ForAll(f => f.Delete());
        }
    }
    #endregion

    #region ----------------- PRIVATE VARIABLES -----------------
    private DBOperations dbOperation = null;
    protected string storeUpdateReportData = "[]";
    private AppCommonSession commonAppSession = null;
    private DataTable dtStoreUploadData
    {
        get
        {
            if (HttpContext.Current.Session["uploadedData"] == null)
            {
                return new DataTable();
            }
            else
            {
                return (DataTable)HttpContext.Current.Session["uploadedData"];
            }
        }
        set
        {
            HttpContext.Current.Session["uploadedData"] = value;
        }
    }
    private string storeUpdateJsonPath
    {
        get
        {
            if (HttpContext.Current.Session["JSONFilePath"] == null)
            {
                return string.Empty;
            }
            else
            {
                return HttpContext.Current.Session["JSONFilePath"].ToString();
            }
        }
        set
        {
            HttpContext.Current.Session["JSONFilePath"] = value;
        }
    }
    #endregion

    #region ----------------- Web Form Designer generated code -----------------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.dbOperation = new DBOperations();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
    #endregion
}