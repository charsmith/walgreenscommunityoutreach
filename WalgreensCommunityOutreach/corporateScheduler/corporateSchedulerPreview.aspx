﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="corporateSchedulerPreview.aspx.cs" Inherits="corporateScheduler_corporateSchedulerPreview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Walgreens- Onsite Clinic Scheduling</title>
 <link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
 <link href="../css/wags.css" rel="stylesheet" type="text/css" />
 <link href="../css/theme.css" rel="stylesheet" type="text/css" />

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <div>
      <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
          <td colspan="2" bgcolor="#FFFFFF">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="800px">
              <tr>
                <td>
                  <table border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow" width="800px">
                    <tr>
                      <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px"><asp:Image ID="clientLogoPreview" runat="server" /></td>
                      <td class="formFields2" align="right" valign="top" bgcolor="#FFFFFF" style="padding:8px">
                        <a class="closeButton" href="javascript:window.close();">Close</a>                            
                      </td>
                    </tr>
                    <tr>
                      <td id="bannerPreview" runat="server" colspan="2" align="left" style="height: 2px; padding-top:0px; padding-bottom: 0px; padding-right: 24px; padding-left: 24px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;" valign="top" >&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="headlineText" ><%=HeaderTextPreview%></td>
                        </tr>
                        <tr>
                          <td class="bodyText"><%=bodyTextPreview%>
                            <p align="center" style="padding-top:24px"><asp:LinkButton ID="lnkScheduleAppointment" PostBackUrl="walgreensSchedulerRegistration.aspx" runat="server" CssClass="wagsSchedBigButton" Enabled="false" >Schedule Your Appointment</asp:LinkButton></p></td>
                        </tr>
                        <tr>
                          <td class="footerText"><%=footerTextPreview%></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
    </form>
</body>
</html>
