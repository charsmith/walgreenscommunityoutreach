﻿using System;
using System.Xml;
using tdEmailLib;
using TdWalgreens;
using TdApplicationLib;
using System.Text;
using System.Collections.Generic;
using System.Configuration;
using System.Xml.XPath;
using System.Web;
using NLog;

public partial class corporateScheduler_walgreensSchedulerConfirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.hfScheduledExistingApptXML.Value = Request.Form["hfScheduledExistingApptXML"].ToString();
            this.setPageStylesAndValidators();
            this.displayAppointmentDetails();
        }
    }

    #region ------------------ PRIVATE FUNCTION ------------------

    /// <summary>
    /// Sets page styles
    /// </summary>
    private void setPageStylesAndValidators()
    {
        this.logger.Info("Method {0} accessed from {1} page - START", "setPageStylesAndValidators",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));
        this.clientSchedulerXML.LoadXml(this.commonAppSession.SelectedStoreSession.schedulerDesignXML);

        //Set page Logos & styles
        this.bannerPreview.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
        this.bannerPreview.Style.Add("color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerTextColor"].Value);
        if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value))
            this.clientLogo.Visible = false;
        else
            this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=" + this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value + "&extraQS=" + DateTime.Now.Second;

        this.varFormLink = ((ConfigurationManager.AppSettings["EmailURL"] != "") ? ConfigurationManager.AppSettings["EmailURL"].ToString() : "http://wagoutreach.com/") + "/controls/ResourceFileHandler.ashx?Path=2015_16/VAR_Form_2015.pdf";

        this.lnkPrintConfirmation.BackColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
        this.lnkPrintConfirmation.ForeColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);
        this.lnkEditAppointment.BackColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
        this.lnkEditAppointment.ForeColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);
        this.anchorPrintVarForm.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
        this.anchorPrintVarForm.Style.Add("color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);
        this.anchorReviewVISForm.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
        this.anchorReviewVISForm.Style.Add("color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);
        this.logger.Info("Method {0} accessed from {1} page - END", "setPageStylesAndValidators",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));
    }

    /// <summary>
    /// Displays scheduled appointment details
    /// </summary>
    private void displayAppointmentDetails()
    {
        this.logger.Info("Method {0} accessed from {1} page - START", "displayAppointmentDetails",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));                
        string patient_name, appt_address1, appt_address2;
        bool is_cancelled = false;
        StringBuilder reg_details = new StringBuilder();

        // this.clientSchedulerXML.LoadXml(this.commonAppSession.SelectedStoreSession.scheduledExistingApptXML.CreateNavigator().OuterXml);
        if (!string.IsNullOrEmpty(this.hfScheduledExistingApptXML.Value))
        {
            this.clientSchedulerXML.LoadXml(HttpUtility.UrlDecode(this.hfScheduledExistingApptXML.Value));
            this.hfSchedulerApptPk.Value = ((XmlDocument)this.clientSchedulerXML).SelectSingleNode("//field[@name='apptPk']").Attributes["value"].Value.Trim();
            if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptDate']").Attributes["value"].Value) && string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptTimeOfDay']").Attributes["value"].Value) && string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptTime']").Attributes["value"].Value))
                is_cancelled = true;

            this.headerText = (is_cancelled) ? (string)GetGlobalResourceObject("errorMessages", "scheduleCancelledHeaderText") : (string)GetGlobalResourceObject("errorMessages", "scheduleConfirmationHeaderText");
            this.headerInfo = (is_cancelled) ? (string)GetGlobalResourceObject("errorMessages", "scheduleCancelledHeaderInfo") : (string)GetGlobalResourceObject("errorMessages", "scheduleConfirmationHeaderInfo");
            this.lblConfirmationId.Text = this.clientSchedulerXML.SelectSingleNode(".//field[@name='confirmationId']").Attributes["value"].Value;
            this.hflblConfirmationId.Value = this.clientSchedulerXML.SelectSingleNode(".//field[@name='confirmationId']").Attributes["value"].Value;

            foreach (KeyValuePair<string, string> appt_details in ApplicationSettings.getSchedulerApptFields)
            {
                if (appt_details.Key == "name")
                {
                    patient_name = !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='firstName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='firstName']").Attributes["value"].Value + " " : "";
                    patient_name += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='middleName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='middleName']").Attributes["value"].Value + " " : "";
                    patient_name += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='lastName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='lastName']").Attributes["value"].Value : "";

                    if (!string.IsNullOrEmpty(patient_name.Trim()))
                    {
                        reg_details.Append("<tr><td width='30%' align='left' valign='top' style='padding-left:24px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>" + appt_details.Value + ":</td>");
                        reg_details.Append("<td width='70%' align='left' valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>" + patient_name.Trim() + "</td></tr>");
                    }
                }
                else if (appt_details.Key != "address")
                {
                    if (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='" + appt_details.Key + "']").Attributes["value"].Value))
                    {
                        reg_details.Append("<tr><td width='30%' align='left' valign='top' style='padding-left:24px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>" + appt_details.Value + ":</td>");
                        reg_details.Append("<td width='70%' align='left' valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>" + this.clientSchedulerXML.SelectSingleNode(".//field[@name='" + appt_details.Key + "']").Attributes["value"].Value + "</td></tr>");
                    }
                }
                else
                {
                    appt_address1 = !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='address1']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='address1']").Attributes["value"].Value + ", " : "";
                    appt_address1 += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='address2']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='address2']").Attributes["value"].Value + " " : "";

                    appt_address2 = !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='city']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='city']").Attributes["value"].Value + ", " : "";
                    appt_address2 += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='state']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='state']").Attributes["value"].Value + " " : "";
                    appt_address2 += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='zipCode']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='zipCode']").Attributes["value"].Value + " " : "";

                    appt_address1 = (!string.IsNullOrEmpty(appt_address1.Trim()) ? appt_address1.Trim().TrimEnd(',') + "<br />" + appt_address2.Trim().TrimStart(',') : appt_address2.Trim().TrimStart(','));

                    if (!string.IsNullOrEmpty(appt_address1))
                    {
                        reg_details.Append("<tr><td width='30%' align='left' valign='top' style='padding-left:24px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>Address:</td>");
                        reg_details.Append("<td width='70%' align='left' valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>" + appt_address1 + "</td></tr>");
                    }
                }
            }
            this.contactInformation = reg_details.ToString();

            this.lblAppointmentTime.Text = (is_cancelled) ? "Cancelled" : this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptDate']").Attributes["value"].Value + " " + this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptTime']").Attributes["value"].Value;
            this.lblLocation.Text = (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicLocationName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicLocationName']").Attributes["value"].Value + ",<br />" : "") + this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicLocation']").Attributes["value"].Value;
            this.lblAppointmentRoom.Text = this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptRoom']").Attributes["value"].Value;
        }
        this.logger.Info("Method {0} accessed from {1} page - END", "displayAppointmentDetails",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path)); 
    }

    #endregion

    #region ------------------ PRIVATE VARIABLES ------------------
    private AppCommonSession commonAppSession = null;
    private XmlDocument clientSchedulerXML;
    protected string scheduledExistingApptXML
    {
        get
        {
            return ViewState["scheduledExistingApptXML"] != null ? ViewState["scheduledExistingApptXML"].ToString() : null;
        }
        set
        {
            ViewState["scheduledExistingApptXML"] = value;
        }
    }
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region ------------------ PUBLIC VARIABLES ------------------
    public string contactInformation;
    public string headerText;
    public string headerInfo;
    public string varFormLink;

    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.clientSchedulerXML = new XmlDocument();
        if (!IsPostBack)
        {
            if (Request.UrlReferrer != null && !string.IsNullOrEmpty(Request.UrlReferrer.ToString()))
            {
                if (Request.UrlReferrer.AbsolutePath.Substring(Request.UrlReferrer.AbsolutePath.LastIndexOf('/') + 1) != "walgreensSchedulerRegistration.aspx")
                    Response.Redirect("walgreensSchedulerLanding.aspx");
            }
            else
                Response.Redirect("walgreensSchedulerLanding.aspx");
        }
    }
    #endregion
}