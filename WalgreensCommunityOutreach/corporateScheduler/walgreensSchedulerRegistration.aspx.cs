﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using TdWalgreens;
using System.Data;
using TdApplicationLib;
using System.Web;
using tdEmailLib;
using System.Configuration;
using System.Collections.Generic;
using System.Xml.Linq;
using System.IO;
using System.Text;
using NLog;

public partial class corporateScheduler_walgreensSchedulerRegistration : Page
{
    #region ----------------- PROTECTED EVENTS -----------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (this.commonAppSession != null && this.commonAppSession.SelectedStoreSession != null && this.commonAppSession.SelectedStoreSession.schedulerDesignXML != null)
            {
                this.logger.Info("Method {0} accessed from {1} page - START", "Page_Load",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));
                this.clientSchedulerXML.LoadXml(this.commonAppSession.SelectedStoreSession.schedulerDesignXML);
                this.setPageStylesAndValidators();
                this.bindClinicLocationStates();
                this.ddlState.bindStates();
                this.populatedDobYearDropdown();

                //Create existing user xml
                this.clientSchedulerXML = null;
                if (Request.Form["hfscheduledExistingApptXML"] != null)
                {
                    XmlDocument appt_xml_doc = new XmlDocument();
                    appt_xml_doc.LoadXml(HttpUtility.UrlDecode(Request.Form["hfscheduledExistingApptXML"].ToString()));
                    this.scheduledApptXML = appt_xml_doc;
                }

                if (this.schedulerClinics.Tables.Count > 3)
                {
                    if (this.schedulerClinics.Tables[3] != null && this.schedulerClinics.Tables[3].Rows.Count > 0)
                    {
                        if (this.scheduledApptXML != null)
                        {
                            this.clientSchedulerXML = (XmlDocument)this.scheduledApptXML;
                            this.scheduledExistingApptXML = this.scheduledApptXML;
                            if (!string.IsNullOrEmpty(Request.Form["hflblConfirmationId"]))
                            {
                                if (Request.Form["hflblConfirmationId"].ToString() == this.clientSchedulerXML.SelectSingleNode(".//field[@name='confirmationId']").Attributes["value"].Value)
                                    this.displayRegistrationDetails();
                                else
                                {
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('" + (string)GetGlobalResourceObject("errorMessages", "multipleTabs") + "');window.close();", true);
                                    return;
                                }
                            }
                        }
                        else
                        {
                            this.prepareExistingUserApptXML(this.schedulerClinics.Tables[3]);
                            this.clientSchedulerXML = (XmlDocument)this.scheduledExistingApptXML;
                            this.scheduledApptXML = (IXPathNavigable)this.clientSchedulerXML;
                            this.displayRegistrationDetails();
                        }
                        this.hfScheduledExistingApptXML.Value = HttpUtility.UrlEncode(this.scheduledExistingApptXML.CreateNavigator().OuterXml.ToString());
                    }
                }
                this.logger.Info("Method {0} accessed from {1} page - END", "Page_Load",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));
            }
            else
                Response.Redirect("../auth/sessionTimeout.aspx");

            if (!string.IsNullOrEmpty(this.hfSchedulerApptPk.Value) && this.hfSchedulerApptPk.Value != "0")
            {
                this.lnkCancelAppointment.Visible = true;
                this.lnkCancelAppointment.CausesValidation = false;
            }
            if (this.ddlTimeOfDay.SelectedValue == "0" && this.ddlAvailableTimes.SelectedValue == "0")
                this.lnkCancelAppointment.Visible = false;
        }
        else
        {
            if(this.schedulerClinics == null)
            {
                this.schedulerClinics = this.commonAppSession.SelectedStoreSession.schedulerClinics;
            }
        }
    }

    protected void lnkScheduleAppointment_Click(object sender, EventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page - START", "lnkScheduleAppointment_Click",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));
        int return_value = 0;
        int appt_pk = 0;
        string confirmation_id = string.Empty;
        this.scheduledApptXML = this.prepareAppointmentXML();
        if (string.IsNullOrEmpty(((XmlDocument)scheduledApptXML).InnerXml))
            return;
        if (this.compareAppointmentXML())
        {
            return_value = this.dbOperation.addScheduledEeApptDetails(this.scheduledApptXML, out appt_pk, out confirmation_id);

            this.logger.Info("Database operation {0} done with return value {1}", "addScheduledEeApptDetails", return_value);

            if (return_value == 0 || return_value == -4)
            {
                this.hfSchedulerApptPk.Value = appt_pk.ToString();
                ((XmlDocument)this.scheduledApptXML).SelectSingleNode(".//field[@name='confirmationId']").Attributes["value"].Value = confirmation_id.ToString();
                ((XmlDocument)this.scheduledApptXML).SelectSingleNode(".//field[@name='apptPk']").Attributes["value"].Value = appt_pk.ToString();
                ((XmlDocument)this.scheduledApptXML).SelectSingleNode(".//field[@name='clinicGroupId']").Attributes["value"].Value = this.hfcGroupId.Value;

                this.scheduledExistingApptXML = this.scheduledApptXML;
                this.clientSchedulerXML.LoadXml(this.scheduledApptXML.CreateNavigator().OuterXml);
                if (return_value == -4)
                {
                    int clinic_pk, clinic_store_id;
                    string business_name = string.Empty;
                    int.TryParse(this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicPk']").Attributes["value"].Value, out clinic_pk);
                    int.TryParse(this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicStoreId']").Attributes["value"].Value, out clinic_store_id);
                    this.sendMaxImmunizersReachedEmail(clinic_pk, clinic_store_id, business_name);
                }
                this.sendApptConfirmationEmail(appt_pk, confirmation_id, false);
            }
            else if (return_value == -2)
            {
                this.clientSchedulerXML = (XmlDocument)this.scheduledApptXML;
                this.displayRegistrationDetails();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('" + (string)GetGlobalResourceObject("errorMessages", "apptNotAvailable") + "');", true);
                return;
            }
            else if (return_value == -3)
            {
                this.clientSchedulerXML = (XmlDocument)this.scheduledApptXML;
                this.displayRegistrationDetails();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('" + (string)GetGlobalResourceObject("errorMessages", "emailNotAvailable") + "');", true);
                return;
            }
        }
        else
            ((XmlDocument)this.scheduledExistingApptXML).SelectSingleNode("//field[@name='clinicLocation']").Attributes["value"].Value = this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "'").ElementAt(0)["clinicAddress"].ToString();

        this.hfScheduledExistingApptXML.Value = HttpUtility.UrlEncode(this.scheduledExistingApptXML.CreateNavigator().OuterXml.ToString());
        this.lnkScheduleAppointment.PostBackUrl = "~/corporateScheduler/walgreensSchedulerConfirmation.aspx";

        this.logger.Info("Method {0} accessed from {1} page - END", "lnkScheduleAppointment_Click",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));
            
    }

    protected void lnkCancelAppointment_Click(object sender, EventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page - START", "lnkCancelAppointment_Click",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path));
        
        int return_value = 0, clinic_pk = 0;
        string confirmation_id = string.Empty;

        if (this.lnkCancelAppointment.Visible)
        {
            this.scheduledExistingApptXML = this.prepareAppointmentXML();
            if (((XmlDocument)this.scheduledExistingApptXML) != null && ((XmlDocument)this.scheduledExistingApptXML).HasChildNodes)
            {
                int.TryParse(((XmlDocument)this.scheduledExistingApptXML).SelectSingleNode("//field[@name='clinicPk']").Attributes["value"].Value, out clinic_pk);
                confirmation_id = ((XmlDocument)this.scheduledExistingApptXML).SelectSingleNode("//field[@name='confirmationId']").Attributes["value"].Value.Trim();
            }

            return_value = this.dbOperation.cancelScheduledAppointment(confirmation_id.Trim(), clinic_pk, !string.IsNullOrEmpty(this.hfSchedulerApptPk.Value) ? Convert.ToInt32(this.hfSchedulerApptPk.Value) : 0);
            this.logger.Info("Database operation {0} done with return value {1}", "cancelScheduledAppointment", return_value);
            if (return_value == 0)
            {
                ((XmlDocument)this.scheduledExistingApptXML).SelectSingleNode("//field[@name='apptDate']").Attributes["value"].Value = string.Empty;
                ((XmlDocument)this.scheduledExistingApptXML).SelectSingleNode("//field[@name='apptTimeOfDay']").Attributes["value"].Value = string.Empty;
                ((XmlDocument)this.scheduledExistingApptXML).SelectSingleNode("//field[@name='apptTime']").Attributes["value"].Value = string.Empty;
                ((XmlDocument)this.scheduledExistingApptXML).SelectSingleNode("//field[@name='clinicLocation']").Attributes["value"].Value = this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "'").ElementAt(0)["clinicAddress"].ToString();
                this.clientSchedulerXML.LoadXml(this.scheduledExistingApptXML.CreateNavigator().OuterXml);

                //Sends cancel appointment email
                this.sendApptConfirmationEmail(!string.IsNullOrEmpty(this.hfSchedulerApptPk.Value) ? Convert.ToInt32(this.hfSchedulerApptPk.Value) : 0, confirmation_id, true);
            }

            this.hfScheduledExistingApptXML.Value = HttpUtility.UrlEncode(this.scheduledExistingApptXML.CreateNavigator().OuterXml.ToString());
            this.lnkCancelAppointment.PostBackUrl = "~/corporateScheduler/walgreensSchedulerConfirmation.aspx";
            this.logger.Info("Method {0} accessed from {1} page - END", "lnkCancelAppointment_Click",
                 System.IO.Path.GetFileNameWithoutExtension(Request.Path));
        
        }
    }

    protected void ddlLocationStates_IndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.ddlLocationStates.SelectedValue))
        {
            this.bindClinicLocations();
            this.ddlClinicDate.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlTimeOfDay.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlAvailableTimes.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlAvailableRooms.Items.Insert(0, new ListItem("-- Select --", "0"));
        }
    }

    protected void ddlLocation_IndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(this.ddlLocation.SelectedValue) > 0)
        {
            this.bindClinicDates();
            this.ddlTimeOfDay.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlAvailableTimes.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlAvailableRooms.Items.Insert(0, new ListItem("-- Select --", "0"));
        }
    }

    protected void ddlClinicDate_IndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(this.ddlClinicDate.SelectedValue) > 0)
        {
            this.bindAvailableTimes();
            this.ddlAvailableTimes.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlAvailableRooms.Items.Insert(0, new ListItem("-- Select --", "0"));
        }
    }

    protected void ddlTimeOfDay_IndexChanged(object sender, EventArgs e)
    {
        if (this.ddlTimeOfDay.SelectedValue != "0")
        {
            this.bindAvailableTimeSlots();
            this.ddlAvailableRooms.Items.Insert(0, new ListItem("-- Select --", "0"));
        }
    }

    protected void ddlAvailableTimes_IndexChanged(object sender, EventArgs e)
    {
        if (this.ddlAvailableTimes.SelectedValue != "0")
        {
            this.bindClinicRoom();
        }
    }

    protected void validatePhone(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validatePhone();
    }

    protected void validateZipCode(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validateZipCode();
    }

    protected void ValidateEmail(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validateEmail();
    }

    protected void validatedDOB(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = true;
        int is_dob_required = 0, dob_month = 0, dob_day = 0, dob_year = 0;
        Int32.TryParse(this.hfIsDobRequired.Value, out is_dob_required);
        Int32.TryParse(this.ddlDobMonth.SelectedValue, out dob_month);
        Int32.TryParse(this.ddlDobDay.SelectedValue, out dob_day);
        Int32.TryParse(this.ddlDobYear.SelectedValue, out dob_year);
        if (is_dob_required == 0)
        {
            if ((dob_month > 0) && (dob_day == 0 || dob_year == 0)) e.IsValid = false;
            else if ((dob_day > 0) && (dob_month == 0 || dob_year == 0)) e.IsValid = false;
            else if ((dob_year > 0) && (dob_month == 0 || dob_day == 0)) e.IsValid = false;
            else if (dob_month > 0 && dob_day > 0 && dob_year > 0)
            {
                DateTime dob;
                if (DateTime.TryParse(this.ddlDobMonth.SelectedValue + "/" + this.ddlDobDay.SelectedValue + "/" + this.ddlDobYear.SelectedValue, out dob) == false)
                    e.IsValid = false;
            }
        }
        else
        {
            if (dob_month == 0 || dob_day == 0 || dob_year == 0) e.IsValid = false;
            else if (dob_month > 0 && dob_day > 0 && dob_year > 0)
            {
                DateTime dob;
                if (DateTime.TryParse(this.ddlDobMonth.SelectedValue + "/" + this.ddlDobDay.SelectedValue + "/" + this.ddlDobYear.SelectedValue, out dob) == false)
                    e.IsValid = false;
            }
            else
                e.IsValid = false;
        }
    }

    #endregion

    #region ----------------- PRIVATE METHODS ------------------

    /// <summary>
    /// Sets page styles and field validators
    /// </summary>
    private void setPageStylesAndValidators()
    {
        //Set page Logos & styles        
        this.bannerPreview.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
        this.bannerPreview.Style.Add("color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerTextColor"].Value);
        if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value))
            this.clientLogo.Visible = false;
        else
            this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=" + this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value + "&extraQS=" + DateTime.Now.Second;

        this.lnkScheduleAppointment.BackColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
        this.lnkScheduleAppointment.ForeColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);
        this.lnkCancelAppointment.BackColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
        this.lnkCancelAppointment.ForeColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);

        //Set page Required fields
        this.lblFirstName.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@firstName").Value)) ? "First Name *" : "First Name";
        this.txtFirstNameReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@firstName").Value));
        this.lblMiddleName.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@middleName").Value)) ? "Middle Name *" : "Middle Name";
        this.txtMiddleNameReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@middleName").Value));
        this.lblLastName.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@lastName").Value)) ? "Last Name *" : "Last Name";
        this.txtLastNameReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@lastName").Value));
        this.lblPhone.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@phone").Value)) ? "Phone *" : "Phone";
        this.txtPhoneReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@phone").Value));
        this.lblGender.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@gender").Value)) ? "Gender *" : "Gender";
        this.ddlGenderReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@gender").Value));
        this.lblDateOfBirth.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@dob").Value)) ? "Date of Birth *" : "Date of Birth";
        //this.dobCV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@dob").Value));
        this.hfIsDobRequired.Value = this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@dob").Value;
        this.lblAddress1.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@address1").Value)) ? "Address 1 *" : "Address 1";
        this.txtAddress1ReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@address1").Value));
        this.lblAddress2.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@address2").Value)) ? "Address 2 *" : "Address 2";
        this.txtAddress2ReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@address2").Value));
        this.lblEmployeeId.Text = (this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@employeeId") != null) ? (Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@employeeId").Value)) ? "Employee ID *" : "Employee ID") : "";
        this.txtEmployeeIdReqFV.Enabled = (this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@employeeId") != null) ? Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@employeeId").Value)) : false;

        this.lblCity.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@city").Value)) ? "City *" : "City";
        this.txtCityReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@city").Value));
        this.lblState.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@state").Value)) ? "State *" : "State";
        this.ddlStateReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@state").Value));
        this.lblZip.Text = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@zip").Value)) ? "Zip *" : "Zip";
        this.txtZipReqFV.Enabled = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredRegistrationFields/@zip").Value));

        //Showing/Hiding fields
        this.tdFirstName.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@firstName").Value));
        this.tdMiddelName.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@middleName").Value));
        this.tdLastName.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@lastName").Value));
        this.tdEmail.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@email").Value));
        this.tdPhone.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@phone").Value));
        this.tdGender.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@gender").Value));
        this.tdDOB.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@dob").Value));
        this.tdAddress1.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@address1").Value));
        this.tdAddress2.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@address2").Value));
        this.tdEmployeeID.Visible = this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@employeeId") != null ? Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@employeeId").Value)) : false;
        this.tdCity.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@city").Value));
        this.tdState.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@state").Value));
        this.tdZip.Visible = Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//visibleRegistrationFields/@zip").Value));
    }

    /// <summary>
    /// Populates date of birth year dropdown with values
    /// </summary>
    private void populatedDobYearDropdown()
    {
        this.ddlDobYear.Items.Insert(0, new ListItem("Year", "0"));
        for (int i = DateTime.Now.Year; i >= 1900; i--)
        {
            this.ddlDobYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    /// <summary>
    /// Binds clinic location states
    /// </summary>
    private void bindClinicLocationStates()
    {
        if (Request.Form["hfSchedulerApptPk"] != null)
            this.hfSchedulerApptPk.Value = Request.Form["hfSchedulerApptPk"].ToString();
        else if (this.commonAppSession.SelectedStoreSession.schedulerApptPk != 0)
            this.hfSchedulerApptPk.Value = this.commonAppSession.SelectedStoreSession.schedulerApptPk.ToString();
        this.schedulerClinics = this.dbOperation.getSchedulerClinicLocations(this.commonAppSession.SelectedStoreSession.schedulerDesignPk, !string.IsNullOrEmpty(this.hfSchedulerApptPk.Value) ? Convert.ToInt32(this.hfSchedulerApptPk.Value) : 0);
        this.commonAppSession.SelectedStoreSession.schedulerClinics = this.schedulerClinics;
        if (this.schedulerClinics != null && this.schedulerClinics.Tables.Count > 0)
        {
            this.hfcGroupId.Value = this.schedulerClinics.Tables[1].Rows[0]["clinicGroupId"].ToString();
            this.ddlLocationStates.ClearSelection();
            this.ddlLocationStates.DataSource = this.schedulerClinics.Tables[0];
            this.ddlLocationStates.DataTextField = "clinicState";
            this.ddlLocationStates.DataValueField = "clinicState";
            this.ddlLocationStates.DataBind();
            this.ddlLocationStates.Items.Insert(0, new ListItem("-- Select --", "0"));
        }
    }

    /// <summary>
    /// Binds clinic locations
    /// </summary>
    private void bindClinicLocations()
    {
        this.ddlLocation.ClearSelection();
        this.ddlLocation.Items.Clear();
        this.ddlClinicDate.ClearSelection();
        this.ddlClinicDate.Items.Clear();
        this.ddlTimeOfDay.ClearSelection();
        this.ddlTimeOfDay.Items.Clear();
        this.ddlAvailableTimes.ClearSelection();
        this.ddlAvailableTimes.Items.Clear();
        this.ddlAvailableRooms.ClearSelection();
        this.ddlAvailableRooms.Items.Clear();

        if (this.schedulerClinics != null && this.schedulerClinics.Tables.Count > 0)
        {
            DataRow[] dr_clinic_locations = this.schedulerClinics.Tables[1].Select("clinicState = '" + this.ddlLocationStates.SelectedValue + "'");
            if (dr_clinic_locations != null && dr_clinic_locations.Count() > 0)
            {
                DataView dv_clinic_locations = new DataView(dr_clinic_locations.CopyToDataTable());

                this.ddlLocation.ClearSelection();
                this.ddlLocation.DataSource = dv_clinic_locations.ToTable(true, "clinicLocation", "clinicLocationId");
                this.ddlLocation.DataTextField = "clinicLocation";
                this.ddlLocation.DataValueField = "clinicLocationId";
                this.ddlLocation.DataBind();
                this.ddlLocation.Items.Insert(0, new ListItem("-- Select Location --", "0"));

                //Display rooms selection if available
                this.lblAvailableRooms.Visible = (dr_clinic_locations.CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0);
                this.ddlAvailableRooms.Visible = (dr_clinic_locations.CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0);
                this.ddlAvailableRoomsReqFV.Enabled = (dr_clinic_locations.CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0);
            }
            else
                this.ddlLocation.Items.Insert(0, new ListItem("-- Select Location --", "0"));
        }
    }

    /// <summary>
    /// Binds location clinic dates
    /// </summary>
    private void bindClinicDates()
    {
        this.ddlClinicDate.ClearSelection();
        this.ddlClinicDate.Items.Clear();
        this.ddlTimeOfDay.ClearSelection();
        this.ddlTimeOfDay.Items.Clear();
        this.ddlAvailableTimes.ClearSelection();
        this.ddlAvailableTimes.Items.Clear();
        this.ddlAvailableRooms.ClearSelection();
        this.ddlAvailableRooms.Items.Clear();

        if (this.schedulerClinics != null && this.schedulerClinics.Tables.Count > 1)
        {
            DataRow[] dr_clinic_dates = this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "'");
            if (dr_clinic_dates.Count() > 0)
            {
                this.ddlClinicDate.DataSource = dr_clinic_dates.CopyToDataTable().DefaultView.ToTable(true, "clinicDateGroupId", "clinicDate");
                this.ddlClinicDate.DataTextField = "clinicDate";
                this.ddlClinicDate.DataValueField = "clinicDateGroupId";
                this.ddlClinicDate.DataBind();
                this.ddlClinicDate.Items.Insert(0, new ListItem("-- Select --", "0"));

                //Display rooms selection if available
                this.lblAvailableRooms.Visible = (dr_clinic_dates.CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0);
                this.ddlAvailableRooms.Visible = (dr_clinic_dates.CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0);
                this.ddlAvailableRoomsReqFV.Enabled = (dr_clinic_dates.CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0);
            }
        }
    }

    /// <summary>
    /// Binds Time of Day
    /// </summary>
    private void bindAvailableTimes()
    {
        this.ddlTimeOfDay.ClearSelection();
        this.ddlTimeOfDay.Items.Clear();
        this.ddlAvailableTimes.ClearSelection();
        this.ddlAvailableTimes.Items.Clear();
        this.ddlAvailableRooms.ClearSelection();
        this.ddlAvailableRooms.Items.Clear();

        if (this.schedulerClinics != null && this.schedulerClinics.Tables.Count > 2)
        {
            DataRow[] dr_avl_times = this.schedulerClinics.Tables[2].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "' AND clinicDateGroupId = '" + this.ddlClinicDate.SelectedValue + "'");

            if (dr_avl_times.Count() > 0)
            {
                //bind time of day dropdown
                this.ddlTimeOfDay.DataSource = dr_avl_times.CopyToDataTable().DefaultView.ToTable(true, "timeOfDay");
                this.ddlTimeOfDay.DataTextField = "timeOfDay";
                this.ddlTimeOfDay.DataValueField = "timeOfDay";
                this.ddlTimeOfDay.DataBind();
                this.ddlTimeOfDay.Items.Insert(0, new ListItem("-- Select --", "0"));

                //Display rooms selection if available
                this.lblAvailableRooms.Visible = this.ddlAvailableRooms.Visible = this.ddlAvailableRoomsReqFV.Enabled = (((this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "' AND clinicDateGroupId = '" + this.ddlClinicDate.SelectedValue + "'")).CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0) ? true : false);
            }
        }
    }

    /// <summary>
    /// Binds available appointment times
    /// </summary>
    private void bindAvailableTimeSlots()
    {
        this.ddlAvailableTimes.ClearSelection();
        this.ddlAvailableTimes.Items.Clear();
        this.ddlAvailableRooms.ClearSelection();
        this.ddlAvailableRooms.Items.Clear();

        if (this.schedulerClinics != null && this.schedulerClinics.Tables.Count > 2)
        {
            DataRow[] dr_avl_times = this.schedulerClinics.Tables[2].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "' AND clinicDateGroupId = '" + this.ddlClinicDate.SelectedValue + "'");

            if (dr_avl_times.Count() > 0 && !string.IsNullOrEmpty(this.ddlTimeOfDay.SelectedValue) && this.ddlTimeOfDay.SelectedValue != "0")
            {
                //bind available time slots
                this.ddlAvailableTimes.DataSource = dr_avl_times.CopyToDataTable().Select("timeOfDay = '" + this.ddlTimeOfDay.SelectedValue + "'").CopyToDataTable().DefaultView.ToTable(true, "apptAvlTimes");
                this.ddlAvailableTimes.DataTextField = "apptAvlTimes";
                this.ddlAvailableTimes.DataValueField = "apptAvlTimes";
                this.ddlAvailableTimes.DataBind();
                this.ddlAvailableTimes.Items.Insert(0, new ListItem("-- Select --", "0"));

                //Display rooms selection if available
                this.lblAvailableRooms.Visible = this.ddlAvailableRooms.Visible = this.ddlAvailableRoomsReqFV.Enabled = (((this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "' AND clinicDateGroupId = '" + this.ddlClinicDate.SelectedValue + "'")).CopyToDataTable().Select("isRoomsAvailable = 1").Count() > 0) ? true : false);
            }
        }
    }

    /// <summary>
    /// Binds clinic rooms
    /// </summary>
    private void bindClinicRoom()
    {
        this.ddlAvailableRooms.Items.Clear();
        this.ddlAvailableRooms.ClearSelection();

        if (this.schedulerClinics != null && this.schedulerClinics.Tables.Count > 1)
        {
            List<string> lst_clinic_rooms = new List<string>();

            DataRow[] dr_avl_rooms = this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "' AND clinicDateGroupId = '" + this.ddlClinicDate.SelectedValue + "' AND isRoomsAvailable = 'true'");
            foreach (DataRow clinics in dr_avl_rooms)
            {
                if (this.schedulerClinics.Tables[2].Select("clinicPk = '" + clinics["clinicPk"].ToString() + "' AND apptAvlTimes = '" + this.ddlAvailableTimes.SelectedValue + "'").Count() > 0)
                {
                    clinics["clinicRoom"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                                         .ToList()
                                                         .ForEach(room => lst_clinic_rooms.Add(room.Trim()));
                }
            }

            if (lst_clinic_rooms.Count() > 0)
            {
                this.ddlAvailableRooms.DataSource = lst_clinic_rooms.Distinct().OrderBy(x => x.ToString()).ToList();
                this.ddlAvailableRooms.DataBind();
                if (lst_clinic_rooms.Count() > 1)
                    this.ddlAvailableRooms.Items.Insert(0, new ListItem("-- Select --", "0"));
            }
            else
            {
                this.lblAvailableRooms.Visible = false;
                this.ddlAvailableRooms.Visible = false;
                this.ddlAvailableRoomsReqFV.Enabled = false;
            }
        }
    }

    /// <summary>
    /// Displays existing registraton details
    /// </summary>
    private void displayRegistrationDetails()
    {
        this.txtFirstName.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='firstName']").Attributes["value"].Value.Trim();
        this.txtMiddleName.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='middleName']").Attributes["value"].Value.Trim();
        this.txtLastName.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='lastName']").Attributes["value"].Value.Trim();
        this.txtEmail.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='email']").Attributes["value"].Value.Trim();
        this.txtPhone.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='phone']").Attributes["value"].Value.Trim();
        if (this.ddlGender.Items.FindByText(this.clientSchedulerXML.SelectSingleNode("//field[@name='gender']").Attributes["value"].Value) != null)
            this.ddlGender.Items.FindByText(this.clientSchedulerXML.SelectSingleNode("//field[@name='gender']").Attributes["value"].Value).Selected = true;
        //this.hfDateOfBirth.Value = this.clientSchedulerXML.SelectSingleNode("//field[@name='dateOfBirth']").Attributes["value"].Value.ToString();
        //this.txtDOB.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='dateOfBirth']").Attributes["value"].Value.ToString();
        if (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode("//field[@name='dateOfBirth']").Attributes["value"].Value))
        {
            string dob = this.clientSchedulerXML.SelectSingleNode("//field[@name='dateOfBirth']").Attributes["value"].Value;
            this.ddlDobMonth.Items.FindByValue(dob.Substring(0, 2)).Selected = true;
            this.ddlDobDay.Items.FindByValue(dob.Substring(3, 2)).Selected = true;
            this.ddlDobYear.Items.FindByValue(dob.Substring(6, 4)).Selected = true;
        }

        this.txtAddress1.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='address1']").Attributes["value"].Value.Trim();
        this.txtAddress2.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='address2']").Attributes["value"].Value.Trim();
        this.txtEmployeeId.Text = (this.clientSchedulerXML.SelectSingleNode("//field[@name='employeeId']") != null) ? this.clientSchedulerXML.SelectSingleNode("//field[@name='employeeId']").Attributes["value"].Value.Trim() : "";
        this.txtCity.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='city']").Attributes["value"].Value.Trim();
        if (this.ddlState.Items.FindByText(this.clientSchedulerXML.SelectSingleNode("//field[@name='state']").Attributes["value"].Value) != null)
            this.ddlState.Items.FindByText(this.clientSchedulerXML.SelectSingleNode("//field[@name='state']").Attributes["value"].Value).Selected = true;
        this.txtZip.Text = this.clientSchedulerXML.SelectSingleNode("//field[@name='zipCode']").Attributes["value"].Value.Trim();

        this.ddlLocationStates.ClearSelection();
        this.ddlLocation.ClearSelection();
        this.ddlClinicDate.ClearSelection();
        this.ddlTimeOfDay.ClearSelection();
        this.ddlAvailableTimes.ClearSelection();
        this.ddlAvailableRooms.ClearSelection();

        if (this.ddlLocationStates.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='clinicState']").Attributes["value"].Value) != null)
        {
            this.ddlLocationStates.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='clinicState']").Attributes["value"].Value).Selected = true;
            this.bindClinicLocations();
        }

        if (this.ddlLocation.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='clinicLocationId']").Attributes["value"].Value) != null)
        {
            this.ddlLocation.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='clinicLocationId']").Attributes["value"].Value).Selected = true;
            this.bindClinicDates();
        }

        if (this.ddlClinicDate.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='clinicDateGroupId']").Attributes["value"].Value) != null)
        {
            this.ddlClinicDate.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='clinicDateGroupId']").Attributes["value"].Value).Selected = true;
            this.bindAvailableTimes();
        }

        if (this.ddlTimeOfDay.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='apptTimeOfDay']").Attributes["value"].Value) != null)
        {
            this.ddlTimeOfDay.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='apptTimeOfDay']").Attributes["value"].Value).Selected = true;
            this.bindAvailableTimeSlots();
        }

        if (this.ddlAvailableTimes.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='apptTime']").Attributes["value"].Value) != null)
        {
            this.ddlAvailableTimes.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='apptTime']").Attributes["value"].Value).Selected = true;
            this.bindClinicRoom();
        }

        if (this.ddlAvailableRooms.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='apptRoom']").Attributes["value"].Value) != null)
            this.ddlAvailableRooms.Items.FindByValue(this.clientSchedulerXML.SelectSingleNode("//field[@name='apptRoom']").Attributes["value"].Value).Selected = true;

        if (this.ddlClinicDate.Items.Count == 0)
            this.ddlClinicDate.Items.Insert(0, new ListItem("-- Select --", "0"));
        if (this.ddlTimeOfDay.Items.Count == 0)
            this.ddlTimeOfDay.Items.Insert(0, new ListItem("-- Select --", "0"));
        if (this.ddlAvailableTimes.Items.Count == 0)
            this.ddlAvailableTimes.Items.Insert(0, new ListItem("-- Select --", "0"));
        if (this.ddlAvailableRooms.Items.Count == 0)
            this.ddlAvailableRooms.Items.Insert(0, new ListItem("-- Select --", "0"));
    }

    /// <summary>
    /// Compares appointment scheduling xml docs
    /// </summary>
    /// <returns></returns>
    private bool compareAppointmentXML()
    {
        bool is_changed = false;

        //Check if appointment registration is changed
        if (this.scheduledExistingApptXML != null)
        {
            XDocument old_client_scheduler_xml = new XDocument();
            XDocument new_client_scheduler_xml = new XDocument();

            old_client_scheduler_xml = XDocument.Parse(this.scheduledExistingApptXML.CreateNavigator().OuterXml);
            old_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "confirmationId").Single().Remove();
            old_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "apptPk").Single().Remove();
            old_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "clinicLocation").Single().Remove();
            //old_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "outreachBusinessPk").Single().Remove();

            new_client_scheduler_xml = XDocument.Parse(this.scheduledApptXML.CreateNavigator().OuterXml);
            new_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "confirmationId").Single().Remove();
            new_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "apptPk").Single().Remove();
            new_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "clinicLocation").Single().Remove();
            //new_client_scheduler_xml.Descendants("field").Where(X => X.Attribute("name").Value == "outreachBusinessPk").Single().Remove();
            is_changed = !(XDocument.DeepEquals(new_client_scheduler_xml, old_client_scheduler_xml));
        }
        else
            is_changed = true;

        return is_changed;
    }

    /// <summary>
    /// Creates appointment details xml document
    /// </summary>
    private XmlDocument prepareAppointmentXML()
    {
        XmlDocument reg_details = new XmlDocument();
        XmlElement reg_ele = reg_details.CreateElement("regDetails");
        XmlElement fields_ele = reg_details.CreateElement("fields");
        string confirmationId = (!string.IsNullOrEmpty(Request.Form["hflblConfirmationId"])) ? Request.Form["hflblConfirmationId"].ToString() : "";
        //Confirmation ID
        if (!string.IsNullOrEmpty(this.hfScheduledExistingApptXML.Value))
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(HttpUtility.UrlDecode(this.hfScheduledExistingApptXML.Value));
            this.scheduledApptXML = xmlDoc;
        }
        if (this.schedulerClinics != null)
        {
            if (this.scheduledApptXML != null && !string.IsNullOrEmpty(((XmlDocument)this.scheduledApptXML).SelectSingleNode("//field[@name='confirmationId']").Attributes["value"].Value.Trim()))
                this.contactInfoElement("confirmationId", ((XmlDocument)this.scheduledApptXML).SelectSingleNode("//field[@name='confirmationId']").Attributes["value"].Value.Trim(), reg_ele, reg_details, out fields_ele, out reg_details);
            else
                this.contactInfoElement("confirmationId", "", reg_ele, reg_details, out fields_ele, out reg_details);

            //Contact Information        
            this.contactInfoElement("firstName", this.txtFirstName.Text.Trim().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("middleName", this.txtMiddleName.Text.Trim().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("lastName", this.txtLastName.Text.Trim().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("email", this.txtEmail.Text.Trim(), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("phone", this.txtPhone.Text.Trim(), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("gender", (this.ddlGender.SelectedValue != "0") ? this.ddlGender.SelectedValue : "", reg_ele, reg_details, out fields_ele, out reg_details);
            //this.contactInfoElement("dateOfBirth", this.hfDateOfBirth.Value.Trim(), reg_ele, reg_details, out fields_ele, out reg_details);
            if (this.ddlDobMonth.SelectedValue != "0" && this.ddlDobDay.SelectedValue != "0" && this.ddlDobYear.SelectedValue != "0")
                this.contactInfoElement("dateOfBirth", this.ddlDobMonth.SelectedValue + "/" + this.ddlDobDay.SelectedValue + "/" + this.ddlDobYear.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);
            else
                this.contactInfoElement("dateOfBirth", "", reg_ele, reg_details, out fields_ele, out reg_details);

            this.contactInfoElement("address1", this.txtAddress1.Text.Trim().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("address2", this.txtAddress2.Text.Trim().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("employeeId", this.txtEmployeeId.Text.Trim(), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("city", this.txtCity.Text.Trim().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("state", this.ddlState.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("zipCode", this.txtZip.Text.Trim(), reg_ele, reg_details, out fields_ele, out reg_details);

            //Appointment information
            this.contactInfoElement("apptDate", this.ddlClinicDate.SelectedItem.Text.Trim(), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("apptTimeOfDay", this.ddlTimeOfDay.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("apptTime", this.ddlAvailableTimes.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("apptRoom", this.ddlAvailableRooms.Visible ? this.ddlAvailableRooms.SelectedValue : "", reg_ele, reg_details, out fields_ele, out reg_details);

            //this.contactInfoElement("outreachBusinessPk", this.ddlLocation.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("clinicState", this.ddlLocationStates.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("clinicLocationId", this.ddlLocation.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("clinicDateGroupId", this.ddlClinicDate.SelectedValue, reg_ele, reg_details, out fields_ele, out reg_details);

            this.contactInfoElement("clinicLocationName", this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "'").ElementAt(0)["clinicLocationName"].ToString().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("clinicLocation", this.schedulerClinics.Tables[1].Select("clinicLocationId = '" + this.ddlLocation.SelectedValue + "'").ElementAt(0)["clinicAddress"].ToString().Replace("'", "'"), reg_ele, reg_details, out fields_ele, out reg_details);

            List<int> clinic_locations = (from clinics in this.schedulerClinics.Tables[2].AsEnumerable() where clinics.Field<int>("clinicLocationId") == Convert.ToInt32(this.ddlLocation.SelectedValue) && clinics.Field<int>("clinicDateGroupId") == Convert.ToInt32(this.ddlClinicDate.SelectedValue) && clinics.Field<string>("apptAvlTimes") == this.ddlAvailableTimes.SelectedValue select clinics.Field<int>("clinicPk")).ToList();

            this.contactInfoElement("clinicPk", clinic_locations.Count > 0 ? clinic_locations.Min().ToString() : "", reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("clinicStoreId", clinic_locations.Count > 0 ? this.schedulerClinics.Tables[1].Select("clinicPk = " + clinic_locations.Min())[0]["clinicStoreId"].ToString() : "", reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("apptPk", (!string.IsNullOrEmpty(this.hfSchedulerApptPk.Value)) ? this.hfSchedulerApptPk.Value : "", reg_ele, reg_details, out fields_ele, out reg_details);
            this.contactInfoElement("clinicGroupId", (!string.IsNullOrEmpty(this.hfcGroupId.Value)) ? this.hfcGroupId.Value : "", reg_ele, reg_details, out fields_ele, out reg_details);

            reg_details.AppendChild(reg_ele);
        }
        else
        {
            this.schedulerClinics = this.commonAppSession.SelectedStoreSession.schedulerClinics;
            if(this.schedulerClinics == null)
                Response.Redirect("../auth/sessionTimeout.aspx");
        }
        return reg_details;
    }

    /// <summary>
    /// Prepares existing appointment details xml document
    /// </summary>
    /// <param name="dt_existing_user"></param>
    private void prepareExistingUserApptXML(DataTable dt_existing_user)
    {
        XmlDocument reg_details = new XmlDocument();
        XmlElement reg_ele = reg_details.CreateElement("regDetails");
        XmlElement fields_ele = reg_details.CreateElement("fields");
        //Confirmation ID
        this.contactInfoElement("confirmationId", dt_existing_user.Rows[0]["confirmationId"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);

        //Contact Information        
        this.contactInfoElement("firstName", dt_existing_user.Rows[0]["firstName"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("middleName", dt_existing_user.Rows[0]["middleName"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("lastName", dt_existing_user.Rows[0]["lastName"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("email", dt_existing_user.Rows[0]["email"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("phone", (dt_existing_user.Columns["phone"] != null ? dt_existing_user.Rows[0]["phone"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("gender", (dt_existing_user.Columns["gender"] != null ? dt_existing_user.Rows[0]["gender"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("dateOfBirth", (dt_existing_user.Columns["dateOfBirth"] != null ? dt_existing_user.Rows[0]["dateOfBirth"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("address1", (dt_existing_user.Columns["address1"] != null ? dt_existing_user.Rows[0]["address1"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("address2", (dt_existing_user.Columns["address2"] != null ? dt_existing_user.Rows[0]["address2"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("employeeId", (dt_existing_user.Columns["employeeId"] != null ? dt_existing_user.Rows[0]["employeeId"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("city", (dt_existing_user.Columns["city"] != null ? dt_existing_user.Rows[0]["city"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("state", (dt_existing_user.Columns["state"] != null ? dt_existing_user.Rows[0]["state"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("zipCode", (dt_existing_user.Columns["zipCode"] != null ? dt_existing_user.Rows[0]["zipCode"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);

        //Appointment information
        this.contactInfoElement("apptDate", dt_existing_user.Rows[0]["apptDate"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("apptTimeOfDay", dt_existing_user.Rows[0]["apptTimeOfDay"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("apptTime", dt_existing_user.Rows[0]["apptTime"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("apptRoom", (dt_existing_user.Columns["apptRoom"] != null ? dt_existing_user.Rows[0]["apptRoom"].ToString() : ""), reg_ele, reg_details, out fields_ele, out reg_details);

        //this.contactInfoElement("outreachBusinessPk", dt_existing_user.Rows[0]["outreachBusinessPk"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("clinicState", dt_existing_user.Rows[0]["clinicState"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("clinicLocationId", dt_existing_user.Rows[0]["clinicLocationId"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("clinicDateGroupId", dt_existing_user.Rows[0]["clinicDateGroupId"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("clinicLocationName", dt_existing_user.Rows[0]["clinicLocationName"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("clinicLocation", dt_existing_user.Rows[0]["clinicLocation"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("clinicPk", dt_existing_user.Rows[0]["clinicPk"].ToString(), reg_ele, reg_details, out fields_ele, out reg_details);
        this.contactInfoElement("apptPk", this.hfSchedulerApptPk.Value, reg_ele, reg_details, out fields_ele, out reg_details);

        reg_details.AppendChild(reg_ele);
        this.scheduledExistingApptXML = reg_details;
    }

    /// <summary>
    /// This function will be use to create xml element
    /// </summary>
    /// <param name="field_name"></param>
    /// <param name="field_value"></param>
    /// <param name="fields_element"></param>
    /// <param name="profile_doc"></param>
    /// <param name="fields_element_create"></param>
    /// <param name="profile_doc_create"></param>
    private void contactInfoElement(string field_name, string field_value, XmlElement fields_element, IXPathNavigable profile_doc, out XmlElement fields_element_create, out XmlDocument profile_doc_create)
    {
        XmlElement create_user;
        create_user = ((XmlDocument)profile_doc).CreateElement("field");
        create_user.SetAttribute("name", field_name);
        create_user.SetAttribute("value", field_value);
        fields_element.AppendChild(create_user);

        fields_element_create = fields_element;
        profile_doc_create = (XmlDocument)profile_doc;
    }

    /// <summary>
    /// Sends appointment confirmation email
    /// </summary>
    /// <param name="appt_pk"></param>
    /// <param name="confirmation_id"></param>
    private void sendApptConfirmationEmail(int appt_pk, string confirmation_id, bool is_cancelled)
    {
        //Send appointment scheduled confirmation email
        string email_body, subject_line, email_path, email_to, contact_info, patient_name, logo_path = string.Empty, appt_address1, appt_address2;
        EncryptQueryStringAES args = new EncryptQueryStringAES();
        email_to = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : this.clientSchedulerXML.SelectSingleNode(".//field[@name='email']").Attributes["value"].Value);
        email_path = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
        args["arg1"] = this.commonAppSession.SelectedStoreSession.schedulerDesignPk.ToString();
        args["arg2"] = appt_pk.ToString();
        args["arg3"] = "walgreensSchedulerRegistration.aspx";
        args["arg4"] = this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicPk']").Attributes["value"].Value;

        subject_line = (is_cancelled) ? (string)GetGlobalResourceObject("errorMessages", "immunizationApptCancelled") : (string)GetGlobalResourceObject("errorMessages", "scheduledApptConfirmation");

        contact_info = "";
        foreach (KeyValuePair<string, string> appt_details in ApplicationSettings.getSchedulerApptFields)
        {
            if (appt_details.Key == "name")
            {
                patient_name = !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='firstName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='firstName']").Attributes["value"].Value + " " : "";
                patient_name += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='middleName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='middleName']").Attributes["value"].Value + " " : "";
                patient_name += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='lastName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='lastName']").Attributes["value"].Value : "";

                if (!string.IsNullOrEmpty(patient_name.Trim()))
                    contact_info += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>" + appt_details.Value + ": " + patient_name.Trim() + "</p>";
            }
            else if (appt_details.Key != "address")
            {
                if (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='" + appt_details.Key + "']").Attributes["value"].Value))
                    contact_info += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>" + appt_details.Value + ": " + this.clientSchedulerXML.SelectSingleNode(".//field[@name='" + appt_details.Key + "']").Attributes["value"].Value + "</p>";
            }
            else
            {
                appt_address1 = !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='address1']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='address1']").Attributes["value"].Value + ", " : "";
                appt_address1 += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='address2']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='address2']").Attributes["value"].Value + " " : "";

                appt_address2 = !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='city']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='city']").Attributes["value"].Value + ", " : "";
                appt_address2 += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='state']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='state']").Attributes["value"].Value + " " : "";
                appt_address2 += !string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='zipCode']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='zipCode']").Attributes["value"].Value + " " : "";

                appt_address1 = (!string.IsNullOrEmpty(appt_address1.Trim()) ? appt_address1.Trim().TrimEnd(',') + "<br />" + appt_address2.Trim().TrimStart(',') : appt_address2.Trim().TrimStart(','));

                if (!string.IsNullOrEmpty(appt_address1))
                    contact_info += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Address: " + appt_address1 + "</p>";
            }
        }

        email_body = "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Appointment:";
        if (!is_cancelled)
            email_body += this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptDate']").Attributes["value"].Value + " " + this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptTime']").Attributes["value"].Value + "</p>";
        else
            email_body += "Cancelled</p>";

        email_body += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Location:" + (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicLocationName']").Attributes["value"].Value) ? this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicLocationName']").Attributes["value"].Value + ",<br />" : "") + this.clientSchedulerXML.SelectSingleNode(".//field[@name='clinicLocation']").Attributes["value"].Value + "<br /></p>";

        if (this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptRoom']").Attributes["value"].Value != null)
            email_body += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Room:" + this.clientSchedulerXML.SelectSingleNode(".//field[@name='apptRoom']").Attributes["value"].Value + "<br /></p>";

        if (this.clientLogo.ImageUrl.Length > 0)
            logo_path = email_path + ApplicationSettings.EmailLogoImagePath() + "/" + this.clientLogo.ImageUrl.Split('=')[1].Substring(0, this.clientLogo.ImageUrl.Split('=')[1].LastIndexOf('&'));

        this.clientSchedulerXML.LoadXml(this.commonAppSession.SelectedStoreSession.schedulerDesignXML);

        if (is_cancelled)
            this.walgreensEmail.sendRescheduleAppointmentEmail(email_path + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + args.ToString(), Server.MapPath("~/emailTemplates/walgreensSchedulerCancelledEmail.html"), subject_line, confirmation_id, contact_info, email_body, email_to, "", true, logo_path, this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
        else
            this.walgreensEmail.sendRescheduleAppointmentEmail(email_path + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + args.ToString(), Server.MapPath("~/emailTemplates/walgreensSchedulerConfirmationEmail.html"), subject_line, confirmation_id, contact_info, email_body, email_to, "", true, logo_path, this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
    }
    /// <summary>
    /// Sends max immunizers reached email to specific users
    /// </summary>
    /// <param name="clinic_pk"></param>
    /// <param name="clinic_store_id"></param>
    /// <param name="business_name"></param>
    private void sendMaxImmunizersReachedEmail(int clinic_pk, int clinic_store_id, string business_name)
    {
        string subject_line, email_to;        
        subject_line = (string)GetGlobalResourceObject("errorMessages", "maxImmunizersReached");
        DataTable dt_business_details = this.dbOperation.getCorporateBusinessName(clinic_pk);
        if (dt_business_details.Rows.Count > 0)
        {
            business_name = dt_business_details.Rows[0]["businessName"].ToString();
            email_to = dt_business_details.Rows[0]["emailTo"].ToString();
            string client_services_emails = ApplicationSettings.getClientServicesInfoFromGroup("MaxImmunizersAlertGroup").Trim();
            if (!string.IsNullOrEmpty(client_services_emails) && !string.IsNullOrEmpty(email_to))
                email_to = (email_to + ", " + client_services_emails);
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()))
                email_to = ConfigurationManager.AppSettings["emailSendTo"].ToString();
            this.walgreensEmail.sendSchedulerMaxImmunizersReachedEmail("Client Services", business_name, clinic_store_id.ToString(), Server.MapPath("~/emailTemplates/schedulerMaxImmunizers.htm"), subject_line, email_to, "", true);
        }
    }
    #endregion

    #region ----------------- PRIVATE VARIABLES ----------------
    private DBOperations dbOperation = null;
    private AppCommonSession commonAppSession = null;
    private XmlDocument clientSchedulerXML;
    private WalgreenEmail walgreensEmail = null;
    private IXPathNavigable scheduledApptXML { get; set; }
    private IXPathNavigable scheduledExistingApptXML { get; set; }
    private DataSet schedulerClinics { get; set; }
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        this.clientSchedulerXML = new XmlDocument();

        if (!IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.referrerPath != null && !string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.referrerPath))
            {
                if (this.commonAppSession.SelectedStoreSession.referrerPath != "walgreensSchedulerLanding.aspx" && this.commonAppSession.SelectedStoreSession.referrerPath != "walgreensSchedulerConfirmation.aspx")
                    Response.Redirect("walgreensSchedulerLanding.aspx");
            }
            else
                Response.Redirect("walgreensSchedulerLanding.aspx");
        }
    }
    #endregion
}