﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensSchedulerConfirmation.aspx.cs" Inherits="corporateScheduler_walgreensSchedulerConfirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Walgreens- Onsite Clinic Scheduling</title>
<link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
<link href="../css/wags.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function printPages() {
        window.print();
        return false;
    }

	//function downloadPrefilledVARform()
    //{       
	//    window.open('prefilledVarForm.aspx', '_blank');	    
    //    return false;
	//}
    function editDetails()
    {
        var form = document.getElementById('form1');
        if (form.target == '_blank')
            form.target = '_self';
    }
	function navigateForm(action) {
	    var submitVal = document.getElementById('hflblConfirmationId').value;

	    form = document.getElementById('form1');
	    form.target = '_blank';
	    form.action = action;
	    form.submit();

	    return false;
	}
	function openPDF() {
        window.open('../resources/VAR_Form_2014.pdf', '_blank', 'width=950, height=850, scrollbars=yes');

        return false;
    }
</script>
<style type="text/css" media="print">
    .hide_print {display: none;}
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
 
    <form id="form1" runat="server">
           <asp:HiddenField ID="hflblConfirmationId" runat="server" />
         <asp:HiddenField ID="hfScheduledExistingApptXML" runat="server" />
            <asp:HiddenField ID="hfSchedulerApptPk" runat="server" />
    <div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow" style="width:95%; max-width:850px;">
          <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px"><table width="100%" cellpadding="0" cellspacing ="0" border="0"><tr><td align="left" valign="top" bgcolor="#FFFFFF"><asp:Image ID="clientLogo" runat="server" AlternateText="Client Logo" /></td><td align="right" valign="top" bgcolor="#FFFFFF">&nbsp;</td></tr></table></td>
          </tr>
          <tr>
            <td id="bannerPreview" runat="server" colspan="2" align="left" style="height: 24px; padding-top:6px; padding-bottom: 4px; padding-right: 24px; padding-left: 24px; font-family: Arial, Helvetica, sans-serif; font-size: 24px;" valign="top" >Immunization Clinic Registration &amp; Scheduling</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding:24px" ><table class="wagsSchedulerGreyBkgd" width="100%" border="0" cellspacing="0" cellpadding="0">          
                  <tr>
                    <td class="bodyText14" style="padding:14px"><span class="pageTitle" style="border-bottom:0; line-height:125%"><%=headerText%></span><br />
                      <%=headerInfo%></td>
                  </tr>
                  <tr>
                    <td style="padding:12px"><table width="100%" border="0" cellspacing="0" cellpadding="4">
                      <tr>
                        <td class="pageTitle" style="border-bottom:0;">Confirmation</td>
                        <td align="right" valign="top" ></td>
                        <td width="70%" rowspan="14" align="right" valign="top" ><table width="300px" border="0" cellspacing="12" cellpadding="0" style=" background-color:#FAFAFA;border-color:#999;border-style:solid;	border-width: 1px;">
                          <tr>
                            <td><span class="pageTitle" style="border-bottom:0;">Please Note</span>
                                <p class="bodyText14">On the day of your appointment:
                                    <ul class="bodyText14">
                                        <li style="padding-bottom:12px;">Bring a completed <a href="#" onclick="return navigateForm('prefilledVarForm.aspx');">Vaccine Administration Record - Informed Consent for Vaccination Form</a> (This will be required before receiving your Immunization)</li>
                                        <li style="padding-bottom:12px;">Review the <a href="../controls/ResourceFileHandler.ashx?Path=2015_16/VIS Form.pdf" target="_blank">Vaccine Information Statement</a></li>
                                        <li style="padding-bottom:12px;">Wear a short sleeve shirt and comfortable clothing</li>
                                    </ul>
                                </p>
                                <div style="padding-bottom:8px;"><span class="hide_print">
                                    <a id="anchorPrintVarForm" runat="server" target="_blank" class="wagsSchedBigButton" onclick="navigateForm('prefilledVarForm.aspx');" style="padding-top:2px; width:270px; font-size:15px;" ><img src="../images/icon_print.png" width="17" height="19" style=" position:relative; top:4px; border:0;" />&nbsp;&nbsp; Print VAR Form</a></span>
                                </div>
                                <div style="padding-bottom:8px;"><span class="hide_print">
                                <asp:LinkButton ID="lnkPrintConfirmation" CssClass="wagsSchedBigButton" runat="server" style="padding-top:2px; width:270px; font-size:15px;" OnClientClick="return printPages();">
                                <img src="../images/icon_print.png" width="17" height="19" alt="Print Confirmation" style="position:relative; top:4px; border:0;" onclick="printPages();" />&nbsp;&nbsp;Print Confirmation</asp:LinkButton></span>
                                </div>
                                <div><span class="hide_print"><a id="anchorReviewVISForm" runat="server" href="../controls/ResourceFileHandler.ashx?Path=2015_16/VIS Form.pdf" target="_blank" class="wagsSchedBigButton" style="width:270px; font-size:15px;" >Vaccine Information Statement</a></span>
                                </div>
                            </td>
                          </tr>
                        </table>                    
                        </td>
                      </tr>
                      <tr>
                        <td width="40%" style="padding-left:24px" class="formFields"><strong>Confirmation ID:</strong></td>
                        <td width="60%" class="formFields"><strong><asp:Label ID="lblConfirmationId" runat="server" ></asp:Label></strong></td>
                      </tr>
                     <tr>
                        <td >&nbsp;</td>
                        <td >&nbsp;</td>
                        </tr>
                    <tr>
                        <td colspan="2" class="pageTitle" style="border-bottom:0;">Contact Information</td></tr>
                    <tr>
                        <%=contactInformation%>
                  </tr>
                    <tr>
                        <td >&nbsp;</td>
                        <td >&nbsp;</td>
                        </tr>
          
                      <tr>
                        <td colspan="2" class="pageTitle" style="border-bottom:0;">Appointment</td>
                      </tr>
                      <tr runat="server" id="rowTime">
                        <td width="30%" align="left" valign="top" class="formFields" style="padding-left:24px">Appointment Time:</td>
                        <td width="70%" align="left" valign="top" class="formFields"><asp:Label ID="lblAppointmentTime" runat="server" Text=""></asp:Label></td>
                      </tr>
                      <tr runat="server" id="rowLocation">
                        <td align="left" valign="top" class="formFields" style="padding-left:24px">Location:</td>
                        <td align="left" valign="top" class="formFields"><asp:Label ID="lblLocation" runat="server" Text=""></asp:Label></td>
                      </tr>
                         <tr runat="server" id="rowApptRoom">
                           <td width="40%" align="left" valign="top" class="formFields" style="padding-left:24px">Room:</td>
                           <td width="60%" align="left" valign="top" class="formFields"><asp:Label ID="lblAppointmentRoom" runat="server" Text=""></asp:Label></td>
                         </tr>
                      </table></td>
                    </tr>
                  <tr>
                    <td style="padding:12px"><span class="hide_print"><asp:LinkButton ID="lnkEditAppointment" OnClientClick="editDetails()" PostBackUrl="~/corporateScheduler/walgreensSchedulerRegistration.aspx" CssClass="wagsSchedBigButton" runat="server">Edit Information</asp:LinkButton></span></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
          </tr>
        </table>
    </div>
    <table id="tblfooter" border="0" cellspacing="0" cellpadding="0" align="center" style="width:95%; max-width:850px;">
        <tr>
            <td style="padding: 0px 46px 0px 48px; font-family:Arial, Helvetica, Sans-Serif; font-size:12px; text-decoration:none; color:#666666; line-height: 18px; text-align:Left; vertical-align:top;" >
                <p>
                    For your protection, only your name and email will be stored to identify your appointment. All other personal data will be removed
                    when you have completed the scheduling and confirmation process. Email addresses stored for appointments will not be used for
                    any other purpose than communicating with the customer about the scheduled immunization clinic and will be flushed from our
                    system after the clinic has been completed.</p>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
