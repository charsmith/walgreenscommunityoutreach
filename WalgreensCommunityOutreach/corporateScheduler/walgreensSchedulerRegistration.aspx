﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensSchedulerRegistration.aspx.cs" Inherits="corporateScheduler_walgreensSchedulerRegistration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Walgreens- Onsite Clinic Scheduling</title>
<link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
 <link href="../css/wags.css" rel="stylesheet" type="text/css" />
 <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
 <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script> 
 <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>   
 <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>  
 <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />

 <script type="text/javascript">

     $(document).ready(function () {
         if (document.getElementById('lnkScheduleAppointment').href.indexOf('walgreensSchedulerConfirmation') > 0)
             document.getElementById('lnkScheduleAppointment').click();

         if (document.getElementById('lnkCancelAppointment')!=null && document.getElementById('lnkCancelAppointment').href.indexOf('walgreensSchedulerConfirmation') > 0)
             document.getElementById('lnkCancelAppointment').click();
//         $("#txtDOB").datepicker({
//             showOn: "both",
//             buttonImage: "../images/btn_calendar.gif",
//             buttonImageOnly: true,
//             changeMonth: true, changeYear: true, yearRange: '1900:+0',
//             //maxDate: new Date(),
//             onSelect: function (dateText) {
//                 $("#hfDateOfBirth").val(dateText);
//             }
//         });

//         $("#txtDOB").val($("#hfDateOfBirth").val());

//         $(".ui-datepicker-trigger").css("margin-bottom", "-6px");
//         $(".ui-datepicker-trigger").css("padding-left", "5px");
     });

     function setSelectedValue() {
         var e = document.getElementById("ddlAvailableTimes");
         var selected_value = e.options[e.selectedIndex].text;
         document.getElementById('hfAvailableTimes').value = selected_value;
     }

     function ValidateEmail(sender, args) {
         var email_id = $.trim(args.Value.replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
         var emailPattern = /^[a-zA-Z0-9][a-zA-Z0-9.!@#$%&*+_-]+[a-zA-Z0-9]@[a-zA-Z0-9][a-zA-Z0-9._-]+\.[a-zA-Z]{2,4}$/;         
         //var emailPattern = ([a-zA-Z0-9!$%&*+=?^_{}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$);
         var valid_emails = true;
         if (!emailPattern.test(email_id)) {
             valid_emails = false;
         }

         if (!valid_emails)
             args.IsValid = false;
         else
             args.IsValid = true;
     }

     function validatedDOB(sender, args) {
         args.IsValid = true;

         var month = document.getElementById("ddlDobMonth");
         var selected_month = month.options[month.selectedIndex].value;
         var day = document.getElementById("ddlDobDay");
         var selected_day = day.options[day.selectedIndex].value;
         var year = document.getElementById("ddlDobYear");
         var selected_year = year.options[year.selectedIndex].value;

         if (document.getElementById("hfIsDobRequired").value == "0") {
             if (parseInt(selected_month) != 0 && (parseInt(selected_day) == 0 || parseInt(selected_year) == 0)) args.IsValid = false;
             else if (parseInt(selected_day, 10) != 0 && (parseInt(selected_month, 10) == 0 || parseInt(selected_year, 10) == 0)) args.IsValid = false;
             else if (parseInt(selected_year, 10) != 0 && (parseInt(selected_month, 10) == 0 || parseInt(selected_day, 10) == 0)) args.IsValid = false;
             else if (parseInt(selected_month, 10) != 0 && parseInt(selected_day, 10) != 0 && parseInt(selected_year, 10) != 0) {

                 if (((parseInt(selected_month, 10) == 4) || (parseInt(selected_month, 10) == 6) || (parseInt(selected_month, 10) == 9) || (parseInt(selected_month, 10) == 11)) && (parseInt(selected_day, 10) > 30)) args.IsValid = false;
                 else if (parseInt(selected_month, 10) == 2) {
                     if (parseInt(selected_year, 10) % 4 == 0 && parseInt(selected_day, 10) > 29) args.IsValid = false;
                     else if (parseInt(selected_year, 10) % 4 != 0 && parseInt(selected_day, 10) > 28) args.IsValid = false;
                 }
                 //else if ((parseInt(selected_month) == 2) && (((parseInt(selected_year) % 400) == 0) || ((parseInt(selected_year) % 4) == 0)) && ((parseInt(selected_year) % 100) != 0) && (parseInt(selected_day) > 28)) args.IsValid = false;
                 //else if ((parseInt(selected_month) == 2) && ((parseInt(selected_year) % 100) == 0) && (parseInt(selected_day) > 29)) args.IsValid = false;
             }
         }
         else {
             if (parseInt(selected_month, 10) == 0 || parseInt(selected_day, 10) == 0 || parseInt(selected_year, 10) == 0) args.IsValid = false;
             else if (parseInt(selected_month, 10) != 0 && parseInt(selected_day, 10) != 0 && parseInt(selected_year, 10) != 0) {
                 if (((parseInt(selected_month, 10) == 4) || (parseInt(selected_month, 10) == 6) || (parseInt(selected_month, 10) == 9) || (parseInt(selected_month, 10) == 11)) && (parseInt(selected_day, 10) > 30)) args.IsValid = false;
                 else if (parseInt(selected_month, 10) == 2) {
                     if (parseInt(selected_year, 10) % 4 == 0 && parseInt(selected_day, 10) > 29) args.IsValid = false;
                     else if (parseInt(selected_year, 10) % 4 != 0 && parseInt(selected_day, 10) > 28) args.IsValid = false;
                 }
             }
             else {
                 args.IsValid = false;
             }
         }
     }
 </script>
 <style type="text/css">
 .ui-widget
    {
        font-size:0.7em;
    }
 </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
     <asp:HiddenField ID="hfIsDobRequired" runat="server" />
     <asp:HiddenField ID="hfScheduledExistingApptXML" runat="server" />
     <asp:HiddenField ID="hfSchedulerApptPk" runat="server" />
     <asp:HiddenField ID="hfcGroupId" runat="server" />
        
     <div>
      <table border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow" style="width:95%; max-width:800px;"> 
      <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" ShowSummary="false" />
      <tr>
        <td align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" cellpadding="0" cellspacing ="0" border="0"><tr><td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px"><asp:Image ID="clientLogo" runat="server" AlternateText="Client Logo" /></td><td align="right" valign="top" bgcolor="#FFFFFF">&nbsp;</td></tr></table></td>
      </tr>
      <tr>    
        <td id="bannerPreview" runat="server" colspan="2" align="left" style="height: 24px; padding-top:6px; padding-bottom: 4px; padding-right: 24px; padding-left: 24px; font-family: Arial, Helvetica, sans-serif; font-size: 24px;" valign="top" >Immunization Clinic Registration &amp; Scheduling</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="padding:24px" ><table class="wagsSchedulerGreyBkgd" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding:12px"><table width="100%" border="0" cellspacing="0" cellpadding="4">
                  <tr>
                    <td colspan="3" class="pageTitle" style="border-bottom:0;">Contact Information</td>
                    </tr>
                    <tr>
                    <td width="28%" class="formFields" runat="server" id="tdFirstName">
                        <asp:Label ID="lblFirstName" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtFirstName" runat="server" style="width:175px" TabIndex="1" MaxLength="50" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtFirstNameReqFV" Enabled="false" runat="server" Display="None" ControlToValidate="txtFirstName" ErrorMessage="First name is required"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtFirstNameReqEV" ControlToValidate="txtFirstName" Display="None" ValidationExpression="[^,&lt;&gt;]+" runat="server" ErrorMessage="First Name: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </td>
                    <td width="28%" class="formFields" runat="server" id="tdMiddelName">
                        <asp:Label ID="lblMiddleName" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtMiddleName" runat="server" style="width:175px" TabIndex="2" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtMiddleNameReqFV" Enabled="false" runat="server" ControlToValidate="txtMiddleName" Display="None" ErrorMessage="Middle name is required"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtMiddleNameRegEV" ControlToValidate="txtMiddleName" Display="None" ValidationExpression="[^,&lt;&gt;]+" runat="server" ErrorMessage="Middle Name: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </td>
                    <td width="44%" class="formFields" runat="server" id="tdLastName">
                        <asp:Label ID="lblLastName" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtLastName" runat="server" style="width:175px" TabIndex="3" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtLastNameReqFV" Enabled="false" runat="server" ControlToValidate="txtLastName" Display="None" ErrorMessage="Last name is required"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtLastNameReqEV" ControlToValidate="txtLastName" Display="None" ValidationExpression="[^,&lt;&gt;]+" runat="server" ErrorMessage="Last Name: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </td>
                    </tr>
                    <tr>
                    <td width="28%" class="formFields" runat="server" id="tdEmail">Email *<br />
                        <asp:TextBox class="formFields" ID="txtEmail" runat="server" style="width:175px" TabIndex="4" MaxLength="255"></asp:TextBox>                        
                        <asp:RequiredFieldValidator ControlToValidate="txtEmail" ID="txtemailReqFV" runat="server" Display="None" ErrorMessage="Email is required"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="txtEmailCV" runat="server" Display="None" ErrorMessage="Valid email is required." ControlToValidate="txtEmail" ClientValidationFunction="ValidateEmail" OnServerValidate="ValidateEmail" ></asp:CustomValidator>                        
                    </td>
                    <td class="formFields" runat="server" id="tdPhone">
                        <asp:Label ID="lblPhone" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtPhone" runat="server" style="width:175px" onblur="textBoxOnBlur(this);" TabIndex="5" MaxLength="15"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtPhone" Enabled="false" ID="txtPhoneReqFV" runat="server" Display="None" ErrorMessage="Phone is required"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="txtPhoneCV" ControlToValidate="txtPhone" runat="server" SetFocusOnError="true" Display="None" ErrorMessage="Valid Phone number is required(ex: ###-###-####)" ClientValidationFunction="ValidateCompanyPhoneFax"></asp:CustomValidator>                       
                    </td>
                    <td class="formFields" align="left" width="28%"><table width="100%" border="0" cellspacing="2" cellpadding="2">
                      <tr>
                        <td runat="server" id="tdGender" style="padding-right:3px;">
                            <asp:Label ID="lblGender" runat="server"></asp:Label><br />
                            <asp:DropDownList ID="ddlGender" runat="server" class="formFields" TabIndex="6">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ddlGender" Enabled="false" ID="ddlGenderReqFV" runat="server" Display="None" InitialValue="0" ErrorMessage="Gender is required"></asp:RequiredFieldValidator>
                        </td>
                        <td valign="top" runat="server" id="tdDOB">
                            <asp:Label ID="lblDateOfBirth" runat="server"></asp:Label><br />
                            <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right:3px;">
                                        <asp:DropDownList ID="ddlDobMonth" runat="server" Width="65px" TabIndex="7">
                                            <asp:ListItem Text="Month" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Jan" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="Feb" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="Mar" Value="03"></asp:ListItem>
                                            <asp:ListItem Text="Apr" Value="04"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                            <asp:ListItem Text="Jun" Value="06"></asp:ListItem>
                                            <asp:ListItem Text="Jul" Value="07"></asp:ListItem>
                                            <asp:ListItem Text="Aug" Value="08"></asp:ListItem>
                                            <asp:ListItem Text="Sep" Value="09"></asp:ListItem>
                                            <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="padding-right:3px;">
                                        <asp:DropDownList ID="ddlDobDay" runat="server" Width="65px" TabIndex="8">
                                            <asp:ListItem Text="Day" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                            <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                            <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                            <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                            <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                            <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                            <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                            <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                            <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                            <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                            <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                            <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                            <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                            <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                            <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                            <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                            <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                            <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                            <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                            <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                            <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                            <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                            <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                            <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                            <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                            <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="padding-right:3px;">
                                        <asp:DropDownList ID="ddlDobYear" runat="server" Width="65px" TabIndex="9"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:CustomValidator ID="dobCV" runat="server" ClientValidationFunction="validatedDOB" OnServerValidate="validatedDOB" ErrorMessage="Select valid Date of Birth (ex: MMM/DD/YYYY)" Display="None" ></asp:CustomValidator>
                        </td>
                        </tr>
                      </table></td>
                    </tr>
                  <tr>
                    <td width="28%" class="formFields" runat="server" id="tdAddress1">
                        <asp:Label ID="lblAddress1" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtAddress1" runat="server" style="width:175px" TabIndex="10" MaxLength="500"></asp:TextBox>
                        <asp:RequiredFieldValidator Enabled="false" ControlToValidate="txtAddress1" ID="txtAddress1ReqFV" runat="server" Display="None" ErrorMessage="Address1 is required"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtAddress1RegEV" ControlToValidate="txtAddress1" Display="None" ValidationExpression="[^&lt;&gt;]+" runat="server" ErrorMessage="Address 1: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </td>
                    <td class="formFields" runat="server" id="tdAddress2">
                        <asp:Label ID="lblAddress2" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtAddress2" runat="server" style="width:175px" TabIndex="11" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator Enabled="false" ControlToValidate="txtAddress2" ID="txtAddress2ReqFV" runat="server" Display="None" ErrorMessage="Address2 is required"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtAddress2RegEV" ControlToValidate="txtAddress2" Display="None" ValidationExpression="[^&lt;&gt;]+" runat="server" ErrorMessage="Address 2: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </td>
                    <td width="28%" class="formFields" runat="server" id="tdEmployeeID">
                        <asp:Label ID="lblEmployeeId" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtEmployeeId" runat="server" style="width:175px" TabIndex="12" MaxLength="500"></asp:TextBox>
                        <asp:RequiredFieldValidator Enabled="false" ControlToValidate="txtEmployeeId" ID="txtEmployeeIdReqFV" runat="server" Display="None" ErrorMessage="Employee ID is required"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtEmployeeIdRegEV" ControlToValidate="txtEmployeeId" Display="None" ValidationExpression="[^&lt;&gt;]+" runat="server" ErrorMessage="Employee ID: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                    </td>
                    </tr>
                    <tr>
                    <td width="28%" class="formFields" runat="server" id="tdCity">
                        <asp:Label ID="lblCity" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtCity" runat="server" style="width:175px" TabIndex="13" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator Enabled="false" ControlToValidate="txtCity" ID="txtCityReqFV" runat="server" Display="None" ErrorMessage="City is required"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtCityRegEV" ControlToValidate="txtCity" Display="None" ValidationExpression="[^,&lt;&gt;]+" runat="server" ErrorMessage="City: , &lt; &gt; Characters are not allowed."></asp:RegularExpressionValidator>
                       </td>
                    <td class="formFields"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    <td width="20%" runat="server" id="tdState">
                        <asp:Label ID="lblState" runat="server"></asp:Label><br />
                        <asp:DropDownList ID="ddlState" runat="server" CssClass="formFields" TabIndex="14"></asp:DropDownList>
                        <asp:RequiredFieldValidator Enabled="false" ControlToValidate="ddlState" ID="ddlStateReqFV" runat="server" Display="None" ErrorMessage="State is required"></asp:RequiredFieldValidator>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td runat="server" id="tdZip">
                        <asp:Label ID="lblZip" runat="server"></asp:Label><br />
                        <asp:TextBox class="formFields" ID="txtZip" runat="server" style="width:75px" MaxLength="5" TabIndex="15"></asp:TextBox>
                        <asp:RequiredFieldValidator Enabled="false" ControlToValidate="txtZip" ID="txtZipReqFV" runat="server" Display="None" ErrorMessage="Zip code is required"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="txtZipCV" runat="server" ErrorMessage="Valid Zip Code is required (ex: #####)" ControlToValidate="txtZip" Display="None" ClientValidationFunction="validateZipCode" OnServerValidate="validateZipCode"></asp:CustomValidator>
                    </td>
                    </tr>
                   </table></td>
                    <td>&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td style="padding:12px"><table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td colspan="2" class="pageTitle" style="border-bottom:0;">Select Location</td>
                    </tr>
                  <tr>
                    <td width="15%" class="formFields">State *<br />
                      <asp:DropDownList ID="ddlLocationStates" runat="server" CssClass="formFields" AutoPostBack="true" OnSelectedIndexChanged="ddlLocationStates_IndexChanged" Width="80px" TabIndex="16"></asp:DropDownList>
                      <asp:RequiredFieldValidator ControlToValidate="ddlLocationStates" ID="ddlLocationStatesReqFV" runat="server" Display="None" ErrorMessage="Location state is required" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td width="85%" class="formFields">Location *<br />
                      <asp:DropDownList ID="ddlLocation" runat="server" CssClass="formFields" AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_IndexChanged" Width="200px" TabIndex="17">
                        <asp:ListItem Text="-- Select Location --" Value="0"></asp:ListItem>
                      </asp:DropDownList>
                      <asp:RequiredFieldValidator ControlToValidate="ddlLocation" ID="txtLocationReqFV" runat="server" Display="None" ErrorMessage="Location is required" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                  </tr>
                  </table></td>
                </tr>
              <tr>
                <td style="padding:12px"><table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td colspan="3" class="pageTitle" style="border-bottom:0;">Select Appointment</td>
                  </tr>
                  <tr>
                    <td width="12%" class="formFields" style="vertical-align:bottom;">Date *</td>
                    <td width="12%" nowrap="nowrap" class="formFields" style="vertical-align:bottom;">Time of Day *</td>
                    <td width="15%" nowrap="nowrap" class="formFields" style="vertical-align:bottom;">Available Times *</td>
                    <td  width="61%" nowrap="nowrap" class="formFields" style="vertical-align:bottom;"><asp:Label ID="lblAvailableRooms" runat="server" Text="Available Rooms *"></asp:Label></td>
                  </tr>
                  <tr>
                        <td width="12%" class="formFields">
                            <%--<asp:Label class="formFields" ID="lblDate" runat="server" Text=""></asp:Label>--%>
                            <asp:DropDownList ID="ddlClinicDate" runat="server" CssClass="formFields" AutoPostBack="true" OnSelectedIndexChanged="ddlClinicDate_IndexChanged" Width="100px" TabIndex="18">
                                <asp:ListItem Text="-- Select --" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator Enabled="true" ControlToValidate="ddlClinicDate" ID="ddlClinicDateReqFV" runat="server" Display="None" InitialValue="0" ErrorMessage="Clinic Date is required"></asp:RequiredFieldValidator>
                        </td>
                        <td width="12%" nowrap="nowrap" class="formFields">
                            <asp:DropDownList ID="ddlTimeOfDay" runat="server"  CssClass="formFields" AutoPostBack="true" OnSelectedIndexChanged="ddlTimeOfDay_IndexChanged" Width="100px" TabIndex="19">
                                <asp:ListItem Text="-- Select --" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator Enabled="true" ControlToValidate="ddlTimeOfDay" ID="ddlTimeOfDayReqFV" runat="server" Display="None" InitialValue="0" ErrorMessage="Time of Day is required"></asp:RequiredFieldValidator>
                        </td>
                        <td width="15%" nowrap="nowrap" class="formFields">
                            <asp:DropDownList ID="ddlAvailableTimes" runat="server"  CssClass="formFields" Width="80px" TabIndex="20" AutoPostBack="true" OnSelectedIndexChanged="ddlAvailableTimes_IndexChanged">
                                <asp:ListItem Text="-- Select --" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator Enabled="true" ControlToValidate="ddlAvailableTimes" ID="ddlAvailableTimesReqFV" runat="server" Display="None" InitialValue="0" ErrorMessage="Available time is required"></asp:RequiredFieldValidator>
                        </td>
                      <td width="61%" nowrap="nowrap" class="formFields">
                           <asp:DropDownList ID="ddlAvailableRooms" runat="server" CssClass="formFields" Width="180px" TabIndex="21">
                               <asp:ListItem Text="-- Select --" Value="0"></asp:ListItem>
                           </asp:DropDownList>
                           <asp:RequiredFieldValidator Enabled="true" ControlToValidate="ddlAvailableRooms" ID="ddlAvailableRoomsReqFV" runat="server" Display="None" InitialValue="0" ErrorMessage="Available room is required"></asp:RequiredFieldValidator>
                       </td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                <td style="padding:12px">
                <table celpadding="0" cellspacing="0" border="0" width="100%">
                <tr><td>
                    <asp:LinkButton ID="lnkScheduleAppointment" CssClass="wagsSchedBigButton" OnClick="lnkScheduleAppointment_Click" runat="server" TabIndex="22">Schedule Appointment</asp:LinkButton> &nbsp; <asp:LinkButton ID="lnkCancelAppointment" CssClass="wagsSchedBigButton" Visible="false" OnClick="lnkCancelAppointment_Click" runat="server" TabIndex="23">Cancel Appointment</asp:LinkButton>
                </td>
                <td align="right" class="formFields" valign="bottom">* Required Fields</td>
                </tr>
                </table>
                </td>
                </tr>
              </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
    </div>
    <table id="tblfooter" border="0" cellspacing="0" cellpadding="0" align="center" style="width:95%; max-width:800px; ">
            <tr>
                <td style="padding: 0px 46px 0px 48px; font-family:Arial, Helvetica, Sans-Serif; font-size:12px; text-decoration:none; color:#666666; line-height: 18px; text-align: left; vertical-align:top;" >
                    <p>
                        For your protection, only your name and email will be stored to identify your appointment. All other personal data will be removed
                        when you have completed the scheduling and confirmation process. Email addresses stored for appointments will not be used for
                        any other purpose than communicating with the customer about the scheduled immunization clinic and will be flushed from our
                        system after the clinic has been completed.</p>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
