﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;
using TdApplicationLib;
using System.Xml;
using System.Data;
using Microsoft.Reporting.WebForms;

public partial class corporateScheduler_clientSchedulerApptTracker : System.Web.UI.Page
{
    #region -------------------- PAGE EVENTS --------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        bool is_valid = true;
        int has_clinics = -1;
        //string redirect_page = string.Empty;
        if (Request.QueryString["args"] != null && Request.QueryString["args"] != "")
        {
            string args_value = Request.QueryString["args"].Replace(" ", "+");

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(args_value))
                    is_valid = false;
                else
                {
                    int design_pk = 0;
                    EncryptQueryStringAES args = new EncryptQueryStringAES();
                    try
                    {
                        args = new EncryptQueryStringAES(args_value);
                    }
                    catch
                    {
                        is_valid = false;
                    }

                    if (args.Count() > 2)
                    {
                        Int32.TryParse(args["arg1"], out design_pk);
                        if (design_pk > 0)
                        {
                            this.hfSchedulerClientId.Value = design_pk.ToString();
                            string client_scheduler_xml = this.dbOperation.getCorporateSchedulerDesign(design_pk, out has_clinics);
                            if (client_scheduler_xml.Length > 0)
                            {
                                this.clientSchedulerXML.LoadXml(client_scheduler_xml);
                                this.commonAppSession.SelectedStoreSession.schedulerDesignXML = this.clientSchedulerXML.OuterXml;
                            }
                            else
                                is_valid = false;
                        }
                        else
                            is_valid = false;
                    }
                    else
                        is_valid = false;
                }

                if (is_valid)
                {
                    this.displaySchedulerLandingPage(is_valid, has_clinics);

                    //Get client appointments
                    this.lnkShowScheduledAppts.Visible = false;
                    //this.lnkViewScheduledApptTracking.Visible = false;
                    this.rowAppointmentsReport.Visible = false;
                    this.bindScheduledAppointments();
                }
                else if (!is_valid)
                    this.displaySchedulerLandingPage(is_valid, has_clinics);
            }
        }
        else
            this.displaySchedulerLandingPage(false, has_clinics);
    }

    protected void grdScheduledAppts_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //if (Convert.ToBoolean(((DataRowView)e.Row.DataItem)["isBlocked"]))
            //{
            //    e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#99999f");
            //}
            Label lbl_clinic_address = (Label)e.Row.FindControl("lblclinicAddress");
            lbl_clinic_address.Text = lbl_clinic_address.Text.Replace("<br />", " ");
        }
    }

    protected void grdScheduledAppts_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdScheduledAppts.PageIndex = e.NewPageIndex;
        this.bindScheduledAppointments();
    }

    protected void grdScheduledAppts_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindScheduledAppointments();
    }

    protected void doProcess_Click(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString())
        {
            case "Logout":
                Response.Redirect("~/corporateScheduler/clientschedulerThankYou.aspx", true);
                break;
            case "Search":
                this.grdScheduledAppts.PageIndex = 0;

                this.bindScheduledAppointments();
                break;
            case "Clear Search":
                this.txtSearchAppointments.Text = "";
                this.grdScheduledAppts.PageIndex = 0;
                this.bindScheduledAppointments();
                break;
            case "View Report":
                this.lblClientScheduleApptTotal.Visible = false;
                this.txtSearchAppointments.Visible = false;
                this.lnkSearchScheduledAppt.Visible = false;
                this.lnkShowAllScheduledAppt.Visible = false;
                this.lnkShowUnScheduledAppts.Visible = false;
                this.lnkShowScheduledAppts.Visible = false;
                this.lnkViewScheduledApptReport.Visible = false;
                this.lnkViewScheduledApptTracking.Visible = true;
                this.rowSchedulerAppointment.Visible = false;
                this.rowAppointmentsReport.Visible = true;

                int client_id = 0;
                Int32.TryParse(this.hfSchedulerClientId.Value, out client_id);
                DataSet ds_corporate_clients = this.dbOperation.getCorporateClientsForReports("Client", null, client_id);
                List<string> lst_clinics = new List<string>();

                if (ds_corporate_clients.Tables[0].Rows.Count > 0)
                {
                    foreach (string clinic_dates in ds_corporate_clients.Tables[1].Rows.OfType<DataRow>().Select(row => row["clinicDate"].ToString()).ToList())
                    {
                        clinic_dates.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                            .ToList()
                            .ForEach(clinic_date => lst_clinics.Add(clinic_date));
                    }

                    this.ddlClinicDates.DataSource = lst_clinics.Distinct().OrderBy(x => x.ToString()).ToList();
                    this.ddlClinicDates.DataBind();
                    if (this.ddlClinicDates.Items.Count > 1)
                        this.ddlClinicDates.Items.Insert(0, new ListItem("All", ""));
                    else
                        this.ddlClinicDates.Items[0].Selected = true;

                    lst_clinics.Clear();
                    foreach (string clinic_rooms in ds_corporate_clients.Tables[1].Rows.OfType<DataRow>().Select(row => row["clinicRoom"].ToString()).ToList())
                    {
                        clinic_rooms.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                            .ToList()
                            .ForEach(clinic_room => lst_clinics.Add(clinic_room.Trim()));
                    }

                    this.ddlClinicRooms.DataSource = lst_clinics.Distinct().OrderBy(x => x.ToString()).ToList();
                    this.ddlClinicRooms.DataBind();
                    this.ddlClinicRooms.Items.Insert(0, new ListItem("All", ""));
                }
                else
                {
                    this.ddlClinicDates.Items.Insert(0, new ListItem("-- Select --", ""));
                    this.ddlClinicRooms.Items.Insert(0, new ListItem("-- Select --", ""));
                    this.ddlClinicDates.Enabled = false;
                    this.ddlClinicRooms.Enabled = false;
                    this.ddlApptTypes.Enabled = false;
                    this.btnSubmit.Enabled = false;
                }

                this.reportBind();
                break;
            case "View Tracking":
                this.lblClientScheduleApptTotal.Visible = true;
                this.txtSearchAppointments.Visible = true;
                this.lnkSearchScheduledAppt.Visible = true;
                this.lnkShowAllScheduledAppt.Visible = true;
                this.lnkShowUnScheduledAppts.Visible = true;
                this.lnkShowScheduledAppts.Visible = false;
                this.lnkViewScheduledApptReport.Visible = true;
                this.lnkViewScheduledApptTracking.Visible = false;
                this.rowSchedulerAppointment.Visible = true;
                this.rowAppointmentsReport.Visible = false;
                break;
            case "Show Unscheduled":
                this.lnkShowUnScheduledAppts.Visible = false;
                this.lnkShowScheduledAppts.Visible = true;
                this.bindScheduledAppointments();
                break;
            case "Show Scheduled":
                this.lnkShowUnScheduledAppts.Visible = true;
                this.lnkShowScheduledAppts.Visible = false;
                this.bindScheduledAppointments();
                break;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }
    #endregion

    #region --------- PRIVATE FUNCTIONS ------------------
    /// <summary>
    /// Displayes scheduler site page
    /// </summary>
    private void displaySchedulerLandingPage(bool is_valid, int has_clinics)
    {
        this.rowSchedulerInactive.Visible = true;
        this.rowSchedulerAppointment.Visible = false;

        if (!is_valid)
        {
            this.rowAppointmentsReport.Visible = false;
            this.lnkBtnClose.Visible = false;
            this.bannerPreview.Style.Add("background-color", "#3096D8");
            this.bannerPreview.Style.Add("color", "#FFFFFF");
            this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=wags_logo.png&extraQS=" + DateTime.Now.Second;
            this.lblWarningMesg.Text = "This is an invalid link. Please contact your Walgreens representative for a valid scheduling link.";
            return;
        }

        this.bannerPreview.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
        this.bannerPreview.Style.Add("color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerTextColor"].Value);
        if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value))
            this.clientLogo.Visible = false;
        else
            this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=" + this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value + "&extraQS=" + DateTime.Now.Second;

        if (is_valid && has_clinics == -1)
        {
            this.rowSchedulerInactive.Visible = true;
            this.rowSchedulerAppointment.Visible = false;
            this.rowAppointmentsReport.Visible = false;
            this.lblWarningMesg.Text = "Sorry, this clinic is not scheduled.";
        }
        else
        {
            this.rowSchedulerInactive.Visible = false;
            this.rowSchedulerAppointment.Visible = true;
        }
    }

    /// <summary>
    /// Binds scheduled appointments
    /// </summary>
    private void bindScheduledAppointments()
    {
        DataTable dt_scheduled_appts = new DataTable();
        int client_id = 0;
        Int32.TryParse(this.hfSchedulerClientId.Value, out client_id);
        if (!this.lnkShowUnScheduledAppts.Visible)
        {
            this.lblClientScheduleApptTotal.Text = @"Total Unscheduled: ";
            dt_scheduled_appts = this.dbOperation.getCorporateClientScheduledApptsReport("Client", null, client_id, 0, 2);
        }
        else
        {
            this.lblClientScheduleApptTotal.Text = @"Total Scheduled: ";
            dt_scheduled_appts = this.dbOperation.getCorporateClientScheduledApptsReport("Client", null, client_id, 0, 1);
        }

        if (dt_scheduled_appts.Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(this.txtSearchAppointments.Text))
            {
                DataRow[] dr_appts = dt_scheduled_appts.Select("businessClinic LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR clinicAddress LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR PatientName LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR employeeId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR confirmationId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptAvlTimes LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptDate LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%'");
                if (dr_appts.Count() > 0)
                    dt_scheduled_appts = dr_appts.CopyToDataTable();
                else
                    dt_scheduled_appts.Clear();
            }

            if (ViewState["sortOrder"] != null)
                dt_scheduled_appts.DefaultView.Sort = ViewState["sortOrder"].ToString();

            this.lblClientScheduleApptTotal.Text += dt_scheduled_appts.Rows.Count.ToString();
        }

        this.grdScheduledAppts.DataSource = dt_scheduled_appts;
        this.grdScheduledAppts.DataBind();
        //else
        //{
        //    this.displaySchedulerLandingPage(true, -1);
        //}
    }

    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }

    /// <summary>
    /// Generates report scheduled appointments report
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        int client_id = 0;
        Int32.TryParse(this.hfSchedulerClientId.Value, out client_id);

        this.ScheduledAppointmentReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getCorporateClientScheduledApptsReport("Client", null, client_id, 0, (string.IsNullOrEmpty(this.ddlApptTypes.SelectedValue) ? 0 : Convert.ToInt32(this.ddlApptTypes.SelectedValue)), this.ddlClinicRooms.SelectedValue, this.ddlClinicDates.SelectedValue);

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblClinicSchedulerEeRegistry";
        rds.Value = data_tbl;
        this.ScheduledAppointmentReport.LocalReport.DataSources.Clear();
        this.ScheduledAppointmentReport.LocalReport.DataSources.Add(rds);
        this.ScheduledAppointmentReport.ShowPrintButton = false;
        this.ScheduledAppointmentReport.LocalReport.ReportPath = Server.MapPath("~/reports/scheduledAppointment.rdlc");

        data_tbl = null;
    }
    #endregion

    #region --------- PRIVATE VARIABLES ------------------
    private DBOperations dbOperation = null;
    private AppCommonSession commonAppSession = null;
    private XmlDocument clientSchedulerXML;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.clientSchedulerXML = new XmlDocument();
    }
    #endregion
}