﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="clientSchedulerThankYou.aspx.cs" Inherits="corporateScheduler_clientSchedulerThankYou" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Walgreens- Onsite Client Scheduling Appointment Tracker</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    .ui-dialog-titlebar-close {
        visibility: hidden;
    }

    .ui-widget
    {
        font-size:11px;
    }
        
    .ui-widget-header
    {
        border-bottom:0px none #FFFFFF;           
    }
        
    .ui-tabs .ui-tabs-panel
    {
        padding:0px;           
    }
        
    .ui-state-active, .ui-state-active, .ui-widget-header .ui-state-active        
    {            
        background:url("../images/tab_gradient.jpg") repeat-x scroll 50% 50%;
	    background-repeat: repeat-x;
	    background-position:bottom; 
    }
    .ui-tabs .ui-tabs-nav li a, .ui-tabs.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-selected a {
	    cursor: pointer;
	    text-align: left;
    }
    .ui-tabs .ui-tabs-nav li a 
    {
	    padding-top:5px;
	    padding-left:15px;		    
    }    
    .gridStyles
    {
        border-bottom-width:1px;
        border-bottom-style:solid;
        border-bottom-color:#d6d6d6;
    }
    .gridHeaderStyle
    {
        border-bottom-width:2px;
        border-bottom-style:solid;
        border-bottom-color:#646464;
    }
    .closeButton
    {
        font-size:13px;
        font-weight:bold;
        color:#5eafda;
    }
    .grdScheduledApptsPagerStyle
    {
        color:#060606;
        font-size:11px;        
    }
    .grdCorporateClinicsPagerStyle
    {
        color:#060606;
        font-size:11px;        
    }
    
    .formReqFields
    {
        text-align:center;
        padding-left:10px;
    }
    
    .wrapword
    { 
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */        
        white-space: -pre-wrap;      /* Opera 4-6 */ 
        white-space: -o-pre-wrap;    /* Opera 7 */ 
        word-wrap: break-word;       /* Internet Explorer 5.5+ */        
        -ms-word-break: break-all;
        word-break: break-all;
        text-overflow: break-all;
    }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient" onload="javascript:window.history.forward(1);">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow" width="935" style="width:95%; max-width:935px;">
          <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:12px 0px 10px 16px;"><asp:Image ID="clientLogo" runat="server" AlternateText="Client Logo" /></td>            
          </tr>
          <tr>            
            <td id="bannerPreview" runat="server" align="left" style="height: 38px; padding-top:0px; padding-bottom: 0px; padding-right: 24px; padding-left: 24px; font-family: Arial, Helvetica, sans-serif; font-size: 18px; " >Immunization Clinic Appointment Tracking</td>
          </tr>        
        <tr id="rowSchedulerInactive">
        <td bgcolor="#FFFFFF" >
            <table width="100%" border="0" cellspacing="0" cellpadding="0">              
            <tr>
                <td align="left" style="height: 38px; padding-top:0px; padding-left: 24px; font-family: Arial, Helvetica, sans-serif; font-size: 24px; " ><asp:Image ID="imgThankYouApptTracker" runat="server" width="790" height="375" ImageUrl="~/images/thank_you_appt_track.jpg" />
                </td>
            </tr>
            <%--<tr>
            <td class="footerText">&nbsp;</td>
            </tr>--%>
        </table></td>
        </tr>
        </table>        
    </div>    
  </form>
</body>
</html>
