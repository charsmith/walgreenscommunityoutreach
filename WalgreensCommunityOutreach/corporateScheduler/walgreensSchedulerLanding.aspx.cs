﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdWalgreens;
using TdApplicationLib;
using System.IO;


public partial class corporateScheduler_walgreensSchedulerLanding : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool is_valid = true;
        int has_clinics = -1;
        string redirect_page = string.Empty;
        if (Request.QueryString["args"] != null && Request.QueryString["args"] != "")
        {
            string args_value = Request.QueryString["args"].Replace(" ", "+");

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(args_value))
                    is_valid = false;
                else
                {
                    int design_pk = 0;
                    EncryptQueryStringAES args = new EncryptQueryStringAES();
                    try
                    {
                        args = new EncryptQueryStringAES(args_value);
                    }
                    catch
                    {
                        is_valid = false;
                    }

                    if (args.Count() > 2)
                    {
                        Int32.TryParse(args["arg1"], out design_pk);
                        if (design_pk > 0)
                        {
                            this.commonAppSession.SelectedStoreSession.schedulerDesignPk = design_pk;
                            string client_scheduler_xml = this.dbOperation.getCorporateSchedulerDesign(design_pk, out has_clinics);
                            if (client_scheduler_xml.Length > 0)
                            {
                                this.clientSchedulerXML.LoadXml(client_scheduler_xml);
                                this.commonAppSession.SelectedStoreSession.schedulerDesignXML = this.clientSchedulerXML.OuterXml;
                            }
                            else
                                is_valid = false;

                            redirect_page = args["arg3"];

                           //this.commonAppSession.SelectedStoreSession.scheduledApptXML = null;
                            //this.commonAppSession.SelectedStoreSession.scheduledExistingApptXML = null;
                            this.commonAppSession.SelectedStoreSession.schedulerClinics = new System.Data.DataSet();

                            if (args.Count() == 4)
                                this.commonAppSession.SelectedStoreSession.schedulerApptPk = !string.IsNullOrEmpty(args["arg2"]) ? Convert.ToInt32(args["arg2"]) : 0;
                            else
                                this.commonAppSession.SelectedStoreSession.schedulerApptPk = 0;

                            Session["logString"] = "Server Name: " + Request.ServerVariables["SERVER_NAME"] + "; Host Address: " + Request.ServerVariables["REMOTE_HOST"]
                                + "; Browser: " + Request.Browser.Browser + "; Version: " + Request.Browser.MajorVersion.ToString() + "; User Agent: "
                                + Request.UserAgent.ToString() + "; DesignPk: " + this.commonAppSession.SelectedStoreSession.schedulerDesignPk.ToString()
                                + "; ApptPk: " + this.commonAppSession.SelectedStoreSession.schedulerApptPk.ToString();
                        }
                        else
                            is_valid = false;
                    }
                    else
                        is_valid = false;
                }

                if (is_valid)
                {
                    if (redirect_page != "walgreensSchedulerLanding.aspx" && has_clinics == 1)
                    {
                        this.commonAppSession.SelectedStoreSession.referrerPath = "walgreensSchedulerLanding.aspx";
                        Response.Redirect(redirect_page);
                    }
                    else
                        this.displaySchedulerLandingPage(is_valid, has_clinics);
                }
                else if (!is_valid)
                    this.displaySchedulerLandingPage(is_valid, has_clinics);
            }
        }
        else
            this.displaySchedulerLandingPage(false, has_clinics);
    }

    protected void lnkScheduleAppointment_Click(object sender, EventArgs e)
    {
        this.commonAppSession.SelectedStoreSession.referrerPath = "walgreensSchedulerLanding.aspx";
        Response.Redirect("walgreensSchedulerRegistration.aspx");
    }

    #region --------- PRIVATE FUNCTIONS ------------------
    /// <summary>
    /// Displayes scheduler landing page
    /// </summary>
    private void displaySchedulerLandingPage(bool is_valid, int has_clinics)
    {
        this.rowSchedulerInactive.Visible = true;
        this.rowSchedulerAppointment.Visible = false;
        if (!is_valid)
        {
            this.bannerPreview.Style.Add("background-color", "#3096D8");
            this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=wags_logo.png&extraQS=" + DateTime.Now.Second;
            this.lblWarningMesg.Text = "This is an invalid link. Please contact your Walgreens representative for a valid scheduling link.";
            return;
        }

        this.bannerPreview.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
        this.headerText = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/headerText").InnerText.Replace("&lt;", "<").Replace("&gt;", ">");
        if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value))
            this.clientLogo.Visible = false;
        else
            this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=" + this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value + "&extraQS=" + DateTime.Now.Second;

        if (is_valid && has_clinics == -1)
            this.lblWarningMesg.Text = "Sorry, this clinic is not scheduled.";
        else if ((has_clinics == 0) ||
                 (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["siteStatus"].Value == "Inactive") ||
                 (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["deactivationDate"].Value) &&
                        Convert.ToDateTime(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["deactivationDate"].Value) <= DateTime.Today.Date))
            this.lblWarningMesg.Text = "Sorry, scheduling has been closed for this clinic.";
        else
        {
            this.rowSchedulerInactive.Visible = false;
            this.rowSchedulerAppointment.Visible = true;

            this.lnkScheduleAppointment.BackColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
            this.lnkScheduleAppointment.ForeColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);
            this.bodyText = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/bodyText").InnerText.Replace("&lt;", "<").Replace("&gt;", ">");
            this.footerText = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/footerText").InnerText.Replace("&lt;", "<").Replace("&gt;", ">");
        }
    }
    #endregion

    #region --------- PRIVATE VARIABLES ------------------
    private DBOperations dbOperation = null;
    private AppCommonSession commonAppSession = null;
    private XmlDocument clientSchedulerXML;
    #endregion

    #region --------- PUBLIC VARIABLES ------------------
    public string headerText;
    public string bodyText;
    public string footerText;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.clientSchedulerXML = new XmlDocument();
    }
    #endregion
}