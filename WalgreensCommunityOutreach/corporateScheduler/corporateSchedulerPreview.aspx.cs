﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using TdWalgreens;
using System.Xml;

public partial class corporateScheduler_corporateSchedulerPreview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["clientDetails"] != null)
            {
                this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
                this.clientSchedulerXML.LoadXml(this.corporateScheduler.clinicSchedulerXML);

                this.bannerPreview.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
                if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value))
                    this.clientLogoPreview.Visible = false;
                else
                    this.clientLogoPreview.ImageUrl = "~/controls/displayImage.ashx?image=" + this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value + "&extraQS=" + DateTime.Now.Second;

                this.lnkScheduleAppointment.BackColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value);
                this.lnkScheduleAppointment.ForeColor = System.Drawing.ColorTranslator.FromHtml(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value);
                this.HeaderTextPreview = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/headerText").InnerText.Replace("&lt;", "<").Replace("&gt;", ">");
                this.bodyTextPreview = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/bodyText").InnerText.Replace("&lt;", "<").Replace("&gt;", ">");
                this.footerTextPreview = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/footerText").InnerText.Replace("&lt;", "<").Replace("&gt;", ">");
            }
        }
        else
            Response.Redirect("walgreensHome.aspx");
    }

    #region ------------------ PUBLIC VARIABLES -----------------
    public string HeaderTextPreview;
    public string bodyTextPreview;
    public string footerTextPreview;
    #endregion

    #region ----------------- PRIVATE VARIABLES -----------------
    protected AppCommonSession commonAppSession = null;        
    private CorporateScheduler corporateScheduler;
    private XmlDocument clientSchedulerXML;
    #endregion
    
    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();        
        this.corporateScheduler = new CorporateScheduler();
        this.clientSchedulerXML = new XmlDocument();
    }
    #endregion
}