﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="clientSchedulerApptTracker.aspx.cs" Inherits="corporateScheduler_clientSchedulerApptTracker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Walgreens- Onsite Client Scheduling Appointment Tracker</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function (e) {
            var $tabs = $('#tabs').tabs();

            $("#ScheduledAppointmentReport").css("height", "80%");
            $("#ScheduledAppointmentReport_ctl10").css("height", "450px");
            $("#ScheduledAppointmentReport_ctl10").css("width", "850px");
            $("#ScheduledAppointmentReport_ctl09").css("height", "450px");
            $("#ScheduledAppointmentReport_ctl09").css("width", "850px");
        });


        function validateSearchText() {
            var value;
            value = $('#txtSearchAppointments').val();
            $('#txtSearchAppointments').val(value.replace(/</g, "").replace(/>/g, ""));
        }
    </script>
    <style type="text/css">
    .ui-dialog-titlebar-close {
        visibility: hidden;
    }

    .ui-widget
    {
        font-size:11px;
    }
        
    .ui-widget-header
    {
        border-bottom:0px none #FFFFFF;           
    }
        
    .ui-tabs .ui-tabs-panel
    {
        padding:0px;           
    }
        
    .ui-state-active, .ui-state-active, .ui-widget-header .ui-state-active        
    {            
        background:url("../images/tab_gradient.jpg") repeat-x scroll 50% 50%;
	    background-repeat: repeat-x;
	    background-position:bottom; 
    }
    .ui-tabs .ui-tabs-nav li a, .ui-tabs.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-selected a {
	    cursor: pointer;
	    text-align: left;
    }
    .ui-tabs .ui-tabs-nav li a 
    {
	    padding-top:5px;
	    padding-left:15px;		    
    }    
    .gridStyles
    {
        border-bottom-width:1px;
        border-bottom-style:solid;
        border-bottom-color:#d6d6d6;
    }
    .gridHeaderStyle
    {
        border-bottom-width:2px;
        border-bottom-style:solid;
        border-bottom-color:#646464;
    }
    .closeButton
    {
        font-size:13px;
        font-weight:bold;
        color:#5eafda;
    }
    .grdScheduledApptsPagerStyle
    {
        color:#060606;
        font-size:11px;        
    }
    .grdCorporateClinicsPagerStyle
    {
        color:#060606;
        font-size:11px;        
    }
    
    .formReqFields
    {
        text-align:center;
        padding-left:10px;
    }
    
    .wrapword
    { 
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */        
        white-space: -pre-wrap;      /* Opera 4-6 */ 
        white-space: -o-pre-wrap;    /* Opera 7 */ 
        word-wrap: break-word;       /* Internet Explorer 5.5+ */        
        -ms-word-break: break-all;
        word-break: break-all;
        text-overflow: break-all;
    }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient" onload="javascript:window.history.forward(1);">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfSchedulerClientId" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow" width="935" style="width:95%; max-width:935px;">
          <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:12px 0px 10px 16px;"><asp:Image ID="clientLogo" runat="server" AlternateText="Client Logo" /></td>
            <td align="right" valign="top" bgcolor="#FFFFFF" class="formFields2" style="padding-left:8px; padding-right:8px; padding-bottom:8px; padding-top:8px; " >
                <asp:LinkButton ID="lnkBtnClose" runat="server" Width="144px" CssClass="closeButton" Text="Logout" CommandArgument="Logout" OnCommand="doProcess_Click" ></asp:LinkButton>
            </td>
          </tr>
          <tr>            
            <td id="bannerPreview" runat="server" colspan="2" align="left" style="height: 38px; padding-top:0px; padding-bottom: 0px; padding-right: 24px; padding-left: 24px; font-family: Arial, Helvetica, sans-serif; font-size: 18px; " >Immunization Clinic Appointment Tracking</td>
          </tr>
          <tr id="rowSchedulerAppointment" runat="server">
            <td bgcolor="#FFFFFF" colspan="2" style="padding:10px;">
              <div id="tabs">
                <div id="idClientSchedulerAppts" style="background-color:#eeeeee">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top:5px;" >                
                    <tr>
                        <td align="left" valign="middle" class="formFields" style="padding-top: 10px; padding-left:10px; font-weight:bold"><asp:Label ID="lblClientScheduleApptTotal" runat="server"></asp:Label></td>
                        <td align="right" valign="middle" class="formFields" style="padding-top: 10px; padding-left:10px; padding-right:5px;">
                            <asp:Panel ID="pnlSearchAppointments" runat="server" DefaultButton="lnkSearchScheduledAppt">
                                <asp:TextBox ID="txtSearchAppointments" runat="server"></asp:TextBox>&nbsp;
                                <span style="padding-top: 0px; text-align:center;"><asp:LinkButton ID="lnkSearchScheduledAppt" runat="server" Width="65px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Search" CommandArgument="Search" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkShowAllScheduledAppt" runat="server" Width="85px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Clear Search" CommandArgument="Clear Search" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkShowUnScheduledAppts" runat="server" Width="130px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Show Unscheduled" CommandArgument="Show Unscheduled" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton><asp:LinkButton ID="lnkShowScheduledAppts" runat="server" Width="130px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Show Scheduled" CommandArgument="Show Scheduled" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkViewScheduledApptReport" runat="server" Width="85px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="View Report" CommandArgument="View Report" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton>
                                </span>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" valign="top" class="formFields2" style="padding:10px;">
                        <asp:GridView ID="grdScheduledAppts" runat="server" AutoGenerateColumns="False" CellPadding="7" CellSpacing="0" AllowPaging="true" PageSize="10" GridLines="None" Width="100%" 
                        AllowSorting="true" OnRowDataBound="grdScheduledAppts_OnRowDataBound" OnPageIndexChanging ="grdScheduledAppts_PageIndexChanging" OnSorting="grdScheduledAppts_sorting">
                            <FooterStyle CssClass="footerGrey" />
                            <HeaderStyle BackColor="#999999" ForeColor="White" Font-Size="12px" Height="30px" CssClass="gridHeaderStyle" />
                            <RowStyle Height="35px" BackColor="White" CssClass="gridStyles" />
                            <PagerStyle CssClass="grdScheduledApptsPagerStyle" />
                            <Columns>
                                <asp:BoundField HeaderText="Clinic" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="170px" DataField="businessClinic" SortExpression="businessClinic" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px" />
                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="clinicAddress" ItemStyle-Width="150px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblclinicAddress" runat="server" Text='<%# Bind("clinicAddress") %>' ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Appt Date" HeaderStyle-HorizontalAlign="Center" DataField="apptDate" SortExpression="apptDate" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="Appt Time" HeaderStyle-HorizontalAlign="Center" DataField="apptAvlTimes" SortExpression="apptTimeSortId" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="Appt Room" HeaderStyle-HorizontalAlign="Center" DataField="apptRoom" SortExpression="apptRoom" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="Time Slot" HeaderStyle-HorizontalAlign="Center" DataField="apptTimeSlot" SortExpression="apptTimeSlot" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                <asp:BoundField HeaderText="Name" HeaderStyle-HorizontalAlign="Center" DataField="PatientName" SortExpression="PatientName" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="Employee ID" HeaderStyle-HorizontalAlign="Center" DataField="employeeId" SortExpression="employeeId" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="Confirmation ID" HeaderStyle-HorizontalAlign="Center" DataField="confirmationId" SortExpression="confirmationId" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <EmptyDataTemplate>
                                <center>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:White; text-align: center;" >
                                        <tr style="height:35px;">
                                            <td>
                                                <b><asp:Label ID="lblNote" Text="No records found" CssClass="formFields" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </EmptyDataTemplate>
                            <SelectedRowStyle BackColor="LightBlue" />
                        </asp:GridView>
                        </td>
                    </tr>
                    
                </table>
                </div>
              </div>
           </td>
        </tr>
        <tr id="rowAppointmentsReport" runat="server">
            <td colspan="2">
            <table width="95%" border="0" cellspacing="0" cellpadding="0" style="padding:10px; background-color: White;" >
            <tr>
                <td valign="top" class="pageTitle">Corporate Clinic Scheduled Appointments Report</td>
                <td align="right" valign="top" class="formFields" style="padding-top: 5px; padding-left:10px; padding-right:5px; border-bottom-width:1px; border-bottom-style: solid; border-bottom-color:#999;">
                <span style="padding-top: 0px; text-align:center;">
                    <asp:LinkButton ID="lnkViewScheduledApptTracking" runat="server" Width="115px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="View Tracking" CommandArgument="View Tracking" OnCommand="doProcess_Click" ></asp:LinkButton>
                </span>
                </td>
            </tr>
            <tr id="rowCorporateClientsList">
                <td align="left" colspan="2">
                    <table  border="0" cellspacing="5" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" valign="top" nowrap="nowrap" width="120px" ><span class="logSubTitles">Clinic Date: </span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="formFields" ID="ddlClinicDates" runat="server" Width="120px" AutoPostBack="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" nowrap="nowrap" width="120px" ><span class="logSubTitles">Room: </span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="formFields" ID="ddlClinicRooms" runat="server" Width="120px" AutoPostBack="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" nowrap="nowrap" width="120px" ><span class="logSubTitles">Appointments: </span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="formFields" ID="ddlApptTypes" runat="server" Width="120px" >
                                    <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Scheduled" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Unscheduled" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;&nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="logSubTitles" Text="Submit" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="601" colspan="2" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;"> 
                    <rsweb:ReportViewer ID="ScheduledAppointmentReport" runat="server"  Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3" AsyncRendering="true" PageCountMode="Actual"><LocalReport></LocalReport></rsweb:ReportViewer>
                </td>
            </tr>
            </table>
            </td>
        </tr>
        <tr id="rowSchedulerInactive" runat="server">
        <td bgcolor="#FFFFFF" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">              
            <tr>
                <td class="bodyText"><p><asp:Label ID="lblWarningMesg" runat="server"></asp:Label></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p></td>
            </tr>
            <tr>
            <td class="footerText">&nbsp;</td>
            </tr>
        </table></td>
        </tr>
        </table>        
    </div>    
  </form>
</body>
</html>
