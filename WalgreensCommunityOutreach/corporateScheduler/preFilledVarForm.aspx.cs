﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;
using TdApplicationLib;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using PdfSharp.Drawing;

public partial class corporateScheduler_prefilledVarForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string scheduled_appt = (Request.Form["hfScheduledExistingApptXML"] != null) ? HttpUtility.UrlDecode(Request.Form["hfScheduledExistingApptXML"].ToString()) : "";

        XmlDocument scheduled_appt_xml = new XmlDocument();
        byte[] new_pdf_bytes = null;
        if (scheduled_appt.Length > 0)
            scheduled_appt_xml.LoadXml(scheduled_appt);
        else
            HttpContext.Current.Response.Redirect("../auth/sessionTimeout.aspx");

        string confirmation_id = Request.Form["hflblConfirmationId"].ToString();
        string[] store_address = Regex.Split(scheduled_appt_xml.SelectSingleNode(".//field[@name='clinicLocation']").Attributes["value"].Value, "<br />");
        string clinic_groupid = scheduled_appt_xml.SelectSingleNode(".//field[@name='clinicGroupId']").Attributes["value"].Value;
        int groupid_length = clinic_groupid.Length;
        List<string> lst_groupids = new List<string>();
        int max_groupid_length = 36;

        if (groupid_length <= 36)
            lst_groupids.Add(clinic_groupid);
        else if (groupid_length <= 104)
        {
            while (clinic_groupid.Length > max_groupid_length)
            {
                for (int char_index = (max_groupid_length); char_index > 0; char_index--)
                {
                    if (char.IsWhiteSpace(clinic_groupid[char_index]))
                    {
                        lst_groupids.Add(clinic_groupid.Substring(0, char_index + 1));
                        clinic_groupid = clinic_groupid.Substring(char_index + 1);
                        break;
                    }
                }
            }
            lst_groupids.Add(clinic_groupid);
        }
        else
            lst_groupids.Add("Refer to Portal for Group ID");

        if (confirmation_id == scheduled_appt_xml.SelectSingleNode(".//field[@name='confirmationId']").Attributes["value"].Value)
        {
            WagStringDictionary wagStringDictionary = new WagStringDictionary();
            wagStringDictionary.Add("storeId", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='clinicStoreId']").Attributes["value"].Value, 430, 58, 6, 1));
            wagStringDictionary.Add("storeAddress", new WagPdfString(System.Web.HttpUtility.HtmlDecode(store_address[0]), 432, 67, 6, 1));
            wagStringDictionary.Add("storeAddress2", new WagPdfString(System.Web.HttpUtility.HtmlDecode(store_address[1]), 432, 75, 6, 1));
            wagStringDictionary.Add("firstName", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='firstName']").Attributes["value"].Value, 87, 128, 10, 1));
            wagStringDictionary.Add("lastname", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='lastName']").Attributes["value"].Value, 359, 128, 10, 1));
            wagStringDictionary.Add("dateOfBirth", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='dateOfBirth']").Attributes["value"].Value, 95, 146, 10, 1));
            wagStringDictionary.Add("age", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='dateOfBirth']").Attributes["value"].Value, 218, 146, 10, 1));
            wagStringDictionary.Add("gender", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='gender']").Attributes["value"].Value, 600, 146, 500, 1));
            wagStringDictionary.Add("phone", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='phone']").Attributes["value"].Value, 404, 146, 10, 1));
            wagStringDictionary.Add("address", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='address1']").Attributes["value"].Value, 102, 166, 10, 1));
            wagStringDictionary.Add("address", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='address2']").Attributes["value"].Value, 102, 166, 10, 1));
            wagStringDictionary.Add("city", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='city']").Attributes["value"].Value, 442, 166, 10, 1));
            wagStringDictionary.Add("state", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='state']").Attributes["value"].Value, 67, 184, 10, 1));
            wagStringDictionary.Add("zipCode", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='zipCode']").Attributes["value"].Value, 170, 184, 10, 1));
            wagStringDictionary.Add("email", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='email']").Attributes["value"].Value, 293, 184, 10, 1));
            wagStringDictionary.Add("fullName", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='firstName']").Attributes["value"].Value, 97, 32, 10, 2));
            wagStringDictionary.Add("fullName", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='middleName']").Attributes["value"].Value, 97, 32, 10, 2));
            wagStringDictionary.Add("fullName", new WagPdfString(scheduled_appt_xml.SelectSingleNode(".//field[@name='lastName']").Attributes["value"].Value, 97, 32, 10, 2));

            int size = 68;
            int groupid_line_count = 0;
            for (int count = 0; count < lst_groupids.Count(); count++)
            {
                wagStringDictionary.Add("clinicGroupId" + groupid_line_count + "", new WagPdfString(lst_groupids[count], 240, size, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                size = size + 8;
                groupid_line_count = groupid_line_count + 1;
            }

            string var_form_2017 = ApplicationSettings.resourcesPath + "2017_18\\VAR_Form_2017_prefill.pdf";
            if (File.Exists(var_form_2017))
            {
                WagPdfMaker wagPdfMaker = new WagPdfMaker(var_form_2017);
                new_pdf_bytes = wagPdfMaker.makePdf(wagStringDictionary);
                if (new_pdf_bytes != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", new_pdf_bytes.Length.ToString());
                    Response.BinaryWrite(new_pdf_bytes);
                }
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('" + (string)GetGlobalResourceObject("errorMessages", "multipleTabs") + "');", true);
            return;
        }
    }
}