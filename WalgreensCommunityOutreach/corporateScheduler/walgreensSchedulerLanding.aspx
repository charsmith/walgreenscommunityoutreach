﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensSchedulerLanding.aspx.cs" Inherits="corporateScheduler_walgreensSchedulerLanding" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title>Walgreens- Onsite Clinic Scheduling</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
  <link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
  <link href="../css/wags.css" rel="stylesheet" type="text/css" />
  <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>    
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
  <form id="form1" runat="server">
    <div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow" style="width:95%; max-width:800px;">
          <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px"><asp:Image ID="clientLogo" runat="server" AlternateText="Client Logo" /></td>
          </tr>
          <tr>            
            <td id="bannerPreview" runat="server" colspan="2" align="left" style="height: 2px; padding-top:0px; padding-bottom: 0px; padding-right: 24px; padding-left: 24px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;" valign="top" >&nbsp;</td>
          </tr>
          <tr id="rowSchedulerAppointment" runat="server">
            <td bgcolor="#FFFFFF">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="headlineText" ><%=headerText%></td>
                </tr>
                <tr>
                    <td class="bodyText"><%=bodyText%>
                    <p align="center" style="padding-top:24px"><asp:LinkButton ID="lnkScheduleAppointment" OnClick="lnkScheduleAppointment_Click" runat="server" CssClass="wagsSchedBigButton">Schedule Your Appointment</asp:LinkButton></p></td>
                </tr>
                <tr>
                    <td class="footerText"><%=footerText%></td>
                </tr>
              </table>              
           </td>
        </tr>
        <tr id="rowSchedulerInactive" runat="server">
            <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="headlineText" ><%=headerText%></td>
              </tr>
              <tr>
                  <td class="bodyText"><p><asp:Label ID="lblWarningMesg" runat="server"></asp:Label></p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                  <p>&nbsp;</p></td>
                </tr>
              <tr>
                <td class="footerText">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    </div>
  </form>
</body>
</html>
