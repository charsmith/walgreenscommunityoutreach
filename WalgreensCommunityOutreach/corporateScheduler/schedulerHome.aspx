﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="schedulerHome.aspx.cs" Inherits="corporateScheduler_schedulerHome" ValidateRequest="false" %>
<%@ Register src="../controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Walgreens- Onsite Clinic Scheduling</title>
 <link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
 <link href="../css/wags.css" rel="stylesheet" type="text/css" />
 <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
 <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
 <link href="../css/evol.colorpicker.css" rel="stylesheet" type="text/css" />
 <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
 <link href="../css/theme.css" rel="stylesheet" type="text/css" />
 <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>    
 <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>  
 <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
 <script src="../javaScript/evol.colorpicker.min.js" type="text/javascript"></script>
 <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
 
<script language="javascript" type="text/javascript">
    var tabId = '<%=commonAppSession.SelectedStoreSession.SelectedCorporateClinicsTabId %>';
    function setTab() {
        var selected_tab;
        switch (tabId) {            
            case "idClinics":
                selected_tab = 0;
                break;
            case "idLogoStyle":
                selected_tab = 1;
                break;
            case "idLandingPage":
                selected_tab = 2;
                break;
            case "idFieldsAndStatus":
                selected_tab = 3;
                break;
            case "idAppointments":
                selected_tab = 4;
                break;
            default:
                selected_tab = 0;
                break;
        }
        var $tabs = $('#tabs').tabs();
        $tabs.tabs('select', selected_tab);
    }

    function showAlertMessage(msg) {
        setTab();
        alert(msg);        
    }

    function showBlockUnblockApptsSuccessMesg(msg) {
        setTab();
        alert(msg);
        __doPostBack("reloadAppointments");
    }

    function bindSchedulerDetails(msg) {
        setTab();
        alert(msg);
        __doPostBack("reloadScheduler");
    }
   
    function setSelectedTab(home_tab_id) {
        $.ajax({
            url: "schedulerHome.aspx/setSelectedTabId",
            type: "POST",
            data: "{'home_tab_id':'" + home_tab_id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {                
            }
        });
        showHideButtons(home_tab_id);
    }

    function showHideButtons(home_tab_id) {
        switch (home_tab_id) {
            case "idClinics":
            case "idLogoStyle":
            case "idLandingPage":
            case "idFieldsAndStatus":
                $("#lnkBlockCheckedAppts").css("display", "none");
                $("#lnkUnBlockCheckedAppts").css("display", "none");
                $("#lnkPreviewScheduler").css("display", "inline-block");
                break;
            case "idAppointments":
                $("#lnkBlockCheckedAppts").css("display", "inline-block");
                $("#lnkUnBlockCheckedAppts").css("display", "inline-block");
                $("#lnkPreviewScheduler").css("display", "none");
                break;
            default:
                $("#lnkBlockCheckedAppts").css("display", "none");
                $("#lnkUnBlockCheckedAppts").css("display", "none");
                $("#lnkPreviewScheduler").css("display", "inline-block");
                break;
        }
    }

    function replaceScriptTags() {        
        var value;
        value = $('#txtSchedulerLPheaderText').val();
        $('#txtSchedulerLPheaderText').val(value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        value = $('#txtSchedulerLPbodyText').val();
        $('#txtSchedulerLPbodyText').val(value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        value = $('#txtSchedulerLPfootertext').val();
        $('#txtSchedulerLPfootertext').val(value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));

        saveRegVisibleFields();
    }

    function checkSelection() {
        var is_valid = true;
        if ($("#ddlCorporateClients").val() == "") {
            alert('Please select a corporate client');
            return false;
        }
        else {
            $("#grdCorporateClinics tr.gridStyles").each(function () {
                var min_immunizers = $(this).find("select[id$='ddlImmunizers']");
                var max_immunizers = $(this).find("select[id$='ddlImmunizersMax']");
                if (!$(min_immunizers)[0].disabled && !$(max_immunizers)[0].disabled)
                {
                    if (min_immunizers.val() > max_immunizers.val()) {
                        alert('Min Immunizers should not exceed Max Immunizers');
                        is_valid = false;
                    }
                }
            })
            if (is_valid) {
                replaceScriptTags();
                return true;
            }
            else
                return false;
        }
    }
    
    function validateSearchText() {
        var value;
        value = $('#txtSearchAppointments').val();
        $('#txtSearchAppointments').val(value.replace(/</g, "").replace(/>/g, ""));
        replaceScriptTags();
    }

    function checkSelected(action) {
        var is_selected = false;

        if (action == 'block') {
            $("#grdScheduledAppts INPUT[type='checkbox']:checked").parent().parent().each(function () {
                if ($(this).find('td').length > 0 && $(this).find('td') != undefined && ($(this).find('td')[6].innerHTML != 'BLOCKED OUT'&&$(this).find('td')[6].innerHTML != 'PHARMACIST BREAK')) {
                    is_selected = true;
                }
            });
        }
        else if (action == 'unblock') {
            $("#grdScheduledAppts INPUT[type='checkbox']:checked").parent().parent().each(function () {
                if ($(this).find('td').length > 0 && $(this).find('td') != undefined && ($(this).find('td')[6].innerHTML == 'BLOCKED OUT' || $(this).find('td')[6].innerHTML == 'PHARMACIST BREAK')) {
                    is_selected = true;
                }
            });
        }
        if (is_selected) {
            replaceScriptTags();
            return true;
        }
        else {
            alert('Please select an appointment to ' + action);
            return false;
        }
    }

    function openPreviewWindow() {
        window.open('corporateSchedulerPreview.aspx', '_new');
    }

    function showSaveChangesWarning(alert) {
        $(function () {
            $("#divConfirmDialog").html(alert);
            $("#divConfirmDialog").dialog({
                closeOnEscape: false,
                beforeclose: function (event, ui) { return false; },
                dialogClass: "noclose",
                height: 200,
                width: 375,
                title: "Save changes",
                buttons: {
                    "Don't Save": function () {
                        replaceScriptTags();
                        __doPostBack("closeSchedulerSetup", 0);
                        $(this).dialog('close');
                    },
                    "Save": function () {
                        replaceScriptTags();
                        __doPostBack("closeSchedulerSetup", 1);
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        });
    }

    function showUnscheduleClinicWarning(alert) {
        $(function () {
            $("#divConfirmDialog").html(alert);
            $("#divConfirmDialog").dialog({
                closeOnEscape: false,
                beforeclose: function (event, ui) { return false; },
                dialogClass: "noclose",
                height: 170,
                width: 450,
                title: "Reschedule Appointment confirmation",
                buttons: {
                    'Continue': function () {
                        replaceScriptTags();
                        __doPostBack("rescheduleAppts", 1);
                        $(this).dialog('close');
                    },
                    'Cancel': function () {
                        replaceScriptTags();
                        __doPostBack("rescheduleAppts", 0);
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        });
    }

    function doBlockUnscheduledAppts() {
        replaceScriptTags();
        __doPostBack("blockSelectedAppts");
    }

    function showBlockApptsWarning(alert) {
        $(function () {
            $("#divConfirmDialog").html(alert);
            $("#divConfirmDialog").dialog({
                closeOnEscape: false,
                beforeclose: function (event, ui) { return false; },
                dialogClass: "noclose",
                height: 170,
                width: 450,
                title: "Block Out Time",
                buttons: {
                    'Continue': function () {
                        replaceScriptTags();
                        __doPostBack("blockSelectedAppts");
                        $(this).dialog('close');
                    },
                    'Cancel': function () {
                        $("#grdScheduledAppts INPUT[type='checkbox']").each(function () {                            
                                $(this).removeAttr('checked');                            
                        });
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        });
    }

    function uploadClientLogo() {
        var myFrame = document.getElementById('iframeFileUpload');

        $(myFrame).focus();
        $(myFrame).contents().find("#uploadLogoFile").click();
        var value = $(myFrame).contents().find("#uploadLogoFile").val();
        if (value != "" && value != undefined) {
            var file_ext = value.substring(value.lastIndexOf('.') + 1, value.length);
            if (file_ext.toUpperCase() != "JPG" && file_ext.toUpperCase() != "PNG" && file_ext.toUpperCase() != "GIF" && file_ext.toUpperCase() != "JPEG") {
                alert("Only image files with a '.png', '.gif', '.jpg' or '.jpeg' file extension are allowed.");
                return;
            }
        }
    }

    function displayClientLogo() {
        replaceScriptTags();
        $("#btnRefresh").click();
    }

    $(document).ready(function () {
        //disable enter key
        $(document).bind("keydown", function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                replaceScriptTags();
            }
        });

        var dateToday = new Date();
        $('#txtBannerColor').colorpicker({ showOn: 'button', history: false });
        $('#hfBannerColor').val($('#txtBannerColor').val());
        $("#txtBannerColor").on("change.color", function (event, color) {
            $('#hfBannerColor').val(color);
        });

        $('#txtBannerTextColor').colorpicker({ showOn: 'button', history: false });
        $('#hfBannerTextColor').val($('#txtBannerTextColor').val());
        $("#txtBannerTextColor").on("change.color", function (event, color) {
            $('#hfBannerTextColor').val(color);
        });

        $('#txtButtonColor').colorpicker({ showOn: 'button', history: false });
        $('#hfButtonColor').val($('#txtButtonColor').val());
        $("#txtButtonColor").on("change.color", function (event, color) {
            $('#hfButtonColor').val(color);
        });

        $('#txtButtonTextColor').colorpicker({ showOn: 'button', history: false });
        $('#hfButtonTextColor').val($('#txtButtonTextColor').val());
        $("#txtButtonTextColor").on("change.color", function (event, color) {
            $('#hfButtonTextColor').val(color);
        });

        $("#txtDeActivationDate").datepicker({
            showOn: "button",
            buttonImage: "../images/btn_calendar.gif",
            buttonImageOnly: true,
            minDate: dateToday,
            onSelect: function (dateText) {
                $("#hfDeactivationDate").val(dateText);
            }
        });

        $("#txtDeActivationDate").val($("#hfDeactivationDate").val());

        var value;
        value = $('#txtSchedulerLPheaderText').val();
        if (value != undefined)
            $('#txtSchedulerLPheaderText').val(value.replace(/&lt;/g, "<").replace(/&gt;/g, ">"));
        value = $('#txtSchedulerLPbodyText').val();
        if (value != undefined)
            $('#txtSchedulerLPbodyText').val(value.replace(/&lt;/g, "<").replace(/&gt;/g, ">"));
        value = $('#txtSchedulerLPfootertext').val();
        if (value != undefined)
            $('#txtSchedulerLPfootertext').val(value.replace(/&lt;/g, "<").replace(/&gt;/g, ">"));

        var chk_clinics = $("input[id$='chkSelectAllClinic']");
        if (chk_clinics != undefined) {
            if ($("#grdCorporateClinics INPUT[id*='chkScheduleClinic']").length == $("#grdCorporateClinics INPUT[id*='chkScheduleClinic']:disabled").length) {
                chk_clinics.attr('disabled', 'disabled')
            }
        }

        //Check all/Uncheck all client clinics
        chk_clinics.click(function () {
            $("#grdCorporateClinics INPUT[type='checkbox']").each(function () {
                if (!$(this)[0].disabled) {
                    $(this).attr('checked', chk_clinics.is(':checked'));
                }
            });

            //$("#grdCorporateClinics INPUT[type='checkbox']").attr('checked', chk_clinics.is(':checked'));
        });

        $("#grdCorporateClinics INPUT[type='checkbox']").click(
        function (e) {
            if ($("#grdCorporateClinics INPUT[id*='chkScheduleClinic']").length == $("#grdCorporateClinics INPUT[id*='chkScheduleClinic']:checked").length) {
                chk_clinics.attr('checked', true);
            }
            else {
                if (!$(this)[0].checked) {
                    chk_clinics.attr('checked', false);
                }
            }
        });

        //Check all/Uncheck all scheduled appointments
        var chk_appt_clinics = $("input[id$='chkAllScheduledAppts']");
        if (chk_appt_clinics != undefined) {
            if ($("#grdScheduledAppts INPUT[id*='chkScheduledAppt']").length == $("#grdScheduledAppts INPUT[id*='chkScheduledAppt']:disabled").length) {
                chk_appt_clinics.attr('disabled', 'disabled')
            }
        }

        chk_appt_clinics.click(function () {
            $("#grdScheduledAppts INPUT[type='checkbox']").each(function () {
                if (!$(this)[0].disabled) {
                    $(this).attr('checked', chk_appt_clinics.is(':checked'));
                }
            });

            //$("#grdCorporateClinics INPUT[type='checkbox']").attr('checked', chk_clinics.is(':checked'));
        });

        $("#grdScheduledAppts INPUT[type='checkbox']").click(
        function (e) {
            if ($("#grdScheduledAppts INPUT[id*='chkScheduledAppt']").length == $("#grdScheduledAppts INPUT[id*='chkScheduledAppt']:checked").length) {
                chk_appt_clinics.attr('checked', true);
            }
            else {
                if (!$(this)[0].checked) {
                    chk_appt_clinics.attr('checked', false);
                }
            }
        });

        $(".ui-datepicker-trigger").css("margin-bottom", "-6px");
        $(".ui-datepicker-trigger").css("padding-left", "5px");

        $('.grdScheduledApptsPagerStyle a').click(function () {
            replaceScriptTags();
        });

        $('.grdCorporateClinicsPagerStyle a').click(function () {
            replaceScriptTags();
        });

        $('.gridHeaderStyle a').click(function () {
            replaceScriptTags();
        });

        $("#cblRequiredFields INPUT[type='checkbox']").each(function () {
            if ($(this)[0].checked) {
                var chk_visible_field = $("input[id$='" + $(this)[0].id.replace("cblRequiredFields", "cblVisibleFields") + "']");
                if (chk_visible_field != undefined) {
                    chk_visible_field.attr("disabled", true);
                    chk_visible_field.attr("checked", true);
                }
            }
        });
    });

    function saveRegVisibleFields() {
        var visible_fields = "";
        $("#cblVisibleFields INPUT[type='checkbox']").each(function () {
            if ($(this)[0].checked) {
                visible_fields = visible_fields + "1,";
            }
            else
                visible_fields = visible_fields + "0,";
        });

        $("#hfRegVisibleFields").val(visible_fields.replace('undefined', ''));
    }

    function setRegFieldsVisibility() {        
        $("#cblRequiredFields INPUT[type='checkbox']").each(function () {
            var chk_visible_field = $("input[id$='" + $(this)[0].id.replace("cblRequiredFields", "cblVisibleFields") + "']");
            if ($(this)[0].checked) {
                chk_visible_field.attr("checked", $(this)[0].checked);
                chk_visible_field.attr("disabled", true);
            }
            else {
                chk_visible_field.removeAttr("disabled");                
            }
        });
    }
</script>
<style type="text/css">
    .ui-dialog-titlebar-close {
        visibility: hidden;
    }

    .ui-widget
    {
        font-size:11px;
    }
        
    .ui-widget-header
    {
        border-bottom:0px none #FFFFFF;           
    }
        
    .ui-tabs .ui-tabs-panel
    {
        padding:0px;           
    }
        
    .ui-state-active, .ui-state-active, .ui-widget-header .ui-state-active        
    {            
        background:url("../images/tab_gradient.jpg") repeat-x scroll 50% 50%;
	    background-repeat: repeat-x;
	    background-position:bottom; 
    }
    .ui-tabs .ui-tabs-nav li a, .ui-tabs.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-selected a {
	    cursor: pointer;
	    text-align: left;
    }
    .ui-tabs .ui-tabs-nav li a 
    {
	    padding-top:5px;
	    padding-left:15px;		    
    }    
    .gridStyles
    {
        border-bottom-width:1px;
        border-bottom-style:solid;
        border-bottom-color:#d6d6d6;
    }
    .gridHeaderStyle
    {
        border-bottom-width:2px;
        border-bottom-style:solid;
        border-bottom-color:#646464;
    }
    .closeButton
    {
        font-size:13px;
        font-weight:bold;
        color:#5eafda;
    }
    .grdScheduledApptsPagerStyle
    {
        color:#060606;
        font-size:11px;        
    }
    .grdCorporateClinicsPagerStyle
    {
        color:#060606;
        font-size:11px;        
    }
    
    .formReqFields
    {
        text-align:center;
        padding-left:10px;
    }
    
    .wrapword
    { 
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */        
        white-space: -pre-wrap;      /* Opera 4-6 */ 
        white-space: -o-pre-wrap;    /* Opera 7 */ 
        word-wrap: break-word;       /* Internet Explorer 5.5+ */        
        -ms-word-break: break-all;
        word-break: break-all;
        text-overflow: break-all;
    }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient" onload="javascript:setTab();">
    <form id="form1" runat="server" enctype="multipart/form-data" method="post">
    <asp:HiddenField ID="hfNavigatePage" runat="server" />
    <asp:HiddenField ID="hfDeactivationDate" runat="server" />
    <asp:HiddenField ID="hfRegVisibleFields" runat="server" />
    <asp:Button ID="btnRefresh" runat="server" OnClick="btnRefresh_Click" style="display:none;"  />
    <div>
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:12px 0px 10px 16px;"><img id="imgLogo" runat="server" src="~/images/wags_logo.png" width="225" height="52" /></td>
            <td class="formFields2" align="right" valign="top" bgcolor="#FFFFFF" style="padding-left:8px; padding-right:8px; padding-bottom:8px; padding-top:8px;">
                <asp:LinkButton ID="lnkBtnClose" runat="server" Width="144px" CssClass="closeButton" Text="Close" CommandArgument="Close" OnCommand="doProcess_Click" OnClientClick="javascript:replaceScriptTags();"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="left" valign="baseline" class="banner" colspan="2">Corporate Scheduler Site Setup</td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#FFFFFF">
            <table width="97%" align="center" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" valign="top" style="padding-top:10px; padding-bottom:10px;">
                    <table border="0">
                        <tr><td class="wagsAddress">Corporate Client:</td>
                          <td class="formFields2">
                            <asp:DropDownList ID="ddlCorporateClients" runat="server" Width="200" AutoPostBack="true" onselectedindexchanged="ddlCorporateClients_SelectedIndexChanged" onchange="javascript:replaceScriptTags();" ></asp:DropDownList>
                            <asp:TextBox ID="txtCorporateClient" runat="server" Visible="false"></asp:TextBox>
                          </td>
                        </tr>
                    </table>
                </td>                                       
              </tr>
              <tr>
                <td>
                    <div id="tabs">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:3px; background-color:White;" >
                        <tr>
                          <td>
                            <ul style="font-size: 70%; ">
                            <li class="profileTabs" style="width: 150px; height: 41px; vertical-align:middle; text-align:center;"><a id="scheduleClinic" href="#idClinics" runat="server" onclick="setSelectedTab('idClinics');"><span style="font-weight:bold; color:#060606; font-size:13px;">Scheduled Clinics</span></a></li>
                            <li class="profileTabs" style="width: 150px; height: 41px; vertical-align:middle; text-align:center;"><a id="logoStyle" href="#idLogoStyle" runat="server" onclick="setSelectedTab('idLogoStyle');"><span style="font-weight:bold; color:#060606; font-size:13px;">Logo & Styles</span></a></li>
                            <li class="profileTabs" style="width: 150px; height: 41px; vertical-align:middle; text-align:center;"><a id="landingPage" href="#idLandingPage" runat="server" onclick="setSelectedTab('idLandingPage');"><span style="font-weight:bold; color:#060606; font-size:13px;">Landing Page Text</span></a></li>
                            <li class="profileTabs" style="width: 150px; height: 41px; vertical-align:middle; text-align:center;"><a id="siteStatus" href="#idFieldsAndStatus" runat="server" onclick="setSelectedTab('idFieldsAndStatus');"><span style="font-weight:bold; color:#060606; font-size:13px;">Form Fields &<br /> Site Status</span></a></li>
                            <li class="profileTabs" style="width: 150px; height: 41px; vertical-align:middle; text-align:center;"><a id="appointments" href="#idAppointments" runat="server" onclick="setSelectedTab('idAppointments');"><span style="font-weight:bold; color:#060606; font-size:13px;">Appointments</span></a></li>
                            </ul>
                          </td>
                        </tr>
                      </table>
                        <div id="idClinics" style="background-color:#eeeeee">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top:3px;" > 
                                <tr>
                                    <td align="left" valign="top" class="formFields" style="padding-top: 15px; padding-left:10px;">
                                        Check the clinics to be scheduled through the scheduler for the selected client and set the number of immunizers.</td>
                                </tr>
                                <tr>
                                  <td align="center" valign="top" class="formFields2" style="padding:10px;">
                                    <asp:GridView ID="grdCorporateClinics" runat="server" AutoGenerateColumns="False" CellPadding="7" CellSpacing="0" AllowPaging="true" PageSize="10" GridLines="None" Width="100%" AllowSorting="false" DataKeyNames="clinicPk"
                                     OnRowDataBound="grdCorporateClinics_OnRowDataBound" OnPageIndexChanging="grdCorporateClinics_OnPageIndexChanging" >
                                        <FooterStyle CssClass="footerGrey" />
                                        <HeaderStyle BackColor="#999999" ForeColor="White" Font-Size="12px" Font-Bold="true" Height="30px" CssClass="gridHeaderStyle" />
                                        <RowStyle Height="35px" BackColor="White" CssClass="gridStyles" />
                                        <PagerStyle CssClass="grdCorporateClinicsPagerStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hfIsStoreConfirmed" runat="server" Visible="false" Value='<%# Eval("isStoreConfirmed") %>' />
                                                    <asp:HiddenField ID="hfIsClinicCanceled" runat="server" Visible="false" Value='<%# Eval("isCanceled") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkSelectAllClinic" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkScheduleClinic" runat="server" Checked='<%# Eval("isScheduled").ToString().Equals("True") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Clinic" HeaderStyle-HorizontalAlign="Left" DataField="businessClinic" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="175px" />
                                            <asp:BoundField HeaderText="Location" HeaderStyle-HorizontalAlign="Left" DataField="clinicAddress" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px" />
                                            <asp:BoundField HeaderText="Date" HeaderStyle-HorizontalAlign="Center" DataField="clinicDate" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Start Time" HeaderStyle-HorizontalAlign="Center" DataField="clinicStartTime" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="75px" />
                                            <asp:BoundField HeaderText="End Time" HeaderStyle-HorizontalAlign="Center" DataField="clinicEndTime" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="65px" />
                                            <asp:BoundField HeaderText="Store" HeaderStyle-HorizontalAlign="Center" DataField="clinicStoreId" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Est Vol" HeaderStyle-HorizontalAlign="Center" DataField="naClinicEstimatedVolume" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="65px" />
                                            <asp:TemplateField HeaderText="Min Immunizers" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlImmunizers" runat="server" Width="45" CssClass="formFields" SelectedValue='<%# Eval("immunizers") %>' >
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
													</asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Max Immunizers" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlImmunizersMax" runat="server" Width="45" CssClass="formFields" >
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <center>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:White; text-align: center;" >
                                                    <tr style="height:35px;">
                                                        <td>
                                                            <b><asp:Label ID="lblNote" Text="No clinics found" CssClass="formFields" runat="server"></asp:Label></b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="LightBlue" />
                                    </asp:GridView>
                                  </td>
                                </tr>
                            </table>
                        </div>
                        <div id="idLogoStyle" style="background-color:#eeeeee">
                            <table width="100%" border="0" cellspacing="20" cellpadding="0"> 
                                <tr>
                                  <td align="left" valign="top">
                                    <table width="100%" border="0" cellpadding="2" id="rowAssignClinics" style="background-color:#eeeeee">
                                      <tr><td colspan="4" class="formFields">Masthead Logo:</td></tr>
                                      <tr>
                                        <td colspan="4" align="left" style="vertical-align:middle;" class="formFields2">
                                        <asp:RadioButtonList ID="rblClientlogo" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="formFields" 
                                        onselectedindexchanged="rblClientlogo_SelectedIndexChanged" AutoPostBack="true" onchange="javascript:replaceScriptTags();" onclick="javascript:replaceScriptTags();">
                                            <asp:ListItem Value="wagsLogo" Text="Default Walgreens Logo"></asp:ListItem>
                                            <asp:ListItem Value="clientLogo" Text="Client Logo"></asp:ListItem>
                                        </asp:RadioButtonList>
                                        </td>
                                      </tr>
                                      <tr style="vertical-align:middle; "><td align="left" style="vertical-align:middle; padding:10px;" colspan="4">
                                        <span style="color:White; "><a href="#" style="color:White;" onclick="javascript:uploadClientLogo();" class="wagsSchedButton" >Browse</a>
                                        <iframe id="iframeFileUpload" runat="server" name="iframeFileUpload" style="display:none" ></iframe>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label CssClass="formFields" ID="lblFileName" runat="server" Text=""></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Image ID="imgClientlogo" runat="server" ImageAlign="Middle" />
                                        </span>
                                      </td></tr>
                                      <tr><td align="left" style="vertical-align:middle; padding:2px;" colspan="4">&nbsp;</td></tr>
                                      <tr>
                                        <td align="left" valign="top" style="width: 105px;" class="formFields">Banner Color:</td>
                                        <td align="left" valign="top" style="width: 70px;"><asp:TextBox ID="txtBannerColor" runat="server" BackColor="White" Enabled="false" Width="65"></asp:TextBox><asp:HiddenField ID="hfBannerColor" runat="server" /></td>
                                        <td align="left" valign="top" style="width: 10px;">&nbsp;</td>
                                        <td align="left" valign="top">
                                            <asp:LinkButton ID="lnkResetDefaultColors" runat="server" ForeColor="White" Width="144px" CssClass="wagsSchedButtonGray" Text="Reset to Default Colors" CommandArgument="Reset Colors" OnCommand="doReset_Click" OnClientClick="javascript:replaceScriptTags();"></asp:LinkButton>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top" style="width: 105px; padding-bottom:17px;" class="formFields">Banner Text Color:</td>
                                        <td align="left" valign="top" style="width: 70px;"><asp:TextBox ID="txtBannerTextColor" runat="server" BackColor="White" Enabled="false" Width="65"></asp:TextBox><asp:HiddenField ID="hfBannerTextColor" runat="server" /></td>
                                        <td align="left" valign="top" style="width: 10px;">&nbsp;</td>
                                        <td align="left" valign="top">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top" style="width: 105px; padding-bottom:17px;" class="formFields">Button Color:</td>
                                        <td align="left" valign="top" style="width: 70px;"><asp:TextBox ID="txtButtonColor" runat="server" BackColor="White" Enabled="false" Width="65"></asp:TextBox><asp:HiddenField ID="hfButtonColor" runat="server" /></td>
                                        <td align="left" valign="top" style="width: 10px;">&nbsp;</td>
                                        <td align="left" valign="top" >&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top" style="width: 105px; padding-bottom:17px;" class="formFields">Button Text Color:</td>
                                        <td align="left" valign="top" style="width: 70px;"><asp:TextBox ID="txtButtonTextColor" runat="server" BackColor="White" Enabled="false" Width="65"></asp:TextBox><asp:HiddenField ID="hfButtonTextColor" runat="server" /></td>
                                        <td align="left" valign="top" style="width: 10px;">&nbsp;</td>
                                        <td align="left" valign="top">&nbsp;</td>
                                      </tr>
                                </table>
                                  </td>
                                </tr>
                            </table>
                        </div>
                        <div id="idLandingPage" style="background-color:#eeeeee">
                            <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                <tr>
                                    <td style="height:5px;" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" width="50%" class="formFields">&nbsp;&nbsp;Landing Page Header Text</td>
                                    <td align="right" valign="top" width="43%" >
                                        <span style="color:White;"><asp:LinkButton ID="lnkBtnResetHeaderText" runat="server" Width="144px" ForeColor="White" CssClass="wagsSchedButtonGray" Text="Reset to Default Text" CommandArgument="Reset Header Text" OnCommand="doReset_Click" OnClientClick="javascript:replaceScriptTags();"></asp:LinkButton></span>
                                    </td>
                                    <td width="7%"></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" colspan="2">&nbsp;&nbsp;<asp:TextBox Height="25" ID="txtSchedulerLPheaderText" runat="server" Width="875"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height:5px;" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" width="50%" class="formFields;">&nbsp;&nbsp;Landing Page Body Text</td>
                                    <td align="right" valign="top" width="43%">
                                        <span style="color:#fff;"><asp:LinkButton ID="lnkBtnResetBodyText" runat="server" Width="144px" ForeColor="White" CssClass="wagsSchedButtonGray" Text="Reset to Default Text" CommandArgument="Reset Body Text" OnCommand="doReset_Click" OnClientClick="javascript:replaceScriptTags();"></asp:LinkButton></span>
                                    </td>
                                    <td width="7%"></td>
                                </tr>
                                <tr>                                    
                                    <td align="left" valign="top" colspan="2">&nbsp;&nbsp;<asp:TextBox ID="txtSchedulerLPbodyText" runat="server" Width="875" TextMode="MultiLine" Rows="10" Columns="885"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height:5px;" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" width="50%" class="formFields">&nbsp;&nbsp;Landing Page Footer Text</td>
                                    <td align="right" valign="top" width="43%">
                                        <span style="color:White;"><asp:LinkButton ID="lnkBtnResetFooterText" runat="server" Width="144px" ForeColor="White" CssClass="wagsSchedButtonGray" Text="Reset to Default Text" CommandArgument="Reset Footer Text" OnCommand="doReset_Click" OnClientClick="javascript:replaceScriptTags();"></asp:LinkButton></span>
                                    </td>
                                    <td width="7%"></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" colspan="2">&nbsp;&nbsp;<asp:TextBox ID="txtSchedulerLPfootertext" runat="server" Width="875" TextMode="MultiLine" Rows="8" Columns="885" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height:15px;" colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                        <div id="idFieldsAndStatus" style="background-color:#eeeeee">
                            <table width="100%" border="0" cellspacing="10" cellpadding="0" style="padding:10px; table-layout: fixed;">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                            <tr>
                                                <td align="left" valign="bottom" style="width:100px;" class="formFields">Site Status:</td>
                                                <td align="left" valign="bottom" class="formFields">
                                                    <asp:DropDownList ID="ddlSiteStatus" runat="server" >
                                                    <asp:ListItem Text ="Active" Value="Active"></asp:ListItem>
                                                    <asp:ListItem Text ="Inactive" Value="Inactive"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="bottom" style="width:100px; padding-top:10px; " class="formFields" >Deactivation Date:</td>
                                                <td align="left" valign="bottom" style="padding-top:10px; " class="formFields">
                                                    <asp:TextBox ID="txtDeActivationDate" runat="server" ReadOnly="true" Width="80px" ></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td class="formFields" >Individual clinic scheduling will close at midnight prior to the clinic date.</td></tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td align="left" valign="bottom" class="formFields">Form Fields:</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" class="formFields" style="padding-left:10px;">
                                        <table cellspacing="0" cellpadding="3" border="0">
                                            <tr>
                                                <td><div style="text-align:center; padding-right:10px;">Required</div>
                                                    <asp:CheckBoxList ID="cblRequiredFields" runat="server" CellSpacing="2" CssClass="formReqFields" onclick="javascript:setRegFieldsVisibility();">
                                                    <asp:ListItem Text="" Value="firstName"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="middleName"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="lastName" ></asp:ListItem>
                                                    <asp:ListItem Text="" Value="employeeId" ></asp:ListItem>
                                                    <asp:ListItem Text="" Value="email" Enabled="false" ></asp:ListItem>
                                                    <asp:ListItem Text="" Value="phone"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="gender"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="dob"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="address1"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="address2"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="city"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="state"></asp:ListItem>
                                                    <asp:ListItem Text="" Value="zip"></asp:ListItem>
                                                    </asp:CheckBoxList>
                                                </td>
                                                <td>
                                                    <div>Visible</div>
                                                    <asp:CheckBoxList ID="cblVisibleFields" runat="server" CellSpacing="2" >
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;First Name" Value="firstName"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Middle Name" Value="middleName"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last Name" Value="lastName"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Employee ID" Value="employeeId"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email" Value="email" Enabled="false"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone" Value="phone"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gender" Value="gender"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth" Value="dob"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address 1" Value="address1"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address 2" Value="address2"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City" Value="city"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State" Value="state"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zip Code" Value="zip"></asp:ListItem>
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" class="formFields">Encrypted Immunization Clinic Appointment Scheduling site Link (Provide this link for employees to access the client scheduler):</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" >
                                        <asp:HyperLink ID="hlSchedulerClientKey" runat="server" CssClass="wrapword" Target="_blank" ForeColor="Blue"></asp:HyperLink>
                                        <asp:HiddenField ID="hfEncryptedLongLink" runat="server" />
                                        <asp:HiddenField ID="hfEncryptedClientLink" runat="server" />                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" class="formFields">Encrypted Immunization Clinic Appointment Tracking site Link (Provide this link to Admins of the client scheduler):</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom" class="wrapword" ><asp:HyperLink ID="hlEncryptedApptTrackingLink" runat="server" CssClass="wrapword" Target="_blank" ForeColor="Blue"></asp:HyperLink></td>
                                </tr>
                            </table>
                        </div>
                        <div id="idAppointments" style="background-color:#eeeeee">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top:3px;" >
                                <tr>
                                    <td align="left" valign="middle" class="formFields" style="padding-top: 10px; padding-left:10px; font-weight:bold"><asp:Label ID="lblTotalApptScheduledText" runat="server"></asp:Label><asp:Label ID="lblTotalApptSchedule" CssClass="formFields2" runat="server"></asp:Label> </td>
                                    <td align="right" valign="middle" class="formFields" style="padding-top: 10px; padding-left:10px; padding-right:5px;">
                                        <asp:TextBox ID="txtSearchAppointments" runat="server"></asp:TextBox>&nbsp;
                                        <span style="padding-top: 0px; text-align:center;"><asp:LinkButton ID="lnkSearchScheduledAppt" runat="server" Width="65px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Search" CommandArgument="Search" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkShowAllScheduledAppt" runat="server" Width="65px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Show All" CommandArgument="Show All" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkShowUnScheduledAppts" runat="server" Width="130px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Show Unscheduled" CommandArgument="Show Unscheduled" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton><asp:LinkButton ID="lnkHideUnScheduledAppts" runat="server" Width="130px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="Hide Unscheduled" CommandArgument="Hide Unscheduled" OnCommand="doProcess_Click" OnClientClick="javascript:validateSearchText();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkViewScheduledApptReport" runat="server" Width="85px" ForeColor="White" CssClass="wagsSchedSmallButton" Text="View Report" CommandArgument="View Report" OnCommand="doProcess_Click" OnClientClick="javascript:replaceScriptTags();"></asp:LinkButton>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                  <td colspan="2" align="center" valign="top" class="formFields2" style="padding:10px;">
                                    <asp:GridView ID="grdScheduledAppts" runat="server" AutoGenerateColumns="False" CellPadding="7" CellSpacing="0" AllowPaging="true" PageSize="10" GridLines="None" Width="100%" 
                                    AllowSorting="true" DataKeyNames="apptPk, apptTimePk, clinicPk" OnRowDataBound="grdScheduledAppts_OnRowDataBound" OnPageIndexChanging ="grdScheduledAppts_PageIndexChanging" OnSorting="grdScheduledAppts_sorting">
                                        <FooterStyle CssClass="footerGrey" />
                                        <HeaderStyle BackColor="#999999" ForeColor="White" Font-Size="12px" Font-Bold="true" Height="30px" CssClass="gridHeaderStyle" />
                                        <RowStyle Height="35px" BackColor="White" CssClass="gridStyles" />
                                        <PagerStyle CssClass="grdScheduledApptsPagerStyle" />                                        
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAllScheduledAppts" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkScheduledAppt" runat="server" />
                                                    <asp:Label ID="lblIsBlocked" runat="server" Text='<%# Bind("isBlocked") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblApptEmail" runat="server" Text='<%# Bind("apptEeEmail") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="apptPk" Visible="false" />
                                            <asp:BoundField DataField="apptTimePk" Visible="false" />
                                            <asp:BoundField DataField="clinicPk" Visible="false" />                                            
                                            <asp:BoundField HeaderText="Clinic" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="170px" DataField="businessClinic" SortExpression="businessClinic" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px" />                                            
                                            <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="clinicAddress" ItemStyle-Width="150px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblclinicAddress" runat="server" Text='<%# Bind("clinicAddress") %>' ></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Appt Date" HeaderStyle-HorizontalAlign="Center" DataField="apptDate" SortExpression="apptDate" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Appt Time" HeaderStyle-HorizontalAlign="Center" DataField="apptAvlTimes" SortExpression="apptTimeSortId" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Time Slot" HeaderStyle-HorizontalAlign="Center" DataField="apptTimeSlot" SortExpression="apptTimeSlot" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" />
                                            <asp:BoundField HeaderText="Name" HeaderStyle-HorizontalAlign="Center" DataField="PatientName" SortExpression="PatientName" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Employee ID" HeaderStyle-HorizontalAlign="Center" DataField="employeeId" SortExpression="employeeId" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Confirmation ID" HeaderStyle-HorizontalAlign="Center" DataField="confirmationId" SortExpression="confirmationId" ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <center>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:White; text-align: center;" >
                                                    <tr style="height:35px;">
                                                        <td>
                                                            <b><asp:Label ID="lblNote" Text="No records found" CssClass="formFields" runat="server"></asp:Label></b>
                                                        </td>
                                                    </tr>
                                                </table>                                                
                                            </center>                                            
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="LightBlue" />
                                    </asp:GridView>
                                  </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
              </tr>
              <tr>
                <td align="left" valign="top" >
                    <br />
                    <span style="padding-top: 0px; text-align:center;"><asp:LinkButton ID="lnkBlockCheckedAppts" runat="server" Width="260px" CssClass="wagsSchedBigButton" Text="Block Out Checked Appointments" CommandArgument="Block" OnCommand="doProcess_Click" OnClientClick="javascript:return checkSelected('block'); replaceScriptTags();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkUnBlockCheckedAppts" runat="server" Width="255px" CssClass="wagsSchedBigButton" Text="Un-Block Checked Appointments" CommandArgument="UnBlock" OnCommand="doProcess_Click" OnClientClick="javascript:return checkSelected('unblock'); javascript:replaceScriptTags();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkPreviewScheduler" runat="server" Width="105px" CssClass="wagsSchedBigButton" Text="Preview" CommandArgument="Preview" OnCommand="doProcess_Click" OnClientClick="javascript:return checkSelection();"></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lnkSaveScheduler" runat="server" Width="105px" CssClass="wagsSchedBigButton" Text="Save" CommandArgument="Save" OnCommand="doProcess_Click" OnClientClick="javascript:return checkSelection(); "></asp:LinkButton>
                    </span>
                    <br />
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 13px; text-align:center; font-weight:bold;"></div>
          </td>
        </tr>        
    </table>
    </div>
    </form>
</body>
</html>
