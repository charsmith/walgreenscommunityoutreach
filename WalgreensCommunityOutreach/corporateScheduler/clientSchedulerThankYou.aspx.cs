﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;
using TdApplicationLib;
using System.Xml;
using System.Data;
using Microsoft.Reporting.WebForms;

public partial class corporateScheduler_clientSchedulerThankYou : System.Web.UI.Page
{
    #region -------------------- PAGE EVENTS --------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.commonAppSession.SelectedStoreSession != null)
        {
            if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.schedulerDesignXML))
            {
                if (!IsPostBack)
                {
                    this.clientSchedulerXML.LoadXml(this.commonAppSession.SelectedStoreSession.schedulerDesignXML);

                    this.bannerPreview.Style.Add("background-color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value);
                    this.bannerPreview.Style.Add("color", this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerTextColor"].Value);
                    if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value))
                        this.clientLogo.Visible = false;
                    else
                        this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=" + this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value + "&extraQS=" + DateTime.Now.Second;

                    this.commonAppSession.SelectedStoreSession = null;
                }
            }
            else
            {
                this.bannerPreview.Style.Add("background-color", "#3096D8");
                this.bannerPreview.Style.Add("color", "#FFFFFF");
                this.clientLogo.ImageUrl = "~/controls/displayImage.ashx?image=wags_logo.png&extraQS=" + DateTime.Now.Second;
            }
        }
        else
            Response.Redirect("~/ErrorPage.aspx", true);
    }
    #endregion

    #region --------- PRIVATE VARIABLES ------------------
    private AppCommonSession commonAppSession = null;
    private XmlDocument clientSchedulerXML;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.clientSchedulerXML = new XmlDocument();
    }
    #endregion
}