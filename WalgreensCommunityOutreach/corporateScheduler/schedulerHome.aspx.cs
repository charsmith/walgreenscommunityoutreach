﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using TdWalgreens;
using TdApplicationLib;
using System.Web.Services;
using System.Data;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml.Linq;
using System.Configuration;
using System.Net.Configuration;
using System.Web.Configuration;
using tdEmailLib;

public partial class corporateScheduler_schedulerHome : Page
{
    #region -------------------- PAGE EVENTS --------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindCorporateClients();
            this.lnkHideUnScheduledAppts.Visible = false;
            this.lblTotalApptScheduledText.Text = @"Total Scheduled: ";
        }
        else
        {
            this.returnValue = 0;
            string event_args = Request["__EVENTTARGET"];
            if (event_args.ToLower() == "closeschedulersetup")
            {
                bool save_scheduler = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));

                if (this.hfNavigatePage.Value == "reports/reportScheduledAppointment.aspx" && this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text) != null)
                    this.commonAppSession.SelectedStoreSession.schedulerDesignPk = Convert.ToInt32(this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text).Value);

                if (save_scheduler)
                {
                    string error_message = string.Empty;
                    this.returnValue = this.doSave(out error_message, 0);
                    if (this.returnValue == -4)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Please upload a client logo.')", true);
                    else if (this.returnValue == -2)
                    {
                        error_message = error_message.TrimStart(',').Replace(",", ", ");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showUnscheduleClinicWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "unscheduleClinic"), error_message) + "');", true);
                    }
                    else if (this.returnValue == 0)
                    {
                        Session.Remove("clientDetails");
                        if (!string.IsNullOrEmpty(this.hfNavigatePage.Value))
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "alert('" + GetGlobalResourceObject("errorMessages", "clientSchedulerSaved") + "'); window.location.href = '../" + this.hfNavigatePage.Value + "';", true);
                        else
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "bindSchedulerDetails('" + GetGlobalResourceObject("errorMessages", "clientSchedulerSaved") + "');", true);
                    }
                }
                else
                {
                    Session.Remove("clientDetails");
                    if (!string.IsNullOrEmpty(this.hfNavigatePage.Value))
                        Response.Redirect("../" + this.hfNavigatePage.Value);
                    else
                        this.bindClientSchedulerClinics(this.ddlCorporateClients.SelectedItem.Text);
                }
            }
            else if (event_args.ToLower() == "blockselectedappts")
            {
                this.clientSchedulerXML = this.prepareClinicAppointmentsXML(0);
                this.returnValue = this.dbOperation.blockUnblockScheduledAppts(1, this.clientSchedulerXML);
                if (this.returnValue == 0)
                {
                    //Send Reschedule Your Appointment email notification for blocked appointments to the patients
                    if (!this.lnkHideUnScheduledAppts.Visible)
                    {
                        this.sendBlockedAppointmentRescheduleEmail();
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "scheduledApptsBlocked") + "');", true);
                    }
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "unScheduledApptsBlocked") + "');", true);
                }
            }
            else if (event_args.ToLower() == "rescheduleappts")
            {
                bool reschedule_appts = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                if (reschedule_appts)
                {
                    string error_message = string.Empty;
                    this.returnValue = this.doSave(out error_message, 1);

                    //Send reschedule appointment email for unscheduled clinics
                    if (this.returnValue == -3)
                        this.sendRescheduleAppointmentEmail();

                    this.bindClientSchedulerClinics(this.ddlCorporateClients.SelectedItem.Text);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "showAlertMessage('" + GetGlobalResourceObject("errorMessages", "clientSchedulerSaved") + "');", true);
                }
                else
                {
                    //bind corporate client clinics
                    this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
                    if (this.corporateScheduler.clientClinics != null && this.corporateScheduler.clientClinics.Rows.Count != 0)
                    {
                        this.grdCorporateClinics.DataSource = this.corporateScheduler.clientClinics;
                        this.grdCorporateClinics.DataBind();
                    }
                }
            }
            else if (event_args.ToLower() == "reloadappointments")
            {
                this.getScheduledAppointments();
            }
            else if (event_args.ToLower() == "reloadscheduler")
            {
                this.bindClientSchedulerClinics(this.ddlCorporateClients.SelectedItem.Text);
            }
        }
    }

    protected void ddlCorporateClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.ddlCorporateClients.SelectedValue))
        {
            if (!string.IsNullOrEmpty(this.txtCorporateClient.Text))
            {
                this.updateClientDetails();
                if (this.checkUnsavedChanges())
                {
                    this.hfNavigatePage.Value = string.Empty;
                    return;
                }
            }

            this.hfDeactivationDate.Value = string.Empty;
            this.disabledButtons(false);
            this.bindClientSchedulerClinics(this.ddlCorporateClients.SelectedItem.Text);
        }
    }

    protected void rblClientlogo_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.lblFileName.Text = "";
        this.imgClientlogo.ImageUrl = "";

        this.updateClientDetails();

        this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
        this.clientSchedulerXML.LoadXml(this.corporateScheduler.clinicSchedulerXML);

        if (rblClientlogo.SelectedValue == "wagsLogo")
        {
            XmlDocument default_scheduler_xml = new XmlDocument();
            default_scheduler_xml.Load(HttpContext.Current.Server.MapPath("defaultSetupXML.xml"));
            this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["mastheadLogo"].Value = default_scheduler_xml.SelectSingleNode(".//logoAndStyles").Attributes["mastheadLogo"].Value;
            this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value = default_scheduler_xml.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value;
        }
        else
        {
            string logo_file_name = string.Empty;
            String[] valid_extensions = { ".gif", ".png", ".jpeg", ".jpg", ".GIF", ".PNG", ".JPEG", ".JPG" };
            logo_file_name = Regex.Replace(this.ddlCorporateClients.SelectedItem.Text, @"[^0-9A-Za-z\s]+", "").Replace(" ", "_");
            this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["mastheadLogo"].Value = this.rblClientlogo.SelectedValue;
            this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value = "";

            //Check if file exists
            for (int i = 0; i < valid_extensions.Length; i++)
            {
                if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + logo_file_name + valid_extensions[i]))
                    this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value = logo_file_name + valid_extensions[i];
            }
        }
        this.corporateScheduler.clinicSchedulerXML = this.clientSchedulerXML.OuterXml;
        Session["clientDetails"] = this.corporateScheduler;
        this.displayClientLogo(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value, this.rblClientlogo.SelectedValue);
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
        this.clientSchedulerXML.LoadXml(this.corporateScheduler.clinicSchedulerXML);
        this.displayClientLogo(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value, this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["mastheadLogo"].Value);
    }

    protected void grdCorporateClinics_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk_clinic = (CheckBox)e.Row.FindControl("chkScheduleClinic");
            DropDownList ddl_immunizers = (DropDownList)e.Row.FindControl("ddlImmunizers");
            DropDownList ddl_max_immunizers = (DropDownList)e.Row.FindControl("ddlImmunizersMax");
            HiddenField hf_clinic_confirmed = (HiddenField)e.Row.FindControl("hfIsStoreConfirmed");
            HiddenField hf_clinic_canceled = (HiddenField)e.Row.FindControl("hfIsClinicCanceled");
            //Set Max immunizers to default value when NULL
            ddl_max_immunizers.SelectedValue = !string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "maxImmunizers").ToString()) ? DataBinder.Eval(e.Row.DataItem, "maxImmunizers").ToString() : "3";
            if (Convert.ToBoolean(hf_clinic_canceled.Value) || !Convert.ToBoolean(hf_clinic_confirmed.Value) || string.IsNullOrEmpty(e.Row.Cells[3].Text.Replace("&nbsp;", "")) || string.IsNullOrEmpty(e.Row.Cells[4].Text.Replace("&nbsp;", "")) || (!string.IsNullOrEmpty(e.Row.Cells[4].Text.Replace("&nbsp;", "")) && Convert.ToDateTime(e.Row.Cells[4].Text) < DateTime.Now) || string.IsNullOrEmpty(e.Row.Cells[5].Text.Replace("&nbsp;", "")) || string.IsNullOrEmpty(e.Row.Cells[6].Text.Replace("&nbsp;", "")))
            {
                ddl_immunizers.Enabled = false;
                ddl_max_immunizers.Enabled = false;
                chk_clinic.Enabled = false;
            }
        }
    }

    protected void grdCorporateClinics_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (Session["clientDetails"] != null)
        {
            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
            if (this.corporateScheduler.clientClinics != null && this.corporateScheduler.clientClinics.Rows.Count != 0)
            {
                this.grdCorporateClinics.PageIndex = e.NewPageIndex;
                this.grdCorporateClinics.DataSource = this.corporateScheduler.clientClinics;
                this.grdCorporateClinics.DataBind();
            }
        }
    }

    protected void grdScheduledAppts_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Convert.ToDateTime(e.Row.Cells[6].Text) < DateTime.Now)
            {
                CheckBox chk_appt = (CheckBox)e.Row.FindControl("chkScheduledAppt");
                chk_appt.Enabled = false;
            }

            if (Convert.ToInt32(((DataRowView)e.Row.DataItem)["isBlocked"]) != 0)
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#99999f");
            }
            Label lbl_clinic_address = (Label)e.Row.FindControl("lblclinicAddress");
            lbl_clinic_address.Text = lbl_clinic_address.Text.Replace("<br />", " ");
        }
    }

    protected void grdScheduledAppts_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (Session["clientDetails"] != null)
        {
            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];

            if (!string.IsNullOrEmpty(this.txtSearchAppointments.Text))
            {
                DataRow[] dr_appts = this.corporateScheduler.scheduledAppointments.Select("businessClinic LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR clinicAddress LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR PatientName LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR confirmationId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptAvlTimes LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptDate LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%'");
                if (dr_appts.Count() > 0)
                    this.dtScheduledAppts = dr_appts.CopyToDataTable();
                else
                    this.dtScheduledAppts.Clear();
            }
            else
                this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;

            this.grdScheduledAppts.PageIndex = e.NewPageIndex;
            this.bindScheduledAppointments();
        }
    }

    protected void grdScheduledAppts_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        if (Session["clientDetails"] != null)
        {
            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
            if (!string.IsNullOrEmpty(this.txtSearchAppointments.Text))
            {
                DataRow[] dr_appts = this.corporateScheduler.scheduledAppointments.Select("businessClinic LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR clinicAddress LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR PatientName LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR confirmationId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptAvlTimes LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptDate LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%'");
                if (dr_appts.Count() > 0)
                    this.dtScheduledAppts = dr_appts.CopyToDataTable();
                else
                    this.dtScheduledAppts.Clear();
            }
            else
                this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;

            this.bindScheduledAppointments();
        }
    }

    protected void doProcess_Click(object sender, CommandEventArgs e)
    {
        //Check if session exists and reload with last selected client details if not exists
        if (Session["clientDetails"] == null && e.CommandArgument.ToString() != "View Report" && e.CommandArgument.ToString() != "Close")
        {
            if (!string.IsNullOrEmpty(this.txtCorporateClient.Text))
                this.bindClientSchedulerClinics(this.txtCorporateClient.Text);

            return;
        }

        this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
        switch (e.CommandArgument.ToString())
        {
            case "Block":
                if (!this.lnkHideUnScheduledAppts.Visible)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showBlockApptsWarning('" + (string)GetGlobalResourceObject("errorMessages", "blockScheduledAppts") + "');", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "doBlockUnscheduledAppts();", true);
                break;
            case "UnBlock":
                this.returnValue = 0;
                this.returnValue = this.dbOperation.blockUnblockScheduledAppts(0, this.prepareClinicAppointmentsXML(1));
                if (this.returnValue == 0)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "scheduledApptsUnblocked") + "');", true);
                break;
            case "Preview":
                this.updateClientDetails();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.open('corporateSchedulerPreview.aspx', '_blank', 'width=850,height=650,scrollbars=yes');", true);
                break;
            case "Close":
                if (!string.IsNullOrEmpty(this.txtCorporateClient.Text))
                {
                    this.updateClientDetails();
                    if (this.checkUnsavedChanges())
                    {
                        this.hfNavigatePage.Value = "walgreensHome.aspx";
                        return;
                    }
                    else
                    {
                        Session.Remove("clientDetails");
                        Response.Redirect("~/walgreensHome.aspx");
                    }
                }
                else
                    Response.Redirect("~/walgreensHome.aspx");
                break;
            case "Save":
                this.updateClientDetails();
                string error_message = string.Empty;
                this.returnValue = this.doSave(out error_message, 0);
                if (this.returnValue == -2)
                {
                    error_message = error_message.TrimStart(',').Replace(",", ", ");
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showUnscheduleClinicWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "unscheduleClinic"), error_message) + "');", true);
                }
                else if (this.returnValue == -4)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "showAlertMessage('" + GetGlobalResourceObject("errorMessages", "uploadClientLogo") + "')", true);
                else if (this.returnValue == 0)
                {
                    this.bindClientSchedulerClinics(this.ddlCorporateClients.SelectedItem.Text);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "showAlertMessage('" + GetGlobalResourceObject("errorMessages", "clientSchedulerSaved") + "');", true);
                }
                break;
            case "Search":
                if (this.corporateScheduler.scheduledAppointments != null && this.corporateScheduler.scheduledAppointments.Rows.Count > 0)
                {
                    DataRow[] dr_appts = this.corporateScheduler.scheduledAppointments.Select("businessClinic LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR clinicAddress LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR PatientName LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR employeeId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR confirmationId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptAvlTimes LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptDate LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%'");
                    if (dr_appts.Count() > 0)
                        this.dtScheduledAppts = dr_appts.CopyToDataTable();
                    else
                        this.dtScheduledAppts.Clear();

                    this.grdScheduledAppts.PageIndex = 0;
                }
                this.bindScheduledAppointments();
                break;
            case "Show All":
                this.getScheduledAppointments();
                break;
            case "View Report":
                if (!string.IsNullOrEmpty(this.txtCorporateClient.Text))
                {
                    this.updateClientDetails();
                    if (this.checkUnsavedChanges())
                    {
                        this.hfNavigatePage.Value = "reports/reportScheduledAppointment.aspx";
                        return;
                    }
                    else
                    {
                        Session.Remove("clientDetails");
                        this.commonAppSession.SelectedStoreSession.schedulerDesignPk = Convert.ToInt32(this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text).Value);
                        Response.Redirect("~/reports/reportScheduledAppointment.aspx");
                    }
                }
                else
                    Response.Redirect("~/reports/reportScheduledAppointment.aspx");
                break;
            case "Show Unscheduled":
                this.lnkShowUnScheduledAppts.Visible = false;
                this.lnkHideUnScheduledAppts.Visible = true;
                this.getScheduledAppointments();
                break;
            case "Hide Unscheduled":
                this.lnkShowUnScheduledAppts.Visible = true;
                this.lnkHideUnScheduledAppts.Visible = false;
                this.getScheduledAppointments();
                break;
        }
    }

    protected void doReset_Click(object sender, CommandEventArgs e)
    {
        this.clientSchedulerXML.Load(HttpContext.Current.Server.MapPath("defaultSetupXML.xml"));
        switch (e.CommandArgument.ToString())
        {
            case "Reset Colors":
                //Logos & styles            
                this.txtBannerColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value;
                this.txtBannerTextColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerTextColor"].Value;
                this.txtButtonColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value;
                this.txtButtonTextColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value;
                break;
            case "Reset Header Text":
                //Scheduler landing page header text
                this.txtSchedulerLPheaderText.Text = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/headerText").InnerText.Replace("<br/>", Environment.NewLine).Replace("<", "&lt;").Replace(">", "&gt;");
                break;
            case "Reset Body Text":
                //Scheduler landing page body text
                this.txtSchedulerLPbodyText.Text = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/bodyText").InnerText.Replace("<br/>", Environment.NewLine).Replace("<", "&lt;").Replace(">", "&gt;");
                break;
            case "Reset Footer Text":
                //Scheduler landing page footer text
                this.txtSchedulerLPfootertext.Text = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/footerText").InnerText.Replace("<br/>", Environment.NewLine).Replace("<", "&lt;").Replace(">", "&gt;");
                break;
            default:
                break;
        }
    }
    #endregion

    #region ------------------ PRIVATE METHODS ------------------
    /// <summary>
    /// Binds corporate clients
    /// </summary>
    private void bindCorporateClients()
    {
        this.ddlCorporateClients.ClearSelection();
        this.ddlCorporateClients.DataSource = this.dbOperation.getCorporateClients;
        this.ddlCorporateClients.DataTextField = "businessName";
        this.ddlCorporateClients.DataValueField = "pk";
        this.ddlCorporateClients.DataBind();
        this.ddlCorporateClients.Items.Insert(0, new ListItem("-- Select Client --", ""));

        this.disabledButtons(true);

        this.grdCorporateClinics.DataSource = this.dtScheduledAppts;
        this.grdCorporateClinics.DataBind();

        this.grdScheduledAppts.DataSource = this.dtScheduledAppts;
        this.grdScheduledAppts.DataBind();
    }

    /// <summary>
    /// Binds corporate client scheduler and clinics details
    /// </summary>
    /// <param name="business_name"></param>
    private void bindClientSchedulerClinics(string business_name)
    {
        DataSet ds_clients = dbOperation.getCorporateBusinesses(business_name);

        if (ds_clients.Tables.Count > 0)
        {
            this.hfRegVisibleFields.Value = string.Empty;   
            this.txtCorporateClient.Text = this.ddlCorporateClients.Items.FindByText(business_name).Text;

            if (string.IsNullOrEmpty(business_name))
                this.iframeFileUpload.Attributes.Add("src", "../controls/fileUpload.aspx?clientName=" + Regex.Replace(this.ddlCorporateClients.Items[0].Text, @"[^0-9A-Za-z\s]+", "").Replace(" ", "_"));
            else
                this.iframeFileUpload.Attributes.Add("src", "../controls/fileUpload.aspx?clientName=" + Regex.Replace(business_name, @"[^0-9A-Za-z\s]+", "").Replace(" ", "_"));

            DataRow dr_client_sch_design;
            dr_client_sch_design = ds_clients.Tables[0].Rows[0];
            this.corporateScheduler.clientClinics = (ds_clients.Tables[1] != null && ds_clients.Tables[1].Rows.Count != 0) ? ds_clients.Tables[1] : null;
            this.corporateScheduler.scheduledAppointments = (ds_clients.Tables[2] != null && ds_clients.Tables[2].Rows.Count != 0) ? ds_clients.Tables[2] : null;

            //bind corporate client clinics            
            this.grdCorporateClinics.DataSource = this.corporateScheduler.clientClinics;
            this.grdCorporateClinics.DataBind();

            //bind scheduled appointments
            this.lnkShowAllScheduledAppt.Enabled = true;
            this.lnkSearchScheduledAppt.Enabled = true;
            this.lnkShowUnScheduledAppts.Visible = true;
            this.lnkHideUnScheduledAppts.Visible = false;
            ViewState["sortOrder"] = null;
            this.lblTotalApptScheduledText.Text = @"Total Scheduled: ";
            this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;
            this.bindScheduledAppointments();

            if (this.corporateScheduler.scheduledAppointments == null || this.corporateScheduler.scheduledAppointments.Rows.Count == 0)
            {
                this.lnkSearchScheduledAppt.Enabled = false;
            }

            //bind client scheduler setup values            
            if (dr_client_sch_design["clinicSchedulerXML"] == DBNull.Value)
                this.clientSchedulerXML.Load(HttpContext.Current.Server.MapPath("defaultSetupXML.xml"));
            else
                this.clientSchedulerXML.LoadXml(dr_client_sch_design["clinicSchedulerXML"].ToString()); //Set default values if the selected corporate client design is not yet created

            bool is_update_client_scheduler = false;

            if (this.clientSchedulerXML != null)
            {
                //Logos & styles
                this.displayClientLogo(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value, this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["mastheadLogo"].Value);
                this.txtBannerColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value;
                this.txtBannerTextColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerTextColor"].Value;
                this.txtButtonColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value;
                this.txtButtonTextColor.Text = this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value;

                //Landing page                
                this.txtSchedulerLPheaderText.Text = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/headerText").InnerText.Replace("<br/>", Environment.NewLine).Replace("<", "&lt;").Replace(">", "&gt;");
                this.txtSchedulerLPbodyText.Text = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/bodyText").InnerText.Replace("<br/>", Environment.NewLine).Replace("<", "&lt;").Replace(">", "&gt;");
                this.txtSchedulerLPfootertext.Text = this.clientSchedulerXML.SelectSingleNode(".//landingPageText/footerText").InnerText.Replace("<br/>", Environment.NewLine).Replace("<", "&lt;").Replace(">", "&gt;");

                //Required fields & site status
                this.ddlSiteStatus.SelectedValue = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["siteStatus"].Value;
                if (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["deactivationDate"].Value))
                {
                    this.txtDeActivationDate.Text = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["deactivationDate"].Value;
                    this.hfDeactivationDate.Value = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["deactivationDate"].Value;
                }
                else
                {
                    this.txtDeActivationDate.Text = string.Empty;
                    this.hfDeactivationDate.Value = string.Empty;
                    this.setSiteDeactivationDate();
                }

                if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields") == null)
                {
                    XmlElement visible_reg_fields = this.clientSchedulerXML.CreateElement("visibleRegistrationFields");
                    foreach (ListItem item in this.cblVisibleFields.Items)
                        visible_reg_fields.SetAttribute(item.Value, "1");

                    XmlNode required_reg_fields = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus");
                    required_reg_fields.InsertAfter(visible_reg_fields, required_reg_fields.FirstChild);
                }

                foreach (ListItem item in this.cblRequiredFields.Items)
                {
                    if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/requiredRegistrationFields").Attributes[item.Value] != null)
                    {
                        item.Selected = (Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/requiredRegistrationFields").Attributes[item.Value].Value)));
                        if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields").Attributes[item.Value] != null && item.Selected)
                            this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields").Attributes[item.Value].Value = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/requiredRegistrationFields").Attributes[item.Value].Value;
                    }
                    else
                        ((XmlElement)this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/requiredRegistrationFields")).SetAttribute(item.Value, "0");
                }

                foreach (ListItem item in this.cblVisibleFields.Items)
                {
                    if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields") != null && this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields").Attributes[item.Value] != null)
                        item.Selected = (Convert.ToBoolean(Convert.ToInt32(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields").Attributes[item.Value].Value)));
                    else if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields") != null)
                    {
                        //item.Selected = false;
                        ((XmlElement)this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields")).SetAttribute(item.Value, "1");

                    }
                }

                if (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedSiteLink").InnerText))
                {
                    this.hfEncryptedLongLink.Value = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedSiteLink").InnerText;

                    //Check & generate shorten scheduler client key if not exits
                    bool is_exists_client_key = true;
                    EncryptQueryStringAES args = new EncryptQueryStringAES();
                    if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientKey") != null)
                    {
                        if (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientKey").InnerText))
                        {
                            this.hfEncryptedClientLink.Value = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientKey").InnerText;
                            args = new EncryptQueryStringAES(this.hfEncryptedClientLink.Value);

                            if (args["arg1"].IndexOf(".") == -1)
                            {
                                this.hlSchedulerClientKey.Text = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + args["arg1"];
                                this.hlSchedulerClientKey.NavigateUrl = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + args["arg1"];
                            }
                            else
                                is_exists_client_key = false;
                        }
                        else
                            is_exists_client_key = false;
                    }
                    else
                    {
                        is_exists_client_key = false;
                        XmlElement encrypted_client_key = this.clientSchedulerXML.CreateElement("encryptedClientKey");
                        XmlNode required_reg_fields = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus");
                        required_reg_fields.InsertAfter(encrypted_client_key, required_reg_fields.LastChild);
                    }

                    if (!is_exists_client_key)
                    {
                        string rand_key = Regex.Replace(this.txtCorporateClient.Text, @"[^a-zA-Z0-9]+", "").Substring(0, 3).ToUpper() + randomDigits(6);
                        args["arg1"] = rand_key;

                        this.hfEncryptedClientLink.Value = args.ToString();
                        this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientKey").InnerText = args.ToString();

                        this.hlSchedulerClientKey.Text = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + args["arg1"];
                        this.hlSchedulerClientKey.NavigateUrl = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + args["arg1"];
                        is_update_client_scheduler = true;
                    }
                }
                else
                {
                    this.hfEncryptedLongLink.Value = string.Empty;
                    this.hfEncryptedClientLink.Value = string.Empty;
                    this.hlSchedulerClientKey.Text = string.Empty;
                    this.hlSchedulerClientKey.NavigateUrl = string.Empty;
                    this.hlEncryptedApptTrackingLink.Text = string.Empty;
                }

                //Display Client Appointment Tracker link
                if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientApptTrackingLink") != null)
                {
                    if (!string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientApptTrackingLink").InnerText))
                    {
                        this.hlEncryptedApptTrackingLink.Text = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientApptTrackingLink").InnerText;
                        this.hlEncryptedApptTrackingLink.NavigateUrl = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientApptTrackingLink").InnerText;
                    }
                }
                else
                {
                    XmlElement encrypted_client_link = this.clientSchedulerXML.CreateElement("encryptedClientApptTrackingLink");
                    XmlNode required_reg_fields = this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus");
                    required_reg_fields.InsertAfter(encrypted_client_link, required_reg_fields.LastChild);
                }

                //Create client appointment tracker link if client is scheduled already and tracker link doesn't exist
                if (!string.IsNullOrEmpty(this.hlSchedulerClientKey.Text) && string.IsNullOrEmpty(this.hlEncryptedApptTrackingLink.Text))
                {
                    EncryptQueryStringAES encrypt_string = new EncryptQueryStringAES();
                    encrypt_string["arg1"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Value;
                    encrypt_string["arg2"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Text;
                    encrypt_string["arg3"] = "clientSchedulerApptTracker.aspx";
                    this.hlEncryptedApptTrackingLink.Text = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + "corporateScheduler/clientSchedulerApptTracker.aspx?args=" + encrypt_string.ToString();
                    this.hlEncryptedApptTrackingLink.NavigateUrl = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + "corporateScheduler/clientSchedulerApptTracker.aspx?args=" + encrypt_string.ToString();
                    this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientApptTrackingLink").InnerText = this.hlEncryptedApptTrackingLink.Text;

                    is_update_client_scheduler = true;
                }
            }

            //Keep client info in session
            this.corporateScheduler.existingClinicSchedulerXML = this.clientSchedulerXML.OuterXml;
            this.corporateScheduler.clinicSchedulerXML = this.clientSchedulerXML.OuterXml;

            Session["clientDetails"] = this.corporateScheduler;

            if (is_update_client_scheduler)
            {
                string error_message = string.Empty;
                this.doSave(out error_message, 0);
            }
        }
    }

    /// <summary>
    /// Sets deactivation date of the corporate business
    /// </summary>
    private void setSiteDeactivationDate()
    {
        DateTime current_deactivation_date;
        if (this.corporateScheduler.clientClinics != null && this.corporateScheduler.clientClinics.Rows.Count > 0)
        {
            var clinic_dates = this.corporateScheduler.clientClinics.AsEnumerable().Where(X => X != null && !string.IsNullOrEmpty(X.Field<string>("clinicDate")) && X.Field<bool>("isChecked") == true).Select(X => X.Field<string>("clinicDate")).Distinct().ToList();

            if (clinic_dates.Count() > 0)
            {
                foreach (string clinic_date in clinic_dates)
                {
                    current_deactivation_date = !string.IsNullOrEmpty(this.hfDeactivationDate.Value) ? Convert.ToDateTime(this.hfDeactivationDate.Value) : Convert.ToDateTime(clinic_date);
                    this.hfDeactivationDate.Value = (DateTime.Compare(Convert.ToDateTime(clinic_date), current_deactivation_date) > 0) ? Convert.ToDateTime(clinic_date).ToString("MM/dd/yyyy") : current_deactivation_date.ToString("MM/dd/yyyy");
                }
            }
        }
    }

    /// <summary>
    /// Updates client details in session
    /// </summary>
    private void updateClientDetails()
    {
        if (Session["clientDetails"] != null)
        {
            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
            if (this.corporateScheduler.clinicSchedulerXML != null)
                this.clientSchedulerXML.LoadXml(this.corporateScheduler.clinicSchedulerXML);

            //Update design info and clinic details in session
            if (this.clientSchedulerXML != null && this.clientSchedulerXML.HasChildNodes)
            {
                //Logos & styles
                this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["mastheadLogo"].Value = this.rblClientlogo.SelectedValue;

                this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value = this.hfBannerColor.Value;
                this.txtBannerColor.Text = this.hfBannerColor.Value;
                this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["bannerTextColor"].Value = this.hfBannerTextColor.Value;
                this.txtBannerTextColor.Text = this.hfBannerTextColor.Value;
                this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonColor"].Value = this.hfButtonColor.Value;
                this.txtButtonColor.Text = this.hfButtonColor.Value;
                this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["buttonTextColor"].Value = this.hfButtonTextColor.Value;
                this.txtButtonTextColor.Text = this.hfButtonTextColor.Value;

                //Landing page
                if (this.clientSchedulerXML.SelectSingleNode(".//landingPageText/headerText").InnerText != this.txtSchedulerLPheaderText.Text.Replace(Environment.NewLine, "<br/>"))
                    this.clientSchedulerXML.SelectSingleNode(".//landingPageText/headerText").InnerText = this.txtSchedulerLPheaderText.Text.Replace(Environment.NewLine, "<br/>");
                if (this.clientSchedulerXML.SelectSingleNode(".//landingPageText/bodyText").InnerText != this.txtSchedulerLPbodyText.Text.Replace(Environment.NewLine, "<br/>").Replace("&lt;", "<").Replace("&gt;", ">"))
                    this.clientSchedulerXML.SelectSingleNode(".//landingPageText/bodyText").InnerText = this.txtSchedulerLPbodyText.Text.Replace(Environment.NewLine, "<br/>").Replace("&lt;", "<").Replace("&gt;", ">");
                if (this.clientSchedulerXML.SelectSingleNode(".//landingPageText/footerText").InnerText != this.txtSchedulerLPfootertext.Text.Replace(Environment.NewLine, "<br/>").Replace("&lt;", "<").Replace("&gt;", ">"))
                    this.clientSchedulerXML.SelectSingleNode(".//landingPageText/footerText").InnerText = this.txtSchedulerLPfootertext.Text.Replace(Environment.NewLine, "<br/>").Replace("&lt;", "<").Replace("&gt;", ">");

                //Required fields & site status
                this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["siteStatus"].Value = this.ddlSiteStatus.SelectedValue;
                if (!string.IsNullOrEmpty(this.hfDeactivationDate.Value))
                    this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["deactivationDate"].Value = string.Format(this.hfDeactivationDate.Value, "MM/dd/yyyy");
                else
                    this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus").Attributes["deactivationDate"].Value = string.Empty;

                this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedSiteLink").InnerText = this.hfEncryptedLongLink.Value;

                if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientKey") != null)
                {
                    this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientKey").InnerText = this.hfEncryptedClientLink.Value;
                }

                foreach (ListItem item in this.cblRequiredFields.Items)
                {
                    if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/requiredRegistrationFields").Attributes[item.Value] != null)
                        this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/requiredRegistrationFields").Attributes[item.Value].Value = (item.Selected) ? "1" : "0";
                    else
                        ((XmlElement)this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/requiredRegistrationFields")).SetAttribute(item.Value, (item.Selected) ? "1" : "0");
                }

                string[] reg_visible_fields = this.hfRegVisibleFields.Value.TrimEnd(',').Split(',');
                int counter = 0;
                foreach (ListItem item in this.cblVisibleFields.Items)
                {
                    if (this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields").Attributes[item.Value] != null)
                    {
                        this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields").Attributes[item.Value].Value = (item.Selected) ? "1" : "0";
                        this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/visibleRegistrationFields").Attributes[item.Value].Value = reg_visible_fields[counter].ToString();
                    }
                    counter += 1;
                }

                this.corporateScheduler.clinicSchedulerXML = this.clientSchedulerXML.OuterXml;
            }

            DataTable dt_client_clinics = this.corporateScheduler.clientClinics;
            string clinic_pk = string.Empty;
            foreach (GridViewRow row in this.grdCorporateClinics.Rows)
            {
                clinic_pk = this.grdCorporateClinics.DataKeys[row.RowIndex]["clinicPk"].ToString();
                CheckBox chk_scheduled = (CheckBox)row.FindControl("chkScheduleClinic");
                DropDownList ddl_immunizer = (DropDownList)row.FindControl("ddlImmunizers");
                DropDownList ddl_max_immunizer = (DropDownList)row.FindControl("ddlImmunizersMax");

                if (dt_client_clinics != null && dt_client_clinics.Rows.Count > 0)
                {
                    DataRow row_clinic = dt_client_clinics.Select("clinicPk = " + clinic_pk).ElementAt(0);
                    row_clinic["isChecked"] = (chk_scheduled.Checked) ? 1 : 0;
                    row_clinic["changedImmunizers"] = ddl_immunizer.SelectedValue;
                    row_clinic["maxImmunizers"] = !string.IsNullOrEmpty(row_clinic["maxImmunizers"].ToString()) ? row_clinic["maxImmunizers"].ToString() : "3";
                    row_clinic["changedMaxImmunizers"] = !string.IsNullOrEmpty(ddl_max_immunizer.SelectedValue) ? ddl_max_immunizer.SelectedValue : "3" ;
                }
            }
            this.corporateScheduler.clientClinics = dt_client_clinics;

            Session["clientDetails"] = this.corporateScheduler;
        }
    }

    /// <summary>
    /// Prepares Clinic Scheduler Design XML
    /// </summary>
    /// <returns></returns>
    private XmlDocument prepareClinicsDetailsXML()
    {
        this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
        DataTable dt_client_clinics = this.corporateScheduler.clientClinics;

        XmlDocument clinics_xml_doc = new XmlDocument();
        XmlElement client_ele = clinics_xml_doc.CreateElement("client");
        int max_immunizers;
        int changed_max_immunizers;

        if (dt_client_clinics != null && dt_client_clinics.Rows.Count > 0)
        {
            foreach (DataRow dr_clinic in dt_client_clinics.Rows)
            {
                Int32.TryParse(dr_clinic["maxImmunizers"].ToString(), out max_immunizers);
                Int32.TryParse(dr_clinic["changedMaxImmunizers"].ToString(), out changed_max_immunizers);

                if (Convert.ToBoolean(dr_clinic["isStoreConfirmed"].ToString()) && (Convert.ToBoolean(dr_clinic["isScheduled"]) != Convert.ToBoolean(dr_clinic["isChecked"]) || Convert.ToInt32(dr_clinic["immunizers"]) != Convert.ToInt32(dr_clinic["changedImmunizers"]) || max_immunizers != changed_max_immunizers))
                {
                    XmlElement clinic_ele = clinics_xml_doc.CreateElement("clinic");
                    clinic_ele.SetAttribute("id", dr_clinic["clinicPk"].ToString());
                    clinic_ele.SetAttribute("isScheduled", (dr_clinic["isChecked"].ToString() == "True") ? "1" : "0");
                    clinic_ele.SetAttribute("immunizers", dr_clinic["changedImmunizers"].ToString());
                    clinic_ele.SetAttribute("maxImmunizers", dr_clinic["changedMaxImmunizers"].ToString());
                    client_ele.AppendChild(clinic_ele);
                }
            }
        }
        clinics_xml_doc.AppendChild(client_ele);

        return clinics_xml_doc;
    }

    /// <summary>
    /// Displays client logo
    /// </summary>
    /// <param name="file_name"></param>
    /// <param name="logo_type"></param>
    private void displayClientLogo(string file_name, string logo_type)
    {
        string image_path_loc = string.Empty;
        this.imgClientlogo.Visible = true;
        if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + file_name))
            image_path_loc = @"ClientLogos";

        this.rblClientlogo.ClearSelection();
        this.rblClientlogo.Items.FindByValue(logo_type).Selected = true;
        if (!string.IsNullOrEmpty(image_path_loc))
        {
            this.lblFileName.Text = file_name;
            this.imgClientlogo.ImageUrl = "~/controls/displayImage.ashx?image=" + file_name + "&extraQS=" + DateTime.Now.Second;
        }
        else
        {
            this.lblFileName.Text = "No file exists!";
            this.imgClientlogo.Visible = false;
        }
    }

    /// <summary>
    /// Checks unsaved changes
    /// </summary>
    private bool checkUnsavedChanges()
    {
        bool is_changed = false;
        if (Session["clientDetails"] != null)
        {
            bool is_equal;
            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
            XDocument old_client_scheduler_xml = new XDocument();
            XDocument new_client_scheduler_xml = new XDocument();

            old_client_scheduler_xml = XDocument.Parse(this.corporateScheduler.existingClinicSchedulerXML);
            new_client_scheduler_xml = XDocument.Parse(this.corporateScheduler.clinicSchedulerXML);
            is_equal = XDocument.DeepEquals(new_client_scheduler_xml, old_client_scheduler_xml);

            if (!is_equal)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alert", "showSaveChangesWarning('" + String.Format((string)GetGlobalResourceObject("errorMessages", "saveClinicSchedulerChanges"), this.txtCorporateClient.Text.Replace("'", "`")) + "');", true);
                is_changed = true;
            }
        }
        return is_changed;
    }

    /// <summary>
    /// Saves Client Scheduler Design info
    /// </summary>
    /// <param name="error_message"></param>
    /// <param name="is_block"></param>
    /// <returns></returns>
    private int doSave(out string error_message, int is_block)
    {
        int return_value = 0;
        error_message = string.Empty;
        EncryptQueryStringAES encrypt_string = new EncryptQueryStringAES();
        if (Session["clientDetails"] != null)
        {
            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
            if (!string.IsNullOrEmpty(this.corporateScheduler.clinicSchedulerXML))
            {
                this.clientSchedulerXML.LoadXml(this.corporateScheduler.clinicSchedulerXML);
                if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedSiteLink").InnerText))
                {
                    encrypt_string["arg1"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Value;
                    encrypt_string["arg2"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Text;
                    encrypt_string["arg3"] = "walgreensSchedulerLanding.aspx";

                    this.hfEncryptedLongLink.Value = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + encrypt_string.ToString();
                    this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedSiteLink").InnerText = this.hfEncryptedLongLink.Value;

                    encrypt_string = new EncryptQueryStringAES();
                    string rand_key = (Regex.Replace(this.txtCorporateClient.Text, @"[^a-zA-Z0-9]+", "").Length < 3 ? Regex.Replace(this.txtCorporateClient.Text, @"[^a-zA-Z0-9]+", "").PadRight(3, '0') : Regex.Replace(this.txtCorporateClient.Text, @"[^a-zA-Z0-9]+", "").Substring(0, 3).ToUpper()) + randomDigits(6);
                    encrypt_string["arg1"] = rand_key;

                    this.hlSchedulerClientKey.Text = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + rand_key;
                    this.hfEncryptedClientLink.Value = encrypt_string.ToString();
                    this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientKey").InnerText = this.hfEncryptedClientLink.Value;
                }

                if (string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientApptTrackingLink").InnerText))
                {
                    encrypt_string = new EncryptQueryStringAES();
                    encrypt_string["arg1"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Value;
                    encrypt_string["arg2"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Text;
                    encrypt_string["arg3"] = "clientSchedulerApptTracker.aspx";

                    this.hlEncryptedApptTrackingLink.Text = ((!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath()) + "corporateScheduler/clientSchedulerApptTracker.aspx?args=" + encrypt_string.ToString();
                    this.clientSchedulerXML.SelectSingleNode(".//requiredFieldsAndSiteStatus/encryptedClientApptTrackingLink").InnerText = this.hlEncryptedApptTrackingLink.Text;
                }

                this.corporateScheduler.clinicSchedulerXML = this.clientSchedulerXML.OuterXml;

                //Check if logo is uploaded when client logo is selected
                if (this.rblClientlogo.SelectedValue == "clientLogo" && string.IsNullOrEmpty(this.clientSchedulerXML.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value))
                    return_value = -4;
                else
                    return_value = this.dbOperation.updateClientSchedulerSetup(Convert.ToInt32(Convert.ToInt32(this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Value)), this.corporateScheduler.clinicSchedulerXML, this.prepareClinicsDetailsXML(), out this.dtScheduledAppts, out error_message, is_block);
            }
        }

        return return_value;
    }

    /// <summary>
    /// Generate random number
    /// </summary>
    /// <param name="length"></param>
    /// <returns></returns>
    private string randomDigits(int length)
    {
        var random = new Random();
        string s = string.Empty;
        for (int i = 0; i < length; i++)
            s = String.Concat(s, random.Next(10).ToString());
        return s;
    }

    /// <summary>
    /// Creats block/unblock appointments xml
    /// </summary>
    /// <param name="action_type"></param>
    /// <returns></returns>
    private XmlDocument prepareClinicAppointmentsXML(int action_type)
    {
        XmlDocument appts_xml_doc = new XmlDocument();
        XmlElement appts_ele = appts_xml_doc.CreateElement("appointments");

        foreach (GridViewRow row_appt in this.grdScheduledAppts.Rows)
        {
            CheckBox chk_appt = (CheckBox)row_appt.FindControl("chkScheduledAppt");
            Label lbl_is_blocked = (Label)row_appt.FindControl("lblIsBlocked");
            if (chk_appt.Checked && (action_type == 1 ? (lbl_is_blocked.Text == "1" || lbl_is_blocked.Text == "2") : lbl_is_blocked.Text == action_type.ToString()))
            {
                XmlElement appt_ele = appts_xml_doc.CreateElement("appointment");
                appt_ele.SetAttribute("clinicPk", this.grdScheduledAppts.DataKeys[row_appt.RowIndex]["clinicPk"].ToString());
                appt_ele.SetAttribute("apptPk", this.grdScheduledAppts.DataKeys[row_appt.RowIndex]["apptPk"].ToString());
                appt_ele.SetAttribute("apptTimePk", this.grdScheduledAppts.DataKeys[row_appt.RowIndex]["apptTimePk"].ToString());
                appt_ele.SetAttribute("confirmationId", !Convert.ToBoolean(action_type) ? row_appt.Cells[10].Text : "");

                appts_ele.AppendChild(appt_ele);
            }
        }
        appts_xml_doc.AppendChild(appts_ele);

        return appts_xml_doc;
    }

    /// <summary>
    /// Sends rescheduled appointment email for the unscheduled clinics
    /// </summary>
    private void sendRescheduleAppointmentEmail()
    {
        if (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0)
        {
            string email_body, subject_line, email_path, email_to, contact_info;
            EncryptQueryStringAES args = new EncryptQueryStringAES();
            string logo_path = string.Empty;
            foreach (DataRow dr_appt in this.dtScheduledAppts.Rows)
            {
                email_to = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dr_appt["email"].ToString());
                email_path = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
                args["arg1"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Value;
                args["arg2"] = dr_appt["apptPk"].ToString();
                args["arg3"] = "walgreensSchedulerRegistration.aspx";
                args["arg4"] = dr_appt["clinicPk"].ToString();

                subject_line = (string)GetGlobalResourceObject("errorMessages", "rescheduleApptSubjectLine");

                contact_info = "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
                foreach (KeyValuePair<string, string> reg_details in this.registrationFields())
                {
                    if (dr_appt[reg_details.Key] != DBNull.Value && !string.IsNullOrEmpty(dr_appt[reg_details.Key].ToString()))
                    {
                        contact_info += "<tr><td width='30%' align='left' valign='top' style='padding-left:24px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>" + reg_details.Value + ":</td>";
                        contact_info += "<td width='70%' align='left' valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>" + dr_appt[reg_details.Key].ToString() + "</td></tr>";
                    }
                }
                contact_info += "</table>";

                //email_body = "<table width='100%' border='0' cellspacing='0' cellpadding='4'><tr><td colspan='2' style='font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #000000; line-height:125%; font-weight:bold'>Appointment</td>";
                email_body = "<table width='100%' border='0' cellspacing='0' cellpadding='4'><tr>";
                email_body += "<td width='30%' align='left' valign='top' nowrap='nowrap' style='padding-left:24px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>Appointment Time:</td>";
                email_body += "<td width='70%' align='left' valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: red; font-weight:bold'>" + dr_appt["apptDate"].ToString() + " " + dr_appt["apptAvlTimes"].ToString() + "</td></tr><tr>";
                email_body += "<td align='left' valign='top' style='padding-left:24px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>Location:</td>";
                email_body += "<td align='left' valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; color: red; font-weight:bold'>" + dr_appt["clinicAddress"].ToString() + "</td></tr></table>";


                if (this.imgClientlogo.ImageUrl.Length > 0)
                    logo_path = email_path + ApplicationSettings.EmailLogoImagePath() + "/" + this.imgClientlogo.ImageUrl.Split('=')[1].Substring(0, this.imgClientlogo.ImageUrl.Split('=')[1].LastIndexOf('&'));

                this.walgreensEmail.sendRescheduleAppointmentEmail(email_path + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + args.ToString(), Server.MapPath("~/emailTemplates/walgreensSchedulerClinicRevisionEmail.html"), subject_line, dr_appt["confirmationId"].ToString(), contact_info, email_body, email_to, "", true, logo_path, this.txtBannerColor.Text);
            }
        }
    }

    /// <summary>
    /// Sends rescheduled appointment email to blocked appointments
    /// </summary>
    private void sendBlockedAppointmentRescheduleEmail()
    {
        if (this.walgreensEmail != null && Session["clientDetails"] != null)
        {
            string email_body, subject_line, email_path, email_to, logo_path = string.Empty;
            EncryptQueryStringAES args = new EncryptQueryStringAES();

            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
            DataTable dt_scheduled_appt = new DataTable();
            dt_scheduled_appt = this.corporateScheduler.scheduledAppointments;

            foreach (XmlNode appt_node in this.clientSchedulerXML.SelectNodes(".//appointment"))
            {
                DataRow dr_appt = dt_scheduled_appt.Select("clinicPk = " + appt_node.Attributes["clinicPk"].Value + " And apptPk = " + appt_node.Attributes["apptPk"].Value).ElementAt(0);

                email_to = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dr_appt["apptEeEmail"].ToString());
                email_path = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
                args["arg1"] = this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text.Trim()).Value;
                args["arg2"] = appt_node.Attributes["apptPk"].Value;
                args["arg3"] = "walgreensSchedulerRegistration.aspx";
                args["arg4"] = appt_node.Attributes["clinicPk"].Value;

                subject_line = (string)GetGlobalResourceObject("errorMessages", "rescheduleApptSubjectLine");

                email_body = "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Appointment: <span style='color: red;'>" + dr_appt["apptDate"].ToString() + " " + dr_appt["apptAvlTimes"].ToString() + "</span></p>";
                email_body += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Location: <br /><span style='color: red;'>" + dr_appt["clinicAddress"].ToString() + "</span></p><br />";

                if (this.imgClientlogo.ImageUrl.Length > 0)
                    logo_path = email_path + ApplicationSettings.EmailLogoImagePath() + "/" + this.imgClientlogo.ImageUrl.Split('=')[1].Substring(0, this.imgClientlogo.ImageUrl.Split('=')[1].LastIndexOf('&'));

                this.walgreensEmail.sendBlockedAppointmentRescheduleEmail(email_path + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + args.ToString(), Server.MapPath("~/emailTemplates/rescheduleAppointmentEmail.html"), subject_line, email_body, email_to, "", true, logo_path, this.txtBannerColor.Text);
            }
        }
    }

    /// <summary>
    /// Gets All scheduled or unscheduled appointments
    /// </summary>
    private void getScheduledAppointments()
    {
        int total_immunizers = 0;
        this.txtSearchAppointments.Text = "";
        if (!string.IsNullOrEmpty(this.txtCorporateClient.Text))
        {
            this.ddlCorporateClients.ClearSelection();
            this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text).Selected = true;

            this.corporateScheduler = (CorporateScheduler)Session["clientDetails"];
            if (!string.IsNullOrEmpty(this.ddlCorporateClients.SelectedValue) && (this.ddlCorporateClients.Items.FindByText(this.txtCorporateClient.Text) != null))
            {
                if (this.lnkHideUnScheduledAppts.Visible)
                {
                    this.lblTotalApptScheduledText.Text = @"Total unscheduled: ";
                    this.corporateScheduler.scheduledAppointments = this.dbOperation.getUnScheduledAppointments(1, Convert.ToInt32(this.ddlCorporateClients.SelectedValue), out total_immunizers);
                }
                else
                {
                    this.lblTotalApptScheduledText.Text = @"Total Scheduled: ";
                    this.corporateScheduler.scheduledAppointments = this.dbOperation.getClinicScheduledAppointments(1, Convert.ToInt32(this.ddlCorporateClients.SelectedValue), out total_immunizers);
                }

                Session["clientDetails"] = this.corporateScheduler;
            }

            this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;
            ViewState["sortOrder"] = null;

            this.grdScheduledAppts.PageIndex = 0;
            this.bindScheduledAppointments();
        }
    }

    /// <summary>
    /// Binds scheduled appointments
    /// </summary>
    private void bindScheduledAppointments()
    {
        bool has_rows = false;
        if (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0)
        {
            has_rows = true;
            if (ViewState["sortOrder"] != null)
                this.dtScheduledAppts.DefaultView.Sort = ViewState["sortOrder"].ToString();

            if (this.dtScheduledAppts.Select("isBlocked = 0").Count() > 0)
                this.lblTotalApptSchedule.Text = this.dtScheduledAppts.Select("isBlocked = 0").Count().ToString();
        }

        this.grdScheduledAppts.DataSource = this.dtScheduledAppts;
        this.grdScheduledAppts.DataBind();
        this.lblTotalApptSchedule.Visible = has_rows;
        this.lnkBlockCheckedAppts.Enabled = (has_rows && (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0 && this.dtScheduledAppts.Select("isBlocked = 0").Count() > 0)) ? true : false; ;
        this.lnkUnBlockCheckedAppts.Enabled = (has_rows && (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0 && this.dtScheduledAppts.Select("isBlocked = 1 or isBlocked = 2").Count() > 0)) ? true : false;
    }

    /// <summary>
    /// Registered appointment fields
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> registrationFields()
    {
        Dictionary<string, string> reg_fields = new Dictionary<string, string>();
        reg_fields.Add("patientName", "Name");
        reg_fields.Add("email", "Email");
        //reg_fields.Add("phone", "Phone");
        //reg_fields.Add("address", "Address");
        //reg_fields.Add("gender", "Gender");
        //reg_fields.Add("dateOfBirth", "Birth Date");

        return reg_fields;
    }

    /// <summary>
    /// Disables scheduler setup buttons
    /// </summary>
    /// <param name="is_disable"></param>
    private void disabledButtons(bool is_disable)
    {
        this.imgClientlogo.Visible = !is_disable;
        this.lnkResetDefaultColors.Enabled = !is_disable;
        this.lnkBtnResetHeaderText.Enabled = !is_disable;
        this.lnkBtnResetBodyText.Enabled = !is_disable;
        this.lnkBtnResetFooterText.Enabled = !is_disable;
        this.lnkSearchScheduledAppt.Enabled = !is_disable;
        this.lnkShowAllScheduledAppt.Enabled = !is_disable;
        this.rblClientlogo.Enabled = !is_disable;
        this.lnkShowUnScheduledAppts.Enabled = !is_disable;
        this.lnkHideUnScheduledAppts.Enabled = !is_disable;
    }

    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }
    #endregion

    [WebMethod]
    public static void setSelectedTabId(string home_tab_id)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        common_app_session.SelectedStoreSession.SelectedCorporateClinicsTabId = home_tab_id;
    }

    #region ----------------- PRIVATE VARIABLES -----------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    private CorporateScheduler corporateScheduler;
    private XmlDocument clientSchedulerXML;
    private int returnValue;
    private WalgreenEmail walgreensEmail = null;
    private DataTable dtScheduledAppts;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        if (this.commonAppSession.LoginUserInfoSession != null && !this.commonAppSession.LoginUserInfoSession.IsAdmin)
            Response.Redirect("../walgreensLandingPage.aspx");

        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.corporateScheduler = new CorporateScheduler();
        this.clientSchedulerXML = new XmlDocument();
        this.dtScheduledAppts = new DataTable();
        this.walgreensEmail = ApplicationSettings.emailSettings();
    }
    #endregion
}