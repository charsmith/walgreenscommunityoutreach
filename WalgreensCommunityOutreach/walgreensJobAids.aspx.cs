﻿using System;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using System.Web;

public partial class walgreensJobAids : Page
{
    #region ------------------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        //Page Load
       
    } 
    #endregion

    #region --------- PRIVATE VARIABLES----------
    protected AppCommonSession commonAppSession = null;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
}
