﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensCharityProgramClinicDetails.aspx.cs"
    Inherits="walgreensCharityProgramClinicDetails" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <script src="javascript/commonFunctions.js?v=10202015" type="text/javascript"></script>
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>    
    <link rel="stylesheet" href="css/ddcolortabs.css" type="text/css" />
    <link rel="stylesheet" href="css/wags.css?v=10202015" type="text/css" />
    <link rel="stylesheet" href="css/theme.css" type="text/css" />
    <link rel="stylesheet" href="css/calendarStyle.css" type="text/css" />
    <link rel="stylesheet" href="themes/jquery-ui-1.8.17.custom.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.timepicker.css" type="text/css" />
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>
	<link rel="stylesheet"  href="css/jquery-ui.css" type="text/css" />
    <style type="text/css">
        .wagButtonDim {
	        -moz-box-shadow: 0px 0px 0px 0px #ffffff;
	        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;
	        box-shadow: 0px 0px 0px 0px #ffffff;
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #CFECEC), color-stop(1, #B7CEEC));
	        background:-moz-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:-webkit-linear-gradient(top,#CFECEC 5%, #B7CEEC 100%);
	        background:-o-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:-ms-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:linear-gradient(to bottom, #CFECEC 5%, #B7CEEC 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#CFECEC', endColorstr='#B7CEEC',GradientType=0);
	        background-color:#CFECEC;
	        -moz-border-radius:3px;
	        -webkit-border-radius:3px;
	        border-radius:3px;
	        border:1px solid #B7CEEC;
	        display:inline-block;
	        cursor:pointer;
	        color:#ffffff;
	        font-family:Arial;
	        font-size:12px;
	        padding:6px 12px;
	        text-decoration:none;
	        text-shadow:0px -1px 0px #B7CEEC;
	        behavior:url(PIE.htc);
        }
        
        .wagButton {
	        -moz-box-shadow: 0px 0px 0px 0px #ffffff;
	        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;
	        box-shadow: 0px 0px 0px 0px #ffffff;
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #57a1d3), color-stop(1, #2f6fa7));
	        background:-moz-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-webkit-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-o-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-ms-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:linear-gradient(to bottom, #57a1d3 5%, #2f6fa7 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#57a1d3', endColorstr='#2f6fa7',GradientType=0);
	        background-color:#57a1d3;
	        -moz-border-radius:3px;
	        -webkit-border-radius:3px;
	        border-radius:3px;
	        border:1px solid #2f6fa7;
	        display:inline-block;
	        cursor:pointer;
	        color:#ffffff;
	        font-family:Arial;
	        font-size:12px;
	        padding:6px 12px;
	        text-decoration:none;
	        text-shadow:0px -1px 0px #2f6fa7;
	        behavior:url(PIE.htc);
        }
        
        .wagButton:hover {
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #2f6fa7), color-stop(1, #57a1d3));
	        background:-moz-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-webkit-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-o-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-ms-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:linear-gradient(to bottom, #2f6fa7 5%, #57a1d3 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#2f6fa7', endColorstr='#57a1d3',GradientType=0);
	        background-color:#2f6fa7;
        }
        .wagButton:active {
	        position:relative;
	        top:1px;
        }
        .ui-dialog-titlebar-close {
            visibility: hidden;
        }
        .ui-widget-content A
        {
            color:#ffffff;
        }
        .ui-timepicker-list li
        {
            color: #333333;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
        }
        .style2
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: bold;
            color: #333;
            width: 90px;
        }
        .style3
        {
            width: 310px;
        }
        .gridViewRow
        {
           
            padding:5px;
        }
        .ui-widget-header
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 13px;
            font-weight: bold;
        }
        
        .ui-widget
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            font-weight: normal;
        }
        .tooltip
        {
            display: none;
            font-size: 12px;
            height: 70px;
            color: #fff;
        }
        .tooltipText
        {
            font-weight:bold;
            font-size:11px;
            color:red;
            position:absolute;
            white-space: pre-wrap;
            max-width:180px;
            padding:2px;
            border-radius:0px;
            -webkit-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            -moz-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.formFields').keypress(function (key) {
                if (key.charCode == 63) return false;
            });
        });
        //setting control css
        var setInvalidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "background-color": "yellow" });

            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.tooltip({
                tooltipClass: "tooltipText"
            });
            ctrl.focus(function (evt) {
                $(evt.currentTarget).tooltip("close");
            });
            ctrl.tooltip("enable");

        }
        var setValidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "border": "1px solid gray" });
            ctrl.css({ "background-color": "" });
            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.removeAttr("title");
            ctrl.tooltip({
                disabled: true
            });
            ctrl.on("click", function () {
                ctrl.data("title", ctrl.attr("title")).removeAttr("title");
            }, function () {
                var title = ctrl.data("title");
                ctrl.tooltip("option", "content", title || ctrl.attr("title"));
            });

        }

        $("[title]").tooltip();
        isPageValid = true;
        $(document).ready(function () {
            if ($.browser.msie) {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keydown(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keydown(function (event) { event.preventDefault(); });
            }
            else {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keypress(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keypress(function (event) { event.preventDefault(); });
            }

           $("#grdLocations").find("input[type=text][id*=txtEstShots]").keypress(function (event) {
               var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
               if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
               else return false;
           });

           $("#grdLocations").find("input[type=text][id*=txtVouchersDistributed]").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#grdLocations").find("input[type=text][id*=txtTotalImm]").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#txtDefaultVouchersDistributed").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#txtDefaultEstShots").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#txtDefaultTotalImm").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            var maxLength = 140;
            var returnValue = false;
            $("#idCount").text(maxLength);
            $("#idLabel").text(maxLength);
            $("#txtFeedBack").keypress(function (e) {
                if (($("#txtFeedBack").val().length < maxLength) || (e.keyCode == 8 || e.keyCode == 46 || (e.keyCode >= 35 && e.keyCode <= 40))) {
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                    returnValue = true;
                }
                else {
                    $("#idCount").text(0);
                    returnValue = false;
                }
                return returnValue;
            });

            $("#txtFeedBack").focusout(function () {
                if ($("#txtFeedBack").val().length > maxLength) {
                    $("#txtFeedBack").val($("#txtFeedBack").val().substring(0, maxLength));
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                }
                else {
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                }

                $("#txtFeedBack").val($("#txtFeedBack").val().replace("<", "&lt;").replace(">", "&gt;"));
            });
        });

        function AlertMessage(alert_message, clinic_location, message) {
            if (alert_message != "")
                alert_message = "\r\n" + clinic_location + ": " + message;
            else
                alert_message = clinic_location + ": " + message;

            return alert_message;
        }

        function CustomValidatorForLocations(validationGroup) {
            var validating_fields = $("form :text, textarea, select");// $('input[type=text],textarea,select');
            var is_MO_state = $('#hfisMOState').val().toLowerCase() === 'true';
            var is_required = !$("#grdLocations").find("input[type=checkbox][id*=chkNoClinic]")[0].checked;
            var validating_field_names = {

                //**********************  Default Store ********************
                'txtDefaultClinicStoreId': { 'isRequired': false, 'when': 'always', 'name': 'Store Id', 'type': 'numeric', 'requiredMessage': 'Store Id is required', 'invalidMessage': 'Please enter valid Store Id' },

                //**********************  Business Information Start ********************
                'txtFirstContactName': { 'isRequired': true, 'when': 'always', 'name': 'Contact First Name', 'type': 'junk', 'requiredMessage': 'Contact First Name is required', 'invalidMessage': 'Contact First Name: , < > characters are not allowed' },
                'txtLastContactName': { 'isRequired': true, 'when': 'always', 'name': 'Contact Last Name', 'type': 'junk', 'requiredMessage': 'Contact Last Name is required', 'invalidMessage': 'Contact Last Name: , < > characters are not allowed' },
                'txtContactJobTitle': { 'isRequired': false, 'when': 'always', 'name': 'Contact Job Title', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': 'Contact Job Title: , < > characters are not allowed' },
                
                //**********************  Billing & Vaccine Information ********************
                'txtPlanId': { 'isRequired': true, 'when': 'always', 'name': 'Plan ID', 'type': 'junk', 'requiredMessage': 'Plan ID is required', 'invalidMessage': 'Plan ID: < > characters are not allowed' },
                'txtGroupId': { 'isRequired': true, 'when': 'always', 'name': 'Group ID', 'type': 'junk', 'requiredMessage': 'Group ID is required', 'invalidMessage': 'Group ID: < > characters are not allowed' },
                'txtIdRecipient': { 'isRequired': true, 'when': 'always', 'name': 'ID Recipient', 'type': 'junk', 'requiredMessage': 'ID Recipient is required', 'invalidMessage': 'ID Recipient: < > characters are not allowed' },
                'txtDefaultVouchersDistributed': { 'isRequired': true, 'when': 'always', 'name': 'Vouchers Distributed', 'type': 'numeric', 'requiredMessage': 'Please enter the number of Vouchers Distributed', 'invalidMessage': 'Please enter the valid number of Vouchers Distributed' },
                'txtDefaultEstShots': { 'isRequired': is_required, 'when': 'always', 'name': 'Estimated Shots', 'type': 'numeric', 'requiredMessage': 'Estimated Shots is required (ex: #####)', 'invalidMessage': 'Please enter valid Estimated Shots (ex: #####).' },
                'txtDefaultTotalImm': { 'isRequired': false, 'when': 'always', 'name': 'Total Immunizations Administered', 'type': 'numeric', 'requiredMessage': '', 'invalidMessage': 'Please enter valid number of Total Immunizations Administered (ex: #####)' },

                //********************** Clinic Information *****************
                'txtVouchersDistributed': { 'isRequired': true, 'when': 'always', 'name': 'Vouchers Distributed', 'type': 'numeric', 'requiredMessage': 'Please enter the number of Vouchers Distributed', 'invalidMessage': 'Please enter the valid number of Vouchers Distributed' },
                'txtContactFirstName': { 'isRequired': false, 'when': 'always', 'name': 'Contact First Name', 'type': 'junk', 'requiredMessage': 'Contact First Name is required', 'invalidMessage': 'Contact First Name: < > characters are not allowed' },
                'txtContactLastName': { 'isRequired': false, 'when': 'always', 'name': 'Contact Last Name', 'type': 'junk', 'requiredMessage': 'Contact Last Name is required', 'invalidMessage': 'Contact Last Name: < > characters are not allowed' },
                'txtLocalContactPhone': { 'isRequired': false, 'when': 'always', 'name': 'Contact Phone', 'type': 'phone', 'requiredMessage': 'Phone number is required', 'invalidMessage': 'Valid phone number is required(ex: ###-###-####)' },
                'txtLocalContactEmail': { 'isRequired': false, 'when': 'always', 'name': 'Contact Email', 'type': 'email', 'requiredMessage': 'Email is required', 'invalidMessage': 'Please enter a valid email address' },
                'txtAddress1': { 'isRequired': (is_MO_state ? true : false), 'when': 'always', 'name': 'Address1', 'type': 'address', 'requiredMessage': 'Address1 is required', 'invalidMessage': 'Clinic Address1: < > characters are not allowed' },
                'txtAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'address', 'requiredMessage': '', 'invalidMessage': 'Clinic Address2: < > characters are not allowed' },
                'txtCity': { 'isRequired': (is_MO_state ? true : false), 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': 'City is required', 'invalidMessage': 'Clinic City: < > characters are not allowed' },
                'txtZipCode': { 'isRequired': (is_MO_state ? true : false), 'when': 'always', 'name': 'ZipCode', 'type': 'zipcode', 'requiredMessage': 'Zip Code is required', 'invalidMessage': 'Please enter a valid Zip Code (ex: #####)' },
                'ddlState': { 'isRequired': (is_MO_state ? true : false), 'when': 'always', 'name': 'state', 'type': 'select', 'requiredMessage': 'Select State', 'invalidMessage': '' },
                'txtClinicStore': { 'isRequired': true, 'when': 'always', 'name': 'Clinic Store', 'type': 'numeric', 'requiredMessage': 'Store Id is required', 'invalidMessage': 'Please enter valid Store Id' },
                'txtEstShots': { 'isRequired': false, 'when': 'always', 'name': 'Estimated Shots', 'type': 'numeric', 'requiredMessage': 'Estimated Shots is required (ex: #####)', 'invalidMessage': 'Please enter valid Estimated Shots (ex: #####)' },
                'txtTotalImm': { 'isRequired': false, 'when': 'always', 'name': 'Total Immunizations Administered', 'type': 'numeric', 'requiredMessage': 'Total Administered is required', 'invalidMessage': 'Please enter valid number of Total Immunizations Administered (ex: #####)' },
                'txtStartTime': { 'isRequired': false, 'when': 'always', 'name': 'Start Time', 'type': '', 'requiredMessage': 'Start Time is required', 'invalidMessage': '' },
                'txtEndTime': { 'isRequired': false, 'when': 'always', 'name': 'End Time', 'type': '', 'requiredMessage': 'End Time is required', 'invalidMessage': '' },
                'PickerAndCalendarFrom': { 'isRequired': false, 'when': 'always', 'name': 'Date Time', 'type': '', 'requiredMessage': 'Clinic Date is required', 'invalidMessage': '' },


                //**********************  Pharmacist & Post Clinic Information ********************
                'txtNameOfRxHost': { 'isRequired': false, 'when': 'always', 'name': 'Name Of Rx Host', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': 'Name Of Rx Host: < > characters are not allowed' },
                'txtRxHostPhone': { 'isRequired': false, 'when': 'always', 'name': 'Phone number', 'type': 'phone', 'requiredMessage': '', 'invalidMessage': 'Valid Phone number is required(ex: ###-###-####)' },
                'txtFeedback': { 'isRequired': false, 'when': 'always', 'name': 'Feedback', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': 'Feed Back: < > characters are not allowed.' },
                'txtTotalHoursClinicHeld': { 'isRequired': false, 'when': 'always', 'name': 'TotalHours', 'type': 'decimal', 'requiredMessage': '', 'invalidMessage': 'Please enter the valid Total Hours Clinic Held' }
            
            };

            var first_identified_control = "";
            var is_valid = true;
            var ctrl_Id;
            var indexOf_Underscore;
            var trimmedValue;
            $.each(validating_fields, function (index, ctrl) {

                setValidControlCss($(ctrl), false);
                ctrl_Id = $(ctrl).attr('id');

                if ((ctrl_Id.indexOf('grdLocations') > -1) && (ctrl_Id.lastIndexOf('_') > -1)) {
                    is_required = !$("input[type=checkbox][id=" + ctrl_Id.substring(0, ctrl_Id.indexOf('_', ctrl_Id.indexOf('_') + 1)) + "_chkNoClinic]")[0].checked;
                    if (ctrl_Id.toLowerCase().indexOf('_picker') > -1) {
                        var pos = 0;
                        var count = 0;
                        while (true) {
                            pos = ctrl_Id.indexOf('_', pos + 1);
                            ++count;
                            if (count == 2) {
                                break;
                            }
                        }
                        ctrl_Id = ctrl_Id.substring(pos + 1, ctrl_Id.indexOf('_', pos + 1))
                    }
                    else {
                        indexOf_Underscore = ctrl_Id.lastIndexOf('_');
                        if (ctrl_Id.length > indexOf_Underscore + 1) {
                            ctrl_Id = ctrl_Id.substring(indexOf_Underscore + 1);
                        }
                    }
                    if (validating_field_names[ctrl_Id] != undefined && validating_field_names[ctrl_Id].isRequired != is_required && ctrl_Id != 'txtAddress2' && ctrl_Id != 'txtVouchersDistributed' && ctrl_Id != 'txtClinicStore' && ctrl_Id != 'txtTotalImm')
                        validating_field_names[ctrl_Id].isRequired = is_required;
                }
                trimmedValue = $.trim($(ctrl).val());
                if ((validating_field_names[ctrl_Id] != undefined) && validating_field_names[ctrl_Id].isRequired &&
                    (validating_field_names[ctrl_Id].when == 'always' || validating_field_names[ctrl_Id].when == validationGroup.toLower()) &&
                    trimmedValue == "") {

                    setInvalidControlCss($(ctrl), false);
                    if (ctrl_Id == 'PickerAndCalendarFrom')
                        $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                    else
                        $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });

                   if (first_identified_control == '')
                        first_identified_control = $(ctrl);
                    is_valid = false;
                }
                else if ((validating_field_names[ctrl_Id] != undefined) && trimmedValue != "") {
                    if (!validateData(validating_field_names[ctrl_Id].type, trimmedValue, (validating_field_names[ctrl_Id].name == 'Estimated Shots' || validating_field_names[ctrl_Id].name == 'Total Immunizations Administered') ? false : true)) {                    
                        setInvalidControlCss($(ctrl), false);
                       if (ctrl_Id == 'PickerAndCalendarFrom')
                            $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                        else
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].invalidMessage });

                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;
                    }
                    else if (validating_field_names[ctrl_Id].type == 'address') {
                        if (!(validatePO(trimmedValue))) {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": '<%= HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert") %>' });
                            if (first_identified_control == '')
                                first_identified_control = $(ctrl);
                            is_valid = false;
                        }
                    }
                }
            });

            if (!is_valid) {
                alert("Highlighted input fields are required/invalid. Please update and submit.");
                first_identified_control[0].focus();
                return false;
            }
            else {
                return true;
            }
        }

        function addTime(oldTime) {
            var time = oldTime.split(":");
            var hours = time[0];
            var ampm = time[1].substring(2, 4);
            var minutes = time[1].substring(0, 2);
            var time_new = '';
            if (+minutes >= 30) {
                hours = (+hours + 1) % 24;
            }
            minutes = (+minutes + 30) % 60;
            if (hours >= 12) {
                time_new = hours - 12 + ':' + minutes + 'pm';
            }
            else
                time_new = hours + ':' + minutes + ampm;
            return time_new;
        }

        function showMaintainContactLogWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "Reset contact status",
                    buttons: {
                        'Maintain Status': function () {
                            __doPostBack("maintainContactStatus", 1);
                            $(this).dialog('close');
                        },
                        'Confirmed': function () {
                            __doPostBack("maintainContactStatus", 0);
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function showStoreChangeWithContactStatusWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "Store change with contact status warning",
                    buttons: {
                        'Ok': function () {                           
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function removeCharityProgram() {
            var warning_mesg = '<%=(string)GetGlobalResourceObject("errorMessages", "deleteCharityProgramWarning") %>';
            $(function () {
                $("#divConfirmDialog").html(warning_mesg);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: "Remove Charity (HHS Voucher)",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("deleteCharityProgram");
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
            return false;
        }

        function disableAddLocations() {
            var is_disable = false;
            if ($("#hfDisableAddLocations").val() != "") {
                var store_message = $("#hfDisableAddLocations").val();
                is_disable = true;
                alert(store_message);
            }

            return !is_disable;
        }


        function showClinicDateReminder(alert, handler) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: "IMPORTANT REMINDER",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("ContinueSaving" + "," + handler, 1);
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            __doPostBack("ContinueSaving" + "," + handler, 0);
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
            return false;
        }
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdMaxClinicNumber" runat="server" />
    <asp:HiddenField ID="hfBusinessClinicPk" runat="server" />
    <asp:HiddenField id="hfisMOState" runat="server" Value="false" />
    <asp:HiddenField ID="hfDisableAddLocations" runat="server" />
    <asp:HiddenField ID="hfBusinessStoreId" runat="server" />
    <asp:HiddenField ID="hfNoClinic" runat="server" />
    <asp:HiddenField ID="hfIsCancelled" runat="server" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="center" style="padding-top: 24px; ">
                <table width="935" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="padding: 12px">
                            <table width="935" border="0" cellspacing="8" cellpadding="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: middle" class="formFields">
                                    	<asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="UpdateClinic" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="logTitle" style="text-align: left;  vertical-align: middle">
                                        <table style="text-align: left; width:100%">
                                            <tr>
                                                <td >Clinic Details</td>
                                                <td style="text-align: right;"">
                                                    <asp:ImageButton ID="imgbtnRemoveClinicAgreement" runat="server" ImageUrl="~/images/btn_remove_charity.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove_charity.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_charity_lit.png');"  OnClientClick="javascript:return removeCharityProgram();" />
                                                </td>   
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <table width="938" border="0" cellspacing="12" cellpadding="0" bgcolor="#d7ecc1"
                                            style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="3" align="left" class="subTitle">Store Information</td>
                                            </tr>
                                            <tr>
                                                <td width="125" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Assigned Store:</td>
                                                <td width="275" valign="middle" class="style3">
                                                    <asp:TextBox ID="txtDefaultClinicStoreId" runat="server" Enabled="false" CssClass="formFields" Width="100px" MaxLength="5" TabIndex="1"></asp:TextBox>
                                                </td>
                                                <td width="438" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">
                                                    <asp:CheckBox CssClass="formFields" ID="chkbxHhsProgram" runat="server" CausesValidation="false" Visible="true" Text="Charity (HHS Voucher) Clinic" Enabled="false" Checked="true" />
                                                </td>
                                            </tr>
                                            </table>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <table width="938" border="0" cellspacing="12" cellpadding="0" bgcolor="#c1dfec" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="5" align="left" class="subTitle">Business Information</td>
                                            </tr>
                                            <tr>
                                                <td width="125" align="left" valign="top" nowrap="nowrap" class="logSubTitles">Client Name:</td>
                                                <td width="275" style="text-align: left; vertical-align: top;" >
                                                    <b><asp:Label ID="lblClientName" runat="server" CssClass="formFields" ></asp:Label></b>
                                                </td>
                                                <td width="148" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">&nbsp;</td>
                                                <td width="280" valign="middle">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="125" style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact First Name:</td>
                                                <td width="350" style="text-align: left; vertical-align: top;">
                                                    <asp:TextBox ID="txtFirstContactName" runat="server" TabIndex="2" Width="200" CssClass="formFields" MaxLength="50"></asp:TextBox>                                                    
                                                </td>
                                                <td width="125" style="text-align: center; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact Job Title:</td>
                                                <td width="250" style="text-align: left; vertical-align: top;">
                                                    <asp:TextBox ID="txtContactJobTitle" runat="server" TabIndex="3" Width="200" CssClass="formFields" MaxLength="100"></asp:TextBox>
                                                </td>                                                                                                
                                            </tr>
                                            <tr>
                                                <td width="125" style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact Last Name:</td>
                                                <td width="275" style="text-align: left; vertical-align: top;">
                                                    <asp:TextBox ID="txtLastContactName" runat="server" TabIndex="4" Width="200" CssClass="formFields" MaxLength="50"></asp:TextBox>                                                    
                                                </td>
                                                <td></td>
                                                <td width="250" style="text-align: left; vertical-align: top;">
                                                    <asp:Label ID="lblLocalContactEmailPrimary" runat="server" CssClass="formFields" Visible="false" ></asp:Label>
                                                    <asp:Label ID="lblLocalContactPhonePrimary" runat="server" CssClass="formFields" Visible="false" ></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center">
                                        <table width="935" border="0" cellspacing="12" cellpadding="0" bgcolor="#f2eebb" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="4" align="left" class="formFields">
                                                    <asp:Label ID="lblClinicDateAlert" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" class="subTitle">Billing &amp; Vaccine Information</td>
                                            </tr>
                                            <tr>
                                                <td width="110" style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Plan ID:</td>
                                                <td width="375" style="text-align: left; vertical-align: top;">                                                    
                                                    <asp:TextBox ID="txtPlanId" MaxLength="256" runat="server" TabIndex="5" Width="350px" CssClass="formFields" Visible="false"></asp:TextBox>                                                    
                                                    <asp:Label ID="lblPlanId" runat="server" CssClass="formFields"></asp:Label>                                                    
                                                </td>
                                                <td width="75" rowspan="2" style="text-align: right; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Group ID:</td>
                                                <td width="250" rowspan="2" style="text-align: left; vertical-align: top;" >
                                                    <asp:TextBox ID="txtGroupId" MaxLength="256" runat="server" TabIndex="6" TextMode="MultiLine" Rows="4" Columns="45" Width="200px" CssClass="formFields" style="background-color: #FF0" Visible="false"></asp:TextBox>                                                    
                                                    <asp:Label ID="lblGroupId" runat="server" CssClass="formFields"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="110" style="text-align: left; vertical-align: top;" class="logSubTitles">ID Recipient:<br />
                                                    <span style="font-weight: normal; font-size: 11px;">(Cash and Corporate<br />to Invoice Claims)</span>
                                                </td>
                                                <td width="375" style="text-align: left; vertical-align: top;" >
                                                    <asp:TextBox ID="txtIdRecipient" CssClass="formFields" runat="server" Width="350px" TabIndex="7" Visible="false" MaxLength="256" style="background-color: #FF0"></asp:TextBox>
                                                    <asp:Label ID="lblIdRecipient" runat="server" CssClass="formFields"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" valign="middle" nowrap="nowrap">
                                                    <table border="0" width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; background: #fff; border-collapse: collapse; text-align: left;">
                                                        <thead>
                                                            <tr>
                                                                <th width="25%" align="left" class="clinicDetailsHeader" scope="col">Vaccines Covered</th>
                                                                <th width="25%" align="right" class="clinicDetailsHeader" scope="col">Vouchers Distributed</th>
                                                                <th width="25%" align="right" class="clinicDetailsHeader" scope="col">Estimated Volume</th>
                                                                <th width="25%" align="right" class="clinicDetailsHeader" scope="col">Total Administered</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="clinicDetailsRow" ><b>Standard – Trivalent</b></td>
                                                                <td style="text-align: right; vertical-align: top;" class="clinicDetailsRow">
                                                                    <asp:TextBox ID="txtDefaultVouchersDistributed" TabIndex="8" runat="server" CssClass="inputFieldRightAlign" Width="100px" MaxLength="5"></asp:TextBox>                                                                    
                                                                </td>
                                                                <td style="text-align: right; vertical-align: top;" class="clinicDetailsRow">
                                                                    <asp:TextBox ID="txtDefaultEstShots" TabIndex="9" runat="server" CssClass="inputFieldRightAlign" Width="100px" MaxLength="5"></asp:TextBox>                                                                    
                                                                </td>
                                                                <td style="text-align: right; vertical-align: top;" class="clinicDetailsRow">
                                                                    <asp:TextBox ID="txtDefaultTotalImm" TabIndex="10" runat="server" MaxLength="5" Width="100px" CssClass="inputFieldRightAlign"></asp:TextBox>                                                                    
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center">
                                    <asp:GridView Width="935" ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="grdLocations_RowDataBound" DataKeyNames="naClinicState,clinicDate,clinicScheduledOn">
                                            <Columns>
                                               <asp:BoundField DataField="naClinicState" Visible="false" />
                                               <asp:BoundField DataField="clinicDate" Visible="false" />
                                               <asp:BoundField DataField="clinicScheduledOn" Visible="false" />
                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="100%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#f0debc" style="border: 1px solid #999;">
                                                            <tr id="rowAddClinicLocation" runat="server">
                                                                <td style="text-align: left; vertical-align: top; font-family: Arial, Helvetica, Sans-Serif; font-size: 14px; font-weight: bold; padding-bottom: 10px; padding-top: 8px;" colspan="2">
                                                                    Clinic Information
                                                                </td>
                                                                <td style="text-align: left; vertical-align: top; padding-bottom: 10px; padding-top: 8px;" colspan="2">
                                                                    <asp:ImageButton ID="imgBtnAddLocation" ImageAlign="Right" ImageUrl="~/images/btn_add_location.png" CausesValidation="false" runat="server" 
                                                                        AlternateText="Remove Location" onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_location.png');"
                                                                        onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_location_lit.png');" OnClientClick="return disableAddLocations()" OnClick="btnAddLocation_Click" />                                                                  
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left; vertical-align: top;" align="left" width="125" class="logSubTitles">Clinic Location:</td>
                                                                <td width="275" style="text-align:left; vertical-align: top;" class="formFields">
                                                                    <asp:Label ID="lblClinicLocation" class="logSubTitles" Text='<%# Bind("naClinicLocation") %>' runat="server" Width="100%"></asp:Label>
                                                                </td>
                                                                <td colspan="2" align="left">
                                                                    <asp:CheckBox ID="chkNoClinic" runat="server" CssClass="logSubTitles" Text="No Clinic (Voucher Distribution Only)" Checked='<%# Convert.ToInt32(Eval("isNoClinic")) == 1 ? true : false %>' />
                                                                </td>
                                                                <td style="text-align: right; vertical-align: top;">
                                                                    <asp:ImageButton ID="imgBtnRemoveLocation" ImageAlign="Right" ImageUrl="images/btn_remove.png" CausesValidation="false" runat="server" 
                                                                        AlternateText="Remove Location" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');" 
                                                                        onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" OnClick="imgBtnRemoveLocation_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left; vertical-align: top;" width="125" class="logSubTitles">Contact First Name:</td>
                                                                <td style="text-align:left; vertical-align: top;" width="275">
                                                                    <asp:TextBox ID="txtContactFirstName" runat="server" CssClass="formFields" Width="200" MaxLength="50" Text='<%# Bind("naContactFirstName") %>'></asp:TextBox>                                                                    
                                                                </td>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Contact Phone:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtLocalContactPhone" runat="server" Text='<%# Bind("naClinicContactPhone") %>' CssClass="formFields" Width="200" MaxLength="14" onblur="textBoxOnBlur(this);"></asp:TextBox>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Contact Last Name:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtContactLastName" runat="server" CssClass="formFields" Width="200" MaxLength="50" Text='<%# Bind("naContactLastName") %>'></asp:TextBox>                                                                   
                                                                </td>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Contact Email:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtLocalContactEmail" runat="server" Text='<%# Bind("naContactEmail") %>' CssClass="formFields" Width="200" MaxLength="256" ></asp:TextBox>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Clinic Address1:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("naClinicAddress1") %>' CssClass="formFields" Width="200" MaxLength="256" ></asp:TextBox>                                                                    
                                                                </td>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Clinic Date:</td>
                                                                <td>
                                                                    <uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" />
                                                                    <asp:TextBox ID="txtCalenderFrom" runat="server" Visible="false" CssClass="formFields" Width="100px" ></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Clinic Address2:</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("naClinicAddress2") %>' CssClass="formFields" Width="200" MaxLength="256" ></asp:TextBox>                                                                    
                                                                </td>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Clinic Start Time:</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtStartTime" onpaste="return false" runat="server" Text='<%# Bind("naClinicStartTime") %>' CssClass="formFields" Width="100px" MaxLength="7" OnLoad="displayTime_Picker" ></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">City:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("naClinicCity") %>' CssClass="formFields" Width="200" MaxLength="50" ></asp:TextBox>                                                                    
                                                                </td>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Clinic End Time:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtEndTime" onpaste="return false" runat="server" Text='<%# Bind("naClinicEndTime") %>' CssClass="formFields" Width="100px" MaxLength="7" ></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="text-align:left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="text-align:left; vertical-align: top;" width="42%" >State:&nbsp;&nbsp;
                                                                                <asp:DropDownList ID="ddlState" runat="server" CssClass="formFields" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"></asp:DropDownList>
                                                                            </td>
                                                                            <td style="text-align:left; vertical-align: top;" width="58%">Zip Code:&nbsp;&nbsp;
                                                                                <asp:TextBox ID="txtZipCode" runat="server" CssClass="formFields" Width="100px" Text='<%# Bind("naClinicZip") %>' MaxLength="5" ></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td colspan="2" style="text-align:left; vertical-align: top;" class="logSubTitles">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblVouchersDistributed" runat="server">
                                                                        <tr>
                                                                            <td style="text-align:left; vertical-align: top;" width="37%" >Vouchers Distributed:</td>
                                                                            <td style="text-align:left; vertical-align: top;" width="61%">
                                                                                <asp:TextBox ID="txtVouchersDistributed" runat="server" CssClass="inputFieldRightAlign" Width="100px" Text='<%# Bind("naClinicVouchersDist") %>' MaxLength="5" ></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <%--<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblConfirmedClientName" runat="server">
                                                                        <tr>
                                                                        <td style="text-align:left; vertical-align: top;" width="35%" >Name of Client Individual<br />Who Confirmed the Clinic:</td>
                                                                        <td style="text-align:left; vertical-align: top;" width="66%">
                                                                            <asp:TextBox ID="txtConfirmedClientName" runat="server" Text='<%# Bind("confirmedClientName") %>' CssClass="formFields" Width="200px" MaxLength="100"></asp:TextBox>                                                                            
                                                                        </td>
                                                                        </tr>
                                                                    </table>--%>
                                                              </td>
                                                            </tr>
                                                            <tr id="rowEstimatedShots" runat="server">
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Estimated Shots:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtEstShots" runat="server" CssClass="inputFieldRightAlign" Width="100px" Text='<%# Bind("naClinicEstimatedVolume") %>' MaxLength="5" ></asp:TextBox>
                                                                </td>
                                                                <%--<td style="text-align:left; vertical-align: top;" class="logSubTitles">Vouchers Distributed:</td>
                                                                <td style="text-align:left; vertical-align: top;">
                                                                    <asp:TextBox ID="txtVouchersDistributed" runat="server" CssClass="inputFieldRightAlign" Width="100px" Text='<%# Bind("naClinicVouchersDist") %>' MaxLength="5"></asp:TextBox>
                                                                </td>--%>
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Total Immunizations<br />Administered:</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTotalImm" runat="server" MaxLength="5" Width="100px" CssClass="inputFieldRightAlign" Text='<%# Bind("naClinicTotalImmAdministered") %>' ></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <%--<tr id="rowTotalImmunizationsAdmin" runat="server">
                                                                <td style="text-align:left; vertical-align: top;" class="logSubTitles">Total Immunizations<br />Administered:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtTotalImm" runat="server" MaxLength="5" Width="100px" CssClass="inputFieldRightAlign" Text='<%# Bind("naClinicTotalImmAdministered") %>'></asp:TextBox>
                                                                </td>
                                                            </tr>--%>
                                                            <tr id="rowStoreAssignment" runat="server" visible="false">
                                                                <td colspan="4">
                                                                    <table border="0" width="100%" cellpadding="0" cellspacing="5">
                                                                        <tr>
                                                                            <td width="100%" colspan="4">
                                                                                <p class="subTitle" style="border-width: 0px 0px 1px 0px; border-style: solid;">Store Assignment</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="logSubTitles" align="left" width="43">Store:</td>
                                                                            <td align="left" width="275">
                                                                                <asp:TextBox ID="txtClinicStore" runat="server" CssClass="formFields" Width="100px" MaxLength="5" Text='<%# Bind("clinicStoreId") %>' ></asp:TextBox>
                                                                            </td>
                                                                            <td align="left"></td>
                                                                            <td align="left"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="rowClinicDateAlert" runat="server" visible="false" >
                                                                    <td colspan="4" class="formFields">
                                                                        <asp:Label ID="lblClinicDateAlert" runat="server" ForeColor="Red"></asp:Label>
                                                                    </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <table width="935" border="0" cellspacing="12" cellpadding="0" bgcolor="#f3d8d9" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="4" align="left" class="subTitle">Pharmacist &amp; Post Clinic Information</td>
                                            </tr>
                                            <tr>
                                                <td width="125" align="left" valign="top" nowrap="nowrap" class="logSubTitles">Name of Rx Host:</td>
                                                <td width="275" valign="middle" align="left" class="formFields">                                                    
                                                    <asp:TextBox ID="txtNameOfRxHost" runat="server" Width="200" CssClass="formFields" MaxLength="100"></asp:TextBox>                                                    
                                                </td>
                                                <td width="125" align="left" valign="top" nowrap="nowrap" class="logSubTitles">Total Hours Clinic Held:</td>
                                                <td valign="top" align="left">
                                                    <asp:TextBox ID="txtTotalHoursClinicHeld" runat="server" CssClass="formFields" MaxLength="5" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" width="125" nowrap="nowrap" class="logSubTitles">Phone #:</td>
                                                <td valign="top" class="formFields" align="left">
                                                    <asp:TextBox ID="txtRxHostPhone" runat="server" Width="200" MaxLength="15" CssClass="formFields" onblur="textBoxOnBlur(this);"></asp:TextBox>                                                    
                                                </td>
                                                <td width="125" align="left" valign="top" nowrap="nowrap" class="logSubTitles">Feedback/Notes:</td>
                                                <td width="275" valign="middle" class="formFields" align="left">
                                                    <asp:TextBox ID="txtFeedBack" CssClass="formFields" runat="server" TextMode="MultiLine" Columns="40" Rows="5" MaxLength="256"></asp:TextBox>
                                                    <table align="left" width="85%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left"><span style="font-size: x-small">*<asp:Label ID="idLabel" runat="server"></asp:Label> characters allowed</span></td>
                                                            <td align="right" class="logSubTitles"><asp:Label ID="idCount" runat="server"></asp:Label></td>
                                                        </tr>
                                                    </table>                                                    
                                                </td>
                                            </tr>
                                        </table>
                                       <%-- <asp:CustomValidator ID="ValidateClinicDetailsCV" runat="server" Display="None" OnServerValidate="ValidateLocations" ValidationGroup="UpdateClinic"></asp:CustomValidator> --%>                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <%--<asp:ImageButton ID="btnConfirmedClinicDim" ImageUrl="~/images/btn_confirmed_clinic_dim.png" CausesValidation="false" runat="server" AlternateText="Confirmed Clinic" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnConfirmedClinicDim" Text="Confirmed Clinic" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Confirmed Clinic" 
                                            OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnConfirmedClinic" ImageUrl="~/images/btn_confirmed_clinic.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_confirmed_clinic.png');"
                                             CausesValidation="true" ValidationGroup="UpdateClinic" onmouseover="javascript:MouseOverImage(this.id,'images/btn_confirmed_clinic_lit.png');"
                                             runat="server" AlternateText="Confirmed Clinic" CommandArgument="Confirmed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnConfirmedClinic" Text="Confirmed Clinic" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Confirmed Clinic" 
                                            CommandArgument="Confirmed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnClinicCompletedDim" ImageUrl="~/images/btn_clinic_completed_dim.png" CausesValidation="false" runat="server" AlternateText="Clinic Completed" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnClinicCompletedDim" Text="Clinic Completed" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Clinic Completed" 
                                            OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnClinicCompleted" ImageUrl="~/images/btn_clinic_completed.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_clinic_completed.png');"
                                             CausesValidation="true" ValidationGroup="UpdateClinic" onmouseover="javascript:MouseOverImage(this.id,'images/btn_clinic_completed_lit.png');"
                                             runat="server" AlternateText="Clinic Completed" CommandArgument="Completed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnClinicCompleted" Text="Clinic Completed" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Clinic Completed" 
                                            CommandArgument="Completed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnCancelClinicDim" ImageUrl="~/images/btn_cancel_clinic_dim.png" CausesValidation="false" runat="server" AlternateText="Cancelled Clinic" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnCancelClinicDim" Text="Cancel Clinic" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Cancelled Clinic" 
                                            OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnCancelClinic" ImageUrl="~/images/btn_cancel_clinic.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_clinic.png');" 
                                             CausesValidation="true" ValidationGroup="UpdateClinic" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_clinic_lit.png');"
                                             runat="server" AlternateText="Cancelled Clinic" CommandArgument="Cancelled" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnCancelClinic" Text="Cancel Clinic" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Cancelled Clinic" 
                                            CommandArgument="Cancelled" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnSubmit" ImageUrl="~/images/btn_submit_changes.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_changes.png');"
                                            CausesValidation="true" ValidationGroup="UpdateClinic" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_changes_lit.png');"
                                            runat="server" AlternateText="Submit" CommandArgument="Submit" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');" />&nbsp;--%>
                                        <asp:LinkButton ID="btnSubmit" Text="Submit Changes" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Submit" 
                                            CommandArgument="Submit" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');" />
                                        <asp:LinkButton ID="btnSubmitDim" Text="Submit Changes" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Submit" 
                                            OnClientClick="javascript: return false;" Visible="false"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnCancel" ImageUrl="~/images/btn_return_home.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_return_home.png');"
                                            CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_return_home_lit.png');"
                                            runat="server" AlternateText="Cancel" CommandArgument="Cancel" OnCommand="doProcess_Click" />--%>
                                        <asp:LinkButton ID="btnCancel" Text="Return To Home" CssClass="wagButton" CausesValidation="false" runat="server" AlternateText="Cancel" 
                                            CommandArgument="Cancel" OnCommand="doProcess_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <table width="935" border="0" cellspacing="12" cellpadding="0" bgcolor="#eeeeee" style="border: 1px solid #999;">
                                            <tr>
                                                <td width="400" colspan="4" align="left" class="subTitle">History Log</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="grdClinicUpdatesHistory" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdClinicUpdatesHistory_OnRowDataBound">
                                                         <Columns>
                                                             <asp:TemplateField HeaderText="">
                                                                <HeaderTemplate>
                                                                <tr>
                                                                    <td width="12%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Date</td>
                                                                    <td width="13%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Action</td>
                                                                    <td width="30%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Updated Field</td>
                                                                    <td width="30%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Update</td>
                                                                    <td width="15%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">User</td>
                                                                </tr>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                <tr>  
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblHistorydate" runat="server" Text='<% #Bind("updatedOn", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblAction" runat="server" Text='<%#Bind("updateAction") %>'></asp:Label>
                                                                    </td>
                                                                    <td id="tdClinicUpdateField" runat="server" class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdatedField" runat="server" Text='<%#Bind("updatedField") %>'></asp:Label>
                                                                    </td>
                                                                    <td id="tdClinicUpdateValue" runat="server" class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdate" runat="server" Text='<%#Bind("updatedValue") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Bind("updatedBy") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                </ItemTemplate>
                                                             </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>  
                                                           <table border="0" width="100%" >
                                                                <tr>
                                                                    <td width="12%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Date</td>
                                                                    <td width="13%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Action</td>
                                                                    <td width="18%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Updated Field</td>
                                                                    <td width="36%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Update</td>
                                                                    <td width="21%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">User</td>
                                                                </tr>
                                                                <tr>  
                                                                    <td colspan="5" class="clinicDetailsRow" style="text-align: center; vertical-align: middle; height:25px; ">No Data Found</td>
                                                                </tr>
                                                            </table> 
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    <div id="divConfirmDialog" style="display: none; font-family: Arial; font-size: 13px;
        text-align: left;">
    </div>
    </form>
</body>
</html>
