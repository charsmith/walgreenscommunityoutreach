﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensFAQs.aspx.cs" Inherits="walgreensFAQs" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="javaScript/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <style type="text/css">
        .linkColor a:link
        {
            color: #3096D8;
        }
        .linkColor a:visited
        {
            color: #3096D8;
        }
        .linkColor a:hover
        {
            color: #3096D8;
        }
        .linkColor a:active
        {
            color: #3096D8;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0" class="linkColor">
                    <tr>
                        <td valign="top" class="pageTitle">
                            FAQs
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table width="890" border="0" cellspacing="20" cellpadding="0">
                                <tr>
                                    <td width="562" class="bestPracticesText">
                                        <span class="class4"><a href="#businessGrid" class="class4">Which businesses should
                                            I contact?</a></span><br />
                                        <span class="class4"><a href="#contactLogger" class="class3">How do I log a contact
                                            I've made?</a></span><br />
                                        <span class="class4"><a href="#contactLog" class="class3">How do I view past contacts
                                            I've logged?</a></span><br />
                                        <span class="class4"><a href="#noBusinesses" class="class4">What do I do if no businesses
                                            have been assigned to my store for an outreach program?</a></span><br />
                                        <span class="class4"><a href="#viewStores" class="class4">How do I view other Walgreens
                                            stores in my District or Community?</a></span><br />
                                        <span class="class4"><a href="#addBusinesses" class="class4">How do I add a business
                                            that I want to contact that is not one of my assigned businesses?</a></span><br />
                                        <span class="class4"><a href="#addOutreach" class="class4">How do I add a business from
                                            one Outreach Campaign to another of my Outreach Campaigns?</a></span><br />
                                        <span class="class4"><a href="#multipleEfforts" class="class4">Why do some of my businesses
                                            have more than one contact listed in the grid?</a></span><br />
                                    </td>
                                    <td width="268" align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/outreachPortal.jpg" width="268" height="202" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="businessGrid" id="businessGrid"></a>Which businesses should I contact?</b></p>
                                        <p>
                                            Your store is responsible for contacting businesses listed on the <a href="walgreensHome.aspx">
                                                Home</a> page of the outreach program. These businesses appear in a grid along
                                            with all of the businesses pertinent contact information. Clicking the <a href="walgreensMap.aspx">
                                                Map</a> button in the navigation bar will allow you to view the businesses locations
                                            in reference to your store. You may also get a listing of your businesses by running
                                            the <a href="reports/walgreensReport.aspx">Store Business Report</a> located under
                                            Reports in the navigation bar.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/grid.jpg" width="268" height="135" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="contactLogger" id="contactLogger"></a>How do I log a contact I've made?</b></p>
                                        <p>
                                            To log a recent contact, use the Log Business Contact form on the right of the <a
                                                href="walgreensHome.aspx">Home</a> or <a href="walgreensContactLog.aspx">Contact Log</a>
                                            pages. Select the business you contacted from the Business Contacted drop down menu.
                                            This will populate the Contact Name fields with any available information. You may
                                            update the Contact Name if the listed name is different than the appropriate contact
                                            at that business. Select the correct Contact Date if it is different than the current
                                            date. Select an Outreach Status based on the results of your outreach call. Enter
                                            any Feedback or Notes that may aid you or your associates in future calls. Finally
                                            click the Submit Entry button to complete your log. A success message will appear
                                            to let you know that your log has been added to the Contact Log. From the <a href="walgreensContactLog.aspx">
                                                Contact Log</a> page, you may view the entire log for each business or remove
                                            an individual log if you have submitted a log incorrectly.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/contactLogger.jpg" width="145" height="236" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="contactLog" id="contactLog"></a>How do I view past contacts I've logged?</b></p>
                                        <p>
                                            Past log entries can be viewed on the <a href="walgreensContactLog.aspx">Contact Log</a>
                                            page. The screen will remain blank, until you select a business from the drop down
                                            menu in the header. Once you've selected a business the dates for each log entry
                                            will appear below the business name. Each date can be toggled open to show the contact
                                            information from that log entry. Some status may contain a link to open a secondary
                                            form if applicable to the status. If a log entry was made incorrectly they can be
                                            removed by clicking the Remove Entry link below the log entry.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/contactLog.jpg" width="268" height="138" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="noBusinesses" id="noBusinesses"></a>What do I do if no businesses have been
                                                assigned to my store for an outreach program?</b></p>
                                        <p>
                                            If you have relationships with local businesses that you want to contact as part
                                            of the outreach campaign, you will still have access to the site. If you have relationships
                                            with local businesses that you contact, you may attempt to add those businesses
                                            to the site. On the <a href="walgreensHome.aspx">Home</a> page there is a link above
                                            the business grid to Search/Add A Business. The link opens a form to allow you to
                                            enter the business information for the business you want to add. Fill out the form
                                            and click Submit. The system will do a quick search through existing businesses
                                            in the site and either add the business to your list or deny the add if the business
                                            is already assigned to another Walgreens store. If added the business will now appear
                                            in the business grid of the home page.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/addBusiness.jpg" width="268" height="193" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="viewStores" id="viewStores"></a>How do I view other Walgreens stores in
                                                my Region, District or Community?</b></p>
                                        <p>
                                            Region, District and Community users who have multiple stores associated with their
                                            login link can view other Walgreens stores associated to them by using the drop
                                            down menu labeled Store Name below the navigation bar. When you've selected a new
                                            store the store id number and address will appear below the outreach name in the
                                            header of the page. The same drop down menu of store locations will be found within
                                            some of the reports that apply to individual store locations.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/storeNameMenu.jpg" width="268" height="68" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="addBusinesses" id="addBusinesses"></a>How do I add a business that I want
                                                to contact that is not one of my assigned businesses?</b></p>
                                        <p>
                                            If you have relationships with local businesses that you want to contact as part
                                            of the outreach campaign, you may attempt to add those businesses to the site. On
                                            the <a href="walgreensHome.aspx">Home</a> page there is a link above the business
                                            grid to <a href="walgreensStoreBusiness.aspx">Search/Add A Business</a>. The link
                                            opens a form to allow you to enter the business information for the business you
                                            want to add. Fill out the form and click Submit. The system will do a quick search
                                            through existing businesses in the site and either add the business to your list
                                            or deny the add if the business is already assigned to another Walgreens store.
                                            If added the business will now appear in the business grid of the home page.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/addBusiness.jpg" alt="" width="268" height="193" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="addOutreach" id="addOutreach"></a>How do I add a business from one Outreach
                                                Campaign to another of my Outreach Campaigns?</b></p>
                                        <p>
                                            If you have a relationship with a local business from another Outreach Campaign
                                            and would like to extend that relationship to other Outreach Campaigns, you can
                                            add the other Outreach Program through the <a href="walgreensEditBusinessProfile.aspx">
                                                Edit Business Profiles</a> screen. Edit the business profile in the Outreach
                                            Campaign in which the business is already assigned to your store. Once you have
                                            navigated to the View/Edit Business Profiles screen, select the business you want
                                            to add from the &quot;Local Business&quot; dropdown menu and select &quot;Outreach
                                            Campaigns&quot; from the &quot;Edit&quot; menu. Once selected, the form below will
                                            fill with all of the data about that business. The Outreach Campaigns will be displayed
                                            in the &ldquo;Outreach Campaigns&quot; section at the bottom of the page. If a campaign
                                            is checked, it is already assigned to the &quot;Store&quot; that is shown to the
                                            right of the campaign name. If it is already checked, you will not be able to add
                                            this business to your list of assigned businesses. If it is unchecked, you may check
                                            the Outreach Campaign. The campaign will be assigned to your store when you click
                                            the &quot;Submit&quot; button to complete the update.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/addOutreach.jpg" alt="" width="268" height="219" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="multipleEfforts" id="multipleEfforts"></a>Why do some of my businesses have
                                                more than one contact listed in the grid?</b></p>
                                        <p>
                                            You may notice that some businesses have multiple contacts, contact dates and statuses.
                                            These occur when a business is associated with more than one outreach campaign.
                                            If you see that a business has recently been contacted for another outreach campaign,
                                            you may want to postpone contacting them immediately about a different outreach
                                            campaign. Only the logs for the current outreach campaign may be affected, which
                                            is why the other contacts are grayed out. You will however be able to view all past
                                            logs for all outreach campaigns in the <a href="walgreensContactLog.aspx">Contact Log</a>.
                                            FI is the acronym for Flu Immunization and SO is the acronym for Senior Outreach.
                                            If you hold the cursor over the link it will also provide the full label.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/multipleEfforts.jpg" width="268" height="109" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bestPracticesText">
                                        Any additional questions, please email <span class="class3"><a href="mailto:MedicarePart-D@Walgreens.com"
                                            class="class3">MedicarePart-D@Walgreens.com</a></span>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
