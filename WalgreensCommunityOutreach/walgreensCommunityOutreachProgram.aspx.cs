﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using System.IO;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Text;
using TdWalgreens;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public partial class walgreensCommunityOutreachProgram : Page
{
    #region --------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk == 0)
                Response.Redirect("walgreensNoAccess.htm");

            this.hfcontactLogPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk.ToString();
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = 0;
            string store_state = this.commonAppSession.SelectedStoreSession.storeState;
            //Checking if the store is MO or DC 
            if (!string.IsNullOrEmpty(store_state) && (store_state == "MO" || store_state == "DC"))
                this.isMOState = true;
            else
                this.isMOState = false;

            this.addClinicLocation(true);
            this.bindCOProgram();
            Session["identifierString"] = " ContactLogPk: " + this.hfcontactLogPk.Value;
        }
        else
        {
            if (!string.IsNullOrEmpty(this.hfcontactLogPk.Value))
                this.contactLogPk = Convert.ToInt32(this.hfcontactLogPk.Value);

            string event_target = Request["__EVENTTARGET"];
            if (event_target.ToLower() == "confirmimmunization")
            {
                bool confirm_immunization = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                if (!confirm_immunization)
                {
                    if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
                    {
                        this.grdImmunizationChecks.FooterRow.Visible = false;
                        this.grdImmunizationChecks.ShowFooter = false;

                        this.imgBtnAddImmunization.Enabled = true;
                        this.lnkAddImmunization.Enabled = true;
                        IPostBackEventHandler schedule_clinic = (IPostBackEventHandler)this.btnScheduleClinic;
                        schedule_clinic.RaisePostBackEvent(string.Empty);
                    }
                }
                else
                {
                    ImageButton img_immunization = null;
                    if (this.grdImmunizationChecks.Rows.Count == 0)
                        img_immunization = (ImageButton)grdImmunizationChecks.Controls[0].Controls[0].FindControl("imgBtnImmunizationOk");
                    else
                        img_immunization = (ImageButton)grdImmunizationChecks.FooterRow.FindControl("imgBtnImmunizationOk");

                    this.imgBtnImmunizationOk_Click(img_immunization, EventArgs.Empty);
                }
            }
            else if (event_target.ToLower() == "continuesending")
            {
                string confirmation_type = (Request["__EVENTARGUMENT"]).ToString();
                if (confirmation_type == "1")
                {
                    this.ValidationsHandler(true, true);
                }
                else
                {
                    if (this.isClinicAfter2Weeks)
                    {
                        string clinic_date_reminder_alert = "coClinicDateReminderAfter2Weeks";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueReminder');", true);
                        //this.isClinicAfter2Weeks = false;
                    }
                    else
                    {
                        this.prepareClinicLocationsXML(-1);
                        // this.doProcess();
                        this.ValidationsHandler(true, false);
                    }
                }
            }
            else if (event_target.ToLower() == "continuereminder")
            {
                this.prepareClinicLocationsXML(-1);
                this.doProcess();
            }
            this.setMinMaxDates();
        }

        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void btnScheduleClinic_Click(object sender, ImageClickEventArgs e)
    {
        if (this.ValidateProgram())
        {
            Int32.TryParse(this.hfcontactLogPk.Value, out this.contactLogPk);
            if (this.contactLogPk > 0)
            {
                this.prepareClinicLocationsXML(-1);
                this.ValidationsHandler(false, false);
            }
            else
                Response.Redirect("walgreensHome.aspx");
        }
        else
        {
            this.prepareClinicLocationsXML(-1);
            this.bindClinicLocations();
        }
    }

    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("walgreensHome.aspx");
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        this.addClinicLocation(false);
        this.bindClinicLocations();
    }

    protected void imgBtnRemoveLocation_Click(object sender, EventArgs e)
    {
        ImageButton img_btn_remove = (ImageButton)sender;
        GridViewRow grd_row = (GridViewRow)img_btn_remove.NamingContainer;

        this.prepareClinicLocationsXML(grd_row.RowIndex);
        this.bindClinicLocations();
    }

    protected void grdLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddl_states = (DropDownList)e.Row.FindControl("ddlState");
            RadioButtonList rbtn_outreach_Types = (RadioButtonList)e.Row.FindControl("rbtnOutreachType");
            string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate");
            string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate");
            if (Convert.ToDateTime(restriction_start_date) < DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
            {
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString(), ApplicationSettings.getStoreStateRestrictions("storeState"));
            }
            else
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString(), "");
            ddl_states.SelectedValue = grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString();

            this.bindOutreachTypeDefaultFields(rbtn_outreach_Types);
            rbtn_outreach_Types.SelectedValue = grdLocations.DataKeys[e.Row.RowIndex].Values["coOutreachTypeId"].ToString();
            if (e.Row.RowIndex == 0)
            {
                ImageButton btn_remove_location = (ImageButton)e.Row.FindControl("imgBtnRemoveLocation");
                btn_remove_location.Visible = false;
            }

            if (this.dtImmunizations.Select("isSelected = 'True'").Count() > 0)
            {
                GridView grd_clinic_immunizations = (GridView)e.Row.FindControl("grdClinicImmunizations");
                this.bindClinicImmunizations(grd_clinic_immunizations, e.Row.RowIndex);
            }

            //disabling address fields if state is MO or DC and for Previous clinics 
            if (isAddressDisabled)
            {
                ((TextBox)e.Row.FindControl("txtAddress1")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtAddress2")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtCity")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtZipCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlState")).Enabled = false;
            }
            //var date_control = e.Row.FindControl("PickerAndCalendarFrom");
            this.setClinicDates(e.Row, true);
        }
    }

    protected void imgBtnAddImmunization_Click(object sender, EventArgs e)
    {
        this.updatePaymentTypeData();
        this.prepareClinicLocationsXML(-1);
        this.bindImmunizationChecks();
        this.bindClinicLocations();
        this.grdImmunizationChecks.ShowFooter = true;
        this.grdImmunizationChecks.FooterRow.Visible = true;
        if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
        {
            DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
            DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");
            this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
            this.imgBtnAddImmunization.Enabled = false;
            this.lnkAddImmunization.Enabled = false;
        }
    }
    /// <summary>
    /// time picker at server side
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void displayTime_Picker(object sender, EventArgs e)
    {
        StringBuilder script_text = new StringBuilder();
        GridViewRow gvr = (GridViewRow)((sender as TextBox).NamingContainer);
        TextBox txt_end_time = new TextBox();
        txt_end_time = (TextBox)gvr.FindControl("txtEndTime");

        script_text = ApplicationSettings.displayTimePicker((sender as TextBox).ClientID, txt_end_time.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DateScript" + gvr.RowIndex, script_text.ToString(), true);
    }

    /// <summary>
    /// To handle the ddlstate selection changed event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        // to clear the date in picker if we select MO or DC state by non admin user
        //calling bind clinic to set min dates dates for date pickers
        DropDownList ddl_state_clinic_location = (DropDownList)sender;
        if (ddl_state_clinic_location != null)
        {
            GridViewRow row = (GridViewRow)ddl_state_clinic_location.Parent.Parent;
            if (row != null)
            {
                var date_control = row.FindControl("PickerAndCalendarFrom");
                DateTime seleted_date = ((PickerAndCalendar)date_control).getSelectedDate;

                if ((ddl_state_clinic_location.SelectedValue == "MO" || ddl_state_clinic_location.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin && seleted_date < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Parse("01/01/0001");
                }
            }
        }

        this.prepareClinicLocationsXML(-1);
        this.bindClinicLocations();
    }
    protected void imgBtnImmunizationOk_Click(object sender, EventArgs e)
    {
        ImageButton img_immunization = (ImageButton)sender;
        GridViewRow grd_immunization_row = (GridViewRow)img_immunization.NamingContainer;
        DropDownList ddl_immunization = (DropDownList)grd_immunization_row.FindControl("ddlImmunizationCheck");
        DropDownList ddl_payment_method = (DropDownList)grd_immunization_row.FindControl("ddlPaymentType");
        if (ddl_payment_method.SelectedValue == "" && hfUnconfirmedPayment.Value != "")
        {
            ddl_payment_method.SelectedIndex = ddl_payment_method.Items.IndexOf(ddl_payment_method.Items.FindByValue(hfUnconfirmedPayment.Value));
            hfUnconfirmedPayment.Value = "";
        }
        this.updatePaymentTypeData();
        this.prepareClinicLocationsXML(-1);

        if (ddl_immunization.SelectedIndex != 0 && ddl_payment_method.SelectedIndex != 0)
        {
            DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + ddl_immunization.SelectedValue + " AND paymentTypeId = " + ddl_payment_method.SelectedValue + " AND isSelected = 'False' AND isPaymentSelected = 'False'");
            if (dr_immunization != null && dr_immunization.Count() > 0)
            {
                dr_immunization[0]["isSelected"] = "True";
                dr_immunization[0]["isPaymentSelected"] = "True";

                XmlNodeList clinic_locations = this.xmlCOProgram.SelectNodes("ClinicAgreement/Clinics/clinic");
                foreach (XmlElement clinic in clinic_locations)
                {
                    XmlElement est_shots = this.xmlCOProgram.CreateElement("immunization");
                    if (clinic.SelectSingleNode("./immunization[@pk = '" + ddl_immunization.SelectedValue + "' and @paymentTypeId = '" + ddl_payment_method.SelectedValue + "']") == null)
                    {
                        est_shots.SetAttribute("pk", ddl_immunization.SelectedValue);
                        est_shots.SetAttribute("paymentTypeId", ddl_payment_method.SelectedValue);
                        est_shots.SetAttribute("estimatedQuantity", string.Empty);
                        clinic.AppendChild(est_shots);
                    }
                }
            }

            if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                this.grdImmunizationChecks.ShowFooter = false;
                this.grdImmunizationChecks.FooterRow.Visible = false;
            }
            else if (this.grdImmunizationChecks.ShowFooter)
                this.grdImmunizationChecks.ShowFooter = false;
        }

        this.bindImmunizationChecks();
        this.bindClinicLocations();

    }
    protected void imgBtnRemoveImmunization_Click(object sender, CommandEventArgs e)
    {
        GridViewRow grid_row = (GridViewRow)((ImageButton)sender).NamingContainer;
        if (e.CommandArgument.ToString().ToLower() == "newimmunization")
        {
            if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                this.grdImmunizationChecks.FooterRow.Visible = false;
                this.grdImmunizationChecks.ShowFooter = false;

                this.imgBtnAddImmunization.Enabled = true;
                this.lnkAddImmunization.Enabled = true;
            }
            this.updatePaymentTypeData();
            this.prepareClinicLocationsXML(-1);
            this.bindClinicLocations();
        }
        else
        {
            Label immunization_pk = (Label)this.grdImmunizationChecks.Rows[grid_row.RowIndex].FindControl("lblImmunizationPk");
            Label payment_pk = (Label)this.grdImmunizationChecks.Rows[grid_row.RowIndex].FindControl("lblPaymentTypeId");

            if (!string.IsNullOrEmpty(immunization_pk.Text))
            {
                //this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
                DataRow[] immunization_selected = this.dtImmunizations.Select("immunizationId = '" + immunization_pk.Text + "' AND paymentTypeId = '" + payment_pk.Text + "'");
                if (immunization_selected.Count() > 0)
                {
                    immunization_selected[0]["isSelected"] = "False";
                    immunization_selected[0]["isPaymentSelected"] = "False";

                    //Clear previous data in the payment method
                    if (Convert.ToInt32(immunization_selected[0]["paymentTypeId"]) == 6)
                    {
                        immunization_selected[0]["name"] = null;
                        immunization_selected[0]["address1"] = null;
                        immunization_selected[0]["address2"] = null;
                        immunization_selected[0]["city"] = null;
                        immunization_selected[0]["state"] = null;
                        immunization_selected[0]["zip"] = null;
                        immunization_selected[0]["phone"] = null;
                        immunization_selected[0]["email"] = null;
                        immunization_selected[0]["tax"] = null;
                        immunization_selected[0]["copay"] = null;
                        immunization_selected[0]["copayValue"] = null;
                    }
                }
            }

            if (this.grdImmunizationChecks.FooterRow == null)
            {
                this.imgBtnAddImmunization.Enabled = true;
                this.lnkAddImmunization.Enabled = true;
            }
            else if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
                DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");
                this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
            }

            this.updatePaymentTypeData();
            this.prepareClinicLocationsXML(-1);
            this.bindImmunizationChecks();
            this.bindClinicLocations();
        }
    }
    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationPk");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");

            DataRow[] row_payment_types = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(lbl_immunization_id.Text) + " AND paymentTypeId = " + Convert.ToInt32(lbl_payment_type_id.Text) + " AND isSelected = 'True' AND isPaymentSelected = 'True'");
            if (row_payment_types.Count() > 0)
            {
                if (Convert.ToInt32(row_payment_types[0]["paymentTypeId"]) == 6)
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice_to = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowSendInvoiceTo");
                    DropDownList ddl_tax_exempt = (DropDownList)e.Row.FindControl("ddlTaxExempt");
                    DropDownList ddl_iscopay = (DropDownList)e.Row.FindControl("ddlIsCopay");
                    DropDownList ddl_payment_state = (DropDownList)e.Row.FindControl("ddlPmtState");
                    row_send_invoice_to.Visible = true;
                    ddl_payment_state.bindStates();

                    if (ddl_payment_state.Items.FindByValue("" + row_payment_types[0]["state"].ToString() + "") != null)
                        ddl_payment_state.Items.FindByValue("" + row_payment_types[0]["state"].ToString() + "").Selected = true;

                    if (ddl_tax_exempt.Items.FindByValue("" + row_payment_types[0]["tax"].ToString() + "") != null)
                        ddl_tax_exempt.Items.FindByValue("" + row_payment_types[0]["tax"].ToString() + "").Selected = true;

                    if (ddl_iscopay.Items.FindByValue("" + row_payment_types[0]["copay"].ToString() + "") != null)
                        ddl_iscopay.Items.FindByValue("" + row_payment_types[0]["copay"].ToString() + "").Selected = true;

                    lbl_price.Text = "$ " + row_payment_types[0]["directBillPrice"].ToString();
                }
                else
                    lbl_price.Text = (row_payment_types[0]["price"] == DBNull.Value) ? "N/A" : "$ " + row_payment_types[0]["price"].ToString();
            }
        }

    }
    protected void grdClinicImmunizations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string immunization_key = "immunizationName";
            string payment_type_key = "paymentTypeName";
            Label lbl_clinic_immunization = (Label)e.Row.FindControl("lblClinicImmunization");
            Label lbl_clinic_payment_type = (Label)e.Row.FindControl("lblClinicPaymentType");
            lbl_clinic_immunization.Text = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblImmunizationId")).Text))[0][immunization_key].ToString();
            lbl_clinic_payment_type.Text = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblImmunizationId")).Text) + " AND paymentTypeId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblPaymentTypeId")).Text))[0][payment_type_key].ToString();
        }

    }
    #endregion

    #region --------------- PRIVATE METHODS ---------------
    private void bindCOProgram()
    {
        DataSet ds_contract_agreement = new DataSet();
        ds_contract_agreement = dbOperation.getClinicAgreement("Walgreens", this.contactLogPk.ToString(), this.commonAppSession.LoginUserInfoSession.Email.ToString(), 2015, true);

        //Store Immunization checks and payment types in session
        this.dtImmunizations = ds_contract_agreement.Tables[1];
        this.bindClinicLocations();
        this.bindImmunizationChecks();

    }
    /// <summary>
    /// Adds new clinic location
    /// </summary>
    /// <param name="is_default"></param>
    private void addClinicLocation(bool is_default)
    {
        double clinics_count = 0;
        Int32.TryParse(this.hfcontactLogPk.Value, out this.contactLogPk);
        XmlElement clinic_locations_node;
        if (is_default)
            clinic_locations_node = this.xmlCOProgram.CreateElement("Clinics");
        else
        {
            this.prepareClinicLocationsXML(-1);

            clinic_locations_node = (XmlElement)this.xmlCOProgram.SelectSingleNode("ClinicAgreement/Clinics");
        }
        clinics_count = (is_default) ? 0 : this.grdLocations.Rows.Count;

        XmlElement clinic_ele = this.xmlCOProgram.CreateElement("clinic");
        string address1, address2, city, state, zip;
        address1 = string.Empty;
        address2 = string.Empty;
        city = string.Empty;
        state = string.Empty;
        zip = string.Empty;
        this.isRestrictedStoreState = ApplicationSettings.isRestrictedStoreState(this.commonAppSession.SelectedStoreSession.storeState, this.commonAppSession.LoginUserInfoSession.UserRole);
        if (is_default || isRestrictedStoreState)
        {
            DataTable dt_business_contact = dbOperation.getBusinessContactDetails(this.contactLogPk);
            address1 = (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address"].ToString() : string.Empty;
            address2 = (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address2"].ToString() : string.Empty;
            city = (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["city"].ToString() : string.Empty;
            state = (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["state"].ToString() : string.Empty;
            zip = (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["zip"].ToString() : string.Empty;
            this.hfBusinessName.Value = (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["businessName"].ToString() : string.Empty;
            isAddressDisabled = this.isRestrictedStoreState;
        }
        clinic_ele.SetAttribute("clinicLocation", ("CLINIC LOCATION ") + ((clinics_count >= 26) ? ((Char)(65 + (clinics_count % 26 == 0 ? Math.Ceiling(clinics_count / 26) - 1 : Math.Ceiling(clinics_count / 26) - 2))).ToString() + "" + ((Char)(65 + (clinics_count % 26))).ToString() : ((Char)(65 + clinics_count % 26)).ToString()));
        clinic_ele.SetAttribute("localContactName", string.Empty);
        clinic_ele.SetAttribute("LocalContactEmail", string.Empty);
        clinic_ele.SetAttribute("LocalContactPhone", string.Empty);
        clinic_ele.SetAttribute("clinicDate", string.Empty);
        clinic_ele.SetAttribute("startTime", string.Empty);
        clinic_ele.SetAttribute("endTime", string.Empty);
        clinic_ele.SetAttribute("Address1", address1);
        clinic_ele.SetAttribute("Address2", address2);
        clinic_ele.SetAttribute("city", city);
        clinic_ele.SetAttribute("state", state);
        clinic_ele.SetAttribute("zipCode", zip);
        clinic_ele.SetAttribute("coOutreachTypeId", string.Empty);
        clinic_ele.SetAttribute("coOutreachTypeDesc", string.Empty);
        //Add selected immunizations to the new clinic location
        if (!is_default)
        {
            XmlNodeList clinic_immunizations = clinic_locations_node.FirstChild.ChildNodes;

            foreach (XmlElement immunization in clinic_immunizations)
            {
                XmlElement estshots_node;
                estshots_node = this.xmlCOProgram.CreateElement("immunization");
                estshots_node.SetAttribute("pk", immunization.Attributes["pk"].Value);
                estshots_node.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
                estshots_node.SetAttribute("estimatedQuantity", string.Empty);
                clinic_ele.AppendChild(estshots_node);
            }
        }

        clinic_locations_node.AppendChild(clinic_ele);
        if (is_default)
            this.xmlCOProgram.AppendChild(clinic_locations_node);
        else
            this.xmlCOProgram.SelectSingleNode("ClinicAgreement").AppendChild(clinic_locations_node);
    }

    /// <summary>
    /// Binds clinic locations to the grid
    /// </summary>
    private void bindClinicLocations()
    {
        StringReader location_reader = new StringReader(this.xmlCOProgram.SelectSingleNode("//Clinics").OuterXml);
        DataSet location_dataset = new DataSet();
        location_dataset.ReadXml(location_reader);

        this.lblClinicDateAlert.Text = string.Format((string)GetGlobalResourceObject("errorMessages", "clinicDateAlertBefore2WeeksEN"));
        if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
            this.lblClinicDateAlert.Visible = true;

        grdLocations.DataSource = location_dataset.Tables[0];
        grdLocations.DataBind();
    }

    /// <summary>
    /// Prepares clinic locations XML
    /// </summary>
    /// <param name="remove_location_id"></param>
    private void prepareClinicLocationsXML(int remove_location_id)
    {
        this.updatePaymentTypeData();
        RadioButtonList rbtn_outreach_type;
        this.xmlCOProgram = new XmlDocument();
        XmlElement co_program_ele = this.xmlCOProgram.CreateElement("ClinicAgreement");
        XmlDocument xdoc_clinic_locations = new XmlDocument();
        XmlElement clinic_locations = xdoc_clinic_locations.CreateElement("Clinics");
        string contact_name = string.Empty;
        double clinic_location_count = 0;
        XmlElement estshots_node, location_node;
        DataRow[] dr_immunization;
        foreach (GridViewRow row in grdLocations.Rows)
        {
            if (row.RowIndex != remove_location_id)
            {
                rbtn_outreach_type = (RadioButtonList)row.FindControl("rbtnOutreachType");
                location_node = xdoc_clinic_locations.CreateElement("clinic");
                location_node.SetAttribute("clinicLocation", "CLINIC LOCATION " + (clinic_location_count >= 26 ? ((Char)(65 + (clinic_location_count % 26 == 0 ? Math.Ceiling(clinic_location_count / 26) - 1 : Math.Ceiling(clinic_location_count / 26) - 2))).ToString() + "" + ((Char)(65 + (clinic_location_count % 26))).ToString() : ((char)(65 + clinic_location_count % 26)).ToString()));
                contact_name = ((TextBox)row.FindControl("txtLocalContactName")).Text;
                location_node.SetAttribute("localContactName", contact_name.Trim());

                location_node.SetAttribute("LocalContactPhone", ((TextBox)row.FindControl("txtLocalContactPhone")).Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim());
                location_node.SetAttribute("LocalContactEmail", ((TextBox)row.FindControl("txtLocalContactEmail")).Text);
                location_node.SetAttribute("Address1", ((TextBox)row.FindControl("txtAddress1")).Text);
                location_node.SetAttribute("Address2", ((TextBox)row.FindControl("txtAddress2")).Text);
                location_node.SetAttribute("city", ((TextBox)row.FindControl("txtCity")).Text);
                location_node.SetAttribute("state", ((DropDownList)row.FindControl("ddlState")).SelectedValue);
                location_node.SetAttribute("zipCode", ((TextBox)row.FindControl("txtZipCode")).Text);
                if (((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                    location_node.SetAttribute("clinicDate", ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString());
                else
                    location_node.SetAttribute("clinicDate", "");
                location_node.SetAttribute("startTime", ((TextBox)row.FindControl("txtStartTime")).Text);
                location_node.SetAttribute("endTime", ((TextBox)row.FindControl("txtEndTime")).Text);
                location_node.SetAttribute("coOutreachTypeId", rbtn_outreach_type.SelectedValue);
                location_node.SetAttribute("coOutreachTypeDesc", (!string.IsNullOrEmpty(rbtn_outreach_type.SelectedValue) && rbtn_outreach_type.SelectedValue.ToLower() == "4") ? ((TextBox)row.FindControl("txtOther")).Text.Trim().Replace("'", "''") : null);

                GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
                if (grd_clinic_immunizations.Rows.Count > 0)
                {
                    foreach (GridViewRow clinic_immunization in grd_clinic_immunizations.Rows)
                    {
                        dr_immunization = this.dtImmunizations.Select("immunizationId = " + ((Label)clinic_immunization.FindControl("lblImmunizationId")).Text + " AND paymentTypeId = " + ((Label)clinic_immunization.FindControl("lblPaymentTypeId")).Text + " AND isSelected = 'True' AND isPaymentSelected = 'True'");
                        if (dr_immunization.Count() > 0)
                        {
                            estshots_node = xdoc_clinic_locations.CreateElement("immunization");
                            estshots_node.SetAttribute("pk", ((Label)clinic_immunization.FindControl("lblImmunizationId")).Text);
                            estshots_node.SetAttribute("paymentTypeId", ((Label)clinic_immunization.FindControl("lblPaymentTypeId")).Text);
                            estshots_node.SetAttribute("estimatedQuantity", !string.IsNullOrEmpty(((TextBox)clinic_immunization.FindControl("txtClinicImmunizationShots")).Text.Trim()) ? Convert.ToInt32(((TextBox)clinic_immunization.FindControl("txtClinicImmunizationShots")).Text).ToString() : "");

                            location_node.AppendChild(estshots_node);
                        }
                    }
                }
                clinic_locations.AppendChild(location_node);
                clinic_location_count++;
            }
        }

        xdoc_clinic_locations.AppendChild(clinic_locations);

        //Update clinic locations details
        XmlDocumentFragment xml_frag = this.xmlCOProgram.CreateDocumentFragment();
        xml_frag.InnerXml = xdoc_clinic_locations.OuterXml;
        co_program_ele.AppendChild(xml_frag);

        DataRow[] dr_selected_immunizations = this.dtImmunizations.Select("isSelected = 'True' AND isPaymentSelected = 'True'");
        XmlElement immunizations = this.xmlCOProgram.CreateElement("Immunizations");
        foreach (DataRow row in dr_selected_immunizations)
        {
            XmlElement immunization = this.xmlCOProgram.CreateElement("Immunization");

            immunization.SetAttribute("pk", row["immunizationId"].ToString());
            immunization.SetAttribute("immunizationName", row["immunizationName"].ToString());
            immunization.SetAttribute("price", ((Convert.ToInt32(row["paymentTypeId"].ToString()) == 6) ? row["directBillPrice"].ToString() : row["price"].ToString()));
            immunization.SetAttribute("paymentTypeId", row["paymentTypeId"].ToString());
            immunization.SetAttribute("paymentTypeName", row["paymentTypeName"].ToString());
            immunization.SetAttribute("name", row["name"] != DBNull.Value ? row["name"].ToString() : string.Empty);
            immunization.SetAttribute("address1", row["address1"] != DBNull.Value ? row["address1"].ToString() : string.Empty);
            immunization.SetAttribute("address2", row["address2"] != DBNull.Value ? row["address2"].ToString() : string.Empty);
            immunization.SetAttribute("city", row["city"] != DBNull.Value ? row["city"].ToString() : string.Empty);
            immunization.SetAttribute("state", row["state"] != DBNull.Value ? row["state"].ToString() : string.Empty);
            immunization.SetAttribute("zip", row["zip"] != DBNull.Value ? row["zip"].ToString() : string.Empty);
            immunization.SetAttribute("phone", row["phone"] != DBNull.Value ? row["phone"].ToString() : string.Empty);
            immunization.SetAttribute("email", row["email"] != DBNull.Value ? row["email"].ToString() : string.Empty);
            immunization.SetAttribute("tax", row["tax"] != DBNull.Value ? row["tax"].ToString() : string.Empty);
            immunization.SetAttribute("copay", row["copay"] != DBNull.Value ? row["copay"].ToString() : string.Empty);
            immunization.SetAttribute("copayValue", row["copayValue"] != DBNull.Value ? row["copayValue"].ToString() : string.Empty);

            immunizations.AppendChild(immunization);
        }
        co_program_ele.AppendChild(immunizations);
        this.xmlCOProgram.AppendChild(co_program_ele);
    }

    /// <summary>
    /// Set control properties
    /// </summary>
    /// <param name="control"></param>
    /// <param name="control_type"></param>
    /// <param name="tool_tip"></param>
    /// <param name="is_invalid"></param>
    private void setControlProperty(Control control, string control_type, string tool_tip, bool is_invalid)
    {
        switch (control_type.ToLower())
        {
            case "textbox":
                if (is_invalid)
                {
                    ((TextBox)control).Attributes["title"] = tool_tip;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true); $('#" + control.ClientID + "').attr({ 'title': '" + tool_tip + "'});", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((TextBox)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }
                break;
            case "dropdownlist":
                if (is_invalid)
                {
                    ((DropDownList)control).ToolTip = tool_tip;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);$('#" + control.ClientID + "').attr({ 'title': '" + tool_tip + "'});", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((DropDownList)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }
                break;
        }
    }

    /// <summary>
    /// Validates clinic duration between clinic date and scheduled date
    /// </summary>
    /// <returns></returns>
    private int validateClinicDates()
    {
        int alert_type = 0;
        DateTime clinic_date;

        XmlNodeList clinic_locations = this.xmlCOProgram.SelectNodes("/Clinics/clinic");

        if (clinic_locations.Count > 0)
        {
            foreach (XmlElement clinic_location in clinic_locations)
            {
                clinic_date = Convert.ToDateTime(clinic_location.Attributes["clinicDate"].Value);

                if (clinic_date.Date < DateTime.Now.Date.AddDays(14))
                {
                    alert_type = 1;
                    break;
                }
            }

            if (alert_type == 0)
                alert_type = 2;
        }

        return alert_type;
    }

    /// <summary>
    /// Save clinic locations
    /// </summary>
    private void doProcess()
    {
        //Validate clinic date and time overlap locally
        XmlNodeList clinic_location_nodes = this.xmlCOProgram.SelectNodes("ClinicAgreement/Clinics/clinic");
        string date_time_stamp_message = "";
        int return_value = this.dbOperation.insertUpdateCOProgram(this.contactLogPk, this.commonAppSession.LoginUserInfoSession.UserID, this.xmlCOProgram.InnerXml, out date_time_stamp_message);

        if (return_value >= 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreementCO") + "'); window.location.href = 'walgreensHome.aspx';", true);
        else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
        {
            this.bindClinicLocations();
            string validation_message = string.Empty;
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "coagreement", this.hfBusinessName.Value, date_time_stamp_message);

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
            return;
        }
    }
    /// <summary>
    /// Binds selected immunization checks to immunization grid
    /// </summary>
    private void bindImmunizationChecks()
    {
        Int32.TryParse(this.hfcontactLogPk.Value.Trim(), out this.contactLogPk);

        DataTable dt_immunizations_selected = new DataTable();
        if (this.dtImmunizations.Select("isSelected = 'True'").Count() == 0)
        {
            this.imgBtnAddImmunization.Enabled = false;
            this.lnkAddImmunization.Enabled = false;
            dt_immunizations_selected = this.dtImmunizations.Clone();
            this.grdImmunizationChecks.DataSource = dt_immunizations_selected;
            this.grdImmunizationChecks.DataBind();

            DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck");
            DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType");
            this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
        }
        else
        {
            dt_immunizations_selected = this.dtImmunizations.Select("isSelected = 'True' AND isPaymentSelected = 'True'").CopyToDataTable();


            this.grdImmunizationChecks.DataSource = dt_immunizations_selected;
            this.grdImmunizationChecks.DataBind();

            if (this.dtImmunizations.Select("isSelected = 'False'").Count() == 0)
            {
                this.imgBtnAddImmunization.Enabled = false;
                this.lnkAddImmunization.Enabled = false;
            }
            else
            {
                if (this.grdImmunizationChecks.FooterRow == null || (this.grdImmunizationChecks.FooterRow != null && !this.grdImmunizationChecks.FooterRow.Visible))
                {
                    this.imgBtnAddImmunization.Enabled = true;
                    this.lnkAddImmunization.Enabled = true;
                }
                else if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
                {
                    DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
                    DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");
                    this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
                }
            }
        }
    }
    /// <summary>
    /// Binds Immunizations and Payment methods to dropdowns
    /// </summary>
    /// <param name="ddl_immunization_checks"></param>
    private void bindImmunizationsToDropdown(DropDownList ddl_immunization_checks, DropDownList ddl_payment_types)
    {
        this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();
        DataView dv_immunizations = new DataView((this.dtImmunizations.Select("isSelected = 'False' AND isActive = 1")).CopyToDataTable());
        DataTable dt_distinct_immunizations = dv_immunizations.ToTable(true, "immunizationId", "immunizationName");

        if (dt_distinct_immunizations.Rows.Count > 0)
        {
            ddl_immunization_checks.DataSource = dt_distinct_immunizations;
            ddl_immunization_checks.DataTextField = "immunizationName";
            ddl_immunization_checks.DataValueField = "immunizationId";
            ddl_immunization_checks.DataBind();
            ddl_immunization_checks.Items.Insert(0, new ListItem("Select Immunization", ""));
        }

        dt_distinct_immunizations = null;
        dt_distinct_immunizations = dv_immunizations.ToTable(true, "paymentTypeId", "paymentTypeName");

        ddl_payment_types.DataSource = dt_distinct_immunizations;
        ddl_payment_types.DataTextField = "paymentTypeName";
        ddl_payment_types.DataValueField = "paymentTypeId";
        ddl_payment_types.DataBind();
        ddl_payment_types.Items.Insert(0, new ListItem("Select Payment Method", ""));
    }
    /// <summary>
    /// Updates Immunization Payment method details
    /// </summary>
    private void updatePaymentTypeData()
    {
        string copay_value = string.Empty;
        int immunization_id = 0;
        int payment_type_id = 0;
        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            Int32.TryParse(((Label)row.FindControl("lblImmunizationPk")).Text, out immunization_id);
            Int32.TryParse(((Label)row.FindControl("lblPaymentTypeId")).Text, out payment_type_id);

            if (immunization_id > 0 && payment_type_id > 0)
            {
                DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + immunization_id + " AND paymentTypeId = " + payment_type_id);
                if (dr_immunization.Count() > 0)
                {
                    if (((System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowSendInvoiceTo")).Visible == true)
                    {
                        dr_immunization[0]["name"] = ((TextBox)row.FindControl("txtPmtName")).Text.Trim();
                        dr_immunization[0]["address1"] = ((TextBox)row.FindControl("txtPmtAddress1")).Text.Trim();
                        dr_immunization[0]["address2"] = ((TextBox)row.FindControl("txtPmtAddress2")).Text.Trim();
                        dr_immunization[0]["city"] = ((TextBox)row.FindControl("txtPmtCity")).Text.Trim();
                        if (((DropDownList)row.FindControl("ddlPmtState")).SelectedIndex != 0 && ((DropDownList)row.FindControl("ddlPmtState")).SelectedIndex != -1)
                            dr_immunization[0]["state"] = ((DropDownList)row.FindControl("ddlPmtState")).SelectedItem.Value;

                        dr_immunization[0]["zip"] = ((TextBox)row.FindControl("txtPmtZipCode")).Text.Trim();
                        dr_immunization[0]["phone"] = ((TextBox)row.FindControl("txtPmtPhone")).Text.Trim();
                        dr_immunization[0]["email"] = ((TextBox)row.FindControl("txtPmtEmail")).Text.Trim();
                        if (((DropDownList)row.FindControl("ddlTaxExempt")).SelectedIndex != 0 && ((DropDownList)row.FindControl("ddlTaxExempt")).SelectedIndex != -1)
                            dr_immunization[0]["tax"] = ((DropDownList)row.FindControl("ddlTaxExempt")).SelectedItem.Value;

                        if (((DropDownList)row.FindControl("ddlIsCopay")).SelectedIndex != 0 && ((DropDownList)row.FindControl("ddlIsCopay")).SelectedIndex != -1)
                        {
                            copay_value = ((DropDownList)row.FindControl("ddlIsCopay")).SelectedItem.Value;
                            dr_immunization[0]["copay"] = copay_value;
                            dr_immunization[0]["copayValue"] = (!string.IsNullOrEmpty(copay_value) || copay_value != "No") ? ((TextBox)row.FindControl("txtCoPay")).Text.Trim() : string.Empty;
                        }
                    }
                }
            }
        }
    }
    /// <summary>
    /// Binds event scheduled default fields
    /// </summary>
    private void bindOutreachTypeDefaultFields(RadioButtonList rbtn_outreach_Types)
    {
        DataTable dt_outreachtype_fields = this.dbOperation.getOutreachTypeFields;
        DataView dv_outreach_types = dt_outreachtype_fields.DefaultView;
        dv_outreach_types.RowFilter = "columnName = 'OutreachType'";

        DataTable dt_outreach_types = dv_outreach_types.Table.Clone();
        foreach (DataRowView drowview in dv_outreach_types)
            dt_outreach_types.ImportRow(drowview.Row);

        rbtn_outreach_Types.DataTextField = "description";
        rbtn_outreach_Types.DataValueField = "fieldValue";
        rbtn_outreach_Types.DataSource = dt_outreach_types;
        rbtn_outreach_Types.DataBind();

    }

    /// <summary>
    /// Binds immunizations to clinic location grid
    /// </summary>
    /// <param name="grd_clinic_immunizations"></param>
    /// <param name="clinic_number"></param>
    private void bindClinicImmunizations(GridView grd_clinic_immunizations, int clinic_number)
    {
        StringReader clinic_immunizations_reader = new StringReader(this.xmlCOProgram.SelectSingleNode("//Clinics").ChildNodes[clinic_number].OuterXml);
        DataSet ds_clinic_immunizations = new DataSet();
        ds_clinic_immunizations.ReadXml(clinic_immunizations_reader);

        if (ds_clinic_immunizations.Tables.Count > 1 && ds_clinic_immunizations.Tables[1].Rows.Count > 0)
        {
            grd_clinic_immunizations.DataSource = ds_clinic_immunizations.Tables[1];
            grd_clinic_immunizations.DataBind();
        }
    }
    /// <summary>
    /// Validate Immunization Values and Clinic Locations
    /// </summary>
    /// <returns></returns>
    private bool ValidateProgram()
    {
        string error_message = string.Empty;
        bool hide_clear_immunization = false;

        bool is_valid = this.ValidateImmunizations(ref error_message, ref hide_clear_immunization);
        is_valid = this.ValidateLocations() && is_valid;

        if (!string.IsNullOrEmpty(error_message))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showImmConfirmationWarning('" + error_message + "','" + hide_clear_immunization + "');", true);
            is_valid = false;
        }
        else
        {
            if (!is_valid)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
        }

        return is_valid;
    }
    /// <summary>
    /// Validate Immunization values
    /// </summary>
    /// <param name="error_message"></param>
    /// <param name="hide_clear_immunization"></param>
    /// <returns></returns>
    private bool ValidateImmunizations(ref string error_message, ref bool hide_clear_immunization)
    {
        bool is_valid = true;
        string value = string.Empty;

        if (this.grdImmunizationChecks.Rows.Count == 0)
        {
            string immunization = ((DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck")).SelectedItem.Value;
            string payment_type = ((DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType")).SelectedItem.Value;

            this.setControlProperty(this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck"), "dropdownlist", (immunization == "" ? "Immunization is required" : string.Empty), (immunization == "" ? true : false));
            this.setControlProperty(this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType"), "dropdownlist", (payment_type == "" ? "Payment Type is required" : string.Empty), (payment_type == "" ? true : false));

            if (immunization != "" && payment_type != "")
            {
                error_message = "Your selections for Immunization and Payment Method have not been confirmed. Click \"OK\" to confirm your selections. The contract will not be emailed until you have entered an Estimated Volume for confirmed immunizations.";
                this.hfUnconfirmedPayment.Value = payment_type;
                hide_clear_immunization = true;
            }

            return false;
        }

        if ((this.grdImmunizationChecks.Rows.Count > 0) && (this.grdImmunizationChecks.FooterRow != null) && (this.grdImmunizationChecks.FooterRow.Visible))
        {
            DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
            DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");

            if ((ddl_immunization_checks != null && ddl_immunization_checks.SelectedItem.Value != "") && (ddl_payment_type != null && ddl_payment_type.SelectedItem.Value != ""))
            {
                error_message = "Your selections for Immunization and Payment Method have not been confirmed. Click \"OK\" to confirm your selections or \"Clear\" to clear the unconfirmed selections. The contract will not be emailed until you have entered an Estimated Volume for confirmed immunizations.";
                this.hfUnconfirmedPayment.Value = ddl_payment_type.SelectedItem.Value;
            }
        }
        else
        {
            foreach (GridViewRow imm_row in this.grdImmunizationChecks.Rows)
            {
                System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice_to = ((System.Web.UI.HtmlControls.HtmlTableRow)imm_row.FindControl("rowSendInvoiceTo"));

                if (row_send_invoice_to.Visible)
                {
                    if (string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtName")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtName"), "textbox", "Name to Send Invoice is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtName")).Text) && !(((TextBox)imm_row.FindControl("txtPmtName")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtName"), "textbox", "Name: , < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtName")), "textbox", string.Empty, false);

                    if (string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtAddress1")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtAddress1"), "textbox", "Address1 is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtAddress1")).Text) && !(((TextBox)imm_row.FindControl("txtPmtAddress1")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtAddress1"), "textbox", " Address1: , < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtAddress1")), "textbox", string.Empty, false);

                    if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtAddress2")).Text) && !(((TextBox)imm_row.FindControl("txtPmtAddress2")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtAddress2"), "textbox", " Address2: , < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtAddress2")), "textbox", string.Empty, false);

                    if (string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtCity")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtCity"), "textbox", "City is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtCity")).Text) && !(((TextBox)imm_row.FindControl("txtPmtCity")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtCity"), "textbox", "City: < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtCity")), "textbox", string.Empty, false);

                    if (((DropDownList)imm_row.FindControl("ddlPmtState")).SelectedItem.Value.Trim() == "")
                    {
                        this.setControlProperty(imm_row.FindControl("ddlPmtState"), "dropdownlist", "State is required", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((DropDownList)imm_row.FindControl("ddlPmtState")), "dropdownlist", string.Empty, false);

                    if (string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtZipCode")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtZipCode"), "textbox", "Zip Code is required", true);
                        is_valid = false;
                    }
                    else if (!(((TextBox)imm_row.FindControl("txtPmtZipCode")).Text).validateZipCode() && !string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtZipCode")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtZipCode"), "textbox", "Invalid Zip Code", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtZipCode")), "textbox", string.Empty, false);

                    value = ((TextBox)imm_row.FindControl("txtPmtPhone")).Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
                    if (string.IsNullOrEmpty(value))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtPhone"), "textbox", "Phone number is required", true);
                        is_valid = false;
                    }
                    else if (!value.validatePhone() && !string.IsNullOrEmpty(value))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtPhone"), "textbox", "Valid Phone number is required(ex: ###-###-####)", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtPhone")), "textbox", string.Empty, false);

                    if (string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtEmail")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtEmail"), "textbox", "Email is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtEmail")).Text) && !(((TextBox)imm_row.FindControl("txtPmtEmail")).Text).validateEmail())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtEmail"), "textbox", "Invalid Email", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtEmail")), "textbox", string.Empty, false);

                    if (string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text))
                    {
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", "Verfiy Email is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text) && !(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text).validateEmail())
                    {
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", "Invalid Verfiy Email", true);
                        is_valid = false;
                    }
                    else
                    {
                        string email = ((TextBox)imm_row.FindControl("txtPmtEmail")).Text;
                        if (email != "" && email.validateEmail())
                        {
                            if (string.Compare(email, ((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text, false) != 0)
                            {
                                this.setControlProperty(imm_row.FindControl("txtPmtVerifyEmail"), "textbox", "Email and Verify Email should match", true);
                                is_valid = false;
                            }
                            else
                                this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", string.Empty, false);
                        }
                        else
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", string.Empty, false);
                    }

                    if (((DropDownList)imm_row.FindControl("ddlTaxExempt")).SelectedItem.Value == "")
                    {
                        this.setControlProperty(imm_row.FindControl("ddlTaxExempt"), "dropdownlist", "Select Is Employer Tax Exempt", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((DropDownList)imm_row.FindControl("ddlTaxExempt")), "dropdownlist", string.Empty, false);

                    if (((DropDownList)imm_row.FindControl("ddlIsCopay")).SelectedItem.Value == "")
                    {
                        this.setControlProperty(imm_row.FindControl("ddlIsCopay"), "dropdownlist", "Select IsCopay", true);
                        is_valid = false;
                    }
                    else if (((DropDownList)imm_row.FindControl("ddlIsCopay")).SelectedItem.Value.Trim() == "Yes")
                    {
                        if (((TextBox)imm_row.FindControl("txtCoPay")).Text.Trim().Length == 0)
                        {
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", "Copay is required", true);
                            is_valid = false;
                        }
                        else if (((TextBox)imm_row.FindControl("txtCoPay")).Text.Trim().Length != 0 && !((TextBox)imm_row.FindControl("txtCoPay")).Text.validateDecimal())
                        {
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", "Invalid Copay", true);
                            is_valid = false;
                        }
                        else
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", string.Empty, false);
                    }
                    else
                    {
                        this.setControlProperty(((DropDownList)imm_row.FindControl("ddlIsCopay")), "dropdownlist", string.Empty, false);
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", string.Empty, false);
                    }
                }
            }
        }

        return is_valid;
    }
    /// <summary>
    /// Validate clinic locations
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateLocations()
    {
        bool is_valid = true;
        string value = string.Empty;

        //Validate clinic locations
        foreach (GridViewRow row in grdLocations.Rows)
        {
            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text) && !(((TextBox)row.FindControl("txtLocalContactName")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name: < > characters are not allowed", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", string.Empty, false);

            //address1
            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1 is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && !(((TextBox)row.FindControl("txtAddress1")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1: < > characters are not allowed", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && !(((TextBox)row.FindControl("txtAddress1")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", string.Empty, false);


            //address2
            if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && !(((TextBox)row.FindControl("txtAddress2")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", "Address2: < > characters are not allowed", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && !(((TextBox)row.FindControl("txtAddress2")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                is_valid = false;
            }
            else
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", string.Empty, false);
            }

            //city
            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text) && !(((TextBox)row.FindControl("txtCity")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City: < > characters are not allowed", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtCity"), "textbox", string.Empty, false);

            //Phone
            value = ((TextBox)row.FindControl("txtLocalContactPhone")).Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();

            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactPhone")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Phone number is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(value) && !value.validatePhone())
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Valid Phone number is required(ex: ###-###-####)", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", string.Empty, false);

            //Zip Code
            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Zip Code is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text) && !(((TextBox)row.FindControl("txtZipCode")).Text.validateZipCode()))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Invalid Zip Code", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", string.Empty, false);


            //Email
            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Email is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text) && !(((TextBox)row.FindControl("txtLocalContactEmail")).Text.validateEmail()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Invalid Email", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", string.Empty, false);

            //State
            if (((DropDownList)row.FindControl("ddlState")).SelectedItem.Value.Trim() == "")
            {
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", "Select State", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", string.Empty, false);

            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", "Start Time is required", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", string.Empty, false);

            if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", "Start Time is required", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", string.Empty, false);

            //clinic date
            value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");
            ComponentArt.Web.UI.Calendar calendarctrl = (ComponentArt.Web.UI.Calendar)((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).FindControl("picker1");
            if (value == "01/01/0001")
            {
                calendarctrl.BorderColor = System.Drawing.Color.Red;
                calendarctrl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                calendarctrl.BorderWidth = Unit.Parse("1px");
                calendarctrl.ToolTip = "Clinic Date is required";
                is_valid = false;
            }
            else
            {
                calendarctrl.Style.Add("border", "1px solid gray");
                calendarctrl.ToolTip = string.Empty;
            }

        }
        return is_valid;
    }

    /// <summary>
    /// Validates contract agreement
    /// </summary>
    /// <param name="is_continue"></param>
    private void ValidationsHandler(bool is_override, bool is_continue)
    {
        DateTime clinic_date;
        bool has_clinic_after_2weeks = false, has_clinic_before_2weeks = false, has_max_qty = false;
        string failed_clinic_locations = string.Empty;
        string clinic_date_reminder_alert = "";
        bool is_having_override = true;
        TextBox txt_clinic_immunization_shots;
        int shots_count = 0;
        string max_qty_error = string.Empty, lbl_clinic_location;
        //this.prepareClinicAggreementXML(false);
        this.validationMsgList = new List<string>();
        foreach (GridViewRow row in grdLocations.Rows)
        {
            bool is_estimated_qnt_exists = false;
            var date_control = row.FindControl("PickerAndCalendarFrom");
            ComponentArt.Web.UI.Calendar calendarctrl = (ComponentArt.Web.UI.Calendar)((PickerAndCalendar)date_control).FindControl("picker1");
            clinic_date = ((PickerAndCalendar)date_control).getSelectedDate;
            lbl_clinic_location = ((Label)row.FindControl("lblClinicLocation")).Text;
            GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
            foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
            {
                txt_clinic_immunization_shots = (TextBox)imm_row.FindControl("txtClinicImmunizationShots");
                string immunization_name = ((Label)imm_row.FindControl("lblClinicImmunization")).Text;
                if (!string.IsNullOrEmpty(txt_clinic_immunization_shots.Text))
                {
                    shots_count = Convert.ToInt32(txt_clinic_immunization_shots.Text);
                    if (this.blockedOutImmunizations.Contains(immunization_name.ToLower().Trim()) && shots_count > 0)
                        is_estimated_qnt_exists = true;

                    if (shots_count > 250 && !is_continue)
                    {
                        has_max_qty = true;
                        max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), shots_count, immunization_name, lbl_clinic_location.Replace("CLINIC LOCATION ", ""));

                    }
                }
            }
            if (clinic_date != Convert.ToDateTime("1/1/0001") && is_estimated_qnt_exists)
            {
                if (clinic_date >= Convert.ToDateTime("04/15/2017") && clinic_date <= Convert.ToDateTime("07/15/2017"))
                {
                    failed_clinic_locations += "\\n" + ((Label)row.FindControl("lblClinicLocation")).Text;
                    calendarctrl.BorderColor = System.Drawing.Color.Red;
                    calendarctrl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                    calendarctrl.BorderWidth = Unit.Parse("1px");
                    calendarctrl.ToolTip = "";
                }
            }

            //Validates clinic duration between clinic date and current date
            if (clinic_date != Convert.ToDateTime("1/1/0001"))
            {
                if (clinic_date.Date < DateTime.Now.Date.AddDays(14))
                    has_clinic_before_2weeks = true;
                else
                    has_clinic_after_2weeks = true;
            }

        }

        if (!string.IsNullOrEmpty(failed_clinic_locations))
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "blackoutImmunizationValidationMessage") + failed_clinic_locations);
            is_having_override = false;
        }
        if (has_clinic_before_2weeks)
        {
            clinic_date_reminder_alert = "coClinicDateReminderBefore2Weeks";
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert));
            if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                is_having_override = false;
        }
        if (this.checkClinicImmunizations())
        {
            //checking date and time stamp locally
            XmlNodeList clinic_location_nodes = this.xmlCOProgram.SelectNodes("ClinicAgreement/Clinics/clinic");
            string failed_location = "";
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "coagreement", this.hfBusinessName.Value, string.Empty);
            if (!String.IsNullOrEmpty(failed_location))
            {
                this.bindClinicLocations();
                is_having_override = false;
                this.validationMsgList.Add(failed_location);
            }

        }
        if (!has_clinic_before_2weeks && has_clinic_after_2weeks)
        {
            clinic_date_reminder_alert = "coClinicDateReminderAfter2Weeks";
            if (this.validationMsgList.Count == 0)
            {
                this.isClinicAfter2Weeks = true;
                if (has_max_qty)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showWarning('WARNING','" + max_qty_error.Substring(6) + "','continuesending');", true);
                    return;
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueReminder');", true);
                return;
            }
            else
                this.isClinicAfter2Weeks = true;
        }
        else
            this.isClinicAfter2Weeks = false;

        if (this.validationMsgList.Count > 0 && !is_override)
        {
            this.showValidationSummary(is_having_override);
        }
        else
        {
            this.validationSummary = "";
            this.validationMsgList.Clear();
            if (has_max_qty)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showWarning('WARNING','" + max_qty_error.Substring(6) + "','continuesending');", true);
                return;
            }
            this.doProcess();
        }
    }
    /// <summary>
    /// Shows Validation summary to the user before sending email/saving agreement.
    /// </summary>
    /// <param name="is_confirmation_alert"></param>
    private void showValidationSummary(bool is_confirmation_alert)
    {
        if (this.validationMsgList.Count > 0)
        // if (!string.IsNullOrEmpty(this.validationSummary) && isConfirmationAlert)
        {
            this.validationSummary = "<ul>";
            foreach (var item in this.validationMsgList)
            {
                this.validationSummary += "<li style=\"text-align:left\">" + item.Replace("\n", "<br />").Trim() + "</li><br/>";
            }
            this.validationSummary += "</ul>";
            if (is_confirmation_alert)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryWarning('" + this.validationSummary + "','continuesending');", true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryAlert('" + this.validationSummary + "','continuesending');", true);
            this.validationSummary = "";
            this.validationMsgList.Clear();
        }
    }
    /// <summary>
    /// Check if all the immunizations are added to clinics
    /// </summary>
    /// <returns></returns>
    private bool checkClinicImmunizations()
    {
        bool is_valid = true;
        XmlNodeList clinic_locations = this.xmlCOProgram.SelectNodes("ClinicAgreement/Clinics/clinic");
        XmlNodeList contract_immunizations = this.xmlCOProgram.SelectNodes("ClinicAgreement/Immunizations/Immunization");

        foreach (XmlElement clinic in clinic_locations)
        {
            if (clinic.SelectNodes("./immunization").Count != contract_immunizations.Count)
            {
                is_valid = false;
                foreach (XmlElement immunization in contract_immunizations)
                {
                    if (clinic.SelectSingleNode("./immunization[@pk = '" + immunization.Attributes["pk"].Value + "' and @paymentTypeId = '" + immunization.Attributes["paymentTypeId"].Value + "']") == null)
                    {
                        XmlElement est_shots = this.xmlCOProgram.CreateElement("immunization");
                        est_shots.SetAttribute("pk", immunization.Attributes["pk"].Value);
                        est_shots.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
                        est_shots.SetAttribute("estimatedQuantity", string.Empty);
                        clinic.AppendChild(est_shots);
                    }
                }
            }
        }

        return is_valid;
    }

    /// <summary>
    /// Sets min/max dates for clinics
    /// </summary>
    /// <param name="row"></param>
    private void setClinicDates(GridViewRow row, bool is_from_grid_load)
    {
        // string selected_store_name = this.commonAppSession.SelectedStoreSession.storeName.Substring(this.commonAppSession.SelectedStoreSession.storeName.LastIndexOf(", ") + 1, 3).Trim();
        var date_control = row.FindControl("PickerAndCalendarFrom");
        DropDownList ddl_states = (DropDownList)row.FindControl("ddlState");
        if (grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
        {
            string clinic_date = grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString();
            if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin)
            {
                if (Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()) >= DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                else if (Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                    ((PickerAndCalendar)date_control).SetMinDate = Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString());
            }
            else if (Convert.ToDateTime(clinic_date) > DateTime.Today.Date)
            {
                if (DateTime.Now.AddDays(14) < Convert.ToDateTime(clinic_date)
                    && !ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(13);
                else if (ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                else
                    ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
            }
            else
                ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
            if (is_from_grid_load)
            {
                ((PickerAndCalendar)date_control).getSelectedDate = Convert.ToDateTime(clinic_date);
                ((TextBox)row.FindControl("txtCalenderFrom")).Text = Convert.ToDateTime(clinic_date).ToString("MM/dd/yyyy");
            }
        }
        else
        {
            //For "MO" stores block out all clinic dates up to September 1, 2016 in the local contract and for charity events
            if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && !commonAppSession.LoginUserInfoSession.IsAdmin)
            {
                ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
            }
            else
            {
                if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Now.AddDays(14);
                else
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
            }
        }
    }

    /// <summary>
    /// Set minimum and maximum dates for date controls.
    /// </summary>
    private void setMinMaxDates()
    {
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            var clinic_date = row.FindControl("PickerAndCalendarFrom");
            if (clinic_date != null)
            {
                this.setClinicDates(row, false);
            }
        }
    }

    #endregion

    #region --------------- PRIVATE VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private int contactLogPk;
    private DataTable dtImmunizations
    {
        get
        {
            DataTable value = new DataTable();
            if (ViewState["dtImmunizations"] != null)
                value = (DataTable)ViewState["dtImmunizations"];
            return value;
        }
        set
        {
            ViewState["dtImmunizations"] = value;
        }
    }
    private bool isMOState
    {
        get
        {
            return Convert.ToBoolean(hfisMOState.Value);
        }
        set
        {
            hfisMOState.Value = value.ToString();
        }
    }
    private bool isAddressDisabled
    {
        get
        {
            bool value = false;
            if (ViewState["isAddressDisabled"] != null)
                value = (bool)ViewState["isAddressDisabled"];
            return value;
        }
        set
        {
            ViewState["isAddressDisabled"] = value;
        }
    }
    private bool isRestrictedStoreState
    {
        get
        {
            bool value = false;
            if (ViewState["isRestrictedStoreState"] != null)
                value = (bool)ViewState["isRestrictedStoreState"];
            return value;
        }
        set
        {
            ViewState["isRestrictedStoreState"] = value;
        }
    }
    private string[] blockedOutImmunizations
    {
        get
        {
            return new string[] { "influenza - standard/pf injectable (trivalent)", "influenza - standard injectable quadrivalent", "influenza - high dose" };
        }
    }
    private string validationSummary { get; set; }
    private List<string> validationMsgList { get; set; }
    private bool isClinicAfter2Weeks
    {
        get
        {
            bool value = false;
            if (ViewState["isClinicAfter2Weeks"] != null)
                value = (bool)ViewState["isClinicAfter2Weeks"];
            return value;
        }
        set
        {
            ViewState["isClinicAfter2Weeks"] = value;
        }
    }
    private XmlDocument xmlCOProgram = null;
    protected string paymentMethods
    {
        get
        {
            string value = "[]";
            if (ViewState["paymentMethods"] != null)
                value = ViewState["paymentMethods"].ToString();
            return value;
        }
        set
        {
            ViewState["paymentMethods"] = value;
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.xmlCOProgram = new XmlDocument();

        if (Session.IsNewSession)
        {
            FormsAuthentication.SignOut();
            string str = Request.Url.ToString();
            Response.Redirect(str);
        }
    }
}