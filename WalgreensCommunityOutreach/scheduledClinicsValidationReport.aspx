﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="scheduledClinicsValidationReport.aspx.cs"
    Inherits="scheduledClinicsValidationReport" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link href="css/wagsSched.css" rel="stylesheet" type="text/css" />
    <link href="css/printOnline.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <table width="936" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                <td width="312" align="left" valign="top" bgcolor="#FFFFFF" style="padding:12px 0px 0px 16px;"><img src="images/wags_logo.png" width="225" height="52" /><br />
                <span class="outreachTitle">Community Outreach</span></td>
                <td width="593" colspan="2" rowspan="2" align="right" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr>
    		    <td align="left" valign="bottom" style="padding:0px 0px 12px 20px;" bgcolor="#FFFFFF">&nbsp;</td>
  		    </tr>
                    <tr>
                        <td colspan="3" align="right" valign="middle" class="navBar">
                            <img src="images/spacer.gif" alt="" name="" width="1" height="25" border="0" id="home" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" bgcolor="#FFFFFF">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2" width="100%" valign="top" bgcolor="#f9f9f9">
                                        <form id="form2" runat="server">
                                        <table width="95%" align="center" cellpadding="0" cellspacing="5">
                                            <tr>
                                                <td colspan="2" width="100%" class="profileTitles">
                                                    <asp:GridView ID="grdClinicValidationRpt" runat="server" AutoGenerateColumns="False"
                                                        GridLines="None" Width="100%" CellPadding="7" CellSpacing="0" AllowPaging="false"
                                                        AllowSorting="false">
                                                        <FooterStyle CssClass="footerGrey" />
                                                        <HeaderStyle BackColor="#999999" ForeColor="White" Font-Size="13px" Font-Bold="false"
                                                            Height="30px" CssClass="gridHeaderStyle" />
                                                        <%--RowStyle Height="35px" BackColor="White" CssClass="gridStyles" />--%>
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Validation Type" HeaderStyle-HorizontalAlign="Left" DataField="validationType"
                                                                ItemStyle-Font-Bold="false" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="45%" />
                                                            <asp:BoundField HeaderText="Count" HeaderStyle-HorizontalAlign="Left" DataField="validationCount"
                                                                ItemStyle-Font-Bold="false" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%" />
                                                            <asp:BoundField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" DataField="validationDescription" ItemStyle-CssClass="wrapword"
                                                                ItemStyle-Font-Bold="false" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50%" />
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <center>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: White;
                                                                    text-align: center;">
                                                                    <tr style="height: 35px;">
                                                                        <td>
                                                                            <b>
                                                                                <asp:Label ID="lblNote" Text="No records found" CssClass="formFields" runat="server"></asp:Label></b>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </center>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="100%" class="profileTitles">
                                                    <asp:Label ID="lblLoggedInWagUsers" runat="server" CssClass="wrapword"></asp:Label>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td colspan="2" width="100%" class="profileTitles">
                                                    <asp:Label ID="lblSessionStorageSize" runat="server" CssClass="wrapword"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="100%" class="profileTitles">
                                                    <asp:Label ID="lblProcessorUsage" runat="server" CssClass="wrapword"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
</body>
</html>
