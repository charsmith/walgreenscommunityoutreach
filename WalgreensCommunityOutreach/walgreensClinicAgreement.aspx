﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensClinicAgreement.aspx.cs"   Inherits="walgreensClinicAgreement" meta:resourcekey="PageResource1" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link rel="stylesheet" type="text/css" href="css/wags.css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css" />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.timepicker.css" />
    <%--<link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />--%>
    
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script src="javaScript/jquery.printelement.min.js" type="text/javascript"></script>    
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>
	<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />    

    <style type="text/css">
        .ui-timepicker-list li
        {
            color: #000000;
            font-family: "Times New Roman",Times,serif;
            font-size: 11px;
        }
		.ui-widget-header
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 13px;
            font-weight: bold;
        }  
        .ui-widget
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            font-weight: normal;
        }
        .tooltipText
        {
            font-weight:bold;
            font-size:11px;
            color:red;
            position:absolute;
            white-space: pre-wrap;
            max-width:180px;
            padding:2px;
            border-radius:0px;
            -webkit-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            -moz-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
        }
    </style>
    <script type="text/javascript">
        if (typeof String.prototype.startsWith != 'function') {
            String.prototype.startsWith = function (str) {
                return this.indexOf(str) === 0;
            };
        }
        //setting control css
        var setInvalidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "background-color": "yellow" });

            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.tooltip({
                tooltipClass: "tooltipText"
            });
            ctrl.focus(function (evt) {
                $(evt.currentTarget).tooltip("close");
            });
            ctrl.tooltip("enable");
        }
        var setValidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "border": "1px solid gray" });
            ctrl.css({ "background-color": "" });
            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.removeAttr("title");
            ctrl.tooltip({
                disabled: true
            });
            ctrl.on("click", function () {
                ctrl.data("title", ctrl.attr("title")).removeAttr("title");
            }, function () {
                var title = ctrl.data("title");
                ctrl.tooltip("option", "content", title || ctrl.attr("title"));
            });

        }
        var paymentMethods = []

        $(document).ready(function () {

            if ($.browser.msie) {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keydown(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keydown(function (event) { event.preventDefault(); });
            }
            else {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keypress(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keypress(function (event) { event.preventDefault(); });
            }

            if ($("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').length > 0) {
                $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').find("option").remove();
                $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').append('<option value=""><%= GetLocalResourceObject("SelectPaymentType").ToString() %></option>');
            }

            // Print contract agreement page
            $("#lnkPrintContract").click(function () {
                $("#trContractBodyText").printElement({ printMode: 'popup' });
            });

            $("#rbtnApprove").change(function () {
                if (this.checked) {
                    $("#trNotes").hide();
                }
                else {
                    $("#trNotes").show();
                }
            });

            $("#rbtnReject").change(function () {
                if (this.checked) {
                    $("#trNotes").show();
                }
                else {
                    $("#trNotes").hide();
                }
            });

            //Hide voucher expiration date when voucher selected is either No or none
            $("#grdImmunizationChecks").find('select[id$=ddlVoucher]').each(function () {
                if ($(this).find('option:selected').text() == "No") {
                    $(this).find("[id=rowExpirationDate]").css('display', 'none');
                }
                else
                    $(this).find("[id=rowExpirationDate]").css('display', 'block');
            });

            //Disable copay textbox when isCopay selected is either No or none
            $("#grdImmunizationChecks").find('select[id$=ddlIsCopay]').each(function () {
                enableCoPay($(this)[0]);
            });

            $("#txtDistrictNum").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            //Disable Add Button when location is not selected in dropdown  for Previous MO State clinics.
            disableAddBtnForMOPrevious();
            $("#<%= grdImmunizationChecks.ClientID %> select[id*='ddlVoucher']").each(function () {
                displayExpirationDate($(this)[0]);
            });
            $("#<%= grdLocations.ClientID %> input[id*='chkNoClinic']").each(function () {
                showHideVoucherDetails($(this)[0].id);
            });
            $("#<%= grdLocations.ClientID %> input[id*='chkNoClinic']").click(function () {
                showHideVoucherDetails($(this)[0].id);
             });

        });

        function disableAddBtnForMOPrevious()
        {
            if ($("#hfIsMoPrevious").val().toString().toLowerCase() == "true")
            {
                var btnAddPreviousClinic = document.getElementById('<%= imgbtnAddPreviousClinic.ClientID %>');
                if (document.getElementById('<%= ddlClinicLocations.ClientID %>') != null)
                {
                    var ddlClinicLocations = document.getElementById('<%= ddlClinicLocations.ClientID %>');
                    if (typeof ddlClinicLocations !== "undefined")
                    {
                        if (ddlClinicLocations.selectedIndex == 0)
                            btnAddPreviousClinic.disabled = true;
                        else
                            btnAddPreviousClinic.disabled = false;
                    }
                }
            }
        }        

        function enableCoPay(e) {
            var txtCoPay = e.id;
            txtCoPay = txtCoPay.replace("ddlIsCopay", "txtCoPay");
            if ($(e).val() == "Yes") {
                $("#" + txtCoPay).removeAttr('disabled');
            }
            else {
                $("#" + txtCoPay).attr('disabled', 'disabled');
                $("#" + txtCoPay).val("");
            }

            if ($('#txtwalgreenname').is(':disabled') == true) {
                $("#" + txtcopay).attr('disabled', 'disabled');
            }
        }

        function displayPaymentMethods(selected_imm_id) {
            paymentMethods = $.parseJSON('<%= this.paymentMethods %>');

            if ($("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').length > 0) {
                $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').find("option").remove();
                $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').append('<option value=""><%= GetLocalResourceObject("SelectPaymentType").ToString() %></option>');
            }

            $.each(paymentMethods, function (key, val) {
                if (val["immunizationId"] == selected_imm_id && val["isPaymentSelected"] == "False")
                    $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').append('<option value="' + val["paymentTypeId"] + '">' + val["<%= GetLocalResourceObject("PaymentTypeNameKey").ToString() %>"] + '</option>'); 
            });
        }        

        function displayExpirationDate(e) {
            var voucher_needed = e.id;
            voucher_needed = voucher_needed.replace("ddlVoucher", "rowExpirationDate");
            if ($(e).val() == "Yes") {
                $("#" + voucher_needed).css('display', 'block');
            }
            else {
                $("#" + voucher_needed).css('display', 'none');
            }
        }

        function validateAgreementToEmails(sender, args) {
            var val = args.Value;
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            var email_ids = val.replace(/\n/g, ",").replace(";", ",");
            email_ids = email_ids.split(",");
            var valid_emails = true;
            for (var i = 0; i < email_ids.length; i++) {                
                if (!emailPattern.test($.trim(email_ids[i]))) {
                    valid_emails = false;
                }
            }

            if (!valid_emails)
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        function CustomValidatorForWalgreensDate(source, arguments) {
            arguments.IsValid = false;
            if ($('#PickerAndCalendarWalgreensDate_Picker1_picker').length > 0) {
                if ($('#PickerAndCalendarWalgreensDate_Picker1_picker').val() != "") {
                    arguments.IsValid = true;
                }
            }
            else
                arguments.IsValid = true;
        }

        function validateImmSelection() {
            if ($("#grdImmunizationChecks").find('select[id$=ddlImmunizationCheck]').length > 0 && $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').length > 0) {
                if ($("#grdImmunizationChecks").find('select[id$=ddlImmunizationCheck]').val() == "" || $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').val() == "") {
                    alert('<%= GetLocalResourceObject("SelectPaymentType").ToString() %>');
                    return false;
                }
            }
        }

        function validateImmunizationPayments() {
            //var isLinkVisible = $("#lnkAddImmunization").prop("disabled");
            var is_add_imm_disabled = $("#lnkAddImmunization").attr("disabled");
            if (is_add_imm_disabled != undefined) {
                if ($("#grdImmunizationChecks tr").length > 5) {
                    showImmConfirmationWarning('<%= GetLocalResourceObject("ImmunizationSelectionValidationMessage1").ToString() %>', false);
                }
                else {
                    showImmConfirmationWarning('<%= GetLocalResourceObject("ImmunizationSelectionValidationMessage2").ToString() %>', true);
                }
                return false;
            }
        }

        function getBool(val) {
            var num = +val;
            return !isNaN(num) ? !!num : !!String(val).toLowerCase().replace(!!0, '');
        }

        function showImmConfirmationWarning(alert, hideClearAction) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "<%= GetLocalResourceObject("ImmunizationPaymentTypeConfirm").ToString() %>",
                    buttons: {
                        'Ok': function () {
                            $(this).dialog('close');
                            __doPostBack("confirmImmunization", 1);
                        },
                        'Clear': function () {
                            $(this).dialog('close');
                            __doPostBack("confirmImmunization", 0);
                        }
                    },
                    modal: true
                });
                if (getBool(hideClearAction))
                    $('.ui-dialog-buttonpane').find('button:last').css('visibility', 'hidden');
            });
        }

        function CustomValidatorForLocations(source, arguments) {
            var validating_fields = $("form :text, textarea, select");
            var clinic_fields = ['PickerAndCalendarFrom', 'txtStartTime', 'txtEndTime'];
            var is_required = false;
            var validationGroup = source;
            var validating_field_names = {
                //Payment type details 
                'txtEmails': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Email', 'type': 'AgreementToEmails', 'requiredMessage': '<%= GetLocalResourceObject("EmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("EmailInvalid").ToString() %>' },
                'txtPmtName': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Pmt Name', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("NameSendToIvoiceRequire").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("NameSendToIvoiceInvalid").ToString() %>' },
                'txtPmtAddress1': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Address1', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("Address1Required").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("Address1Invalid").ToString() %>' },
                'txtPmtAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': '<%= GetLocalResourceObject("Address2Invalid").ToString() %>' },
                'txtPmtCity': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("CityRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("CityInvalid").ToString() %>' },
                'txtPmtZipCode': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Zip Code', 'type': 'zipcode', 'requiredMessage': '<%= GetLocalResourceObject("ZipCodeRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ZipCodeInvalid").ToString() %>' },
                'txtPmtPhone': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Phone', 'type': 'phone', 'requiredMessage': '<%= GetLocalResourceObject("PhoneRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("PhoneInvalid").ToString() %>' },
                'txtPmtEmail': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'email', 'type': 'email', 'requiredMessage': '<%= GetLocalResourceObject("UserEmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("UserEmailInvalid").ToString() %>' },
                'txtPmtVerifyEmail': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'VeriyEmail', 'type': 'email', 'requiredMessage': '<%= GetLocalResourceObject("UserVerifyEmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("UserVerifyEmailInvalid").ToString() %>' },
                'txtCoPay': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'CopayYes', 'name': 'Copay', 'type': 'decimal', 'requiredMessage': '<%= GetLocalResourceObject("CopayRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("CopayInvalid").ToString() %>' },
                'ddlImmunizationCheck': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Immunizations', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("ddlImmCheckRequired").ToString() %>', 'invalidMessage': '' },
                'ddlPaymentType': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Payment Type', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("ddlPaymentTypeRequired").ToString() %>', 'invalidMessage': '' },                
                'ddlPmtState': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Pmt state', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("StateRequired").ToString() %>', 'invalidMessage': '' },
                'ddlIsCopay': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Is copay', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("ddlCopayRequired").ToString() %>', 'invalidMessage': '' },
                'ddlTaxExempt': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Tax Exempt', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("TaxExemptRequired").ToString() %>', 'invalidMessage': '' },
                'ddlVoucher': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Voucher', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("VoucherRequired").ToString() %>', 'invalidMessage': '' },

                //Clinic Location details
                'txtLocalContactName': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Contact Name', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("LocalContactNameRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("LocalContactNameInvalid").ToString() %>' },
                'txtAddress1': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Address1', 'type': 'address', 'requiredMessage': '<%= GetLocalResourceObject("Address1Required").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("Address1Invalid").ToString() %>' },
                'txtAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'address', 'requiredMessage': '', 'invalidMessage': '<%= GetLocalResourceObject("Address2Invalid").ToString() %>' },
                'txtCity': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("CityRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("CityInvalid").ToString() %>' },
                'txtLocalContactPhone': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Contact Phone', 'type': 'phone', 'requiredMessage': 'Phone is required', 'invalidMessage': 'Valid Phone number is required(ex: ###-###-####)' },
                'txtZipCode': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'ZipCode', 'type': 'zipcode', 'requiredMessage': '<%= GetLocalResourceObject("ZipCodeRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ZipCodeInvalid").ToString() %>' },
                'txtLocalContactEmail': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Contact Email', 'type': 'email', 'requiredMessage': '<%= GetLocalResourceObject("UserEmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("UserEmailInvalid").ToString() %>' },
                'txtClinicImmunizationShots': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Estimated shots', 'type': 'numeric', 'requiredMessage': '<%= GetLocalResourceObject("ClinicImmunizationShotsRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ClinicImmunizationShotsInvalid").ToString() %>' },
                'ddlState': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'state', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("StateRequired").ToString() %>', 'invalidMessage': '' },
                'txtStartTime': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Start Time', 'type': '', 'requiredMessage': '<%= GetLocalResourceObject("StartTimeRequired").ToString() %>', 'invalidMessage': '' },
                'txtEndTime': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'End Time', 'type': '', 'requiredMessage': '<%= GetLocalResourceObject("EndTimeRequired").ToString() %>', 'invalidMessage': '' },
                'PickerAndCalendarFrom': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Date Time', 'type': '', 'requiredMessage': '<%= GetLocalResourceObject("CliniDateRequired").ToString() %>', 'invalidMessage': '' },

                //Walgreens co Data
                'txtWalgreenName': { 'isRequired': false, 'when': 'always', 'name': 'Walgreen Name', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("WalgreenNameRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("WalgreenNameInvalid").ToString() %>' },
                'txtWalgreenTitle': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Walgreen Title', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("WalgreenTitleRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("WalgreenTitleInvalid").ToString() %>' },
                'txtDistrictNum': { 'isRequired': (validationGroup == 'WalgreensUser' ? true : false), 'when': 'always', 'name': 'Email', 'type': 'numeric', 'requiredMessage': '<%= GetLocalResourceObject("DistrictNumRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("DistrictNumInvalid").ToString() %>' }
            };

            var first_identified_control = "";
            var is_valid = true;
            var ctrl_Id;
            var indexOf_Underscore;
            var txtPmtEmail;
            var trimmedValue;
            $.each(validating_fields, function (index, ctrl) {
                setValidControlCss($(ctrl), false);
                $(ctrl).attr({ "title": "" });
                ctrl_Id = $(ctrl).attr('id');

                if ((ctrl_Id.indexOf('grdImmunizationChecks') > -1 || ctrl_Id.indexOf('grdLocations') > -1 || ctrl_Id.indexOf('grdClinicImmunizations') > -1) && (ctrl_Id.lastIndexOf('_') > -1)) {
                    if(ctrl_Id.indexOf('grdLocations') > -1)
                        is_required = !$("input[type=checkbox][id=" + ctrl_Id.substring(0, ctrl_Id.indexOf('_', ctrl_Id.indexOf('_') + 1)) + "_chkNoClinic]")[0].checked;
                    if (ctrl_Id.toLowerCase().indexOf('_picker') > -1) {
                        var pos = 0;
                        var count = 0;
                        while (true) {
                            pos = ctrl_Id.indexOf('_', pos + 1);
                            ++count;
                            if (count == 2) {
                                break;
                            }
                        }
                        ctrl_Id = ctrl_Id.substring(pos + 1, ctrl_Id.indexOf('_', pos + 1))
                    }
                    else {
                        indexOf_Underscore = ctrl_Id.lastIndexOf('_');
                        if (ctrl_Id.length > indexOf_Underscore + 1) {
                            ctrl_Id = ctrl_Id.substring(indexOf_Underscore + 1);
                        }
                    }

                    if (validating_field_names[ctrl_Id] != undefined && validating_field_names[ctrl_Id].isRequired != is_required && validationGroup == 'WalgreensUser') {
                        if (ctrl_Id == 'PickerAndCalendarFrom' || ctrl_Id == 'txtStartTime' || ctrl_Id == 'txtEndTime')
                            validating_field_names[ctrl_Id].isRequired = is_required;
                    }
                }
                trimmedValue = $.trim($(ctrl).val());
                //storing for email comparison
                if (ctrl_Id == 'txtPmtEmail') {
                    txtPmtEmail = $(ctrl);
                }                
                if ((validating_field_names[ctrl_Id] != undefined) && validating_field_names[ctrl_Id].isRequired &&
                    (validating_field_names[ctrl_Id].when == 'always' || $(ctrl).attr('disabled') != 'disabled') &&
                    trimmedValue == "") {
                    if (validating_field_names[ctrl_Id].type == 'select') {
                        if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 6.") != -1) || (navigator.userAgent.indexOf("Trident") != -1)) {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                        else {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                    }
                    else {
                        setInvalidControlCss($(ctrl), false);
                        if (ctrl_Id == 'PickerAndCalendarFrom')
                            $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });                        
                        else
                        $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                    }
                    if (first_identified_control == '')
                        first_identified_control = $(ctrl);
                    is_valid = false;
                }

                else if ((validating_field_names[ctrl_Id] != undefined) && trimmedValue != "") {
                    if (!validateData(validating_field_names[ctrl_Id].type, trimmedValue, (validating_field_names[ctrl_Id].name == 'Estimated shots') ? false : true)) {
                        setInvalidControlCss($(ctrl), false);
                        if (ctrl_Id == 'PickerAndCalendarFrom')
                            $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                        else
                        $(ctrl).attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;
                    }
                    else if (validating_field_names[ctrl_Id].type == 'address' && validationGroup == 'WalgreensUser')
                    {
                        if((!(validatePO(trimmedValue)))&&($(ctrl)[0].disabled==false))
                        {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": '<%= HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert") %>' });
                            if (first_identified_control == '')
                                first_identified_control = $(ctrl);
                            is_valid = false;
                        }
                    }

                    if (ctrl_Id == 'txtPmtVerifyEmail') {
                        if (trimmedValue != txtPmtEmail.val()) {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": '<%= GetLocalResourceObject("VerifyEmailMessage").ToString() %>' });
                            is_valid = false;
                        }
                    }
                }
            });

            if (!is_valid) {
                alert("<%= GetLocalResourceObject("MandatoryFieldsMessage").ToString() %>");
				first_identified_control[0].focus();
                return false;
            }
            else return true;
        }

        function disableAddLocations() {
            var is_disable = false;
            if ($("#hfDisableAddLocations").val() != "") {
                var store_message = $("#hfDisableAddLocations").val();
                is_disable = true;
                alert(store_message);
            }            
            return !is_disable;
        }

        function setCurrentCulture(culture) {
            if (CustomValidatorForLocations('SendLater', this)) {
                if (culture.outerHTML.indexOf("English") >= 0) {
                    $("#hfLanguage").val("en-US");
                }
                else {
                    var storeName = '<%= this.commonAppSession.SelectedStoreSession.storeName %>';
                    if (storeName.substr(storeName.lastIndexOf(",") + 2, 2) == 'PR')
                        $("#hfLanguage").val("es-PR");
                    else
                    $("#hfLanguage").val("es-MX");
                }
            }
            else
            {
                return false;
            }
        }
        function showClinicDateReminder(alert, handler) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: "IMPORTANT REMINDER",
                    buttons: {
                        'Ok': function () {
                            __doPostBack(handler);
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
            return false;
        }
        function showHideVoucherDetails(ctrl)
        {
            var ctl_id = ctrl.replace("chkNoClinic", "");
            if ($("#"+ctrl+"").attr('checked') == 'checked') {
                $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowClinicDateLabels']").hide();
                    $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowClinicDateControls']").hide();
                    $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowVoucherDateLabels']").show();
                    $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowVoucherDateControls']").show();
                } else {
                    $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowClinicDateLabels']").show();
                    $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowClinicDateControls']").show();
                    $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowVoucherDateLabels']").hide();
                    $("#<%= grdLocations.ClientID %> tr[id*='" + ctl_id + "rowVoucherDateControls']").hide();
                }
        }
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfContactLogPk" runat="server" />
    <asp:HiddenField ID="hfBussinessClinicPk" runat="server" />
    <asp:HiddenField ID="hfBusinessStoreId" runat="server" />
    <asp:HiddenField ID="hfDisableAddLocations" runat="server" />
    <asp:HiddenField ID="hfUnconfirmedPayment" runat="server" />
    <asp:HiddenField ID="hfLanguage" runat="server" Value="en-US" />
    <asp:HiddenField ID="hfIsMoPrevious" runat="server" Value="false" />
    <asp:HiddenField ID="hfLastUpdatedDate" runat="server" Value="" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList"  HeaderText="Error: " ShowMessageBox="True" ShowSummary="False" ValidationGroup="WalgreensUser" meta:resourcekey="ValidationSummary1Resource1" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="BulletList"  HeaderText="Error: " ShowMessageBox="True" ShowSummary="False" ValidationGroup="SendLater" meta:resourcekey="ValidationSummary2Resource1" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="left">
              <table width="935" border="0" cellspacing="22" cellpadding="0">
                <tr>
                    <td colspan="2" valign="top" class="pageTitle"><asp:Label ID="lblAgreementTitle" runat="server" Text="Walgreens Community Off-Site Agreement" meta:resourcekey="lblAgreementTitleResource1"></asp:Label></td>
                </tr>
                <tr>
                    <td valign="top" width="599" >
                    <table width="599" border="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="612" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td  class="contractBodyText" style="padding-bottom:6px">
                            <table  align="right">
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkChangeCulture" runat="server" OnClientClick="return setCurrentCulture(this);" onclick="lnkChangeCulture_Click" Text="" meta:resourcekey="lnkChangeCultureResource1"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrintContract" runat="server" Visible="False" Text="" meta:resourcekey="lnkPrintContractResource1"></asp:LinkButton>
                                </td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="612" border="0" cellspacing="0" cellpadding="36" id="trContractBodyText" style="border:#CCC solid 1px" >
                                    <tr>
                                        <td class="contractBodyText">
                                            <p align="center">
                                                
                                                    <img src="images/contract_logo.gif" width="191" height="37" alt="Walgreens" /><br />
                                                   <b><asp:Label ID="lblAgreementName" runat="server" meta:resourcekey="lblAgreementNameResource1"></asp:Label></b></p>
                                            <p align="justify">
                                                <asp:Label ID="lblAgreementFirstPara" runat="server" meta:resourcekey="lblAgreementFirstParaResource1"></asp:Label></p>
                                            <p align="justify">
                                               <asp:Label ID="lblAgreementSecondPara" runat="server" meta:resourcekey="lblAgreementSecondParaResource1"></asp:Label></p>
                                             <p align="justify">
                                               <asp:Label ID="lblAgreementThirdPara" runat="server" meta:resourcekey="lblAgreementThirdParaResource1"></asp:Label></p>
                                            <table width="580" border="0" align="center" cellpadding="0" cellspacing="5" id="tblBusinesses" runat="server">
                                                <tr>
                                                    <td align="left">&nbsp;</td>
                                                    <td align="right">
                                                        <table width="220" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="200" align="right" valign="top">
                                                                    <asp:LinkButton ID="lnkAddImmunization" runat="server" OnClick="imgBtnAddImmunization_Click"  meta:resourcekey="lnkAddImmunizationResource1" ></asp:LinkButton>&nbsp;
                                                                </td>
                                                                <td width="20" align="right" valign="top">
                                                                    <asp:ImageButton ID="imgBtnAddImmunization" ImageUrl="images/btn_add_business.png" 
                                                                    runat="server" AlternateText="Add Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_business.png');"
                                                                    onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_business_lit.png');" onclick="imgBtnAddImmunization_Click" meta:resourcekey="imgBtnAddImmunizationResource1"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:GridView ID="grdImmunizationChecks" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" onrowdatabound="grdImmunizationChecks_RowDataBound" 
                                                        meta:resourcekey="grdImmunizationChecksResource1" >
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Immunization" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="contractHeaderRow" 
                                                                    ItemStyle-CssClass="contractRow" FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="35%" 
                                                                    FooterStyle-Height="25px" meta:resourcekey="TemplateFieldResource1">
                                                                        <ItemTemplate>
                                                                            <table id="tblImmunization" width="100%" border="0">
                                                                                <tr>
                                                                                    <td colspan="2" valign="top">
                                                                                        <asp:Label ID="lblImmunizationCheck" runat="server" Text='<%# Bind("immunizationName") %>' meta:resourcekey="lblImmunizationCheckResource1" ></asp:Label>
                                                                                        <asp:Label ID="lblImmunizationPk" runat="server" Text='<%# Bind("immunizationId") %>' Visible="False" meta:resourcekey="lblImmunizationPkResource1"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                              <tr>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlImmunizationCheck" runat="server" CssClass="contractBodyText" Height="20px" Width="210px" AutoPostBack="false" 
                                                                                    onchange="javascript:displayPaymentMethods(this.value);" meta:resourcekey="ddlImmunizationCheckResource1"></asp:DropDownList>
                                                                                </td>
                                                                              </tr>
                                                                            </table>
                                                                        </FooterTemplate>
                                                                        <FooterStyle CssClass="contractRow" Height="25px"></FooterStyle>
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="contractHeaderRow" Width="35%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="contractRow" Width="35%"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Payment Method" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="contractHeaderRow" 
                                                                    ItemStyle-CssClass="contractRow" FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="40%" 
                                                                    meta:resourcekey="TemplateFieldResource2">
                                                                        <ItemTemplate>
                                                                            <table id="tblPaymentTypes" width="100%" border="0">
                                                                                <tr>
                                                                                    <td colspan="2" valign="top">
                                                                                        <asp:Label ID="lblPaymentType" Text='<%# Bind("paymentTypeName") %>' runat="server" meta:resourcekey="lblPaymentTypeResource1" ></asp:Label>
                                                                                        <asp:Label ID="lblPaymentTypeId" Text='<%# Bind("paymentTypeId") %>' runat="server" Visible="False" meta:resourcekey="lblPaymentTypeIdResource1"></asp:Label>
                                                                                    </td>
                                                                                    </tr>
                                                                                <tr id="rowSendInvoiceTo" runat="server" visible="false">
                                                                                <td colspan="2" >
                                                                                    <table width="100%" border="0"  >
                                                                                        <tr><td colspan="2" runat="server">
                                                                                            <asp:Label ID="lblSendInvoiceTo" runat="server" Text="Send Invoice To:" meta:resourcekey="lblSendInvoiceToResource2"></asp:Label>
                                                                                        </td></tr>
                                                                                        <tr>
                                                                                            <td><asp:Label ID="lblName" runat="server" Text="Name:" meta:resourcekey="lblNameResource1" ></asp:Label></td>
                                                                                            <td runat="server"><asp:TextBox ID="txtPmtName" Text='<%# Bind("name") %>' CssClass="contractBodyText" runat="server" MaxLength="256" 
                                                                                            meta:resourcekey="txtPmtNameResource1"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td runat="server"><asp:Label ID="lblAddress1" runat="server" Text="Address1:" meta:resourcekey="lblAddress1Resource2"  ></asp:Label></td>
                                                                                            <td runat="server"><asp:TextBox ID="txtPmtAddress1" Text='<%# Bind("address1") %>' CssClass="contractBodyText" runat="server" MaxLength="256" 
                                                                                            meta:resourcekey="txtPmtAddress1Resource1"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td runat="server"><asp:Label ID="lblAddress2" runat="server" Text="Address2:" meta:resourcekey="lblAddress2Resource1"></asp:Label></td>
                                                                                            <td runat="server"><asp:TextBox ID="txtPmtAddress2" Text='<%# Bind("address2") %>' CssClass="contractBodyText" runat="server" MaxLength="256" 
                                                                                            meta:resourcekey="txtPmtAddress2Resource1"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr >
                                                                                            <td runat="server"><asp:Label ID="lblCity" runat="server" Text="City:" meta:resourcekey="lblCityResource1"></asp:Label></td>
                                                                                            <td runat="server"><asp:TextBox ID="txtPmtCity" Text='<%# Bind("city") %>' CssClass="contractBodyText" runat="server" MaxLength="100" 
                                                                                            meta:resourcekey="txtPmtCityResource1"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr >
                                                                                            <td runat="server"><asp:Label ID="lblState" runat="server" Text="State:" meta:resourcekey="lblStateResource1"></asp:Label></td>
                                                                                            <td runat="server"><table border="0" cellpadding="0" cellspacing="0"><tr><td><asp:DropDownList ID="ddlPmtState" runat="server" CssClass="contractBodyText" 
                                                                                            meta:resourcekey="ddlPmtStateResource1"></asp:DropDownList></td></tr></table></td>
                                                                                        </tr>
                                                                                        <tr runat="server">
                                                                                            <td runat="server"><asp:Label ID="lblZip" runat="server" Text="Zip Code:" meta:resourcekey="lblZipResource1"></asp:Label></td>
                                                                                            <td runat="server"><asp:TextBox ID="txtPmtZipCode" Text='<%# Bind("zip") %>' CssClass="contractBodyText" runat="server" MaxLength="5"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td runat="server"><asp:Label ID="lblPhone" runat="server" Text="Phone:" meta:resourcekey="lblPhoneResource1"></asp:Label></td>
                                                                                            <td runat="server"><asp:TextBox ID="txtPmtPhone" Text='<%# Bind("phone") %>' CssClass="contractBodyText" runat="server" onblur="textBoxOnBlur(this);" 
                                                                                            MaxLength="14" meta:resourcekey="txtPmtPhoneResource1"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr >
                                                                                            <td runat="server"><asp:Label ID="lblEmail" runat="server" Text="Email:" meta:resourcekey="lblEmailResource1"></asp:Label></td>
                                                                                            <td runat="server"><asp:TextBox ID="txtPmtEmail" Text='<%# Bind("email") %>' CssClass="contractBodyText" runat="server" MaxLength="256" 
                                                                                            meta:resourcekey="txtPmtEmailResource1"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr >
                                                                                            <td runat="server" ><asp:Label ID="lblVerifyEmail" runat="server" Text="Verify Email:" meta:resourcekey="lblVerifyEmailResource1"></asp:Label></td>
                                                                                            <td runat="server" ><asp:TextBox ID="txtPmtVerifyEmail" Text='<%# Bind("email") %>' CssClass="contractBodyText" runat="server" MaxLength="256" 
                                                                                            meta:resourcekey="txtPmtVerifyEmailResource1"></asp:TextBox></td>
                                                                                        </tr>
                                                                                        <tr runat="server">
                                                                                            <td colspan="2" runat="server"><table border="0" cellpadding="0" cellspacing="0"><tr><td><asp:Label ID="lblIsEmpTxExp" runat="server" Text="" 
                                                                                            meta:resourcekey="lblIsEmpTxExpResource1" ></asp:Label></td>
                                                                                               <td><asp:DropDownList ID="ddlTaxExempt" runat="server" CssClass="contractBodyText">
                                                                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                                                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                                  </asp:DropDownList></td></tr></table>
                                                                                           </td>
                                                                                        </tr>
                                                                                        <tr runat="server">
                                                                                            <td colspan="2" runat="server">
                                                                                                <asp:Label ID="lblCopayExixts" runat="server" Text="" meta:resourcekey="lblCopayExixtsResource1"></asp:Label><br />
                                                                                                <table border="0" cellpadding="0" cellspacing="0"><tr><td>
                                                                                                <asp:DropDownList ID="ddlIsCopay" runat="server" CssClass="contractBodyText" onChange="javascript:enableCoPay(this);">
                                                                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                                                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                                </asp:DropDownList></td><td>
                                                                                                <asp:Label ID="lblDollar" runat="server" Text="$"></asp:Label>
                                                                                                <asp:TextBox CssClass="contractBodyText" ID="txtCoPay" Text='<%# Bind("copayValue") %>' runat="server" MaxLength="8"></asp:TextBox></td></tr></table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                </tr>
                                                                                <tr id="rowVoucherNeeded" runat="server" visible="false">
                                                                                <td colspan="2" runat="server">
                                                                                    <table>
                                                                                    <tr>
                                                                                        <td><table border="0" cellpadding="0" cellspacing="0"><tr><td>
                                                                                        <asp:Label ID="lblVoucherNeeded" runat="server" Text="" meta:resourcekey="lblVoucherNeededResource1"></asp:Label></td>
                                                                                            <td><asp:DropDownList ID="ddlVoucher" CssClass="contractBodyText" runat="server" onChange="javascript:displayExpirationDate(this);">
                                                                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                                                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                            </asp:DropDownList></td></tr></table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="rowExpirationDate" runat="server">
                                                                                        <td runat="server"><asp:Label ID="lblExirationDate" runat="server" Text="Expiration Date:" meta:resourcekey="lblExirationDateResource1"></asp:Label>
                                                                                            <uc1:PickerAndCalendar ID="pcVaccineExpirationDate" runat="server" />
                                                                                            <asp:TextBox ID="txtVaccineExpirationDate" runat="server" Visible="False" CssClass="contractBodyText" Width="100px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                          <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr><td>
                                                                                <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="contractBodyText" height="20px" Width="245px" meta:resourcekey="ddlPaymentTypeResource1"></asp:DropDownList>
                                                                            </td></tr>
                                                                          </table>
                                                                        </FooterTemplate>
                                                                        <FooterStyle CssClass="contractRow"></FooterStyle>
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="contractHeaderRow" Width="40%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="contractRow" Width="40%"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Rates" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="contractHeaderRow" ItemStyle-CssClass="contractRow" 
                                                                    FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%" meta:resourcekey="TemplateFieldResource3">
                                                                        <ItemTemplate>
                                                                            <table id="tblPrice" width="100%" border="0">
                                                                                <tr>
                                                                                    <td colspan="2" valign="top" style="text-align: right;">
                                                                                        <asp:Label ID="lblValue" runat="server" CssClass="contractBodyText" meta:resourcekey="lblValueResource1"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:ImageButton ID="imgBtnImmunizationRemove" ImageUrl="images/btn_remove.png" ImageAlign="Right" ToolTip="Remove Immunization"
                                                                                runat="server" AlternateText="Remove Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');"
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" CommandArgument="newImmunization" OnCommand="imgBtnRemoveImmunization_Click" 
                                                                                meta:resourcekey="imgBtnImmunizationRemoveResource1" />
                                                                        </FooterTemplate>
                                                                        <FooterStyle CssClass="contractRow"></FooterStyle>
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="contractHeaderRow" Width="10%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Top" CssClass="contractRow" Width="10%"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="contractHeaderRow" ItemStyle-CssClass="contractRow" 
                                                                    FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="right" ItemStyle-VerticalAlign="Top" ItemStyle-Width="5%" meta:resourcekey="TemplateFieldResource4">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgBtnImmunizationRemove" ImageUrl="images/btn_remove.png"
                                                                            runat="server" AlternateText="Remove Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');"
                                                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" CommandArgument="existingImmunization" OnCommand="imgBtnRemoveImmunization_Click" 
                                                                            meta:resourcekey="imgBtnImmunizationRemoveResource2" />
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:ImageButton ID="imgBtnImmunizationOk" ImageUrl="images/btn_ok.png" CausesValidation="true"
                                                                                runat="server" AlternateText="Add Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_ok.png');"
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_ok_lit.png');" CommandArgument="immunization" OnCommand="imgBtnImmunizationOk_Click" 
                                                                                OnClientClick="return validateImmSelection(this);" meta:resourcekey="imgBtnImmunizationOkResource1" />
                                                                        </FooterTemplate>
                                                                        <FooterStyle CssClass="contractRow"></FooterStyle>
                                                                        <HeaderStyle HorizontalAlign="Right" CssClass="contractHeaderRow" Width="5%"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Top" CssClass="contractRow" Width="5%"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    <table width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; background: #fff; border-collapse: collapse; text-align: left;">
                                                                        <tr>
                                                                            <th width="35%" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">
                                                                                <asp:Label ID="lblImmunizationSelection" runat="server" Text=" Immunization" meta:resourcekey="lblImmunizationSelectionResource1"></asp:Label>
                                                                            </th>
                                                                            <th width="40%" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">
                                                                                <asp:Label ID="lblPaymentMethodSelection" runat="server" Text="Payment Method" meta:resourcekey="lblPaymentMethodSelectionResource1"></asp:Label>
                                                                            </th>
                                                                            <th width="10%" align="center" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">
                                                                                <asp:Label ID="lblemptyRatecolHeader" runat="server" meta:resourcekey="lblemptyRatecolHeaderResource1"></asp:Label>
                                                                            </th>
                                                                            <th width="5%" align="right" valign="top" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">&nbsp;</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="contractRow" style="height:25px; text-align: left; vertical-align: middle;">
                                                                            <table border="0" cellpadding="0" cellspacing="0"><tr><td>
                                                                                <asp:DropDownList ID="ddlImmunizationCheck" runat="server" CssClass="contractBodyText" AutoPostBack="false" onchange="javascript:displayPaymentMethods(this.value);" 
                                                                                height="20px" Width="210px" meta:resourcekey="ddlImmunizationCheckResource2"></asp:DropDownList>
                                                                            </td></tr></table></td>
                                                                            <td class="contractRow" style="height:25px; text-align: left; vertical-align: middle;" ><table border="0" cellpadding="0" cellspacing="0"><tr><td>
                                                                                <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="contractBodyText" height="20px" Width="245px" meta:resourcekey="ddlPaymentTypeResource2"></asp:DropDownList>
                                                                            </td></tr></table>
                                                                            </td>
                                                                            <td class="contractRow" style="text-align: left; vertical-align: middle;">&nbsp;</td>
                                                                            <td class="contractRow" style="text-align: right; vertical-align: middle;">
                                                                                <asp:ImageButton ID="imgBtnImmunizationOk" ImageUrl="images/btn_ok.png" CausesValidation="true" runat="server" AlternateText="Add Immunization" 
                                                                                onmouseout="javascript:MouseOutImage(this.id,'images/btn_ok.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_ok_lit.png');" CommandArgument="immunization" 
                                                                                OnCommand="imgBtnImmunizationOk_Click" OnClientClick="return validateImmSelection(this);" meta:resourcekey="imgBtnImmunizationOkResource2" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td><asp:Label ID="lblImmDisclaimer" runat="server" Text="*Rates includes vaccine and administration." meta:resourcekey="lblImmDisclaimerResource1"></asp:Label></td>
                                                            </tr>
                                                        </table> 
                                                    </td>
                                                </tr>
                                                <tr><td colspan="3">&nbsp;</td></tr>
                                                <tr>
                                                    <td align="left"><b><asp:Label ID="lblClineLocations" runat="server" Text="Client Facility Location(s)*:" meta:resourcekey="lblClineLocationsResource1"></asp:Label></b></td>
                                                    <td align="right" colspan="2">
                                                        <table width="220" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="202" align="right" valign="middle">
                                                                    <asp:LinkButton ID="lnkAddLocation" runat="server" OnClientClick="return disableAddLocations()" onclick="btnAddLocation_Click"  ToolTip="Add Locations" 
                                                                    meta:resourcekey="lnkAddLocationResource1">Add Locations</asp:LinkButton>
                                                                    <asp:DropDownList ID="ddlClinicLocations" runat="server" onchange ="disableAddBtnForMOPrevious()"  CssClass="contractBodyText" Visible ="false"></asp:DropDownList>
                                                                </td>
                                                                <td width="18" align="right" valign="middle">
                                                                    <asp:ImageButton ID="imgbtnAddPreviousClinic" ImageUrl="images/btn_add_business.png" ToolTip="Add Locations" runat="server" AlternateText="Add Location" 
                                                                    onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_business.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_business_lit.png');" Visible="false" 
                                                                    OnClick="imgbtnAddPreviousClinic_Click" />
                                                                    <asp:ImageButton ID="btnAddLocation" ImageUrl="images/btn_add_business.png" ToolTip="Add Locations" runat="server" AlternateText="Add Location" 
                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_business.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_business_lit.png');" 
                                                                        OnClientClick="return disableAddLocations()" onclick="btnAddLocation_Click" meta:resourcekey="btnAddLocationResource1" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td colspan="5">
                                                        <asp:Label ID="lblClinicDateAlert" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:GridView ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" onrowdatabound="grdLocations_RowDataBound" DataKeyNames="state,clinicDate,fluExpiryDate,routineExpiryDate" 
                                                        meta:resourcekey="grdLocationsResource1" >
                                                            <Columns>
                                                                <asp:BoundField DataField="state" Visible="false" meta:resourcekey="BoundFieldResource1" />
                                                                <asp:BoundField DataField="clinicDate" Visible="false" meta:resourcekey="BoundFieldResource2" />
                                                                <asp:BoundField DataField="fluExpiryDate" Visible="false" meta:resourcekey="BoundFieldResource3" />
                                                                <asp:BoundField DataField="routineExpiryDate" Visible="false" meta:resourcekey="BoundFieldResource4" />
                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="100%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource6">
                                                                    <ItemTemplate>
                                                                    <table width="100%"  style="border-style:solid; border-width:0px; border-color:Black">
                                                                        <tr>
                                                                        <td colspan="2" align="left">
                                                                            <asp:Label ID="lblClinicLocation" Text='<%# Bind("clinicLocation") %>' runat="server" style="font-weight:bold" meta:resourcekey="lblClinicLocationResource1"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3" align="right">
                                                                            <asp:CheckBox ID="chkReassignClinic" runat="server" Text="Re-assign based on geography*" Checked='<%# Convert.ToInt32(Eval("isReassign")) == 1 ? true : false %>' 
                                                                            meta:resourcekey="chkReassignClinicResource1" />
                                                                        </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%"  style="border-style:solid; border-width:0px; border-color:Black">
                                                                        <tr>
                                                                            <td colspan="2" align="left">                                                                       
                                                                                <asp:CheckBox ID="chkNoClinic" runat="server" Text="No Clinic (Voucher Distribution Only)" Checked='<%# Convert.ToInt32(Eval("isNoClinic")) == 1 && this.isCTIExists ? true : false %>'
                                                                                    meta:resourcekey="chkNoClinicResource1" Enabled='<%# this.isCTIExists %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%"  style="border-style:solid; border-width:1px; border-color:Black">
                                                                        <tr>
                                                                            <td colspan="5" style="text-align: left;" class="contractBodyText">
                                                                            <asp:GridView ID="grdClinicImmunizations" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdClinicImmunizations_RowDataBound" 
                                                                            meta:resourcekey="grdClinicImmunizationsResource1">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="<b>Estimated Shots per Immunization</b>" meta:resourcekey="TemplateFieldResource5">
                                                                                        <ItemTemplate>                                                                                            
                                                                                                <table width="100%" border="0" >                                                                                                    
                                                                                                    <tr>
                                                                                                        <asp:Label ID="lblImmunizationId" runat="server" Text='<%# Bind("pk") %>' Visible="False" meta:resourcekey="lblImmunizationIdResource1"></asp:Label>
                                                                                                        <asp:Label ID="lblPaymentTypeId" runat="server" Text='<%# Bind("paymentTypeId") %>' Visible="False" meta:resourcekey="lblPaymentTypeIdResource2"></asp:Label>
                                                                                                        <td style="text-align: left; vertical-align: middle; width: 50px; ">
                                                                                                            <asp:TextBox ID="txtClinicImmunizationShots" runat="server" Text='<%# Bind("estimatedQuantity") %>' CssClass="contractBodyText" MaxLength="5" 
                                                                                                            Width="35px" meta:resourcekey="txtClinicImmunizationShotsResource1"></asp:TextBox></td>
                                                                                                        <td style="text-align: left; vertical-align: top; font-weight: bold;">
                                                                                                            <asp:Label ID="lblClinicImmunization" runat="server" CssClass="contractBodyText" meta:resourcekey="lblClinicImmunizationResource1" ></asp:Label><br />
                                                                                                            (<asp:Label ID="lblClinicPaymentType" runat="server" Font-Bold="False" meta:resourcekey="lblClinicPaymentTypeResource1" ></asp:Label>)
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b> <asp:Label ID="lblContactName" runat="server" Text="Local Contact Name" meta:resourcekey="lblContactNameResource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblContactPhone" runat="server" Text="Local Contact Phone" meta:resourcekey="lblContactPhoneResource1"></asp:Label></b></td>
                                                                            <td colspan="3"><b> <asp:Label ID="lblContactEmail" runat="server" Text="Local Contact Email" meta:resourcekey="lblContactEmailResource1"></asp:Label></b></td>                                                                        
                                                                        </tr>
                                                                        <tr>
                                                                            <td><asp:TextBox ID="txtLocalContactName" runat="server" Text='<%# Bind("localContactName") %>'  CssClass="contractBodyText"  Width="130px" MaxLength="100" 
                                                                            meta:resourcekey="txtLocalContactNameResource1" ></asp:TextBox></td>
                                                                            <td><asp:TextBox ID="txtLocalContactPhone" runat="server" Text='<%# Bind("LocalContactPhone") %>'  CssClass="contractBodyText" Width="100px" MaxLength="12" 
                                                                            onblur="textBoxOnBlur(this);" meta:resourcekey="txtLocalContactPhoneResource1" ></asp:TextBox></td>
                                                                            <td colspan="3"><asp:TextBox ID="txtLocalContactEmail" runat="server" Text='<%# Bind("LocalContactEmail") %>'  CssClass="contractBodyText" Width="170px" MaxLength="256" 
                                                                            meta:resourcekey="txtLocalContactEmailResource1" ></asp:TextBox></td>         
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b> <asp:Label ID="lblContactAddress1" runat="server" Text="Address1" meta:resourcekey="lblContactAddress1Resource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblConatctAddress2" runat="server" Text="Address2" meta:resourcekey="lblConatctAddress2Resource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblContactCity" runat="server" Text="City" meta:resourcekey="lblContactCityResource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblContactState" runat="server" Text="State" meta:resourcekey="lblContactStateResource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblContactZip" runat="server" Text="Zip" meta:resourcekey="lblContactZipResource1"></asp:Label></b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("Address1") %>'  CssClass="contractBodyText" Width="130px" MaxLength="256" meta:resourcekey="txtAddress1Resource1" ></asp:TextBox></td>
                                                                            <td><asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("Address2") %>'  CssClass="contractBodyText" Width="100px" MaxLength="50" meta:resourcekey="txtAddress2Resource1" ></asp:TextBox></td>
                                                                            <td><asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("city") %>'  CssClass="contractBodyText" Width="100px" MaxLength="100" meta:resourcekey="txtCityResource1" ></asp:TextBox></td>
                                                                            <td><asp:DropDownList ID="ddlState" runat="server"  CssClass="contractBodyText" Width="95%" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" meta:resourcekey="ddlStateResource1"  ></asp:DropDownList></td>
                                                                            <td><asp:TextBox ID="txtZipCode" runat="server" CssClass="contractBodyText" Width="85%" Text='<%# Bind("zipCode") %>' MaxLength="5" meta:resourcekey="txtZipCodeResource1"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr id="rowClinicDateLabels" runat="server">
                                                                            <td><b> <asp:Label ID="lblContactClinicDate" runat="server" Text="Clinic Date" meta:resourcekey="lblContactClinicDateResource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblContactSTime" runat="server" Text="Start Time" meta:resourcekey="lblContactSTimeResource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblContactETime" runat="server" Text="End Time" meta:resourcekey="lblContactETimeResource1"></asp:Label></b></td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr id="rowClinicDateControls" runat="server">
                                                                            <td><uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" />
                                                                            <asp:TextBox ID="txtCalenderFrom" runat="server" Visible="False" CssClass="contractBodyText" Width="100px" meta:resourcekey="txtCalenderFromResource1"></asp:TextBox></td>
                                                                            <td><asp:TextBox ID="txtStartTime" runat="server"  Text='<%# Bind("startTime") %>' CssClass="contractBodyText" Width="100px" MaxLength="7" OnLoad="displayTime_Picker" 
                                                                            meta:resourcekey="txtStartTimeResource1" ></asp:TextBox></td>
                                                                            <td><asp:TextBox ID="txtEndTime" runat="server" Text='<%# Bind("endTime") %>' CssClass="contractBodyText" Width="100px"  MaxLength="7" 
                                                                            meta:resourcekey="txtEndTimeResource1"></asp:TextBox></td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr id="rowVoucherDateLabels" runat="server">
                                                                            <td><b> <asp:Label ID="lblFluExpiryDate" runat="server" Text="Voucher Flu Exp." Visible='<%# this.isFluImmunizationExists %>' meta:resourcekey="lblFluExpiryDateResource1"></asp:Label></b></td>
                                                                            <td><b> <asp:Label ID="lblRoutineExpiryDate" runat="server" Text="Voucher Routine Exp." Visible='<%# this.isRoutineImmunizationExists %>' meta:resourcekey="lblRoutineExpiryDateResource1"></asp:Label></b></td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr id="rowVoucherDateControls" runat="server">                                                                                   
                                                                            <td><uc1:PickerAndCalendar ID="pcFluExpiryDate" runat="server" Visible='<%# this.isFluImmunizationExists %>' />
                                                                                <asp:TextBox ID="txtFluExpiryDate" runat="server" Visible="False" CssClass="contractBodyText" Width="100px"></asp:TextBox></td>
                                                                            <td><uc1:PickerAndCalendar ID="pcRoutineExpiryDate" runat="server" Visible='<%# this.isRoutineImmunizationExists %>'/>
                                                                                <asp:TextBox ID="txtRoutineExpiryDate" runat="server" Visible="False" CssClass="contractBodyText" Width="100px"></asp:TextBox></td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                                    <br />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" Width="100%"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Top" 
                                                                meta:resourcekey="TemplateFieldResource7">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgBtnRemoveLocation"  ImageUrl="images/btn_remove.png" CausesValidation="true" runat="server" AlternateText="Remove Location" ToolTip="Remove Location" 
                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" 
                                                                        onclick="imgBtnRemoveLocation_Click" meta:resourcekey="imgBtnRemoveLocationResource1" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%"></ItemStyle>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>                                                    
                                                    </td>
                                                </tr>
                                                <tr id="rowReassignClinicStore" runat="server">
                                                    <td colspan="2">
                                                    <asp:Label ID="lblLocationDisclaimer" runat="server" Text="*Clinics will be re-assigned to another store if the clinic location is over 10 miles from the contracting store or crosses state boundaries." 
                                                    meta:resourcekey="lblLocationDisclaimerResource1"></asp:Label></td>
                                                </tr>                      
                                            </table>
                                            <table id="tblAgreement" runat="server" width="100%" border="0">
                                            <tr><td>
                                            <p align="center">
                                                <asp:Label ID="lblLocationDiscliamer2" runat="server" Text="&lt;b&gt;IN WITNESS WHEREOF&lt;/b&gt;, Client and Walgreens have electronically executed this Agreement, as of the Effective Date." 
                                                meta:resourcekey="lblLocationDiscliamer2Resource1"></asp:Label></p>
                                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
                                                <tr>
                                                    <td width="46" align="left" valign="middle">
                                                        <b><asp:Label ID="lblClinet" runat="server" Text="CLIENT:" meta:resourcekey="lblClinetResource1"></asp:Label></b>
                                                    </td>
                                                    <td width="210" align="left" valign="middle">
                                                        <asp:Label ID="lblClient" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblClientResource1" ></asp:Label>
                                                    </td>
                                                    <td colspan="2" align="left" valign="middle">
                                                        <b><u><asp:Label ID="lblWalgreenTitle" runat="server" Text="WALGREEN CO." meta:resourcekey="lblWalgreenTitleResource1"></asp:Label></u></b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblClientUserName" runat="server" Text="NAME:" meta:resourcekey="lblClientUserNameResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblClientName" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblClientNameResource1" ></asp:Label>
                                                    </td>
                                                    <td width="46" align="left" valign="middle">
                                                        <asp:Label ID="lblWGUserName" runat="server" Text="NAME:" meta:resourcekey="lblWGUserNameResource1"></asp:Label>
                                                    </td>
                                                    <td width="210" align="left" valign="middle">
                                                        <asp:TextBox ID="txtWalgreenName" runat="server" Width="200px" CssClass="contractBodyText" ValidationGroup="WalgreensUser" MaxLength="256" meta:resourcekey="txtWalgreenNameResource1" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblClinetUserTitle" runat="server" Text="TITLE:" meta:resourcekey="lblClinetUserTitleResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblClientTitle" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblClientTitleResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblWGUserTitle" runat="server" Text="TITLE:" meta:resourcekey="lblWGUserTitleResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <asp:TextBox ID="txtWalgreenTitle" runat="server" Width="200px" CssClass="contractBodyText" ValidationGroup="WalgreensUser" MaxLength="256" meta:resourcekey="txtWalgreenTitleResource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblClientUserDate" runat="server" Text="DATE:" meta:resourcekey="lblClientUserDateResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblClientDate" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblClientDateResource1" ></asp:Label>
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <asp:Label ID="lblWGUserDate" runat="server" Text="DATE:" meta:resourcekey="lblWGUserDateResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" valign="middle">
                                                        <uc1:PickerAndCalendar ID="PickerAndCalendarWalgreensDate" runat="server" />
                                                        <asp:TextBox ID="txtWalgreensDate" runat="server" Visible="False" CssClass="contractBodyText" Width="200px" meta:resourcekey="txtWalgreensDateResource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="left" valign="middle">
                                                        <u><asp:Label ID="lblSendLegalNotices" runat="server" Text="Send Legal Notices To Client At:" meta:resourcekey="lblSendLegalNoticesResource1"></asp:Label></u>
                                                    </td>
                                                    <td colspan="2" align="left" valign="middle">
                                                        <asp:Label ID="lblDistrictNumber" runat="server" Text="DISTRICT NUMBER:" meta:resourcekey="lblDistrictNumberResource1"></asp:Label>&nbsp;
                                                        <asp:TextBox ID="txtDistrictNum" runat="server" Width="103px" CssClass="contractBodyText" ValidationGroup="WalgreensUser" MaxLength="5" meta:resourcekey="txtDistrictNumResource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td colspan="2" align="left" valign="middle" rowspan="3" >
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td><asp:Label ID="lblAttentionTo" runat="server" Text="Attention to:" meta:resourcekey="lblAttentionToResource1"></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLegalAttentionTo" runat="server"  CssClass="contractBodyText" meta:resourcekey="lblLegalAttentionToResource1" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblWalgreenAddress1" runat="server" Text="Address1:" meta:resourcekey="lblWalgreenAddress1Resource1"></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLegalAddress1" runat="server" CssClass="contractBodyText" meta:resourcekey="lblLegalAddress1Resource1" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblWalgreenAddress2" runat="server" Text="Address2:" meta:resourcekey="lblWalgreenAddress2Resource1"></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLegalAddress2" runat="server"  CssClass="contractBodyText" meta:resourcekey="lblLegalAddress2Resource1" ></asp:Label>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblWalgreenCity" runat="server" Text="City:" meta:resourcekey="lblWalgreenCityResource1" ></asp:Label> </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLegalCity" runat="server"  CssClass="contractBodyText" meta:resourcekey="lblLegalCityResource1" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblWalgreenState" runat="server" Text="State:" meta:resourcekey="lblWalgreenStateResource1"></asp:Label></td> 
                                                            <td align="left">
                                                                <asp:Label ID="lblLegalState" runat="server"  CssClass="contractBodyText" meta:resourcekey="lblLegalStateResource1" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:Label ID="lblWalgreenZip" runat="server" Text="Zip Code:" meta:resourcekey="lblWalgreenZipResource1"></asp:Label></td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLegalZipCode" runat="server"  CssClass="contractBodyText" meta:resourcekey="lblLegalZipCodeResource1" ></asp:Label>
                                                                </td>
                                                        </tr>
                                                    </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="left" valign="middle"><u><asp:Label ID="lblWalgreenSendLegalNoticesTo1" runat="server" Text="Send Legal Notices To Walgreens At:" meta:resourcekey="lblWalgreenSendLegalNoticesTo1Resource1"></asp:Label></u></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="left" valign="top">
                                                        Healthcare Innovations Group<br />
                                                        200 Wilmot Rd<br />
                                                        MS2222<br />
                                                        Deerfield, IL 60015<br />
                                                        Attn: Health Law – Divisional Vice President<br />
                                                        cc: clinicalcontracts@walgreens.com</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                </tr>
                                            </table>
                                            <p align="center"><b><asp:Label ID="lblAgreementTC" runat="server" Text="" meta:resourcekey="lblAgreementTCResource1"></asp:Label></b></p>
                                            <table width="540" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="47%" align="left" valign="top">
                                                        <p style="text-align:justify">
                                                            <asp:Label ID="lblTC1" runat="server" meta:resourcekey="lblTC1Resource1">
                                                            </asp:Label>
                                                        </p>
                                                        <p style="text-align:justify">
                                                            <asp:Label ID="lblTC2" runat="server" meta:resourcekey="lblTC2Resource1">
                                                            </asp:Label>
                                                        </p>
                                                        <p style="text-align:justify">
                                                            <asp:Label ID="lblTC3" runat="server" meta:resourcekey="lblTC3Resource1">
                                                            </asp:Label>
                                                        </p>
                                                       
                                                    </td>
                                                    <td width="6%" align="left" valign="middle">&nbsp;</td>
                                                    <td width="47%" colspan="2" align="left" valign="top">
                                                        <p style="text-align:justify">
                                                            <asp:Label ID="lblTC4" runat="server" meta:resourcekey="lblTC4Resource1">
                                                            </asp:Label>
                                                        </p>
                                                        <p style="text-align:justify">
                                                            <asp:Label ID="lblTC5" runat="server" meta:resourcekey="lblTC5Resource1">
                                                            </asp:Label>
                                                        </p>
                                                        <p style="text-align:justify">
                                                            <asp:Label ID="lblTC6" runat="server" meta:resourcekey="lblTC6Resource1">
                                                            </asp:Label>
                                                        </p>
                                                        <p style="text-align:justify">
                                                            <asp:Label ID="lblTC7" runat="server" meta:resourcekey="lblTC7Resource1">
                                                            </asp:Label>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">&nbsp;</td>
                                                    <td align="left" valign="middle">&nbsp;</td>
                                                </tr>
                                            </table>
                                            <p align="right">
                                                <span style="font-size: 10px;"><asp:Label ID="lblCopyRights" runat="server" Text="" meta:resourcekey="lblCopyRightsResource1"></asp:Label></span>
                                            </p>
                                            </td></tr></table>
                                       
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </table>
                        </td>
                    </tr>
                    </table>
                    </td>
                    <td valign="top" width="268" style="padding-top:20px"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                        <tr>
                            <td>
                                <table border="0" cellspacing="12">
                                <tr>
                                    <td  align="left" valign="top">
                                        <asp:Label ID="lblErrorMessage" runat="server" class="bestPracticesText" ForeColor="Red" meta:resourcekey="lblErrorMessageResource1" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top">
                                        <table runat="server" id="tblEmails" ><tr><td><span class="bestPracticesText">
                                        <b><asp:Label ID="lblEmailAgreementTo" runat="server" Text="" meta:resourcekey="lblEmailAgreementToResource1"></asp:Label>
                                            <asp:TextBox ID="txtEmails" runat="server"  CssClass="bestPracticesText" ValidationGroup="WalgreensUser" Width="205px" meta:resourcekey="txtEmailsResource1"></asp:TextBox>
                                            </b></span><br />
                                            <span class="noteText"><asp:Label ID="lblEmailAgreementToInformation" runat="server" Text="" meta:resourcekey="lblEmailAgreementToInformationResource1"></asp:Label></span>
                                        </td></tr></table>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px">
                                        <table border="0" cellspacing="0" cellpadding="1" id="tblApproval" runat="server" visible="false">
                                            <tr>
                                                <td>
                                                    <b><asp:RadioButton ID="rbtnApprove" CssClass="formFields" runat="server" Text="Approve" GroupName="agreement" meta:resourcekey="rbtnApproveResource1" /></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" id="tdElectronicSignature"><asp:Label ID="lblElectronicSignature" runat="server" Text="" meta:resourcekey="lblElectronicSignatureResource1"></asp:Label> &nbsp;
                                                    <asp:TextBox ID="txtElectronicSign" runat="server" MaxLength="500" CssClass="bestPracticesText" Style="background-color: #FF0" Width="90%" meta:resourcekey="txtElectronicSignResource1"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b><asp:RadioButton ID="rbtnReject" CssClass="formFields" runat="server" Text="" GroupName="agreement" meta:resourcekey="rbtnRejectResource1" /></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trNotes"  runat="server" visible="false">
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px"><asp:Label ID="lblNotes" runat="server" Text="" meta:resourcekey="lblNotesResource1"></asp:Label><br />
                                        <label for="textarea"></label>
                                        <asp:TextBox ID="txtNotes" class="formFields" runat="server" Columns="75" Rows="5" TextMode="MultiLine" Width="210px" meta:resourcekey="txtNotesResource1"></asp:TextBox>
                                        <asp:RegularExpressionValidator ControlToValidate="txtNotes" ID="txtNotesREV" runat="server" Display="None" ValidationExpression="[^<>]+" ErrorMessage="Notes:: < > Characters are not allowed." 
                                        ValidationGroup="WalgreensUser" meta:resourcekey="txtNotesREVResource1" ></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center"  class="bestPracticesText" >
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="btnSendMail" ImageUrl="images/btn_send_email.png" runat="server" AlternateText="Send Email" ToolTip="Send Email" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_send_email.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_send_email_lit.png');" 
                                            OnClick="btnSendMail_Click" ValidationGroup="WalgreensUser" ImageAlign="Middle" OnClientClick="return CustomValidatorForLocations('WalgreensUser',this);" meta:resourcekey="btnSendMailResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                     <asp:ImageButton ID="btnSendLater" ImageUrl="images/btn_save_send_later.png" runat="server" AlternateText="Send Later" ToolTip="Send Later" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_send_later.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_send_later_lit.png');" 
                                            onclick="btnSendLater_Click"  ValidationGroup="SendLater" ImageAlign="Middle"  OnClientClick="return CustomValidatorForLocations('SendLater',this);" meta:resourcekey="btnSendLaterResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="btnCancel" ImageUrl="images/btn_cancel_mini.png" CausesValidation="False" runat="server" AlternateText="Cancel" ToolTip="Cancel" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_mini.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_mini_lit.png');" 
                                            onclick="btnCancel_Click" ImageAlign="Middle" meta:resourcekey="btnCancelResource1" />
                                                </td>
                                            </tr>
                                        </table>
                                       </td>
                                    </tr>
                                    
                                <tr>
                                    <td colspan="2" align="left" valign="top"><span class="noteText"><asp:Label ID="lblAgreementUserDisclaimer" runat="server" Text="" meta:resourcekey="lblAgreementUserDisclaimerResource1"></asp:Label></span>
                                    </td>
                                </tr>
                                </table>
                            </td>
                        </tr>
                    </table></td>
                </tr>                   
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    <div id="divConfirmDialog" style="display: none; font-family: Arial; font-size: 13px; text-align: left; "></div>
    </form>
</body>
</html>