﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensCharityProgram.aspx.cs"   Inherits="walgreensCharityProgram" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="uc2" %>
<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link rel="stylesheet" type="text/css" href="css/wags.css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css" />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css" />
        
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/jquery.printelement.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />

    <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>

    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
	<link rel="stylesheet"  href="css/jquery-ui.css" type="text/css" />
    <style type="text/css">
        .ui-dialog-titlebar-close {
            visibility: hidden;
        }
        .ui-timepicker-list li
        {
            color: #000000;
            font-family: "Times New Roman",Times,serif;
            font-size: 11px;
        }
        .ui-widget-header
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 13px;
            font-weight: bold;
        }  
        .ui-widget
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            font-weight: normal;
        }
        .tooltipText
        {
            font-weight:bold;
            font-size:11px;
            color:red;
            position:absolute;
            white-space: pre-wrap;
            max-width:180px;
            padding:2px;
            border-radius:0px;
            -webkit-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            -moz-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtOthers1").html($("#txtOther1"));
            $("#txtOthers2").html($("#txtOther2"));

            if ($.browser.msie) {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keydown(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keydown(function (event) { event.preventDefault(); });
            }
            else {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keypress(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keypress(function (event) { event.preventDefault(); });
            }

            $("#grdLocations").find("input[type=text][id*=txtEstShots]").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });
            $("#grdLocations").find("input[type=text][id*=txtVouchersDistributed]").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });
        });

        function AlertMessage(alert_message, clinic_location, message) {
            if (alert_message != "")
                alert_message = "\r\n" + clinic_location + ": " + message;
            else
                alert_message = clinic_location + ": " + message;

            return alert_message;
        }

        var setInvalidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "background-color": "yellow" });

            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
                    }
            ctrl.tooltip({
                tooltipClass: "tooltipText"
            });
            ctrl.focus(function (evt) {
                $(evt.currentTarget).tooltip("close");
            });
            ctrl.tooltip("enable");
                            }
        var setValidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "border": "1px solid gray" });
            ctrl.css({ "background-color": "" });
            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
                        }
            ctrl.removeAttr("title");
            ctrl.tooltip({
                disabled: true
            });
            ctrl.on("click", function () {
                ctrl.data("title", ctrl.attr("title")).removeAttr("title");
            }, function () {
                var title = ctrl.data("title");
                ctrl.tooltip("option", "content", title || ctrl.attr("title"));
            });

        }
        function CustomValidatorForLocations(source, arguments) {
            var validating_fields = $("form :text, textarea, select");
            var validationGroup = source;
            var is_MO_state = $('#hfisMOState').val().toLowerCase() === 'true';
            //var is_required = !$("#grdLocations").find("input[type=checkbox][id*=chkNoClinic]")[0].checked;
            var is_required=false;
            var validating_field_names = {
                    //Clinic Location details
                'txtLocalContactName': { 'isRequired': is_required, 'when': 'always', 'name': 'Contact Name', 'type': 'junk', 'requiredMessage': 'Contact Name is required', 'invalidMessage': 'Contact Name: < > characters are not allowed' },
                'txtAddress1': { 'isRequired': (is_MO_state ? true : is_required), 'when': 'always', 'name': 'Address1', 'type': 'address', 'requiredMessage': 'Address1 is required', 'invalidMessage': 'Address1: , < > characters are not allowed' },
                'txtAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'address', 'requiredMessage': '', 'invalidMessage': 'Address2: , < > characters are not allowed' },
                'txtCity': { 'isRequired': (is_MO_state ? true : is_required), 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': 'City is required', 'invalidMessage': 'City: < > characters are not allowed' },
                'txtLocalContactPhone': { 'isRequired': is_required, 'when': 'always', 'name': 'Contact Phone', 'type': 'phone', 'requiredMessage': 'Phone number is required', 'invalidMessage': 'Valid Phone number is required(ex: ###-###-####)' },
                'txtZipCode': { 'isRequired': (is_MO_state ? true : is_required), 'when': 'always', 'name': 'ZipCode', 'type': 'zipcode', 'requiredMessage': 'Zip Code is required', 'invalidMessage': 'Invalid Zip Code' },
                'txtLocalContactEmail': { 'isRequired': is_required, 'when': 'always', 'name': 'Contact Email', 'type': 'email', 'requiredMessage': 'Email is required', 'invalidMessage': 'Invalid Email' },
                'txtEstShots': { 'isRequired': is_required, 'when': 'always', 'name': 'Estimated shots', 'type': 'numeric', 'requiredMessage': 'Estimated Shots is required (ex: #####)', 'invalidMessage': 'Invalid Estimated Shots value' },
                'ddlState': { 'isRequired': (is_MO_state ? true : is_required), 'when': 'always', 'name': 'state', 'type': 'select', 'requiredMessage': 'Select State', 'invalidMessage': '' },
                'txtStartTime': { 'isRequired': is_required, 'when': 'always', 'name': 'Start Time', 'type': '', 'requiredMessage': 'Start Time is required', 'invalidMessage': '' },
                'txtEndTime': { 'isRequired': is_required, 'when': 'always', 'name': 'End Time', 'type': '', 'requiredMessage': 'End Time is required', 'invalidMessage': '' },
                'PickerAndCalendarFrom': { 'isRequired': is_required, 'when': 'always', 'name': 'Date Time', 'type': '', 'requiredMessage': 'Clinic Date is required', 'invalidMessage': '' },
                'txtVouchersDistributed': { 'isRequired': true, 'when': 'always', 'name': 'Vouchers Distributed', 'type': 'numeric', 'requiredMessage': 'Please enter the number of Vouchers Distributed', 'invalidMessage': 'Invalid Number of Vouchers' }
                };
            var first_identified_control = "";
            var is_valid = true;
            var ctrl_Id;
            var indexOf_Underscore;
            var txtPmtEmail;
            var trimmedValue;
                $.each(validating_fields, function (index, ctrl) {
                    setValidControlCss($(ctrl), false);
                    $(ctrl).attr({ "title": "" });
                    ctrl_Id = $(ctrl).attr('id');

                    if (ctrl_Id.indexOf('grdLocations') > -1) {
                        is_required = !$("input[type=checkbox][id=" + ctrl_Id.substring(0, ctrl_Id.indexOf('_', ctrl_Id.indexOf('_') + 1)) + "_chkNoClinic]")[0].checked;
                        if (ctrl_Id.toLowerCase().indexOf('_picker') > -1) {
                            var pos = 0;
                            var count = 0;
                            while (true) {
                                pos = ctrl_Id.indexOf('_', pos + 1);
                                ++count;
                                if (count == 2) {
                                    break;
                            }
                        }
                            ctrl_Id = ctrl_Id.substring(pos + 1, ctrl_Id.indexOf('_', pos + 1))
                            }
                            else {
                            indexOf_Underscore = ctrl_Id.lastIndexOf('_');
                            if (ctrl_Id.length > indexOf_Underscore + 1) {
                                ctrl_Id = ctrl_Id.substring(indexOf_Underscore + 1);
                                }
                        }

                        if (validating_field_names[ctrl_Id].isRequired != is_required && ctrl_Id != 'txtAddress2' && ctrl_Id != 'txtVouchersDistributed')
                            validating_field_names[ctrl_Id].isRequired = is_required;

                       }
                    trimmedValue = $.trim($(ctrl).val());

                    if ((validating_field_names[ctrl_Id] != undefined) && validating_field_names[ctrl_Id].isRequired &&
                        (validating_field_names[ctrl_Id].when == 'always' || $(ctrl).attr('disabled') != 'disabled') &&
                        trimmedValue == "") {
                        if (validating_field_names[ctrl_Id].type == 'select') {
                            if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 6.") != -1) || (navigator.userAgent.indexOf("Trident") != -1)) {
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            }
                            else {
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                                    }
                                }
                                else {
                            setInvalidControlCss($(ctrl), false);
                            if (ctrl_Id == 'PickerAndCalendarFrom')
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            else
                                $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;
                                }

                    else if ((validating_field_names[ctrl_Id] != undefined) && trimmedValue != "") {
                        if (!validateData(validating_field_names[ctrl_Id].type, trimmedValue, (validating_field_names[ctrl_Id].name == 'Estimated shots') ? false : true)) {
                            setInvalidControlCss($(ctrl), false);
                            if (ctrl_Id == 'PickerAndCalendarFrom')
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                            else
                                $(ctrl).attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                            if (first_identified_control == '')
                                first_identified_control = $(ctrl);
                            is_valid = false;
                            }
                        else if (validating_field_names[ctrl_Id].type == 'address') {
                            if (!(validatePO(trimmedValue))) {
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).attr({ "title": '<%= HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert") %>' });
                                if (first_identified_control == '')
                                    first_identified_control = $(ctrl);
                                is_valid = false;
                        }
                            else
                                setValidControlCss($(ctrl), false);
                    }
                }
            });

            if (!is_valid) {
                alert("Highlighted input fields are required/invalid. Please update and submit.");
                first_identified_control[0].focus();
                arguments.IsValid = false;
            return false;
        }
            else {
                arguments.IsValid = true;
                return true;
            }
        }
        function addTime(oldTime) {
            var time = oldTime.split(":");
            var hours = time[0];
            var ampm = time[1].substring(2, 4);
            var minutes = time[1].substring(0, 2);
            var time_new = '';
            if (+minutes >= 30) {
                hours = (+hours + 1) % 24;
            }
            minutes = (+minutes + 30) % 60;
            if (hours >= 12) {
                time_new = hours - 12 + ':' + minutes + 'pm';
            }
            else
                time_new = hours + ':' + minutes + ampm;
            return time_new;
        }
        function disableAddLocations() {
            var is_disable = false;
            if ($("#hfDisableAddLocations").val() != "") {
                var store_message = $("#hfDisableAddLocations").val();
                is_disable = true;
                alert(store_message);
            }

            return !is_disable;
        }

        function showClinicDateReminder(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: "IMPORTANT REMINDER",
                    buttons: {
                        'Ok': function () {
                            __doPostBack("ContinueSaving");
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
            return false;
        }

        function showClinicDateReminderWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: "IMPORTANT REMINDER",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("ContinueSaving");
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
            return false;
        }

    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfcontactLogPk" runat="server" />
        <asp:HiddenField ID="hfBusinessName" runat="server" />
        <asp:HiddenField id="hfisMOState" runat="server" Value="false" />
        <asp:HiddenField ID="hfDisableAddLocations" runat="server" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" 
                                ShowSummary="false" ValidationGroup="WalgreensUser" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <uc2:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="left">
              <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td colspan="2" valign="top" class="pageTitle">Walgreens Charity (HHS Voucher)</td>
                    </tr>
                    <tr>
                      <td colspan="2" valign="top" style="border:#CCC solid 1px; background-color:#b4df92; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>ATTENTION:</b> Please enter the number of Vouchers Distributed and Estimated Shots below, even if a clinic is <b>not</b> conducted.</td>
                    </tr>
                    <tr>
                      <td valign="top" width="599" ><table width="599" border="0" cellpadding="0">
                        <tr>
                          <td>
                            <table width="612" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="612" border="0" cellspacing="0" cellpadding="12" id="trContractBodyText" style="border:#CCC solid 1px" >
                                <tr>
                                    <td class="contractBodyText">
                                        <p align="center">
                                            <b>
                                                <img src="images/contract_logo.gif" width="191" height="37" alt="Walgreens" /><br />
                                                CHARITY (HHS VOUCHER)</b></p>
                                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="5" id="tblBusinesses" runat="server">
                                            <tr>
                                                <td colspan="2" align="left">
                                                    <b>Client Facility Location(s)*:</b>
                                                </td>
                                                <td colspan="4" align="right">
                                                    <table width="100" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="82" align="right" valign="middle">
                                                                <asp:LinkButton ID="lnkAddLocation" runat="server" OnClientClick="return disableAddLocations()" onclick="btnAddLocation_Click">Add Locations</asp:LinkButton>&nbsp;
                                                            </td>
                                                            <td width="18" align="right" valign="middle">

                                                                <asp:ImageButton ID="btnAddLocation" ImageUrl="images/btn_add_business.png" CausesValidation="true"
                                                                    runat="server" AlternateText="Add Location" onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_business.png');"
                                                                    onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_business_lit.png');" 
                                                                    onclick="btnAddLocation_Click" OnClientClick="return disableAddLocations()" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>                                            
                                            <tr>
                                                <td colspan="6" >
                                                    <asp:Label ID="lblClinicDateAlert" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <asp:GridView ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                        Width="100%" onrowdatabound="grdLocations_RowDataBound" DataKeyNames="clinicState,clinicDate" >
                                                        <Columns>
                                                            <asp:BoundField DataField="clinicState" Visible="false" />
                                                            <asp:BoundField DataField="clinicDate" Visible="false" />
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="90%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                <asp:Label ID="lblClinicLocation" Text='<%# Bind("clinicLocation") %>' runat="server" style="font-weight:bold"></asp:Label>    
                                                                <table width="100%"  style="border-style:solid; border-width:0px; border-color:Black">
                                                                   <tr>
                                                                        <td colspan="2" align="left">                                                                       
                                                                            <asp:CheckBox ID="chkNoClinic" runat="server" Text="No Clinic (Voucher Distribution Only)" Checked='<%# Convert.ToInt32(Eval("isNoClinic")) == 1 ? true : false %>' />
                                                                        </td>
                                                                   </tr>
                                                                </table>
                                                               <table width="100%"  style="border-style:solid; border-width:1px; border-color:Black">
                                                                     <tr>
                                                                        <td><b> Local Contact Name</b></td>
                                                                        <td><b> Local Contact Phone</b></td>
                                                                        <td colspan="3"><b> Local Contact Email</b></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:TextBox ID="txtLocalContactName" runat="server" Text='<%# Bind("clinicContactName") %>'  CssClass="contractBodyText"  Width="130px" MaxLength="100" ></asp:TextBox></td>
                                                                        <td><asp:TextBox ID="txtLocalContactPhone" runat="server" Text='<%# Bind("clinicContactPhone") %>'  CssClass="contractBodyText" Width="100px" MaxLength="14" onblur="textBoxOnBlur(this);" ></asp:TextBox></td>
                                                                        <td colspan="3"><asp:TextBox ID="txtLocalContactEmail" runat="server" Text='<%# Bind("clinicContactEmail") %>'  CssClass="contractBodyText" Width="170px" MaxLength="256" ></asp:TextBox></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Address1</b></td>
                                                                        <td><b>Address2</b></td>
                                                                        <td><b>City</b></td>
                                                                        <td><b>State</b></td>
                                                                        <td><b>Zip</b></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("clinicAddress1") %>'  CssClass="contractBodyText" Width="130px" MaxLength="256" ></asp:TextBox></td>
                                                                        <td><asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("clinicAddress2") %>'  CssClass="contractBodyText" Width="100px" MaxLength="50" ></asp:TextBox></td>
                                                                        <td><asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("clinicCity") %>'  CssClass="contractBodyText" Width="100px" MaxLength="50" ></asp:TextBox></td>
                                                                        <td><asp:DropDownList ID="ddlState" runat="server"  CssClass="contractBodyText" Width="95%"   AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"></asp:DropDownList></td>
                                                                        <td><asp:TextBox ID="txtZipCode" runat="server" CssClass="contractBodyText" Width="85%" Text='<%# Bind("clinicZipCode") %>' MaxLength="5"></asp:TextBox></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Clinic Date</b></td>
                                                                        <td><b>Start Time</b></td>
                                                                        <td><b>End Time</b></td>
                                                                        <td><b>Est. Shots:</b></td>
                                                                        <td><b>Vouchers <br />Distributed:</b></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" /><asp:TextBox ID="txtCalenderFrom" runat="server" Visible="false" CssClass="contractBodyText" Width="100px"></asp:TextBox></td>
                                                                        <td><asp:TextBox ID="txtStartTime" runat="server"  Text='<%# Bind("clinicStartTime") %>' CssClass="contractBodyText" Width="100px" MaxLength="7" OnLoad="displayTime_Picker" ></asp:TextBox></td>
                                                                        <td><asp:TextBox ID="txtEndTime" runat="server" Text='<%# Bind("clinicEndTime") %>' CssClass="contractBodyText" Width="100px"  MaxLength="7"></asp:TextBox></td>
                                                                        <td><asp:TextBox ID="txtEstShots" runat="server" CssClass="contractBodyText" Width="55px" Text='<%# Bind("EstShots") %>' MaxLength="5"></asp:TextBox></td>
                                                                        <td><asp:TextBox ID="txtVouchersDistributed" runat="server" CssClass="contractBodyText" Width="55px" Text='<%# Bind("VouchersDistributed") %>' MaxLength="5"></asp:TextBox></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                                <br />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgBtnRemoveLocation" 
                                                                        ImageUrl="images/btn_remove.png" CausesValidation="true"
                                                                        runat="server" AlternateText="Remove Location" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');"
                                                                        onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" onclick="imgBtnRemoveLocation_Click" 
                                                                        />
                                                                    </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <asp:CustomValidator ID="grdLocationsCV" runat="server" Display="None"  ValidationGroup="WalgreensUser" OnServerValidate="ValidateLocations"></asp:CustomValidator>                                                    
                                                </td>
                                            </tr>                                        
                                        </table>
                                        <table id="tblAgreement" runat="server" width="100%" border="0"><tr></tr></table>                                       
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                          </td>
                        </tr>
                      </table></td>
                      <td valign="top" width="268" style="padding-top:20px"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                            <tr>
                              <td>
                                 <table cellspacing="5" cellpadding="0" align="center">
                                <tr>
                                    <td colspan="2" align="center" valign="top" class="bestPracticesText">
                                    <asp:ImageButton ID="btnScheduleClinic" ImageUrl="images/btn_submit_details.png" CausesValidation="true"
                                            runat="server" AlternateText="Submit" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_details.png');"
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_details_lit.png');" OnClientClick="return CustomValidatorForLocations('WalgreensUser',this);" 
                                            onclick="btnScheduleClinic_Click" ValidationGroup="WalgreensUser" />
                                     </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" valign="top" class="bestPracticesText">
                                       <asp:ImageButton ID="btnCancel" ImageUrl="images/btn_cancel_mini.png" CausesValidation="false"
                                            runat="server" AlternateText="Cancel" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_mini.png');"
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_mini_lit.png');" 
                                            onclick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                              </td>
                            </tr>
                      </table></td>
                    </tr>                   
                </table>
            </td>
        </tr>
    </table>
        <div id="divConfirmDialog" style="display: none; font-family: Arial; font-size: 13px;
        text-align: left; ">
        </div>
        <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
