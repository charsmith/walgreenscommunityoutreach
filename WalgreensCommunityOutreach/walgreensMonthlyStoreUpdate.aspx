﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensMonthlyStoreUpdate.aspx.cs" Inherits="walgreensMonthlyStoreUpdate" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Src="~/controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="~/controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Store Profile Upload</title>
    <link rel="stylesheet" type="text/css" href="../css/wags.css" />
    <link rel="stylesheet" type="text/css" href="../css/ddcolortabs.css" />
    <link rel="stylesheet" type="text/css" href="../css/theme.css" />
    <link rel="stylesheet" type="text/css" href="../css/calendarStyle.css" />
    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />
    <link href="../css/gridstyles.css" rel="stylesheet" type="text/css" />

    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="../javaScript/grid.locale-en.js" type="text/javascript"></script>
    <script src="../javaScript/jquery.jqGrid.src.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../themes/ui.jqgrid.css" />

    <script type="text/javascript">
        var selectedRemovedStores = [];        
        function bindUpdatedStoresData() {
            var nc_data = "";
            nc_data = <%= this.storeUpdateReportData %>;
            selectedRemovedStores = [];
            var mydata = {};
            
            varColModel = [
                                     {  name: 'id',
                                         jsonmap: 'id',
                                         index: 'id', 
                                         hidden: true
                                     },
                                     {  name: 'type', 
                                         jsonmap: 'type',
                                         width: 300, 
                                         index: 'type', 
                                         search: true,
                                         editable: false
                                     },
                                     {  name: 'updatedValue',
                                         jsonmap: 'updatedValue',
                                         width: 300, 
                                         index: 'updatedValue', 
                                         search: true,
                                         editable: false
                                     },
                                     {  name: 'storeId',
                                         jsonmap: 'storeId',
                                         width: 200,
                                         index: 'storeId',
                                         sorttype:'int',
                                         search: true,
                                         //key: true,
                                         editable: true,
                                         editrules : { required: true, number:true}
                                     },
                                     {  name: 'address', 
                                         jsonmap: 'address', 
                                         width: 500,
                                         index: 'address', 
                                         search: true,
                                         editable: true,
                                         editrules : { required: true}
                                     },
                                     {  name: 'city', 
                                         jsonmap: 'city', 
                                         width: 350,
                                         index: 'city', 
                                         search: true,
                                         editable: true,
                                         editrules : { required: true}
                                     },
                                     {  name: 'state', 
                                         jsonmap: 'state', 
                                         index: 'state', 
                                         search: true,
                                         editable: true,
                                         editrules : { required: true}
                                     },
                                     {  name: 'zip', 
                                         jsonmap: 'zip', 
                                         index: 'zip', 
                                         search: true,
                                         editable: true,
                                         editrules : { required: true, number:true}
                                     },
                                     {  name: 'created', 
                                         width: 250,
                                         index: 'created',
                                         formatter: 'date', 
                                         datefmt: 'M/d/Y',
                                         hidden: true
                                     },
                                     {  name: 'districtNumber', 
                                         jsonmap: 'districtNumber', 
                                         index: 'districtNumber', 
                                         search: true,
                                         editable: true,
                                         editrules : { required: true, number:true}
                                     },
                                     {   name: 'districtName', 
                                         jsonmap: 'districtName', 
                                         width: 300,
                                         index: 'districtName', 
                                         search: true
                                         ,editable: true
                                         ,editrules : { required: true}
                                     },
                                     {  name: 'areaNumber', 
                                         jsonmap: 'areaNumber', 
                                         index: 'areaNumber', 
                                         search: true,
                                         editable: true,
                                         editrules : { required: true, number:true}
                                     },
                                     {  name: 'areaName', 
                                         jsonmap: 'areaName', 
                                         width: 300, 
                                         index: 'areaName', 
                                         search: true
                                         ,editable: true
                                         ,editrules : { required: true}
                                     },
                                     {  name: 'marketNumber', 
                                         jsonmap: 'marketNumber', 
                                         index: 'marketNumber', 
                                         search: true,
                                         editable: true,
                                         editrules : { required: true, number:true}
                                     }
                                     ,{  name: 'marketname',  
                                         jsonmap: 'marketname',  
                                         width: 300,
                                         index: 'marketname', 
                                         search: true
                                         ,editable: true
                                         ,editrules : { required: true}
                                     }
            ];            
  
            if(nc_data != "")
                mydata = JSON.stringify(<%= this.storeUpdateReportData %>);        
            var jsonurl = "";
            jsonurl = $('#hfJsonFileName').val();
            jQuery("#gridMainStoreData").jqGrid({
              
                url : window.location.protocol +'//'+ window.location.host + '/storeUpdateFiles/StoreProfileHandler.ashx',                
                editurl: window.location.protocol +'//'+ window.location.host + '/storeUpdateFiles/StoreProfileHandler.ashx',
                datatype: "json",
                colModel: varColModel, 
                sortname: 'storeID',
                sortorder: "asc",
                loadonce: true,
                jsonReader: {
                    repeatitems: false,
                    root: function (obj) { return obj; },
                    page: function (obj) { return  jQuery("#gridMainStoreData").jqGrid('getGridParam', 'pagerSD'); },
                    total: function (obj) { return Math.ceil(obj.length /  jQuery("#gridMainStoreData").jqGrid('getGridParam', 'rowNum')); },
                    records: function (obj) { return obj.length; }
                },
                viewrecords: true,
                width: 780,
                height: 'auto',
                rowNum: 10,
                pager: '#pagerSD', 
                loadui: 'disable',
                gridview: true,
                autowidth: true,
                rownumbers: true,
                autoencode : true,
                sortable: false,
                toppager: true,
                scrollOffset: 0,
                columnReordering: false,
                ignoreCase: true,  
                colNames:  ['id', 'Type', 'Updated Value', 'Store ID', 'Address', 'City', 'State', 'Zip', 'created','District Number', 'District Name', 'Area Number', 'Area Name', 'Region Number', 'Region Name']
               
            });
            jQuery("#gridMainStoreData").jqGrid('navGrid', '#pagerSD', {
                edit: false, add: true, del: true, search: true, refresh: false, view: false, position: "left", cloneToTop: false
            },
            // options for the Edit Dialog
                {
                    editCaption: "The Edit Dialog",
                    recreateForm: true,
                    checkOnUpdate : true,
                    checkOnSubmit : true,
                    closeAfterEdit: true,
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    },
                    closeOnEscape: true,//Closes the popup on pressing escape key
                    reloadAfterSubmit: true,
                    drag: true,
                    afterSubmit: function (response, postdata) {
                        if (response.responseText == "") {

                            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');//Reloads the grid after edit
                            return [true, '']
                        }
                        else {
                            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid'); //Reloads the grid after edit
                            return [false, response.responseText]//Captures and displays the response text on th Edit window
                        }
                    },
                    editData: {
                        EmpId: function () {
                            var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                            var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'id');
                            return value;
                        }
                    }
                },
                // options for the Add Dialog
                {
                    closeAfterAdd: true,
                    recreateForm: true,
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    },
                    afterSubmit: function (response, postdata) {
                        if (response.responseText == "") {

                            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid',[{page:1}])
                            return [true, '']
                        }
                        else {
                            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid',[{page:1}])
                            return [false, response.responseText]
                        }
                    }
                },
                // options for the Delete Dailog
                {
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    },
                    closeOnEscape: true,
                    closeAfterDelete: true,
                    reloadAfterSubmit: true,
                    closeOnEscape: true,
                    drag: true,
                    afterSubmit: function (response, postdata) {
                        if (response.responseText == "") {

                            $("#jQGridDemo").trigger("reloadGrid", [{ current: true}]);
                            return [false, response.responseText]
                        }
                        else {
                            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid',[{page:1}])
                            return [true, response.responseText]
                        }
                    },
                    delData: {
                        EmpId: function () {
                            var sel_id = $('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                            var value = $('#jQGridDemo').jqGrid('getCell', sel_id, 'id');
                            return value;
                        }
                    }
                }
            );
        }
        function submitStoreUpdateWarning(alert){
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 350,
                    title: "Store Profile Update",
                    buttons: {
                        'Yes': function () {
                            __doPostBack('StoreUpdate');
                            $(this).dialog('close');
                        },
                        No: function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }  
        function validateUploadFile() {
            //Validate File type
            var fileExtension = ['txt'];
            if ($.inArray($('#storeProfileUploadControl').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Please select only txt files.");
                return false;
            }
            else {
                var fileContent = $('#storeProfileUploadControl').val();
                if (fileContent != null && fileContent != undefined)
                    document.getElementById("btnUpload").click();                
            }
        }
        $(document).ready(function () {
            bindUpdatedStoresData();
            var chk_select_all_onsite_stores = $("input[id$='chkSelectAllOnsiteStores']");
            $("table[id$='grdStoreUploadData'] INPUT[type='checkbox']").click(function (e) {
                if ($("table[id$='grdStoreUploadData'] INPUT[id*='chkSelectOnsiteStore']").length == $("table[id$='grdStoreUploadData'] INPUT[id*='chkSelectOnsiteStore']:checked").length) {
                    $('input:checkbox').prop('checked', this.checked);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_select_all_onsite_stores.attr('checked', false);
                    }
                }
            });
            $("input[id$='chkSelectAllOnsiteStores']").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            if( $("#pg_pagerSD").length > 0)
            {
                if($("input[id$='rbnIsPartial_1']")[0].checked || $("#hfIsStoresUpdated")[0].value == 'true')
                    $("#pg_pagerSD")[0].style.display = 'none';            
                else
                    $("#pg_pagerSD")[0].style.display ="";
            }
            if($("#hfUpdateProgress")[0].value != 0)
            {
                __doPostBack("UpdateInProgress", $("#hfUpdateProgress")[0].value);
            }            
        });   
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form2" runat="server">
        <asp:HiddenField ID="hfUploadedFile" runat="server" />
        <asp:HiddenField ID="hfJsonFileName" runat="server" />
        <asp:HiddenField ID="hfUploadedTime" runat="server" />
        <asp:HiddenField ID="hfIsStoresUpdated" runat="server" Value="false" />
        <asp:HiddenField ID="hfUpdateProgress" runat="server" Value="0" />
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
            <tbody>
               <tr>
                <td colspan="2">
                    <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
                </td>
            </tr>
                <tr>
                    <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table width="935" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" style="background-color: #FFFFFF;">
                                    <table width="900" border="0" cellspacing="8" style="border: 1px solid #999;">
                                        <tr>
                                            <td align="left" class="subTitle">Store Profile Uploader</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" class="logSubTitles">
                                                <asp:Label ID="userMessage" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" nowrap="nowrap" class="formFields">
                                                <asp:Panel ID="pnlUpload" runat="server" Visible="true">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>Select a file to upload: &nbsp;&nbsp;
                                                            </td>
                                                            <td align="left">
                                                                <asp:FileUpload ID="storeProfileUploadControl" runat="server" Style="color: transparent; width: 40%" onchange="validateUploadFile();" CssClass="profileFormObject"></asp:FileUpload>
                                                                <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Style="display: none;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Label runat="server" ID="lblOr" Text="OR"></asp:Label>
                                                <asp:Panel ID="pnlUploadedFiles" runat="server" Visible="true">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left">Select an uploaded feed: &nbsp;&nbsp;
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="ddlUploadedFiles" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUploadedFiles_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; vertical-align: top; padding-top: 10px;" nowrap="nowrap" class="logSubTitles">
                                                <asp:Label ID="lblErrorNote" ForeColor="Red" Text="" runat="server" Width="150px">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" nowrap="nowrap" class="formFields">
                                                <asp:Panel ID="pnlUpdateReport" runat="server" Visible="true">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr style="height: 50px; display: none; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                            <td>Selected File: &nbsp;&nbsp;
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="labelUploadedfile" class="fileInfo" runat="server" Width="350px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 50px; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                            <td>Update Mode: &nbsp;&nbsp;
                                                            </td>
                                                            <td align="left">
                                                                <asp:RadioButtonList runat="server" OnSelectedIndexChanged="rbnIsPartial_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal" ID="rbnIsPartial">
                                                                    <asp:ListItem Text="Full" Selected="True" Enabled="true" Value="false" />
                                                                    <asp:ListItem Text="Partial" Enabled="true" Value="true" />
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:ImageButton ID="btnViewReport" ImageUrl="~/images/btn_view_report.png"
                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_view_report.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_view_report_lit.png');"
                                                        runat="server" AlternateText="View Report" OnCommand="btnSubmitReport_Command" CommandArgument="View" />
                                                    <asp:ImageButton ID="btnSubmitReport" ImageUrl="~/images/btn_submit_img.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_img.png');"
                                                        CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_img_lit.png');"
                                                        runat="server" AlternateText="Submit" OnCommand="btnSubmitReport_Command" CommandArgument="Submit" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>

                                                    <tr>
                                                        <td style="vertical-align: top; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 10px; text-align: left;" class="formFields2">
                                                            <asp:GridView ID="grdStoreUploadData" runat="server" AutoGenerateColumns="False" CellPadding="3" AllowPaging="True" AllowSorting="false" GridLines="None"
                                                                Width="100%" AlternatingRowStyle-BackColor="#E9E9E9" OnRowDataBound="grdStoreUploadData_RowDataBound" PageSize="25" PagerSettings-Mode="NumericFirstLast" OnPageIndexChanging="grdStoreUploadData_PageIndexChanging" OnRowCancelingEdit="grdStoreUploadData_OnRowCancelingEdit" OnSorting="grdStoreUploadData_sorting"
                                                                OnRowEditing="grdStoreUploadData_RowEditing" OnRowUpdating="grdStoreUploadData_RowUpdating" OnRowDeleting="grdStoreUploadData_RowDeleting" ShowHeader="true">
                                                                <FooterStyle CssClass="footerGrey" />
                                                                <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                                                <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" HorizontalAlign="Center" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Is Onsite?" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkSelectAllOnsiteStores" runat="server" />
                                                                            <asp:Label ID="lblIsOnsiteStore" runat="server" Text='Onsite?'></asp:Label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelectOnsiteStore" runat="server" />
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Center" SortExpression="location_number" HeaderStyle-BorderColor="#336699"
                                                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLocationNumber" runat="server" Text='<%# Bind("location_number") %>' Visible="true"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtLocationNumber" Width="50%" type="number" runat="server" Text='<%# Bind("location_number") %>' required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Center" SortExpression="address" HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true"
                                                                        HeaderStyle-BorderColor="#336699" ItemStyle-HorizontalAlign="Left" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("address") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtAddress" Width="100%" runat="server" Text='<%# Bind("address") %>' required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="City" HeaderStyle-HorizontalAlign="Center" SortExpression="city" HeaderStyle-BorderColor="#336699"
                                                                        HeaderStyle-BorderStyle="Solid" ItemStyle-HorizontalAlign="Left" HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCity" runat="server" Text='<%# Bind("city") %>' Width="85px" class="wrapword"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtCity" Width="100%" runat="server" Text='<%# Bind("city") %>' required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="State" HeaderStyle-HorizontalAlign="Center" SortExpression="state" HeaderStyle-BorderColor="#336699"
                                                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblState" runat="server" Text='<%# Bind("state") %>' Width="55px" class="wrapword"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtState" Width="100%" runat="server" Text='<%# Bind("state") %>' required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Zip" HeaderStyle-HorizontalAlign="Center" SortExpression="zip" HeaderStyle-BorderColor="#336699"
                                                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblZip" runat="server" Text='<%# Bind("zip") %>' class="wrapword"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtZip" Width="100%" runat="server" Text='<%# Bind("zip") %>' pattern="[0-9]{5}" required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="District" HeaderStyle-HorizontalAlign="Center" SortExpression="district_number" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid"
                                                                        HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDistrictNumber" runat="server" Text='<%# Bind("district_number") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtDistrictNumber" Width="50%" type="number" runat="server" Text='<%# Bind("district_number") %>' required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%-- <asp:TemplateField HeaderText="District Name" HeaderStyle-HorizontalAlign="Center" SortExpression="district_name" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid"
                                                                    HeaderStyle-BorderWidth="1px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDistrictName" runat="server" Text='<%# Bind("district_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtDistrictName" Width="100%" runat="server" Text='<%# Bind("district_name") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="Region" HeaderStyle-HorizontalAlign="Center" SortExpression="region_number" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid"
                                                                        HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRegionNumber" runat="server" Text='<%# Bind("region_number") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtRegionNumber" Width="50%" type="number" runat="server" Text='<%# Bind("region_number") %>' required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--  <asp:TemplateField HeaderText="Region Name" HeaderStyle-HorizontalAlign="Center" SortExpression="region_name" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid"
                                                                    HeaderStyle-BorderWidth="1px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRegionName" runat="server" Text='<%# Bind("region_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtRegionName" Width="100%" runat="server" Text='<%# Bind("region_name") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="Area" HeaderStyle-HorizontalAlign="Center" SortExpression="area_number" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid"
                                                                        HeaderStyle-BorderWidth="1px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAreaNumber" runat="server" Text='<%# Bind("area_number") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="txtAreaNumber" Width="50%" type="number" runat="server" Text='<%# Bind("area_number") %>' required="required"></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%-- <asp:TemplateField HeaderText="Area Name" HeaderStyle-HorizontalAlign="Center" SortExpression="area_name" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid"
                                                                    HeaderStyle-BorderWidth="1px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAreaName" runat="server" Text='<%# Bind("area_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtAreaName" Width="100%" runat="server" Text='<%# Bind("area_name") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid"
                                                                        HeaderStyle-BorderWidth="1px">
                                                                        <EditItemTemplate>
                                                                            <asp:ImageButton ID="imgBtnDone" ValidationGroup="Update" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');"
                                                                                CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update"
                                                                                runat="server" AlternateText="Save" /><br />
                                                                            <%--<br />--%>
                                                                            <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');"
                                                                                CausesValidation="false" formnovalidate onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel"
                                                                                runat="server" AlternateText="Cancel" />
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');"
                                                                                CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit"
                                                                                runat="server" AlternateText="Edit" />
                                                                            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/images/btn_remove.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');"
                                                                                CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');"
                                                                                CommandName="Delete" OnClientClick="return confirm('Are you sure you want to remove this store?');"
                                                                                AlternateText="Delete" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    <center>
                                                                        <b>
                                                                        <asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                                                    </center>
                                                                </EmptyDataTemplate>
                                                                <SelectedRowStyle BackColor="LightBlue" />
                                                                <PagerStyle ForeColor="Blue"
                                                                    BackColor="LightBlue" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="idStoreUpdates" style="width: 900px;" runat="server" align="center">
                                                                <asp:Label ID="lblUpdateCounts" ForeColor="Blue" Text="" runat="server" Width="150px" CssClass="formFields"></asp:Label>
                                                                <table runat="server" width="100%" border="0" cellspacing="2" cellpadding="0">
                                                                    <tr align="right">
                                                                        <td>
                                                                            <table runat="server" border="0" cellpadding="0" cellspacing="0" width="50%">
                                                                                <tr>
                                                                                    <td width="120" align="right" nowrap="nowrap" style="vertical-align: middle;">
                                                                                        <asp:ImageButton ID="btnGeneratexlsReport" ImageUrl="~/images/excel.png"
                                                                                            runat="server" AlternateText="Generate Report" OnCommand="btnGenerateReport_Command" CommandArgument="excel" Visible="false" />
                                                                                        <asp:ImageButton ID="btnGeneratepdfReport" ImageUrl="~/images/pdf.png"
                                                                                            runat="server" AlternateText="Generate Report" OnCommand="btnGenerateReport_Command" CommandArgument="pdf" Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table runat="server" name="gridMainStoreData" id="gridMainStoreData" width="100%"></table>
                                                                <div id="pagerSD"></div>
                                                                <img src="../images/spacer.gif" width="1" height="5" alt="spacer image" /><br />
                                                            </div>
                                                            <div id="divConfirmDialog" style="display: none; font-family: Arial; font-size: 13px; text-align: center; font-weight: bold; min-height: 50px;"></div>
                                                            <rsweb:ReportViewer ID="StoreUpdateReport" runat="server" Font-Names="Verdana"
                                                                Font-Size="8pt" Width="850px" Height="450px" AsyncRendering="False" BorderStyle="None">
                                                            </rsweb:ReportViewer>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <img src="../images/spacer.gif" width="1" height="10" alt="spacer" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
