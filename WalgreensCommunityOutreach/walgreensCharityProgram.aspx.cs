﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using System.IO;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Text;
using TdWalgreens;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public partial class walgreensCharityProgram : Page
{
    #region --------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk == 0)
                Response.Redirect("walgreensNoAccess.htm");

            this.hfcontactLogPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk.ToString();
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = 0;
            string[] storeDetails = this.commonAppSession.SelectedStoreSession.storeName.Split(',');

            //Checking if the store is MO or DC
            if (storeDetails[storeDetails.Length - 1].Trim().Substring(0, 2) == "MO" || storeDetails[storeDetails.Length - 1].Trim().Substring(0, 2) == "DC")
                this.isMOState = true;
            else
                this.isMOState = false;

            this.addClinicLocation(true);
            this.bindClinicLocations();
        }
        else
        {
            string event_target = Request["__EVENTTARGET"];
            //string event_args = (Request["__EVENTARGUMENT"]).ToString();
            Int32.TryParse(this.hfcontactLogPk.Value, out this.contactLogPk);
            if (event_target.ToLower() == "continuesaving")
            {
                if (this.contactLogPk > 0)
                {
                    this.prepareClinicLocationsXML(-1);
                    this.doProcess();
                }
                else
                    Response.Redirect("walgreensHome.aspx");
            }
            if (event_target.ToLower() == "continuesending")
            {
                if (this.contactLogPk > 0)
                {
                    this.prepareClinicLocationsXML(-1);
                    this.doProcess(true);
                }
                else
                    Response.Redirect("walgreensHome.aspx");
            }
            this.setMinMaxDates();
        }

        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void btnScheduleClinic_Click(object sender, ImageClickEventArgs e)
    {
        Validate("WalgreensUser");
        if (Page.IsValid)
        {
            Int32.TryParse(this.hfcontactLogPk.Value, out this.contactLogPk);
            if (this.contactLogPk > 0)
            {
                this.prepareClinicLocationsXML(-1);
                int alert_type = this.validateClinicDates();

                if (alert_type == 1)
                {
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminderWarning('" + (string)GetGlobalResourceObject("errorMessages", "clinicDateReminderBefore2WeeksCharity") + "');", true);
                    return;
                }
                if (alert_type == 2)
                {
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", "clinicDateReminderAfter2WeeksCharity") + "');", true);
                    return;
                }
                else
                    this.doProcess();
            }
            else
                Response.Redirect("walgreensHome.aspx");
        }
        else
        {
            this.prepareClinicLocationsXML(-1);
            this.bindClinicLocations();
        }
    }

    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("walgreensHome.aspx");
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        this.addClinicLocation(false);
        this.bindClinicLocations();
    }

    protected void imgBtnRemoveLocation_Click(object sender, EventArgs e)
    {
        ImageButton img_btn_remove = (ImageButton)sender;
        GridViewRow grd_row = (GridViewRow)img_btn_remove.NamingContainer;

        this.prepareClinicLocationsXML(grd_row.RowIndex);
        this.bindClinicLocations();
    }

    protected void grdLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddl_states = (DropDownList)e.Row.FindControl("ddlState");
            string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate");
            string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate");
            if (Convert.ToDateTime(restriction_start_date) < DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
            {
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicState"].ToString(), ApplicationSettings.getStoreStateRestrictions("storeState"));
            }
            else
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicState"].ToString(), "");
            ddl_states.SelectedValue = grdLocations.DataKeys[e.Row.RowIndex].Values["clinicState"].ToString();

            if (e.Row.RowIndex == 0)
            {
                ImageButton btn_remove_location = (ImageButton)e.Row.FindControl("imgBtnRemoveLocation");
                btn_remove_location.Visible = false;
            }


            //disabling address fields if state is MO or DC and for Previous clinics 
            if (isAddressDisabled)
            {
                ((TextBox)e.Row.FindControl("txtAddress1")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtAddress2")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtCity")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtZipCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlState")).Enabled = false;
            }
            //var date_control = e.Row.FindControl("PickerAndCalendarFrom");
            this.setClinicDates(e.Row, true);
        }
    }

    protected void ValidateLocations(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = true;
        string value = string.Empty;
        string validation_group = ((BaseValidator)sender).ValidationGroup;

        //Validate clinic locations
        foreach (GridViewRow row in grdLocations.Rows)
        {
            bool is_required = !((CheckBox)row.FindControl("chkNoClinic")).Checked;
            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name is required", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text) && !(((TextBox)row.FindControl("txtLocalContactName")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name: < > characters are not allowed", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", string.Empty, false);

            //address1
            if (validation_group == "WalgreensUser" && (this.isMOState || is_required) && string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1 is required", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && !(((TextBox)row.FindControl("txtAddress1")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1: < > characters are not allowed", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && !(((TextBox)row.FindControl("txtAddress1")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", string.Empty, false);

            //address2
            if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && !(((TextBox)row.FindControl("txtAddress2")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", "Address2: < > characters are not allowed", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && !(((TextBox)row.FindControl("txtAddress2")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                e.IsValid = false;
            }
            else
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", string.Empty, false);
            }

            //city
            if (validation_group == "WalgreensUser" && (this.isMOState || is_required) && string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City is required", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text) && !(((TextBox)row.FindControl("txtCity")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City: < > characters are not allowed", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtCity"), "textbox", string.Empty, false);

            //Phone
            value = ((TextBox)row.FindControl("txtLocalContactPhone")).Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();

            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactPhone")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Phone number is required", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(value) && !value.validatePhone())
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Valid Phone number is required(ex: ###-###-####)", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", string.Empty, false);

            //Zip Code
            if (validation_group == "WalgreensUser" && (this.isMOState || is_required) && string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Zip Code is required", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text) && !(((TextBox)row.FindControl("txtZipCode")).Text.validateZipCode()))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Invalid Zip Code", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", string.Empty, false);

            //Email
            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Email is required", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text) && !(((TextBox)row.FindControl("txtLocalContactEmail")).Text.validateEmail()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Invalid Email", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", string.Empty, false);

            //State
            if (validation_group == "WalgreensUser" && (this.isMOState || is_required) && ((DropDownList)row.FindControl("ddlState")).SelectedItem.Value.Trim() == "")
            {
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", "Select State", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", string.Empty, false);

            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", "Start Time is required", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", "Start Time is required", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtEstShots")).Text))
            {
                this.setControlProperty(row.FindControl("txtEstShots"), "textbox", "Estimated Shots is required (ex: #####)", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtEstShots")).Text) && !(((TextBox)row.FindControl("txtEstShots")).Text.validateEstimatedShots()))
            {
                this.setControlProperty(row.FindControl("txtEstShots"), "textbox", "Invalid Estimated Shots value", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtEstShots"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtVouchersDistributed")).Text))
            {
                this.setControlProperty(row.FindControl("txtVouchersDistributed"), "textbox", "Please enter the number of Vouchers Distributed.", true);
                e.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtVouchersDistributed")).Text) && !(((TextBox)row.FindControl("txtVouchersDistributed")).Text.validateEstimatedShots()))
            {
                this.setControlProperty(row.FindControl("txtVouchersDistributed"), "textbox", "Invalid Number of Vouchers", true);
                e.IsValid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtVouchersDistributed"), "textbox", string.Empty, false);

            //clinic date
            value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");
            ComponentArt.Web.UI.Calendar calendarctrl = (ComponentArt.Web.UI.Calendar)((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).FindControl("picker1");
            if (value == "01/01/0001" && validation_group == "WalgreensUser" && is_required)
            {
                calendarctrl.BorderColor = System.Drawing.Color.Red;
                calendarctrl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                calendarctrl.BorderWidth = Unit.Parse("1px");
                calendarctrl.ToolTip = "Clinic Date is required";
                e.IsValid = false;
            }
            else
            {
                calendarctrl.Style.Add("border", "1px solid gray");
                calendarctrl.ToolTip = string.Empty;
            }
        }

        if (!e.IsValid)
            return;
    }

    /// <summary>
    /// time picker at server side
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void displayTime_Picker(object sender, EventArgs e)
    {
        StringBuilder script_text = new StringBuilder();
        GridViewRow gvr = (GridViewRow)((sender as TextBox).NamingContainer);
        TextBox txt_end_time = new TextBox();
        txt_end_time = (TextBox)gvr.FindControl("txtEndTime");

        script_text = ApplicationSettings.displayTimePicker((sender as TextBox).ClientID, txt_end_time.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DateScript" + gvr.RowIndex, script_text.ToString(), true);
    }

    /// <summary>
    /// To handle the ddlstate selection changed event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        // to clear the date in picker if we select MO or DC state by non admin user
        //calling bind clinic to set min dates dates for date pickers
        DropDownList ddl_state_clinic_location = (DropDownList)sender;
        if (ddl_state_clinic_location != null)
        {
            GridViewRow row = (GridViewRow)ddl_state_clinic_location.Parent.Parent;
            if (row != null)
            {
                var date_control = row.FindControl("PickerAndCalendarFrom");
                DateTime seleted_date = ((PickerAndCalendar)date_control).getSelectedDate;

                if ((ddl_state_clinic_location.SelectedValue == "MO" || ddl_state_clinic_location.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin && seleted_date < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Parse("01/01/0001");
                }
            }
        }

        this.prepareClinicLocationsXML(-1);
        this.bindClinicLocations();
    }
    #endregion

    #region --------------- PRIVATE METHODS ---------------
    /// <summary>
    /// Adds new clinic location
    /// </summary>
    /// <param name="is_default"></param>
    private void addClinicLocation(bool is_default)
    {
        Int32.TryParse(this.hfcontactLogPk.Value, out this.contactLogPk);
        double clinics_count;
        clinics_count = (is_default) ? 0 : this.grdLocations.Rows.Count;

        XmlNode clinic_locations = null;

        if (is_default)
        {
            clinic_locations = this.xmlCharityProgram.CreateElement("ClinicLocations");
            this.xmlCharityProgram.AppendChild(clinic_locations);
        }
        else
        {
            this.prepareClinicLocationsXML(-1);
            clinic_locations = this.xmlCharityProgram.SelectSingleNode("/ClinicLocations");
        }

        XmlElement clinic_location = this.xmlCharityProgram.CreateElement("ClinicLocation");
        clinic_location.SetAttribute("clinicLocation", (is_default) ? "CLINIC A" : "CLINIC " + (clinics_count >= 26 ? ((Char)(65 + (clinics_count % 26 == 0 ? Math.Ceiling(clinics_count / 26) - 1 : Math.Ceiling(clinics_count / 26) - 2))).ToString() + "" + ((Char)(65 + (clinics_count % 26))).ToString() : ((Char)(65 + clinics_count % 26)).ToString()));
        clinic_location.SetAttribute("clinicContactName", string.Empty);
        clinic_location.SetAttribute("contactFirstName", string.Empty);
        clinic_location.SetAttribute("contactLastName", string.Empty);
        clinic_location.SetAttribute("clinicContactPhone", string.Empty);
        clinic_location.SetAttribute("clinicContactEmail", string.Empty);

        this.isRestrictedStoreState = ApplicationSettings.isRestrictedStoreState(this.commonAppSession.SelectedStoreSession.storeState, this.commonAppSession.LoginUserInfoSession.UserRole);
        if (is_default || isRestrictedStoreState)
        {
            DataTable dt_business_contact = dbOperation.getBusinessContactDetails(this.contactLogPk);
            clinic_location.SetAttribute("clinicAddress1", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address"].ToString() : string.Empty);
            clinic_location.SetAttribute("clinicAddress2", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address2"].ToString() : string.Empty);
            clinic_location.SetAttribute("clinicCity", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["city"].ToString() : string.Empty);
            clinic_location.SetAttribute("clinicState", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["state"].ToString() : string.Empty);
            clinic_location.SetAttribute("clinicZipCode", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["zip"].ToString() : string.Empty);
            this.hfBusinessName.Value = (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["businessName"].ToString() : string.Empty;
            isAddressDisabled = this.isRestrictedStoreState;
        }
        else
        {
            clinic_location.SetAttribute("clinicAddress1", string.Empty);
            clinic_location.SetAttribute("clinicAddress2", string.Empty);
            clinic_location.SetAttribute("clinicCity", string.Empty);
            clinic_location.SetAttribute("clinicState", string.Empty);
            clinic_location.SetAttribute("clinicZipCode", string.Empty);
        }
        clinic_location.SetAttribute("clinicDate", string.Empty);
        clinic_location.SetAttribute("clinicStartTime", string.Empty);
        clinic_location.SetAttribute("clinicEndTime", string.Empty);
        clinic_location.SetAttribute("estShots", string.Empty);
        clinic_location.SetAttribute("vouchersDistributed", string.Empty);
        clinic_location.SetAttribute("isNoClinic", "0");

        clinic_locations.AppendChild(clinic_location);
        this.xmlCharityProgram.AppendChild(clinic_locations);
    }

    /// <summary>
    /// Binds clinic locations to the grid
    /// </summary>
    private void bindClinicLocations()
    {
        StringReader location_reader = new StringReader(this.xmlCharityProgram.InnerXml);
        DataSet location_dataset = new DataSet();
        location_dataset.ReadXml(location_reader);

        this.lblClinicDateAlert.Text = string.Format((string)GetGlobalResourceObject("errorMessages", "clinicDateAlertBefore2WeeksEN"));
        if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
            this.lblClinicDateAlert.Visible = true;

        grdLocations.DataSource = location_dataset.Tables[0];
        grdLocations.DataBind();
    }

    /// <summary>
    /// Prepares charity clinic locations XML
    /// </summary>
    /// <param name="remove_location_id"></param>
    private void prepareClinicLocationsXML(int remove_location_id)
    {
        this.xmlCharityProgram = new XmlDocument();
        XmlElement clinic_locations = this.xmlCharityProgram.CreateElement("ClinicLocations");
        string contact_name = string.Empty;
        double clinic_location_count = 0;

        foreach (GridViewRow row in grdLocations.Rows)
        {
            if (row.RowIndex != remove_location_id)
            {
                XmlElement location_node = this.xmlCharityProgram.CreateElement("ClinicLocation");
                location_node.SetAttribute("clinicLocation", "CLINIC " + (clinic_location_count >= 26 ? ((Char)(65 + (clinic_location_count % 26 == 0 ? Math.Ceiling(clinic_location_count / 26) - 1 : Math.Ceiling(clinic_location_count / 26) - 2))).ToString() + "" + ((Char)(65 + (clinic_location_count % 26))).ToString() : ((char)(65 + clinic_location_count % 26)).ToString()));
                contact_name = ((TextBox)row.FindControl("txtLocalContactName")).Text;
                location_node.SetAttribute("clinicContactName", contact_name.Trim());
                location_node.SetAttribute("contactFirstName", (contact_name.IndexOf(' ') > 0) ? contact_name.Substring(0, contact_name.IndexOf(' ')).Trim() : contact_name.Trim());
                location_node.SetAttribute("contactLastName", (contact_name.IndexOf(' ') > 0) ? contact_name.Substring(contact_name.IndexOf(' ')).Trim() : string.Empty);
                location_node.SetAttribute("clinicContactPhone", ((TextBox)row.FindControl("txtLocalContactPhone")).Text);
                location_node.SetAttribute("clinicContactEmail", ((TextBox)row.FindControl("txtLocalContactEmail")).Text);
                location_node.SetAttribute("clinicAddress1", ((TextBox)row.FindControl("txtAddress1")).Text);
                location_node.SetAttribute("clinicAddress2", ((TextBox)row.FindControl("txtAddress2")).Text);
                location_node.SetAttribute("clinicCity", ((TextBox)row.FindControl("txtCity")).Text);
                location_node.SetAttribute("clinicState", ((DropDownList)row.FindControl("ddlState")).SelectedValue);
                location_node.SetAttribute("clinicZipCode", ((TextBox)row.FindControl("txtZipCode")).Text);
                if (((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                    location_node.SetAttribute("clinicDate", ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString());
                else
                    location_node.SetAttribute("clinicDate", "");
                location_node.SetAttribute("clinicStartTime", ((TextBox)row.FindControl("txtStartTime")).Text);
                location_node.SetAttribute("clinicEndTime", ((TextBox)row.FindControl("txtEndTime")).Text);
                location_node.SetAttribute("estShots", ((TextBox)row.FindControl("txtEstShots")).Text);
                location_node.SetAttribute("vouchersDistributed", ((TextBox)row.FindControl("txtVouchersDistributed")).Text);
                location_node.SetAttribute("isNoClinic", ((CheckBox)row.FindControl("chkNoClinic")).Checked ? "1" : "0");
                location_node.SetAttribute("clinicGroupId", (string)GetGlobalResourceObject("errorMessages", "charityClinicGroupId"));
                location_node.SetAttribute("clinicPlanId", (string)GetGlobalResourceObject("errorMessages", "charityClinicPlanId"));
                location_node.SetAttribute("clinicRecipientId", (string)GetGlobalResourceObject("errorMessages", "charityClinicRecipientId"));

                clinic_locations.AppendChild(location_node);
                clinic_location_count++;
            }
        }

        this.xmlCharityProgram.AppendChild(clinic_locations);
    }

    /// <summary>
    /// Set control properties
    /// </summary>
    /// <param name="control"></param>
    /// <param name="control_type"></param>
    /// <param name="tool_tip"></param>
    /// <param name="is_invalid"></param>
    private void setControlProperty(Control control, string control_type, string tool_tip, bool is_invalid)
    {
        switch (control_type.ToLower())
        {
            case "textbox":
                if (is_invalid)
                {
                    ((TextBox)control).Attributes["title"] = tool_tip;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true); $('#" + control.ClientID + "').attr({ 'title': '" + tool_tip + "'});", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((TextBox)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }
                break;
            case "dropdownlist":
                if (is_invalid)
                {
                    ((DropDownList)control).ToolTip = tool_tip;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);$('#" + control.ClientID + "').attr({ 'title': '" + tool_tip + "'});", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((DropDownList)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }
                break;
        }
    }

    /// <summary>
    /// Validates clinic duration between clinic date and scheduled date
    /// </summary>
    /// <returns></returns>
    private int validateClinicDates()
    {
        int alert_type = 0;
        DateTime clinic_date;

        XmlNodeList clinic_locations = this.xmlCharityProgram.SelectNodes("/ClinicLocations/ClinicLocation[@isNoClinic = 0]");

        if (clinic_locations.Count > 0)
        {
            foreach (XmlElement clinic_location in clinic_locations)
            {
                clinic_date = Convert.ToDateTime(clinic_location.Attributes["clinicDate"].Value);

                if (clinic_date.Date < DateTime.Now.Date.AddDays(14))
                {
                    alert_type = 1;
                    break;
                }
            }

            if (alert_type == 0)
                alert_type = 2;
        }

        return alert_type;
    }

    /// <summary>
    /// Save charity clinic locations
    /// </summary>
    private void doProcess(bool is_continue = false)
    {
        //Validate clinic date and time overlap locally
        XmlNodeList clinic_location_nodes = this.xmlCharityProgram.SelectNodes("/ClinicLocations/ClinicLocation[@clinicDate!='' and @clinicStartTime!='' and  @clinicEndTime!='' ]");
        string failed_location = "";
        ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "charityagreement", this.hfBusinessName.Value, string.Empty);
        if (!String.IsNullOrEmpty(failed_location))
        {
            this.bindClinicLocations();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", failed_location, true);
            return;
        }
        CheckBox chk_no_clinic;
        string max_qty_error = string.Empty;
        bool has_max_qty = false;
        foreach (GridViewRow row in grdLocations.Rows)
        {
            int shots_count = 0;
            chk_no_clinic = (CheckBox)row.FindControl("chkNoClinic");

            if (!chk_no_clinic.Checked) //If not voucher only clinic
            {
                string lbl_clinic_location = ((Label)row.FindControl("lblClinicLocation")).Text;
                string est_shots = ((TextBox)row.FindControl("txtEstShots")).Text;

                if (!string.IsNullOrEmpty(est_shots))
                {
                    shots_count = Convert.ToInt32(est_shots);
                    if (shots_count > 250 && !is_continue)
                    {
                        has_max_qty = true;
                        max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), shots_count, "Standard – Trivalent", lbl_clinic_location.Replace("CLINIC ", ""));

                    }
                }
            }
        }
        if (has_max_qty)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showWarning('WARNING','" + max_qty_error.Substring(6) + "','continuesending');", true);
            return;
        }
        string date_time_stamp_message = "";
        int return_value = this.dbOperation.insertUpdateHHSProgramClinics(this.contactLogPk, this.commonAppSession.LoginUserInfoSession.UserID, this.xmlCharityProgram.InnerXml, out date_time_stamp_message);

        if (return_value >= 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreementHHS") + "'); window.location.href = 'walgreensHome.aspx';", true);
        else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
        {
            this.bindClinicLocations();
            string validation_message = string.Empty;
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "charityagreement", this.hfBusinessName.Value, date_time_stamp_message);

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
            return;
        }
    }

    /// <summary>
    /// Sets min/max dates for clinics
    /// </summary>
    /// <param name="row"></param>
    /// <param name="is_from_grid_load"></param>
    private void setClinicDates(GridViewRow row, bool is_from_grid_load)
    {
        // string selected_store_name = this.commonAppSession.SelectedStoreSession.storeName.Substring(this.commonAppSession.SelectedStoreSession.storeName.LastIndexOf(", ") + 1, 3).Trim();
        var date_control = row.FindControl("PickerAndCalendarFrom");
        DropDownList ddl_states = (DropDownList)row.FindControl("ddlState");
        if (grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
        {
            string clinic_date = grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString();
            if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin)
            {
                if (Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()) >= DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                else if (Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                    ((PickerAndCalendar)date_control).SetMinDate = Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString());
            }
            else if (Convert.ToDateTime(clinic_date) > DateTime.Today.Date)
            {
                if (DateTime.Now.AddDays(14) < Convert.ToDateTime(clinic_date)
                    && !ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(13);
                else if (ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                else
                    ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
            }
            else
                ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);

            if (is_from_grid_load)
            {
                ((PickerAndCalendar)date_control).getSelectedDate = Convert.ToDateTime(clinic_date);
                ((TextBox)row.FindControl("txtCalenderFrom")).Text = Convert.ToDateTime(clinic_date).ToString("MM/dd/yyyy");
            }
        }
        else
        {
            if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && !commonAppSession.LoginUserInfoSession.IsAdmin)
                ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
            else
            {
                if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Now.AddDays(14);
                else
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
            }
        }//For "MO" stores block out all clinic dates up to September 1, 2016 in the local contract and for charity events
    }

    /// <summary>
    /// Set minimum and maximum dates for date controls.
    /// </summary>
    private void setMinMaxDates()
    {
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            var clinic_date = row.FindControl("PickerAndCalendarFrom");
            if (clinic_date != null)
            {
                this.setClinicDates(row, false);
            }
        }
    }

    #endregion

    #region --------------- PRIVATE VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private int contactLogPk;
    private bool isMOState
    {
        get
        {
            return Convert.ToBoolean(hfisMOState.Value);
        }
        set
        {
            hfisMOState.Value = value.ToString();
        }
    }
    private bool isAddressDisabled
    {
        get
        {
            bool value = false;
            if (ViewState["isAddressDisabled"] != null)
                value = (bool)ViewState["isAddressDisabled"];
            return value;
        }
        set
        {
            ViewState["isAddressDisabled"] = value;
        }
    }
    private bool isRestrictedStoreState
    {
        get
        {
            bool value = false;
            if (ViewState["isRestrictedStoreState"] != null)
                value = (bool)ViewState["isRestrictedStoreState"];
            return value;
        }
        set
        {
            ViewState["isRestrictedStoreState"] = value;
        }
    }
    private XmlDocument xmlCharityProgram = null;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.xmlCharityProgram = new XmlDocument();

        if (Session.IsNewSession)
        {
            FormsAuthentication.SignOut();
            string str = Request.Url.ToString();
            Response.Redirect(str);
        }
    }
}