﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="scheduleEventForm.aspx.cs" Inherits="scheduleEventForm"%>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register src="controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>

    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css"  />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css"  />
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />

    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <script type="text/javascript"  src="javaScript/jquery-ui.js"></script>
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtOthers").html($("#txtOther"));
            $("#txtRoleOthers").html($("#txtRoleOther"));

            $("#rbtnEventType").click(function () {
                if ($('input[type=radio]:checked', '#rbtnEventType').val() == 8)
                    $("#txtOther").focus();
            });
            $("#rbtnWalgreenRole").click(function () {
                if ($('input[type=radio]:checked', '#rbtnWalgreenRole').val() == 3)
                    $("#txtRoleOther").focus();
            });
            $("#txtOther").focusout(function () {
                $("#txtOther").val($("#txtOther").val().replace("<", "&lt;").replace(">", "&gt;"));
            });

            $("#txtRoleOther").focusout(function () {
                $("#txtRoleOther").val($("#txtRoleOther").val().replace("<", "&lt;").replace(">", "&gt;"));
            });

            $("#txtNoOfAttendees").focusout(function () {
                $("#txtNoOfAttendees").val($("#txtNoOfAttendees").val().replace("<", "&lt;").replace(">", "&gt;"));
            });

            $("#txtDescriptionOfGiveaway").focusout(function () {
                $("#txtDescriptionOfGiveaway").val($("#txtDescriptionOfGiveaway").val().replace("<", "&lt;").replace(">", "&gt;"));
            });

            $("#txtTotalCost").focusout(function () {
                $("#txtTotalCost").val($("#txtTotalCost").val().replace("<", "&lt;").replace(">", "&gt;"));
            });

            var dateToday = new Date();

            $("#txtEventDate").datepicker({
                showOn: "button",
                buttonImage: "images/btn_calendar.gif",
                buttonImageOnly: true,
                //minDate: dateToday,
                onSelect: function (dateText) {
                    $("#hfEventDate").val(dateText);
                }
            });

            $("#txtEventDate").val($("#hfEventDate").val());

            if ($.browser.msie) {
                $('#txtEventStartTime').keydown(function (event) { event.preventDefault(); });
                $('#txtEventEndTime').keydown(function (event) { event.preventDefault(); });
            }
            else {
                $('#txtEventStartTime').keypress(function (event) { event.preventDefault(); });
                $('#txtEventEndTime').keypress(function (event) { event.preventDefault(); });
            }

            $('#txtEventStartTime').timepicker({ step: 30, minTime: '00:00am', maxTime: '11:30pm' });
            $('#txtEventEndTime').timepicker({ step: 30, minTime: '00:00am', maxTime: '11:30pm' });

            $("#txtOther").change(function () {
                if ($("#txtOther").val().trim() != "") {
                    var radio = $("[id*=rbtnEventType] label:contains('Other')").closest("td").find("input");
                    radio.attr("checked", "checked");
                }
                
            });
        });

        function addTime(oldTime) {
            var time = oldTime.split(":");
            var hours = time[0];
            var ampm = time[1].substring(2, 4);
            var minutes = time[1].substring(0, 2);
            var time_new = '';
            if (+minutes >= 30) {
                hours = (+hours + 1) % 24;
            }
            minutes = (+minutes + 30) % 60;
            if (hours >= 12) {
                time_new = hours - 12 + ':' + minutes + 'pm';
            }
            else
                time_new = hours + ':' + minutes + ampm;
            return time_new;
        }
    </script>
    <style type="text/css">
    .ui-widget
    {
        font-size:11px;
    }
    .ui-datepicker-trigger
    {
        vertical-align:bottom;
        padding-left:5px;
    }
    .ui-timepicker-list li
    {
        color: #333333;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 12px;
    }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" >
<asp:HiddenField ID="hfEventDate" runat="server" />
<asp:HiddenField ID="hfEventStartTime" runat="server" />
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
<tr>
    <td colspan="2">
        <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
     <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td valign="top" align="center" >
                       <div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" ShowSummary="false" />

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
              <td class="backgroundGradientLog" align="left"><table width="100%" border="0" cellspacing="18" cellpadding="0">
                <tr>
                  <td colspan="2" class="logTitle">Scheduled Event Information</td>
                </tr>
                <tr>
                  <td width="151" nowrap="nowrap"><span class="profileTitles">Date of Event:</span></td>
                  <td width="539" class="logSubTitles">
                    <asp:TextBox ID="txtEventDate" runat="server" TabIndex="1" ReadOnly="true" Width="80px" ></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="txtEventDate" ID="txtEventDateReqFV" runat="server" Display="None" ErrorMessage="Date of Event is required"></asp:RequiredFieldValidator>                  
                  </td>
                </tr>
                <tr>
                  <td class="profileTitles">Event Time Start</td>
                  <td class="logSubTitles">
                    <asp:TextBox ID="txtEventStartTime" runat="server" TabIndex="2" MaxLength="7" Width="80px" CssClass="formFields"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="txtEventStartTime" ID="txtEventStartTimeReqFV" runat="server" Display="None" ErrorMessage="Event Start Time is required"></asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td class="profileTitles">Event Time End</td>
                  <td class="logSubTitles">
                    <asp:TextBox ID="txtEventEndTime" runat="server" TabIndex="3" MaxLength="7" Width="80px" CssClass="formFields"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="txtEventEndTime" ID="txtEventEndTimeReqFV" runat="server" Display="None" ErrorMessage="Event End Time is required"></asp:RequiredFieldValidator>
                  </td>
                </tr>
                <tr>
                  <td align="left" valign="top" class="profileTitles">Event Type:</td>
                  <td align="left" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td colspan="2">
                        <asp:RadioButtonList ID="rbtnEventType" runat="server" RepeatLayout="Table" RepeatColumns="4" CssClass="formFields"  GroupName="eventType" CellSpacing="4" TabIndex="4" ></asp:RadioButtonList>
                       <asp:TextBox MaxLength="500" ID="txtOther" runat="server" CssClass="formFields"> </asp:TextBox>
                       <asp:RequiredFieldValidator ID="rbtnEventTypeFV" runat="server" ControlToValidate="rbtnEventType" Display="None" ErrorMessage="Event Type is required"></asp:RequiredFieldValidator>
                     </td>
                    </tr>
                    <tr>
                     <td></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top" nowrap="nowrap"><span class="profileTitles">Walgreens Role:<label for="textarea"></label></span></td>
                  <td>
                  <asp:RadioButtonList ID="rbtnWalgreenRole" runat="server" RepeatLayout="Table" RepeatColumns="3" CssClass="formFields" GroupName="walgreenRole" CellSpacing="5" TabIndex="5">
                  </asp:RadioButtonList>
                  <asp:TextBox ID="txtRoleOther" MaxLength="500" runat="server" CssClass="formFields"></asp:TextBox>
                  <%--<asp:RequiredFieldValidator ID="rbtnWalgreenRoleFV" runat="server" ControlToValidate="rbtnWalgreenRole" Display="None" ErrorMessage="Walgreens Role is required"></asp:RequiredFieldValidator>--%>
                 </td>
                </tr>
                <tr>
                  <td valign="top" nowrap="nowrap"><span class="profileTitles">Number of Attendees:<label for="textarea"></label></span></td>
                  <td><span class="logSubTitles">
                    <asp:TextBox ID="txtNoOfAttendees" runat="server" CssClass="formFields" MaxLength="5" TabIndex="6" > </asp:TextBox>                     
                     <asp:RegularExpressionValidator ControlToValidate="txtNoOfAttendees" ID="txtNoOfAttendeesEV" runat="server"  Display="None" ErrorMessage="Number of Attendees should be numeric only." ValidationExpression="0*[0-9][0-9]*"></asp:RegularExpressionValidator>
                  </span></td>
                </tr>
                <tr>
                  <td valign="top" nowrap="nowrap"><span class="profileTitles"><asp:Label ID="lblRefreshments" runat="server" Text="Refreshments/Giveaways:"></asp:Label><label for="textarea"></label></span></td>
                  <td>
                      <span class="formFields">
                        <asp:RadioButton ID="rbtnRefreshments1" CssClass="formFields" Checked="true" runat="server" Text="Yes" GroupName="refreshments" TabIndex="7" />
                        <asp:RadioButton ID="rbtnRefreshments2" CssClass="formFields" runat="server" Text="No" GroupName="refreshments" TabIndex="8" />
                  </span></td>
                </tr>
                <tr id="trFunding" runat="server">
                  <td valign="top" nowrap="nowrap" class="profileTitles">Funding of Giveaways:</td>
                  <td>
                    <asp:DropDownList ID="ddlFundingOfGiveaways" runat="server" CssClass="formFields">
                    </asp:DropDownList>
                    <%--<asp:RequiredFieldValidator ID="ddlFundingOfGiveawaysFV" runat="server" ControlToValidate="ddlFundingOfGiveaways" Display="None" ErrorMessage="Funding of Giveaways is required"></asp:RequiredFieldValidator>--%>
                   </td>
                </tr>
                <tr>
                  <td valign="top" nowrap="nowrap"><span class="profileTitles"><asp:Label ID="lblDescription" runat="server" Text="Description of Giveaway:"></asp:Label>
                  </span></td>
                  <td><span class="formFields">
                     <asp:TextBox ID="txtDescriptionOfGiveaway" MaxLength="500" runat="server" TextMode="MultiLine" Height="52px" Width="365px" CssClass="formFields" TabIndex="9"></asp:TextBox>
                     <asp:RegularExpressionValidator ControlToValidate="txtDescriptionOfGiveaway" ID="txtDescriptionOfGiveawayRE" runat="server"  Display="None" ErrorMessage="Description of Giveaway should be 500 characters" ValidationExpression="[\s\S]{1,500}"></asp:RegularExpressionValidator>
                  </span></td>
                </tr>
                <tr id="trTotalCost" runat="server">
                  <td valign="top" nowrap="nowrap"><span class="profileTitles">Total Cost:
                      <label for="textarea"></label>
                  </span></td>
                  <td><span class="logSubTitles">
                    $
                    <asp:TextBox ID="txtTotalCost" runat="server" MaxLength="8" TabIndex="10"> </asp:TextBox>                    
                     <asp:RegularExpressionValidator ControlToValidate="txtTotalCost" ID="txtTotalCostEV" runat="server"  Display="None" ErrorMessage="Cost should be numeric only." ValidationExpression="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$"></asp:RegularExpressionValidator>
                  </span></td>
                </tr>
                <tr>
                  <td colspan="2" align="right" valign="bottom" style="padding-top:0px">
                  <asp:ImageButton ID="btnSubmit" runat="server" AlternateText="Submit" CausesValidation="true" ImageUrl="images/btn_submit_event.png"  onclick="btnSubmit_Click" />
                  &nbsp;
                  <asp:ImageButton ID="btnCancel" runat="server"  AlternateText="Cancel" 
                          CausesValidation="false" ImageUrl="images/btn_cancel_event.png" Visible="false" 
                          onclick="btnCancel_Click" />
                  </td>
                </tr>
                </table></td>
  </tr>
</table>
    </div>
                      </td>
                    </tr>                   
                </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
</form>
</body>
</html>
