﻿using System;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using TdWalgreens;
using System.Net;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Generic;
using TdApplicationLib;
public partial class thankYou : Page
{
    #region ------------------ PROTECTED EVENTS -----------
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!string.IsNullOrEmpty(Request.QueryString["key"])) && Request.QueryString.Count > 0)
        {
            this.lblOutreachTitle.Visible = false;
            if (!IsPostBack)
            {
                if (PreviousPage != null)
                {
                    previousPageValues = ((HiddenField)PreviousPage.FindControl("hfThankYouPageReqValues")).Value;
                }
            }
            if (previousPageValues != "")//if approved
            {
                tableThankYouAgreementApproved.Visible = true;
                tableThankYouScheduled.Visible = false;
                this.setLinkButtons();
                this.setLables();
            }
            else // if rejected
            {
                imgThankYou.ImageUrl = "images/thank_you_contract.jpg";

                Dictionary<int, string> wag_user = (Dictionary<int, string>)Application["WagUsers"];
                bool user_exists = false;

                if (wag_user.Count > 0)
                {
                    AppCommonSession common_app_session = new AppCommonSession();
                    foreach (KeyValuePair<int, string> user in wag_user)
                    {
                        if (user.Key == common_app_session.LoginUserInfoSession.UserID)
                            user_exists = true;
                    }

                    if (user_exists)
                        wag_user.Remove(common_app_session.LoginUserInfoSession.UserID);

                    Application["WagUsers"] = wag_user;
                }
            }
        }
        else
        {
            //this.lblOutreachTitle.Visible = false;
            this.imgThankYou.ImageUrl = "images/thank_you.jpg";
        }
        Session.RemoveAll();
        FormsAuthentication.SignOut();
    }
    #endregion
    #region ------------------ PRIVATE METHODS -----------
    /// <summary>
    /// Shows VAR PDF links 
    /// </summary>
    private void setLinkButtons()
    {
        if (previousPageValues.Split('|')[0].ToLower() != "none")
        {
            if (previousPageValues.Split('|')[0].ToLower() == "prefilled")
            {
                this.lnkbtnVARFormEn.Attributes.Add("href", "/controls/ResourceFileHandler.ashx?Path=" + previousPageValues.Split('|')[2].ToString() + "&FileType=Prefilled");
                if (previousPageValues.Split('|')[3].Length > 0)
                    this.lnkbtnVARFormSp.Attributes.Add("href", "/controls/ResourceFileHandler.ashx?Path=" + previousPageValues.Split('|')[3].ToString() + "&FileType=Prefilled");
                else
                    this.lnkbtnVARFormSp.Visible = false;
            }
            else
            {
                this.lnkbtnVARFormEn.Attributes.Add("href", "/controls/ResourceFileHandler.ashx?Path=" + previousPageValues.Split('|')[2].ToString());
                if (previousPageValues.Split('|')[3].Length > 0)
                    this.lnkbtnVARFormSp.Attributes.Add("href", "/controls/ResourceFileHandler.ashx?Path=" + previousPageValues.Split('|')[3].ToString());
                else
                    this.lnkbtnVARFormSp.Visible = false;
            }
        }
        else
        {
            this.lnkbtnVARFormEn.Visible = false;
            this.lnkbtnVARFormSp.Visible = false;
        }
    }

    /// <summary>
    /// Displays Lables
    /// </summary>
    /// <param name="corporate_to_invoice"></param>
    /// <param name="insurance"></param>
    /// <param name="state"></param>
    private void setLables()
    {
        //Do not show VAR links when copay group id is pending
        if (previousPageValues.Split('|')[0].ToLower() != "none")
        {
            this.lblVARText.Text = (string)GetGlobalResourceObject("errorMessages", "ThankYouScreenVAR");
            //this.lnkbtnVARFormEn.Visible = (previousPageValues.Split('|')[0] != "PR");
        }
        this.lblCorporateToInvoice.Text = Array.IndexOf(previousPageValues.Split('|'), "CorpToInvoice") >= 0 ? (string)GetGlobalResourceObject("errorMessages", "ThankYouScreenCorporateToInvoice") : "";
        this.lblInsurance.Text = Array.IndexOf(previousPageValues.Split('|'), "Insurance") >= 0 ? (string)GetGlobalResourceObject("errorMessages", "ThankYouScreenInsurancePayment") : "";
    }
    #endregion
    #region ------------------ PRIVATE VARIABLES -----------
    /// <summary>
    /// contains previous page hidden field value 
    /// </summary>
    private string previousPageValues = "";
    #endregion
}
