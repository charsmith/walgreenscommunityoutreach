﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using TdApplicationLib;
using tdEmailLib;
using TdWalgreens;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using NLog;
using System.Web.Services;

public partial class walgreensVoteVaxClinicDetails : System.Web.UI.Page
{
    #region ------------ PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserName))
        {
            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "admin":
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                    //Enable store assignment access to Admin,DM,HCS,DPR only
                    this.isStoreEditable = true;
                    break;
                default: this.isStoreEditable = false;
                    break;
            }
        }
        if (!Page.IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk > 0)
            {
                this.hfBusinessClinicPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk.ToString();
                this.hfBusinessStoreId.Value = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.txtDefaultClinicStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.isDisableEditClinicDetails = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails");
                if (this.isDisableEditClinicDetails)
                    this.imgbtnRemoveClinicAgreement.Visible = false;
                this.bindLocalClinicDetails();
                this.controlAccess();
                if (this.isMOPreviousSeasonBusiness)
                    this.bindLocalClinicPreviousLocationToDropdown();                
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
            }
            else
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                Response.Redirect("walgreensHome.aspx");
            }
        }
        else
        {
            string event_args = Request["__EVENTTARGET"];
            if (event_args.ToLower() == "maintaincontactstatus")
            {
                bool maintain_log = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                if (!maintain_log)
                {
                    int business_clinic_pk;
                    Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);

                    if (business_clinic_pk > 0)
                        this.dbOperation.removeClinicContactLogs(business_clinic_pk, "Local");
                }

                Session.Remove(this.hfBusinessClinicPk.Value);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
            else if (event_args.ToLower() == "deleteclinicagreement")
            {
                int business_clinic_pk;
                int return_value = 0;
                Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);

                if (business_clinic_pk > 0)
                {
                    return_value = this.dbOperation.deleteAgreement(business_clinic_pk, this.commonAppSession.LoginUserInfoSession.UserID, "Vote & Vax");

                    if (return_value == 0)
                    {
                        this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;

                        //Send delete contract agreement notification email to admin users
                        string to_email = string.Empty;
                        to_email += ApplicationSettings.getEmailInfoFromGroup("deleteContractAgreementNotificationGroup", true).Trim();

                        this.walgreensEmail.sendCommunityOffsiteAgreementDeletedNotificationEmail("Admin", this.commonAppSession.LoginUserInfoSession.UserName, "Clinic " + ((Label)this.grdLocations.Rows[0].FindControl("lblClinicLocation")).Text, Server.MapPath("~/emailTemplates/communityOffsiteAgreementDeletionTemplate.htm"), String.Format((string)GetGlobalResourceObject("errorMessages", "deleteContractAgreementNotifyEmailSubject"), this.lblClientName.Text), this.hfBusinessStoreId.Value, this.lblClientName.Text, this.hfContractedStoreId.Value, (string)GetGlobalResourceObject("errorMessages", "nlaThankYouLine"), (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : to_email), "", true);

                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "deletedClinicAgreement") + "'); window.location.href = 'walgreensHome.aspx';", true);
                    }
                }
            }
            else if (event_args.ToLower() == "clinicdetailsupdated")
            {
                bool save_changes = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                if (save_changes)
                {
                    this.updatedAction = "Submit";
                    if (this.ValidateLocations("UpdateClinic"))
                    {
                        if (!this.validateImmunizationBlackedOutDates())
                        {
                            this.showAlertMessage = true;
                            return;
                        }
                        else
                        {
                            this.doProcess(true, true);

                            if (!showAlertMessage)
                            {
                                Session.Remove(this.hfBusinessClinicPk.Value);
                                Response.Redirect("walgreensHome.aspx");
                            }
                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
                        return;
                    }
                }
                if (!showAlertMessage)
                {
                    Session.Remove(this.hfBusinessClinicPk.Value);
                    Response.Redirect("walgreensHome.aspx");
                }

            }
            else if (event_args.ToLower().Contains("continuesaving"))
            {
                bool save_clinic = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                if (save_clinic)
                {
                    this.updatedAction = event_args.ToLower().Split(',')[1];
                    this.doProcess(false, false);
                }
                else
                {
                    this.prepareClinicLocationDetailsXml(-1);
                    this.bindClinicLocations();
                }
            }
        }

        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false; 
        ddlClinicLocations = (this.grdLocations.Rows.Count > 0) ? (DropDownList)this.grdLocations.Rows[0].FindControl("ddlClinicLocations") : null;
        imgBtnAddLocation = (this.grdLocations.Rows.Count > 0) ? (ImageButton)this.grdLocations.Rows[0].FindControl("imgBtnAddLocation") : null;
    }

    protected void grdLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Web.UI.HtmlControls.HtmlControl row_store_assign = (System.Web.UI.HtmlControls.HtmlControl)e.Row.FindControl("rowStoreAssignment");
            System.Web.UI.HtmlControls.HtmlControl row_reAssign = (System.Web.UI.HtmlControls.HtmlControl)e.Row.FindControl("rowReAssignment");
            System.Web.UI.HtmlControls.HtmlTableRow row_add_clinic = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowAddClinicLocation");
            GridView grd_clinic_immunizations = (GridView)e.Row.FindControl("grdClinicImmunizations");
            row_store_assign.Visible = false;

            DropDownList ddl_states = (DropDownList)e.Row.FindControl("ddlState");
            string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate");
            string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate");
            if (Convert.ToDateTime(restriction_start_date) < DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
            {
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString(), ApplicationSettings.getStoreStateRestrictions("storeState"));
            }
            else
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString(), "");

            if (ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString()) != null)
                ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString()).Selected = true;

            string clinic_date = "";
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
                clinic_date = grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString();

            if (e.Row.RowIndex == 0)
            {
                ImageButton btn_remove_clinic = (ImageButton)e.Row.FindControl("imgBtnRemoveLocation");
                System.Web.UI.HtmlControls.HtmlTableRow row_clinic_immunizations = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowClinicImmunizations");

                row_reAssign.Visible = false;
                btn_remove_clinic.Visible = false;
                row_clinic_immunizations.Visible = false;
                grd_clinic_immunizations.Visible = false;

                if (this.isDisableEditClinicDetails)
                {
                    row_add_clinic.Visible = false;
                    ddl_states.Enabled = false;
                }

                //Show clinic date reminder above Billing & Vaccine Information for default clinic location
                DateTime clinic_scheduled_on = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicScheduledOn"].ToString());
                if (Convert.ToDateTime(clinic_date) <= clinic_scheduled_on.AddDays(14))
                {
                    this.lblClinicDateAlert.Text = (string)GetGlobalResourceObject("errorMessages", "clinicApprovedBefore2Weeks");
                    this.lblClinicDateAlert.Visible = true;
                }
                else
                    this.lblClinicDateAlert.Visible = false;
            }
            else
            {
                row_add_clinic.Visible = false;

                System.Web.UI.HtmlControls.HtmlTable tbl_confirmed_client_name = (System.Web.UI.HtmlControls.HtmlTable)e.Row.FindControl("tblConfirmedClientName");
                tbl_confirmed_client_name.Visible = false;

                //bind immunizations to new clinic locations
                this.bindClinicImmunizations(grd_clinic_immunizations, e.Row.RowIndex);

                //Displays store assignment to specific users only
                if (this.isStoreEditable)
                    row_store_assign.Visible = true;

                if (clinic_date.Length > 0 && Convert.ToDateTime(clinic_date) <= DateTime.Now.AddDays(14))
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row_clinic_date_alert = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowClinicDateAlert");
                    Label lbl_clinic_date_alert = (Label)e.Row.FindControl("lblClinicDateAlert");
                    row_clinic_date_alert.Visible = true;
                    lbl_clinic_date_alert.Text = (string)GetGlobalResourceObject("errorMessages", "clinicCreatedBefore2Weeks");
                }
            }

            //Enable Default store assignment to specific users only
            if (isStoreEditable)
                this.txtDefaultClinicStoreId.Enabled = true;

            CheckBox chk_reassign_clinics = (CheckBox)e.Row.FindControl("chkReassignClinic");
            Label lbl_reassign_clinics = (Label)e.Row.FindControl("lblIsReassignClinic");
            int is_reassign = 0;
            Int32.TryParse(lbl_reassign_clinics.Text, out is_reassign);
            chk_reassign_clinics.Checked = (is_reassign == 0) ? false : true;
            chk_reassign_clinics.Visible = chkReassignClinicPrimary.Visible;

            var date_control = e.Row.FindControl("PickerAndCalendarFrom");
            //string selected_store_name = this.commonAppSession.SelectedStoreSession.storeName.Substring(this.commonAppSession.SelectedStoreSession.storeName.LastIndexOf(", ") + 1, 3).Trim();

            if (grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
            {
                if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    ((PickerAndCalendar)date_control).MinDate = ApplicationSettings.getOutreachStartDate;
                else
                {
                    if (ddl_states.SelectedValue == "MO")
                    {
                        if (Convert.ToDateTime(clinic_date) >= DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                            ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                        else if (Convert.ToDateTime(clinic_date) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                            ((PickerAndCalendar)date_control).SetMinDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString());
                    }
                    else if (Convert.ToDateTime(clinic_date) > DateTime.Today.Date)
                        ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                    else
                        ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
                }

                ((PickerAndCalendar)date_control).getSelectedDate = Convert.ToDateTime(clinic_date);
                ((TextBox)e.Row.FindControl("txtCalenderFrom")).Text = Convert.ToDateTime(clinic_date).ToString("MM/dd/yyyy");
            }
            else
            {
                if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    ((PickerAndCalendar)date_control).MinDate = ApplicationSettings.getOutreachStartDate;
                else if (ddl_states.SelectedValue == "MO" && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                else
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
            }

            //disabling address fields if state is MO and for Previous clinics 
            if (this.isAddressDisabled)
            {
                ((TextBox)e.Row.FindControl("txtAddress1")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtAddress2")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtCity")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtZipCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlState")).Enabled = false;
            }
        }
    }

    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            lbl_price.Text = "N/A";

        }
    }

    protected void grdClinicUpdatesHistory_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_update_action = (Label)e.Row.FindControl("lblAction");
            if (lbl_update_action.Text.ToLower() == "updated")
            {
                System.Web.UI.HtmlControls.HtmlTableCell update_field = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateField");
                update_field.ColSpan = 2;
                System.Web.UI.HtmlControls.HtmlTableCell update_value = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateValue");
                update_value.Visible = false;
            }
        }
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        this.prepareClinicLocationDetailsXml(-1);

        XmlNode clinic_locations = this.xmlClinicDetails.SelectSingleNode("//clinicLocations");
        XmlElement clinic_location = this.xmlClinicDetails.CreateElement("clinicLocation");

        int max_clinic_number;
        double locations_count = 0;

        Int32.TryParse(this.hdMaxClinicNumber.Value, out max_clinic_number);
        locations_count = max_clinic_number + (this.grdLocations.Rows.Count - 1);

        clinic_location.SetAttribute("naClinicLocation", ((locations_count >= 26) ? ((Char)(65 + (locations_count % 26 == 0 ? Math.Ceiling(locations_count / 26) - 1 : Math.Ceiling(locations_count / 26) - 2))).ToString() + "" + ((Char)(65 + locations_count % 26)).ToString() : ((Char)(65 + locations_count % 26)).ToString()));
        clinic_location.SetAttribute("naContactFirstName", string.Empty);
        clinic_location.SetAttribute("naContactLastName", string.Empty);
        clinic_location.SetAttribute("naClinicContactPhone", string.Empty);
        clinic_location.SetAttribute("naContactEmail", string.Empty);
        if (this.isRestrictedStoreState && ddlClinicLocations.Visible && ddlClinicLocations.SelectedItem.Value != "0")
        {
            DataRow added_row = this.previousClinicLocation.Select("rowId='" + ddlClinicLocations.SelectedItem.Value + "'").FirstOrDefault();
            clinic_location.SetAttribute("naClinicAddress1", added_row["naClinicAddress1"].ToString());
            clinic_location.SetAttribute("naClinicAddress2", added_row["naClinicAddress2"].ToString());
            clinic_location.SetAttribute("naClinicCity", added_row["naClinicCity"].ToString());
            clinic_location.SetAttribute("naClinicState", added_row["naClinicState"].ToString());
            clinic_location.SetAttribute("naClinicZip", added_row["naClinicZip"].ToString());
        }
        else if (this.isRestrictedStoreState)
        {
            DataTable dt_business_contact = dbOperation.getBusinessContactDetails(Convert.ToInt32(this.hfContactLogPk.Value));
            clinic_location.SetAttribute("naClinicAddress1", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicAddress2", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address2"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicCity", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["city"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicState", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["state"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicZip", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["zip"].ToString() : string.Empty);
        }
        else
        {
            clinic_location.SetAttribute("naClinicAddress1", string.Empty);
            clinic_location.SetAttribute("naClinicAddress2", string.Empty);
            clinic_location.SetAttribute("naClinicCity", string.Empty);
            clinic_location.SetAttribute("naClinicState", string.Empty);
            clinic_location.SetAttribute("naClinicZip", string.Empty);
        }
        clinic_location.SetAttribute("clinicDate", string.Empty);
        clinic_location.SetAttribute("naClinicStartTime", string.Empty);
        clinic_location.SetAttribute("naClinicEndTime", string.Empty);
        clinic_location.SetAttribute("clinicScheduledOn", string.Empty);

        //Add re-assign attribute
        clinic_location.SetAttribute("isReassign", "0");
        clinic_location.SetAttribute("clinicStoreId", this.hfBusinessStoreId.Value);
        //Add clinic latitude and longitude attributes
        clinic_location.SetAttribute("clinicLatitude", string.Empty);
        clinic_location.SetAttribute("clinicLongitude", string.Empty);
        clinic_location.SetAttribute("confirmedClientName", string.Empty);

        //Add selected immunizations to the new clinic location
        XmlNodeList default_clinic_immunizations = clinic_locations.FirstChild.FirstChild.ChildNodes;
        XmlElement clinic_immunizations = this.xmlClinicDetails.CreateElement("Immunizations");
        foreach (XmlElement immunization in default_clinic_immunizations)
        {
            XmlElement estshots_node = this.xmlClinicDetails.CreateElement("Immunization");
            estshots_node.SetAttribute("pk", immunization.Attributes["pk"].Value);
            estshots_node.SetAttribute("immunizationName", immunization.Attributes["immunizationName"].Value);
            estshots_node.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
            estshots_node.SetAttribute("paymentTypeName", immunization.Attributes["paymentTypeName"].Value);
            estshots_node.SetAttribute("estimatedQuantity", string.Empty);
            estshots_node.SetAttribute("totalImmAdministered", string.Empty);
            clinic_immunizations.AppendChild(estshots_node);
        }

        clinic_location.AppendChild(clinic_immunizations);
        clinic_locations.AppendChild(clinic_location);
        this.bindClinicLocations();
        if (this.isRestrictedStoreState)
            this.bindLocalClinicPreviousLocationToDropdown();
    }

    protected void imgBtnRemoveLocation_Click(object sender, EventArgs e)
    {
        ImageButton img_btn_remove = (ImageButton)sender;
        GridViewRow grd_row = (GridViewRow)img_btn_remove.NamingContainer;

        this.prepareClinicLocationDetailsXml(grd_row.RowIndex);
        this.bindClinicLocations();
        if (this.isMOPreviousSeasonBusiness)
            this.bindLocalClinicPreviousLocationToDropdown();

    }

    /// <summary>
    /// Time picker at server side
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void displayTime_Picker(object sender, EventArgs e)
    {
        StringBuilder script_text = new StringBuilder();
        GridViewRow gvr = (GridViewRow)((sender as TextBox).NamingContainer);
        TextBox txt_end_time = new TextBox();
        txt_end_time = (TextBox)gvr.FindControl("txtEndTime");

        script_text = ApplicationSettings.displayTimePicker((sender as TextBox).ClientID, txt_end_time.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DateScript" + gvr.RowIndex, script_text.ToString(), true);
    }

    protected void lnkContractPageNav_Click(object sender, EventArgs e)
    {
        int business_clinic_pk, store_id;

        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        Int32.TryParse(this.hfBusinessClinicPk.Value.Trim(), out business_clinic_pk);
        Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);

        this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;
        this.commonAppSession.SelectedStoreSession.storeID = store_id;
        this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = this.contactLogPk;
        this.commonAppSession.SelectedStoreSession.referrerPath = "walgreensVoteVaxClinicDetails.aspx";
        Session.Remove(this.hfBusinessClinicPk.Value);
        Response.Redirect("walgreensVoteVaxAgreement.aspx");
    }

    protected void doProcess_Click(object sender, CommandEventArgs e)
    {
        this.updatedAction = e.CommandArgument.ToString();
        bool is_valid = true;
        if (this.updatedAction != "Cancel")
        {
            if (this.updatedAction == "Confirmed")
                is_valid = this.ValidateLocations("ConfirmClinic");
            else if (this.updatedAction == "Completed")
                is_valid = this.ValidateLocations("CompleteClinic");
            else if (this.updatedAction == "Cancelled")
                is_valid = this.ValidateLocations("CancelledClinic");
            else
                is_valid = this.ValidateLocations("UpdateClinic");

            if (is_valid)
            {
                //checking immunizations blackout dates 
                if (!this.validateImmunizationBlackedOutDates())
                    return;

                doProcess(false, true);
            }
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
        }
        else
        {
            if (!ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails"))
                this.doProcess(false, true);
            else
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                Response.Redirect("~/walgreensHome.aspx");
            }
        }
    }

    protected void ValidateCompanyPhoneFax(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validatePhone();
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        // to clear the date in picker if we select MO state by non admin user
        //calling bind clinic to set min dates dates for date pickers
        DropDownList ddl_state_clinic_location = (DropDownList)sender;
        if (ddl_state_clinic_location != null)
        {
            GridViewRow row = (GridViewRow)ddl_state_clinic_location.Parent.Parent.Parent;
            if (row != null)
            {
                var date_control = row.FindControl("PickerAndCalendarFrom");
                DateTime seleted_date = ((PickerAndCalendar)date_control).getSelectedDate;

                if (ddl_state_clinic_location.SelectedValue == "MO" && !commonAppSession.LoginUserInfoSession.IsAdmin && seleted_date < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Parse("01/01/0001");
                }
            }
            this.prepareClinicLocationDetailsXml(-1);
            this.bindClinicLocations();
        }
    }
    #endregion

    #region ------------ PRIVATE METHODS -------------
    /// <summary>
    /// Validates clinic data and checks mandatory field
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateLocations(string validation_group)
    {
        bool is_page_valid = true;
        string value = string.Empty;

        //Validate client business informatio
        is_page_valid = this.txtDefaultClinicStoreId.validateControls("textbox", "number", true, string.Empty, "Please enter valid Store Id", this.Page);

        //Validate clinic billing & vaccine information
        is_page_valid = this.txtFirstContactName.validateControls("textbox", "string", false, "Contact First Name is required", "Contact First Name: , < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtLocalContactPhonePrimary.validateControls("textbox", "phone", false, "Contact Phone number is required", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        is_page_valid = this.txtLastContactName.validateControls("textbox", "string", false, "Contact Last Name is required", "Contact Last Name: , < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtLocalContactEmailPrimary.validateControls("textbox", "email", true, string.Empty, "Please enter a valid Email address", this.Page) && is_page_valid;
        is_page_valid = this.txtContactJobTitle.validateControls("textbox", "string", true, string.Empty, "Contact Job Title: , < > characters are not allowed", this.Page) && is_page_valid;

        //Validate clinic billing & vaccine information
        if (this.txtPlanId.Visible)
            is_page_valid = this.txtPlanId.validateControls("textbox", "string", false, "Plan ID is required", "Plan ID: < > characters are not allowed", this.Page) && is_page_valid;
        if (this.txtGroupId.Visible)
            is_page_valid = this.txtGroupId.validateControls("textbox", "string", false, "Group ID is required", "Group ID: < > characters are not allowed", this.Page) && is_page_valid;
        if (this.txtIdRecipient.Visible)
            is_page_valid = this.txtIdRecipient.validateControls("textbox", "string", false, "ID Recipient is required", "ID Recipient: < > characters are not allowed", this.Page) && is_page_valid;

        foreach (GridViewRow row in grdImmunizationChecks.Rows)
        {
            is_page_valid = ((TextBox)row.FindControl("txtEstimatedQuantity")).validateControls("textbox", "zero", false, "Please enter the number of Estimated Vol", "Please enter valid number of Estimated Vol (ex: #####)", this.Page) && is_page_valid;

            if (validation_group == "CompleteClinic")
                is_page_valid = ((TextBox)row.FindControl("txtTotalImmAdministered")).validateControls("textbox", "zero", false, "Please enter number of Total Immunizations Administered", "Please enter valid number of Total Immunizations Administered (ex: #####)", this.Page) && is_page_valid;
            else
                is_page_valid = ((TextBox)row.FindControl("txtTotalImmAdministered")).validateControls("textbox", "zero", true, string.Empty, "Please enter valid number of Total Immunizations Administered (ex: #####)", this.Page) && is_page_valid;
        }

        //Validate clinic location information
        foreach (GridViewRow row in grdLocations.Rows)
        {
            is_page_valid = ((TextBox)row.FindControl("txtContactFirstName")).validateControls("textbox", "string", false, "Contact First Name is required", "Contact First Name: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtContactLastName")).validateControls("textbox", "string", false, "Contact Last Name is required", "Contact Last Name: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtAddress1")).Enabled ? ((TextBox)row.FindControl("txtAddress1")).validateControls("textbox", "address", false, "Address1 is required", "Clinic Address1: < > characters are not allowed", this.Page) && is_page_valid : is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtAddress2")).Enabled ? ((TextBox)row.FindControl("txtAddress2")).validateControls("textbox", "address", true, string.Empty, "Clinic Address2: < > characters are not allowed", this.Page) && is_page_valid : is_page_valid;

            value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");
            HtmlControl cont = (HtmlControl)((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).Parent;
            if (value == "01/01/0001")
            {
                cont.Style.Add("border", "1px solid red");
                cont.Attributes.Add("title", "Clinic Date is required");
                is_page_valid = false;
            }
            else
            {
                cont.Style.Add("border", "0px gray");
                cont.Attributes.Add("title", "");
                is_page_valid = true && is_page_valid;
            }

            is_page_valid = ((TextBox)row.FindControl("txtCity")).validateControls("textbox", "string", false, "City is required", "Clinic City: < > characters are not allowed", this.Page) && is_page_valid;

            if ((validation_group == "ConfirmClinic" || validation_group == "CompleteClinic") && (row.RowIndex == 0))
                is_page_valid = ((TextBox)row.FindControl("txtConfirmedClientName")).validateControls("textbox", "string", false, "This clinic cannot be logged as Confirmed/Completed until you have entered the 'Name of Client Individual Who Confirmed the Clinic'", "Name of Client Individual Who Confirmed the Clinic: < > characters are not allowed", this.Page) && is_page_valid;
            else
                is_page_valid = ((TextBox)row.FindControl("txtConfirmedClientName")).validateControls("textbox", "string", true, string.Empty, "Name of Client Individual Who Confirmed the Clinic: < > characters are not allowed", this.Page) && is_page_valid;

            is_page_valid = ((TextBox)row.FindControl("txtLocalContactPhone")).validateControls("textbox", "phone", false, "Contact Phone is required", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtZipCode")).validateControls("textbox", "zip", false, "Zip Code is required", "Please enter a valid Zip Code (ex: #####)", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtLocalContactEmail")).validateControls("textbox", "email", false, "Contact Email is required", "Please enter a valid Email address", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtStartTime")).validateControls("textbox", "string", false, "Start Time is required", "Clinic Start Time: < > Characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtEndTime")).validateControls("textbox", "string", false, "End Time is required", "Clinic End Time: < > Characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((DropDownList)row.FindControl("ddlState")).validateControls("dropdownlist", "select", false, "State is required", string.Empty, this.Page) && is_page_valid;

            if (row.RowIndex != 0)
            {
                GridView grdClinicImmunization = (GridView)row.FindControl("grdClinicImmunizations");
                if (grdClinicImmunization != null)
                {
                    foreach (GridViewRow immRow in grdClinicImmunization.Rows)
                    {
                        is_page_valid = ((TextBox)immRow.FindControl("txtEstimatedQuantity")).validateControls("textbox", "zero", false, "Please enter the number of Estimated Vol", "Please enter valid number of Estimated Vol (ex: #####)", this.Page) && is_page_valid;
                    }
                }
            }
        }

        //Validate pharmacist and post clinic information
        if (validation_group == "CompleteClinic")
        {
            is_page_valid = this.txtRxHost.validateControls("textbox", "string", false, "This clinic cannot be logged as Completed until you have entered 'Name of Rx Host'", "Name Of Rx Host: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = this.txtTotalHoursClinicHeld.validateControls("textbox", "decimal", false, "This clinic cannot be logged as Completed until you have entered 'Total Hours Clinic Held'", "Please enter the valid Total Hours Clinic Held", this.Page) && is_page_valid;
            is_page_valid = this.txtRxHostPhone.validateControls("textbox", "phone", false, "This clinic cannot be logged as Completed until you have entered 'Phone #'", "Valid Phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        }
        else
        {
            is_page_valid = this.txtRxHost.validateControls("textbox", "string", true, string.Empty, "Name Of Rx Host: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = this.txtTotalHoursClinicHeld.validateControls("textbox", "decimal", true, string.Empty, "Please enter the valid Total Hours Clinic Held", this.Page) && is_page_valid;
            is_page_valid = this.txtRxHostPhone.validateControls("textbox", "phone", true, string.Empty, "Valid Phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        }

        if (validation_group == "CancelledClinic")
            is_page_valid = this.txtFeedBack.validateControls("textbox", "string", false, "This clinic cannot be logged as Cancelled until you have entered 'Feedback Notes'", "Feedback: < > characters are not allowed", this.Page) && is_page_valid;
        else
            is_page_valid = this.txtFeedBack.validateControls("textbox", "string", true, string.Empty, "Feedback: < > characters are not allowed", this.Page) && is_page_valid;

        return is_page_valid;
    }

    private void setImmunizationsBlackoutValidationRequiredValue()
    {
        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            if (blackedOutImmunizations.Contains(((Label)row.FindControl("lblImmunizationCheck")).Text.ToLower().Trim()))
            {
                isImmunizationsBlackoutValidationRequired = true;
            }
        }
    }
    private bool validateImmunizationBlackedOutDates()
    {
        bool is_valid = true;
        string value = string.Empty;
        string failed_clinic_locations = string.Empty;
        this.setImmunizationsBlackoutValidationRequiredValue();
        if (isImmunizationsBlackoutValidationRequired)
        {
            foreach (GridViewRow row in grdLocations.Rows)
            {
                bool is_estimated_qnt_exists = false;
                value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");

                if (row.RowIndex != 0)
                {
                    GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
                    foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
                    {
                        if (blackedOutImmunizations.Contains(((Label)imm_row.FindControl("lblImmunizationCheck")).Text.ToLower().Trim()) && !string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtEstimatedQuantity")).Text.Trim()))
                        {
                            is_estimated_qnt_exists = Convert.ToInt32(((TextBox)imm_row.FindControl("txtEstimatedQuantity")).Text) == 0 ? false : true;
                        }
                    }
                }
                else
                {
                    foreach (GridViewRow imm_row in grdImmunizationChecks.Rows)
                    {
                        if (blackedOutImmunizations.Contains(((Label)imm_row.FindControl("lblImmunizationCheck")).Text.ToLower().Trim()) && !string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtEstimatedQuantity")).Text.Trim()))
                        {
                            is_estimated_qnt_exists = Convert.ToInt32(((TextBox)imm_row.FindControl("txtEstimatedQuantity")).Text) == 0 ? false : true;
                        }
                    }
                }
                if (is_estimated_qnt_exists && (Convert.ToDateTime(value) >= Convert.ToDateTime("04/15/2016") && Convert.ToDateTime(value) <= Convert.ToDateTime("07/15/2016")))
                {
                    failed_clinic_locations += "\\n" + "Clinic Location" + ((Label)row.FindControl("lblClinicLocation")).Text;
                    is_valid = false;
                }
            }
            if (!is_valid)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "blackoutImmunizationValidationMessageForDetails") + failed_clinic_locations + "');", true);
            }
        }
        return is_valid;
    }
    /// <summary>
    /// Binds clinic details and locations information
    /// </summary>
    private void bindLocalClinicDetails()
    {
        int business_clinic_pk;
        Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);
        if (!string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserRole))
        {
            DataSet ds_clinic_details = dbOperation.getClinicLocationDetails(business_clinic_pk, 2015, 1, this.commonAppSession.LoginUserInfoSession.UserRole);
            if (ds_clinic_details != null && ds_clinic_details.Tables.Count > 0)
            {
                if (ds_clinic_details.Tables[0].Rows.Count > 0)
                {

                    this.hfContactLogPk.Value = ds_clinic_details.Tables[0].Rows[0]["contactLogPk"].ToString();
                    this.hfIsPreviousSeasonLog.Value = ds_clinic_details.Tables[0].Rows[0]["isPreviousSeasonLog"].ToString();
                    this.hdMaxClinicNumber.Value = ds_clinic_details.Tables[0].Rows[0]["maxClinicLocationId"].ToString();
                    this.hfContractedStoreId.Value = ds_clinic_details.Tables[0].Rows[0]["contractedStoreId"].ToString();

                    //Business Information
                    this.lblClientName.Text = ds_clinic_details.Tables[0].Rows[0]["businessName"].ToString();
                    this.txtFirstContactName.Text = ds_clinic_details.Tables[0].Rows[0]["firstName"].ToString();
                    this.txtLastContactName.Text = ds_clinic_details.Tables[0].Rows[0]["lastName"].ToString();
                    this.txtContactJobTitle.Text = ds_clinic_details.Tables[0].Rows[0]["jobTitle"].ToString();
                    this.txtLocalContactEmailPrimary.Text = ds_clinic_details.Tables[0].Rows[0]["businessContactEmail"].ToString();
                    this.txtLocalContactPhonePrimary.Text = ds_clinic_details.Tables[0].Rows[0]["phone"].ToString();

                    //Billing & Vaccine Information
                    this.txtPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                    this.lblPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                    this.txtGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();
                    this.lblGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();

                    this.txtIdRecipient.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();
                    this.lblIdRecipient.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();

                    this.txtComments.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicAddlComments"].ToString();
                    this.dtClinicImmunizations = ds_clinic_details.Tables[1];

                    this.grdImmunizationChecks.DataSource = this.dtClinicImmunizations;
                    this.grdImmunizationChecks.DataBind();

                    if (ApplicationSettings.isRestrictedStoreState(ds_clinic_details.Tables[0].Rows[0]["storeState"].ToString(), this.commonAppSession.LoginUserInfoSession.UserRole))
                    {
                        this.isMOPreviousSeasonBusiness = true;
                        chkReassignClinicPrimary.Visible = false;
                        this.isAddressDisabled = true;
                        this.isRestrictedStoreState = true;
                    }
                    this.grdLocations.DataSource = ds_clinic_details.Tables[0];
                    this.grdLocations.DataBind();

                    //Bind Pharmacist & Post Clinic Information
                    this.txtRxHost.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistName"].ToString();
                    this.txtRxHostPhone.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistPhone"].ToString();
                    this.txtTotalHoursClinicHeld.Text = ds_clinic_details.Tables[0].Rows[0]["totalHours"].ToString();

                    //Show/hide outreach contact status buttons
                    this.btnCancelClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnCancelClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnConfirmedClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnConfirmedClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnClinicCompleted.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnClinicCompletedDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());

                    //disable edit clinic details facility to specific clients
                    if (this.isDisableEditClinicDetails)
                    {
                        this.btnCancelClinic.Visible = false;
                        this.btnConfirmedClinic.Visible = false;
                        this.btnClinicCompleted.Visible = false;
                        this.btnSubmit.Visible = false;
                        this.btnCancelClinicDim.Visible = true;
                        this.btnConfirmedClinicDim.Visible = true;
                        this.btnClinicCompletedDim.Visible = true;
                        this.btnSubmitDim.Visible = true;
                    }

                    //Bind clinic updates history log
                    this.grdClinicUpdatesHistory.DataSource = ApplicationSettings.getClinicUpdateHistory(ds_clinic_details.Tables[2], "Local");
                    this.grdClinicUpdatesHistory.DataBind();

                    //setting hfScheduledDate value to store scheduled on date 
                    DataRow[] row = ds_clinic_details.Tables[2].Select("updateAction = 'Scheduled'");
                    if (row.Count() > 0)
                    {
                        hfScheduledDate.Value = row[0]["updatedOn"].ToString();
                    }
                    //this.Logger.Trace("Client browser(Server-side)::" + browser.Browser + " " + browser.Version + "; Action::Page Load; Username::" + this.commonAppSession.LoginUserInfoSession.UserName + "; StoreId::" + this.hfBusinessStoreId.Value + "; ClinicPk::" + this.hfBusinessClinicPk.Value + "; Clinic Location::" + this.lblClientName.Text + "-Clinic " + ((Label)this.grdLocations.Rows[0].FindControl("lblClinicLocation")).Text);
                }

                this.prepareClinicLocationDetailsXml(-1);
                if (this.isMOPreviousSeasonBusiness)
                    this.bindLocalClinicPreviousLocationToDropdown();

                Session[this.hfBusinessClinicPk.Value] = this.xmlClinicDetails;
            }
        }
        else
            Response.Redirect("auth/sessionTimeout.aspx");
    }

    /// <summary>
    /// Binds clinic locations to Grid
    /// </summary>
    private void bindClinicLocations()
    {
        StringReader location_reader = new StringReader(this.xmlClinicDetails.SelectSingleNode("//clinicLocations").OuterXml.ToString());
        DataSet location_dataset = new DataSet();
        location_dataset.ReadXml(location_reader);

        grdLocations.DataSource = location_dataset.Tables[0];
        grdLocations.DataBind();
        if (this.isMOPreviousSeasonBusiness)
            this.bindLocalClinicPreviousLocationToDropdown();
    }

    /// <summary>
    /// Binds Immunizations to new clinic locations
    /// </summary>
    /// <param name="grd_clinic_immunizations"></param>
    /// <param name="clinic_number"></param>
    private void bindClinicImmunizations(GridView grd_clinic_immunizations, int clinic_number)
    {
        StringReader clinic_immunizations_reader = new StringReader(this.xmlClinicDetails.SelectSingleNode("//clinicLocations").ChildNodes[clinic_number].SelectSingleNode("Immunizations").OuterXml);
        DataSet ds_clinic_immunizations = new DataSet();
        ds_clinic_immunizations.ReadXml(clinic_immunizations_reader);

        if (ds_clinic_immunizations.Tables.Count > 0 && ds_clinic_immunizations.Tables[0].Rows.Count > 0)
        {
            grd_clinic_immunizations.DataSource = ds_clinic_immunizations.Tables[0];
            grd_clinic_immunizations.DataBind();
        }
    }

    /// <summary>
    /// Displays controls based on user login
    /// </summary>
    private void controlAccess()
    {
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.txtPlanId.Visible = true;
            this.lblPlanId.Visible = false;
            this.txtIdRecipient.Visible = true;
            this.lblIdRecipient.Visible = false;
        }
    }

    /// <summary>
    /// Prepares clinic locations XML document
    /// </summary>
    /// <param name="remove_location_id"></param>
    private void prepareClinicLocationDetailsXml(int remove_location_id)
    {
        XmlElement clinic_details_ele = this.xmlClinicDetails.CreateElement("clinicDetails");
        int new_store_id = 0;
        int max_clinic_number = 0;
        Int32.TryParse(this.hdMaxClinicNumber.Value, out max_clinic_number);
        double location_count = max_clinic_number - 1;

        GridView grd_clinic_immunizations = null;

        //Clinic client details
        XmlElement client_info = this.xmlClinicDetails.CreateElement("clientInformation");
        client_info.SetAttribute("clientName", this.lblClientName.Text.Trim());
        client_info.SetAttribute("contactFirstName", this.txtFirstContactName.Text.Trim());
        client_info.SetAttribute("contactLastName", this.txtLastContactName.Text.Trim());
        client_info.SetAttribute("contactPhone", this.txtLocalContactPhonePrimary.Text.Trim());
        client_info.SetAttribute("contactEmail", this.txtLocalContactEmailPrimary.Text.Trim());
        client_info.SetAttribute("jobTitle", this.txtContactJobTitle.Text.Trim());
        client_info.SetAttribute("naClinicPlanId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtPlanId.Text.Trim() : this.lblPlanId.Text.Trim()));
        client_info.SetAttribute("naClinicGroupId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtGroupId.Text.Trim() : this.lblGroupId.Text.Trim()));
        client_info.SetAttribute("recipientId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtIdRecipient.Text.Trim() : this.lblIdRecipient.Text.Trim()));
        client_info.SetAttribute("comments", this.txtComments.Text.Trim());
        client_info.SetAttribute("feedback", this.txtFeedBack.Text);
        clinic_details_ele.AppendChild(client_info);

        //Clinic locations details
        XmlElement clinic_locations = this.xmlClinicDetails.CreateElement("clinicLocations");
        foreach (GridViewRow row in grdLocations.Rows)
        {
            if (row.RowIndex != remove_location_id)
            {
                XmlElement location_node = this.xmlClinicDetails.CreateElement("clinicLocation");
                location_node.SetAttribute("naClinicLocation", (row.RowIndex == 0) ? ((Label)row.FindControl("lblClinicLocation")).Text : ((location_count >= 26) ? ((Char)(65 + (location_count % 26 == 0 ? Math.Ceiling(location_count / 26) - 1 : Math.Ceiling(location_count / 26) - 2))).ToString() + "" + ((Char)(65 + location_count % 26)).ToString() : ((Char)(65 + location_count % 26)).ToString()));

                location_node.SetAttribute("naContactFirstName", ((TextBox)row.FindControl("txtContactFirstName")).Text);
                location_node.SetAttribute("naContactLastName", ((TextBox)row.FindControl("txtContactLastName")).Text);
                location_node.SetAttribute("naClinicContactPhone", ((TextBox)row.FindControl("txtLocalContactPhone")).Text);
                location_node.SetAttribute("naContactEmail", ((TextBox)row.FindControl("txtLocalContactEmail")).Text);
                location_node.SetAttribute("naClinicAddress1", ((TextBox)row.FindControl("txtAddress1")).Text);
                location_node.SetAttribute("naClinicAddress2", ((TextBox)row.FindControl("txtAddress2")).Text);
                location_node.SetAttribute("naClinicCity", ((TextBox)row.FindControl("txtCity")).Text);
                location_node.SetAttribute("naClinicState", ((DropDownList)row.FindControl("ddlState")).SelectedValue);
                location_node.SetAttribute("naClinicZip", ((TextBox)row.FindControl("txtZipCode")).Text);
                if (((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                    location_node.SetAttribute("clinicDate", ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString());
                else
                    location_node.SetAttribute("clinicDate", "");
                location_node.SetAttribute("naClinicStartTime", ((TextBox)row.FindControl("txtStartTime")).Text);
                location_node.SetAttribute("naClinicEndTime", ((TextBox)row.FindControl("txtEndTime")).Text);
                location_node.SetAttribute("clinicScheduledOn", grdLocations.DataKeys[row.RowIndex].Values["clinicScheduledOn"].ToString());

                //Add clinic store Id & re-assign attribute
                if (row.RowIndex == 0)
                {
                    grd_clinic_immunizations = this.grdImmunizationChecks;
                    Int32.TryParse(txtDefaultClinicStoreId.Text, out new_store_id);
                    location_node.SetAttribute("isReassign", (chkReassignClinicPrimary.Checked ? "1" : "0"));
                }
                else
                {
                    grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
                    Int32.TryParse(((TextBox)row.FindControl("txtClinicStore")).Text, out new_store_id);
                    location_node.SetAttribute("isReassign", (((CheckBox)row.FindControl("chkReassignClinic")).Checked ? "1" : "0"));
                }

                if (isStoreEditable)
                    location_node.SetAttribute("clinicStoreId", (new_store_id > 0) ? new_store_id.ToString() : "");
                else
                    location_node.SetAttribute("clinicStoreId", "");

                //Add clinic latitude and longitude attributes
                location_node.SetAttribute("clinicLatitude", ((HiddenField)row.FindControl("hfClinicLatitude")).Value);
                location_node.SetAttribute("clinicLongitude", ((HiddenField)row.FindControl("hfClinicLongitude")).Value);
                location_node.SetAttribute("confirmedClientName", ((TextBox)row.FindControl("txtConfirmedClientName")).Text);

                clinic_locations.AppendChild(location_node);
                location_count++;

                //Add clinic immunizations
                XmlElement clinic_immunizations = this.xmlClinicDetails.CreateElement("Immunizations");
                if (grd_clinic_immunizations != null)
                {
                    foreach (GridViewRow immunization in grd_clinic_immunizations.Rows)
                    {
                        XmlElement immunization_ele = this.xmlClinicDetails.CreateElement("Immunization");
                        immunization_ele.SetAttribute("pk", ((Label)immunization.FindControl("lblImmunizationPk")).Text);
                        immunization_ele.SetAttribute("immunizationName", ((Label)immunization.FindControl("lblImmunizationCheck")).Text);
                        immunization_ele.SetAttribute("paymentTypeId", ((Label)immunization.FindControl("lblPaymentTypeId")).Text);
                        immunization_ele.SetAttribute("paymentTypeName", ((Label)immunization.FindControl("lblPaymentType")).Text);
                        immunization_ele.SetAttribute("estimatedQuantity", ((TextBox)immunization.FindControl("txtEstimatedQuantity")).Text);
                        immunization_ele.SetAttribute("totalImmAdministered", ((TextBox)immunization.FindControl("txtTotalImmAdministered")).Text);

                        clinic_immunizations.AppendChild(immunization_ele);
                    }
                    location_node.AppendChild(clinic_immunizations);
                }
            }
        }
        clinic_details_ele.AppendChild(clinic_locations);

        XmlElement post_clinic_info = this.xmlClinicDetails.CreateElement("postClinicInformation");
        post_clinic_info.SetAttribute("pharmacistName", this.txtRxHost.Text);
        post_clinic_info.SetAttribute("totalHours", this.txtTotalHoursClinicHeld.Text);
        post_clinic_info.SetAttribute("pharmacistPhone", this.txtRxHostPhone.Text);
        clinic_details_ele.AppendChild(post_clinic_info);

        this.xmlClinicDetails.AppendChild(clinic_details_ele);
    }

    /// <summary>
    /// Submits clinic details and clinic outreach contact status
    /// </summary>
    /// <param name="is_return_home"></param>
    /// <param name="is_check_clinic_dates"></param>
    private void doProcess(bool is_return_home, bool is_check_clinic_dates)
    {
        int business_clinic_pk, store_id, new_store_id;
        Int32.TryParse(this.hfBusinessClinicPk.Value.Trim(), out business_clinic_pk);
        Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);
        Int32.TryParse(this.txtDefaultClinicStoreId.Text, out new_store_id);
        int return_value = 0;

        if (new_store_id == 0 || new_store_id == store_id)
            new_store_id = 0;

        if (Session[this.hfBusinessClinicPk.Value] != null && business_clinic_pk > 0 && this.commonAppSession.LoginUserInfoSession != null)
        {
            string error_message = string.Empty;
            string email_body;
            //string billing_email;
            //bool send_emailto_clinical_contract = false;

            this.prepareClinicLocationDetailsXml(-1);
            bool is_equal = ApplicationSettings.compareXMLDocuments(this.xmlClinicDetails, ((XmlDocument)Session[this.hfBusinessClinicPk.Value]));

            //this.Logger.Trace("Action: " + this.updatedAction + "; Username::" + this.commonAppSession.LoginUserInfoSession.UserName + "; StoreId::" + this.hfBusinessStoreId.Value + "; ClinicPk::" + this.hfBusinessClinicPk.Value + "; Clinic Location::" + this.lblClientName.Text + "-Clinic " + ((Label)this.grdLocations.Rows[0].FindControl("lblClinicLocation")).Text);
            //Warning to save changes before returning to home page
            switch (this.updatedAction.ToLower())
            {
                case "cancel":
                    if (!is_equal)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showUpdateClinicWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "clinicDetailsChanged")) + "');", true);
                        return;
                    }
                    else
                    {
                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Response.Redirect("~/walgreensHome.aspx");
                    }
                    break;
                case "submit":
                    if (is_equal)
                    {
                        //calling this method to set min date css for clinic date picker
                        this.bindClinicLocations();
                        return;
                    }
                    break;
                case "confirmed":
                case "completed":
                case "cancelled":
                    if (new_store_id > 0)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showStoreChangeWithContactStatusWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "storeChangedWithContactStatusUpdate")) + "');", true);
                        return;
                    }
                    break;
            }

            //Check duration between clinic scheduled on and clinic date
            if (is_check_clinic_dates)
            {
                if (!this.validateClinicDates())
                {
                    this.showAlertMessage = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + string.Format((string)GetGlobalResourceObject("errorMessages", "clinicCreatedBefore2Weeks")) + "','" + this.updatedAction + "');", true);
                    return;
                }
            }

            //checking date and time stamp locally
            XmlNodeList clinic_location_nodes = xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation[@isReassign='0']");
            string failed_location = "";
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "details", this.lblClientName.Text, string.Empty);
            if (!String.IsNullOrEmpty(failed_location))
            {
                this.showAlertMessage = true;
                //calling this methos due to set min date css for picker 
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", failed_location, true);
                return;
            }

            DataTable dt_reassigned_clinics = new DataTable();
            Dictionary<string, string> updated_values = ApplicationSettings.getUpdatedClinicDetails(this.xmlClinicDetails, ((XmlDocument)Session[this.hfBusinessClinicPk.Value]), "Local");
            email_body = updated_values["emailBody"];
            
            if (!string.IsNullOrEmpty(updated_values["historyLog"]))
            {
                //Prepare updated fields xml and append to clinic details xml document
                XmlDocumentFragment xml_frag = this.xmlClinicDetails.CreateDocumentFragment();
                xml_frag.InnerXml = updated_values["historyLog"];
                this.xmlClinicDetails.DocumentElement.AppendChild(xml_frag);
            }

            //reassign clinic if store state is not restricted.
            bool can_reassign_clinic = (!ApplicationSettings.isRestrictedStoreState("MO", this.commonAppSession.LoginUserInfoSession.UserRole));
            return_value = this.dbOperation.updateClinicLocationDetails(business_clinic_pk, "Local", this.commonAppSession.LoginUserInfoSession.UserID, this.updatedAction, this.xmlClinicDetails.InnerXml, can_reassign_clinic, out error_message, out dt_reassigned_clinics);

            if (return_value == -2 && !string.IsNullOrEmpty(error_message))
            {
                //calling this methos due to set min date css for picker 
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message.Substring(4) + "');", true);
                return;
            }
            if (return_value == -4 && !String.IsNullOrEmpty(error_message))
            {
                string validation_message = string.Empty;
                ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "details", this.lblClientName.Text, error_message);
                this.showAlertMessage = true;
                //calling this methos due to set min date css for picker 
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
                return;
            }
            if (return_value == -5 && !string.IsNullOrEmpty(error_message))//Assigned to Restricted store state
            {
                //calling this method due to set min date css for picker 
                this.showAlertMessage = true;
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "disableAddBusinessToStore"), "Missouri", "20") + "');", true);
                return;
            }
            if (return_value == -10 || return_value == -11 || return_value == -12)
            {
                this.showAlertMessage = true;
                txtGroupId.Text = lblGroupId.Text;
                string alert_message = return_value == -10 ? (string)GetGlobalResourceObject("errorMessages", "copayInvalidGroupIds") : (return_value == -11 ? (string)GetGlobalResourceObject("errorMessages", "copayInvalidFluGroupId") : (string)GetGlobalResourceObject("errorMessages", "copayInvalidRoutineGroupId"));
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + alert_message + "');", true);
                return;
            }
            DataTable dt_default_clinic_user = dbOperation.getStoreUsersEmails(new_store_id > 0 ? new_store_id : store_id);
            EmailOperations email_operations = new EmailOperations();

            //Sending clinic details changed email 'Business Information'...
           if (!string.IsNullOrEmpty(email_body) && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
                email_operations.sendClinicDetailsChangedEmail(dt_default_clinic_user, "VoteVax", email_body, string.Empty, this.lblClientName.Text, business_clinic_pk.ToString(), this.hfContactLogPk.Value.Trim(), (new_store_id > 0 ? new_store_id : store_id).ToString(), false);
             

            if (dt_reassigned_clinics != null && dt_reassigned_clinics.Rows.Count > 0 && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
            {
                //Sending store re-assignment email...
                email_operations.sendLocalClinicStoreReassignmentEmail(dt_default_clinic_user, dt_reassigned_clinics);

                //Sending store assignment notification email to assigned store
                email_operations.sendLocalClinicStoreAssignmentEmail(dt_reassigned_clinics, this.hfContactLogPk.Value);
            }

            if (return_value == -3 && !string.IsNullOrEmpty(error_message))
            {
                //calling this methos due to set min date css for picker 
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showMaintainContactLogWarning('" + error_message + "');", true);
                return;
            }
            else if (!is_return_home)
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;

                if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "confirmed")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "confirmedClinic") + "'); window.location.href = 'walgreensVoteVaxClinicDetails.aspx';", true);
                else if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "completed")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicCompleted") + "'); window.location.href = 'walgreensVoteVaxClinicDetails.aspx';", true);
                else if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "cancelled")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicCancelled") + "'); window.location.href = 'walgreensVoteVaxClinicDetails.aspx';", true);
                else
                {
                    if (new_store_id > 0)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensVoteVaxClinicDetails.aspx';", true);
                }
            }
        }
        else
        {
            Session.Remove(this.hfBusinessClinicPk.Value);
            Response.Redirect("~/walgreensHome.aspx");
        }
    }

    /// <summary>
    /// Binds the previous season clinic locations to dropdown
    /// </summary>
    private void bindLocalClinicPreviousLocationToDropdown()
    {
        ddlClinicLocations = (this.grdLocations.Rows.Count > 0) ? (DropDownList)this.grdLocations.Rows[0].FindControl("ddlClinicLocations") : null;
        imgBtnAddLocation = (this.grdLocations.Rows.Count > 0) ? (ImageButton)this.grdLocations.Rows[0].FindControl("imgBtnAddLocation") : null;
        if (this.previousClinicLocation.Rows.Count > 0 && ddlClinicLocations != null)
        {
            ddlClinicLocations.DataSource = this.previousClinicLocation;
            ddlClinicLocations.DataTextField = "naClinicAddress";
            ddlClinicLocations.DataValueField = "rowId";
            ddlClinicLocations.DataBind();
            ddlClinicLocations.Items.Insert(0, new ListItem("Select Location", "0"));
            ddlClinicLocations.Visible = true;
        }
        else
            ddlClinicLocations.Visible = false;
    }

    /// <summary>
    /// Validates clinic duration between clinic date and scheduled date
    /// </summary>
    /// <returns></returns>
    private bool validateClinicDates()
    {
        bool is_date_valid = true;
        int clinic_count = 1;
        var clinic_details_xml = XDocument.Parse(this.xmlClinicDetails.InnerXml);

        var clinic_dates = from dates in clinic_details_xml.Descendants("clinicLocation")
                           select dates.Attribute("clinicDate").Value;

        foreach (var clinic_date in clinic_dates)
        {
            if (clinic_count == 1)
            {
                DateTime previous_clinic_date = Convert.ToDateTime(((XmlDocument)Session[this.hfBusinessClinicPk.Value]).SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicDate").Value);
                DateTime clinic_scheduled_date = Convert.ToDateTime(((XmlDocument)Session[this.hfBusinessClinicPk.Value]).SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicScheduledOn").Value);

                if (previous_clinic_date > clinic_scheduled_date.AddDays(14) && Convert.ToDateTime(clinic_date) <= clinic_scheduled_date.AddDays(14))
                {
                    is_date_valid = false;
                    break;
                }
            }
            else
            {
                if (Convert.ToDateTime(clinic_date) <= DateTime.Now.AddDays(14))
                {
                    is_date_valid = false;
                    break;
                }
            }

            clinic_count++;
        }

        return is_date_valid;
    }
    #endregion

    #region ------------ PRIVATE VARIABLES -----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private WalgreenEmail walgreensEmail = null;
    private int contactLogPk;
    private string updatedAction;
    private DataTable dtClinicImmunizations;
    private XmlDocument xmlClinicDetails;
    private bool showAlertMessage = false;
    private bool isImmunizationsBlackoutValidationRequired = false;
    private string[] blackedOutImmunizations = { "influenza - standard/pf injectable (trivalent)", "influenza - standard injectable quadrivalent", "influenza - high dose" };
    private bool isDisableEditClinicDetails = false;
    private bool isStoreEditable = false;
    private bool isMOPreviousSeasonBusiness
    {
        get
        {
            return Convert.ToBoolean(hfIsMoPrevious.Value);
        }
        set
        {
            hfIsMoPrevious.Value = value.ToString();
        }
    }
    private DataTable previousClinicLocation
    {
        get
        {
            DataTable result = new DataTable();

            if (ViewState["previousClinicLocation"] != null)
            {
                result = (DataTable)ViewState["previousClinicLocation"];
            }
            return result;
        }
        set
        {
            ViewState["previousClinicLocation"] = value;
        }
    }

    public DropDownList ddlClinicLocations { get; set; }

    public ImageButton imgBtnAddLocation { get; set; }

    private bool isAddressDisabled
    {
        get
        {
            bool value = false;
            if (ViewState["isAddressDisabled"] != null)
                value = (bool)ViewState["isAddressDisabled"];
            return value;
        }
        set
        {
            ViewState["isAddressDisabled"] = value;
        }
    }
    private bool isRestrictedStoreState
    {
        get
        {
            bool value = false;
            if (ViewState["isRestrictedStoreState"] != null)
                value = (bool)ViewState["isRestrictedStoreState"];
            return value;
        }
        set
        {
            ViewState["isRestrictedStoreState"] = value;
        }
    }
    #endregion

    #region ----------------- PUBLIC FUNCTIONS -----------------
    [WebMethod]
    public static void logJSerrors(string log_details)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        Logger logger = LogManager.GetLogger("ClinicDetailsLogger");
        logger.Info("Clinic Details Update - VoteVax ; Username::" + common_app_session.LoginUserInfoSession.UserName + "; " + log_details);
    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        this.updatedAction = string.Empty;
        this.dtClinicImmunizations = new DataTable();
        this.xmlClinicDetails = new XmlDocument();
    }
}