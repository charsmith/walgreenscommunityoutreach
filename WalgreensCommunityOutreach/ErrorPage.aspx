﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorPage.aspx.cs" Inherits="ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />    
    <style type="text/css">
        .style1
        {
            height: 41px;
        }
    </style>
    
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="936" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
  	    <td class="landingHeadBkgd">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="312" align="left" valign="top" style="padding:12px 0px 0px 16px;"><img src="images/wags_landing_logo.png" width="312" alt="Walgreens Community Outreach Logo"/><br />
                <span class="outreachTitle">Community Outreach</span></td>
                <td width="593" colspan="2" rowspan="2" align="right" valign="top"><img src="../images/spacer.gif" width="1" height="302" alt="spacer" /></td>
            </tr>
            <tr>
    		    <td align="left" valign="bottom" style="padding:0px 0px 12px 20px;">&nbsp;</td>
  		    </tr>
            <tr>
    		    <td colspan="3" align="left" valign="top" class="navBar"><img src="images/spacer.gif" alt="" name="" width="1" height="25" border="0" id="home" /></td>
  		    </tr>
            <tr>
                <td colspan="3" bgcolor="#FFFFFF">
                     <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td valign="top" class="pageTitle2">Error Page.</td>
                    </tr>
                    <tr><td align="left" valign="middle" class="formFields"><p>You have 
                        encountered a system error. Please <asp:LinkButton runat="server" ID="lnkLogOut" OnClick="lnkLogOut_Click" Text="log out"></asp:LinkButton>
                        &nbsp;or <a href="javascript:history.go(-1);">go back</a>&nbsp;and try again.</p></tr>
                    <tr><td align="left" valign="top" class="style1"><asp:Label runat="server" class="formFields" id="lblError" Visible="false" Font-Bold="true" ForeColor="Red"></asp:Label></td></tr>
                    <tr>
                        <td width="601" valign="top"> &nbsp;</td>
                    </tr>
                </table>
                </td>
            </tr>       
        </table>
    </td>
  </tr>
</table>
    <p>&nbsp;</p>
    </form>
</body>
</html>
