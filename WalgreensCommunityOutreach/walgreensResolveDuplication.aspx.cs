﻿using System;
using TdApplicationLib;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Drawing;
using System.Web.UI;
using TdWalgreens;

public partial class walgreensResolveDuplication : Page
{
    #region ------------------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDuplicateBusiness();
        }
    }

    protected void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        StringBuilder remove_businesses_list = new StringBuilder();
        StringBuilder keep_businesses_list = new StringBuilder();
        RadioButtonList rbl_resolution;

        foreach (GridViewRow row in this.grdDuplicateBusiness.Rows)
        {
            rbl_resolution = (RadioButtonList)row.FindControl("rblResolution");

            if (!string.IsNullOrEmpty(rbl_resolution.SelectedValue) && Convert.ToInt32(rbl_resolution.SelectedValue) == 0)
                remove_businesses_list.Append(((Label)row.FindControl("lblBusinessPk")).Text + ",");
            if (!string.IsNullOrEmpty(rbl_resolution.SelectedValue) && Convert.ToInt32(rbl_resolution.SelectedValue) == 1)
                keep_businesses_list.Append(((Label)row.FindControl("lblBusinessPk")).Text + ",");
        }

        int return_value = this.dbOperation.processDuplicateBusiness(remove_businesses_list.ToString().TrimEnd(','), keep_businesses_list.ToString().TrimEnd(','));
        this.bindDuplicateBusiness();
    }

    protected void grdDuplicateBusiness_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contacts = (Label)e.Row.FindControl("lblContacts");
            Label lbl_keep_business = (Label)e.Row.FindControl("lblKeepBusiness");
            RadioButtonList rbl_resolution = (RadioButtonList)e.Row.FindControl("rblResolution");
            if ((!(string.IsNullOrEmpty(lbl_contacts.Text)) && Convert.ToInt32(lbl_contacts.Text) > 0) || (!string.IsNullOrEmpty(lbl_keep_business.Text) && Convert.ToBoolean(lbl_keep_business.Text)))
            {
                foreach (ListItem item in rbl_resolution.Items)
                {
                    if (item.Text.ToLower() == "keep")
                        item.Selected = true;

                    if (!(string.IsNullOrEmpty(lbl_contacts.Text)) && Convert.ToInt32(lbl_contacts.Text) > 0)
                        item.Enabled = false;
                }
            }
            Label lbl_duplicate_type = (Label)e.Row.FindControl("lblDuplication");
            if (lbl_duplicate_type.Text.Substring(0, 2) == ", ")
                lbl_duplicate_type.Text = lbl_duplicate_type.Text.Replace(lbl_duplicate_type.Text.Substring(0, 2), "");
            if (lbl_duplicate_type.Text.EndsWith(", "))
                lbl_duplicate_type.Text = lbl_duplicate_type.Text.Replace(", ", "");
        }
    }

    protected void grdDuplicateBusiness_sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt_businesses = this.dbOperation.getDuplicateBusinesses(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId));
        if (dt_businesses.Rows.Count > 0)
        {
            ViewState["sortOrder"] = e.SortExpression + "" + getGridSortDirection(e);
            this.bindDuplicateBusiness();
            this.setDuplicateRowsBackColor();
        }
    }

    protected void grdDuplicateBusiness_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdDuplicateBusiness.PageIndex = e.NewPageIndex;
        this.bindDuplicateBusiness();
    }

    protected void btnRefresh_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
     
    }
    #endregion

    #region ------------------- PRIVATE METHODS ------------
    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }

    /// <summary>
    /// Sets row color to matching duplicate records
    /// </summary>
    private void setDuplicateRowsBackColor()
    {
        //Default values
        string str_previous_data_key_value = "";
        int data_key = 0;
        Color row_color = System.Drawing.ColorTranslator.FromHtml("#E9E9E9");
        foreach (GridViewRow row in this.grdDuplicateBusiness.Rows)
        {
            if (this.grdDuplicateBusiness.DataKeys[row.RowIndex][2].ToString().ToLower() == "address, phone")
                data_key = 1;
            else if (this.grdDuplicateBusiness.DataKeys[row.RowIndex][2].ToString().ToLower() == "phone")
                data_key = 1;
            else if (this.grdDuplicateBusiness.DataKeys[row.RowIndex][2].ToString().ToLower() == "phone")
                data_key = 0;



            if (!str_previous_data_key_value.Equals(this.grdDuplicateBusiness.DataKeys[row.RowIndex][data_key].ToString()))
                row_color = row_color == Color.White ? System.Drawing.ColorTranslator.FromHtml("#E9E9E9") : Color.White;
            row.BackColor = row_color;
            str_previous_data_key_value = this.grdDuplicateBusiness.DataKeys[row.RowIndex][data_key].ToString();
        }
    }

    /// <summary>
    /// Binds and Displays duplicate businesses
    /// </summary>
    private void bindDuplicateBusiness()
    {
        DataTable dt_businesses = this.dbOperation.getDuplicateBusinesses(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId));
        this.commonAppSession.LoginUserInfoSession.DuplicateBusinessCount = dt_businesses.Rows.Count;
        if (dt_businesses.Rows.Count > 0)
        {
            if (ViewState["sortOrder"] != null)
                dt_businesses.DefaultView.Sort = ViewState["sortOrder"].ToString();

            this.grdDuplicateBusiness.DataSource = dt_businesses;
            this.grdDuplicateBusiness.DataBind();
            this.setDuplicateRowsBackColor();
        }
        else
        {
            Response.Redirect("walgreensHome.aspx");
        }
    }
    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}