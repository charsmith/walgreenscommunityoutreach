﻿//Common functions for all pages
function MouseOverImage(imgName, url) {
    document.getElementById(imgName).src = url;
    var path = document.getElementById(imgName).src;
    var path_exist = path.search("reports");
    if (path_exist > 0) {
        path = path.replace("/reports", "");
        document.getElementById(imgName).src = path;
    }
    path_exist = path.search("corporateUploader");
    if (path_exist > 0) {
        path = path.replace("/corporateUploader", "");
        document.getElementById(imgName).src = path;
    }
}

function MouseOutImage(imgName, url) {
    document.getElementById(imgName).src = url;
    var path = document.getElementById(imgName).src;
    var path_exist = path.search("reports");
    if (path_exist > 0) {
        path = path.replace("/reports", "");
        document.getElementById(imgName).src = path;
    }
    path_exist = path.search("corporateUploader");
    if (path_exist > 0) {
        path = path.replace("/corporateUploader", "");
        document.getElementById(imgName).src = path;
    }
}

function disabledropdown() {
    $('select').hide();
}
function enabledropdown() {
    $('select').show();
}

function dropdownRepleceText(ddlCtrl, txtCtrl) {
    $("#" + txtCtrl).hide(); 

    $("#reportsMenu").mouseover(function (event) {
        $("#" + ddlCtrl).hide();
        $("#" + txtCtrl).show();
        $("#" + txtCtrl).val($("#" + ddlCtrl + " option:selected").text());
    });

    $("#reportsMenu").mouseout(function (event) {
        $("#" + ddlCtrl).show();
        $("#" + txtCtrl).hide();
        $("#" + txtCtrl).val("");
    });

    $("#dropmenu1_a").mouseover(function (event) {
        $("#" + ddlCtrl).hide();
        $("#" + txtCtrl).show();
        $("#" + txtCtrl).val($("#" + ddlCtrl + " option:selected").text());       
    });

    $("#dropmenu1_a").mouseout(function (event) {
        $("#" + ddlCtrl).show();
        $("#" + txtCtrl).hide();
        $("#" + txtCtrl).val("");
    });

    $("#dropmenu1_b").mouseover(function (event) {
        $("#" + ddlCtrl).hide();
        $("#" + txtCtrl).show();
        $("#" + txtCtrl).val($("#" + ddlCtrl + " option:selected").text());
    });

    $("#dropmenu1_b").mouseout(function (event) {
        $("#" + ddlCtrl).show();
        $("#" + txtCtrl).hide();
        $("#" + txtCtrl).val("");
    });

    $("#dropmenu1_c").mouseover(function (event) {
        $("#" + ddlCtrl).hide();
        $("#" + txtCtrl).show();
        $("#" + txtCtrl).val($("#" + ddlCtrl + " option:selected").text());
    });

    $("#dropmenu1_c").mouseout(function (event) {
        $("#" + ddlCtrl).show();
        $("#" + txtCtrl).hide();
        $("#" + txtCtrl).val("");
    });

    $("#dropmenu1_d").mouseover(function (event) {
        $("#" + ddlCtrl).hide();
        $("#" + txtCtrl).show();
        $("#" + txtCtrl).val($("#" + ddlCtrl + " option:selected").text());
    });

    $("#dropmenu1_d").mouseout(function (event) {
        $("#" + ddlCtrl).show();
        $("#" + txtCtrl).hide();
        $("#" + txtCtrl).val("");
    });
}

function textBoxOnBlur(elementRef) {
    //5754578787, it should be 575-457-8787
    var elementValue = elementRef.value;
    if (elementValue != '') {
        // Remove all "(", ")", "-", and spaces...
        elementValue = elementValue.replace(/\(/g, '');
        elementValue = elementValue.replace(/\)/g, '');
        elementValue = elementValue.replace(/\-/g, '');
        elementValue = elementValue.replace(/\s+/g, '')

        if (elementValue.length == 10) {
            elementRef.value = (elementValue.substr(0, 3) + '-' + elementValue.substr(3, 3) + '-' + elementValue.substr(6, 4));
            return;
        }
    }
}

function ValidateCompanyPhoneFax(sender, args) {
    args.Value = args.Value.replace(/\(/g, '');
    args.Value = args.Value.replace(/\)/g, '');
    args.Value = args.Value.replace(/\-/g, '');
    args.Value = args.Value.replace(/\s+/g, '')
    var val = args.Value;
    var regEx = /^[0-9]+$/;
    if (!regEx.test(val)) {
        args.IsValid = false;
    }
    else {
        if (val > 0) {
            if (args.Value.length != 10)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        else
            args.IsValid = false;
    }
}

function validateZipCode(sender, args) {
    var val = args.Value;
    var regEx = /^[0-9]+$/;

    if (!regEx.test(val)) {
        args.IsValid = false;
    }
    else {
        if (val.length == 5) {
            if (val > 0) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }
        else
            args.IsValid = false;
    }
}

function validateAddress(sender, args) {
    var val = document.getElementById('txtAddress1').value.trim() + document.getElementById('txtAddress1').value.trim();
    var isvalid = validatePO(val);
    args.IsValid = isvalid;
}

function GetRadWindowA() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}

function winClose() {
    GetRadWindowA().Close();

}

function alertMessages(alertMesg, mesg) {
    if (alertMesg != "")
        alertMesg += "\r\n" + mesg;
    else
        alertMesg = mesg;

    return alertMesg;
}

function checkSpamWordsXML(searchTextObject) {
    var searchText = searchTextObject.value;

    var spamWords = new Array();
    spamWords = readXMLFile(searchText);

    var str = "";
    for (var i = 0; i < spamWords.length; i++) {
        str += spamWords[i] + ", ";
    }

    if (spamWords.length > 0) {
        alert("Application found the following words : " + str.slice(0, -2).toUpperCase() + " as spam. Please remove and continue");
        return
    }
}

function readXMLFile(textWord) {
    var xmlhttp;
    var flag = false;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("GET", "../xmlDoc/spamWords.xml", false);
    xmlhttp.send();
    var keys = xmlhttp.responseXML.getElementsByTagName("word");
    var spamWords = new Array();
    for (var i = 0; i < keys.length; i++) {
        if ((textWord.toLowerCase().search(keys[i].childNodes[0].nodeValue.toLowerCase())) != -1) {
            spamWords.push(keys[i].childNodes[0].nodeValue);
        }
    }

    return spamWords;
}

function pwdConfirmationAlert() {
    var pwdConfirmation = window.confirm('Password is not provided.\r\nClick Ok to set the default password for the user or Cancel to set password.');
    __doPostBack('PwdConfirmationPostBack', pwdConfirmation);
}

function doClick(buttonName, e) {
    //the purpose of this function is to allow the enter key to 
    //point to the correct button to click.
    var key;
    alert("a");
    if (window.event)
        key = window.event.keyCode;     //IE
    else
        key = e.which;     //firefox

    if (key == 13) {
        //Get the button the user wants to have clicked
        var btn = document.getElementById(buttonName);
        if (btn != null) { //If we find the button click it
            btn.click();
            event.keyCode = 0
        }
    }
}

function disableEnterKey(e) {
    var key;
    if (window.event)
        key = window.event.keyCode; //IE
    else
        key = e.which; //firefox      

    return (key != 13);
}

/*Contract Agreement functions & validation -- START*/
function addTime(oldTime) {
    var time = oldTime.split(":");
    var hours = time[0];
    var ampm = time[1].substring(2, 4);
    var minutes = time[1].substring(0, 2);
    var time_new = '';
    if (+minutes >= 30) {
        hours = (+hours + 1) % 24;
    }
    minutes = (+minutes + 30) % 60;
    if (hours >= 12) {
        time_new = hours - 12 + ':' + minutes + 'pm';
    }
    else
        time_new = hours + ':' + minutes + ampm;
    return time_new;
}

function alertMessages(alertMesg, mesg) {
    if (alertMesg != "")
        alertMesg += "\r\n- " + mesg;
    else
        alertMesg = mesg;

    return alertMesg;
}

//Validates junk characters in text fields
function validateJunkCharacters(value) {
    var reg_ex = /^[^<>]+$/;
    var is_valid = true;
    if (!reg_ex.test(value)) {
        is_valid = false;
    }

    return is_valid;
}

//Validates phone number
function validatePhoneNumber(value) {
    var reg_ex = /^[0-9]+$/;
    var is_valid = true;
    value = value.replace(/\(/g, '').replace(/\)/g, '').replace(/\-/g, '').replace(/\s+/g, '');

    if (!reg_ex.test(value)) {
        is_valid = false;
    }
    else if (value == 0 || value.length != 10) {
        is_valid = false;
    }
    return is_valid;
}

function validateDecimal(value)    {
    var reg_ex = /^\d*\.?\d{0,2}$/;
    if(reg_ex.test(value)){
       return true;
    }else{
       return false;
    }
}
//Validates PO Box
function validatePO(value) {
    var is_valid = true;
    //var val = document.getElementById("txtAddress1").value + document.getElementById("txtAddress2").value;
    var val = value;
    val = val.replace(/\(/g, '');
    val = val.replace(/\)/g, '');
    val = val.replace(/\-/g, '');
    val = val.replace(/\s+/g, '');
    val = val.replace(/\./g, '');
    val = val.replace(/\#/g, '');
    val = val.replace(/\:/g, '');
    var regEx = /^[0-9a-zA-Z]+$/;
    if (regEx.test(val)) {
        if (val.toLowerCase().indexOf("pobox") > -1)
            is_valid = false;
        else
            is_valid = true;
    }
    return is_valid;
}

//Validates zip code
function validateZip(value) {
    var reg_ex = /^\d{5}(-\d{4})?$/;
    var is_valid = true;
    if (!reg_ex.test(value)) {
        is_valid = false;
    }
    else if (value.length != 5 || value == '00000') {
        is_valid = false;
    }
    return is_valid;
}

//Validates email
function validateEmail(value) {
    var reg_ex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    var is_valid = true;
    if (!reg_ex.test(value)) {
        is_valid = false;
    }

    return is_valid;
}

function validateData(flag, value, isZeroCheck) {
    var is_valid = true;
    if (flag == 'numeric') {
        is_valid = isZeroCheck ? validateNumeric(value, isZeroCheck) : validateNumeric(value);
    }
    else if (flag == 'decimal') {
        is_valid = validateDecimal(value); 
    }
    else if (flag == 'email') {
        is_valid = validateEmail(value);
    }
    else if (flag == 'junk' || flag == 'address') {
        is_valid = validateJunkCharacters(value);
    }
    else if (flag == 'phone') {
        is_valid = validatePhoneNumber(value);
    }
    else if (flag == 'zipcode') {
        is_valid = validateZip(value);
    }
    else if (flag == 'AgreementToEmails') {
    	is_valid = validateAgreementToEmailMultiple(value);
    }
    return is_valid;
}

function validateNumeric(value) {
    var reg_ex = /^[0-9]+$/;
    var is_valid = true;

    if (!reg_ex.test(value)) {
        is_valid = false;
    }
    return is_valid;
}

function validateNumeric(value, isZeroCheck) {
    var reg_ex = /^[0-9]+$/;
    var is_valid = true;

    if (!reg_ex.test(value) || (isZeroCheck && value.charAt(0) == "0")) {
        is_valid = false;
    }
    return is_valid;
}

function validateAgreementToEmailMultiple(value) {
    var val = value;
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var email_ids = val.replace(/\n/g, ",").replace(";", ",").replace(/;/g, ",");
    email_ids = email_ids.split(",");
    var valid_emails = true;
    for (var i = 0; i < email_ids.length; i++) {
        if (!emailPattern.test($.trim(email_ids[i]))) {
            valid_emails = false;
        }
    }
    return valid_emails;
}

var ret_val = '';
function showUpdateClinicWarning(alert) {
    $(function () {
        $("#divConfirmDialog").html(alert);
        $("#divConfirmDialog").dialog({
            closeOnEscape: false,
            beforeclose: function (event, ui) { return false; },
            dialogClass: "noclose",
            height: 200,
            width: 375,
            title: "Clinic Details Updated",
            buttons: {
                'Yes': function () {
                    ret_val = true;
                    $(this).dialog('close');
                    validateOnDemand();
                },
                'No': function () {
                    ret_val = false;
                    $(this).dialog('close');
                    validateOnDemand();
                }
            },
            modal: true
        });
    });
}

function showDateTimeStampValidationWarning(alert, titleText) {
    $(function () {
        $("#divConfirmDialog").css({ 'font-weight': 'normal' });
        $("#divConfirmDialog").css({ 'text-align': 'left' });
        $("#divConfirmDialog").html(alert);
        $("#divConfirmDialog").dialog({
            closeOnEscape: false,
            beforeclose: function (event, ui) { return false; },
            dialogClass: "noclose",
            height: 200,
            width: 375,
            title: titleText,
            buttons: {
                'Continue': function () {
                    ret_val = true;
                    $(this).dialog('close');
                    __doPostBack("datetimeOverlap", 1);
                },
                'Cancel': function () {
                    ret_val = false;
                    $(this).dialog('close');
                    __doPostBack("datetimeOverlap", 0);
                }
            },
            modal: true
        });
    });
}

function showDateTimeStampValidationMessage(alert, titleText) {
    $(function () {
        $("#divConfirmDialog").css({ 'font-weight': 'normal' });
        $("#divConfirmDialog").css({ 'text-align': 'left' });
        $("#divConfirmDialog").html(alert);
        $("#divConfirmDialog").dialog({
            closeOnEscape: false,
            beforeclose: function (event, ui) { return false; },
            dialogClass: "noclose",
            height: 200,
            width: 375,
            title: titleText,
            buttons: {
                'Ok': function () {
                    $(this).dialog('close');
                }
            },
            modal: true
        });
    });
}


function validateOnDemand() {
    var is_valid = true;
    if (ret_val) {

        //if (typeof (Page_Validators) != "undefined") {
        is_valid = CustomValidatorForLocations('UpdateClinic', 'btnCancel');
        if (is_valid) {
            isPageValid = false;
            __doPostBack("clinicDetailsUpdated", 1);
        }
        else
            Page_BlockSubmit = true;
        //        }
        //        else {
        //            isPageValid = false;
        //            __doPostBack("clinicDetailsUpdated", 0);
        //        }
    }
    else {

        isPageValid = false;
        __doPostBack("clinicDetailsUpdated", 0);

    }
}
/*Contract Agreement functions & validation -- END*/

function showWarning(title, alert, target) {
    $(function () {
        $("#divConfirmDialog").html(alert);
        $("#divConfirmDialog").dialog({
            closeOnEscape: false,
            beforeclose: function (event, ui) { return false; },
            dialogClass: "noclose",
            //height: 200,
            width: 425,
            autoResize:true ,
            title: title,
            buttons: {
                'Continue': function () {
                    __doPostBack(target, 1);
                    $(this).dialog('close');
                },
                "Cancel": function () {
                    $(this).dialog('close');
                }
            },
            modal: true
        });
    });
}
function showValidationSummaryAlert(alert, handler) {
    $(function () {
        $("#divConfirmDialog").html(alert);
        $("#divConfirmDialog").dialog({
            closeOnEscape: false,
            beforeclose: function (event, ui) { return false; },
            dialogClass: "noclose",
            height: 'auto',
            width: 780,
            title: "Please resolve the below issues to continue further",
            buttons: {
                'Ok': function () {
                    $(this).dialog('close');
                }
            },
            modal: true
        });
    });
    return false;
}

function showValidationSummaryWarning(alert, handler) {
    $(function () {
        $("#divConfirmDialog").html(alert);
        $("#divConfirmDialog").dialog({
            closeOnEscape: false,
            beforeclose: function (event, ui) { return false; },
            dialogClass: "noclose",
            height: 'auto',
            width: 780,
            title: "Please resolve the below issues to continue further",
            buttons: {
                'Override': function () {
                    __doPostBack(handler, "isValid");
                    $(this).dialog('close');
                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            },
            modal: true
        });
    });
    return false;
}
function storeSearch(userId) {
      $(function () {
        function log(selected, storeidvalue) {

            $("#walgreensHeaderCtrl_txtStoreId").val(storeidvalue);
            $("#walgreensHeaderCtrl_txtStoreProfiles").val(selected);
            $("#walgreensHeaderCtrl_btnRefresh").click();
        }
        $("#storeId").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "GET",
                    url: '/WalgreensStoreSearchService.asmx/GetStoreData',
                    data: {
                        parameter: request.term,
                        type: "storeId",
                        userId: userId
                    },
                    dataType: "text",
                    success: function (data) {
                        var items;
                        $("#walgreensHeaderCtrl_txtStoreProfiles").val("Nothing selected, input was " + $('#storeId').val());
                        $("#walgreensHeaderCtrl_txtStoreId").val(0);
                        if (data != "") {
                            items = JSON.parse(data);
                            response($.map(items, function (item) {
                                if (item.name == "Number$format" || item.name == "Number$localeFormat" || item.name == "Number$_toFormattedString")
                                    return;
                                return {
                                    label: item.name,
                                    value: item.id
                                }
                            }));
                        }
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function (event, ui) {
            log(ui.item.label, ui.item.value);
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                if (window.location.pathname.indexOf("walgreensHome.aspx") == -1)
                    disabledropdown();
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                if (window.location.pathname.indexOf("walgreensHome.aspx") == -1)
                    enabledropdown();
            }
        });
    });
}
