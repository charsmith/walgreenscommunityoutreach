﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="encryptedLoginLink.aspx.cs"
    Inherits="encryptedLoginLink" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="WFooter" TagPrefix="ucFooter" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <style type="text/css">
        .loginLink
        {
            color: #333333;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <table width="936" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="312" align="left" valign="top" bgcolor="#FFFFFF" style="padding:12px 0px 0px 16px;">
                            <img src="images/wags_logo.png" width="225" height="52" /><br />
                            <span class="outreachTitle">Community Outreach</span>
                        </td>
                        <td width="593" colspan="2" rowspan="2" align="right" valign="top" bgcolor="#FFFFFF">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="bottom" style="padding: 0px 0px 12px 20px;" bgcolor="#FFFFFF">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right" valign="middle" class="navBar">
                            <img src="images/spacer.gif" alt="" name="" width="1" height="25" border="0" id="home" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2" width="100%" valign="top" bgcolor="#f9f9f9">
                                        <form id="form2" runat="server">
                                        <table width="75%" align="center" cellpadding="0" cellspacing="5">
                                            <tr>
                                                <td align="right" width="20%" valign="bottom">
                                                    <span class="logSubTitles">User Email:&nbsp;</span>
                                                </td>
                                                <td align="left" width="80%" valign="bottom">
                                                    <asp:TextBox ID="txtUserEmail" Width="250px" MaxLength="250" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                                    <asp:ImageButton ID="btnSubmit" ImageUrl="~/images/btn_submit_img.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_img.png');"
                                                        CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_img_lit.png');"
                                                        runat="server" Width="60" Height="26" OnClick="btnSubmit_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" width="20%" valign="middle">
                                                    <span class="logSubTitles">Login Link:&nbsp;</span>
                                                </td>
                                                <td align="left" width="80%" valign="middle">
                                                    <asp:Label CssClass="loginLink" ID="lblUserLoginLink" Width="550px" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucFooter:WFooter ID="walgreensFooter" runat="server" />
</body>
</html>
