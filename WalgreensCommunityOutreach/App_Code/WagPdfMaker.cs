﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TdWalgreens
{
    public class WagPdfMaker
    {
        public WagPdfMaker(String pdf_in_path)
        {
            this.pdfInPath = pdf_in_path;
        }

        XGraphics gfx;
        public byte[] makePdf(WagStringDictionary wagPdfDictionary, int vaccine_options_count = 0, string image_path = null)
        {
            PdfDocument pdf_doc = PdfReader.Open(this.pdfInPath, PdfDocumentOpenMode.Import);
            PdfDocument pdf_new_doc = new PdfDocument();

            for (int the_page = 0; the_page < pdf_doc.Pages.Count; the_page++)
            {
                PdfPage pp = pdf_new_doc.AddPage(pdf_doc.Pages[the_page]);
                gfx = XGraphics.FromPdfPage(pp);
                XFont font;
                foreach (WagPdfString wps in wagPdfDictionary.Values)
                {
                    if ((wps.page - 1) == the_page)
                    {
                        XBrush redBrush = new XSolidBrush(wps.redBrush);
                        string font_weight = wps.fontweight;
                        XFontStyle fontWeight = (XFontStyle)Enum.Parse(typeof(XFontStyle), font_weight);
                        font = new XFont(wps.fontName, wps.pt, fontWeight);
                        gfx.DrawString(wps.pdfString, font, redBrush, new XPoint(wps.x, wps.y));
                    }
                }
            }
            if (vaccine_options_count > 0 && !string.IsNullOrEmpty(image_path))
            {
                int value = 586;
                for (int imm_count = 1; imm_count <= vaccine_options_count; imm_count++)
                {
                    XImage image = XImage.FromFile(image_path);
                    gfx.DrawImage(image, 415, value, 15, 15);
                    value = value + 24;
                }
            }

            using (MemoryStream new_pdf_stream = new MemoryStream())
            {
                pdf_new_doc.Save(new_pdf_stream);
                return new_pdf_stream.ToArray();
            }
        }
        public string pdfInPath { get; private set; }
    }
}
