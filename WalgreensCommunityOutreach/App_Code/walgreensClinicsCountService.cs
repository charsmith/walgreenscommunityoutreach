﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using TdApplicationLib;
using TdWalgreens;

/// <summary>
/// service to get clinics count on each store 
/// </summary>
[WebService(Namespace = "https://wagoutreach.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class walgreensClinicsCountService : System.Web.Services.WebService
{

    public walgreensClinicsCountService()
    {
    }

    [WebMethod]
    public XmlDocument getOutreachStatusCountByStore()
    {
        try
        {
            DBOperations dbOperations = new DBOperations();
            return dbOperations.getClinicsCountXML();
        }
        catch (Exception ex)
        {
            logError(ex);
            throw RaiseException("https://wagoutreach.com/", "https://wagoutreach.com/", "Problem with Service ,Please contact Administrator", "200", "Contact the Administrator");
        }

    }
    public void logError(Exception ex)
    {
        CustomExceptionHandler cust_exce = new CustomExceptionHandler();
        string log_string = string.Empty;
        log_string = "Walgreens Clinics Count Service Client ";
        string displayErrorMessage = cust_exce.getErrorMessage(ex, log_string);

    }
    //Creates the SoapException object with all the error details
    public SoapException RaiseException(string uri, string webServiceNamespace, string errorMessage, string errorNumber, string errorSource)
    {
        XmlQualifiedName faultCodeLocation = SoapException.ServerFaultCode;
        XmlDocument xmlDoc = new XmlDocument();
        //Create the Detail node
        XmlNode rootNode = xmlDoc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
        XmlNode errorNode = xmlDoc.CreateNode(XmlNodeType.Element, "Error", webServiceNamespace);
        XmlNode errorNumberNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorNumber", webServiceNamespace);
        errorNumberNode.InnerText = errorNumber;
        XmlNode errorMessageNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorMessage", webServiceNamespace);
        errorMessageNode.InnerText = errorMessage;
        XmlNode errorSourceNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorSource", webServiceNamespace);
        errorSourceNode.InnerText = errorSource;
        errorNode.AppendChild(errorNumberNode);
        errorNode.AppendChild(errorMessageNode);
        errorNode.AppendChild(errorSourceNode);
        rootNode.AppendChild(errorNode);
        SoapException soapEx = new SoapException(errorMessage, faultCodeLocation, uri, rootNode);
        return soapEx;
    }

}
