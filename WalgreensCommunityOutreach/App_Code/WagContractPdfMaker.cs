﻿using PdfSharp.Drawing;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace TdWalgreens
{
    public class WagContractPdfMaker
    {
        public WagContractPdfMaker()
        {

        }

        /// <summary>
        /// Generate bytes data for Voucher PDF from the dataset
        /// </summary>
        /// <param name="ds_voucher_var_data"></param>
        /// <param name="pdf_type"></param>
        /// <returns></returns>
        public byte[] getVoucherFormBytes(DataSet ds_voucher_var_data, string pdf_type)
        {
            byte[] pdf_bytes = null;
            string pdf_path = string.Empty;
            WagStringDictionary wagStringDictionary = new WagStringDictionary();
            int vaccine_options_count = 0;
            string image_path = HttpContext.Current.Server.MapPath("~/images/btn_check_box.PNG");
            int clinic_group_id = 1;
            int vaccine_id = 1;
            int clinic_groupId_dimension;
            int abbreviation_dimension = 598;

            string business_name = ds_voucher_var_data.Tables[0].Rows[0]["businessName"].ToString();

            string state_specific_path = null;

            if (ds_voucher_var_data.Tables.Count > 1)
            {
                DataTable dt_voucher_details = ds_voucher_var_data.Tables[1].Select().CopyToDataTable();

                if ((pdf_type.ToUpper() == "FLUEN" || pdf_type.ToUpper() == "FLUSP") && dt_voucher_details.Select("groupIdType='flu'").Count() > 0)
                {
                    DataTable dt_flu_details = dt_voucher_details.Select("groupIdType = 'flu'").CopyToDataTable();
                    decimal copay_value = dt_flu_details.AsEnumerable()
                                        .Select(row => row.Field<decimal?>("copayValue"))
                                        .Where(val => val.HasValue)
                                        .Select(val => Convert.ToDecimal(val.Value))
                                        .Sum();
                    List<string> unique_group_ids = dt_flu_details.AsEnumerable()
                                          .GroupBy(row => row.Field<string>("groupId"))
                                          .Select(g => g.First()["groupId"].ToString()).ToList();

                    string unique_groupId_values = string.Join(", ", unique_group_ids);

                    if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
                    {
                        if (copay_value > 0)
                            state_specific_path = ApplicationSettings.getPDFPath(pdf_type.ToUpper().Trim(), pdf_type.ToUpper().Trim().Substring(pdf_type.Length - 2));
                        else
                            state_specific_path = ApplicationSettings.getPDFPath(pdf_type.ToUpper().Trim(), pdf_type.ToUpper().Trim().Substring(pdf_type.Length - 2), "noCopay");

                        pdf_path = ApplicationSettings.resourcesPath + state_specific_path;
                    }

                    if (pdf_type.ToUpper() == "FLUEN")
                    {
                        clinic_groupId_dimension = 640;
                        wagStringDictionary.Add("title", new WagPdfString("Dear " + business_name + " Employee:", 55, 100, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("company", new WagPdfString(business_name, 55, 365, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("clinicGroupId" + clinic_group_id, new WagPdfString(unique_groupId_values, 67, clinic_groupId_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                        foreach (DataRow row in dt_flu_details.Rows)
                        {
                            string[] vaccine_names = Regex.Split(row["immunizationAbvEN"].ToString(), "<br/>");
                            if (vaccine_names.Count() >= 2)
                            {
                                for (int count = 0; count < vaccine_names.Count(); count++)
                                {
                                    wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(vaccine_names[count], 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                    abbreviation_dimension = abbreviation_dimension + 10;
                                    vaccine_id = vaccine_id + 1;
                                }
                                abbreviation_dimension = abbreviation_dimension + 4;
                            }
                            else
                            {
                                wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(row["immunizationAbvEN"].ToString(), 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                abbreviation_dimension = abbreviation_dimension + 24;
                                vaccine_id = vaccine_id + 1;
                            }
                            vaccine_options_count = vaccine_options_count + 1;
                        }
                        wagStringDictionary.Add("Expires", new WagPdfString(Convert.ToDateTime(ds_voucher_var_data.Tables[0].Rows[0]["voucherFluExpDate"]).ToString("MM/dd/yyyy"), 240, 640, 9, 1, XColor.FromArgb(34, 30, 31), "Arial"));

                        if (copay_value > 0)
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("ONE FLU SHOT AT $" + copay_value + ".*", 25, 540, 30, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                            wagStringDictionary.Add("withCopayValue", new WagPdfString("$" + copay_value + "", 489, 148, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                            wagStringDictionary.Add("disclaimerWithCopayValue", new WagPdfString("$" + copay_value + "", 518, 745, 5.4, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        }
                        else
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("ONE FLU SHOT AT NO CHARGE.*", 30, 540, 30, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                            wagStringDictionary.Add("withOutCopayValue", new WagPdfString("no charge", 489, 148, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                            wagStringDictionary.Add("disclaimerWithOutCopayValue", new WagPdfString("No charge", 516, 745, 5.4, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        }
                        wagStringDictionary.Add("company2", new WagPdfString("A wellness benefit brought to you by Walgreens and " + business_name + "", 30, 560, 12, 1, XColor.FromArgb(34, 30, 31), "Arial", "Regular"));
                    }
                    else if (pdf_type.ToUpper() == "FLUSP")
                    {
                        clinic_groupId_dimension = 635;
                        wagStringDictionary.Add("title", new WagPdfString("Estimado(a) empleado de " + business_name + ":", 55, 100, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("company", new WagPdfString(business_name, 55, 390, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("clinicGroupId" + clinic_group_id, new WagPdfString(unique_groupId_values, 85, clinic_groupId_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));

                        foreach (DataRow row in dt_flu_details.Rows)
                        {
                            string[] vaccine_names = Regex.Split(row["immunizationAbvSP"].ToString(), "<br/>");
                            if (vaccine_names.Count() >= 2)
                            {
                                for (int count = 0; count < vaccine_names.Count(); count++)
                                {
                                    wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(vaccine_names[count], 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                    abbreviation_dimension = abbreviation_dimension + 10;
                                    vaccine_id = vaccine_id + 1;
                                }
                                abbreviation_dimension = abbreviation_dimension + 4;
                            }
                            else
                            {
                                wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(row["immunizationAbvSP"].ToString(), 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                abbreviation_dimension = abbreviation_dimension + 24;
                                vaccine_id = vaccine_id + 1;
                            }

                            vaccine_options_count = vaccine_options_count + 1;
                        }
                        wagStringDictionary.Add("Expires", new WagPdfString(Convert.ToDateTime(ds_voucher_var_data.Tables[0].Rows[0]["voucherFluExpDate"]).ToString("MM/dd/yyyy"), 280, 634, 9, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        if (copay_value > 0)
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("UNA VACUNA CONTRA LA GRIPE $" + copay_value + ".*", 27, 535, 25, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                            wagStringDictionary.Add("withCopayValue", new WagPdfString("$" + copay_value + ".", 367, 163, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                            wagStringDictionary.Add("disclaimerWithCopayValue", new WagPdfString("$" + copay_value + "", 528, 739, 5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        }
                        else
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("UNA VACUNA CONTRA LA GRIPE SIN CARGO ALGUNO.*", 27, 533, 20, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                        }

                        wagStringDictionary.Add("company2", new WagPdfString("Un beneficio de bienestar traído a ti por Walgreens y " + business_name + "", 29, 558, 12, 1, XColor.FromArgb(34, 30, 31), "Arial", "Regular"));
                    }
                }
                else if ((pdf_type.ToUpper() == "ROUTINEEN" || pdf_type.ToUpper() == "ROUTINESP") && dt_voucher_details.Select("groupIdType='routine'").Count() > 0)
                {
                    DataTable dt_routine_details = dt_voucher_details.Select("groupIdType='routine'").CopyToDataTable();
                    decimal copay_value = dt_routine_details.AsEnumerable()
                                        .Select(row => row.Field<decimal?>("copayValue"))
                                        .Where(val => val.HasValue)
                                        .Select(val => Convert.ToDecimal(val.Value))
                                        .Sum();
                    List<string> lst_unique_routine_groupIds = dt_routine_details.AsEnumerable()
                                                   .GroupBy(row => row.Field<string>("groupId"))
                                                   .Select(g => g.First()["groupId"].ToString()).ToList();

                    string unique_routine_groupId_values = string.Join(", ", lst_unique_routine_groupIds);

                    if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
                    {
                        if (copay_value > 0)
                            state_specific_path = ApplicationSettings.getPDFPath(pdf_type.ToUpper().Trim(), pdf_type.ToUpper().Trim().Substring(pdf_type.Length - 2));
                        else
                            state_specific_path = ApplicationSettings.getPDFPath(pdf_type.ToUpper().Trim(), pdf_type.ToUpper().Trim().Substring(pdf_type.Length - 2), "noCopay");

                        pdf_path = ApplicationSettings.resourcesPath + state_specific_path;
                    }

                    if (pdf_type.ToUpper() == "ROUTINEEN")
                    {
                        clinic_groupId_dimension = 637;
                        wagStringDictionary.Add("title", new WagPdfString("Dear " + business_name + " Employee:", 55, 100, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("company", new WagPdfString(business_name, 55, 350, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("clinicGroupId" + clinic_group_id, new WagPdfString(unique_routine_groupId_values, 65, clinic_groupId_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                        foreach (DataRow row in dt_routine_details.Rows)
                        {
                            string[] vaccine_names = Regex.Split(row["immunizationAbvEN"].ToString(), "<br/>");
                            if (vaccine_names.Count() >= 2)
                            {
                                for (int count = 0; count < vaccine_names.Count(); count++)
                                {
                                    wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(vaccine_names[count], 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                    abbreviation_dimension = abbreviation_dimension + 10;
                                    vaccine_id = vaccine_id + 1;
                                }
                                abbreviation_dimension = abbreviation_dimension + 4;
                            }
                            else
                            {
                                wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(row["immunizationAbvEN"].ToString(), 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                abbreviation_dimension = abbreviation_dimension + 24;
                                vaccine_id = vaccine_id + 1;
                            }
                            vaccine_options_count = vaccine_options_count + 1;
                        }
                        wagStringDictionary.Add("Expires", new WagPdfString(Convert.ToDateTime(ds_voucher_var_data.Tables[0].Rows[0]["voucherRoutineExpDate"]).ToString("MM/dd/yyyy"), 235, 637, 9, 1, XColor.FromArgb(34, 30, 31), "Arial"));

                        if (copay_value > 0)
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("ONE IMMUNIZATION AT $" + copay_value + ".*", 25, 540, 28, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                            wagStringDictionary.Add("withCopayValue", new WagPdfString("$" + copay_value + "", 170, 148, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                            wagStringDictionary.Add("disclaimerWithCopayValue", new WagPdfString("$" + copay_value + "", 500, 740, 5.4, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        }
                        else
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("ONE IMMUNIZATION AT NO CHARGE.*", 30, 540, 28, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                            wagStringDictionary.Add("withOutCopayValue", new WagPdfString("no charge", 167, 148, 11.7, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                            wagStringDictionary.Add("disclaimerWithOutCopayValue", new WagPdfString("No charge", 500, 740, 5.4, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        }
                        wagStringDictionary.Add("company2", new WagPdfString("A wellness benefit brought to you by Walgreens and " + business_name + "", 30, 560, 12, 1, XColor.FromArgb(34, 30, 31), "Arial", "Regular"));
                    }
                    else if (pdf_type.ToUpper() == "ROUTINESP")
                    {
                        clinic_groupId_dimension = 635;
                        wagStringDictionary.Add("title", new WagPdfString("Estimado(a) empleado de " + business_name + ":", 54, 100, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("company", new WagPdfString(business_name, 55, 372, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        wagStringDictionary.Add("clinicGroupId" + clinic_group_id, new WagPdfString(unique_routine_groupId_values, 85, clinic_groupId_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                        foreach (DataRow row in dt_routine_details.Rows)
                        {
                            string[] vaccine_names = Regex.Split(row["immunizationAbvSP"].ToString(), "<br/>");
                            if (vaccine_names.Count() >= 2)
                            {
                                for (int count = 0; count < vaccine_names.Count(); count++)
                                {
                                    wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(vaccine_names[count], 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                    abbreviation_dimension = abbreviation_dimension + 10;
                                    vaccine_id = vaccine_id + 1;
                                }
                                abbreviation_dimension = abbreviation_dimension + 4;
                            }
                            else
                            {
                                wagStringDictionary.Add("Vaccine" + vaccine_id, new WagPdfString(row["immunizationAbvSP"].ToString(), 430, abbreviation_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                                abbreviation_dimension = abbreviation_dimension + 24;
                                vaccine_id = vaccine_id + 1;
                            }

                            vaccine_options_count = vaccine_options_count + 1;
                        }
                        wagStringDictionary.Add("Expires", new WagPdfString(Convert.ToDateTime(ds_voucher_var_data.Tables[0].Rows[0]["voucherRoutineExpDate"]).ToString("MM/dd/yyyy"), 265, 635, 9, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        if (copay_value > 0)
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("UNA VACUNA CONTRA LA GRIPE $" + copay_value + ".*", 29, 533, 25, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                            wagStringDictionary.Add("withCopayValue", new WagPdfString("$" + copay_value + ".", 517, 148, 11.5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                            wagStringDictionary.Add("disclaimerWithCopayValue", new WagPdfString("$" + copay_value + "", 528, 740, 5, 1, XColor.FromArgb(34, 30, 31), "Arial"));
                        }
                        else
                        {
                            wagStringDictionary.Add("entitle", new WagPdfString("UNA VACUNA CONTRA LA GRIPE SIN CARGO ALGUNO.*", 29, 531, 20, 1, XColor.FromArgb(86, 160, 212), "Arial", "Bold"));
                        }
                        wagStringDictionary.Add("company2", new WagPdfString("Un beneficio de bienestar traído a ti por Walgreens y " + business_name + "", 30, 558, 12, 1, XColor.FromArgb(34, 30, 31), "Arial", "Regular"));
                    }
                }
            }
            
            if (pdf_type.ToUpper() == "VARSP" || pdf_type.ToUpper() == "VAREN")
            {
                string group_id = ds_voucher_var_data.Tables[0].Rows[0]["varGroupId"].ToString();
                string store_id = ds_voucher_var_data.Tables[0].Rows[0]["storeId"].ToString();
                string address = ds_voucher_var_data.Tables[0].Rows[0]["storeAddress"].ToString();
                string state_name = ds_voucher_var_data.Tables[0].Rows[0]["storeState"].ToString();
                string[] store_address = Regex.Split(address, "<br/>");
                int group_id_length = group_id.Length;
                List<string> lst_group_ids = new List<string>();
                int max_length = 26;
                int store_id_dimension = 78;
                int store_address_dimension = 87;
                int store_address_dimension1 = 93;
                int clinic_group_id_dimension = 93;
                int clinic_group_id_dimension1 = 103;
                lst_group_ids = WagContractPdfMaker.splitStrings(max_length, group_id, group_id_length);

                var specific_states = new string[] { "PR", "GA", "NC", "SC", "DR" };

                if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
                {
                    if (specific_states.Contains(state_name.ToUpper().Trim()))
                        state_specific_path = ApplicationSettings.getPDFPath(state_name.ToUpper().Trim(), pdf_type.Substring(pdf_type.Length - 2));
                    else
                    {
                        state_name = "OT";
                        state_specific_path = ApplicationSettings.getPDFPath(state_name.ToUpper().Trim(), pdf_type.Substring(pdf_type.Length - 2));
                    }

                    pdf_path = ApplicationSettings.resourcesPath + state_specific_path;
                }

                if (pdf_type.ToUpper() == "VARSP")
                {
                    int size = 95;
                    if (state_name.ToUpper().Trim() == "PR")
                    {
                        store_id_dimension = 75;
                        store_address_dimension = 84;
                        store_address_dimension1 = 90;
                        size = 94;
                    }
                    else if (state_name.ToUpper().Trim() == "DR")
                    {
                        store_id_dimension = 91;
                        store_address_dimension = 100;
                        store_address_dimension1 = 106;
                        clinic_group_id_dimension = 110;
                        clinic_group_id_dimension1 = 120;
                        size = 108;
                    }
                    else if (state_name.ToUpper().Trim() == "NC")
                    {
                        store_id_dimension = 88;
                        store_address_dimension = 97;
                        store_address_dimension1 = 103;
                        clinic_group_id_dimension = 105;
                        clinic_group_id_dimension1 = 115;
                        size = 105;
                    }
                   else if (state_name.ToUpper().Trim() == "SC")
                    {
                        store_id_dimension = 55;
                        store_address_dimension = 64;
                        store_address_dimension1 = 70;
                        clinic_group_id_dimension = 74;
                        clinic_group_id_dimension1= 84;
                        size = 74;
                    }
                    else if (state_name.ToUpper().Trim() == "GA")
                    {
                        clinic_group_id_dimension = 94;
                        clinic_group_id_dimension1 = 104;
                    }

                    int clinic_count = 0;
                    wagStringDictionary.Add("storeId", new WagPdfString(store_id, 450, store_id_dimension, 6, 1));
                    wagStringDictionary.Add("storeAddress", new WagPdfString(System.Web.HttpUtility.HtmlDecode(store_address[0]), 466, store_address_dimension, 6, 1));
                    wagStringDictionary.Add("storeAddress1", new WagPdfString(System.Web.HttpUtility.HtmlDecode(store_address[1]), 466, store_address_dimension1, 6, 1));
                    if (group_id_length > 26 && group_id_length <= 35)
                    {
                        for (int count = 0; count < lst_group_ids.Count(); count++)
                        {
                            wagStringDictionary.Add("clinicGroupId" + clinic_count + "", new WagPdfString(lst_group_ids[count], 270, size, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                            size = size + 8;
                            clinic_count = clinic_count + 1;
                        }
                    }
                    else if (group_id_length <= 26)
                    {
                        wagStringDictionary.Add("clinicGroupId" + clinic_count + "", new WagPdfString(group_id, 270, size, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                    }
                    else
                    {
                        wagStringDictionary.Add("clinicGroupId", new WagPdfString("Consulte Portal para obtener ", 270, clinic_group_id_dimension, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                        wagStringDictionary.Add("clinicGroupId1", new WagPdfString("la ID de grupo", 270, clinic_group_id_dimension1, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                    }
                }
                else if (pdf_type.ToUpper() == "VAREN")
                {
                    int clinic_count = 0;
                    int size = 88;
                    if (state_name.ToUpper().Trim() == "DR")
                    {
                        store_id_dimension = 68;
                        store_address_dimension = 77;
                        store_address_dimension1 = 83;
                        size = 78;
                    }
                    else if (state_name.ToUpper().Trim() == "NC")
                    {
                        store_id_dimension = 78;
                        store_address_dimension = 87;
                        store_address_dimension1 = 93;
                        size = 88;
                    }
                    else if (state_name.ToUpper().Trim() == "GA")
                    {
                        store_id_dimension = 78;
                        store_address_dimension = 87;
                        store_address_dimension1 = 93;
                        size = 89;
                    }
                    else if (state_name.ToUpper().Trim() == "SC")
                    {
                        store_id_dimension = 71;
                        store_address_dimension = 80;
                        store_address_dimension1 = 89;
                        size = 82;
                    }

                    wagStringDictionary.Add("storeId", new WagPdfString(store_id, 445, store_id_dimension, 6, 1));
                    wagStringDictionary.Add("storeAddress", new WagPdfString(System.Web.HttpUtility.HtmlDecode(store_address[0]), 445, store_address_dimension, 6, 1));
                    wagStringDictionary.Add("storeAddress1", new WagPdfString(System.Web.HttpUtility.HtmlDecode(store_address[1]), 445, store_address_dimension1, 6, 1));
                    if (group_id_length > 26 && group_id_length <= 66)
                    {
                        for (int count = 0; count < lst_group_ids.Count(); count++)
                        {
                            wagStringDictionary.Add("clinicGroupId" + clinic_count + "", new WagPdfString(lst_group_ids[count], 268, size, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                            size = size + 8;
                            clinic_count = clinic_count + 1;
                        }
                    }
                    else if (group_id_length <= 26)
                    {
                        wagStringDictionary.Add("clinicGroupId" + clinic_count + "", new WagPdfString(group_id, 268, size, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                    }
                    else
                    {
                        wagStringDictionary.Add("clinicGroupId", new WagPdfString("Refer to Portal for Group ID", 270, 90, 8, 1, XColor.FromArgb(34, 30, 31), "Arial", "Bold"));
                    }
                }
            }
            if (File.Exists(pdf_path))
            {
                WagPdfMaker wagPdfMaker = new WagPdfMaker(pdf_path);
                pdf_bytes = wagPdfMaker.makePdf(wagStringDictionary, vaccine_options_count, image_path);

            }
            return pdf_bytes;
        }

        /// <summary>
        /// Splits string exceeding 99 characters
        /// </summary>
        /// <param name="maxLength"></param>
        /// <param name="name"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static List<string> splitStrings(int maxLength, string name, int length)
        {
            List<string> lst_group_ids = new List<string>();
            if (length <= 99)
            {
                while (name.Length > maxLength)
                {
                    for (int charIndex = (maxLength); charIndex > 0; charIndex--)
                    {
                        if (char.IsWhiteSpace(name[charIndex]))
                        {
                            lst_group_ids.Add(name.Substring(0, charIndex + 1));
                            name = name.Substring(charIndex + 1);
                            break;
                        }
                    }
                }
            }
            lst_group_ids.Add(name);
            return lst_group_ids;
        }
    }
}