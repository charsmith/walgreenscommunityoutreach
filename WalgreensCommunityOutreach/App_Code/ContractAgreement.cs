﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Xml.XPath;

namespace TdWalgreens
{
    /// <summary>
    /// Summary description for ContractAgreement
    /// </summary>
    public class ContractAgreement
    {
        public ContractAgreement()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// Creates new clinic location
        /// </summary>
        /// <param name="xdoc_clinic_locations"></param>
        /// <param name="is_default"></param>
        /// <returns></returns>
        public XmlDocument createNewClinicLocation(XmlDocument xdoc_clinic_locations, bool is_default)
        {
            int clinics_count = 0;
            XmlElement clinic_locations_node;
            if (is_default)
                clinic_locations_node = xdoc_clinic_locations.CreateElement("Businesses");
            else
                clinic_locations_node = (XmlElement)xdoc_clinic_locations.SelectSingleNode("Businesses");

            clinics_count = (is_default) ? 1 : clinic_locations_node.SelectNodes("Business").Count + 1;

            XmlElement clinic_ele = xdoc_clinic_locations.CreateElement("Business");

            clinic_ele.SetAttribute("clinicLocation", "CLINIC LOCATION " + ((clinics_count > 26) ? 'A' + "" + ((Char)(64 + (clinics_count % 26))).ToString() : ((Char)(64 + clinics_count)).ToString()));
            clinic_ele.SetAttribute("localContactName", string.Empty);
            clinic_ele.SetAttribute("LocalContactEmail", string.Empty);
            clinic_ele.SetAttribute("LocalContactPhone", string.Empty);
            clinic_ele.SetAttribute("clinicDate", string.Empty);
            clinic_ele.SetAttribute("startTime", string.Empty);
            clinic_ele.SetAttribute("endTime", string.Empty);
            clinic_ele.SetAttribute("Address1", string.Empty);
            clinic_ele.SetAttribute("Address2", string.Empty);
            clinic_ele.SetAttribute("city", string.Empty);
            clinic_ele.SetAttribute("state", string.Empty);
            clinic_ele.SetAttribute("zipCode", string.Empty);
            clinic_ele.SetAttribute("EstShots", string.Empty);
            clinic_ele.SetAttribute("isReassign", "0");

            clinic_locations_node.AppendChild(clinic_ele);
            xdoc_clinic_locations.AppendChild(clinic_locations_node);

            return xdoc_clinic_locations;
        }
    }
}