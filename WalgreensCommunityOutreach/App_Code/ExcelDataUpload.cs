﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Xml.Linq;
using TdApplicationLib;
using System.Configuration;

namespace TdWalgreens
{
    public class ExcelDataUpload
    {
        DataTable dtClinicsData;
        string errorMessage, uploadTimeStamp;
        AppCommonSession commonAppSession;
        string[] columnHeaders = null;
        string[] clinicStates = null;

        public ExcelDataUpload(string upload_timestamp)
        {
            this.columnHeaders = this.getColumnsDefinition.Elements().Select(element => element.Attribute("name").Value).ToArray();
            this.clinicStates = this.getStates.Elements().Select(element => element.Attribute("Value").Value).ToArray();
            errorMessage = string.Empty;
            uploadTimeStamp = upload_timestamp;
            commonAppSession = AppCommonSession.initCommonAppSession();

            // Initialize an instance of DataTable
            dtClinicsData = getStrongTypeDataSource();
        }

        /// <summary>
        /// create strong type datatable
        /// </summary>
        /// <returns></returns>
        private DataTable getStrongTypeDataSource()
        {
            dtClinicsData = new DataTable();
            for (int count = 0; count < this.columnHeaders.Length; count++)
            {
                if (this.columnHeaders[count].ToLower().Trim() == "by pass to hosting store #")
                {
                    DataColumn col_by_pass = new DataColumn("By Pass to Hosting Store #", typeof(System.String));
                    col_by_pass.DefaultValue = DBNull.Value;
                    dtClinicsData.Columns.Add(col_by_pass);
                }
                else
                    dtClinicsData.Columns.Add(this.columnHeaders[count], typeof(String));
            }

            //Add remarks column
            dtClinicsData.Columns.Add("Remarks", typeof(String));
            //Add file uploaded on column with default timestamp
            DataColumn col_uploaded_on = new DataColumn("UploadedOn", typeof(System.String));
            col_uploaded_on.DefaultValue = this.uploadTimeStamp;
            dtClinicsData.Columns.Add(col_uploaded_on);

            //Add unique row identifier   
            dtClinicsData.Columns.Add("origPk", typeof(System.Int32));

            dtClinicsData.Columns.Add("isValidRecord", typeof(System.Boolean));
            dtClinicsData.Columns.Add("clientContactPhone", typeof(System.String));
            dtClinicsData.Columns.Add("clientContactPhoneExt", typeof(System.String));

            return dtClinicsData;
        }

        /// <summary>
        ///  Read Data from selected excel file into DataTable
        /// </summary>
        /// <param name="file_name">Excel File Path</param>
        /// <returns></returns>
        public DataTable readExcelFile(string file_name, out string error_message)
        {
            StringBuilder failure_message = new StringBuilder();
            // string column_name = string.Empty;
            error_message = string.Empty;
            string clinic_state = string.Empty;

            // Use SpreadSheetDocument class of Open XML SDK to open excel file
            using (SpreadsheetDocument spread_sheet_document = SpreadsheetDocument.Open(file_name, false))
            {
                List<XElement> column_definition_elements = this.getColumnsDefinition.Elements().ToList();

                // Get all valid sheets in spread sheet document 
                List<KeyValuePair<string[], Row[]>> valid_sheet_clinics_list = getValidSheetClinics(spread_sheet_document);
                if (valid_sheet_clinics_list.Count > 0)
                {
                    bool is_having_admin_override_access = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "AccessOverlapDBForCorporateClinics");
                    string[] column_names = null;
                    int column_index = 0;
                    int row_count = 1;
                    string column_value;
                    XElement input_element;
                    CellFormats cellFormats = spread_sheet_document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats;
                    foreach (KeyValuePair<string[], Row[]> sheet_clinics in valid_sheet_clinics_list)
                    {
                        column_names = sheet_clinics.Key;
                        int vaccine_count = 0;
                        Int64 estimated_volume = 0;
                        foreach (Row row in sheet_clinics.Value.Skip(1))
                        {
                            DataRow temp_row = this.dtClinicsData.NewRow();
                            failure_message.Clear();
                            column_index = 0;

                            Cell[] row_cells = row.Descendants<Cell>().ToArray();
                            for (int cell_order_index = 0; cell_order_index < row_cells.Count(); cell_order_index++)
                            {
                                Cell cell = row_cells[cell_order_index];
                                if (column_names.Count() > column_index)
                                {
                                    string header_name = column_names[column_index];
                                    if (columnHeaders.Contains(header_name))
                                    {
                                        int cell_column_index = getColumnIndex(getColumnName(cell.CellReference));
                                        if (cell_column_index > column_index)
                                        {
                                            column_value = string.Empty;
                                            cell_order_index = cell_order_index - 1;
                                        }
                                        else
                                        {
                                            column_value = getValueOfCell(spread_sheet_document, cell);
                                        }

                                        column_value = column_value.removeJunkCharacters();
                                        temp_row[header_name] = column_value;

                                        input_element = column_definition_elements.Where(element => element.Attribute("name").Value == header_name).Select(element => element).FirstOrDefault();
                                        if (input_element.Attribute("isRequired").Value == "true" && column_value.Trim().Length == 0)
                                        {
                                            failure_message.Append("[" + header_name + "] is required;");
                                        }
                                        else if (!string.IsNullOrEmpty(column_value))
                                        {
                                            switch (header_name)
                                            {
                                                case "Clinic Start Time":
                                                case "Clinic End Time":
                                                    double time_value = 0;
                                                    DateTime datetime_value = DateTime.Now;
                                                    string time_in_upper_case = (column_value.Replace('.', ':').ToUpper()).Replace("AM", " AM").Replace("PM", " PM").Replace("  ", " ").Trim();
                                                    if ((DateTime.TryParse(time_in_upper_case, out datetime_value)) && (time_in_upper_case.Contains("AM") || time_in_upper_case.Contains("PM")))
                                                    {
                                                        column_value = DateTime.Parse(time_in_upper_case).ToString("hh:mm:ss tt");
                                                    }
                                                    else if (Double.TryParse(column_value, out time_value) && (cell.DataType == null) && (cell.StyleIndex != null))
                                                    {
                                                        CellFormat cell_format = (CellFormat)cellFormats.ElementAt((int)cell.StyleIndex.Value);
                                                        if (cell_format.NumberFormatId != 0)
                                                        {
                                                            column_value = DateTime.FromOADate(time_value).ToString("hh:mm:ss tt");
                                                        }
                                                        else
                                                        {
                                                            failure_message.Append("Invalid [" + header_name + "];");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    }
                                                    break;
                                                case "Clinic Date":
                                                    double date_value = 0;
                                                    if (Double.TryParse(column_value, out date_value) && (cell.DataType == null) && (cell.StyleIndex != null))
                                                    {
                                                        CellFormat cell_format = (CellFormat)cellFormats.ElementAt((int)cell.StyleIndex.Value);
                                                        if (cell_format.NumberFormatId != 0)
                                                        {
                                                            column_value = DateTime.FromOADate(date_value).ToString("MM/dd/yyyy");
                                                            this.validateClinicDate(failure_message, column_value, header_name, clinic_state);
                                                        }
                                                        else
                                                        {
                                                            failure_message.Append("Invalid [" + header_name + "];");
                                                        }
                                                    }
                                                    else if (DateTime.TryParse(column_value, out datetime_value))
                                                    {
                                                        column_value = datetime_value.ToString("MM/dd/yyyy");
                                                        this.validateClinicDate(failure_message, column_value, header_name, clinic_state);
                                                    }
                                                    else
                                                    {
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    }
                                                    break;
                                                case "Local Client Contact Phone #":
                                                    column_value = Regex.Replace(column_value, "[^0-9]", "");
                                                    if (column_value.Length > 0)
                                                    {
                                                        if (column_value.Substring(0, 1) == "1")
                                                            column_value = column_value.Substring(1);

                                                        if (column_value.Length >= 10 && column_value.Length <= 15)
                                                        {
                                                            if (!column_value.Substring(0, 10).validatePhone())
                                                                failure_message.Append("Invalid [" + header_name + "];");
                                                            else
                                                            {
                                                                temp_row["clientContactPhone"] = column_value.Substring(0, 10);
                                                                if (!string.IsNullOrEmpty(column_value.Substring(10)))
                                                                    temp_row["clientContactPhoneExt"] = column_value.Substring(10);
                                                            }
                                                        }
                                                        else if (column_value.Length > 15)
                                                            failure_message.Append("Data for [" + header_name + "] exceeds max allowed length of " + input_element.Attribute("dataLength").Value + ";");
                                                        else if (column_value.Length < 10)
                                                            failure_message.Append("Invalid [" + header_name + "];");
                                                    }
                                                    else
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    break;
                                                case "Zip":
                                                    column_value = column_value.PadLeft(5, '0');
                                                    break;
                                                case "State":
                                                    //Allowing specific users to upload MO and DC state clinics
                                                    string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate", column_value);
                                                    string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate", column_value);
                                                    if (!is_having_admin_override_access && (column_value == "MO" || column_value == "DC") && Convert.ToDateTime(restriction_start_date) <= DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
                                                    {
                                                        if (column_value == "MO")
                                                            failure_message.Append("MO clinics may not be uploaded;");
                                                        else
                                                            failure_message.Append("DC clinics may not be uploaded;");
                                                    }
                                                    else
                                                        clinic_state = column_value;

                                                    if (!clinicStates.Contains(column_value.ToUpper()))
                                                        failure_message.Append("Invalid clinic state;");
                                                    break;
                                                case "Flu Vaccines Covered":
                                                    vaccine_count = column_value.Replace(", ", ",").Trim().Split(',').Count();

                                                    string[] vaccines = column_value.Contains(',') ?
                                                                                    column_value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(vaccine => vaccine.Trim()).ToArray() : null;

                                                    if ((vaccines != null) && (vaccines.GroupBy(vaccine => vaccine).Where(duplicate_vaccines => duplicate_vaccines.Count() > 1).Count() > 0))
                                                    {
                                                        failure_message.Append("[Flu Vaccines Covered] contains duplicate vaccines;");
                                                    }
                                                    break;
                                                case "Room":
                                                    string[] clinic_rooms = column_value.Split(',');

                                                    if (clinic_rooms.Count() > 0)
                                                    {
                                                        column_value = string.Empty;

                                                        foreach (string room in clinic_rooms)
                                                        {
                                                            if (!string.IsNullOrEmpty(room.Trim()) && room.removeJunkCharacters().Length != 0)
                                                                column_value += ", " + room.Trim();
                                                        }
                                                    }
                                                    column_value = column_value.TrimStart(',').TrimEnd(',').Trim();
                                                    break;
                                                case "Estimated Volume Per Product":
                                                    column_value = column_value.Replace(", ", ",");
                                                    Int64.TryParse(column_value.Replace(",", ""), out estimated_volume);

                                                    if (estimated_volume > 0)
                                                    {
                                                        if (vaccine_count < column_value.Trim().Split(',').Count())
                                                            failure_message.Append("[Flu Vaccines Covered] count is less than [Estimated Volume Per Product] count;");
                                                        else if (vaccine_count > column_value.Trim().Split(',').Count())
                                                            failure_message.Append("[Flu Vaccines Covered] count is more than [Estimated Volume Per Product] count;");
                                                        //Commenting this logic as we are not restricting this for 2017-18 season
                                                        //else
                                                        //{
                                                        //    string[] vaccine_estimated_shots = column_value.Trim().Split(',');
                                                        //    int total_estimated_shots = 0;
                                                        //    foreach (string estimated_shots in vaccine_estimated_shots)
                                                        //    {
                                                        //        total_estimated_shots += Convert.ToInt32(estimated_shots);
                                                        //    }                                                           
                                                        //    if (!is_having_admin_override_access && total_estimated_shots < 75)
                                                        //       failure_message.Append("Doesn’t meet 75 shot minimum");
                                                        //}
                                                    }
                                                    else
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    break;
                                                case "Total Employee Count":
                                                    column_value = column_value.Replace("N/A", "");
                                                    break;
                                                case "Employee Copay":
                                                    if (column_value.Substring(column_value.IndexOf('.') + 1) == "0000")
                                                        column_value = column_value.Replace(column_value.Substring(column_value.IndexOf('.') + 1), "00");

                                                    break;
                                            }

                                            if (header_name != "Local Client Contact Phone #")
                                            {
                                                temp_row[header_name] = column_value;
                                                //checks field data length
                                                if (!string.IsNullOrEmpty(input_element.Attribute("dataLength").Value) && (column_value.Length > Convert.ToInt32(input_element.Attribute("dataLength").Value)))
                                                    failure_message.Append("Data for [" + header_name + "] exceeds max allowed length of " + input_element.Attribute("dataLength").Value + ";");
                                            }

                                            switch (input_element.Attribute("type").Value)
                                            {
                                                case "Integer":
                                                    if (!column_value.validateIsNumeric(false))
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    break;
                                                case "String":
                                                    if (!column_value.validateJunkCharacters())
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    break;
                                                case "Email":
                                                    if (!string.IsNullOrEmpty(column_value) && !column_value.validateEmail())
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    break;
                                                case "ZipCode":
                                                    if (!column_value.validateZipCode())
                                                        failure_message.Append("Invalid [" + header_name + "];");
                                                    break;
                                            }
                                        }
                                        else if (header_name == "Room" && column_value.Replace(",", "").Length == 0)
                                        {
                                            //Check clinic room field value
                                            temp_row[header_name] = column_value;
                                        }
                                    }
                                    ++column_index;
                                }
                            }
                            if (!string.IsNullOrEmpty(failure_message.ToString()))
                            {
                                temp_row["Remarks"] = failure_message.ToString().TrimEnd(';');
                                temp_row["isValidRecord"] = 0;
                            }
                            else
                                temp_row["isValidRecord"] = 1;

                            temp_row["origPk"] = row_count;
                            row_count++;

                            this.dtClinicsData.Rows.Add(temp_row);
                        }
                    }
                }
            }

            error_message = this.errorMessage;

            return this.dtClinicsData;
        }

        /// <summary>
        /// Validates Clinic Date
        /// </summary>
        /// <param name="failure_message"></param>
        /// <param name="column_value"></param>
        /// <param name="header_name"></param>
        /// <param name="clinic_state"></param>
        private void validateClinicDate(StringBuilder failure_message, string column_value, string header_name, string clinic_state)
        {
            if (!column_value.validateIsDate())
            {
                failure_message.Append("Invalid [" + header_name + "];");
            }
            else if (column_value.validateIsDate())
            {
                if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "admin" &&
                  !ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "AccessOverlapDBForCorporateClinics"))
                {
                    if ((clinic_state == "MO" || clinic_state == "DC") && DateTime.Parse(column_value) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                        failure_message.Append("[" + header_name + "]; cannot be before 1st September.");
                }
                //checking clinic date in past or not, except the authorized users
                if (DateTime.Parse(column_value) < DateTime.Now && !ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "AccessOverlapDBForCorporateClinics"))
                {
                    failure_message.Append("[" + header_name + "]; cannot be in past");
                }
            }
        }

        /// <summary>
        /// Get all column headers in a valid sheet 
        /// </summary>
        /// <param name="spread_sheet_document"></param>
        /// <param name="header_row"></param>
        /// <returns></returns>
        private string[] getValidatedSheetColumns(SpreadsheetDocument spread_sheet_document, Row header_row)
        {
            int invalid_column_count = 0;
            string error_message = string.Empty;
            List<string> header_columns_list = new List<string>();
            string[] header_columns = getColumnHeaders(spread_sheet_document, header_row);
            foreach (string data_column in columnHeaders)
            {
                if (!header_columns.Contains(data_column))
                {
                    error_message += " The Column " + data_column + " is missing;";
                    invalid_column_count++;
                }
            }

            if (invalid_column_count == 0)
            {
                header_columns_list.AddRange(header_columns);
            }
            else
            {
                error_message = (invalid_column_count > 10) ? "Please upload a valid file." : error_message;
                this.errorMessage = string.IsNullOrEmpty(this.errorMessage) ? error_message : this.errorMessage;
            }

            return header_columns_list.ToArray();
        }

        /// <summary>
        /// Get Column Headers in sheet
        /// </summary>
        /// <param name="spread_sheet_document"></param>
        /// <param name="header_row"></param>
        /// <returns></returns>
        private static string[] getColumnHeaders(SpreadsheetDocument spread_sheet_document, Row header_row)
        {
            List<string> column_headers = new List<string>();
            List<Cell> header_cells = header_row.Elements<Cell>().ToList();
            SharedStringTablePart shared_string = spread_sheet_document.WorkbookPart.SharedStringTablePart;

            foreach (Cell header_cell in header_cells)
            {
                if (header_cell.DataType != null && header_cell.DataType.Value == DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString)
                {
                    column_headers.Add(shared_string.SharedStringTable.ChildElements[int.Parse(header_cell.CellValue.Text)].InnerText.Trim());
                }
                else
                {
                    column_headers.Add((header_cell.CellValue == null) ? string.Empty : header_cell.CellValue.Text.Trim());
                }
            }

            return column_headers.ToArray();
        }

        /// <summary>
        ///  Get Value of Cell 
        /// </summary>
        /// <param name="spread_sheet_document">SpreadSheet Document Object</param>
        /// <param name="cell">Cell Object</param>
        /// <returns>The Value in Cell</returns>
        private static string getValueOfCell(SpreadsheetDocument spread_sheet_document, Cell cell)
        {
            // Get value in Cell
            SharedStringTablePart shared_string = spread_sheet_document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                return string.Empty;
            }

            string cellValue = cell.CellValue.InnerText;

            // The condition that the Cell DataType is SharedString
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return shared_string.SharedStringTable.ChildElements[int.Parse(cellValue)].InnerText.Trim();
            }
            else
            {
                return cellValue;
            }
        }

        /// <summary>
        /// Get Valid Sheet - column names and records
        /// </summary>
        /// <param name="spread_sheet_document"></param>
        /// <returns></returns>
        private List<KeyValuePair<string[], Row[]>> getValidSheetClinics(SpreadsheetDocument spread_sheet_document)
        {
            List<KeyValuePair<string[], Row[]>> valid_sheet_clinics = new List<KeyValuePair<string[], Row[]>>();

            // Get all sheets in spread sheet document 
            List<Sheet> sheet_collection = spread_sheet_document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();

            sheet_collection = sheet_collection.OfType<Sheet>().Where(sheet => (!sheet.Name.ToString().Contains("_FilterDatabase") ||
                                                                                !sheet.Name.ToString().Contains("Sheet") ||
                                                                                !sheet.Name.ToString().Contains("$")
                                                                               )
                                                                     )
                                                              .Select(sheet => sheet)
                                                              .ToList();
            if (sheet_collection.Count() > 0)
            {
                foreach (Sheet sheet in sheet_collection)
                {
                    // Get sheet1 Part of Spread Sheet Document
                    WorksheetPart worksheet_part = (WorksheetPart)spread_sheet_document.WorkbookPart.GetPartById(sheet.Id.Value);

                    // Get Data in Excel file
                    SheetData sheet_data = worksheet_part.Worksheet.Elements<SheetData>().First();
                    IEnumerable<Row> row_collection = sheet_data.Descendants<Row>().Where(row => !row.Descendants<Cell>().All(cell => (cell.CellValue == null)));

                    if (row_collection.Count() > 0)
                    {
                        string[] column_names = getValidatedSheetColumns(spread_sheet_document, row_collection.ElementAt(0));
                        if (column_names.Count() > 0)
                        {
                            valid_sheet_clinics.Add(new KeyValuePair<string[], Row[]>(column_names, row_collection.ToArray()));
                        }
                    }
                }
            }

            return valid_sheet_clinics;
        }

        /// <summary>
        /// Gets spreadsheet columns
        /// </summary>
        private XElement getColumnsDefinition
        {
            get
            {
                XDocument corporate_upload_fields = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("~/xmlDoc/corporateClinicDetailsFields.xml"));
                XElement clinic_details_element = corporate_upload_fields.Element("clinicDetailsFields");
                XElement upload_corporate_element = clinic_details_element.Element("uploadClinicInfo");
                return upload_corporate_element;
            }
        }

        private XElement getStates
        {
            get
            {
                XDocument clinic_states_document = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("~/xmlDoc/states.xml"));
                XElement clinic_states_element = clinic_states_document.Element("states");
                return clinic_states_element;
            }
        }

        /// <summary>
        /// Get Column Name From given cell name
        /// </summary>
        /// <param name="cell_reference">Cell Name(For example,A1)</param>
        /// <returns>Column Name(For example, A)</returns>
        private string getColumnName(string cell_reference)
        {
            // Create a regular expression to match the column name of cell
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cell_reference);
            return match.Value;
        }

        /// <summary>
        /// Get Index of Column from given column name
        /// </summary>
        /// <param name="column_name">Column Name(For Example,A or AA)</param>
        /// <returns>Column Index</returns>
        private int getColumnIndex(string column_name)
        {
            int column_index = 0;
            int factor = 1;

            // From right to left
            for (int position = column_name.Length - 1; position >= 0; position--)
            {
                // For letters
                if (Char.IsLetter(column_name[position]))
                {
                    column_index += factor * ((column_name[position] - 'A') + 1) - 1;
                    factor *= 26;
                }
            }

            return column_index;
        }
    }
}
