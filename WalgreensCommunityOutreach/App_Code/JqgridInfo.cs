﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TdWalgreens
{
    /// <summary>
    /// Summary description for JqgridInfo
    /// </summary>
    public class JqgridInfo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string outreachStatusId { get; set; }
    }
}