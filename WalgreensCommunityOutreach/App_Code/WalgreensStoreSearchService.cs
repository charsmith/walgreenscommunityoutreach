﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using TdWalgreens;

/// <summary>
/// Summary description for WalgreensStoreSearchService
/// </summary>
[WebService(Namespace = "https://wagoutreach.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WalgreensStoreSearchService : System.Web.Services.WebService {

    public WalgreensStoreSearchService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    /// <summary>
    /// This function will be used to get store data
    /// </summary>
    /// <param name="type"></param>
    /// <param name="parameter"></param>
    /// <returns></returns>
    ///
    public void GetStoreData(string type, string parameter, string userId)
    {
        HttpResponse response = HttpContext.Current.Response;
        string sched_conn_string = ConfigurationManager.ConnectionStrings["printOnlineConnString"].ConnectionString;
        SqlConnection conn = new SqlConnection(sched_conn_string);
        conn.Open();
        SqlCommand command = new SqlCommand("uspGetUserStoresSearch", conn);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.Clear();
        if (type == "storeId")
        {
            command.Parameters.AddWithValue("@userId", userId);
            command.Parameters.AddWithValue("@storeId", parameter);
        }
        SqlDataReader dr = command.ExecuteReader();

        int count = 0;
        List<jsonReturnValue> return_value = new List<jsonReturnValue>();
        while (dr.Read())
        {
            jsonReturnValue return_item = new jsonReturnValue();
            switch (type)
            {
                case "storeId":
                    return_item.id = dr["storeId"].ToString().Trim();
                    return_item.name = dr["address"].ToString().Trim();
                    break;
            }

            return_value.Add(return_item);
            count++;
        }
        conn.Close();
        StringBuilder sb = new StringBuilder();
        if (return_value != null && return_value.Count > 0)
        {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            jss.Serialize(return_value, sb);
            response.ContentType = "application/json";
            response.Write(sb.ToString());
        }
    }
}
