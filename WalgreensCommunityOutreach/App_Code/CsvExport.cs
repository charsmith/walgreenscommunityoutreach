using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;

namespace TdWalgreens
{
	public class ExportToCSV
	{
		List<string> fields = new List<string>();
		List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
		Dictionary<string, object> currentRow { get { return rows[rows.Count - 1]; } }

		/// <summary>
        /// Set a value on this column
		/// </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		public object this[string field]
		{
			set
			{
				// Keep track of the field names, because the dictionary loses the ordering
				if (!fields.Contains(field)) fields.Add(field);
				currentRow[field] = value;
			}
		}

		/// <summary>
        /// Call this before setting any fields on a row
		/// </summary>
		public void AddRow()
		{
			rows.Add(new Dictionary<string, object>());
		}

		/// <summary>
        /// Add a list of typed objects, maps object properties to CsvFields
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		public void AddRows<T>(IEnumerable<T> list)
		{
			if (list.Any())
			{
				foreach (var obj in list)
				{
					AddRow();
					var values = obj.GetType().GetProperties();
					foreach (var value in values)
					{
						this[value.Name] = value.GetValue(obj, null);
					}
				}
			}
		}
        /// <summary>
        /// Making value CSV friendly, if contains commas
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        string MakeValueCsvFriendly(object value, string field)
		{
			if (value == null) return "";
			if (value is INullable && ((INullable)value).IsNull) return "";
			if (value is DateTime)
			{
				if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
                    return ((DateTime)value).ToString("MM/dd/yyyy");
                return ((DateTime)value).ToString("MM/dd/yyyy HH:mm");
			}
            if ((field.ToLower() == "clinic date" || field.ToLower() == "clinic entry date" || field.ToLower() == "clinic confirm date") && (value != null && value.ToString().Length > 0))
                return Convert.ToDateTime(value).ToString("MM/dd/yyyy");
            if (field.ToLower() == "clinic mo/yr" && (value != null && value.ToString().Length > 0))
                return  " " + Convert.ToDateTime(value).ToString("MM/yyyy").ToString();
            if ((field.ToLower() == "scheduled on" || field.ToLower() == "last contact date") && (value != null && value.ToString().Length > 0))
                return " " + Convert.ToDateTime(value).ToString("MM/dd/yyyy H:mm tt").ToString();
            

			string output = value.ToString();
			if (output.Contains(",") || output.Contains("\"") || output.Contains("\n") || output.Contains("\r"))
				output = '"' + output.Replace("\"", "\"\"") + '"';
				
			if (output.Length > 30000) //cropping value for excel
			{
				if (output.EndsWith("\""))
					output = output.Substring(0, 30000) + "\"";
				else
					output = output.Substring(0, 30000);
			}

			return output.Length <= 32767 ? output : output.Substring(0, 32767);
		}

       /// <summary>
       /// Exports to a file
       /// </summary>
       /// <param name="path"></param>
        public void ExportToFile(string path)
        {
            File.WriteAllText(path, Export());
        }

		/// <summary>
        ///  Output all rows as a CSV returning a string
		/// </summary>
		/// <returns></returns>
		public string Export()
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendLine("sep=,");

			// header
			sb.Append(string.Join(",", fields.ToArray()));
			sb.AppendLine();

			// rows
			foreach (Dictionary<string, object> row in rows)
			{
				fields.Where(f => !row.ContainsKey(f)).ToList().ForEach(k =>
		                {
		                    row[k] = null;
		                });
                sb.Append(string.Join(",", fields.Select(field => MakeValueCsvFriendly(row[field], field)).ToArray()));
				sb.AppendLine();
			}

			return sb.ToString();
		}

		/// <summary>
        ///  Exports as raw UTF8 bytes
		/// </summary>
		/// <returns></returns>
		public byte[] ExportToBytes()
		{
			var data = Encoding.UTF8.GetBytes(Export());
			return Encoding.UTF8.GetPreamble().Concat(data).ToArray();
		}
        /// <summary>
        /// Return column Caption as per report
        /// </summary>
        /// <param name="column_name"></param>
        /// <returns></returns>
        public string GetColumnName(string column_name)
        {
            string caption_name = "";
            switch(column_name)
            {
                case "storeid":
                    caption_name = "Store ID";
                    break;
                case "marketNumber":
                    caption_name = "Region#";
                    break;
                case "areaNumber":
                    caption_name = "Area#";
                    break;
                case "districtNumber":
                    caption_name = "District #";
                    break;
                case "clinicName":
                    caption_name = "Client/Clinic Name";
                    break;
                case "clinicEntryDate":
                    caption_name = "Clinic Entry Date";
                    break;
                case "clinicConfirmedDate":
                    caption_name = "Clinic Confirm Date";
                    break;
                case "estimatedVolume":
                    caption_name = "Est Vol (Doses)";
                    break;
                case "vaccinesType":
                    caption_name = "Vaccine Type Needed";
                    break;
                case "BussinessCorporateClient":
                    caption_name = "Business/Corporate Client";
                    break;
                case "BussinessCorporateClientLocation":
                    caption_name = "Business/Corporate Client Location";
                    break;
                case "ClientLocationState":
                    caption_name = "Client Location State";
                    break;
                case "ClinicType":
                    caption_name = "Clinic Type";
                    break;
                case "OutreachType":
                    caption_name = "Outreach Type";
                    break;
                 case "Industry":
                    caption_name = "Industry";
                    break;
                case "Comments":
                    caption_name = "Comments";
                    break;
                case "LocalContractLocation":
                    caption_name = "Local Contract Location";
                    break;
                case "ClinicLocation":
                    caption_name = "Clinic Location";
                    break;
                case "ClinicState":
                    caption_name = "Clinic Location State";
                    break;
                case "LocationContactName":
                    caption_name = "Location Contact Name";
                    break;
                case "LocationContactPhone":
                    caption_name = "Location Contact Phone";
                    break;
                case "LocationContactEmail":
                    caption_name = "Location Contact Email";
                    break;
                case "ClinicDate":
                case "clinicDate":
                    caption_name = "Clinic Date";
                    break;
                case "ClinicMoYr":
                    caption_name = "Clinic Mo/Yr";
                    break;
                case "StartTime":
                case "startTime":
                    caption_name = "Start Time";
                    break;
                case "EndTime":
                case "endTime":
                    caption_name = "End Time";
                    break;
                case "EstVol":
                    caption_name = "Est Vol";
                    break;
                case "naClinicVouchersDist":
                    caption_name = "Vouchers Distributed";
                    break;
                case "ExpirationDate":
                    caption_name = "Expiration Date";
                    break;
                case "actualCorporateEmploymentSize":
                    caption_name = "Actual Corporate Employment Size";
                    break;
                case "WorksiteNeeded":
                    caption_name = "Worksite Needed";
                    break;
                case "VaccinesCoveredGlobal":
                    caption_name = "Vaccines Covered";
                    break;
                case "ClinicCoverageType":
                    caption_name = "Clinic Coverage Type";
                    break;
                case "ClinicCopay":
                    caption_name = "Clinic Copay";
                    break;
                case "PaymentTypes":
                    caption_name = "Payment Types";
                    break;
                case "InvoiceName":
                    caption_name = "Invoice Name";
                    break;
                case "InvoiceAddress":
                    caption_name = "Invoice Address";
                    break;
                case "InvoicePhone":
                    caption_name = "Invoice Phone";
                    break;
                case "InvoiceEmail":
                    caption_name = "Invoice Email";
                    break;
                case "EmployerTaxExempt":
                    caption_name = "Employer Tax Exempt";
                    break;
                case "VoucherNeeded":
                    caption_name = "Voucher Needed";
                    break;
                case "AdditionalComments":
                    caption_name = "Additional Comments";
                    break;
                case "SpecialBillingComments":
                    caption_name = "Special Billing Comments";
                    break;
                case "PlanID":
                    caption_name = "Plan ID";
                    break;
                case "GroupID":
                    caption_name = "Group ID";
                    break;
                case "IDRecipient":
                    caption_name = "ID Recipient";
                    break;
                case "TotalImmunizationsAdministered":
                    caption_name = "Total Immunizations Administered";
                    break;
                case "ScheduledOn":
                    caption_name = "Scheduled On";
                    break;
                case "LastContactDate":
                    caption_name = "Last Contact Date";
                    break;
                 case "LastNotes":
                    caption_name = "Last Notes";
                    break;
                 case "LastOutreachStatus":
                    caption_name = "Last Outreach Status";
                    break;
                case "storeId":
                    caption_name = "Store Id";
                    break;
                case "districtId":
                    caption_name = "District";
                    break;
                case "areaId":
                    caption_name = "Area";
                    break;
                case "regionId":
                    caption_name = "Region";
                    break;
                case "businessName":
                    caption_name = "Client Name";
                    break;
                case "empSize":
                    caption_name = "Emp";
                    break;
                case "contactName":
                    caption_name = "Contact Name";
                    break;
                case "phone":
                    caption_name = "Phone";
                    break;
            }
            return caption_name;
        }

	}
}
