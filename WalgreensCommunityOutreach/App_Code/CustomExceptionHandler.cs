﻿using System;
using System.Web;
using TdApplicationLib;
using NLog;
using System.Diagnostics;
namespace TdWalgreens
{
    /// <summary>
    /// Summary description for CustomException
    /// </summary>
    public class CustomExceptionHandler
    {
        /// <summary>
        /// Logs error in logs database and returns custom error message
        /// </summary>
        /// <param name="e"></param>
        /// <param name="log_string"></param>
        /// <returns></returns>
        public string getErrorMessage(Exception e, string log_string)
        {
            DBOperations db_operation = new DBOperations();
            string return_error_message = "";

            return_error_message = (string)(System.Web.HttpContext.GetGlobalResourceObject("systemExceptionErrors", e.GetType().Name));
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            HttpContext.Current.ClearError();
            switch (return_error_message)
            {
                case "Not Record":
                    System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
                    break;
                default:
                    Logger logger = LogManager.GetCurrentClassLogger();
                    StackTrace st_trace = new StackTrace(e, true);
                    StackFrame st_frame = st_trace.GetFrame(0);
                    int line_number = st_frame.GetFileLineNumber();
                    if(!string.IsNullOrEmpty(log_string))
                        logger.Error("User Log: " + log_string + "|Location: " + System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString() + "|Error message: " + e.Message + " Stack Trace: " + e.StackTrace + " Line Number:" + line_number.ToString());
                    
                    db_operation.logError(-1, e.Message + " " + log_string + "; Location: " + System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString(), e.StackTrace);
                    break;
                }
            
            return return_error_message;
        }
    }
}


