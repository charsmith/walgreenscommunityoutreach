﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace TdWalgreens
{
    public class CorporateScheduler
    {
        public string existingClinicSchedulerXML { get; set; }
        public string clinicSchedulerXML { get; set; }
        public DataTable clientClinics { get; set; }
        public DataTable scheduledAppointments { get; set; }
    }
}