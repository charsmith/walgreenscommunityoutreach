﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ExtentionMethods
/// </summary>
namespace TdWalgreens
{
    public static class CommonExtensionsMethods
    {
        public static string getJSONObject(this object obj_data)
        {
            string json_obj = string.Empty;
            var json_serializer = new JavaScriptSerializer();
            json_serializer.MaxJsonLength = 2147483644;
            if (obj_data.GetType().Name == "DataTable")
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;

                DataTable dt = new DataTable();
                dt = (DataTable)obj_data;

                foreach (DataRow dt_row in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn dt_column in dt.Columns)
                    {
                        row.Add(dt_column.ColumnName, dt_row[dt_column]);
                    }
                    rows.Add(row);
                }

                json_obj = json_serializer.Serialize(rows);
            }

            return json_obj;
        }

        /// <summary>
        /// Gets JSON object for Immunizations Payment Methods
        /// </summary>
        /// <param name="obj_data"></param>
        /// <returns></returns>
        public static string getImmunizationPaymentMethodsJSONObject(this object obj_data)
        {
            string json_obj = string.Empty;
            var json_serializer = new JavaScriptSerializer();

            if (obj_data.GetType().Name == "DataTable")
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;

                DataTable dt = new DataTable();
                dt = (DataTable)obj_data;

                foreach (DataRow dt_row in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn dt_column in dt.Columns)
                    {
                        if ((dt_column.ColumnName == "immunizationId") || (dt_column.ColumnName == "paymentTypeId") || (dt_column.ColumnName == "paymentTypeName") || (dt_column.ColumnName == "paymentTypeSpanishName") || (dt_column.ColumnName == "isPaymentSelected"))
                            row.Add(dt_column.ColumnName, dt_row[dt_column]);
                    }
                    rows.Add(row);
                }

                json_obj = json_serializer.Serialize(rows);
            }

            return json_obj;
        }

        /// <summary>
        /// Confirms sending clinic details changes email to Clinical Contract
        /// </summary>
        /// <param name="updateHistoryLog"></param>
        /// <returns></returns>
        public static bool sendEmailToClinicalContract(string updateHistoryLog)
        {
            bool isSendBCC = false;
            XElement updated_historyLog = XElement.Parse(updateHistoryLog);
            XElement billing_info = updated_historyLog.Element("clinicBillingInfo");
            if ((billing_info != null))
            {
                XElement estimatedQuantity = billing_info.Elements("field")
                                            .Where(ele => ele.Attribute("displayName").Value == "Estimated Volume")
                                                    .Select(ele => ele)
                                                        .FirstOrDefault();
                if (estimatedQuantity != null)
                {
                    isSendBCC = true;
                }
            }

            return isSendBCC;
        }

        /// <summary>
        /// Binds states list to dropdown
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Control bindStates(this Control control)
        {
            DataSet ds_states = ApplicationSettings.getStates;
            DropDownList ddl_states = (DropDownList)control;
            if (ds_states.Tables.Count > 0)
            {
                ddl_states.DataSource = ds_states;
                ddl_states.DataValueField = ds_states.Tables[0].Columns["Value"].ToString();
                ddl_states.DataTextField = ds_states.Tables[0].Columns["Value"].ToString();
                ddl_states.DataBind();
                ddl_states.Items.Insert(0, new ListItem("-- Select --", ""));
            }
            return ddl_states;
        }

        /// <summary>
        /// Binds states list to dropdown excluding restricted states
        /// </summary>
        /// <param name="control"></param>
        /// <param name="include_state"></param>
        /// <param name="restricted_states"></param>
        /// <returns></returns>
        public static Control bindStatesWithRestriction(this Control control, string include_state, string restricted_states)
        {
            DataSet ds_states = ApplicationSettings.getStates;
            DropDownList ddl_states = (DropDownList)control;
            if (ds_states.Tables.Count > 0)
            {
                ddl_states.DataSource = ds_states;
                ddl_states.DataValueField = ds_states.Tables[0].Columns["Value"].ToString();
                ddl_states.DataTextField = ds_states.Tables[0].Columns["Value"].ToString();
                ddl_states.DataBind();
                ddl_states.Items.Insert(0, new ListItem("-- Select --", ""));

                if (!string.IsNullOrEmpty(restricted_states))
                {
                    string[] states_list = restricted_states.Replace(" ", "").Split(',');

                    if (states_list.Count() > 0)
                    {
                        foreach (string state in states_list)
                        {
                            if (ddl_states.Items.FindByValue(state) != null && state != include_state)
                                ddl_states.Items.Remove(ddl_states.Items.FindByValue(state));
                        }
                    }
                }
            }
            return ddl_states;
        }

        /// <summary>
        /// Binds location type filters
        /// </summary>
        /// <param name="control"></param>
        /// <param name="user_role"></param>
        /// <returns></returns>
        public static Control bindDataFilterTypes(this Control control, string user_role)
        {
            DropDownList ddl_data_filters = (DropDownList)control;

            switch (user_role.ToLower())
            {
                case "healthcare supervisor":
                case "director – rx & retail ops":
                    ddl_data_filters.Items.Add(new ListItem("District"));
                    break;
                case "regional healthcare director":
                case "regional vice president":
                    ddl_data_filters.Items.Add(new ListItem("Area"));
                    ddl_data_filters.Items.Add(new ListItem("District"));
                    break;
                case "admin":
                    ddl_data_filters.Items.Add(new ListItem("Region"));
                    ddl_data_filters.Items.Add(new ListItem("Area"));
                    ddl_data_filters.Items.Add(new ListItem("District"));
                    break;
            }
            ddl_data_filters.Items.Add(new ListItem("Store"));
            ddl_data_filters.Items.Insert(0, new ListItem("Select", ""));
            ddl_data_filters.SelectedIndex = 0;

            return ddl_data_filters;
        }

        /// <summary>
        /// Validates phone number
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validatePhone(this string value)
        {
            string phone_value = value.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
            bool is_valid_phone = false;

            const string regex_pattern_1 = @"^[0-9]+$";

            if (Regex.IsMatch(phone_value, regex_pattern_1))
            {
                if (!string.IsNullOrEmpty(phone_value))
                {
                    if (Convert.ToInt64(phone_value) > 0)
                        is_valid_phone = (phone_value.Length == 10) ? true : false;
                    else
                        is_valid_phone = false;
                }
            }
            return is_valid_phone;
        }

        /// <summary>
        /// Validates zip code
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateZipCode(this string value)
        {
            bool is_valid_zip = true;
            const string regex_pattern_1 = @"^[0-9]+$";
            if (Regex.IsMatch(value, regex_pattern_1))
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (Convert.ToInt64(value) <= 0 || value.Length != 5)
                        is_valid_zip = false;
                }
            }
            else
                is_valid_zip = false;

            return is_valid_zip;
        }
        /// <summary>
        /// Validates address and verify if PO Box is present
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateAddress(this string value)
        {
            bool is_valid_address = true;
            string validate = string.Empty;
            //const string regex_pattern_1 = @"\\b[p]*(ost)*\\.*\\s*[o|0]*(ffice)*\\.*\\s*b[o|0]x\\b";
            //Regex.Replace(value, @"[^0-9a-zA-Z]+", ",");
            validate = Regex.Replace(value, "[ ().-]+", "");
            if (!string.IsNullOrEmpty(value))
            {
                value = validate.Replace(" ", string.Empty);
                if (value.ToLower().Contains("pobox"))
                {
                    is_valid_address = false;
                }
            }

            return is_valid_address;
        }

        /// <summary>
        /// Validates estimated shots
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateEstimatedShots(this string value)
        {
            bool is_valid = true;
            const string regex_pattern = @"^[0-9]+$";

            if (!Regex.IsMatch(value, regex_pattern))
            {
                is_valid = false;
            }

            return is_valid;
        }

        /// <summary>
        /// Validates email address format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateEmail(this string value)
        {
            bool is_valid = true;
            const string regex_pattern = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";

            if (Regex.IsMatch(value, regex_pattern))
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (value.Length == 0)
                        is_valid = false;
                }
            }
            else
                is_valid = false;

            return is_valid;
        }

        /// <summary>
        /// Validates multiple email addresses
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateMultipleEmails(this string value)
        {
            bool is_valid = true;
            const string regex_pattern = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
            string[] _emails = value.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string email in _emails)
            {
                if (Regex.IsMatch(email, regex_pattern))
                {
                    if (!string.IsNullOrEmpty(email))
                    {
                        if (email.Length == 0)
                            is_valid = false;
                    }
                }
                else
                    is_valid = false;
            }

            return is_valid;
        }

        /// <summary>
        /// Validates junk values
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateJunkCharacters(this string value)
        {
            bool is_valid = true;

            if (!string.IsNullOrEmpty(value))
            {
                if (value.IndexOfAny(new char[] { '<', '>' }) > -1)
                {
                    is_valid = false;
                }
            }

            return is_valid;
        }

        /// <summary>
        /// Validate if value is numeric
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateIsDate(this string value)
        {
            bool is_valid = true;
            const string regex_pattern = @"(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)";

            if (!string.IsNullOrEmpty(value))
            {
                if (!Regex.IsMatch(value, regex_pattern))
                {
                    is_valid = false;
                }
            }

            return is_valid;
        }

        /// <summary>
        /// Validate if value is numeric
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateIsNumeric(this string value, bool isZeroCheck = true)
        {
            bool is_valid = true;
            const string regex_pattern = @"^[0-9]+$";

            if (!string.IsNullOrEmpty(value))
            {
                if (Regex.IsMatch(value, regex_pattern))
                {
                    if (isZeroCheck && Convert.ToInt64(value) == 0)
                        is_valid = false;
                }
                else
                    is_valid = false;
            }

            return is_valid;
        }

        /// <summary>
        /// Validates decimal value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validateDecimal(this string value)
        {
            bool is_valid = true;
            decimal totHours;
            if (!string.IsNullOrEmpty(value))
            {
                is_valid = decimal.TryParse(value, out totHours);
            }
            return is_valid;
        }

        /// <summary>
        /// Removes junk characters
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string removeJunkCharacters(this string value)
        {
            string junk_value = string.Empty;
            junk_value = value.Replace("\"", "");
            junk_value = value.Replace("''''''''", "''");
            junk_value = value.TrimEnd(new char[] { '\r', '\n' });
            junk_value = value.Replace("\r", "").Trim();
            junk_value = value.Replace("\n", "").Trim();
            //value = value.Replace("'", "''");
            junk_value = value.TrimEnd(' ');
            junk_value = value.TrimStart(' ');

            if (Regex.Replace(junk_value.Replace(" ", ""), "[^a-zA-Z0-9]+", "", RegexOptions.Compiled).Length == 0)
                junk_value = string.Empty;

            return junk_value;
        }

        /// <summary>
        /// Gets formatted store address
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string getStoreAddress(this string value)
        {
            value = value.Substring(value.IndexOf("-") + 1).TrimStart();
            value = value.Substring(0, value.IndexOf(',')) + "<br/>" + value.Substring(value.IndexOf(',') + 1);

            return value;
        }

        /// <summary>
        /// This function is used to validate input controls
        /// </summary>
        /// <param name="control"></param>
        /// <param name="control_type"></param>
        /// <param name="type"></param>
        /// <param name="allow_empty"></param>
        /// <param name="require_message"></param>
        /// <param name="invalid_message"></param>
        /// <returns></returns>
        public static bool validateControls(this Control control, string control_type, string type, bool allow_empty, string require_message, string invalid_message, Page page)
        {
            //is valid : 0 - Valid, 1 - Invalid Characters, (-1) - Required Failure
            int is_valid = 0;
            switch (control_type.ToLower())
            {
                case "textbox":
                    string input_text = ((TextBox)control).Text.Trim();
                    switch (type)
                    {
                        case "string":
                            if ((!string.IsNullOrEmpty(input_text)))
                            {
                                if (!input_text.validateJunkCharacters())
                                    is_valid = 1;
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "number": // Zero value should not accept as valid value
                            if ((!string.IsNullOrEmpty(input_text)))
                            {
                                if (!input_text.validateIsNumeric())
                                    is_valid = 1;
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "zero": // Zero value should accept as valid value
                            if ((!string.IsNullOrEmpty(input_text)))
                            {
                                if (!input_text.validateIsNumeric(false))
                                    is_valid = 1;
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "zip":
                            if (!string.IsNullOrEmpty(input_text))
                            {
                                if (!input_text.validateZipCode())
                                    is_valid = 1;
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "decimal":
                            if (!string.IsNullOrEmpty(input_text))
                            {
                                decimal totalHours = 0;
                                if (!(Decimal.TryParse(input_text, out totalHours)))
                                    is_valid = 1;
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "email":
                            if (!string.IsNullOrEmpty(input_text))
                            {
                                if (!input_text.validateEmail())
                                    is_valid = 1;
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "phone":
                            input_text = input_text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
                            if (!string.IsNullOrEmpty(input_text))
                            {
                                if (!input_text.validatePhone())
                                    is_valid = 1;
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "extension":
                            if ((!string.IsNullOrEmpty(input_text)))
                            {
                                if (!input_text.validateIsNumeric(false) || !(input_text.Length <= 5))
                                {
                                    is_valid = 1;
                                }
                            }
                            else if (!allow_empty)
                                is_valid = -1;

                            break;
                        case "address":
                            if (!string.IsNullOrEmpty(input_text))
                            {
                                if (!input_text.validateJunkCharacters())
                                    is_valid = 1;
                                if (!input_text.validateAddress()) //PO Box check
                                    is_valid = 2;
                            }
                            else if (!allow_empty)
                                is_valid = -1;
                            break;
                    }

                    if (is_valid != 0)
                    {
                        //((TextBox)control).Style.Add("border", "1px solid red");
                        ((TextBox)control).ToolTip = is_valid == -1 ? require_message : (is_valid == 1 ? invalid_message : HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString());
                        if (control.Visible)
                            page.ClientScript.RegisterStartupScript(page.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);", true);
                    }
                    else
                    {
                        if (control != null)
                        {
                            //((TextBox)control).Style.Add("border", "1px solid gray");
                            ((TextBox)control).ToolTip = string.Empty;
                            if (control.Visible)
                                page.ClientScript.RegisterStartupScript(page.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                        }
                    }

                    break;
                case "dropdownlist":
                    input_text = ((DropDownList)control).SelectedValue;
                    if (!allow_empty && string.IsNullOrEmpty(input_text))
                    {
                        is_valid = -1;
                    }
                    if (is_valid != 0)
                    {
                        HtmlControl cont = (HtmlControl)((DropDownList)control).Parent;
                        //cont.Style.Add("border", "1px solid red");
                        cont.Attributes.Add("title", (is_valid == -1 ? require_message : invalid_message));
                        if (control.Visible)
                            page.ClientScript.RegisterStartupScript(page.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);", true);
                    }
                    else
                    {
                        if (control != null)
                        {
                            HtmlControl cont = (HtmlControl)((DropDownList)control).Parent;
                            // cont.Style.Add("border", "1px solid gray");
                            cont.Attributes.Add("title", "");
                            if (control.Visible)
                                page.ClientScript.RegisterStartupScript(page.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                        }
                    }

                    break;
            }

            return is_valid != 0 ? false : true;
        }
    }
}