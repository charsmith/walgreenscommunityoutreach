﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StoreProfile
/// </summary>
public class StoreProfile
{
    public StoreProfile()
    {

        created = DateTime.Now;
        
    }

    public string type { get; set; }
    public string updatedValue { get; set; }
    //public string percentNew { get; set; }
    //public string percentDropped { get; set; }
    //public string percentChanged { get; set; }
    public int storeId { get; set; }
    public string address { get; set; }
    public string address2 { get; set; }
    public string city { get; set; }
    //public string county { get; set; }
    public string state { get; set; }
    public string zip { get; set; }
   // public string phone { get; set; }
   // public string fax { get; set; }
    public DateTime created { get; set; }
    //public string createdBy { get; set; }
   // public decimal latitude { get; set; }
   // public decimal longitude { get; set; }
   // public string storeName { get; set; }
   // public int areaCode { get; set; }
   // public bool isCurrent { get; set; }
    public int districtNumber { get; set; }
    public string districtName { get; set; }
    public int areaNumber { get; set; }
    public string areaName { get; set; }
    public int marketNumber { get; set; }
    public string marketname { get; set; }
    public int id { get; set; }
    public bool isCurrent { get; set; }
    public bool isStoreUpdated { get; set; }

}