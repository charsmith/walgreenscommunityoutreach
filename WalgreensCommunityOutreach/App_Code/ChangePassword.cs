﻿using System;
using System.Web.Security;

/// <summary>
/// Summary description for ChangePassword
/// </summary>
public class ChangePassword
{
    #region ---------- PUBLIC CONSTRUCTOR --------------------
    public ChangePassword()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion

    #region ---------- PUBLIC METHODS --------------------
    /// <summary>
    /// To set the prefix of the password.
    /// </summary>
    /// <returns></returns>
    public string getPasswordPrefix()
    {
        string[] array_of_string = new[] {"AliceBlue","AntiqueWhite","Aqua","Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown",
            "BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod","DarkGray",
        	"DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","Darkorange","DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen","DarkSlateBlue","DarkSlateGray",
        	"DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DodgerBlue","Feldspar","FireBrick","FloralWhite","ForestGreen","Fuchsia","Gainsboro",
        	"GhostWhite","Gold","GoldenRod","Gray","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki","Lavender","LavenderBlush",
        	"LawnGreen","LemonChiffon","LightBlue","LightCoral","LightCyan","LightGoldenRodYellow","LightGrey","LightGreen","LightPink","LightSalmon","LightSeaGreen",
        	"LightSkyBlue","LightSlateBlue","LightSlateGray","LightSteelBlue","LightYellow","Lime","LimeGreen","Linen","Magenta","Maroon","MediumAquaMarine",
        	"MediumBlue","MediumOrchid","MediumPurple","MediumSeaGreen","MediumSlateBlue","MediumSpringGreen","MediumTurquoise","MediumVioletRed","MidnightBlue",
        	"MintCream","MistyRose","Moccasin","NavajoWhite","Navy","OldLace","Olive","OliveDrab","Orange","OrangeRed","Orchid","PaleGoldenRod","PaleGreen",
        	"PaleTurquoise","PaleVioletRed","PapayaWhip","PeachPuff","Peru","Pink","Plum","PowderBlue","Purple","Red","RosyBrown","RoyalBlue","SaddleBrown",
        	"Salmon","SandyBrown","SeaGreen","SeaShell","Sienna","Silver","SkyBlue","SlateBlue","SlateGray","Snow","SpringGreen","SteelBlue","Tan","Teal","Thistle",
        	"Tomato","Turquoise","Violet","VioletRed","Wheat","White","WhiteSmoke","Yellow","YellowGreen"};
        Random random = new Random();
        string random_string_from_array = array_of_string[random.Next(0, array_of_string.Length)];
        return random_string_from_array;
    }
     /// <summary>
    /// To generate the password to specific username.
    /// </summary>
    /// <param name="user_name"></param>
    /// <param name="email"></param>
    /// <returns></returns>
    public string generatePassword(string user_name,out string email)
    {
        //Update EmailID of selected user
        string new_password = this.setPassword();

         MembershipUser selected_user = Membership.GetUser(user_name.Trim(), false);
        if (selected_user != null)
        {
        string old_password = selected_user.ResetPassword();
        email = selected_user.Email;
        selected_user.ChangePassword(old_password, new_password);
        Membership.UpdateUser(selected_user);
        }
        else
        {
            email = null;
            new_password = null;
        }

        return new_password;
    }
    #endregion

    #region ---------- PRIVATE METHODS --------------------
    /// <summary>
    /// To set the password.
    /// </summary>
    /// <returns></returns>
    private string setPassword()
    {
        Random rand = new Random();
        int number = rand.Next(10, 99);
        return getPasswordPrefix() + number + "$";
    }
   
    #endregion
}
