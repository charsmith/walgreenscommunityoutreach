﻿using System;
using System.Configuration;
using System.Web;
using TdApplicationLib;
using System.Data.SqlClient;
using TdLoggingLib.Logging;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Xml;
using System.Linq;
using tdEmailLib;
using System.Net.Configuration;
using System.Web.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Text;
using System.Drawing;
using System.Reflection;

namespace TdWalgreens
{
    /// <summary>
    /// Summary description for ApplicationSettings
    /// </summary>
    public class ApplicationSettings
    {
        public ApplicationSettings()
        {
        }

        #region ----------- PUBLIC METHODS------------

        /// <summary>
        /// Gets outreach campaign start date
        /// </summary>
        public static DateTime getOutreachStartDate
        {
            get
            {
                AppCommonSession common_app_session = (AppCommonSession)HttpContext.Current.Session["commonAppSession"];
                DateTime outreach_start_date = DateTime.Now;
                outreach_start_date = Convert.ToDateTime(ConfigurationManager.AppSettings["outreachStartDate" + common_app_session.SelectedStoreSession.OutreachProgramSelected].ToString().Trim().Length == 0 ? DateTime.Now.ToShortDateString() : ConfigurationManager.AppSettings["outreachStartDate" + common_app_session.SelectedStoreSession.OutreachProgramSelected].ToString());

                return outreach_start_date;
            }
        }

        /// <summary>
        /// Get outreach compaign end date with adding 12 months to start date
        /// </summary>
        public static DateTime getOutreachEndDate
        {
            get
            {
                return ApplicationSettings.getOutreachStartDate.Date.AddMonths(12).AddDays(-1);
            }
        }

        /// <summary>
        /// Gets outreach campaign report start year
        /// </summary>
        public int getOutreachReportStartYear
        {
            get
            {
                int report_year = DateTime.Now.Year;
                if (ConfigurationManager.AppSettings != null && ConfigurationManager.AppSettings["reportStartYear"] != null)
                    int.TryParse(ConfigurationManager.AppSettings["reportStartYear"].ToString(), out report_year);

                return report_year;
            }
        }

        /// <summary>
        /// Gets Senior Outreach fiscal year quarters
        /// </summary>
        public static int getCurrentQuarter
        {
            get
            {
                int quarter = 0;
                switch (DateTime.Now.Month)
                {
                    case 9:
                    case 10:
                    case 11:
                        quarter = 1;
                        break;
                    case 12:
                    case 1:
                    case 2:
                        quarter = 2;
                        break;
                    case 3:
                    case 4:
                    case 5:
                        quarter = 3;
                        break;
                    case 6:
                    case 7:
                    case 8:
                        quarter = 4;
                        break;
                }
                return quarter;
            }
        }

        /// <summary>
        /// Gets outreach campaign report filter options for Year
        /// </summary>
        public List<int> getOutreachReportYears
        {
            get
            {
                List<int> report_years = new List<int>();
                for (int int_year = getOutreachReportStartYear; int_year <= getOutreachStartDate.Year + 1; int_year++)
                {
                    report_years.Add(int_year);
                }

                return report_years;
            }
        }

        /// <summary>
        /// Gets report filter options for Quarters
        /// </summary>
        public Dictionary<int, string> getReportYearQuarters
        {
            get
            {
                Dictionary<int, string> report_year_quarters = new Dictionary<int, string>();
                report_year_quarters.Add(1, "Q1");
                report_year_quarters.Add(2, "Q2");
                report_year_quarters.Add(3, "Q3");
                report_year_quarters.Add(4, "Q4");
                report_year_quarters.Add(5, "Fiscal Year to Date");

                return report_year_quarters;
            }
        }

        /// <summary>
        /// Returns max voucher expiration date
        /// </summary>
        public static DateTime getVoucherMaxExpDate
        {
            get
            {
                DateTime voucher_exp_date = DateTime.Now;
                voucher_exp_date = Convert.ToDateTime(ConfigurationManager.AppSettings["voucherExpirationDateMax"].ToString().Trim().Length == 0 ? DateTime.Now.AddYears(1).ToShortDateString() : ConfigurationManager.AppSettings["voucherExpirationDateMax"].ToString());

                return voucher_exp_date;
            }

        }

        /// <summary>
        /// Returns resource folder path
        /// </summary>
        public static string reportsPath
        {
            get
            {
                string result = string.Empty;
                result = @"D:\WebToPrintResources\Walgreens\Production\Reports\";
                if (ConfigurationManager.AppSettings != null && ConfigurationManager.AppSettings["reportsPath"] != null)
                {
                    result = ConfigurationManager.AppSettings["reportsPath"].ToString();
                }
                return result;
            }
        }

        /// <summary>
        /// Returns resource folder path
        /// </summary>
        public static string resourcesPath
        {
            get
            {
                string result = string.Empty;
                result = @"D:\WebToPrintResources\Walgreens\Production\Resources\";
                if (ConfigurationManager.AppSettings != null && ConfigurationManager.AppSettings["resourcePath"] != null)
                {
                    result = ConfigurationManager.AppSettings["resourcePath"].ToString();
                }
                return result;
            }
        }

        public static DateTime getDeploymentDate
        {
            get
            {
                AppCommonSession common_app_session = (AppCommonSession)HttpContext.Current.Session["commonAppSession"];
                DateTime deployment_date = DateTime.Now;
                if (common_app_session.SelectedStoreSession.OutreachProgramSelectedId == "1")
                {
                    deployment_date = Convert.ToDateTime(ConfigurationManager.AppSettings["outreachStartDateSO"].ToString().Trim().Length == 0 ? DateTime.Now.ToShortDateString() : ConfigurationManager.AppSettings["outreachStartDateSO"].ToString());
                }
                else if (common_app_session.SelectedStoreSession.OutreachProgramSelectedId == "3")
                {
                    deployment_date = Convert.ToDateTime(ConfigurationManager.AppSettings["outreachStartDateIP"].ToString().Trim().Length == 0 ? DateTime.Now.ToShortDateString() : ConfigurationManager.AppSettings["outreachStartDateIP"].ToString());
                }
                return deployment_date;
            }
        }

        public static WalgreenEmail emailSettings()
        {
            config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            if (((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Host != null)
            {
                string password = ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Password != null ? ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Password : "";
                walgreenEmail = new WalgreenEmail(((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Host, ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.UserName, password, ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.Network.Port, ((MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings")).Smtp.From);
            }
            return walgreenEmail;
        }

        public static string encryptedLink(string email, string page_name)
        {
            EncryptedQueryString args = new EncryptedQueryString();
            args["arg1"] = email;
            args["arg2"] = walgreensAppPwd();
            emailUrlPath = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
            return emailUrlPath + page_name + "?args=" + args.ToString();
        }

        public static string encryptedLinkWithThreeArgs(string email, string page_name, string navigation_page)
        {
            EncryptedQueryString args = new EncryptedQueryString();
            args["arg1"] = email;
            args["arg2"] = walgreensAppPwd();
            args["arg3"] = navigation_page;
            emailUrlPath = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
            return emailUrlPath + page_name + "?args=" + args.ToString();
        }

        public static string encryptedLinkWithFourArgs(string email, string page_name, string navigation_page, string business_pk)
        {
            EncryptedQueryString args = new EncryptedQueryString();
            args["arg1"] = email;
            args["arg2"] = walgreensAppPwd();
            args["arg3"] = navigation_page;
            args["arg4"] = business_pk;
            emailUrlPath = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
            return emailUrlPath + page_name + "?args=" + args.ToString();
        }

        public static string encryptedLinkWithFiveArgs(string email, string page_name, string navigation_page, string contactlog_pk, string store_id)
        {
            EncryptedQueryString args = new EncryptedQueryString();
            args["arg1"] = email;
            args["arg2"] = walgreensAppPwd();
            args["arg3"] = navigation_page;
            args["arg4"] = contactlog_pk;
            args["arg5"] = store_id;
            emailUrlPath = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
            return emailUrlPath + page_name + "?args=" + args.ToString();
        }

        public static string encryptedLinkWithSixArgs(string email, string page_name, string navigation_page, string business_pk, string contactlog_pk, string store_id)
        {
            EncryptedQueryString args = new EncryptedQueryString();
            args["arg1"] = email;
            args["arg2"] = walgreensAppPwd();
            args["arg3"] = navigation_page;
            args["arg4"] = business_pk;
            args["arg5"] = contactlog_pk;
            args["arg6"] = store_id;
            emailUrlPath = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
            return emailUrlPath + page_name + "?args=" + args.ToString();
        }

        public static AppCommonSession initCommonAppSession()
        {
            if (HttpContext.Current.Session["commonAppSession"] == null)
            {
                HttpContext.Current.Session["commonAppSession"] = new AppCommonSession();
            }
            return (AppCommonSession)HttpContext.Current.Session["commonAppSession"];
        }

        public static void validateSession()
        {
            if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session.IsNewSession)
                {
                    HttpCookie newSessionIdCookie = HttpContext.Current.Request.Cookies["ASP.NET_SessionId"];
                    if (newSessionIdCookie != null)
                    {
                        string newSessionIdCookieValue = newSessionIdCookie.Value;
                        if (newSessionIdCookieValue.Length > 0)
                        {
                            if (HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.ToLower().Contains("admin") || HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.ToLower().Contains("reports") || HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.ToLower().Contains("corporatescheduler"))
                                HttpContext.Current.Response.Redirect("../auth/sessionTimeout.aspx");
                            else
                                HttpContext.Current.Response.Redirect("auth/sessionTimeout.aspx");
                        }
                    }
                }
            }
        }

        public SqlConnection setLogConnectionString()
        {
            SqlConnection conn_local = null;
            try
            {
                string connection_string = ConfigurationManager.ConnectionStrings["tdLoggingConnString"].ConnectionString;
                conn_local = new SqlConnection(connection_string);
                conn_local.Open();
            }
            catch (Exception ex)
            {
                logError(-1, ex.Message, ex.StackTrace);
                conn_local = null;
            }
            return conn_local;
        }

        /// <summary>
        /// Gets common password for walgreens
        /// </summary>
        /// <returns></returns>
        public static string walgreensAppPwd()
        {
            string wags_pwd = "welcome123$";

            if (ConfigurationManager.AppSettings != null && ConfigurationManager.AppSettings["walgreensPwd"] != null)
            {
                wags_pwd = ConfigurationManager.AppSettings["walgreensPwd"].ToString();
            }
            return wags_pwd;
        }

        public static string emailPath()
        {
            string email_url = "http://WagOutreach.com/";

            if (ConfigurationManager.AppSettings != null && ConfigurationManager.AppSettings["EmailURL"] != null)
            {
                email_url = ConfigurationManager.AppSettings["EmailURL"].ToString();
            }
            return email_url;
        }

        /// <summary>
        /// function finding whether logged in user is admin
        /// </summary>
        /// <returns></returns>
        public static bool isAdmin()
        {
            return (initCommonAppSession().LoginUserInfoSession.UserRole.Contains("Admin")) ? true : false;
        }

        /// <summary>
        /// function finding whether logged in user is Power User.
        /// </summary>
        /// <returns></returns>
        public static bool isPowerUser()
        {
            return (initCommonAppSession().LoginUserInfoSession.UserRole.Contains("Power User")) ? true : false;
        }

        public void logError(int job_number, String error_message, String stack_trace)
        {
            AppCommonSession common_app_session = (AppCommonSession)HttpContext.Current.Session["commonAppSession"];
            if (this.logConn == null)
            {
                string connection_string = ConfigurationManager.ConnectionStrings["tdLoggingConnString"].ConnectionString;
                this.logConn = new SqlConnection(connection_string);
                this.logConn.Open();
            }
            if ((common_app_session != null) && (common_app_session.LoginUserInfoSession != null) && (common_app_session.LoginUserInfoSession.UserName != null))
            {
                if (common_app_session.LoginUserInfoSession.UserName.Length > 15)
                    TdLogEvent.log(this.logConn, 1, ConfigurationManager.AppSettings["clientNumber"].ToString(), -1, "list", DateTime.Now, "listJobError", "listJobError", "<error><message>" + HttpUtility.HtmlEncode(error_message) + "</message><stackTrace>" + HttpUtility.HtmlEncode(stack_trace) + "</stackTrace></error>", error_message + " Stopping job at this point.", common_app_session.LoginUserInfoSession.UserName.ToString().Substring(0, 15), 0, 1);
                else
                    TdLogEvent.log(this.logConn, 1, ConfigurationManager.AppSettings["clientNumber"].ToString(), -1, "list", DateTime.Now, "listJobError", "listJobError", "<error><message>" + HttpUtility.HtmlEncode(error_message) + "</message><stackTrace>" + HttpUtility.HtmlEncode(stack_trace) + "</stackTrace></error>", error_message + " Stopping job at this point.", common_app_session.LoginUserInfoSession.UserName, 0, 1);
            }
        }

        /// <summary>
        /// Removes breaks in a string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string removeLineBreaks(string value)
        {
            return value.Replace(Environment.NewLine, " ").Replace('\r', ' ').Replace('\n', ' ');
        }

        public static string clientNumber()
        {
            string client_number = "";
            if (ConfigurationManager.AppSettings["clientNumber"] != null)
            {
                client_number = ConfigurationManager.AppSettings["clientNumber"].ToString();
            }
            return client_number;
        }

        public static string imageExtention()
        {
            string image_extention = ".jpg";

            if (ConfigurationManager.AppSettings["ImageExt"] != null)
            {
                image_extention = ConfigurationManager.AppSettings["ImageExt"].ToString();
            }

            return image_extention;

        }

        public static string clientLogoImagePath()
        {
            string image_path = @"d:\WebToPrintResources\Walgreens\Production\";
            if (ConfigurationManager.AppSettings["clientLogosPath"] != null)
                image_path = ConfigurationManager.AppSettings["clientLogosPath"].ToString() + @"client_logos";
            else
                image_path = image_path + @"client_logos";

            return image_path;
        }

        public static string emailImagePathWin()
        {
            string url_path = emailImagePath();
            url_path = url_path.Replace("..", "");
            url_path = url_path.Replace("/", "\\");

            if (!url_path.EndsWith("\\")) url_path += "\\";

            return url_path;
        }

        public static string emailImagePath()
        {
            string email_url = "images";

            if (ConfigurationManager.AppSettings["EmailImageURL"] != null)
            {
                email_url = ConfigurationManager.AppSettings["EmailImageURL"].ToString();
            }

            if (!email_url.EndsWith("/")) email_url += "/";
            return email_url;
        }

        public static string buttonImagePath()
        {
            string base_id = "~/images/buttons/";

            if (ConfigurationManager.AppSettings["buttonImageUrl"] != null)
            {
                base_id = ConfigurationManager.AppSettings["buttonImageUrl"].ToString();
            }
            return base_id;
        }

        public static string contactEmailXml()
        {
            string file_path = @"..\xmlDoc\email_config.xml";

            if (ConfigurationManager.AppSettings != null && ConfigurationManager.AppSettings["contactEmailXml"] != null)
            {
                if (ConfigurationManager.AppSettings != null)
                    file_path = ConfigurationManager.AppSettings["contactEmailXml"].ToString();
            }
            return file_path;
        }

        /// <summary>
        /// Return comma separated string of email ids from email_config.xml file for the selected group
        /// </summary>
        /// <param name="group_name"></param>
        /// <returns></returns>
        public static string getEmailInfoFromGroup(string group_name, bool is_pre_populate)
        {
            XmlDocument email_config_doc = new XmlDocument();
            if (!is_pre_populate)
                email_config_doc.Load(HttpContext.Current.Server.MapPath(contactEmailXml()));
            else
                email_config_doc.Load(HttpContext.Current.Server.MapPath(contactEmailXml().TrimStart('.').TrimStart('\\')));

            var email_elements = email_config_doc.SelectNodes("emails/emailActions/emailAction").Cast<XmlElement>().Where(
                    emails => emails.Attributes["name"].Value.ToLower() == group_name.ToLower());

            string email_ids = "";
            if (email_elements.Count() > 0)
            {
                foreach (XmlElement email in email_elements.ElementAt(0).ChildNodes)
                {
                    if (email_ids.Trim().Length != 0)
                        email_ids += "," + email.Attributes["address"].Value;
                    else
                        email_ids = email.Attributes["address"].Value;
                }
            }
            return email_ids;
        }
        /// <summary>
        /// Disables required services to users
        /// </summary>
        /// <returns></returns>
        public static string getClientServicesInfoFromGroup(string user_action)
        {
            string email_ids = "";
            if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
            {
                XmlDocument client_services_xdoc = new XmlDocument();
                client_services_xdoc.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml"));

                if (client_services_xdoc.SelectNodes("//clientAction[@name='" + user_action + "']") != null)
                {
                    XmlNodeList service_clients = client_services_xdoc.SelectNodes("//clientAction[@name='" + user_action + "']/client");

                    foreach (XmlNode client in service_clients)
                    {
                        if (email_ids.Trim().Length != 0)
                            email_ids += "," + client.Attributes["username"].Value;
                        else
                            email_ids = client.Attributes["username"].Value;
                    }
                }
            }
            return email_ids;
        }

        /// <summary>
        /// Disables required services to users
        /// </summary>
        /// <returns></returns>
        public static bool isdisableClientServices(string user_name, string user_action)
        {
            bool disable_service = false;

            if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
            {
                XmlDocument client_services_xdoc = new XmlDocument();
                client_services_xdoc.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml"));

                if (client_services_xdoc.SelectNodes("//clientAction[@name='" + user_action + "']") != null)
                {
                    XmlNodeList service_clients = client_services_xdoc.SelectNodes("//clientAction[@name='" + user_action + "']/client");

                    foreach (XmlNode client in service_clients)
                    {
                        if (!string.IsNullOrEmpty(user_name) && client.Attributes["username"].Value == user_name.Trim())
                            disable_service = true;
                    }
                }
            }

            return disable_service;
        }

        /// <summary>
        /// Returns list of States
        /// </summary>
        public static DataSet getStates
        {
            get
            {
                DataSet ds_states = new DataSet();
                string file_path = @".\xmlDoc\states.xml";
                if (File.Exists(HttpContext.Current.Server.MapPath(file_path)))
                    ds_states.ReadXml(HttpContext.Current.Server.MapPath(file_path));
                else
                {
                    file_path = @"..\xmlDoc\states.xml";
                    ds_states.ReadXml(HttpContext.Current.Server.MapPath(file_path));
                }

                return ds_states;
            }
        }

        /// <summary>
        /// Gets outreach program business types
        /// </summary>
        /// <param name="outreach_effort"></param>
        /// <returns></returns>
        public static DataTable getOutreachBusinessTypes(string outreach_effort)
        {
            return initCommonAppSession().LoginUserInfoSession.BusinessTypes.Select("category = '" + outreach_effort + "'").CopyToDataTable();
        }

        /// <summary>
        /// Gets outreach program business types for Contact Log binding
        /// </summary>
        /// <param name="outreach_effort"></param>
        /// <returns></returns>
        public static DataTable getOutreachBusinessTypesForContactLog(string outreach_effort)
        {
            DataView dv_business_types = initCommonAppSession().LoginUserInfoSession.BusinessTypes.Select("category = '" + outreach_effort + "' AND businessType <> 'National Contracts'").CopyToDataTable().DefaultView;
            dv_business_types.Sort = "name ASC";

            //DataTable dt_business_types = initCommonAppSession().LoginUserInfoSession.BusinessTypes.Select("category = '" + outreach_effort + "' AND businessType <> 'National Contracts'").CopyToDataTable();
            return dv_business_types.ToTable();
        }

        /// <summary>
        /// Sets tool tip for each item in drop down list
        /// </summary>
        /// <param name="ddl_list"></param>
        public static Control setItemToolTip(Control dropdown)
        {
            DropDownList drop_down = (DropDownList)dropdown;
            foreach (ListItem list_item in drop_down.Items)
            {
                list_item.Attributes.Add("title", list_item.Text);
            }
            return drop_down;

        }

        /// <summary>
        /// This function will use to validate phone
        /// </summary>
        /// <param name="validation_group"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool validatePhoneGroup(string validation_group, string value)
        {
            bool is_valid_phone = false;
            if (validation_group == "WalgreensUser")
            {
                is_valid_phone = value.validatePhone();
            }
            else if (validation_group == "SendLater")
            {
                if (value.Trim().Length != 0)
                {
                    is_valid_phone = value.validatePhone();
                }
                else
                    is_valid_phone = true;
            }
            return is_valid_phone;
        }

        /// <summary>
        /// Time picker at server side
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static StringBuilder displayTimePicker(string start_time_client_id, string end_time_client_id)
        {
            StringBuilder script_text = new StringBuilder();
            script_text.Append("$(function() {");
            script_text.Append("var DateSelector1 = $('#" + start_time_client_id + "'); ");
            //script_text.Append("DateSelector1.timepicker({ step:30,minTime: '5:00am',maxTime: '11:00pm' });");
            script_text.Append("DateSelector1.timepicker({ step:30, minTime: '00:00am',maxTime: '11:30pm' });");
            //script_text.Append("$('#" + end_time_client_id + "').timepicker({ step:30,minTime: $('#" + start_time_client_id + "').val(), maxTime: '00:00am' });");
            script_text.Append("$('#" + end_time_client_id + "').timepicker({ step:30, minTime: '00:00am', maxTime: '11:30pm' });");
            script_text.Append("DateSelector1.on('changeTime',function() {");
            script_text.Append("var min_time = ''; $('#" + end_time_client_id + "').val(''); ");
            script_text.Append("min_time = addTime($('#" + start_time_client_id + "').val());");
            script_text.Append("$('#" + end_time_client_id + "').timepicker('remove');");
            script_text.Append("$('#" + end_time_client_id + "').timepicker({ step:30, minTime: '00:00am', maxTime: '11:30pm'});");
            script_text.Append("}); ");
            script_text.Append(" });");
            return script_text;
        }

        /// <summary>
        /// Gets immunization scheduler appointment fields
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> getSchedulerApptFields
        {
            get
            {
                Dictionary<string, string> reg_fields = new Dictionary<string, string>();
                reg_fields.Add("name", "Name");
                reg_fields.Add("email", "Email");
                reg_fields.Add("phone", "Phone");
                reg_fields.Add("address", "Address");
                reg_fields.Add("gender", "Gender");
                reg_fields.Add("dateOfBirth", "Birth Date");
                reg_fields.Add("employeeId", "Employee ID");

                return reg_fields;
            }
        }

        /// <summary>
        /// Gets updated field label names
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> getLocalClinicFields
        {
            get
            {
                Dictionary<string, string> idic = new Dictionary<string, string>();
                idic.Add("clientName", "Client Name");
                idic.Add("contactPhone", "Contact Phone");
                idic.Add("contactFirstName", "Contact First Name");
                idic.Add("contactLastName", "Contact Last Name");
                idic.Add("contactEmail", "Contact Email");
                idic.Add("jobTitle", "Job Title");
                idic.Add("naClinicPlanId", "Plan ID");
                idic.Add("naClinicGroupId", "Group ID");
                idic.Add("recipientId", "ID Recipient");

                return idic;
            }
        }

        /// <summary>
        /// Compares clinic details
        /// </summary>
        /// <param name="new_clinic_details_xml"></param>
        /// <param name="old_clinic_details_xml"></param>
        /// <returns></returns>
        public static bool compareXMLDocuments(XmlDocument new_clinic_details_xml, XmlDocument old_clinic_details_xml)
        {
            XDocument new_clinic_details_xdoc = new XDocument();
            XDocument old_clinic_details_xdoc = new XDocument();
            new_clinic_details_xdoc = XDocument.Parse(new_clinic_details_xml.OuterXml);
            old_clinic_details_xdoc = XDocument.Parse(old_clinic_details_xml.OuterXml);

            return XDocument.DeepEquals(new_clinic_details_xdoc, old_clinic_details_xdoc);
        }

        /// <summary>
        /// Gets Compares clinic details xml and returns updated values
        /// </summary>
        /// <param name="new_clinic_details_xml"></param>
        /// <param name="old_clinic_details_xml"></param>
        /// <param name="clinic_type"></param>
        /// <returns></returns>
        public static Dictionary<string, string> getUpdatedClinicDetails(XmlDocument new_clinic_details_xml, XmlDocument old_clinic_details_xml, string clinic_type)
        {
            //Stopping "Updated Clinic Billing Information" email to all users - CR(28/82/2015 - Vishnu)
            bool is_changed = false, is_details_changed = false;//, is_billing_changed = false, is_billing_all_clinics_changed = false;
            string old_value = string.Empty, new_value = string.Empty;
            int counter = 0;

            Dictionary<string, string> updated_values = new Dictionary<string, string>();
            XmlDocument clinic_update_fields_xml = new XmlDocument();

            if (clinic_type.ToLower() == "local" || clinic_type.ToLower() == "localprevious")
                clinic_update_fields_xml.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/localClinicDetailsFields.xml"));
            else if (clinic_type.ToLower() == "charity")
                clinic_update_fields_xml.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/charityClinicDetailsFields.xml"));
            else if (clinic_type.ToLower() == "community outreach")
                clinic_update_fields_xml.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/coClinicDetailsFields.xml"));
            else
                clinic_update_fields_xml.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/corporateClinicDetailsFields.xml"));

            XmlDocument clinic_updates_xml = new XmlDocument();
            XmlElement clinic_updates_ele = clinic_updates_xml.CreateElement("clinicUpdates");

            string str_email_body = "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #000000; cellspacing: 5;'>";
            //string str_billing_email_body = "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #000000; cellspacing: 5;'>";
            //string str_billing_email_to_all_clinics_body = "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #000000; cellspacing: 5; width:100%;'>";

            foreach (XmlNode clinic_xn in clinic_update_fields_xml.DocumentElement.ChildNodes)
            {
                if (clinic_xn.Name != "uploadClinicInfo")
                {
                    counter = 0;
                    XmlElement clinic_updated_section = clinic_updates_xml.CreateElement(clinic_xn.Name);

                    clinic_updated_section.SetAttribute("displayName", clinic_xn.Attributes["displayName"].Value);

                    foreach (XmlNode update_field in clinic_xn.SelectNodes("field"))
                    {
                        if ((clinic_type.ToLower() == "local" || clinic_type.ToLower() == "community outreach" || clinic_type.ToLower() == "corporate") && (update_field.Attributes["name"].Value == "estimatedQuantity" || update_field.Attributes["name"].Value == "totalImmAdministered"))
                        {
                            XmlNodeList old_clinic_immunizations = old_clinic_details_xml.SelectNodes("//" + update_field.Attributes["section"].Value + "/Immunizations/Immunization");
                            XmlNodeList new_clinic_immunizations = new_clinic_details_xml.SelectNodes("//" + update_field.Attributes["section"].Value + "/Immunizations/Immunization");

                            for (int i = 0; i < old_clinic_immunizations.Count; i++)
                            {
                                old_value = old_clinic_immunizations[i].Attributes[update_field.Attributes["name"].Value].Value;
                                new_value = new_clinic_immunizations[i].Attributes[update_field.Attributes["name"].Value].Value;
                                if (old_value != new_value)
                                {
                                    if (clinic_type.ToLower() == "corporate")
                                    {
                                        if (update_field.Attributes["name"].Value == "estimatedQuantity")
                                            updated_values.Add("Estimated " + old_clinic_immunizations[i].Attributes["immunizationName"].Value + " Volume:", new_value);
                                        else if (update_field.Attributes["name"].Value == "totalImmAdministered")
                                            updated_values.Add(old_clinic_immunizations[i].Attributes["immunizationName"].Value + " Total Administered:", new_value);
                                    }
                                    else
                                    {
                                        if (update_field.Attributes["name"].Value == "estimatedQuantity")
                                            updated_values.Add("Estimated " + old_clinic_immunizations[i].Attributes["immunizationName"].Value + " &lt;br/&gt;(" + old_clinic_immunizations[i].Attributes["paymentTypeName"].Value + ") Shots:", new_value);
                                        else if (update_field.Attributes["name"].Value == "totalImmAdministered")
                                            updated_values.Add(old_clinic_immunizations[i].Attributes["immunizationName"].Value + " &lt;br/&gt;(" + old_clinic_immunizations[i].Attributes["paymentTypeName"].Value + ") Total Administered:", new_value);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (old_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value) != null)
                            {
                                if (old_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes[update_field.Attributes["name"].Value] != null)
                                    old_value = old_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes[update_field.Attributes["name"].Value].Value;

                                if (new_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes[update_field.Attributes["name"].Value] != null)
                                    new_value = new_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes[update_field.Attributes["name"].Value].Value;

                                if (clinic_type.ToLower() == "corporate" && update_field.Attributes["name"].Value == "contactPhone")
                                {
                                    old_value += (!string.IsNullOrEmpty(old_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes["contactPhoneExt"].Value) ? " Ext: " + old_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes["contactPhoneExt"].Value : "");
                                    new_value += (!string.IsNullOrEmpty(new_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes["contactPhoneExt"].Value) ? " Ext: " + new_clinic_details_xml.SelectSingleNode("//" + update_field.Attributes["section"].Value).Attributes["contactPhoneExt"].Value : "");
                                }
                            }
                            else if (clinic_type.ToLower() == "corporate" && clinic_xn.Name == "postClinicInfo")
                            {
                                if (old_clinic_details_xml.SelectSingleNode("//clinicInformation").Attributes[update_field.Attributes["name"].Value] != null)
                                    old_value = old_clinic_details_xml.SelectSingleNode("//clinicInformation").Attributes[update_field.Attributes["name"].Value].Value;

                                if (new_clinic_details_xml.SelectSingleNode("//clinicInformation").Attributes[update_field.Attributes["name"].Value] != null)
                                    new_value = new_clinic_details_xml.SelectSingleNode("//clinicInformation").Attributes[update_field.Attributes["name"].Value].Value;
                            }
                        }

                        if ((old_value != new_value && !(update_field.Attributes["type"] != null && update_field.Attributes["type"].Value == "ClinicOrVoucherField" && new_value == "")) || updated_values.Count() > 0)
                        {
                            //Create updated field value xml for history log
                            is_details_changed = true;
                            XmlElement clinic_updated_field = clinic_updates_xml.CreateElement("field");
                            clinic_updated_field.SetAttribute("displayName", update_field.Attributes["displayName"].Value);
                            if ((clinic_type.ToLower() == "local" || clinic_type.ToLower() == "community outreach" || clinic_type.ToLower() == "corporate") && (update_field.Attributes["name"].Value == "estimatedQuantity" || update_field.Attributes["name"].Value == "totalImmAdministered"))
                            {
                                clinic_updated_field.SetAttribute("value", "");
                                if (updated_values.Count() > 0)
                                {
                                    foreach (KeyValuePair<string, string> updated_imm in updated_values)
                                    {
                                        XmlElement updated_imm_ele = clinic_updates_xml.CreateElement("immunization");
                                        updated_imm_ele.SetAttribute("name", updated_imm.Key);
                                        updated_imm_ele.SetAttribute("value", updated_imm.Value);
                                        clinic_updated_field.AppendChild(updated_imm_ele);
                                    }
                                }
                            }
                            else
                            {
                                if (update_field.Attributes["name"].Value == "isNoClinic" && new_value != "")
                                    new_value = new_value == "1" ? "Yes" : "No";
                                clinic_updated_field.SetAttribute("value", ((update_field.Attributes["name"].Value == "clinicDate" && new_value != "") ? Convert.ToDateTime(new_value).ToString("MM/dd/yyyy") : new_value));
                            }

                            clinic_updated_section.AppendChild(clinic_updated_field);

                            //Create updated field values table for email
                            //if (Convert.ToBoolean(update_field.Attributes["isEmail"].Value) || Convert.ToBoolean(update_field.Attributes["isBillingEmail"].Value))
                            if (Convert.ToBoolean(update_field.Attributes["isEmail"].Value))
                            {
                                if (counter == 0)
                                    str_email_body += "<tr class='fields' style='font-weight: bold; color: #222222;' align='left' valign='top'><b>" + clinic_xn.Attributes["displayName"].Value + ":</b></td></tr>";

                                if ((clinic_type.ToLower() == "local" || clinic_type.ToLower() == "community outreach" || clinic_type.ToLower() == "corporate") && (update_field.Attributes["name"].Value == "estimatedQuantity" || update_field.Attributes["name"].Value == "totalImmAdministered") && updated_values.Count() > 0)
                                {
                                    foreach (KeyValuePair<string, string> updated_imm in updated_values)
                                    {
                                        if (Convert.ToBoolean(update_field.Attributes["isEmail"].Value))
                                        {
                                            str_email_body += "<tr class='rowStyle' align='left'><td class='fieldTitle'>" + updated_imm.Key.Replace("&lt;br/&gt;", "") + "</td></tr>";
                                            str_email_body += "<tr class='rowStyle' align='left'><td class='fieldData'>" + updated_imm.Value + "</td></tr>";
                                            is_changed = true;
                                        }
                                        //if (Convert.ToBoolean(update_field.Attributes["isBillingEmail"].Value))
                                        //{
                                        //    str_billing_email_body += "<tr class='rowStyle' align='left'><td class='fieldTitle'>" + updated_imm.Key.Replace("&lt;br/&gt;", "") + "</td></tr>";
                                        //    str_billing_email_body += "<tr class='rowStyle' align='left'><td class='fieldData'>" + updated_imm.Value + "</td></tr>";
                                        //    is_billing_changed = true;
                                        //}
                                    }
                                }
                                else
                                {
                                    if (Convert.ToBoolean(update_field.Attributes["isEmail"].Value))
                                    {
                                        str_email_body += "<tr class='rowStyle' align='left'><td class='fieldTitle'>" + update_field.Attributes["displayName"].Value + ":</td><tr>";
                                        str_email_body += "<tr class='rowStyle' align='left'><td class='fieldData'>" + ((update_field.Attributes["name"].Value == "clinicDate" && new_value != "") ? Convert.ToDateTime(new_value).ToString("MM/dd/yyyy") : new_value) + "</td></tr>";
                                        is_changed = true;
                                    }
                                    //if (Convert.ToBoolean(update_field.Attributes["isBillingEmail"].Value))
                                    //{
                                    //    str_billing_email_body += "<tr class='rowStyle' align='left'><td class='fieldTitle'>" + update_field.Attributes["displayName"].Value + ":</td><tr>";
                                    //    str_billing_email_body += "<tr class='rowStyle' align='left'><td class='fieldData'>" + ((update_field.Attributes["name"].Value == "clinicDate") ? Convert.ToDateTime(new_value).ToString("MM/dd/yyyy") : new_value) + "</td></tr>";
                                    //    is_billing_changed = true;

                                    //    //Billing Code Updates Email Alert to All Clinics Associated with a Contract or Charity Program - TDirect - 10759
                                    //    if (update_field.Attributes["name"].Value == "naClinicGroupId" || update_field.Attributes["name"].Value == "naClinicPlanId" || update_field.Attributes["name"].Value == "recipientId")
                                    //    {
                                    //        str_billing_email_to_all_clinics_body += "<tr class='rowStyle' align='left'><td class='fieldTitle'>" + update_field.Attributes["displayName"].Value + ":</td><tr>";
                                    //        str_billing_email_to_all_clinics_body += "<tr class='rowStyle' align='left'><td class='fieldData'>" + ((update_field.Attributes["name"].Value == "clinicDate") ? Convert.ToDateTime(new_value).ToString("MM/dd/yyyy") : new_value) + "</td></tr>";
                                    //        is_billing_all_clinics_changed = true;
                                    //    }
                                    //}
                                }

                                counter++;
                            }
                        }
                        updated_values.Clear();
                        old_value = "";
                        new_value = "";
                    }
                    clinic_updates_ele.AppendChild(clinic_updated_section);
                }
            }
            str_email_body += "</table>";
            //str_billing_email_body += "</table>";

            clinic_updates_xml.AppendChild(clinic_updates_ele);

            updated_values.Clear();
            updated_values.Add("emailBody", (!is_changed ? string.Empty : str_email_body));
            updated_values.Add("historyLog", (!is_details_changed ? string.Empty : clinic_updates_xml.OuterXml));
            //updated_values.Add("billingEmail", (!is_billing_changed ? string.Empty : str_billing_email_body));
            //updated_values.Add("billingEmailToAllClinics", (!is_billing_all_clinics_changed ? string.Empty : str_billing_email_to_all_clinics_body));
            return updated_values;
        }

        /// <summary>
        /// Returns clinic update history logs
        /// </summary>
        /// <param name="dt_update_history"></param>
        /// <param name="clinic_type"></param>
        /// <returns></returns>
        public static DataTable getClinicUpdateHistory(DataTable dt_update_history, string clinic_type)
        {
            DataTable dt_clinic_updates = new DataTable();
            dt_clinic_updates = dt_update_history.Clone();
            if (dt_update_history.Rows.Count > 0)
            {
                string updated_field = string.Empty;
                string updated_value = string.Empty;
                int counter = 0;
                XmlDocument clinic_updates_xml;

                foreach (DataRow row in dt_update_history.Rows)
                {
                    updated_field = string.Empty;
                    updated_value = string.Empty;
                    DataRow dr_update = dt_clinic_updates.NewRow();
                    dr_update["updatedOn"] = row["updatedOn"];
                    dr_update["updateAction"] = row["updateAction"];
                    if (row["updateAction"].ToString() == "Updated")
                    {
                        counter = 0;
                        clinic_updates_xml = new XmlDocument();
                        clinic_updates_xml.LoadXml(row["updatedValue"].ToString());
                        foreach (XmlNode clinic_xn in clinic_updates_xml.DocumentElement.ChildNodes)
                        {

                            if (clinic_xn.SelectNodes("field").Count > 0)
                            {
                                updated_field += "<table border='0' class='formFields' cellspacing='0' cellpadding='2' width='100%'>";
                                //updated_value += "<table border='0' class='formFields' cellspacing='0' cellpadding='2'>";
                                updated_field += "<tr><td colspan='2' style='vertical-align:top; text-align: left; " + ((counter == 0) ? "" : "padding-top:10px;") + "'><b>" + clinic_xn.Attributes["displayName"].Value + ":</b></td></tr>";
                                //updated_value += "<tr><td style='vertical-align:top; text-align: left; height: 25px; " + ((counter == 0) ? "" : "padding-top:10px;") + "'>&nbsp;</td></tr>";

                                foreach (XmlNode update_field in clinic_xn.SelectNodes("field"))
                                {
                                    if ((clinic_type.ToLower() == "local" || clinic_type.ToLower() == "corporate") && (update_field.Attributes["displayName"].Value == "Estimated Volume" || update_field.Attributes["displayName"].Value == "Total Administered"))
                                    {
                                        foreach (XmlNode clinic_imm in update_field.SelectNodes("immunization"))
                                        {
                                            updated_field += "<tr><td width='50%' style='vertical-align:top; text-align: left;'>" + clinic_imm.Attributes["name"].Value.Replace("&lt;br/&gt;", "<br/>") + "</td><td style='vertical-align:top; text-align: left;'>" + clinic_imm.Attributes["value"].Value + "</td></tr>";
                                            //updated_value += "<tr><td style='vertical-align:top; text-align: left;'>" + clinic_imm.Attributes["value"].Value + "</td></tr>";
                                        }
                                    }
                                    else
                                    {
                                        updated_field += "<tr><td width='50%' style='vertical-align:top; text-align: left;'>" + update_field.Attributes["displayName"].Value + "</td><td style='vertical-align:top; text-align: left;'>" + update_field.Attributes["value"].Value + "</td></tr>";
                                        //updated_value += "<tr><td style='vertical-align:top; text-align: left;'>" + update_field.Attributes["value"].Value + "</td></tr>";
                                    }
                                }

                                counter++;
                                updated_field += "</table>";
                                //updated_value += "</table>";
                            }
                        }

                        dr_update["updatedField"] = updated_field;
                        dr_update["updatedValue"] = updated_value;
                    }
                    else
                    {
                        dr_update["updatedField"] = row["updatedField"];
                        dr_update["updatedValue"] = row["updatedValue"];
                    }
                    dr_update["updatedBy"] = row["updatedBy"];
                    dt_clinic_updates.Rows.Add(dr_update);
                }
            }

            return dt_clinic_updates;
        }

        /// <summary>
        /// Compares two xml and returns string
        /// </summary>
        /// <param name="new_clinic_details_xml"></param>
        /// <param name="old_clinic_details_xml"></param>
        /// <returns></returns>
        public static string compareXML(XDocument new_clinic_details_xml, XDocument old_clinic_details_xml, out string updatedFields, out string updatedValues)
        {
            Dictionary<string, string> dic_labels = getLocalClinicFields;
            XmlDocument xdoc = new XmlDocument();
            XmlDocument xdoc1 = new XmlDocument();
            StringBuilder sb_updatedFields = new StringBuilder();
            StringBuilder sb_updatedValues = new StringBuilder();

            string str_output = "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #000000;'>";
            bool is_changed = false;
            xdoc1.LoadXml(new_clinic_details_xml.ToString());
            xdoc.LoadXml(old_clinic_details_xml.ToString());

            foreach (XmlNode xn in xdoc)
            {
                XmlNodeList x = ((XmlElement)xn).ChildNodes;

                for (int j = 0; j < x.Count; j++)
                {
                    XmlNodeList xNodeOld = xdoc.SelectNodes("clinicDetails/" + x[j].Name);
                    XmlNodeList xNodeNew = xdoc1.SelectNodes("clinicDetails/" + x[j].Name);

                    for (int k = 0; k < xNodeOld.Count; k++)
                    {
                        XmlAttributeCollection attrColl = xNodeOld[k].Attributes;
                        XmlAttributeCollection attrCol2 = xNodeNew[k].Attributes;
                        for (int i = 0; i < attrColl.Count; i++)
                        {
                            if (dic_labels.ContainsKey(attrCol2[i].Name))
                            {
                                if (!(attrColl[i].Value.Equals(attrCol2[i].Value.ToString()) && attrColl[i].Name.Equals(attrCol2[i].Name.ToString())) && (attrCol2[i].Name.ToLower() != "comments") && (attrCol2[i].Name.ToLower() != "feedback"))
                                {
                                    if (!is_changed)
                                        is_changed = true;

                                    str_output += "<tr><td><b>" + dic_labels[attrCol2[i].Name] + ":</b></td>";
                                    str_output += "<td>" + attrCol2[i].InnerText + "</td></tr>";

                                    sb_updatedFields.Append(dic_labels[attrCol2[i].Name]);
                                    sb_updatedFields.Append(" || ");

                                    sb_updatedValues.Append(dic_labels[attrCol2[i].Name]);
                                    sb_updatedValues.Append(":");
                                    sb_updatedValues.Append(attrCol2[i].InnerText);
                                    sb_updatedValues.Append(" || ");
                                }
                            }
                        }
                    }
                }
            }
            str_output += "</table>";

            str_output = !is_changed ? string.Empty : str_output;
            updatedFields = !is_changed ? string.Empty : sb_updatedFields.ToString().Trim(new char[] { '|', ' ' });
            updatedValues = !is_changed ? string.Empty : sb_updatedValues.ToString().Trim(new char[] { '|', ' ' });

            return str_output;
        }

        /// <summary>
        /// This function will be use to set color
        /// </summary>
        /// <param name="xml_doc"></param>
        /// <param name="style_name"></param>
        /// <returns></returns>
        public static System.Drawing.Color setColors(IXPathNavigable xml_doc, string style_name)
        {
            return System.Drawing.ColorTranslator.FromHtml(xml_doc.CreateNavigator().SelectSingleNode("//site/logoAndStyles/@" + style_name).Value);
        }

        public static string EmailLogoImagePath()
        {
            string logo_image_path = @"SchedulerLogoImages";

            if (ConfigurationManager.AppSettings["EmailLogoImages"] != null)
                logo_image_path = ConfigurationManager.AppSettings["EmailLogoImages"].ToString();

            return logo_image_path;
        }

        /// <summary>
        /// Resizes logo image
        /// </summary>
        /// <param name="path"></param>
        /// <param name="in_width"></param>
        /// <param name="in_height"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public static void getResizedImage(string path, int in_width, int in_height, out int width, out int height)
        {
            string file_path = ApplicationSettings.clientLogoImagePath() + "\\" + path.Substring(path.LastIndexOf('/') + 1, (path.Length - path.LastIndexOf('/') - 1));
            Bitmap img_in = new Bitmap(file_path);
            if (img_in.Width < 300)
            {
                width = img_in.Width;
                height = img_in.Height;
                return;
            }

            double y = img_in.Height;
            double x = img_in.Width;

            double factor = 1;
            if (in_width > 0)
            {
                factor = in_width / x;
            }
            else if (in_height > 0)
            {
                factor = in_height / y;
            }
            //System.IO.MemoryStream outStream =
            //new System.IO.MemoryStream();
            Bitmap img_out =
            new Bitmap((int)(x * factor), (int)(y * factor));
            Graphics g = Graphics.FromImage(img_out);
            g.Clear(Color.White);
            g.DrawImage(img_in, new Rectangle(0, 0, (int)(factor * x),
            (int)(factor * y)),
            new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

            width = img_out.Width;
            height = img_out.Height;
        }

        /// <summary>
        /// Get Corporate Files Upload Path
        /// </summary>
        public static string GetCorporateClinicsUploadFilesPath
        {
            get
            {
                string Path = @"D:\WebToPrintResources\Walgreens\Production\CorporateClinicsData\";
                if (ConfigurationManager.AppSettings["corporateUploadPath"] != null)
                {
                    Path = ConfigurationManager.AppSettings["corporateUploadPath"].ToString();
                }
                return Path;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetContractVoucherVARPdfFilesPath
        {
            get
            {
                string Path = @"D:\WebToPrintResources\Walgreens\Production\contractVoucherVARPdfs\";
                if (ConfigurationManager.AppSettings["contractVoucherVARPdfs"] != null)
                {
                    Path = ConfigurationManager.AppSettings["contractVoucherVARPdfs"].ToString();
                }
                return Path;
            }
        }

        /// <summary>
        /// Validates clinic date and time overlap between clinics within the store
        /// </summary>
        /// <param name="clinic_nodes"></param>
        /// <param name="failed_clinic_locations"></param>
        /// <param name="type"></param>
        /// <param name="business_name"></param>
        /// <param name="date_time_stamp_message"></param>
        /// <returns></returns>
        public static bool checkDateTimeStampValidation(XmlNodeList clinic_nodes, out string failed_clinic_locations, string type, string business_name, string date_time_stamp_message)
        {
            business_name = business_name.Replace("'", "`");
            string[] single_conflict_values = null;
            List<string> compared_list = new List<string>();
            List<string> conflicted_list = new List<string>();
            bool is_conflict_location = false;
            bool result = true;
            string title_text = "Multiple Conflicts Alert";

            string clinic_date_Attribute = "clinicDate";
            string clinic_location_Attribute = type.Contains("agreement") ? "clinicLocation" : "naClinicLocation";
            string clinic_end_time_Attribute = type.Contains("agreement") ? (type == "charityagreement" ? "clinicEndTime" : "endTime") : "naClinicEndTime";
            string clinic_start_time_Attribute = type.Contains("agreement") ? (type == "charityagreement" ? "clinicStartTime" : "startTime") : "naClinicStartTime";

            //date_time_stamp_message
            List<string> failed_locations = string.IsNullOrEmpty(date_time_stamp_message) ? new List<string>() : date_time_stamp_message.Trim().Split(',').Select(x => x.Replace("Clinic", "").Replace("Location", "").Trim()).ToList();
            int start_index = string.IsNullOrEmpty(date_time_stamp_message) ? 1 : 0;
            string validation_message_key = type.Contains("agreement") ? "datetimeOverlapMultipleConflictMessage" : "datetimeOverlapMultipleConflictMessageDetails";
            if (type == "charityagreement")
            {
                validation_message_key = "datetimeOverlapMultipleConflictMessageDetails";
            }
            failed_clinic_locations = (string)HttpContext.GetGlobalResourceObject("errorMessages", validation_message_key) + "<br/>";
            if (clinic_nodes.Count == 0)
            {
                failed_clinic_locations = string.Empty;
                return result;
            }
            foreach (XmlNode node in clinic_nodes)
            {
                for (int i = start_index; i < clinic_nodes.Count; i++)
                {
                    is_conflict_location = false;
                    XmlNode compare_node = clinic_nodes[i];
                    if (!string.IsNullOrEmpty(date_time_stamp_message))//checking for database fail
                    {
                        is_conflict_location = failed_locations.Contains(node.Attributes[clinic_location_Attribute].Value.Replace("Clinic", "").Replace("Location", "").Trim());
                    }
                    else//checking for internalclinics fail
                    {
                        if (!string.IsNullOrEmpty(node.Attributes[clinic_date_Attribute].Value) && !string.IsNullOrEmpty(compare_node.Attributes[clinic_date_Attribute].Value))
                        {
                            if (node.Attributes[clinic_location_Attribute].Value != compare_node.Attributes[clinic_location_Attribute].Value && !compared_list.Contains(compare_node.Attributes[clinic_location_Attribute].Value))
                            {
                                DateTime clinic1_start_time = Convert.ToDateTime(Convert.ToDateTime(node.Attributes[clinic_date_Attribute].Value).ToShortDateString() + ' ' + Convert.ToDateTime(node.Attributes[clinic_start_time_Attribute].Value).ToShortTimeString());
                                DateTime clinic1_end_time = Convert.ToDateTime(Convert.ToDateTime(node.Attributes[clinic_date_Attribute].Value).ToShortDateString() + ' ' + Convert.ToDateTime(node.Attributes[clinic_end_time_Attribute].Value).ToShortTimeString()).AddMinutes(30);
                                DateTime clinic2_start_time = Convert.ToDateTime(Convert.ToDateTime(compare_node.Attributes[clinic_date_Attribute].Value).ToShortDateString() + ' ' + Convert.ToDateTime(compare_node.Attributes[clinic_start_time_Attribute].Value).ToShortTimeString());
                                DateTime clinic2_end_time = Convert.ToDateTime(Convert.ToDateTime(compare_node.Attributes[clinic_date_Attribute].Value).ToShortDateString() + ' ' + Convert.ToDateTime(compare_node.Attributes[clinic_end_time_Attribute].Value).ToShortTimeString()).AddMinutes(30);
                                if (!(clinic2_end_time <= clinic1_start_time || clinic1_end_time <= clinic2_start_time))
                                {
                                    is_conflict_location = true;
                                    compared_list.Add(compare_node.Attributes[clinic_location_Attribute].Value);
                                }
                            }
                        }
                    }
                    XmlNode value_node = string.IsNullOrEmpty(date_time_stamp_message) ? compare_node : node;
                    if (is_conflict_location)
                    {
                        if (single_conflict_values == null)
                        {
                            single_conflict_values = new string[] { type.Contains("agreement") ? value_node.Attributes[clinic_location_Attribute].Value : "Clinic " + value_node.Attributes[clinic_location_Attribute].Value, Convert.ToDateTime(value_node.Attributes[clinic_date_Attribute].Value).ToShortDateString(), Convert.ToDateTime(value_node.Attributes[clinic_start_time_Attribute].Value).ToShortTimeString(), Convert.ToDateTime(value_node.Attributes[clinic_end_time_Attribute].Value).ToShortTimeString(), business_name };
                        }
                        if (!conflicted_list.Contains(value_node.Attributes[clinic_location_Attribute].Value))
                        {
                            failed_clinic_locations += String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictLocations"), type.Contains("agreement") ? value_node.Attributes[clinic_location_Attribute].Value : "Clinic " + value_node.Attributes[clinic_location_Attribute].Value, Convert.ToDateTime(value_node.Attributes[clinic_date_Attribute].Value).ToShortDateString(), Convert.ToDateTime(value_node.Attributes[clinic_start_time_Attribute].Value).ToShortTimeString(), Convert.ToDateTime(value_node.Attributes[clinic_end_time_Attribute].Value).ToShortTimeString(), business_name);
                            conflicted_list.Add(value_node.Attributes[clinic_location_Attribute].Value);
                        }
                        result = false;
                        if (!string.IsNullOrEmpty(date_time_stamp_message))//break if databse validation faild check
                        {
                            break;
                        }
                    }
                }
            }

            if (compared_list.Count == 1 || failed_locations.Count == 1)
            {
                title_text = "Single Conflict Alert";
                string vlidation_message = type.Contains("agreement") ? "datetimeOverlapSingleConflict" : "datetimeOverlapSingleConflictDetails";
                if (type == "charityagreement")
                {
                    vlidation_message = "datetimeOverlapSingleConflictDetails";
                }
                failed_clinic_locations = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", vlidation_message), single_conflict_values[0], single_conflict_values[1], single_conflict_values[2], single_conflict_values[3], single_conflict_values[4]);
            }

            if (result)
            {
                failed_clinic_locations = string.Empty;
            }

            failed_clinic_locations.TrimEnd(',');
            if (failed_clinic_locations.Length > 0)
            {
                if (!((type == "localagreement" || type == "localdetails" || type == "coagreement" || type == "codetails") && string.IsNullOrEmpty(date_time_stamp_message)))
                    failed_clinic_locations = "showDateTimeStampValidationMessage('" + failed_clinic_locations + "','" + title_text + "');";
            }

            return result;
        }

        public string getLocationFilterExpression(DropDownList ddl_location, TextBox txt_id_location)
        {
            string result = "";
            switch (ddl_location.SelectedItem.Value.ToLower())
            {
                case "region":
                    result = " AND regionNumber =" + Convert.ToInt32(txt_id_location.Text);
                    break;
                case "area":
                    result = " AND areaNumber =" + Convert.ToInt32(txt_id_location.Text);
                    break;
                case "district":
                    result = " AND districtId =" + Convert.ToInt32(txt_id_location.Text);
                    break;
                case "store":
                    result = " AND storeId =" + Convert.ToInt32(txt_id_location.Text);
                    break;
            }
            return result;
        }

        /// <summary>
        /// Get restrictions for State stores
        /// </summary>
        /// <param name="restriction_key"></param>
        /// <param name="state_name"></param>
        /// <returns></returns>
        public static string getStoreStateRestrictions(string restriction_key, string state_name = null)
        {
            string restriction_value = "";

            if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
            {
                XmlDocument client_services_xdoc = new XmlDocument();
                client_services_xdoc.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml"));

                if (client_services_xdoc.SelectNodes("//clientAction[@name='StoreStateRestrictions']") != null)
                {
                    XmlNodeList node_list = client_services_xdoc.SelectNodes("//clientAction[@name='StoreStateRestrictions']/restriction");

                    if (!string.IsNullOrEmpty(state_name))
                        node_list = client_services_xdoc.SelectNodes("//clientAction[@name='StoreStateRestrictions']/restriction[@storeState = '" + state_name + "']");

                    foreach (XmlNode node in node_list)
                    {
                        if (node.Attributes[restriction_key] != null)
                            restriction_value = node.Attributes[restriction_key].Value;
                    }
                }
            }

            return restriction_value;
        }

        /// <summary>
        /// Check if Store State is restricted during the current time period
        /// </summary>
        /// <param name="state_name"></param>
        /// <param name="user_role"></param>
        /// <returns></returns>
        public static bool isRestrictedStoreState(string state_name, string user_role)
        {
            bool is_restricted = false;
            string restricted_states = getStoreStateRestrictions("storeState", state_name);
            //string override_user = getStoreStateRestrictions("override");
            string restriction_start_date = getStoreStateRestrictions("restrictionStartDate", state_name);
            string restriction_end_date = getStoreStateRestrictions("restrictionEndDate", state_name);
            if (!string.IsNullOrEmpty(restriction_start_date) && !string.IsNullOrEmpty(restriction_end_date))
            {
                if (Convert.ToDateTime(restriction_start_date) <= DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
                {
                    if (!string.IsNullOrEmpty(restricted_states) && restricted_states.Contains(state_name))
                        is_restricted = true;
                }
            }

            return is_restricted;
        }

        /// <summary>
        /// Returns New SO template date
        /// </summary>
        public static DateTime newSOTemplateDate
        {
            get
            {
                DateTime result = new DateTime();
                if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
                {
                    XmlDocument client_services_xdoc = new XmlDocument();
                    client_services_xdoc.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml"));

                    if (client_services_xdoc.SelectNodes("//clientAction[@name='SONewTemplate']") != null)
                    {
                        XmlNodeList service_clients = client_services_xdoc.SelectNodes("//clientAction[@name='SONewTemplate']/NewTemplateInstallation");
                        result = Convert.ToDateTime(service_clients[0].Attributes["Date"].Value);
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Gets all the special user emails from xml.
        /// </summary>
        /// <param name="client_action_key"></param>
        /// <returns></returns>
        public static string getAllSpecialUserEmails(string client_action_key)
        {
            IList<string> lst_user_emails = new List<string>();

            if (File.Exists(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml")))
            {
                XmlDocument client_services_xdoc = new XmlDocument();
                client_services_xdoc.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml"));

                if (client_services_xdoc.SelectNodes("//clientAction[@name='" + client_action_key + "']") != null)
                {
                    XmlNodeList node_list = client_services_xdoc.SelectNodes("//clientAction[@name='" + client_action_key + "']/client");

                    foreach (XmlNode node in node_list)
                    {
                        if (node.Attributes["username"] != null)
                            lst_user_emails.Add(node.Attributes["username"].Value);
                    }
                }
            }

            return string.Join(", ", lst_user_emails);
        }

        /// <summary>
        /// Creates Datatable from Collections
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity, null);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        /// <summary>
        /// Gets the values from xml config
        /// </summary>
        /// <param name="state_name"></param>
        /// <param name="state_language"></param>
        /// <returns></returns>
        public static string getPDFPath(string state_name, string state_language, string path = "value")
        {
            XmlDocument client_services_xdoc = new XmlDocument();
            string return_val = string.Empty;
            client_services_xdoc.Load(HttpContext.Current.Server.MapPath("~/xmlDoc/clientServicesConfigXML.xml"));

            if (client_services_xdoc.SelectNodes("//clientAction[@name='StateSpecificVARForms']") != null)
            {
                XmlNodeList node_list = client_services_xdoc.SelectNodes("//clientAction[@name='StateSpecificVARForms']/state");

                if (!string.IsNullOrEmpty(state_name))
                    node_list = client_services_xdoc.SelectNodes("//clientAction[@name='StateSpecificVARForms']/state[@name = '" + state_name + "' and @language ='" + state_language.ToUpper().Trim() + "']");
                else
                    node_list = client_services_xdoc.SelectNodes("//clientAction[@name='StateSpecificVARForms']/state[@name = '" + state_name + "' and @language ='" + state_language.ToUpper().Trim() + "']");

                foreach (XmlNode node in node_list)
                {
                    if (node.Attributes["language"] != null)
                        return_val = node.Attributes[path].Value;
                }
            }
            return return_val;
        }

        /// <summary>
        /// Enable store assignment access to specific user roles like Admin, DM, HCS, DPR only
        /// </summary>
        /// <param name="user_name"></param>
        /// <returns></returns>
        public static bool isAuthorisedToChangeClinicStore(string user_name)
        {
            bool is_authorised;
            switch (user_name.ToLower())
            {
                case "admin":
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                    is_authorised = true;
                    break;
                default: is_authorised = false;
                    break;
            }

            return is_authorised;
        }

        /// <summary>
        /// Enable clinic dates below 2weeks to specific user roles like Admin, HCS, DPR only
        /// </summary>
        /// <param name="user_name"></param>
        /// <returns></returns>
        public static bool isAuthorisedForBelow2Weeks(string user_name)
        {
            bool is_authorised;
            switch (user_name.ToLower())
            {
                case "admin":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                case "regional healthcare director":
                case "regional vice president":
                    is_authorised = true;
                    break;
                default: is_authorised = false;
                    break;
            }

            return is_authorised;
        }
        #endregion ------------------------------------

        #region ---------- PRIVATE VARIABLES ---------
        private SqlConnection logConn;
        private static Configuration config;
        private static WalgreenEmail walgreenEmail;
        private static string emailUrlPath;
        #endregion
    }
}
