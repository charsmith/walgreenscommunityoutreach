﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WagPdfDictionary
/// </summary>
public class WagStringDictionary : Dictionary<string, TdWalgreens.WagPdfString>
{
    public new void Add(string key, TdWalgreens.WagPdfString wagPdfString)
    {
        if (!string.IsNullOrEmpty(wagPdfString.pdfString))
        {
            switch (key.ToLower())
            {
                case "age":
                    DateTime today = DateTime.Today;
                    DateTime birth_day = DateTime.ParseExact(wagPdfString.pdfString, "MM/dd/yyyy", new System.Globalization.CultureInfo("en-US"));
                    int current_date = (today.Year * 100 + today.Month) * 100 + today.Day;
                    int birth_date = (birth_day.Year * 100 + birth_day.Month) * 100 + birth_day.Day;
                    wagPdfString.pdfString = ((current_date - birth_date) / 10000).ToString();
                    break;
                case "gender":
                    string gender = wagPdfString.pdfString;
                    if (gender.ToLower() == "male")
                    {
                        key = "genderMale";
                        wagPdfString = new TdWalgreens.WagPdfString("x", 331, 148, 10, 1);
                    }
                    else
                    {
                        key = "genderFemale";
                        wagPdfString = new TdWalgreens.WagPdfString("x", 295, 148, 10, 1);
                    }
                    break;
                case "address":
                case "fullname":
                    if (this.ContainsKey(key))
                    {
                        wagPdfString.pdfString = this[key].pdfString + ((key == "address") ? ", " : " ") + wagPdfString.pdfString;
                    }
                    break;
                default:
                    break;
            }
            this[key] = wagPdfString;
        }
    }
}