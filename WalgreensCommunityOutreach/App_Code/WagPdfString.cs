﻿using PdfSharp.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdWalgreens
{
    public class WagPdfString
    {
        public WagPdfString(string the_pdf_string, int the_x, int the_y, double the_pt, int the_page, XColor red_brush = default(XColor), string font_name = "Arial", string font_weight = "Regular")
        {
            this.pdfString = the_pdf_string;
            this.x = the_x;
            this.y = the_y;
            this.pt = the_pt;
            this.page = the_page;
            this.fontName = font_name;
            this.fontweight = font_weight;
            this.redBrush = red_brush;
        }

        public string pdfString { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public double pt { get; set; }
        public int page { get; set; }
        public string fontName { get; set; }
        public string fontweight { get; set; }
        public XColor redBrush { get; set; }
    }
}
