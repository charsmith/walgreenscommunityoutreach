﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using System.Data;
using TdApplicationLib;
using tdEmailLib;
using System.Xml;
using System.Configuration;

namespace TdWalgreens
{
    /// <summary>
    /// Summary description for EmailOperations
    /// </summary>
    public class EmailOperations
    {
        public EmailOperations()
        {
            //
            // TODO: Add constructor logic here
            //
            this.dbOperation = new DBOperations();
            this.walgreensEmail = ApplicationSettings.emailSettings();
            this.encrypedLink = new EncryptedQueryString();
            this.emailPath = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
        }

        /// <summary>
        /// Sends Corporate clinic store assignment approved email
        /// </summary>
        /// <param name="dt_corporate"></param>
        public void sendCorporateClinicStoreAssignmentEmail(DataTable dt_corporate, int user_id, string assigned_to)
        {
            var lst_store_ids = (from r in ((DataTable)dt_corporate).AsEnumerable()
                                 where r["isStoreConfirmed"].ToString().Equals("0") && r["isChecked"].ToString().Equals("1")
                                 select r["storeId"]).Distinct().ToList();
            ApplicationSettings app_settings = new ApplicationSettings();
            string store_manager_email, pharmacy_manager_email, district_manager_email, director_retail_ops_email, healthcare_supervisor_email;

            foreach (int store_id in lst_store_ids)
            {
                DataRow[] store_rows = dt_corporate.Select("storeId = '" + store_id + "' And isStoreConfirmed = 0 AND ISNULL(isChecked, 0) = '1'");

                if (this.walgreensEmail != null)
                {
                    DataTable dt_user_emails = this.dbOperation.getStoreUserEmails(user_id, store_id);
                    pharmacy_manager_email = dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
                    store_manager_email = dt_user_emails.Rows[0]["storeManagerEmail"].ToString();
                    district_manager_email = dt_user_emails.Rows[0]["districtManagerEmail"].ToString();
                    director_retail_ops_email = dt_user_emails.Rows[0]["directorRetailOpsEmail"].ToString();
                    healthcare_supervisor_email = dt_user_emails.Rows[0]["healthcareSupervisorEmail"].ToString();

                    if (assigned_to == "store")
                    {
                        this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : ((!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "");
                        this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

                        this.emailCC = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? ((!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "") : "";
                        this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;
                        this.emailCC = (!string.IsNullOrEmpty(director_retail_ops_email)) ? this.emailCC + "," + director_retail_ops_email : this.emailCC;
                    }
                    else
                    {
                        this.emailTo = (!string.IsNullOrEmpty(district_manager_email)) ? district_manager_email : "";
                        this.emailSalutation = "District Offsite Immunization Manager";
                        this.emailSubject = "Corporate Worksite Immunization Clinic Assignment Needed";
                    }

                    this.emailCC = (!string.IsNullOrEmpty(healthcare_supervisor_email)) ? this.emailCC + "," + healthcare_supervisor_email : this.emailCC;

                    if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                    {
                        this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                        this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    }

                    foreach (DataRow row in store_rows)
                    {
                        if (assigned_to == "store")
                        {
                            this.emailBody = "<p class='assignment' align='left'><b>Client: </b>" + row["businessName"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Clinic Date: </b>" + row["naClinicDate"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Plan ID: </b>" + ((row.Table.Columns["naClinicPlanId"] != null) ? row["naClinicPlanId"].ToString() : "") + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Group ID: </b>" + ((row.Table.Columns["naClinicGroupId"] != null) ? row["naClinicGroupId"].ToString() : "") + "</p>";

                            this.emailBody += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                    <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

                            this.emailBody += "<p class='assignment' align='left'><b>Clinic Time: </b>" + row["naClinicStartTime"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Clinic Location: </b>" + row["accountAddress"].ToString() + "</p>";

                            this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaSubjectLine"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                            this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/nationalAssignmentsTemplate.htm");

                            if (!string.IsNullOrEmpty(this.emailTo))
                                this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensHome.aspx"), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), store_id.ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), this.emailPath, this.emailSubject, this.emailBody, this.emailTo, this.emailCC.TrimStart(','), true);
                        }
                        else
                        {
                            string store_instr = ((row.Table.Columns["isAddStoreInstr"] != null) ? (row["isAddStoreInstr"].ToString() == "1" ? "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'><b>The required protocol physician for this clinic will be obtained prior to the clinic date from Walgreens corporate office and does not require any action from field leadership.</b></p>" : "") : "");

                            this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/nationalLargeAccountTemplate.htm");
                            if (!string.IsNullOrEmpty(this.emailTo))
                                this.walgreensEmail.sendCorpClinicStoreAssignmentEmail(this.emailSalutation, ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensDistrictUsersHome.aspx"), store_instr, store_id.ToString(), row["naClinicDate"].ToString(), row["businessName"].ToString(), this.emailPath, this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), true);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sends Direct B2B store assignment approved email
        /// </summary>
        /// <param name="dt_corporate"></param>
        /// <param name="user_id"></param>
        /// <param name="assigned_to"></param>
        public void sendDirectB2BStoreAssignmentEmail(DataTable dt_corporate, int user_id, string assigned_to)
        {
            var lst_store_ids = (from r in ((DataTable)dt_corporate).AsEnumerable()
                                 where r["isStoreConfirmed"].ToString().Equals("False") && r["isChecked"].ToString().Equals("1")
                                 select r["storeId"]).Distinct().ToList();
            ApplicationSettings app_settings = new ApplicationSettings();
            string store_manager_email, pharmacy_manager_email, outreach_status, phone_no;

            foreach (int store_id in lst_store_ids)
            {
                DataRow[] store_rows = dt_corporate.Select("storeId = '" + store_id + "' And isStoreConfirmed = 0 AND ISNULL(isChecked, 0) = '1'");

                if (this.walgreensEmail != null)
                {
                    DataTable dt_user_emails = this.dbOperation.getStoreUserEmails(user_id, store_id);
                    pharmacy_manager_email = dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
                    store_manager_email = dt_user_emails.Rows[0]["storeManagerEmail"].ToString();

                    if (assigned_to == "store")
                    {
                        this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : ((!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "");
                        this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

                        this.emailCC = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? ((!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "") : "";
                    }

                    if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                    {
                        this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                        this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    }

                    foreach (DataRow row in store_rows)
                    {
                        if (assigned_to == "store")
                        {
                            outreach_status = row["outreachStatus"].ToString();
                            this.emailBody = "<p class='assignment' align='left'><b>Client: </b>" + row["businessName"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Status: </b>" + outreach_status.Trim() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Contact: </b>" + row["ContactName"].ToString() + "</p>";

                            this.emailBody += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                    <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

                            phone_no = (row["Phone"].ToString() != string.Empty) ? String.Format("{0:###-###-####}", Convert.ToInt64(row["Phone"].ToString())) : row["Phone"].ToString();
                            this.emailBody += "<p class='assignment' align='left'><b>Phone: </b>" + phone_no + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Industry: </b>" + row["Industry"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Employee Count: </b>" + row["actualLocationEmploymentSize"].ToString() + "</p>";

                            this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "directB2BMailBusinessSubjectLine"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                            this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/storeDirectB2BAssignmentTemplate.htm");

                            if (!string.IsNullOrEmpty(this.emailTo))
                                this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensHome.aspx"), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), store_id.ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), this.emailPath, this.emailSubject, this.emailBody, this.emailTo, this.emailCC.TrimStart(','), true, row["businessName"].ToString());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sends Local Leads store assignment approved email
        /// </summary>
        /// <param name="dt_corporate"></param>
        /// <param name="user_id"></param>
        /// <param name="assigned_to"></param>
        public void sendLocalLeadsStoreAssignmentEmail(DataTable dt_corporate, int user_id, string assigned_to)
        {
            var lst_store_ids = (from r in ((DataTable)dt_corporate).AsEnumerable()
                                 where r["isStoreConfirmed"].ToString().Equals("False") && r["isChecked"].ToString().Equals("1")
                                 select r["storeId"]).Distinct().ToList();
            ApplicationSettings app_settings = new ApplicationSettings();
            string store_manager_email, pharmacy_manager_email, outreach_status, phone_no, business_name;

            foreach (int store_id in lst_store_ids)
            {
                DataRow[] store_rows = dt_corporate.Select("storeId = '" + store_id + "' And isStoreConfirmed = 0 AND ISNULL(isChecked, 0) = '1'");

                if (this.walgreensEmail != null)
                {
                    DataTable dt_user_emails = this.dbOperation.getStoreUserEmails(user_id, store_id);
                    pharmacy_manager_email = dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
                    store_manager_email = dt_user_emails.Rows[0]["storeManagerEmail"].ToString();

                    if (assigned_to == "store")
                    {
                        this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : ((!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "");
                        this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

                        this.emailCC = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? ((!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "") : "";
                    }

                    if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                    {
                        this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                        this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    }

                    foreach (DataRow row in store_rows)
                    {
                        if (assigned_to == "store")
                        {
                            business_name = row["businessName"].ToString();
                            outreach_status = row["outreachStatus"].ToString();
                            this.emailBody = "<p class='assignment' align='left'><b>Client: </b>" + business_name + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Status: </b>" + outreach_status.Trim() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Contact: </b>" + row["ContactName"].ToString() + "</p>";

                            this.emailBody += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                    <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

                            phone_no = (row["Phone"].ToString() != string.Empty) ? String.Format("{0:###-###-####}", Convert.ToInt64(row["Phone"].ToString())) : row["Phone"].ToString();
                            this.emailBody += "<p class='assignment' align='left'><b>Phone: </b>" + phone_no + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Industry: </b>" + row["Industry"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Employee Count: </b>" + row["actualLocationEmploymentSize"].ToString() + "</p>";

                            this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "localLeadsSubjectLine"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                            this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/storeLocalLeadAssignmentTemplate.htm");

                            if (!string.IsNullOrEmpty(this.emailTo))
                                this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensHome.aspx"), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), store_id.ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), this.emailPath, this.emailSubject, this.emailBody, this.emailTo, this.emailCC.TrimStart(','), true, business_name);
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Updates store reassignment and sends revised assignment mails
        /// </summary>
        /// <param name="dt_send_revised_store_assignments"></param>
        /// <param name="store_type"></param>
        public void sendRevisedStoreAssignments(DataTable dt_send_revised_store_assignments, string store_type, int user_id)
        {
            ApplicationSettings app_settings = new ApplicationSettings();
            List<object> lst_store_ids = new List<object>();
            string store_manager_email, pharmacy_manager_email, district_manager_email, director_retail_ops_email, healthcare_supervisor_email;

            switch (store_type)
            {
                case "newStore":
                    lst_store_ids = (from r in ((DataTable)dt_send_revised_store_assignments).AsEnumerable()
                                     where r["isStoreUpdated"].ToString().Equals("1") && r["isStoreConfirmed"].ToString().Equals("1") && r["storeId"].ToString() != "-1" && r["storeId"].ToString() != r["oldStoreID"].ToString()
                                     select r["storeId"]).Distinct().ToList();
                    break;
                case "oldStore":
                    lst_store_ids = (from r in ((DataTable)dt_send_revised_store_assignments).AsEnumerable()
                                     where r["isStoreUpdated"].ToString().Equals("1") && r["isStoreConfirmed"].ToString().Equals("1") && r["storeId"].ToString() != r["oldStoreID"].ToString()
                                     select r["oldStoreID"]).Distinct().ToList();
                    break;
                case "newLeadStore":
                    lst_store_ids = (from r in ((DataTable)dt_send_revised_store_assignments).AsEnumerable()
                                     where r["isLeadStoreUpdated"].ToString().Equals("1") && r["isStoreConfirmed"].ToString().Equals("1")
                                     select r["leadStoreId"]).Distinct().ToList();
                    break;
                case "oldLeadStore": break; //todo: discuss with vishnu  
            }

            foreach (int store_id in lst_store_ids)
            {
                DataRow[] store_rows = null;
                switch (store_type)
                {
                    case "newStore":
                        store_rows = dt_send_revised_store_assignments.Select("ISNULL(isStoreUpdated, 0) = '1' And isStoreConfirmed = 1 And storeId <> '-1' And storeId = '" + store_id + "' And storeId <> oldStoreID");
                        break;
                    case "oldStore":
                        store_rows = dt_send_revised_store_assignments.Select("ISNULL(isStoreUpdated, 0) = '1' And isStoreConfirmed = 1 And oldStoreId = '" + store_id + "' And storeId <> oldStoreID");
                        break;
                    case "newLeadStore":
                        store_rows = dt_send_revised_store_assignments.Select("ISNULL(isLeadStoreUpdated, 0) = '1' And isStoreConfirmed = 1 And leadStoreId = '" + store_id + "'");
                        break;
                    //case "oldLeadStore": break; //todo: discuss with vishnu  
                }

                if (this.walgreensEmail != null)
                {
                    DataTable dt_user_emails = dbOperation.getStoreUserEmails(user_id, store_id);
                    pharmacy_manager_email = dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
                    store_manager_email = dt_user_emails.Rows[0]["storeManagerEmail"].ToString();
                    district_manager_email = dt_user_emails.Rows[0]["districtManagerEmail"].ToString();
                    //adding DPR ,HCS in cc
                    director_retail_ops_email = dt_user_emails.Rows[0]["directorRetailOpsEmail"].ToString();
                    healthcare_supervisor_email = dt_user_emails.Rows[0]["healthcareSupervisorEmail"].ToString();

                    this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                    this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

                    this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
                    this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;
                    this.emailCC = (!string.IsNullOrEmpty(director_retail_ops_email)) ? this.emailCC + "," + director_retail_ops_email : this.emailCC;
                    this.emailCC = (!string.IsNullOrEmpty(healthcare_supervisor_email)) ? this.emailCC + "," + healthcare_supervisor_email : this.emailCC;

                    if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                    {
                        this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                        this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    }

                    foreach (DataRow row in store_rows)
                    {
                        this.emailPath = string.Empty;
                        this.emailBody = string.Empty;
                        this.emailSubject = string.Empty;

                        if (store_type == "newStore")
                        {
                            this.emailBody = "<p class='assignment' align='left'><b>Client: </b>" + row["businessName"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Clinic Date: </b>" + row["naClinicDate"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Plan ID: </b>" + ((row.Table.Columns["naClinicPlanId"] != null) ? row["naClinicPlanId"].ToString() : "") + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Group ID: </b>" + ((row.Table.Columns["naClinicGroupId"] != null) ? row["naClinicGroupId"].ToString() : "") + "</p>";

                            this.emailBody += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                    <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

                            this.emailBody += "<p class='assignment' align='left'><b>Clinic Time: </b>" + row["naClinicStartTime"].ToString() + "</p>";
                            this.emailBody += "<p class='assignment' align='left'><b>Clinic Location: </b>" + row["accountAddress"].ToString() + "</p>";

                            this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaSubjectLine"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                            this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/nationalAssignmentsTemplate.htm");

                            if (!string.IsNullOrEmpty(this.emailTo))
                                this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensHome.aspx"), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), store_id.ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), this.emailPath, this.emailSubject, this.emailBody, this.emailTo, this.emailCC.TrimStart(','), true);
                        }
                        else if (store_type == "oldStore")
                        {
                            if (!string.IsNullOrEmpty(this.emailTo))
                            {
                                this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/nationalReAssignmentsTemplate.htm");

                                if (row["storeId"].ToString() != "-1")
                                {
                                    this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicStoreReAssignment"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                                    this.walgreensEmail.sendNationalLargeAccountReAssignmentEmailFromWeb(this.emailSalutation, row["businessName"].ToString(), this.emailPath, this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), true, "reassigned");
                                }
                                else
                                {
                                    this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicCancelled"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                                    this.walgreensEmail.sendNationalLargeAccountReAssignmentEmailFromWeb(this.emailSalutation, row["businessName"].ToString(), this.emailPath, this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), true, "cancelled");
                                }
                            }
                        }
                        else if (store_type == "newLeadStore")
                        {
                            this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaSubjectLine"), app_settings.removeLineBreaks(row["businessName"].ToString()));

                            if (row["storeId"].ToString() != store_id.ToString())
                            {
                                if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                                {
                                    this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                                    this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                                }

                                this.emailBody = "<p class='assignment' align='left'><b>Client: </b>" + row["businessName"].ToString() + "</p>";
                                this.emailBody += "<p class='assignment' align='left'><b>Clinic Date: </b>" + row["naClinicDate"].ToString() + "</p>";
                                this.emailBody += "<p class='assignment' align='left'><b>Plan ID: </b>" + ((row.Table.Columns["naClinicPlanId"] != null) ? row["naClinicPlanId"].ToString() : "") + "</p>";
                                this.emailBody += "<p class='assignment' align='left'><b>Group ID: </b>" + ((row.Table.Columns["naClinicGroupId"] != null) ? row["naClinicGroupId"].ToString() : "") + "</p>";

                                this.emailBody += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                    <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

                                this.emailBody += "<p class='assignment' align='left'><b>Clinic Time: </b>" + row["naClinicStartTime"].ToString() + "</p>";
                                this.emailBody += "<p class='assignment' align='left'><b>Clinic Location: </b>" + row["accountAddress"].ToString() + "</p>";

                                this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaSubjectLine"), app_settings.removeLineBreaks(row["businessName"].ToString()));

                                this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicLeadStoreAssignmentEmailSubject"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                                this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/nationalAssignmentsTemplateToLeadStore.htm");

                                //To Lead store
                                if (!string.IsNullOrEmpty(this.emailTo))
                                    this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensHome.aspx"), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), store_id.ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), this.emailPath, this.emailSubject, this.emailBody, this.emailTo, this.emailCC.TrimStart(','), true);

                                dt_user_emails = dbOperation.getStoreUserEmails(user_id, Convert.ToInt32(row["storeId"].ToString()));
                                store_manager_email = dt_user_emails.Rows[0]["storeManagerEmail"].ToString();
                                pharmacy_manager_email = dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
                                district_manager_email = dt_user_emails.Rows[0]["districtManagerEmail"].ToString();

                                this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                                this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
                                this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;
                                this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

                                //To Clinic store
                                this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicLeadStoreAssignmentEmailSubject"), app_settings.removeLineBreaks(row["businessName"].ToString()));
                                this.emailPath = HttpContext.Current.Server.MapPath("~/emailTemplates/nationalAssignmentsTemplateLeadStoreAssigned.htm");

                                if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                                {
                                    this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                                    this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                                }

                                //if (!string.IsNullOrEmpty(this.emailTo))
                                //    this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensHome.aspx"), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), store_id.ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), this.emailPath, this.emailSubject, this.emailBody, this.emailTo, this.emailCC.TrimStart(','), true);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sends Corporate clinic store reassignment email
        /// </summary>
        /// <param name="dt_user_emails"></param>
        /// <param name="client_name"></param>
        public void sendCorporateClinicStoreReassignmentEmail(DataTable dt_user_emails, string client_name)
        {
            if (this.walgreensEmail != null)
            {
                string pharmacy_manager_email, store_manager_email, district_manager_email;

                store_manager_email = dt_user_emails.Rows[0]["storeManagerEmail"].ToString();
                pharmacy_manager_email = dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
                district_manager_email = dt_user_emails.Rows[0]["districtManagerEmail"].ToString();

                this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";
                this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";

                ApplicationSettings app_setting = new ApplicationSettings();
                this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicStoreReAssignment"), app_setting.removeLineBreaks(client_name) + " Clinic");

                if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                {
                    this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                }

                if (!string.IsNullOrEmpty(this.emailTo))
                    this.walgreensEmail.sendNationalLargeAccountReAssignmentEmailFromWeb(this.emailSalutation, client_name.Trim(), HttpContext.Current.Server.MapPath("~/emailTemplates/nationalReAssignmentsTemplate.htm"), this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), true, "reassigned");
            }
        }

        /// <summary>
        /// Updates store reassignment and sends revised assignment email
        /// </summary>
        /// <param name="dt_user_emails"></param>
        /// <param name="revised_storeid"></param>
        /// <param name="business_pk"></param>
        /// <param name="client_name"></param>
        /// <param name="clinic_time"></param>
        /// <param name="clinic_full_address"></param>
        /// <param name="clinic_date"></param>
        /// <param name="plan_id"></param>
        /// <param name="group_id"></param>
        public void sendRevisedStoreAssignments(DataTable dt_user_emails, int revised_storeid, int business_pk, string client_name, string clinic_time, string clinic_full_address, string clinic_date, string plan_id, string group_id)
        {
            if (this.walgreensEmail != null)
            {
                string store_manager_email, pharmacy_manager_email, district_manager_email, director_retail_ops_email, healthcare_supervisor_email;
                store_manager_email = dt_user_emails.Rows[0]["storeManagerEmail"].ToString();
                pharmacy_manager_email = dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
                district_manager_email = dt_user_emails.Rows[0]["districtManagerEmail"].ToString();
                director_retail_ops_email = dt_user_emails.Rows[0]["directorRetailOpsEmail"].ToString();
                healthcare_supervisor_email = dt_user_emails.Rows[0]["healthcareSupervisorEmail"].ToString();


                this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";
                this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
                this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;
                this.emailCC = (!string.IsNullOrEmpty(director_retail_ops_email)) ? this.emailCC + "," + director_retail_ops_email : this.emailCC;
                this.emailCC = (!string.IsNullOrEmpty(healthcare_supervisor_email)) ? this.emailCC + "," + healthcare_supervisor_email : this.emailCC;

                if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                {
                    this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    this.emailCC = this.emailTo;
                }
                ApplicationSettings app_setting = new ApplicationSettings();
                this.emailSubject = String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaSubjectLine"), app_setting.removeLineBreaks(client_name));

                this.emailBody = "<p class='assignment' align='left'><b>Client: </b>" + client_name + "</p>";
                this.emailBody += "<p class='assignment' align='left'><b>Clinic Date: </b>" + clinic_date + "</p>";
                this.emailBody += "<p class='assignment' align='left'><b>Plan ID: </b>" + plan_id + "</p>";
                this.emailBody += "<p class='assignment' align='left'><b>Group ID: </b>" + group_id + "</p>";

                this.emailBody += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

                this.emailBody += "<p class='assignment' align='left'><b>Clinic Time: </b>" + clinic_time + "</p>";
                this.emailBody += "<p class='assignment' align='left'><b>Clinic Location: </b>" + clinic_full_address + "</p>";

                if (!string.IsNullOrEmpty(this.emailTo))
                    this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensCorporateClinicDetails.aspx", business_pk.ToString(), "0", revised_storeid.ToString()), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), revised_storeid.ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), HttpContext.Current.Server.MapPath("~/emailTemplates/nationalAssignmentsTemplate.htm"), this.emailSubject, this.emailBody, this.emailTo, this.emailCC.TrimStart(','), true);
            }
        }

        /// <summary>
        /// Sends Corporate clinic reschedule appointment emails
        /// </summary>
        /// <param name="dt_rescheduled_appts"></param>
        /// <param name="client_design_id"></param>
        public void sendRescheduleAppointmentEmail(DataTable dt_rescheduled_appts, string client_design_id)
        {
            if (this.walgreensEmail != null)
            {
                string contact_info, client_logo = string.Empty, banner_bg_color = string.Empty;
                int clinic_design_pk = 0, has_clinics = 0;

                EncryptQueryStringAES args = new EncryptQueryStringAES();
                XmlDocument clinic_scheduler_design_xml = new XmlDocument();

                Int32.TryParse(client_design_id, out clinic_design_pk);
                clinic_scheduler_design_xml.LoadXml(this.dbOperation.getCorporateSchedulerDesign(clinic_design_pk, out has_clinics));
                client_logo = clinic_scheduler_design_xml.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value;
                banner_bg_color = clinic_scheduler_design_xml.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value;
                this.emailSubject = (string)HttpContext.GetGlobalResourceObject("errorMessages", "rescheduleApptSubjectLine");

                foreach (DataRow dr_appt in dt_rescheduled_appts.Rows)
                {
                    this.emailTo = dr_appt["email"].ToString();
                    if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                        this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();

                    this.emailPath = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
                    args["arg1"] = clinic_design_pk.ToString();
                    args["arg2"] = dr_appt["apptPk"].ToString();
                    args["arg3"] = "walgreensSchedulerRegistration.aspx";
                    args["arg4"] = dr_appt["clinicPk"].ToString();
                    contact_info = "";
                    foreach (KeyValuePair<string, string> reg_details in ApplicationSettings.getSchedulerApptFields)
                    {
                        if (dt_rescheduled_appts.Columns[reg_details.Key] != null)
                        {
                            if (dr_appt[reg_details.Key] != DBNull.Value && !string.IsNullOrEmpty(dr_appt[reg_details.Key].ToString()))
                                contact_info += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>" + reg_details.Value + ": " + dr_appt[reg_details.Key].ToString() + "</p>";
                        }
                    }

                    this.emailBody = "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Appointment:";
                    this.emailBody = dr_appt["apptDate"].ToString() + " " + dr_appt["apptAvlTimes"].ToString() + "</p>";
                    this.emailBody += "<p style='color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; -ms-word-break: none; word-break: none; // non standard for webkitword-break: break-word; -webkit-hyphens: none; -moz-hyphens: none; hyphens: none; margin: 0 0 10px; padding: 0;' align='left'>Location:" + dr_appt["clinicAddress"].ToString() + "<br /></p>";

                    this.walgreensEmail.sendRescheduleAppointmentEmail(this.emailPath + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + args.ToString(), HttpContext.Current.Server.MapPath("~/emailTemplates/walgreensSchedulerClinicRevisionEmail.html"), this.emailSubject, dr_appt["confirmationId"].ToString(), contact_info, this.emailBody, this.emailTo, "", true, this.emailPath + ApplicationSettings.EmailLogoImagePath() + "/" + client_logo, banner_bg_color);
                }
            }
        }

        /// <summary>
        /// Sends rescheduled appointment email to canceled clinic appointments
        /// </summary>
        /// <param name="dt_cancel_appts"></param>
        public void sendCancelClinicApptsEmail(DataTable dt_cancel_appts)
        {
            if (this.walgreensEmail != null)
            {
                string email_body, subject_line, email_path, email_to, client_logo = string.Empty, banner_bg_color = string.Empty;
                int clinic_design_pk = 0, has_clinics = 0;
                EncryptQueryStringAES args = new EncryptQueryStringAES();

                XmlDocument clinic_scheduler_design_xml = new XmlDocument();
                clinic_design_pk = Convert.ToInt32(dt_cancel_appts.Rows[0]["clinicDesignPk"]);
                clinic_scheduler_design_xml.LoadXml(this.dbOperation.getCorporateSchedulerDesign(clinic_design_pk, out has_clinics));
                client_logo = clinic_scheduler_design_xml.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value;
                banner_bg_color = clinic_scheduler_design_xml.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value;

                foreach (DataRow dr_appt in dt_cancel_appts.Rows)
                {
                    email_to = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dr_appt["apptEmail"].ToString());
                    email_path = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
                    args["arg1"] = clinic_design_pk.ToString();
                    args["arg2"] = dr_appt["apptPk"].ToString();
                    args["arg3"] = "walgreensSchedulerRegistration.aspx";
                    args["arg4"] = dr_appt["clinicPk"].ToString();

                    subject_line = (string)HttpContext.GetGlobalResourceObject("errorMessages", "rescheduleApptSubjectLine");

                    email_body = "The <b> &lt;" + dr_appt["clinicName"].ToString() + "&gt; </b> has been cancelled.  Please check the scheduling site for another possible clinic location.";

                    this.walgreensEmail.sendRescheduleApptToCancelledClinicEmail(email_path + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + args.ToString(), HttpContext.Current.Server.MapPath("~/emailTemplates/canceledClinicEmail.html"), subject_line, email_body, email_to, "", true, email_path + ApplicationSettings.EmailLogoImagePath() + "/" + client_logo, banner_bg_color);
                }
            }
        }

        /// <summary>
        /// Sends clinic cancellation notification to admin
        /// </summary>
        /// <param name="dt_notify_users"></param>
        /// <param name="is_clinic_past"></param>
        /// <param name="cancelled_by"></param>
        public void sendClinicCancelNotificationToAdmin(DataTable dt_notify_users, bool is_clinic_past, string cancelled_by, bool is_admin)
        {
            if (this.walgreensEmail != null)
            {
                string to_email = string.Empty;

                if (!is_clinic_past)
                    to_email += ApplicationSettings.getEmailInfoFromGroup("deleteContractAgreementNotificationGroup", true).Trim();
                else
                    to_email += ApplicationSettings.getEmailInfoFromGroup("ClinicCancelledPastClinicDateNotificationGroup", true).Trim();

                string[] notify_users = to_email.Split(',');

                foreach (string notify_to in notify_users)
                {
                    if (is_clinic_past)
                        walgreensEmail.sendClinicCancelledPastClinicDateNotificationEmail("Admin", cancelled_by, dt_notify_users.Rows[0]["clinicName"].ToString(), HttpContext.Current.Server.MapPath("~/emailTemplates/schedulerClinicPastClinicDateCancellationEmail.htm"), (string)HttpContext.GetGlobalResourceObject("errorMessages", "clinicCancellationNotifySubjectLine"), dt_notify_users.Rows[0]["clinicStoreId"].ToString(), dt_notify_users.Rows[0]["clinicDate"].ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : notify_to), "", true);
                    else if (!is_admin)
                        walgreensEmail.sendClinicCancelledNotificationEmail("Admin", ApplicationSettings.encryptedLink(notify_to, "walgreensLandingPage.aspx"), HttpContext.Current.Server.MapPath("~/emailTemplates/nationalLargeAccountCancellationTemplate.htm"), (string)HttpContext.GetGlobalResourceObject("errorMessages", "clinicCancellationNotifySubjectLine"), dt_notify_users.Rows[0]["clinicStoreId"].ToString(), dt_notify_users.Rows[0]["clinicName"].ToString(), dt_notify_users.Rows[0]["clinicDate"].ToString(), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : notify_to), "", true);
                }
            }
        }

        /// <summary>
        /// Sends Community Off-site Clinic Agreement to Business Users
        /// </summary>
        /// <param name="emails"></param>
        /// <param name="contact_pk"></param>
        /// <param name="is_current_season"></param>
        /// <returns></returns>
        public bool sendClinicAgreementToBusinessUsers(string emails, int contact_pk, bool is_current_season, string account_type = "Local")
        {
            bool is_email_sent = false;
            string[] user_emails = emails.Replace(";", ",").Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (user_emails.Count() > 0)
            {
                if (this.walgreensEmail != null)
                {
                    foreach (string str_email in user_emails)
                    {
                        this.encrypedLink["arg1"] = str_email.Trim();
                        this.encrypedLink["arg2"] = contact_pk.ToString();

                        if (!is_current_season)
                            this.encrypedLink["arg3"] = "BusinessUser";

                        if (account_type == "Local")
                            this.walgreensEmail.sendOffSiteAggreementMailToBusinessUser(this.emailPath + (is_current_season ? "walgreensClinicAgreementForUser.aspx?args=" : "walgreensClinicAgreementPrevSeason.aspx?args=") + encrypedLink.ToString(), HttpContext.Current.Server.MapPath("~/emailTemplates/agreementToBusinessUsersTemplate.htm"), (string)HttpContext.GetGlobalResourceObject("errorMessages", "walgreensCAToBusinessUserSubject"), str_email.Trim(), true);
                        else if (account_type == "VoteVax")
                            this.walgreensEmail.sendOffSiteAggreementMailToBusinessUser(this.emailPath + "walgreensVoteVaxAgreementForUser.aspx?args=" + this.encrypedLink.ToString(), HttpContext.Current.Server.MapPath("~/emailTemplates/agreementToBusinessUsersTemplate.htm"), (string)HttpContext.GetGlobalResourceObject("errorMessages", "walgreensVoteVaxCAToBusinessUserSubject"), str_email.Trim(), true);

                        if (!is_email_sent)
                        {
                            this.dbOperation.setContractAgreementReviewedStatus(contact_pk, 1);
                            is_email_sent = true;
                        }
                    }
                }
            }

            return is_email_sent;
        }

        /// <summary>
        /// Sends Community Off-site Clinic Agreement to Walgreens Users
        /// </summary>
        /// <param name="pharmacy_user_email"></param>
        /// <param name="store_manager_email"></param>
        /// <param name="business_name"></param>
        /// <param name="contact_log_pk"></param>
        /// <param name="attachment_instr"></param>
        /// <param name="account_type"></param>
        /// <param name="regional_name"></param>
        /// <param name="store_id"></param>
        /// <param name="attachment_files"></param>
        public void sendClinicAgreementToWalgreensUser(string pharmacy_user_email, string store_manager_email, string business_name, int contact_log_pk, string attachment_instr, string account_type = "Local", string regional_name = "Pharmacy Manager", int store_id = -1, List<string> attachment_files = null)
        {
            string str_user_email = (!string.IsNullOrEmpty(pharmacy_user_email)) ? pharmacy_user_email : store_manager_email;
            this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";

            if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 && store_manager_email.Length > 0)
            {
                this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
            }

            //string email_template = account_type == "Local" ? "agreementToWalgreensUserTemplate.htm" : "agreementToWalgreensUserTemplate.htm";
            if (this.walgreensEmail != null && !string.IsNullOrEmpty(str_user_email))
            {
                foreach (string user_email in str_user_email.Split(','))
                {
                    this.emailTo = user_email;
                    if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                    {
                        this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    }
                    this.walgreensEmail.sendOffSiteAggreementMailToWalgreensUser(business_name,
                        store_id != -1 ? ApplicationSettings.encryptedLinkWithFiveArgs(user_email, "walgreensLandingPage.aspx", account_type == "Local" ? "walgreensClinicAgreement.aspx" : "walgreensVoteVaxAgreement.aspx", contact_log_pk.ToString(), store_id.ToString()) :
                        ApplicationSettings.encryptedLinkWithFourArgs(user_email, "walgreensLandingPage.aspx", account_type == "Local" ? "walgreensClinicAgreement.aspx" : "walgreensVoteVaxAgreement.aspx", contact_log_pk.ToString()),
                        HttpContext.Current.Server.MapPath("~/emailTemplates/agreementToWalgreensUserTemplate.htm"),
                        (string)HttpContext.GetGlobalResourceObject("errorMessages", account_type == "Local" ? "walgreensClinicAgreementEmailSubject" : "walgreensVoteVaxClinicAgreementEmailSubject"),
                        this.emailTo, this.emailCC, true, attachment_instr, regional_name, attachment_files);
                }
            }
        }

        /// <summary>
        /// Sends auto geo-code store reassignment email to pharmacy managers
        /// </summary>
        /// <param name="dt_user_emails"></param>
        /// <param name="dt_reassigned_clinics"></param>
        public void sendClinicStoreAutoGeoCodeReassignmentEmail(DataTable dt_user_emails, DataTable dt_reassigned_clinics, string account_type = "Local")
        {
            string store_manager_email, pharmacy_manager_email, district_manager_email, hcs_email;

            store_manager_email = (dt_user_emails.Rows[0]["storeManagerEmail"] != DBNull.Value) ? dt_user_emails.Rows[0]["storeManagerEmail"].ToString() : string.Empty;
            pharmacy_manager_email = (dt_user_emails.Rows[0]["pharmacyManagerEmail"] != DBNull.Value) ? dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString() : string.Empty;
            district_manager_email = (dt_user_emails.Rows[0]["districtManagerEmail"] != DBNull.Value) ? dt_reassigned_clinics.Rows[0]["districtManagerEmail"].ToString() : string.Empty;
            hcs_email = (dt_user_emails.Rows[0]["healthcareSupervisorEmail"] != DBNull.Value) ? dt_reassigned_clinics.Rows[0]["healthcareSupervisorEmail"].ToString() : string.Empty;

            this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
            this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

            this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
            this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;
            this.emailCC = (!string.IsNullOrEmpty(hcs_email)) ? this.emailCC + "," + hcs_email : this.emailCC;

            if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
            {
                this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
            }

            string email_template = account_type == "Local" ? "clinicStoreReAssignmentsTemplate.htm" : "clinicStoreReAssignmentsTemplate.htm";
            if (this.walgreensEmail != null && !string.IsNullOrEmpty(this.emailTo))
            {
                foreach (DataRow row in dt_reassigned_clinics.Rows)
                {
                    this.emailSubject = account_type == "Local" ? string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicStoreReAssignment"), row["businessName"].ToString()) : string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteVoteVaxClinicStoreReAssignment"), row["businessName"].ToString());
                    this.walgreensEmail.sendAutoGeocodeClinicStoreReAssignmentEmail(this.emailSalutation, row["businessName"].ToString(), row["clinicStoreId"].ToString(), HttpContext.Current.Server.MapPath("~/emailTemplates/" + email_template), this.emailSubject, this.emailTo, this.emailCC, true);
                }
            }
        }

        /// <summary>
        /// Sends clinic details changed email
        /// </summary>
        /// <param name="dt_default_clinic_user"></param>
        /// <param name="clinic_type"></param>
        /// <param name="email_body"></param>
        /// <param name="billing_email"></param>
        /// <param name="client_name"></param>
        /// <param name="business_clinic_pk"></param>
        /// <param name="contact_log_pk"></param>
        /// <param name="store_id"></param>
        /// <param name="send_emailto_clinical_contract"></param>
        public void sendClinicDetailsChangedEmail(DataTable dt_default_clinic_user, string clinic_type, string email_body, string billing_email, string client_name, string business_clinic_pk, string contact_log_pk, string store_id, bool send_emailto_clinical_contract)
        {
            string pharmacy_manager_email, store_manager_email, district_manager_email, encryp_url = string.Empty, email_template;

            pharmacy_manager_email = (dt_default_clinic_user.Rows[0]["pharmacyManagerEmail"] != DBNull.Value) ? dt_default_clinic_user.Rows[0]["pharmacyManagerEmail"].ToString() : string.Empty;
            store_manager_email = (dt_default_clinic_user.Rows[0]["storeManagerEmail"] != DBNull.Value) ? dt_default_clinic_user.Rows[0]["storeManagerEmail"].ToString() : string.Empty;
            district_manager_email = (dt_default_clinic_user.Rows[0]["districtManagerEmail"] != DBNull.Value) ? dt_default_clinic_user.Rows[0]["districtManagerEmail"].ToString() : string.Empty;

            this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
            this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";
            this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
            this.emailCC = (!string.IsNullOrEmpty(district_manager_email) && clinic_type == "corporate") ? this.emailCC + "," + district_manager_email : this.emailCC;

            if (clinic_type == "Local")
                encryp_url = ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensLocalClinicDetails.aspx", business_clinic_pk, contact_log_pk, store_id);
            else if (clinic_type == "Charity")
                encryp_url = ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensCharityProgramClinicDetails.aspx", business_clinic_pk, contact_log_pk, store_id);
            else if (clinic_type == "Corporate")
                encryp_url = ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensCorporateClinicDetails.aspx", business_clinic_pk, contact_log_pk, store_id);
            else if (clinic_type == "VoteVax")
                encryp_url = ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensVoteVaxClinicDetails.aspx", business_clinic_pk, contact_log_pk, store_id);
            else if (clinic_type == "Community Outreach")
                encryp_url = ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensCommOutreachClinicDetails.aspx", business_clinic_pk, contact_log_pk, store_id);

            if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
            {
                this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
            }

            if (this.walgreensEmail != null)
            {
                //Revised Clinic Details email
                if (!string.IsNullOrEmpty(email_body))
                {
                    email_template = clinic_type == "VoteVax" ? "clinicDetailsChangesTemplate.htm" : "clinicDetailsChangesTemplate.htm";
                    this.emailSubject = (clinic_type == "VoteVax" ? "Clinic Details Have Changed for the " : "Clinic Details Have Changed for the ") + new ApplicationSettings().removeLineBreaks(client_name) + " Clinic";
                    this.walgreensEmail.sendClinicDetailsChangesEmail(this.emailSalutation, encryp_url, email_body, client_name, HttpContext.Current.Server.MapPath("~/emailTemplates/" + email_template), this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), null, true);
                }

                //Updated Clinic Billing Information email
                //if (!string.IsNullOrEmpty(billing_email))
                //{
                //    if (send_emailto_clinical_contract)
                //    {
                //        clinical_contracts_email = ConfigurationManager.AppSettings["emailToClinicalContracts"].ToString();
                //        if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                //            clinical_contracts_email = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                //    }

                //    this.emailSubject = "Clinic Billing Information Have Changed for the " + new ApplicationSettings().removeLineBreaks(client_name) + " Clinic";
                //    this.walgreensEmail.sendClinicDetailsChangesEmail(this.emailSalutation, encryp_url, billing_email, client_name, HttpContext.Current.Server.MapPath("~/emailTemplates/clinicBillingInfoChangesTemplate.htm"), this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), clinical_contracts_email, true);
                //}
            }
        }

        /// <summary>
        /// Sends Billing Code Updates Email Alert to All Clinics Associated with a Contract or Charity Program - TDirect - 10759
        /// </summary>
        /// <param name="clinic_type"></param>
        /// <param name="email_body"></param>
        /// <param name="billing_email"></param>
        /// <param name="client_name"></param>
        /// <param name="business_clinic_pk"></param>
        /// <param name="contact_log_pk"></param>
        /// <param name="send_emailto_clinical_contract"></param>
        public void sendUpdateBillingInfoEmailToAllClinics(string clinic_type, string email_body, string billing_email, string client_name, int business_clinic_pk, string contact_log_pk, bool send_emailto_clinical_contract)
        {
            string pharmacy_manager_email, store_manager_email, district_manager_email, clinical_contracts_email = string.Empty, encryp_url = string.Empty;

            DataTable dt_clinic_users = new DataTable();
            dt_clinic_users = this.dbOperation.getClinicStoreUserEmails(business_clinic_pk);

            if (dt_clinic_users != null && dt_clinic_users.Rows.Count > 0)
            {
                foreach (DataRow clinic_store in dt_clinic_users.Rows)
                {
                    pharmacy_manager_email = (clinic_store["pharmacyManagerEmail"] != DBNull.Value) ? clinic_store["pharmacyManagerEmail"].ToString() : string.Empty;
                    store_manager_email = (clinic_store["storeManagerEmail"] != DBNull.Value) ? clinic_store["storeManagerEmail"].ToString() : string.Empty;
                    district_manager_email = (clinic_store["districtManagerEmail"] != DBNull.Value) ? clinic_store["districtManagerEmail"].ToString() : string.Empty;

                    this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                    this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";
                    this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
                    this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;

                    if (clinic_type == "Local")
                        encryp_url = ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensLocalClinicDetails.aspx", clinic_store["clinicPk"].ToString(), contact_log_pk, clinic_store["storeID"].ToString());
                    else if (clinic_type == "Charity")
                        encryp_url = ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensCharityProgramClinicDetails.aspx", clinic_store["clinicPk"].ToString(), contact_log_pk, clinic_store["storeID"].ToString());

                    if (this.walgreensEmail != null)
                    {
                        if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                        {
                            this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                            this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                        }

                        //Updated Clinic Billing Information email
                        if (!string.IsNullOrEmpty(billing_email))
                        {
                            if (send_emailto_clinical_contract)
                            {
                                clinical_contracts_email = ConfigurationManager.AppSettings["emailToClinicalContracts"].ToString();
                                if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                                    clinical_contracts_email = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                            }

                            this.emailSubject = "Clinic Billing Information Have Changed for the " + new ApplicationSettings().removeLineBreaks(client_name) + " Clinic";
                            this.walgreensEmail.sendClinicDetailsChangesEmail(this.emailSalutation, encryp_url, billing_email, client_name, HttpContext.Current.Server.MapPath("~/emailTemplates/clinicBillingInfoChangesTemplate.htm"), this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), clinical_contracts_email, true);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sends local clinic store reassignment email
        /// </summary>
        /// <param name="dt_user_emails"></param>
        /// <param name="dt_reassigned_clinics"></param>
        public void sendLocalClinicStoreReassignmentEmail(DataTable dt_user_emails, DataTable dt_reassigned_clinics, string account_type = "Local")
        {
            string store_manager_email, pharmacy_manager_email, hcs_email;

            pharmacy_manager_email = (dt_user_emails.Rows[0]["pharmacyManagerEmail"] != DBNull.Value) ? dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString() : string.Empty;
            store_manager_email = (dt_user_emails.Rows[0]["storeManagerEmail"] != DBNull.Value) ? dt_user_emails.Rows[0]["storeManagerEmail"].ToString() : string.Empty;
            //district_manager_email = (dt_reassigned_clinics.Rows[0]["districtManagerEmail"] != DBNull.Value) ? dt_reassigned_clinics.Rows[0]["districtManagerEmail"].ToString() : string.Empty;
            hcs_email = (dt_user_emails.Rows[0]["healthcareSupervisorEmail"] != DBNull.Value) ? dt_user_emails.Rows[0]["healthcareSupervisorEmail"].ToString() : string.Empty;

            this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
            this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

            this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
            //this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;
            this.emailCC = (!string.IsNullOrEmpty(hcs_email)) ? this.emailCC + "," + hcs_email : this.emailCC;

            if (this.walgreensEmail != null && !string.IsNullOrEmpty(this.emailTo))
            {
                if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                {
                    this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                }

                foreach (DataRow row in dt_reassigned_clinics.Rows)
                {
                    this.emailSubject = account_type == "Local" ? string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicStoreReAssignment"), row["businessName"].ToString()) : string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteVoteVaxClinicStoreReAssignment"), row["businessName"].ToString());
                    if (Convert.ToBoolean(row["isStoreChangedManual"].ToString()))
                        this.walgreensEmail.sendNationalLargeAccountReAssignmentEmailFromWeb(this.emailSalutation, row["businessName"].ToString(), account_type == "Local" ? HttpContext.Current.Server.MapPath("~/emailTemplates/localReAssignmentsTemplate.htm") : HttpContext.Current.Server.MapPath("~/emailTemplates/localReAssignmentsTemplate.htm"), this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), true, "reassigned");
                    else
                        this.walgreensEmail.sendAutoGeocodeClinicStoreReAssignmentEmail(this.emailSalutation, row["businessName"].ToString(), row["clinicStoreId"].ToString(), account_type == "Local" ? HttpContext.Current.Server.MapPath("~/emailTemplates/clinicStoreReAssignmentsTemplate.htm") : HttpContext.Current.Server.MapPath("~/emailTemplates/clinicStoreReAssignmentsTemplate.htm"), this.emailSubject, this.emailTo, this.emailCC.TrimStart(','), true);
                }
            }
        }

        /// <summary>
        /// Sends local clinic store assigned email
        /// </summary>
        /// <param name="dt_reassigned_clinics"></param>
        public void sendLocalClinicStoreAssignmentEmail(DataTable dt_reassigned_clinics, string contact_log_pk, string account_type = "Local")
        {
            string store_manager_email, pharmacy_manager_email, hcs_email, contracted_store, clinic_time, email_template;
            if (this.walgreensEmail != null)
            {
                foreach (DataRow row in dt_reassigned_clinics.Rows)
                {
                    pharmacy_manager_email = (row["pharmacyManagerEmail"] != DBNull.Value) ? row["pharmacyManagerEmail"].ToString() : string.Empty;
                    store_manager_email = (row["storeManagerEmail"] != DBNull.Value) ? row["storeManagerEmail"].ToString() : string.Empty;
                    //district_manager_email = (row["districtManagerEmail"] != DBNull.Value) ? row["districtManagerEmail"].ToString() : string.Empty;
                    hcs_email = (row["healthcareSupervisorEmail"] != DBNull.Value) ? row["healthcareSupervisorEmail"].ToString() : string.Empty;

                    this.emailTo = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                    this.emailCC = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
                    //this.emailCC = (!string.IsNullOrEmpty(district_manager_email)) ? this.emailCC + "," + district_manager_email : this.emailCC;
                    this.emailCC = (!string.IsNullOrEmpty(hcs_email)) ? this.emailCC + "," + hcs_email : this.emailCC;

                    this.emailSalutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";

                    if (ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0)
                    {
                        this.emailTo = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                        this.emailCC = ConfigurationManager.AppSettings["emailSendTo"].ToString();
                    }

                    if (!string.IsNullOrEmpty(this.emailTo))
                    {
                        clinic_time = row["clinicStartTime"].ToString();
                        clinic_time = (!string.IsNullOrEmpty(clinic_time)) ? clinic_time + ((!string.IsNullOrEmpty(row["clinicEndTime"].ToString())) ? " - " + row["clinicEndTime"].ToString() : "") : clinic_time + row["clinicEndTime"].ToString().Trim();

                        //email_body = "<table width='100%' style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #000000; height: 20px;' cellspacing='0' cellpadding='0'><tr><td style='border: 1px solid black;'><b> Business Account Name </b></td> <td style='border: 1px solid black;'><b> Full Address </b></td><td style='border: 1px solid black;'><b> Clinic Date </b></td><td style='border: 1px solid black;'><b> Clinic Time </b></td></tr>";
                        //email_body += "<tr><td style='border: 1px solid black; height: 20px;'> " + row["businessName"].ToString() + "</td> <td style='border: 1px solid black;'>" + row["clinicAddress"].ToString() + " </td><td style='border: 1px solid black;'> " + row["clinicDate"].ToString() + " </td><td style='border: 1px solid black;'> " + clinic_time + " </td></tr>";
                        //email_body += "</tabel>";

                        this.emailBody = "<p class='assignment' align='left'><b>Client: </b>" + row["businessName"].ToString() + "</p>";
                        this.emailBody += "<p class='assignment' align='left'><b>Clinic Date: </b>" + row["clinicDate"].ToString() + "</p>";
                        this.emailBody += "<p class='assignment' align='left'><b>Plan ID: </b>" + row["naClinicPlanId"].ToString() + "</p>";
                        this.emailBody += "<p class='assignment' align='left'><b>Group ID: </b>" + row["naClinicGroupId"].ToString() + "</p>";

                        this.emailBody += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
                                <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

                        this.emailBody += "<p class='assignment' align='left'><b>Clinic Time: </b>" + clinic_time + "</p>";
                        this.emailBody += "<p class='assignment' align='left'><b>Clinic Location: </b>" + row["clinicAddress"].ToString() + "</p>";

                        if (account_type == "VoteVax")
                        {
                            this.emailSubject = string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteVoteVaxClinicStoreReAssignment"), row["businessName"].ToString());
                            contracted_store = string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "voteVaxClinicAssignmentContractedStoreId"), row["contractedClinicStoreId"].ToString());
                            email_template = "localAssignmentsTemplate.htm";
                        }
                        else if (account_type == "Community Outreach")
                        {
                            this.emailSubject = string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicStoreReAssignment"), row["businessName"].ToString());
                            contracted_store = string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "coClinicAssignmentContractedStoreId"), row["contractedClinicStoreId"].ToString());
                            email_template = "localCOAssignmentsTemplate.htm";
                        }
                        else
                        {
                            this.emailSubject = string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "worksiteClinicStoreReAssignment"), row["businessName"].ToString());
                            contracted_store = string.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "localClinicAssignmentContractedStoreId"), row["contractedClinicStoreId"].ToString());
                            email_template = "localAssignmentsTemplate.htm";
                        }

                        if (!string.IsNullOrEmpty(this.emailTo))
                            this.walgreensEmail.sendLocalClinicAssignmentEmailFromWeb(this.emailSalutation, ApplicationSettings.encryptedLinkWithSixArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensLocalClinicDetails.aspx", row["clinicPk"].ToString(), contact_log_pk.Trim(), row["clinicStoreId"].ToString()), String.Format((string)HttpContext.GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), HttpContext.Current.Server.MapPath("~/emailTemplates/" + email_template), ApplicationSettings.encryptedLinkWithThreeArgs(this.emailTo, "walgreensLandingPage.aspx", "walgreensJobAids.aspx"), this.emailSubject, this.emailBody, contracted_store, this.emailTo, this.emailCC.TrimStart(','), true);
                    }
                }
            }
        }

        #region ---------- PRIVATE VARIABLES-----------
        private DBOperations dbOperation = null;
        private WalgreenEmail walgreensEmail = null;
        private string emailPath = string.Empty;
        private EncryptedQueryString encrypedLink = null;
        private string emailTo;
        private string emailCC;
        private string emailSubject;
        private string emailBody;
        private string emailSalutation;
        #endregion
    }
}