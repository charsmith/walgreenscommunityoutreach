﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensBestPractices.aspx.cs"
    Inherits="walgreensBestPractices" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#popupDialog").hide();
        });

        function showPdf(file_type, width, height) {
            if (confirm('The Immunizations Sales Sheet was sent in Sign Pack 25 and will be in stores on 6/18/14.')) {
                window.open('controls/ResourceFileHandler.ashx?Path=2014_15/14WG0020_SellSheet_English.pdf', '_blank');
            }
        }

    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td colspan="2" valign="top" class="pageTitle">
                            Small Business Outreach Efforts: Best Practices
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="598">
                            <table width="100%" border="0" cellspacing="12">
                                <tr>
                                    <td width="715" colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Prepare</span>
                                        <ul>
                                            <li>Review the content of the small business collateral materials and walgreensflu.com,
                                                in detail</li>
                                            <li>Print copies of the Immunization Solutions sell sheet to bring to potential clients</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Practice (See sample call script below) </span>
                                        <ul>
                                            <li>In opening a phone conversation, identify your name, title and store location. Immediately
                                                identify your purpose for calling, explaining the Walgreens Immunization program
                                                and how their business can benefit from Flu as well as other immunization services</li>
                                            <li>Ask for a few minutes of their very busy time and be prepared to stress the important
                                                messages in the Immunization Solution Sell sheet as to why Walgreens is the ideal
                                                provider for a small business such as theirs</li>
                                            <li>Ask if they‘ve ever explored options to provide an onsite benefit for their employees.
                                                Refer them to Walgreensflu.com to understand the benefit of immunizing their employee</li>
                                            <li>Let them know we can bill an existing employee benefit, or set-up an employer-funded
                                                program. </li>
                                            <li>Attempt to schedule a follow-up appointment to review the materials and program
                                                options.</li>
                                            <li>If a formal meeting is not a possible, try to schedule a future follow-up phone
                                                call and mail the Immunization Solution Sell sheet to the business.</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Provide Updates</span>
                                        <ul>
                                            <li>Be proactive and provide updates as you go. District and Region leadership will
                                                be checking the status on your outreach efforts.</li>
                                            <li>Consider splitting your business list between store and pharmacy management. Work
                                                together on completing the initial outreach efforts and share ‘Best Practices’ on
                                                what approach worked best.</li>
                                            <li>Schedule weekly meetings to review past week’s status and develop a completion plan
                                                to reach out to remaining businesses.</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Additional Best Practices</span>
                                        <ul>
                                            <li>Extend outreach efforts beyond the list of businesses provided in your community.
                                                Additional collateral material is available to print on StoreNet and the Opportunity
                                                Directory</li>
                                            <li>Review the list of corporately contracted worksites to prevent duplication of efforts.</li>
                                            <li>Work with your community leader to keep track of businesses that have been contacted
                                                by store managers in your community to decrease duplicate efforts and avoid confusion</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-top: 12px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="12">
                                            <tr>
                                                <td width="715" align="left" valign="top" class="resourcesTitle">
                                                    Marketing Materials
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="715" colspan="2" align="left" valign="top" class="class3">
                                                    <a href="#callScript">Sample Call Script</a>
                                                    <p>
                                                        <a href="javascript:showPdf('Immunizations Sales Sheet', 300, 140);">Immunizations Sales
                                                            Sheet (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=SmallBusinessThankYouLetter.pdf"
                                                            target="_blank">Small Business Thank You Letter (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/370_1713-2_Flu Dosing Formrv5.pdf"
                                                            target="_blank">Key Facts About Available Vaccines (pdf)</a></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="715" align="left" valign="top" class="resourcesTitle">
                                                    How To Guides
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="715" colspan="2" align="left" valign="top" class="class3">
                                                    <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Add a Business and Contract to Outreach Tool.pdf"
                                                        target="_blank">Add a Business and Contract to the Outreach Tool (pdf)</a>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Corporate Immunization Clinics Online Scheduler Job Aid.pdf"
                                                            target="_blank">Corporate Immunization Clinics Online Scheduler Job Aid (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/How to Assign Clinics in Multiple Districts.pdf"
                                                            target="_blank">How to Assign Clinics in Multiple Districts (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/How to Change a Clinic Status to Completed.pdf"
                                                            target="_blank">How to Change a Clinic to Completed (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/How To Confirm a Corporate Clinic after Store been Assigned.pdf"
                                                            target="_blank">How to Confirm a Corporate Clinic after Store has been Assigned
                                                            (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/How to Delete a Contract.pdf"
                                                            target="_blank">How to Delete a Contract (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/How-To Guide Chairty Program.pdf"
                                                            target="_blank">How To Guide - Charity Program (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/How To Request a Direct Bill Voucher.pdf"
                                                            target="_blank">How To Request a Direct Bill Voucher (pdf)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Store to Invoice Template - Community Offsites.pdf"
                                                            target="_blank">Store to Invoice Template - Community Offsites (pdf)</a></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <a name="callScript" id="callScript"></a>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="20">
                                            <tr>
                                                <td width="715" align="left" valign="top" class="bestPracticesSubTitle">
                                                    Sample Call Script
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="715" colspan="2" align="left" valign="top" class="bestPracticesText">
                                                    <span class="bestPracticesTopic">Initial contact with business:</span>
                                                    <ul>
                                                        <li>Hi, my name is _____and I’m a Walgreens Community Leader/Store Manager/Pharmacy
                                                            Manager at ___ (intersection of your store).</li>
                                                    </ul>
                                                    <ul>
                                                        <li>Can I please speak to ______ (contact name provided in business directory)</li>
                                                    </ul>
                                                    <ul>
                                                        <li>I’d like to tell you about our flu shot clinic program and other available immunizations
                                                            for small businesses. </li>
                                                    </ul>
                                                    <ul>
                                                        <li>Ask for a few minutes of their very busy time and be prepared to stress four important
                                                            reasons why Walgreens is the ideal flu shot provider for a small business:
                                                            <ul style="list-style-type: circle;">
                                                                <li>We offer a flu shot service, which can be coupled with other immunization services.</li>
                                                                <li>With Walgreens immunization solutions, you’ll have convenient access to preventive
                                                                    care for employees, spouses and dependents.</li>
                                                                <li>Walgreens may be able to process immunizations as a pharmacy benefit or a medical
                                                                    benefit.</li>
                                                                <li>Online technologies allow you to easily schedule on-site health center events or
                                                                    create custom marketing materials, such as posters and flyers, to promote this new
                                                                    benefit.</li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    <ul>
                                                        <li>If they have time to talk, continue with the conversation below. If they do not
                                                            have time, attempt to schedule a follow up appointment to review the materials and
                                                            program options. If a formal meeting is not possible, try to schedule a future follow-up
                                                            call.</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                                    <p>
                                                        <span class="bestPracticesTopic">If the employer agrees to conversation, continue through
                                                            remaining questions below</span>.</p>
                                                    <p>
                                                        Let them know we can bill an existing employee benefit, or set-up an employer-funded
                                                        program.
                                                    </p>
                                                    <ul>
                                                        <li><strong>Insurance Billing:</strong> Walgreens can bill patient’s insurance directly
                                                            and the patient would be responsible for the copay. Walgreens can bill many pharmacy
                                                            and medical plans.</li></ul>
                                                    <ul>
                                                        <li><strong>Cash Payment:</strong> Payment for the vaccine can be collected from the
                                                            patient receiving the immunization at the time of vaccination by cash or via check
                                                            from the employer/location.</li></ul>
                                                    <ul>
                                                        <li><strong>Direct Bill:</strong> Employer/Location will pay for employee flu shots.
                                                            They will receive an invoice directly from Walgreens for the number of immunizations
                                                            provided. No payment will be collected at the time of the immunization.</li></ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                                    <span class="bestPracticesTopic">Ask them if they are interested in proceeding with
                                                        Walgreens Immunizations Clinic.</span>
                                                    <ul>
                                                        <li>If yes, explain the process of completing the community off-site agreement and next
                                                            steps.</li>
                                                    </ul>
                                                    <ul>
                                                        <li>If no, refer them to WalgreensFlu.com to try our new flu ROI estimator tool and
                                                            review the FAQs on the site explaining Walgreens program.</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                                    <span class="bestPracticesTopic">Provide your contact information for future follow-up
                                                        or questions and thank them for their time.</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
