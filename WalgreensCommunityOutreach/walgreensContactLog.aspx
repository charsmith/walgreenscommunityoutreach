﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensContactLog.aspx.cs" Inherits="walgreensContactLog" %>
<%@ Register src="controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>

   
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css"  />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css"  />
    
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
   
     <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="javaScript/jquery-ui.js"></script>

     <script type="text/javascript">

         $(document).ready(function () {
             var OutreachProgramSelectedId = '<%=commonAppSession.SelectedStoreSession.OutreachProgramSelectedId%>';
             var outreach_effort = '<%=commonAppSession.SelectedStoreSession.OutreachProgramSelected%>';
             var business_type = '<%=commonAppSession.SelectedStoreSession.SelectedBusinessTypeId%>';
             $("#lblOutreachStatuses").text("Outreach Status:");

             var maxLength = 140;
             var returnValue = false;
             $("#idCount").text(maxLength);
             $("#idLabel").text(maxLength);
             $("#txtFeedBack").keypress(function (e) {
                 if (($("#txtFeedBack").val().length < maxLength) || (e.keyCode == 8 || e.keyCode == 46 || (e.keyCode >= 35 && e.keyCode <= 40))) {
                     $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                     returnValue = true;
                 }
                 else {
                     $("#idCount").text(0);
                     returnValue = false;
                 }
                 return returnValue;
             });

             if ($('#ddlOutreach').val() != undefined && $('#ddlOutreach').val() != "" && $('#ddlOutreach').val() == "4" ) {
                 $("#rowTitle").removeAttr("style");
                 setRequireFieldEnabled(true);
             }

             $("#txtFeedBack").focusout(function () {
                 if ($("#txtFeedBack").val().length > maxLength) {
                     $("#txtFeedBack").val($("#txtFeedBack").val().substring(0, maxLength))
                     $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                 }
                 else {
                     $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                 }
                 $("#txtFeedBack").val($("#txtFeedBack").val().replace("<", "&lt;").replace(">", "&gt;"));
             });

             dropdownRepleceText("ddlAssignedBusinesses", "txtAssignedBusinesses");
             dropdownRepleceText("ddlAssignedBusiness", "txtAssignedBusiness");
             dropdownRepleceText("ddlOutreach", "txtOutreach");

             if (outreach_effort == "IP" && (business_type == 2 || business_type == 3)) {
                 $("#tblLogContact").hide();
             }
             else
                 $("#tblLogContact").show();
         });

         function showContractInitiatedWarning(alert, contactLogPk) {
             $(function () {
                 $("#divConfirmDialog").html(alert);
                 $("#divConfirmDialog").dialog({
                     closeOnEscape: false,
                     beforeclose: function (event, ui) { return false; },
                     dialogClass: "noclose",
                     height: 200,
                     width: 375,
                     title: "Contract has been initiated",
                     buttons: {
                         'Yes': function () {
                             __doPostBack("showContract", contactLogPk);
                             $(this).dialog('close');
                         },
                         No: function () {
                             __doPostBack("showContract", 0);
                             $(this).dialog('close');
                         }
                     },
                     modal: true
                 });
             });
         }

         function showPrevSeasonContract(url, contact_log_pk) {
             __doPostBack("showPrevSeasonContract", contact_log_pk);
         }

         function showContactLogDetails(url, contact_log_pk) {
             $.ajax({
                 url: "walgreensContactLog.aspx/setSelectedContactLogPk",
                 type: "POST",
                 data: "{'contact_log_pk':'" + contact_log_pk + "'}",
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function (data) {
                     window.location.href = url;
                     return false;
                 }
             });             
         }

    </script>
    <script type="text/javascript" language="javascript">
        //accordion
        var ContentHeight = 0;
        var TimeToSlide = 250.0;

        var openAccordion = '';
        var openAccordionPrevious = '';

        function runAccordion(index, row_height, is_preview) {
            ContentHeight = row_height;
            var nID;
            if (is_preview == 0) {
                nID = "Accordion" + index + "Content";
                if (openAccordion == nID) {
                    nID = '';
                }

                setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'"
              + openAccordion + "','" + nID + "')", 33);

                openAccordion = nID;
            }
            else {
                nID = "AccordionPrevious" + index + "Content";
                if (openAccordionPrevious == nID) {
                    nID = '';
                }
                setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'"
              + openAccordionPrevious + "','" + nID + "')", 33);

                openAccordionPrevious = nID;
            }
        }

        function animate(lastTick, timeLeft, closingId, openingId) {
            if (closingId != '') {
                document.getElementById(closingId + "Img").src = "images/closedArrow.png";
                if (closingId != openingId) {
                    if (openingId != '') {
                        document.getElementById(openingId + "Img").src = "images/openArrow.png";
                    }
                    else {
                        document.getElementById(closingId + "Img").src = "images/closedArrow.png";
                    }
                }
            }
            else {
                document.getElementById(openingId + "Img").src = "images/openArrow.png";
            }

            var curTick = new Date().getTime();
            var elapsedTicks = curTick - lastTick;

            var opening = (openingId == '') ? null : document.getElementById(openingId);
            var closing = (closingId == '') ? null : document.getElementById(closingId);

            if (timeLeft <= elapsedTicks) {
                if (opening != null) {
                    opening.style.height = ContentHeight + 'px';
                    opening.style.height = 'auto';
                }

                if (closing != null) {
                    closing.style.display = 'none';
                    closing.style.height = '0px';
                }
                return;
            }

            timeLeft -= elapsedTicks;
            var newClosedHeight = Math.round((timeLeft / TimeToSlide) * ContentHeight);

            if (opening != null) {
                if (opening.style.display != 'block')
                    opening.style.display = 'block';
                //opening.style.height = (ContentHeight - newClosedHeight) + 'px';
                opening.style.height = 'auto';
            }

            if (closing != null)
                closing.style.height = newClosedHeight + 'px';

            setTimeout("animate(" + curTick + "," + timeLeft + ",'"
              + closingId + "','" + openingId + "')", 33);
        }


        function deleteCatalog(catLogPk) {
            __doPostBack('deleteContactLog', catLogPk);
        }

        function displayContact(businessId) {
            var storeId = ('<%= storeId %>');
            $('#ddlOutreach').removeAttr('disabled');
            $("#txtTitle").val("");
            $("#txtContactFirstName").val("");
            $("#txtContactLastName").val("");
             $("#hfIsExistingBusiness").val("");
            var business_type_id = 1;
            if (businessId != "0") {
                var outreach = []
                outreach_effort = ('<%=commonAppSession.SelectedStoreSession.OutreachProgramSelected%>');
                if (business_type_id == 1)
                    outreach = $.parseJSON('<%=commonAppSession.SelectedStoreSession.OutreachBusinessesJson%>');
                else
                    outreach = $.parseJSON('<%=commonAppSession.SelectedStoreSession.AssignedClinicsJson%>');

                if (!$.isEmptyObject(outreach)) {
                    $.each(outreach, function (i, val) {
                        if (val.businessId == businessId || val.businessPk == businessId) {
                            $("#txtContactFirstName").val(val.firstName);
                            $("#txtContactLastName").val(val.lastName);
                             $("#hfIsExistingBusiness").val(val.isExistingBusiness);
                             $("#txtTitle").val(val.jobTitle);
                            if (business_type_id == 1) {
                                if (outreach_effort == 'IP' && (val.outreachStatusId == 0 || val.outreachStatusId == 1 || val.outreachStatusId == 6 || val.outreachStatusId == 7)) {
                                    $("#rowTitle").css("display", "none");
                                    setRequireFieldEnabled(false);
                                    $('#ddlOutreach').val(0);
                                }
                                else if (outreach_effort == 'SO' && val.outreachStatusId == 0) {
                                    $("#rowTitle").css("display", "none");
                                    setRequireFieldEnabled(false);
                                    $('#ddlOutreach').val(0);
                                }
                                else {
                                    $('#ddlOutreach').val(val.outreachStatusId);
                                    if (val.outreachStatusId == 4) {
                                        $("#rowTitle").removeAttr("style");
                                        $("#txtTitle").val(val.jobTitle);
                                        setRequireFieldEnabled(true);
                                    }
                                    else {
                                        $("#rowTitle").css("display", "none");
                                        setRequireFieldEnabled(false);
                                    }
                                }

                            }
                        }
                    });
                }
                else {
                    var business_type = ('<%=this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId%>');
                    var outreach_pk = ('<%=this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId%>');
                    $.ajax({
                        url: "walgreensContactLog.aspx/getContactName",
                        type: "POST",
                        data: "{'store_id':'" + storeId + "', 'business_id':'" + businessId + "', 'business_type':'" + business_type + "', 'outreach_pk':'" + outreach_pk + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            orderInfo = data.d;
                            if (orderInfo.firstName != null && orderInfo.lastName != null) {
                                $("#txtContactFirstName").val(orderInfo.firstName);
                                $("#txtContactLastName").val(orderInfo.lastName);
                                if (orderInfo.outreachStatusId == 6 || orderInfo.outreachStatusId == 7 || orderInfo.outreachStatusId == 1 || orderInfo.outreachStatusId == 0)
                                    $('#ddlOutreach').val(0);
                                else if (orderInfo.outreachStatusId == 3 || orderInfo.outreachStatusId == 8)
                                    $('#ddlOutreach').val(orderInfo.outreachStatusId);
                                else
                                    $('#ddlOutreach').val(orderInfo.outreachStatusId);
                            }
                        }
                    });
                }
            }
            else {
                $("#txtContactFirstName").val("");
                $("#txtContactLastName").val("");
            }
        }

        function validateRequired(contact_status) {
            var outreach_effort = ('<%=commonAppSession.SelectedStoreSession.OutreachProgramSelected%>');       
            $("#rowTitle").css("display", "none");
            var business_type = $("#ddlBussinessType").val();
            switch (contact_status.options[contact_status.selectedIndex].text) {
                case "No Client Interest":
                    if (typeof (Page_Validators) != 'undefined') {                      
                            $("#rowTitle").removeAttr("style");
                            setRequireFieldEnabled(true);
                    };
                    break;
                default:
                    if (typeof (Page_Validators) != 'undefined') {
                        setRequireFieldEnabled(false);
                    };

            }
        }

        function setRequireFieldEnabled(is_enabled) {
            
            for (i = 0; i <= Page_Validators.length; i++) {
                if (Page_Validators[i] != null) {
                    if (Page_Validators[i].controlToValidate == "txtContactFirstName" || Page_Validators[i].controltovalidate == "txtContactFirstName")
                        ValidatorEnable(Page_Validators[i], is_enabled);
                    if (Page_Validators[i].controltovalidate == "txtContactLastName")
                        ValidatorEnable(Page_Validators[i], is_enabled);
                    if (Page_Validators[i].controltovalidate == "txtFeedBack")
                        ValidatorEnable(Page_Validators[i], is_enabled);
                    if (Page_Validators[i].controltovalidate == "txtTitle")
                        ValidatorEnable(Page_Validators[i], is_enabled);
                   
                }
            }
        }

        function validateForm() {
            var noValidationError = true;
            var isValid = false;
            if (typeof (Page_ClientValidate) == 'function') {
                isValid = Page_ClientValidate();
            }

            if (isValid)
                return true;
            else
                return false;
        }
    </script>

    <style type="text/css">
        .noclose .ui-dialog-titlebar-close { display:none; }
        .ui-icon ui-icon-closethick { display:none; }
        .AccordionTitle{font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:12px;color:#666;font-weight:700;padding-top:10px;padding-left:20px;cursor:pointer}
        .AccordionContent{height:10px;overflow:auto;display:none;font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:12px;color:#1C2023;padding-left:30px;padding-top:5px;padding-bottom:5px}
        .AccordionContainer{border-top:solid 1px #C1C1C1;border-bottom:solid 1px #C1C1C1;border-left:solid 2px #C1C1C1;border-right:solid 2px #C1C1C1}
        .AccordionContent1{height:10px;overflow:auto;display:none;font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:12px;color:#1C2023;position:relative;padding:5px 40px 5px 60px}
        .AccordionTitle,.AccordionContent,.AccordionContainer,.AccordionContainer1{position:relative}
    </style>

    <style type="text/css">
        .noclose .ui-dialog-titlebar-close { display:none; }
        .ui-icon ui-icon-closethick { display:none; }
        .AccordionPreviousTitle{font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:12px;color:#666;font-weight:700;padding-top:10px;padding-left:20px;cursor:pointer}
        .AccordionPreviousContent{height:10px;overflow:auto;display:none;font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:12px;color:#1C2023;padding-left:30px;padding-top:5px;padding-bottom:5px}
        .AccordionPreviousContainer{border-top:solid 1px #C1C1C1;border-bottom:solid 1px #C1C1C1;border-left:solid 2px #C1C1C1;border-right:solid 2px #C1C1C1}
        .AccordionPreviousContent1{height:10px;overflow:auto;display:none;font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:12px;color:#1C2023;position:relative;padding:5px 40px 5px 60px}
        .AccordionPreviousTitle,.AccordionPreviousContent,.AccordionPreviousContainer,.AccordionPreviousContainer1{position:relative}
    </style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:HiddenField ID="hfIsExistingBusiness" runat="server" Value="false" />
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
    <td colspan="2">
        <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
      <table width="935" border="0" cellspacing="22" cellpadding="0" style="min-height: 400px;">
        <tr>
            <td width="601" style="text-align: left; min-height: 400px; vertical-align: top;">                        
            <asp:Label ID="lblMessage" runat="server" Text="" class="formFields"  Font-Bold="true" ForeColor="Red"></asp:Label>
                <table width="599" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="230" class="pageTitle"><asp:Label ID="lblContactLabel" runat="server" Text="Contact Log"></asp:Label></td>
                        <td width="369" align="right" valign="top" class="pageTitle"><span class="logSubTitles">
                        <asp:TextBox id="txtAssignedBusinesses" CssClass="formFields" visible="true" runat="server" Width="280px" ></asp:TextBox>
                        <asp:DropDownList ID="ddlAssignedBusinesses" runat="server" AutoPostBack="true" onselectedindexchanged="ddlAssignedBusinesses_SelectedIndexChanged" Width="280px"></asp:DropDownList></span></td>
                    </tr>
                </table>
                <br />
                <table id="tblCurrentContactLogs" runat="server" width="599" border="0" cellspacing="0" cellpadding="0" >
                    <tr>
                        <td style="vertical-align: top; text-align: left;">
                            <asp:Label ID="lblBusinessName" runat="server" class="formFieldsBlack"></asp:Label><br />
                            <asp:Label ID="lblCurrentSeasonLogs" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tblPreviousSeasonLogs" runat="server" width="599" border="0" cellspacing="0" cellpadding="0">
                    <tr>                        
                        <td class="pageTitle" style="padding-top:25px;">Contact Log History</td>                        
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblPreviousSeasonLogs" runat="server" Text=""></asp:Label></td>
                    </tr>
                </table>
            </td>
            <td width="268" align="left" valign="top">
                <table id="tblLogContact" runat="server" width="268" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="backgroundGradientLog"><table width="268" border="0" cellspacing="8" cellpadding="0">
                                <tr>
                                    <td class="logTitle">Log Business Contact</td>
                                </tr>
                                <tr>
                                <td><asp:ValidationSummary ID="ValidationSummary1" runat="server" class="formFields" DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" ShowSummary="false" ValidationGroup="contactLog"/></td>
                                </tr>
                                <tr>
                                    <td><span class="logSubTitles">Business Contacted:<br />
                                            <label for="select"></label>
                                            <asp:DropDownList ID="ddlAssignedBusiness" runat="server" AutoPostBack="false" onchange="javascript:displayContact(this.value)" CausesValidation="false" Width="249px"></asp:DropDownList>
                                            <asp:TextBox id="txtAssignedBusiness" CssClass="formFields" visible="true" runat="server" Width="247px" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqBusiness" runat="server" ControlToValidate="ddlAssignedBusiness" Display="None" ValidationGroup="contactLog" ErrorMessage="Please select Business Contacted" InitialValue="0">*</asp:RequiredFieldValidator>
                                        </span></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr><td style="padding-right:2px"><span class="logSubTitles">Contact First Name:<br />
                                            <asp:TextBox ID="txtContactFirstName" runat="server" Width="115px" MaxLength="50" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="txtContactFirstName" Enabled="false" Display="None"
                                            ErrorMessage="Please enter contact First Name" CssClass="logSubTitles" Font-Bold="True" Font-Names="Calibri" Font-Size="Medium">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="txtContactFirstNameEV" ControlToValidate="txtContactFirstName"
                                            Display="None" ValidationExpression="[^,<>@#&]+" runat="server" ErrorMessage="Contact First Name: , < > @ # & Characters are not allowed." ValidationGroup="contactLog">*</asp:RegularExpressionValidator>
                                                   
                                        </span></td>                                                    
                                        <td><span class="logSubTitles">Contact Last Name:<br />
                                            <asp:TextBox ID="txtContactLastName" runat="server" Width="115px" MaxLength="50" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtContactLastName" Enabled="false" Display="None" 
                                            ErrorMessage="Please enter contact Last Name" CssClass="logSubTitles" Font-Bold="True" Font-Names="Calibri" Font-Size="Medium">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="txtContactLastNameEv" ControlToValidate="txtContactLastName"
                                            Display="None" ValidationExpression="[^,<>@#&]+" runat="server" ErrorMessage="Contact Last Name: , < > @ # & Characters are not allowed." ValidationGroup="contactLog">*</asp:RegularExpressionValidator>                                                   
                                        </span>
                                        </td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr id="rowTitle" runat="server" style="display:none;" >
                                    <td>
                                        <span class="logSubTitles">Job Title:</span><br />
                                            <asp:TextBox ID="txtTitle" CssClass="formFields" runat="server" MaxLength="100" Width="210px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="txtTitleReqFV" runat="server" ControlToValidate="txtTitle" Enabled="false" Display="None" ErrorMessage="Please enter Job Title" CssClass="logSubTitles" Font-Bold="True" 
                                            Font-Names="Calibri" Font-Size="Medium">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="txtTitleRegEV" ControlToValidate="txtTitle" Display="None" ValidationExpression="[^,<>@#&]+" runat="server" 
                                            ErrorMessage="Job Title: , < > @ # & Characters are not allowed." ValidationGroup="contactLog">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="logSubTitles">Contact Date:</span>                                                   
                                        <uc1:PickerAndCalendar ID="PickerAndCalendar1" runat="server" /></td>
                                </tr>
                                    <tr>
                                    <td><span class="logSubTitles"><asp:Label ID="lblOutreachStatuses" runat="server"></asp:Label></span><br />
                                        <asp:DropDownList ID="ddlOutreach" runat="server" CausesValidation="false" Width="215px" onchange="javascript:validateRequired(this);"></asp:DropDownList>
                                        <asp:TextBox id="txtOutreach" CssClass="formFields" visible="true" runat="server" Width="213px" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqOutreach" runat="server" ControlToValidate="ddlOutreach" Display="None" 
                                        ErrorMessage="Please select Outreach Status" InitialValue="0" ValidationGroup="contactLog">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="logSubTitles">Feedback/Notes:<br /></span>
                                            <asp:TextBox ID="txtFeedBack" class="formFields" runat="server" Columns="39" Rows="10" TextMode="MultiLine" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="txtFeedBackReqFV" runat="server" 
                                            ControlToValidate="txtFeedBack" Enabled="false" Display="None"
                                            ErrorMessage="Please enter Notes" CssClass="logSubTitles" Font-Bold="True" 
                                            Font-Names="Calibri" Font-Size="Medium" ValidationGroup="contactLog">*</asp:RequiredFieldValidator>
                                            <span class="logSubTitles">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left">
                                                        <span style="font-size: xx-small">*<asp:Label ID="idLabel" runat="server"></asp:Label> characters allowed</span>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="idCount" Text="" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>  
                                            </span></td>     
                                </tr>
                                <tr>
                                    <td><asp:ImageButton ID="btnSubmit" runat="server" AlternateText="Submit Entry" OnClientClick="javascript:return validateForm();"  
                                            ImageUrl="images/btn_submit.png" ValidationGroup="contactLog" OnClick="btnSubmit_Click" /></td>
                                </tr>
                        </table></td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 13px; text-align:center; font-weight:bold;"></div>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
</form>
</body>
</html>

