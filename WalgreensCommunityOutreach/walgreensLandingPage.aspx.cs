﻿using System;
using TdApplicationLib;
using System.Web.Security;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections.Generic;
using TdWalgreens;
using NLog;
using System.Text;

public partial class walgreensLandingPage : Page
{
    #region -------------- PAGE EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserName))
            {
                Session.RemoveAll();
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!IsPostBack)
            {
                this.getStoreDetails(string.Empty);
                this.navigateUserToSpecificPage();
                this.getCustomMessage();
            }

            if (this.commonAppSession.LoginUserInfoSession.UserOutreachEfforts.Count > 0)
            {
                foreach (KeyValuePair<String, String> user_outreach_effort in this.commonAppSession.LoginUserInfoSession.UserOutreachEfforts)
                {
                    if (user_outreach_effort.Key != "2")
                    {
                        this.plsOutreachEfforts.Controls.Add(new LiteralControl("<p id='" + user_outreach_effort.Value.Replace(" ", "") + "Link' runat='server' class='class1'>"));
                        LinkButton lnk_outreach_effort = new LinkButton();
                        lnk_outreach_effort.ID = "lnk" + user_outreach_effort.Value.Replace(" ", "");
                        lnk_outreach_effort.CssClass = "class1";
                        lnk_outreach_effort.CommandArgument = user_outreach_effort.Key;
                        lnk_outreach_effort.Text = user_outreach_effort.Value + @" »";
                        this.plsOutreachEfforts.Controls.Add(lnk_outreach_effort);
                        this.plsOutreachEfforts.Controls.Add(new LiteralControl("</p>"));
                        ((LinkButton)this.plsOutreachEfforts.FindControl("lnk" + user_outreach_effort.Value.Replace(" ", ""))).Command += new CommandEventHandler(lnkOutreach_Click);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Error((Session["logString"] != null ? "User Log:" + Session["logString"].ToString() : "") + "|Error message: " + ex.Message + " Stack Trace: " + ex.StackTrace);
        }
    }

    protected void lnkOutreach_Click(object sender, CommandEventArgs e)
    {
        this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId = e.CommandArgument.ToString();

        this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
        this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1") this.commonAppSession.SelectedStoreSession.OutreachProgramSelected = "SO";
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3") this.commonAppSession.SelectedStoreSession.OutreachProgramSelected = "IP";

        if (this.commonAppSession.LoginUserInfoSession.OutreachEffortAccessed.ContainsKey(e.CommandArgument.ToString()) && !(this.commonAppSession.LoginUserInfoSession.OutreachEffortAccessed[e.CommandArgument.ToString()]))
        {
            this.dbOperation.insertLoginCountByUser(this.commonAppSession.LoginUserInfoSession.UserID, e.CommandArgument.ToString());
            this.commonAppSession.LoginUserInfoSession.OutreachEffortAccessed[e.CommandArgument.ToString()] = true;
        }

        if ((this.commonAppSession.LoginUserInfoSession.IsAdmin || this.commonAppSession.LoginUserInfoSession.IsPowerUser) && (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "regional vice president" && this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "regional healthcare director"))
            this.getStoreStatusAndGoal();

        if (string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.storeID.ToString()) || this.commonAppSession.SelectedStoreSession.storeID == 0)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + GetGlobalResourceObject("errorMessages", "noStoreAssigned") + "');", true);
        else
        {
            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "admin":
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                case "regional healthcare director":
                case "regional vice president":
                    string store_state;
                    this.commonAppSession.SelectedStoreSession.getAssignedBusinesses = this.dbOperation.getAssignedBusinesses(this.commonAppSession.SelectedStoreSession.storeID, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), out store_state);
                    this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses = this.dbOperation.getAssistedClinicBusinesses(this.commonAppSession.SelectedStoreSession.storeID);
                    this.commonAppSession.LoginUserInfoSession.AutoOpenDupBusinesses = false;
                    //if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                    //{
                    //    //this.commonAppSession.LoginUserInfoSession.DuplicateBusinessCount = this.dbOperation.getDuplicateBusinesses(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId)).Rows.Count;
                    //    //if (this.commonAppSession.LoginUserInfoSession.DuplicateBusinessCount > 0)
                    //    //    Response.Redirect("walgreensDistrictUsersHome.aspx");
                    //    //else
                    //    Response.Redirect("walgreensHome.aspx", false);
                    //    Context.ApplicationInstance.CompleteRequest();
                    //}
                    if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
                    {
                        this.commonAppSession.dtCorporateClinics = new DataTable();
                        this.commonAppSession.dtDirectB2BBusiness = new DataTable();
                        this.commonAppSession.dtLocalLeads = new DataTable();

                        Response.Redirect("walgreensDistrictUsersHome.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        Response.Redirect("walgreensHome.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                    break;
                default:
                    Response.Redirect("walgreensHome.aspx", false);
                    Context.ApplicationInstance.CompleteRequest();
                    break;
            }
        }
    }

    #endregion

    #region -------------- PRIVATE METHODS ----------
    private bool getStoreDetails(string store_id)
    {
        bool has_store = false;
        if (this.commonAppSession.SelectedStoreSession.storeID == 0)
        {
            DataTable user_stores = this.dbOperation.getUserStores(this.commonAppSession.LoginUserInfoSession.UserID, store_id);

            if (user_stores != null && user_stores.Rows.Count > 0)
            {
                has_store = true;
                this.commonAppSession.SelectedStoreSession.storeID = Convert.ToInt32(user_stores.Rows[0]["storeID"]);
                this.commonAppSession.SelectedStoreSession.storeName = user_stores.Rows[0]["address"].ToString();
                this.commonAppSession.SelectedStoreSession.storeState = user_stores.Rows[0]["state"].ToString();

                if (!this.commonAppSession.LoginUserInfoSession.IsAdmin && !this.commonAppSession.LoginUserInfoSession.IsPowerUser)
                {
                    var regex = new Regex(",");
                    this.lblStoreName.Text = regex.Replace(user_stores.Rows[0]["address"].ToString(), "<br />", 1);
                }
            }
        }

        return has_store;
    }

    /// <summary>
    /// Navigating user to particular Immunization Program page based on requested page
    /// </summary>
    private void navigateUserToSpecificPage()
    {
        EncryptedQueryString args = new EncryptedQueryString(Request.QueryString["args"]);
        this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId = "3";
        this.commonAppSession.SelectedStoreSession.OutreachProgramSelected = "IP";

        if (args.Count > 5)
        {
            if (!string.IsNullOrEmpty(args["arg6"]) && this.commonAppSession.SelectedStoreSession.storeID != Convert.ToInt32(args["arg6"].ToString()))
            {
                if (!this.getStoreDetails(args["arg6"].ToString()))
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + GetGlobalResourceObject("errorMessages", "noStoreAssigned") + "'); window.location.href = 'thankYou.aspx';", true);
            }

            this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = Convert.ToInt32(args["arg4"].ToString());
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(args["arg5"].ToString());
            this.commonAppSession.SelectedStoreSession.storeID = Convert.ToInt32(args["arg6"].ToString());
        }
        else if (args.Count > 3)
        {
            if (args["arg3"] == "reports/reportScheduledAppointment.aspx")
            {
                this.commonAppSession.SelectedStoreSession.referrerPath = "walgreensLandingPage.aspx";
                this.commonAppSession.SelectedStoreSession.schedulerDesignPk = Convert.ToInt32(args["arg4"].ToString());
            }
            else if (args.Count == 5)
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(args["arg4"].ToString());
                if (this.getStoreDetails(args["arg5"].ToString()))
                    this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 2;
            }
            else
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(args["arg4"].ToString());
        }

        if (args.ContainsKey("arg3") && !string.IsNullOrEmpty(args["arg3"].ToString()))
        {
            Response.Redirect(args["arg3"].ToString(), false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }

    /// <summary>
    /// Get custom message
    /// </summary>
    private void getCustomMessage()
    {
        StringBuilder sb = new StringBuilder();
        DataTable dt_custom_msg = this.dbOperation.getCustomMessages(true);
        if (dt_custom_msg != null && dt_custom_msg.Rows.Count > 0)
        {
            sb.AppendLine(dt_custom_msg.Rows[0]["customMessage"].ToString());
            this.lblCustomMessage.Text = Server.HtmlDecode(sb.ToString());
        }
        else
        {
            this.lblCustomMessage.Visible = false;
        }
    }

    /// <summary>
    /// Gets and sets store goal and status
    /// </summary>
    private void getStoreStatusAndGoal()
    {
        string last_access = string.Empty;
        string to_goal = string.Empty;
        string to_complete = string.Empty;
        DateTime fiscal_year_start = ApplicationSettings.getOutreachStartDate;

        this.dbOperation.getHeaderValues(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, fiscal_year_start, ApplicationSettings.getCurrentQuarter.ToString(), this.commonAppSession.LoginUserInfoSession.UserID, out last_access, out to_goal, out to_complete);

        this.commonAppSession.SelectedStoreSession.HeaderDetails = string.Empty;
        if (!string.IsNullOrEmpty(last_access))
            this.commonAppSession.SelectedStoreSession.lastStatusProvided = (string)GetGlobalResourceObject("errorMessages", "lastStatusProvided") + " " + last_access;
        else
            this.commonAppSession.SelectedStoreSession.lastStatusProvided = "";

        this.commonAppSession.SelectedStoreSession.storeGoalMsg = String.Format((string)GetGlobalResourceObject("errorMessages", "goalMessage"), to_goal, to_complete, "Q" + ApplicationSettings.getCurrentQuarter.ToString());
    }
    #endregion

    #region -------------- PRIVATE VARIABLES --------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();

        if (Session.IsNewSession)
        {
            FormsAuthentication.SignOut();
            string str = Request.Url.ToString();
            Response.Redirect(str);
        }

        if (Request.QueryString.Count > 0 && Request.QueryString["args"] != null)
        {
            string args_value = Request.QueryString["args"];
            EncryptedQueryString args = new EncryptedQueryString(args_value);
            if (args.Count > 0)
            {
                string user_name = args["arg1"];
                if (this.commonAppSession.LoginUserInfoSession.UserName != user_name)
                {
                    FormsAuthentication.SignOut();
                    string str = Request.Url.ToString();
                    Response.Redirect(str);
                }
            }
            else
            {
                Response.Redirect("auth/auth.aspx");
            }
        }
    }
    #endregion
}
