﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensHome.aspx.cs" Inherits="walgreensHome" %>
<%@ Register src="controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register Src="controls/WorkflowMonitor.ascx" TagName="workflowMonitor" TagPrefix="ucWMonitor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/ddcolortabs.css" /> 
    <link rel="stylesheet" type="text/css" href="css/wags.css" />    
    <link rel="stylesheet" type="text/css" href="css/theme.css"  />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css"  />    
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
       
    <script src="javaScript/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="javaScript/grid.locale-en.js" type="text/javascript"></script>
    <script src="javaScript/jquery.jqGrid.src.js" type="text/javascript"></script>
    <script src="javaScript/jquery.jqGrid.min.js?a=1" type="text/javascript"></script>    
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/ui.jqgrid.css" />
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />    
    
<script type="text/javascript">
    function setTab() {
        var $tabs = $('#tabs').tabs();
    }

    function setDropDowns(outreach_effort, business_type_id) {
       
        $("#hfSelectedBusinessTypeTabId").val(business_type_id);

        if (business_type_id == 3) {
            var dnc_data = "";
            dnc_data = <%=this.nationalContracts %>;

            if(dnc_data.length == 0){
                __doPostBack("getDNCClientsList", 0);
            }
        }

        $.ajax({
            url: "walgreensHome.aspx/setSelectedBusinessId",
            type: "POST",
            data: "{'business_id':'" + business_type_id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
            }
        });
        
        var count = 0;
        var business_selected = $("#ddlAssignedBusiness").val();
        $("#lblOutreachStatuses").text("Outreach Status:"); 
        if (outreach_effort == "IP" && business_type_id == 1) {
            var mydata = [];
            mydata = '<%=commonAppSession.SelectedStoreSession.OutreachBusinessesJson%>';
            mydata = $.parseJSON(mydata);
            $("#ddlAssignedBusiness").children('option:not(:first)').remove();
            $('#ddlAssignedBusiness').find("option").remove();
            $('#ddlAssignedBusiness').append('<option value="0">-- Select Business --</option>');
            $.each(mydata, function (i, val) {
                $('#ddlAssignedBusiness').append('<option value="' + val.businessPk + '">' + $.trim(val.businessName.replace('*', '')) + '</option>');
                count++;
            });

            $("#lblOutreachStatuses").text("Outreach Status:");
            var outreach = []
            outreach = $.parseJSON('<%= this.outreachStatuses %>');
            $('#ddlOutreach').find("option").remove();
            $.each(outreach, function (i, val) {
                if (val.category == outreach_effort && val.businessTypeId == business_type_id) {
                    if (val.outreachStatus != "DM Contacted" && val.outreachStatus != "HCS Contacted") {
                        if (val.outreachStatus == "No Outreach" || val.outreachStatus == "Contact Client" || val.outreachStatus == "Provide Vouchers")
                            $('#ddlOutreach').append('<option value="' + val.pk + '">-- Select Outreach Status --</option>');
                        else
                            $('#ddlOutreach').append('<option value="' + val.pk + '">' + val.outreachStatus + '</option>');
                    }
                }
            });
        }

        $("#ddlAssignedBusiness").val(business_selected);
        if ($("#ddlAssignedBusiness").val() == 0) {
            $("#txtContactFirstName").val('');
            $("#txtContactLastName").val('');
        }

        if (outreach_effort == "IP" && business_type_id == 2) {
            $("#tblContactLog").hide();
            $("#tblScheduledClinicsTabMesg").show();
        }
        else if (outreach_effort == "IP" && business_type_id == 3) {
            $("#tblContactLog").hide();
            $("#tblScheduledClinicsTabMesg").hide();
        }
        else {
            $("#tblContactLog").show();
            $("#tblScheduledClinicsTabMesg").hide();
        }
    }

    function showDeleteDNCClientWarning(alert, selectedRemoveDNCClients){
        $(function () {
            $("#divConfirmDialog").html(alert);
            $("#divConfirmDialog").dialog({
                closeOnEscape: false,
                beforeclose: function (event, ui) { return false; },
                dialogClass: "noclose",
                height: 150,
                width: 350,
                title: "Remove Do Not Call National Contract Client",
                buttons: {
                    'Yes': function () {
                        __doPostBack("deletedDNCClient", selectedRemoveDNCClients);
                        $(this).dialog('close');
                    },
                    No: function () {
                        __doPostBack("deletedDNCClient", "");
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        });
    }

    function showContractInitiatedWarning(alert, contactLogPk) {
        $(function () {
            $("#divConfirmDialog").html(alert);
            $("#divConfirmDialog").dialog({
                closeOnEscape: false,
                beforeclose: function (event, ui) { return false; },
                dialogClass: "noclose",
                height: 150,
                width: 350,
                title: "Contract has been initiated",
                buttons: {
                    'Yes': function () {
                        __doPostBack("showContract", contactLogPk);
                        $(this).dialog('close');
                    },
                    No: function () {
                        __doPostBack("showContract", 0);
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        });
    }

    $(document).ready(function () {
      
        if ($("#hfSelectedBusinessTypeTabId").val() != 3)
            setBusinessTypeTab('<%=commonAppSession.SelectedStoreSession.SelectedBusinessTypeId%>');
        else {
            $("#tabs").tabs({ selected: $('#tabs ul li a:visible').length -1 });
            $("#tblContactLog").hide();
            $("#tblScheduledClinicsTabMesg").hide();
        }

        //$("#ddlStoreList").change(function () {
        //    $("#txtStoreId").val($("#ddlStoreList option:selected").val());
        //    $("#btnRefresh").click();
        //});

        dropdownRepleceText("ddlAssignedBusinesses", "txtAssignedBusinesses");
        dropdownRepleceText("ddlAssignedBusiness", "txtAssignedBusiness");
        dropdownRepleceText("ddlOutreach", "txtOutreach");
       // dropdownRepleceText("ddlStoreList", "txtStoreList");

        //$("#walgreensHeaderCtrl_lblStoreProfiles").html($("#txtStoreProfiles").val());
        var maxLength = 140;
        var returnValue = false;
        $("#idCount").text(maxLength);
        $("#idLabel").text(maxLength);
        $("#txtFeedBack").keypress(function (e) {
            if (($("#txtFeedBack").val().length < maxLength) || (e.keyCode == 8 || e.keyCode == 46 || (e.keyCode >= 35 && e.keyCode <= 40))) {
                $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                returnValue = true;
            }
            else {
                $("#idCount").text(0);
                returnValue = false;
            }
            return returnValue;
        });

        $("#txtFeedBack").focusout(function () {
            if ($("#txtFeedBack").val().length > maxLength) {
                $("#txtFeedBack").val($("#txtFeedBack").val().substring(0, maxLength));
                $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
            }
            else {
                $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
            }

            $("#txtFeedBack").val($("#txtFeedBack").val().replace("<", "&lt;").replace(">", "&gt;"));
        });
    });

    function setOutreachField() {
        var noValidationError = true;
        var isValid = false;
        if (typeof (Page_ClientValidate) == 'function') {
            isValid = Page_ClientValidate();
        }

        if (isValid && noValidationError) {
            $("#hdOutreachStatus").val("true");
            return true;
        }
        else
            return false;
    }

    function setBusinessTypeTab(business_type_id) {
        var mydata = [];
        mydata = '<%=commonAppSession.SelectedStoreSession.AssignedClinicsJson%>';
        var tab_id = 0;
        if (mydata != "[]") {
            $("#walgreensHeaderCtrl_lnkScheduledBusinesses").show();
        }
        else {        
            $("#walgreensHeaderCtrl_lnkScheduledBusinesses").hide();
        }

        var assisted_clincs = [];
        assisted_clincs = '<%=this.assistedClinics %>';
        if (assisted_clincs != "[]") {        
            $("#walgreensHeaderCtrl_lnkAssistedClinics").show();
        }
        else {
            $("#walgreensHeaderCtrl_lnkAssistedClinics").hide();
        }

        var $tabs = $('#tabs').tabs();
        if (mydata != "[]") {
            $tabs.tabs('select', business_type_id - 1);
        }
        else {
            $tabs.tabs('select', 0);
        }

        displayContact(0);
    }

    function displayContact(businessId) {
        if (businessId != "0") {            
            var OutreachProgramSelectedId = '<%=commonAppSession.SelectedStoreSession.OutreachProgramSelectedId%>';
            var outreach_effort = ('<%=commonAppSession.SelectedStoreSession.OutreachProgramSelected%>');
            var business_type_id = 1;
            var mydata = [];
            mydata = '<%=commonAppSession.SelectedStoreSession.OutreachBusinessesJson %>';
            mydata = $.parseJSON(mydata);

            $.each(mydata, function (i, val) {
                if (val.businessPk == businessId) {
                    $("#txtContactFirstName").val(val.firstName);
                    $("#txtContactLastName").val(val.lastName);
                    $("#txtTitle").val(val.jobTitle);
                    $("#ddlOutreach").removeAttr('disabled');

                    if (business_type_id == 1 && (val.outreachStatusId == 7 || val.outreachStatusId == 6)) { //7 - Contacted Last Season && 6 - Undecided
                        $("#ddlOutreach").val(0);
                    }
                    else {
                        $("#ddlOutreach").val(val.outreachStatusId);
                        if (val.outreachStatusId == 4) {
                            $("#rowJobTitle").removeAttr("style");
                            $("#txtTitle").val(val.jobTitle);
                            setRequireFieldEnabled(true);
                        }
                        else {
                            $("#rowJobTitle").css("display", "none");
                            setRequireFieldEnabled(false);
                        }
                    }

                    return false;
                }
            });
        }
        else {
            $("#ddlAssignedBusiness").val(0);
            $("#txtContactFirstName").val("");
            $("#txtContactLastName").val("");
            $("#txtTitle").val("");
            $("#ddlOutreach").val(0);
        }
    }

    function setRequireFieldEnabled(is_enabled) {
        for (i = 0; i <= Page_Validators.length; i++) {
            if (Page_Validators[i] != null) {
                if (Page_Validators[i].controlToValidate == "txtContactFirstName" || Page_Validators[i].controltovalidate == "txtContactFirstName")
                    ValidatorEnable(Page_Validators[i], is_enabled);
                if (Page_Validators[i].controltovalidate == "txtContactLastName")
                    ValidatorEnable(Page_Validators[i], is_enabled);
                if (Page_Validators[i].controltovalidate == "txtFeedBack")
                    ValidatorEnable(Page_Validators[i], is_enabled);
                if (Page_Validators[i].controltovalidate == "txtTitle")
                    ValidatorEnable(Page_Validators[i], is_enabled);
               
            }
        }
    }

    function showContactLogDetails(page_id, contact_log_pk) {
        $.ajax({
            url: "walgreensContactLog.aspx/setSelectedContactLogPk",
            type: "POST",
            data: "{'contact_log_pk':'" + contact_log_pk + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                switch (page_id) {
                    case 1:
                        window.location.href = "scheduleEventForm.aspx";
                        return false;
                        break;
                    case 2:
                        window.location.href = "walgreensFollowupEmail.aspx";
                        return false;
                        break;
                    case 3:
                        var mydata = [];
                        //var is_previous_contract;
                        mydata = '<%=commonAppSession.SelectedStoreSession.OutreachBusinessesJson %>';
                        mydata = $.parseJSON(mydata);
//                        $.each(mydata, function (i, val) {
//                            if ($.inArray("" + contact_log_pk + "", val.contactLogPk.split(',')) != -1) {
//                                is_previous_contract = val.isPreviousSeasonContract;
//                            }
//                        });
//                        if (is_previous_contract == "True")
//                            window.location.href = "walgreensClinicAgreementPrevSeason.aspx";
//                        else
                            window.location.href = "walgreensClinicAgreement.aspx";

                        return false;
                        break;
                    case 4:
                        window.location.href = "walgreensCharityProgram.aspx";
                        return false;
                        break;
//                    case 5:
//                        window.location.href = "walgreensVoteVaxAgreement.aspx";
//                        return false;
//                        break;

                    case 6:
                        window.location.href = "walgreensCommunityOutreachProgram.aspx";
                        return false;
                        break; 
                }
            }
        });
    }

    function validateRequired(contact_status) {
      
        var outreach_effort = ('<%=commonAppSession.SelectedStoreSession.OutreachProgramSelected%>');
        $("#rowJobTitle").css("display", "none");

        switch (contact_status.options[contact_status.selectedIndex].text) {
            case "No Client Interest":
                if (typeof (Page_Validators) != 'undefined') {
                      $("#rowJobTitle").removeAttr("style");
                        setRequireFieldEnabled(true);
            };
               break;
            default:
                if (typeof (Page_Validators) != 'undefined') {
                    setRequireFieldEnabled(false);
                };
        }
    }

    function disableAddBusiness() {
        var is_disable;
        if ($("#hfDisableAddBusinessToStoreState").val() != "") {
            var store_message = $("#hfDisableAddBusinessToStoreState").val();
            is_disable = true;
            alert(store_message);
        }
        else {
            is_disable = false;
            window.location.href = "walgreensStoreBusiness.aspx";
        }
        return !is_disable;
    }

//    isSafari = ($.browser.webkit || $.browser.safari) && parseFloat($.browser.version) < 536.5 ? true : false; // Chrome < version 19
</script>

<style type="text/css" >
    .noclose .ui-dialog-titlebar-close { display:none; }
    .ui-icon ui-icon-closethick { display:none; }
              
    .rowColorGREY
    {
        color:#999999;
    }
        
    .rowColor7
    {
        background-color:#ffffcc;
        border-color:Gray;
    }

    .rowClinicUnderLeadstore {
        background-color:lightblue;
        border-color:Gray;
    }  

    .rowColor14
    {
        background-color:#ffcccc;
        border-color:Gray;
    }
    .rowBgGreen
    {
        background-color:#d8f1c4;
        border-color:Gray;
    }
    .rowBgOrange
    {
        background-color:#fbe4b1;
        border-color:Gray;
    }
        
    .rowColorGreen
    {
        color:#418200;            
    }
            
    /*#pagernav_center{ display:none;}
    #pagernav_right{display:block;}
    #refresh_gridMain{display:none;}
    #pagernav_left{display:none;}*/
    .ui-widget
    {
        font-size:11px;
    }
        
    .ui-widget-header
    {
        border-bottom:0px none #FFFFFF;           
    }
        
    .ui-state-active, .ui-state-active, .ui-widget-header .ui-state-active        
    {            
        background:url("images/tab_gradient.jpg") repeat-x scroll 50% 50%;
	    background-repeat: repeat-x;
	    background-position:bottom; 
    }
	.ui-tabs .ui-tabs-nav li a, .ui-tabs.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-selected a {
		cursor: pointer;
		text-align: left;
	} 
	.ui-tabs .ui-tabs-nav li a {
		font-weight:bold;
    }
        
    /* .ui-jqgrid tr.ui-row-ltr td {
        border-right-style: none;
        border-left-style: none;
        border-bottom-style: none;
    }*/
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient" onload="javascript:setTab()">
<form id="form1" runat="server" >
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:HiddenField ID="hfSelectedBusinessTypeTabId" runat="server" Value="0" />
<asp:HiddenField ID="hfDisableAddBusinessToStoreState" runat="server" />
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
      <table width="935" border="0" cellspacing="22" cellpadding="0">
        <tr>
            <td width="599" style="min-height: 400px; vertical-align: top;">
                <!-- START JQGRID-->
                <asp:Label ID="lblMessage" runat="server" Text=""  Font-Bold="true" ForeColor="Red" class="formFields"></asp:Label>
                <table  border="0" cellspacing="5" cellpadding="0" runat="server" width="100%">
                    <tr><td>
                    <ucWMonitor:workflowMonitor ID="workflowMonitor" runat="server" /> 
                   </td></tr>
                </table>
                <table border="0" cellspacing="5" cellpadding="0" runat="server" id="rowMessageSO" visible="false" width="100%">
                    <tr>
                        <td align="left" valign="top" colspan="2" nowrap="nowrap" width="100%" ><span class="statusLine" style="color:Black;"><asp:Literal ID="ltlMessageSO" runat="server"></asp:Literal></span></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                  <tr>
                   <td>
                    <table id="tblPreferredPick" runat="server" width="100%" visible="false">
                    <tr>
                    <td class="preferredPick"><b>Preferred Pick:</b>
                            <asp:HyperLink ID="lnkPrefferedBusiness" style="color:#FFFFFF" runat="server" NavigateUrl="walgreensMap.aspx"></asp:HyperLink> - <asp:Label ID="lblPrefferedPhone" runat="server" >
                            </asp:Label><img src="images/spacer.gif" width="1" height="5" alt="spacer image" /><br />
                    </td>
                    </tr>
                </table>
                </td>
                </tr>
                <tr><td style="height:4px;"></td></tr>
                <tr>
                <td>
                <div id="tabs">
                    <ul style="font-size: 70%">
                        <asp:Literal ID="lblTabLinks" runat="server" Text=""></asp:Literal>
                    </ul>
                    <div id="idOutreachBusiness">
                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                        <td width="100%" align="left" nowrap="nowrap"><img id="imgPreviousClient" runat="server" src="images/outreach_color_key_previous_client.gif" width="86" height="12" alt="Previous Client" /></td>
                        <td width="130" align="right" nowrap="nowrap" class="class2">
                            <asp:HyperLink ID="lnkPrintableList" runat="server" NavigateUrl="~/reports/walgreensReport.aspx">Printable List</asp:HyperLink></td>
                        <td width="32" align="right">
                            <asp:ImageButton ID="imgPrintableList" runat="server" ImageUrl="~/images/btn_printable_list.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_printable_list.png');" CausesValidation="false"
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_printable_list_lit.png');" AlternateText="Printable List" PostBackUrl="~/reports/walgreensReport.aspx"  />
                        </td>
                        <td width="130" align="right" nowrap="nowrap" class="class2"><asp:HyperLink ID="lnkEditBusinessProfile" runat="server" NavigateUrl="~/walgreensEditBusinessProfile.aspx">View/Edit Business Profile</asp:HyperLink></td>
                        <td width="32" align="right">
                            <asp:ImageButton ID="imgBtnEditBusinessProfile" runat="server" ImageUrl="~/images/btn_business_profile.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_business_profile.png');" CausesValidation="false"
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_business_profile_lit.png');"  AlternateText="View or Edit Business Profile" postbackurl="~/walgreensEditBusinessProfile.aspx" />
                        </td>
                        </tr>
                        <tr>
                        <td align="left" ><img src="images/outreach_color_key.gif" runat="server" id="imgOutreachColorKey" width="263" height="21" alt="potential local business row color codes" /></td>
                        <td colspan="3" align="right" nowrap="nowrap" class="class2">
                            <asp:HyperLink ID="lnkAddBusiness" runat="server" NavigateUrl="~/walgreensStoreBusiness.aspx">Add A Business</asp:HyperLink>
                        </td>
                        <td width="32" align="right">
                            <asp:ImageButton ID="imgBtnAddBusiness"  ImageUrl="~/images/btn_add_business.png"  onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_business.png');" CausesValidation="false"
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_business_lit.png');" runat="server" AlternateText="Add A Business" OnClientClick="return disableAddBusiness()"  onclick="imgBtnAddBusiness_Click" />
                        </td>
                        </tr>
                        </table>
                        <table id="tblOutreachBusinesses" width="100%" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                        <td><table runat="server" name="gridMain"  ID="gridMain" width="100%"></table>
                            <div id="pagernav"></div><img src="images/spacer.gif" width="1" height="5" alt="spacer image" /><br /></td>
                        </tr>
                        </table>
                        <asp:Label ID="lblBusinsessContactMsg" CssClass="noteText" runat="server" Text="* Added Business Contacts." Visible="false"></asp:Label>
                    </div>
                    <div id="idAssignedClinics" runat="server" visible="false">
                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                        <td width="100%" align="left" nowrap="nowrap"><img src="images/clinic_color_key.gif" width="375" height="23" alt="clinic row color codes" /></td>
                        <td width="130" align="right" nowrap="nowrap" class="class2">&nbsp;</td>
                        <td width="32" align="right">&nbsp;</td>
                        </tr>
                        </table>
                        <table id="tblAssignedClinics" width="100%" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                        <td><table runat="server" name="gridMainAC"  ID="gridMainAC" width="100%"></table>
                            <div id="pagerAC"></div><img src="images/spacer.gif" width="1" height="5" alt="spacer image" /><br /></td>
                        </tr>
                        </table>
                    </div>
                    <div id="idNationalContracts" runat="server" visible="false">
                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                    <tr>
                    <td align="left" nowrap="nowrap" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border: 0px solid #999; padding: 5px;" >National Accounts should <u>NOT</u> be reached out to – unless you have an assigned clinic to perform for that business.
                    </td>
                    </tr>
                    <tr align="right" >
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="40%">
                            <tr>
							    <td width="142" align="right" nowrap="nowrap" style="vertical-align: middle;"><a class="class2" runat="server" id="lnkRemoveClients" href="#" title="Remove Clients">Remove Clients&nbsp;</a></td>
                                 <td align="right" width="23" style="vertical-align: middle;"><img src="images/btn_remove.png" alt="Remove Clients" width="18" height="18" runat="server" id="btnRemoveClients" style="cursor: hand; padding-right: 5px" onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');" /></td>
                                <td width="142" align="right" nowrap="nowrap" style="vertical-align:middle;"><a class="class2" id="lnkSearch" href="#" title="Search for Client" >Search for Client&nbsp;</a></td>
                                <td align="right" width="23" style="vertical-align:middle;"><img src="images/btn_business_search.png" alt="Search for Client" width="18" height="18" id="btnSearch" style="cursor:hand;padding-right:5px" onmouseover="javascript:MouseOverImage(this.id,'images/btn_business_search_lit.png');" onmouseout="javascript:MouseOutImage(this.id,'images/btn_business_search.png');" /></td>
                                <td width="50px" align="right" nowrap="nowrap" style="vertical-align:middle;"><a class="class2" id="lnkRefreshGrid" href="#" title="Refresh" >Refresh&nbsp;</a></td>
                                <td align="right" width="23" style="vertical-align:middle;"><img src="images/btn_business_refresh.png" alt="Refresh" width="18" height="18" id="btnRefreshGrid" style="cursor:hand; padding-right:5px" onmouseover="javascript:MouseOverImage(this.id,'images/btn_business_refresh_lit.png');" onmouseout="javascript:MouseOutImage(this.id,'images/btn_business_refresh.png');" /></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                    <table runat="server" name="gridMainNC"  ID="gridMainNC" width="100%"></table>
                    <div id="pagerNC"></div><img src="images/spacer.gif" width="1" height="5" alt="spacer image" /><br />
                    </div>
                </div>
                </td>
                </tr>
                <tr>
                <td align="right"></td>
                </tr>
                </table>
                            
                <%--<table runat="server" name="gridMain"  ID="gridMain" width="50%"></table>
                <div id="pagernav"></div><img src="images/spacer.gif" width="1" height="5" /><br />--%>                            
                <!-- END JQGRID--></td>
            <td width="268" align="left" valign="top">
                <table width="268" border="0" cellspacing="0" cellpadding="0">
                    <tr id="tblContactLog">
                      <td class="backgroundGradientLog">
                        <table width="268" border="0" cellspacing="8" cellpadding="0">
                            <tr>
                                <td class="logTitle">Log Business Contact</td>
                            </tr>
                            <tr>
                                <td><asp:ValidationSummary ID="ValidationSummary1" runat="server" class="formFields" DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" ShowSummary="false"/></td>
                            </tr>
                            <tr>
                                <td><span class="logSubTitles">Business Contacted: </span><br />
                                    <asp:DropDownList ID="ddlAssignedBusiness" runat="server" CssClass="formFields" AutoPostBack="false" onchange="javascript:displayContact(this.value)" CausesValidation="false" Width="249px"></asp:DropDownList>
                                    <asp:TextBox id="txtAssignedBusiness" CssClass="formFields" visible="true" runat="server" Width="247px" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqBusiness" runat="server" ControlToValidate="ddlAssignedBusiness" Display="None" ErrorMessage="Please select Business Contacted" InitialValue="0">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr><td style="padding-right:2px"><span class="logSubTitles">Contact First Name:</span><br />
                                        <asp:TextBox ID="txtContactFirstName" runat="server" Width="115px" CssClass="formFields"  MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="txtContactFirstName" Enabled="false" Display="None" ErrorMessage="Please enter contact First Name" CssClass="logSubTitles" Font-Bold="True" Font-Names="Calibri" Font-Size="Medium">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="txtContactFirstNameEV" ControlToValidate="txtContactFirstName" Display="None" ValidationExpression="[^,<>@#&]+" runat="server" ErrorMessage="Contact First Name: , < > @ # & Characters are not allowed."></asp:RegularExpressionValidator>
                                    </td>
                                    <td><span class="logSubTitles">Contact Last Name:</span><br />
                                        <asp:TextBox ID="txtContactLastName" runat="server" Width="115px" CssClass="formFields" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtContactLastName" Enabled="false" Display="None" ErrorMessage="Please enter contact Last Name" CssClass="logSubTitles" Font-Bold="True" Font-Names="Calibri" Font-Size="Medium">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="txtContactLastNameEv" ControlToValidate="txtContactLastName" Display="None" ValidationExpression="[^,<>@#&]+" runat="server" ErrorMessage="Contact Last Name: , < > @ # & Characters are not allowed."></asp:RegularExpressionValidator>
                                    </td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                            <tr id="rowJobTitle" style="display:none;" >
                                <td><span class="logSubTitles">Job Title:</span><br />
                                    <asp:TextBox ID="txtTitle" CssClass="formFields" runat="server" MaxLength="100" Width="210px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtTitleReqFV" runat="server" ControlToValidate="txtTitle" Enabled="false" Display="None" ErrorMessage="Please enter Job Title" CssClass="logSubTitles" Font-Bold="True" Font-Names="Calibri" Font-Size="Medium">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="txtTitleRegEV" ControlToValidate="txtTitle" Display="None" ValidationExpression="[^,<>@#&]+" runat="server" ErrorMessage="Job Title: , < > @ # & Characters are not allowed." ValidationGroup="contactLog">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="logSubTitles">Contact Date:</span><uc1:PickerAndCalendar ID="PickerAndCalendar1" runat="server" /></td>
                            </tr>
                            <tr>
                                <td><span class="logSubTitles" id="lblOutreachStatuses"></span><br />
                                <asp:DropDownList ID="ddlOutreach" runat="server" CausesValidation="false" CssClass="formFields" Width="215px" onchange="javascript:validateRequired(this);"></asp:DropDownList>
                                <asp:TextBox id="txtOutreach" CssClass="formFields" visible="true" runat="server" Width="213px" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqOutreach" runat="server" ControlToValidate="ddlOutreach" Display="None" ErrorMessage="Please select Outreach Status" InitialValue="0">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="logSubTitles">Feedback/Notes:<br /></span>
                                        <label for="textarea"></label>
                                        <asp:TextBox ID="txtFeedBack" class="formFields" runat="server" Columns="39" Rows="10" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="txtFeedBackReqFV" runat="server" ControlToValidate="txtFeedBack" Enabled="false" Display="None" ErrorMessage="Please enter Notes" CssClass="logSubTitles" Font-Bold="True"  Font-Names="Calibri" Font-Size="Medium">*</asp:RequiredFieldValidator>
                                        <table width="100%">
                                            <tr>
                                                <td style="text-align: left; vertical-align: top; ">
                                                    <span class="logSubTitles">
                                                    <span style="font-size: xx-small">*<asp:Label ID="idLabel" runat="server"></asp:Label> characters allowed</span></span>
                                                </td>
                                                <td style="text-align: right; vertical-align: top; "><span class="logSubTitles"><asp:Label ID="idCount" runat="server"></asp:Label></span></td>
                                            </tr>
                                        </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="btnSubmit" runat="server" AlternateText="Submit Entry" CausesValidation="true"
                                        ImageUrl="images/btn_submit.png"  OnClientClick="javascript:return setOutreachField();" OnClick="btnSubmit_Click" />
                                </td>
                            </tr>
                        </table>
                      </td>
                    </tr>
                    <tr id="tblScheduledClinicsTabMesg">
                        <td>
                            <p>&nbsp;</p><br />
                            <table width="268" cellspacing="15" cellpadding="0" style="border:1px; border-style: solid; border-color: #999;" class="wagsInstrBoxRoundedCorners">
                            <tr>
                                <td class="logSubTitles"><b>Logging Scheduled Clinics:</b></td>
                            </tr>
                            <tr>
                                <td align="left" class="formFields" style="line-height: 22px;" >
                                    To confirm, complete or cancel a Scheduled Clinic, 
                                    click the Open button in the Clinic Details column of the clinic's row. 
                                    Please review and complete all of the necessary Clinic Details 
                                    before confirming or completing a clinic.
                                </td>
                            </tr>
                            <tr id="toAssignedRow" runat="server" visible="false">
                                <td align="left" class="formFields" style="line-height: 22px;color:#5B9EC6" >
                                     <b>*LEAD STORE</b> - When another store number
                                     is present, a lead store has been assigned to
                                     a group of corporate clinics. Do not contact
                                     the client, it is the lead store's responsibility to
                                     "Confirm" the clinic.
                                </td>
                            </tr>
                            <tr id="toLeadRow" runat="server" visible="false">
                                <td align="left" class="formFields" style="line-height: 22px;color:#5B9EC6">
                                    <b>*ASSIGNED STORE</b> – To prevent multiple
                                    Calls to the same contact, your store has been
                                    assigned as the Lead Store for the clinics
                                    highlighted in blue. Your store is responsible
                                    for Confirming these clinics only. The clinic
                                    will be performed by the store listed in the
                                    Status column, Please confirm the clinic, enter
                                    the Contacts name in the Clinic Details page
                                    and click the <b>Confirmed Clinic</b> button.

                                </td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
      </table>
      <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 13px; text-align:center; font-weight:bold;"></div>
    </td>
  </tr>
</table>
<asp:HiddenField ID="hdOutreachStatus" runat="server" Value="false" />
<asp:HiddenField ID="hdDeploymentDate" runat="server" />
<ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
</form>
</body>
</html>
<script type="text/javascript">
    var selectedRemoveDNCClients = [];
    function bindNcClientsData() {
        var nc_data = "";

        nc_data = <%= this.nationalContracts %>;
        selectedRemoveDNCClients = [];
        var mydata = [];
        var varColModel = [
                             { name: 'clientName', index: 'clientName', search: true }
                            ];
        var isAdminUser = '<%=commonAppSession.LoginUserInfoSession.IsAdmin%>';
        if (isAdminUser.toLowerCase() == "true") {
            varColModel = [
                                     { name: 'ncCheckbox', width: 5, editable: true,  edittype: 'checkbox', editoptions: { value: "True:False" }, formatter: checkboxFormatter, formatoptions: { disabled: false}, search: false},
                                     { name: 'clientName', index: 'clientName', search: true}
            ];
        }
  
        if(nc_data != "")
            mydata = <%= this.nationalContracts %>;        

        jQuery("#gridMainNC").jqGrid({
            datatype: "local",
            data: mydata,
            pager: '#pagerNC',
            sortname: 'clientName',
            rowNum: 25,
            sortorder: "asc",
            sortable: false,
            toppager: true,
            scrollOffset: 0,
            columnReordering: false,
            height: 'auto',
            width: 625,
            ignoreCase: true,  
            colNames: (isAdminUser.toLowerCase() == "true" ? ['', 'Client Name'] : ['Client Name']),
            colModel: varColModel            
        });
        jQuery("#gridMainNC").jqGrid('navGrid', '#pagerNC', { add: false, edit: false, del: false, search:false, refresh:false });

        $("#btnSearch").click(function(){
            jQuery("#gridMainNC").jqGrid('searchGrid',
            {sopt:['cn','bw','eq','ne','lt','gt','ew'],closeAfterSearch:true,closeAfterReset: true, closeOnEscape: true}
            );
        });

        $("#lnkSearch").click(function(){
            jQuery("#gridMainNC").jqGrid('searchGrid',
	            {sopt:['cn','bw','eq','ne','lt','gt','ew'],closeAfterSearch:true,closeAfterReset: true, closeOnEscape: true}
            );
        });

        $("#btnRemoveClients").click(function(){          
            if (selectedRemoveDNCClients.length > 0) {
                showDeleteDNCClientWarning("Are you sure you want to remove Do Not Call National Contracts client?", selectedRemoveDNCClients.join(','));
            } 
            bindNcClientsData();
        });

        $("#lnkRemoveClients").click(function(){ 
            if (selectedRemoveDNCClients.length > 0) {
                showDeleteDNCClientWarning("Are you sure you want to remove Do Not Call National Contracts client?", selectedRemoveDNCClients.join(','));
            } 
            bindNcClientsData();
        });

         $("#btnRefreshGrid").click(function(){
            jQuery('#gridMainNC').jqGrid('GridUnload');
            bindNcClientsData();
        });

        $("#lnkRefreshGrid").click(function(){
             jQuery('#gridMainNC').jqGrid('GridUnload');
             bindNcClientsData();
        });
    }
   
    function checkboxFormatter(cellvalue, options, rowObject) {
        if(selectedRemoveDNCClients.length > 0 && selectedRemoveDNCClients.indexOf(' '+ rowObject.pk) > -1) //Don't remove space
            return "<input type='checkbox' checked onchange='javascript:selectDNCClient(this," + rowObject.pk + ")'>";
        else
            return "<input type='checkbox' onchange='javascript:selectDNCClient(this," + rowObject.pk + ")'>";
    }

    function selectDNCClient(ctrl, selected_pk){
        selected_pk = ' ' + selected_pk; //Don't remove space
        
        if(ctrl.checked)
            selectedRemoveDNCClients.push(selected_pk);
        else
            selectedRemoveDNCClients.splice( $.inArray(selected_pk, selectedRemoveDNCClients), 1 );     
    }
   
   /**********  Begin Assigned Clinic *****************/
    function bindAssignedClinicData() {
        var ac_data = "";
        var mydata = [];
        ac_data = '<%=commonAppSession.SelectedStoreSession.AssignedClinicsJson%>';
        var varColModel = [
                             { name: 'businessPk', index: 'businessPk', hidden:true},
                             { name: 'businessName', index: 'businessName',  width: 250},                             
                             { name: 'firstName', index: 'firstName', hidden:true},
                             { name: 'lastName', index: 'lastName', hidden:true},
                             { name: 'contactName', index: 'contactName', hidden:true},
                             { name: 'contactPhone', index: 'contactPhone', hidden:true},
                             { name: 'contactEmail', index: 'contactEmail', hidden:true},
                             { name: 'ClinicDate', index: 'ClinicDate', hidden:true},
                             { name: 'accountType', index: 'accountType', width: 90},
                             { name: 'contacts', index: 'contacts', width: 50, hidden:true},
                             { name: 'lastContact', index: 'lastContact', width: 50, hidden:true},
                             { name: 'contactStatus', index: 'contactStatus', width: 130},
                             { name: 'clinicDetails', index: 'clinicDetails', align: "center", formatter: clinicDetailsLink, width: 110},
                             { name: 'outreachStatusId', index: 'outreachStatusId', align: "right", sorttype: "string", hidden: true },
                             { name: 'totalImmAdministered', index: 'totalImmAdministered', align: "right", sorttype: "string", hidden: true },
                             { name: 'isCurrentSeason', index: 'isCurrentSeason', width: 50, hidden:true},
                             { name: 'isClinicUnderLeadStore', index: 'isClinicUnderLeadStore', hidden:true}
                            ];
    
        if(ac_data != "")
            mydata = <%=commonAppSession.SelectedStoreSession.AssignedClinicsJson%>;

        jQuery("#gridMainAC").jqGrid({
            datatype: "local",
            data: mydata,
            pager: '#pagerAC',
            sortname: 'id',
            rowNum: 25,
            sortorder: "desc",
            sortable: false,
            toppager: true,
            scrollOffset: 0,
            columnReordering: false,
            height: 'auto',
            width: 625,
            colNames: ['BusinessPk','Clinic Name', 'First Name', 'Last Name', 'Contact', 'Phone', 'Email', 'Clinic Date', 'Clinic Type', 'Logged', 'Last Contact', 'Status', 'Clinic Details', 'outreachStatusId', 'totalImmAdministered', 'isCurrentSeason', 'isClinicUnderLeadStore'],
            colModel: varColModel,
            loadComplete: function () { setAssignedClinicsRowColor() }
        });
        jQuery("#gridMainAC").jqGrid('navGrid', '#pagerAC', { add: false, edit: false, del: false, search: false, refresh: false });
    }

     function clinicDetailsLink(cellvalue, options, rowObject) {     
         return "<img src='images/btn_open.png' alt='View/Edit Clinic Details' onClick=showClinicDetails(" + rowObject.businessPk + ",'" + encodeURIComponent( rowObject.accountType) + "'," + rowObject.outreachStatusId + "," + rowObject.isCurrentSeason + ",'" + rowObject.isClinicUnderLeadStore +"'); onmouseover=showLightButton(this); onmouseout=showOriginalButton(this); />";
    }
    
    function showLightButton(row_obj) {
        row_obj.setAttribute("src", "images/btn_open_lit.png");
    }

    function showOriginalButton(row_obj) {
        row_obj.setAttribute("src", "images/btn_open.png");
    }

    function showClinicDetails(business_clinic_pk, account_type, outreach_status_id, is_current_season, is_clinic_under_leadstore) {    
        $.ajax({
            url: "walgreensHome.aspx/setSelectedBusinessPk",
            type: "POST",
            data: "{'business_clinic_pk':'" + business_clinic_pk + "', 'account_type':'" + account_type + "', 'outreach_status_id':'" + outreach_status_id + "', 'is_clinic_under_leadstore':'" + is_clinic_under_leadstore + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
            if(decodeURIComponent(account_type) == "Charity (HHS Voucher)")
                window.location.href = "walgreensCharityProgramClinicDetails.aspx";
            else if(account_type == "Local") {
                window.location.href = "walgreensLocalClinicDetails.aspx";
            }
            else if(decodeURIComponent(account_type)=="Uploaded Local")
                window.location.href="walgreensCorporateClinicDetails.aspx";
            else if(decodeURIComponent(account_type)=="Community Outreach")
                window.location.href="walgreensCommOutreachClinicDetails.aspx";
            else 
                window.location.href = "walgreensCorporateClinicDetails.aspx";
            return false;
            }
        });
    }    
    
    function showBusinessContactLog(business_pk, outreach_status_id, outreach_effort_id) {
        $.ajax({
            url: "walgreensHome.aspx/setSelectedBusinessContactLog",
            type: "POST",
            data: "{'business_pk':'" + business_pk + "', 'outreach_status_id':'" + outreach_status_id + "', 'outreach_effort_id':'" + outreach_effort_id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
            window.location.href = "walgreensContactLog.aspx";
            return false;
            }
        });        
    }
    /**********  End Assigned Clinic *****************/

    function addDays(givenDate, days) {
        var thisDate = givenDate;
        thisDate.setDate(thisDate.getDate() + days);
        return thisDate;
    }
    function subtractDays(givenDate, days) {
        var thisDate = givenDate;
        thisDate.setDate(thisDate.getDate() - days);
        return thisDate;
    }

    function bindOutreachBusinesses(){
        var mydata = [];
        var ob_data = "";
        ob_data = ('<%=commonAppSession.SelectedStoreSession.OutreachBusinessesJson%>');
        var outreach_effort = ('<%=commonAppSession.SelectedStoreSession.OutreachProgramSelected%>');
        var varColModel = [
                             { name: 'businessId', index: 'businessId', hidden: true },
                             { name: 'businessName', index: 'businessName', width: 125 },
                             { name: 'Industry', index: 'Industry', width: 75 },
                             { name: 'employmentSize', index: 'employmentSize', width: 30, align: "center", sorttype: "int" },
                             { name: 'phone', index: 'phone', width: 75 },
                             { name: 'address', index: 'address', hidden: true },
                             { name: 'contacts', index: 'contacts', width: 55, align: "center", sorttype: "string", cellattr: function (rowId, val, rowObject, cm, rdata) { return "title='" + rowObject.contactsToolTip.replace('|', '\r\n') + "'"; } },
                             { name: 'lastContactGrid', index: 'lastContactGrid', width: 70, align: "center", sorttype: "string", cellattr: function (rowId, val, rowObject, cm, rdata) { return "title='" + rowObject.lastContactToolTip.replace('|', '\r\n') + "'"; } },
                             { name: 'outreachStatusGrid', index: 'outreachStatusTitleGrid', width: 82, align: "center", sorttype: "string", formatter: status, cellattr: function (rowId, val, rowObject, cm, rdata) { return "title='" + rowObject.statusToolTip.replace('|', '\r\n') + "'"; } },
                             { name: 'outreachStatusIdGrid', index: 'outreachStatusIdGrid', align: "right", sorttype: "string", hidden: true },
                             { name: 'contactName', index: 'contactName', align: "right", sorttype: "string", hidden: true },
                             { name: 'firstName', index: 'firstName', align: "right", sorttype: "string", hidden: true },
                             { name: 'lastName', index: 'lastName', align: "right", sorttype: "string", hidden: true },
                             { name: 'jobTitle', index: 'jobTitle', align: "right", sorttype: "string", hidden: true },
                             { name: 'lastContact', index: 'lastContact', width: 55, align: "center", sorttype: "string", hidden: true },
                             { name: 'outreachStatus', index: 'outreachStatus', width: 132, align: "center", sorttype: "string", hidden: true },
                             { name: 'outreachStatusId', index: 'outreachStatusId', align: "right", sorttype: "string", hidden: true },
                             { name: 'createdDate', index: 'createdDate', align: "right", sorttype: "string", hidden: true },
                             { name: 'isExistingBusiness', index: 'isExistingBusiness', align: "right", sorttype: "string", hidden: true },                             
                             { name: 'outreachContactDate', index: 'outreachContactDate', align: "right", sorttype: "string", hidden: true },
                             { name: 'sicCode', index: 'sicCode', align: "right", sorttype: "string", hidden: true },
                             { name: 'sicPriority', index: 'sicPriority', align: "right", sorttype: "string", hidden: true },
                             ];

        if(ob_data != "")
            mydata = <%=commonAppSession.SelectedStoreSession.OutreachBusinessesJson%>;

        jQuery("#gridMain").jqGrid({
                    datatype: "local",
                    data: mydata,
                    pager: '#pagernav',
                    sortname: 'id',
                    rowNum: 25,
                    sortorder: "desc",
                    sortable: false,
                    toppager: true,
                    scrollOffset: 0,
                    columnReordering: false,
                    height: 'auto',
                    width: 625,
                    colNames: ['businessId', 'Assigned Businesses', 'Industry', 'Emp', 'Phone', 'Address', 'Contacted', 'Last Contact', 'Status', 'Status', 'Contact Name', 'First Name', 'Last Name', 'jobTitle', 'sicCode', 'lastContact', 'outreachStatus', 'outreachStatusId', 'createdDate', 'isExistingBusiness', 'outreachContactDate','sicPriority'],
                    colModel: varColModel,
                    onSortCol: function (name, index) { },
                    loadComplete: function () { colorCode() },
                    onSelectRow: function (id) {
                        $('#ddlOutreach').removeAttr('disabled');
                        var grid = $("#gridMain");
                        $("#ddlAssignedBusiness").val(grid.getRowData(id).businessId);
                        if ($("#ddlAssignedBusiness").val() == null) $("#ddlAssignedBusiness").val(0);
                        $("#txtContactFirstName").val(grid.getRowData(id).firstName);
                        $("#txtContactLastName").val(grid.getRowData(id).lastName);
                        $("#txtTitle").val(grid.getRowData(id).jobTitle);
                        var OutreachProgramSelectedId = '<%=commonAppSession.SelectedStoreSession.OutreachProgramSelectedId%>';
                        if (grid.getRowData(id).outreachStatusId == "" || grid.getRowData(id).outreachStatusId == 0 || grid.getRowData(id).outreachStatusId == 7 || grid.getRowData(id).outreachStatusId == 6)
                        {
                            $("#ddlOutreach").val(0);
                            $("#rowJobTitle").css("display", "none");
                            setRequireFieldEnabled(false);
                        }
                       else
                       {
                            $("#ddlOutreach").val(grid.getRowData(id).outreachStatusId);
                            if (grid.getRowData(id).outreachStatusId == 4) {
                                $("#rowJobTitle").removeAttr("style");    
                                $("#txtTitle").val(grid.getRowData(id).jobTitle);
                                setRequireFieldEnabled(true);
                            }
                            else {
                                $("#rowJobTitle").css("display", "none");
                                setRequireFieldEnabled(false);
                            }
                       }
                    }
                });
                jQuery("#gridMain").jqGrid('navGrid', '#pagernav', { add: false, edit: false, del: false, search: false, refresh: false });
    }    

    function setAssignedClinicsRowColor() {
        var rowIDs = jQuery("#gridMainAC").getDataIDs();
        //var current_date = new Date();
        for (var i = 0; i < rowIDs.length; i = i + 1) {
            rowData = jQuery("#gridMainAC").getRowData(rowIDs[i]);
            var trElement = jQuery("#" + rowIDs[i], jQuery('#gridMainAC'));
            var clinic_date = new Date(new Date(rowData.ClinicDate).getFullYear(), new Date(rowData.ClinicDate).getMonth(), new Date(rowData.ClinicDate).getDate());
            var curr_date = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            if (rowData.isClinicUnderLeadStore == "true") {
                trElement.removeClass('ui-widget-content');
                trElement.addClass('rowClinicUnderLeadstore');  
            }
            else if (rowData.contactStatus.indexOf("Contact Client") != -1) {
                    trElement.removeClass('ui-widget-content');
                    trElement.addClass('rowColor7');
            }
            else if (rowData.contactStatus.indexOf("Provide Vouchers") != -1) {
                trElement.removeClass('ui-widget-content');
                trElement.addClass('rowColor7');                    
            }
            else if(rowData.contactStatus == "Confirmed") {
                trElement.removeClass('ui-widget-content');
                //$(trElement.selector).css('background-color', '#F6FAC5');
                if(clinic_date < curr_date)
                {
                    $(trElement.selector).css('background-color', '#ffcccc');
                }
            }
            else if(rowData.contactStatus == "Vouchers Provided") {
                trElement.removeClass('ui-widget-content');
                //$(trElement.selector).css('background-color', '#F6FAC5');
                if(clinic_date < curr_date)
                {
                    $(trElement.selector).css('background-color', '#ffcccc');
                }
            }
            else if(rowData.contactStatus != "Completed")
            {
                if(rowData.contactStatus != "Cancelled")
                {
                    trElement.removeClass('ui-widget-content');
                    $(trElement.selector).css('background-color', '#F5A0A0');
                }
            }
        }
    }

    function colorCode() {        
        var rowIDs = jQuery("#gridMain").getDataIDs();
        for (var i = 0; i < rowIDs.length; i = i + 1) {
            rowData = jQuery("#gridMain").getRowData(rowIDs[i]);
            var trElement = jQuery("#" + rowIDs[i], jQuery('#gridMain'));
            var outreachContactDate = Date.parse(rowData.outreachContactDate);
            var lastContact = Date.parse(rowData.lastContact);
            var lastContactDate = new Date(lastContact);
            var businessName = rowData.businessName;            
            var sevenDay = new Date();
            sevenDay = subtractDays(sevenDay, 7);
            var fourteenDay = new Date();
            fourteenDay = subtractDays(fourteenDay, 14);

            var today = new Date();
            var deploymentDate = new Date(Date.parse($("#hdDeploymentDate").val()));
            var deploymentDate7 = addDays(deploymentDate, 7);

            var deploymentDate1 = new Date(Date.parse($("#hdDeploymentDate").val()));
            var deploymentDate14 = addDays(deploymentDate1, 14);

            var business_create_date = new Date(Date.parse(rowData.createdDate));
            var OutreachProgramSelectedId = '<%=commonAppSession.SelectedStoreSession.OutreachProgramSelectedId%>';

            if (rowData.outreachStatus.indexOf("Contract Initiated") != -1)
                continue;
            else if(rowData.outreachStatus.indexOf("Contract Sent") != -1)
            {
                trElement.removeClass('ui-widget-content');
                trElement.addClass('rowBgGreen');
                continue;

            }
            else if(rowData.outreachStatus.indexOf("Contract Rejected") != -1)
            {
                trElement.removeClass('ui-widget-content');
                trElement.addClass('rowBgOrange');
                continue;
            }

            if (rowData.outreachStatus.indexOf("Event Scheduled") != -1)
            {
                if(OutreachProgramSelectedId == 1 && rowData.sicCode > 0)
                {
                    $(trElement.selector).css('color', '#418200');
                    $(trElement.selector + " a").css('color', '#418200');
                }
                continue;
            }

            if (rowData.outreachStatus.indexOf("No Client Interest") != -1 || rowData.outreachStatus.indexOf("Business Closed") != -1) {
                $(trElement.selector).css('color', '#999999');
                $(trElement.selector + " a").css('color', '#999999');
            }
            else if ((outreachContactDate > fourteenDay) && (outreachContactDate < sevenDay)) {
                trElement.removeClass('ui-widget-content');
                trElement.addClass('rowColor7');

                if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                    $(trElement.selector).css('color', '#418200');
                    $(trElement.selector + " a").css('color', '#418200');
                }
            }
            else if (outreachContactDate < fourteenDay) {
                trElement.removeClass('ui-widget-content');
                trElement.addClass('rowColor14');

                if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                    $(trElement.selector).css('color', '#418200');
                    $(trElement.selector + " a").css('color', '#418200');
                }
            }
            else if (rowData.outreachContactDate == "NaN" || rowData.outreachContactDate == null || rowData.outreachContactDate == "") {

                if (rowData.createdDate != "" && rowData.businessName.indexOf("*") > -1) {
                    if ((business_create_date > fourteenDay) && (business_create_date < sevenDay)) {
                        trElement.removeClass('ui-widget-content');
                        trElement.addClass('rowColor7');

                        if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                            $(trElement.selector).css('color', '#418200');
                            $(trElement.selector + " a").css('color', '#418200');
                        }
                    }
                    else if (business_create_date < fourteenDay) {
                        trElement.removeClass('ui-widget-content');
                        trElement.addClass('rowColor14');

                        if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                            $(trElement.selector).css('color', '#418200');
                            $(trElement.selector + " a").css('color', '#418200');
                        }
                    }
                    else{
                         if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                            $(trElement.selector).css('color', '#418200');
                            $(trElement.selector + " a").css('color', '#418200');
                        }
                    }
                }
                else {
                    if ((deploymentDate7 < today) && (today < deploymentDate14)) {
                        trElement.removeClass('ui-widget-content');
                        trElement.addClass('rowColor7');

                        if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                            $(trElement.selector).css('color', '#418200');
                            $(trElement.selector + " a").css('color', '#418200');
                        }
                    }
                    if (today > deploymentDate14) {
                        trElement.removeClass('ui-widget-content');
                        trElement.addClass('rowColor14');

                        if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                            $(trElement.selector).css('color', '#418200');
                            $(trElement.selector + " a").css('color', '#418200');
                        }
                    }
                    else{
                         if (rowData.isExistingBusiness == "True" && OutreachProgramSelectedId == 3) {
                            $(trElement.selector).css('color', '#418200');
                            $(trElement.selector + " a").css('color', '#418200');
                        }
                    }
                }
            }

            if(OutreachProgramSelectedId == 1 && rowData.sicPriority > 0 && rowData.outreachStatus.indexOf("No Client Interest") == -1)
            {
                if(rowData.sicPriority == 1)
                {
                    $(trElement.selector).css('color', '#006699');
                    $(trElement.selector + " a").css('color', '#006699');
                }
                else if(rowData.sicPriority == 2)
                {
                    $(trElement.selector).css('color', '#418200');
                    $(trElement.selector + " a").css('color', '#418200');
                }
            }
        }
    }

    $(document).ready(function () {    
        bindOutreachBusinesses();
        if ($("#hfSelectedBusinessTypeTabId").val() == "3")
            bindNcClientsData();

        bindAssignedClinicData();
    });

function status(cellvalue, options, rowObject) { 
    return rowObject.outreachStatusTitleGrid;
}
</script>
