﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using tdEmailLib;
using System.IO;
using TdWalgreens;
using System.Threading;
using System.Globalization;
using System.Collections.Generic;
using NLog;

public partial class walgreensClinicAgreementForUser : Page
{
    #region ------------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.ddlLegalState.bindStates();
            this.bindContractAgreement();

            //Set contract agreement viewed status
            this.dbOperation.setContractAgreementReviewedStatus(this.contactLogPk, 2);
            this.businessUsersLog(true);
        }
        else
        {
            this.setMinMaxDates();
            Int32.TryParse(this.hfContactLogPk.Value, out this.contactLogPk);
        }

        this.setImageButtons();
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page by business user with contactlogpk: {2} - START", "btnSubmit_Click",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.contactLogPk.ToString());
        
        Validate("BusinessUser");
        if (Page.IsValid)
        {
            if (!isValidExpiryDates())
                return;

            if (!string.IsNullOrEmpty(this.hfBusinessUserEmail.Value) && !string.IsNullOrEmpty(this.hfContactLogPk.Value) && Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value] != null)
            {
                int outreach_business_pk = 0;
                int store_id = 0;
                string store_state = this.hfStoreState.Value;
                string var_pdf_files = string.Empty;

                Int32.TryParse(this.hfOutreachBusinessPk.Value, out outreach_business_pk);
                Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);
                DataSet ds_clinic_agreement = (DataSet)Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value];
                string business_name = ds_clinic_agreement.Tables[0].Rows[0]["businessName"].ToString();
                DataTable dt_approved_contract_details;
                int return_value = this.dbOperation.insertClinicAgreementApproval(this.contactLogPk, rbtnApprove.Checked == true ? 1 : 0, this.hfBusinessUserEmail.Value, this.txtElectronicSign.Text, "", "", this.txtNotes.Text, this.prepareBusinessUserXml(), Convert.ToDateTime(this.hfLastUpdatedDate.Value), out dt_approved_contract_details);
                this.logger.Info("Database Operation {0} done with return value {1}", "insertClinicAgreementApproval", return_value);     
                
                if (return_value == -2)
                {
                    if (dt_approved_contract_details.Rows.Count > 0)
                    {
                        Session.Remove(this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "approvedContractAlreadyExists"), dt_approved_contract_details.Rows[0]["electronicSign"].ToString(), dt_approved_contract_details.Rows[0]["email"].ToString()) + "');window.location.href='thankYou.aspx?key=fromUserPage';", true);
                    }
                }
                else if (return_value == -6)
                {
                    Session.Remove(this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "AgreementStatusChanged") + "');window.location.href='thankYou.aspx?key=fromUserPage';", true);
                    return;
                }
                else if (return_value != -1)
                {
                    EmailOperations email_operations = new EmailOperations();
                    if (Convert.ToBoolean(this.hfIsModified.Value))
                    {
                        this.logger.Info("Modified Agreement was submitted by business user with contactlogpk: {0}", this.contactLogPk.ToString());    
                        string email_to = ApplicationSettings.getAllSpecialUserEmails("ModifyApprovedAgreement");
                        email_operations.sendClinicAgreementToWalgreensUser(email_to, "", business_name, Convert.ToInt32(hfContactLogPk.Value), "", regional_name: "Admin", store_id: store_id);
                        if (rbtnApprove.Checked) //Send to store users if it is approved after modification.
                        {
                            DataTable dt_user_emails = dbOperation.getStoreUsersEmails(store_id);
                            if (dt_user_emails.Rows.Count > 0)
                                email_operations.sendClinicAgreementToWalgreensUser(dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString(), dt_user_emails.Rows[0]["storeManagerEmail"].ToString(), business_name, Convert.ToInt32(hfContactLogPk.Value), "");
                        }
                    }
                    else
                    {
                        DataTable dt_user_emails = dbOperation.getStoreUsersEmails(store_id);
                        if (dt_user_emails.Rows.Count > 0)
                        {
                            List<string> contract_pdf_files = new List<string>();
                            DataSet ds_form_data = this.dbOperation.getContractVoucherVarFormData(outreach_business_pk, 0);

                            if (ds_form_data != null && ds_form_data.Tables.Count > 0)
                            {
                                string business_pk;
                                string pdf_file_name;
                                byte[] pdf_form;
                                WagContractPdfMaker contract_pdf_maker = new WagContractPdfMaker();

                                DataTable dt_contract_data = ds_form_data.Tables[0];
                                business_pk = dt_contract_data.Rows[0]["businessPk"].ToString();
                                this.logger.Info("Generating PDFs for vouchers & var forms..");    

                                if (dt_contract_data.Select("showVarPdf = 'True'").Count() > 0)
                                {
                                    pdf_file_name = Path.Combine(ApplicationSettings.GetContractVoucherVARPdfFilesPath + "VAR_" + business_pk + "_" + store_id.ToString() + "_" + store_state + "_EN" + "_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
                                    pdf_form = contract_pdf_maker.getVoucherFormBytes(ds_form_data, "VAREN");
                                    if (pdf_form != null)
                                    {
                                        using (var file_stream = new FileStream(pdf_file_name, FileMode.Create))
                                        {
                                            file_stream.Write(pdf_form, 0, pdf_form.Length);
                                            file_stream.Close();
                                        }
                                        var_pdf_files = pdf_file_name.Substring(pdf_file_name.LastIndexOf("\\") + 1) + "|";
                                        contract_pdf_files.Add(pdf_file_name);
                                    }

                                    pdf_file_name = Path.Combine(ApplicationSettings.GetContractVoucherVARPdfFilesPath + "VAR_" + business_pk + "_" + store_id.ToString() + "_" + store_state + "_ESP" + "_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
                                    pdf_form = contract_pdf_maker.getVoucherFormBytes(ds_form_data, "VARSP");
                                    if (pdf_form != null)
                                    {
                                        using (var file_stream = new FileStream(pdf_file_name, FileMode.Create))
                                        {
                                            file_stream.Write(pdf_form, 0, pdf_form.Length);
                                            file_stream.Close();
                                        }
                                        var_pdf_files += var_pdf_files = pdf_file_name.Substring(pdf_file_name.LastIndexOf("\\") + 1) + "|";
                                        contract_pdf_files.Add(pdf_file_name);
                                    }
                                }

                                if (dt_contract_data.Select("showVoucherPdf = 'True'").Count() > 0 && ds_form_data.Tables.Count > 1)
                                {
                                    if (ds_form_data.Tables[1].Select("groupIdType = 'Flu'").Count() > 0)
                                    {
                                        pdf_file_name = Path.Combine(ApplicationSettings.GetContractVoucherVARPdfFilesPath + "Voucher_EN_Flu_" + business_pk + "_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
                                        pdf_form = contract_pdf_maker.getVoucherFormBytes(ds_form_data, "FLUEN");
                                        if (pdf_form != null)
                                        {
                                            using (var file_stream = new FileStream(pdf_file_name, FileMode.Create))
                                            {
                                                file_stream.Write(pdf_form, 0, pdf_form.Length);
                                                file_stream.Close();
                                            }
                                            contract_pdf_files.Add(pdf_file_name);
                                        }

                                        pdf_file_name = Path.Combine(ApplicationSettings.GetContractVoucherVARPdfFilesPath + "Voucher_ESP_Flu_" + business_pk + "_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
                                        pdf_form = contract_pdf_maker.getVoucherFormBytes(ds_form_data, "FLUSP");
                                        if (pdf_form != null)
                                        {
                                            using (var file_stream = new FileStream(pdf_file_name, FileMode.Create))
                                            {
                                                file_stream.Write(pdf_form, 0, pdf_form.Length);
                                                file_stream.Close();
                                            }
                                            contract_pdf_files.Add(pdf_file_name);
                                        }
                                    }

                                    if (ds_form_data.Tables[1].Select("groupIdType = 'Routine'").Count() > 0)
                                    {
                                        pdf_file_name = Path.Combine(ApplicationSettings.GetContractVoucherVARPdfFilesPath + "Voucher_EN_Enhanced_" + business_pk + "_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
                                        pdf_form = contract_pdf_maker.getVoucherFormBytes(ds_form_data, "ROUTINEEN");
                                        if (pdf_form != null)
                                        {
                                            using (var file_stream = new FileStream(pdf_file_name, FileMode.Create))
                                            {
                                                file_stream.Write(pdf_form, 0, pdf_form.Length);
                                                file_stream.Close();
                                            }
                                            contract_pdf_files.Add(pdf_file_name);
                                        }

                                        pdf_file_name = Path.Combine(ApplicationSettings.GetContractVoucherVARPdfFilesPath + "Voucher_ESP_Enhanced_" + business_pk + "_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
                                        pdf_form = contract_pdf_maker.getVoucherFormBytes(ds_form_data, "ROUTINESP");
                                        if (pdf_form != null)
                                        {
                                            using (var file_stream = new FileStream(pdf_file_name, FileMode.Create))
                                            {
                                                file_stream.Write(pdf_form, 0, pdf_form.Length);
                                                file_stream.Close();
                                            }
                                            contract_pdf_files.Add(pdf_file_name);
                                        }
                                    }
                                }                                
                            }
                            this.logger.Info("Generating PDFs for vouchers & var forms done. Sending email to Walgreens user.."); 
                            email_operations.sendClinicAgreementToWalgreensUser(dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString(), dt_user_emails.Rows[0]["storeManagerEmail"].ToString(), business_name, Convert.ToInt32(hfContactLogPk.Value), (contract_pdf_files.Count() > 0 ? String.Format((string)GetGlobalResourceObject("errorMessages", "CommunityOffsiteAgreementAttachmentsInstr"), business_name) : ""), "Local", "Pharmacy Manager", -1, contract_pdf_files);
                        }
                        //Sends clinic store re-assignment emails
                        if (return_value == -3)
                        {
                            if (dt_approved_contract_details != null && dt_approved_contract_details.Rows.Count > 0)
                            {
                                //Sends clinic store re-assigned email to contracted store user
                                email_operations.sendClinicStoreAutoGeoCodeReassignmentEmail(dt_user_emails, dt_approved_contract_details);

                                //Sends clinic assigned email to the assigned store user
                                email_operations.sendLocalClinicStoreAssignmentEmail(dt_approved_contract_details, hfContactLogPk.Value);
                            }
                        }
                    }

                    if (rbtnApprove.Checked)
                    {
                        this.logger.Info("Redirecting from {0} page to Thankyou page for business user with contactlogpk: {1}", 
                        System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.contactLogPk.ToString());
                        bool has_corp_to_invoice_payment = false;
                        bool has_insurance_payment = false;

                        //checking for Corporate to invoice

                        this.xmlContractAgreement.LoadXml(ds_clinic_agreement.Tables[0].Rows[0]["clinicAgreementXml"].ToString());
                        DataSet ds_clinic_details = new DataSet();
                        //Bind immunizations and payment methods
                        StringReader immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Immunizations").OuterXml);
                        ds_clinic_details.ReadXml(immunizations_reader);
                        foreach (DataRow row in ds_clinic_details.Tables[0].Rows)
                        {
                            if (row["paymentTypeName"].ToString() == "Corporate to Invoice Employer Directly" && !has_corp_to_invoice_payment)
                            {
                                has_corp_to_invoice_payment = true;
                            }
                            else if ((row["paymentTypeName"].ToString() == "Submit Claims to Pharmacy Insurance" || row["paymentTypeName"].ToString() == "Submit Claims to Medical Insurance") && !has_insurance_payment)
                            {
                                has_insurance_payment = true;
                            }
                        }

                        if (var_pdf_files.Length > 0)
                        {
                            this.hfThankYouPageReqValues.Value = "Prefilled|";
                        }
                        else
                        {
                            this.hfThankYouPageReqValues.Value = has_corp_to_invoice_payment ? "None|" : "Blank|";
                        }

                        var specific_states = new string[] { "PR", "GA", "NC", "SC", "DR" };
                        if (specific_states.Contains(store_state.ToUpper().Trim()))
                            this.hfThankYouPageReqValues.Value += store_state + "|";
                        else
                            this.hfThankYouPageReqValues.Value += "OT|";

                        //Add VAR blank PDF file paths
                        if (hfThankYouPageReqValues.Value.Contains("Blank"))
                        {
                            this.hfThankYouPageReqValues.Value += ApplicationSettings.getPDFPath((specific_states.Contains(store_state.ToUpper().Trim()) ? store_state.ToUpper().Trim() : "OT"), "EN") + "|";
                            this.hfThankYouPageReqValues.Value += ApplicationSettings.getPDFPath((specific_states.Contains(store_state.ToUpper().Trim()) ? store_state.ToUpper().Trim() : "OT"), "SP") + "|";
                        }
                        else if (hfThankYouPageReqValues.Value.Contains("Prefilled"))
                        {
                            this.hfThankYouPageReqValues.Value += (var_pdf_files.Split('|').Count() > 2) ? var_pdf_files : var_pdf_files + "|";
                        }
                        else
                            this.hfThankYouPageReqValues.Value += "|||";

                        this.hfThankYouPageReqValues.Value += has_corp_to_invoice_payment ? "CorpToInvoice|" : "|";
                        this.hfThankYouPageReqValues.Value += has_insurance_payment ? "Insurance|" : "|";
                    }
                    this.businessUsersLog();
                    Session.Remove(this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value);
                    Server.Transfer("thankYou.aspx?key=fromUserPage");
                }
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by business user with contactlogpk: {2} - END", "btnSubmit_Click",
                   System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.contactLogPk.ToString());
    }

    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationPk");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");
            if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
            {
                Label lbl_Immunization = (Label)e.Row.FindControl("lblImmunizationCheck");
                Label lbl_payment_type = (Label)e.Row.FindControl("lblPaymentType");
                lbl_Immunization.Text = ((DataRowView)(e.Row.DataItem)).Row["immunizationSpanishName"].ToString();
                lbl_payment_type.Text = ((DataRowView)(e.Row.DataItem)).Row["paymentTypeSpanishName"].ToString();
            }

            if (Convert.ToInt32(lbl_payment_type_id.Text) == 6)
            {
                System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice_to = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowSendInvoiceTo");
                System.Web.UI.HtmlControls.HtmlTableRow row_voucher_needed = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowVoucherNeeded");
                System.Web.UI.HtmlControls.HtmlTableRow row_voucher_exp_date = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowExpirationDate");
                DropDownList ddl_tax_exempt = (DropDownList)e.Row.FindControl("ddlTaxExempt");
                DropDownList ddl_iscopay = (DropDownList)e.Row.FindControl("ddlIsCopay");
                DropDownList ddl_voucher = (DropDownList)e.Row.FindControl("ddlVoucher");
                var voucher_exp_date = (PickerAndCalendar)e.Row.FindControl("pcVaccineExpirationDate");

                if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
                {
                    List<ListItem> items_voucher = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };
                    List<ListItem> items_iscopayr = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };
                    List<ListItem> items_tax_exempt = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };

                    ddl_voucher.Items.Clear();
                    ddl_voucher.Items.AddRange(items_voucher.ToArray());
                    ddl_iscopay.Items.Clear();
                    ddl_iscopay.Items.AddRange(items_iscopayr.ToArray());
                    ddl_tax_exempt.Items.Clear();
                    ddl_tax_exempt.Items.AddRange(items_tax_exempt.ToArray());
                }

                row_send_invoice_to.Visible = true;
                row_voucher_needed.Visible = true;

                XmlElement immunization_ele = (XmlElement)this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + lbl_immunization_id.Text.Trim() + "' and @paymentTypeId='" + lbl_payment_type_id.Text.Trim() + "']");
                if (ddl_tax_exempt.Items.FindByValue("" + immunization_ele.Attributes["tax"].Value + "") != null)
                    ddl_tax_exempt.Items.FindByValue("" + immunization_ele.Attributes["tax"].Value + "").Selected = true;

                if (ddl_iscopay.Items.FindByValue("" + immunization_ele.Attributes["copay"].Value + "") != null)
                    ddl_iscopay.Items.FindByValue("" + immunization_ele.Attributes["copay"].Value + "").Selected = true;

                if (ddl_voucher.Items.FindByValue("" + immunization_ele.Attributes["isVoucherNeeded"].Value + "") != null)
                    ddl_voucher.Items.FindByValue("" + immunization_ele.Attributes["isVoucherNeeded"].Value + "").Selected = true;

                //Set default max and min voucher expiration dates
                if (!string.IsNullOrEmpty(immunization_ele.Attributes["isVoucherNeeded"].Value) && immunization_ele.Attributes["isVoucherNeeded"].Value == "Yes")
                {
                    ((PickerAndCalendar)voucher_exp_date).getSelectedDate = Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value);
                    ((TextBox)e.Row.FindControl("txtVaccineExpirationDate")).Text = Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value).ToString("MM/dd/yyyy");

                    if (DateTime.Now < Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value))
                        ((PickerAndCalendar)voucher_exp_date).MinDate = DateTime.Now.AddDays(-1);
                    else
                        ((PickerAndCalendar)voucher_exp_date).MinDate = Convert.ToDateTime(immunization_ele.Attributes["voucherExpirationDate"].Value).AddDays(-1);
                }
                else
                {
                    ((PickerAndCalendar)voucher_exp_date).getSelectedDate = (immunization_ele.Attributes["immunizationName"].Value.Contains("Influenza")) ? ApplicationSettings.getVoucherMaxExpDate : DateTime.Today.AddYears(1);
                    ((PickerAndCalendar)voucher_exp_date).MinDate = DateTime.Now.AddDays(-1);
                }

                ((PickerAndCalendar)voucher_exp_date).MaxDate = (immunization_ele.Attributes["immunizationName"].Value.Contains("Influenza")) ? ApplicationSettings.getVoucherMaxExpDate.AddDays(1) : DateTime.Today.AddYears(1).AddDays(1);

                if (ddl_voucher.SelectedValue == "Yes")
                    row_voucher_exp_date.Attributes.CssStyle.Add("display", "block");
                else
                    row_voucher_exp_date.Attributes.CssStyle.Add("display", "none");

                lbl_price.Text = "$ " + lbl_price.Text;
            }
            else
                lbl_price.Text = (string.IsNullOrEmpty(lbl_price.Text)) ? "N/A" : "$ " + lbl_price.Text;
        }
    }

    protected void grdLocationsRowdatabound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_clinic_datetime = (Label)e.Row.FindControl("lblClinicDateTime");
            GridView grd_clinic_immunizations = (GridView)e.Row.FindControl("grdClinicImmunizations");

            if (!string.IsNullOrEmpty(lbl_clinic_datetime.Text))
                lbl_clinic_datetime.Text = Convert.ToDateTime(lbl_clinic_datetime.Text).ToString("MM/dd/yyyy");

            var flu_exp_date = e.Row.FindControl("pcFluExpiryDate");
            //if (!string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString()) && grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString().Trim().Length != 0)
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"] != null && !string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString()))
            {
                ((PickerAndCalendar)flu_exp_date).MinDate = DateTime.Now.AddDays(-1);
                if (DateTime.Now < Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString()))
                    ((PickerAndCalendar)flu_exp_date).MinDate = DateTime.Now.AddDays(-1);
                else
                    ((PickerAndCalendar)flu_exp_date).MinDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString()).AddDays(-1);

                ((PickerAndCalendar)flu_exp_date).getSelectedDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString());
                ((TextBox)e.Row.FindControl("txtFluExpiryDate")).Text = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString()).ToString("MM/dd/yyyy");
                ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
            }
            else
            {
                ((PickerAndCalendar)flu_exp_date).getSelectedDate = ApplicationSettings.getVoucherMaxExpDate;
                ((PickerAndCalendar)flu_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
            }

            var routine_exp_date = e.Row.FindControl("pcRoutineExpiryDate");
            //if (!string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString()) && grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString().Trim().Length != 0)
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"] != null && !string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString()))
            {
                ((PickerAndCalendar)routine_exp_date).MinDate = DateTime.Now.AddDays(-1);
                if (DateTime.Now < Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString()))
                    ((PickerAndCalendar)routine_exp_date).MinDate = DateTime.Now.AddDays(-1);
                else
                    ((PickerAndCalendar)routine_exp_date).MinDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString()).AddDays(-1);

                ((PickerAndCalendar)routine_exp_date).getSelectedDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString());
                ((TextBox)e.Row.FindControl("txtRoutineExpiryDate")).Text = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString()).ToString("MM/dd/yyyy");
                ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
            }
            else
            {
                ((PickerAndCalendar)routine_exp_date).getSelectedDate = DateTime.Today.AddYears(1);
                ((PickerAndCalendar)routine_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
            }

            this.bindClinicImmunizations(grd_clinic_immunizations, e.Row.RowIndex);
        }
    }

    protected void grdClinicImmunizations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string immunization_key = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "immunizationSpanishName" : "immunizationName";
            string payment_type_key = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "paymentTypeSpanishName" : "paymentTypeName";
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationId");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");
            Label lbl_clinic_immunization = (Label)e.Row.FindControl("lblClinicImmunization");
            Label lbl_clinic_payment_type = (Label)e.Row.FindControl("lblClinicPaymentType");

            XmlElement immunization_ele = (XmlElement)this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + lbl_immunization_id.Text.Trim() + "' and @paymentTypeId='" + lbl_payment_type_id.Text.Trim() + "']");
            lbl_clinic_immunization.Text = immunization_ele.Attributes[immunization_key].Value;
            lbl_clinic_payment_type.Text = immunization_ele.Attributes[payment_type_key].Value;
        }
    }

    protected void ValidateData(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = true;
        string value = string.Empty;
        // txtClient
        value = txtClient.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClient"), "textbox", "Enter a Business Name for the Client field.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClient"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtClient.Style.Add("border", "1px solid gray");
            txtClient.ToolTip = "";
        }
        //txtClientName
        value = txtClientName.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientName"), "textbox", "Enter a first and last name in the Client's Name field.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientName"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtClientName.Style.Add("border", "1px solid gray");
            txtClientName.ToolTip = "";
        }
        //txtClientTitle
        value = txtClientTitle.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientTitle"), "textbox", "Enter the Title for the Client's representative named in the Name field.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtClientTitle"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtClientTitle.Style.Add("border", "1px solid gray");
            txtClientTitle.ToolTip = "";
        }
        //txtAttentionTo
        value = txtAttentionTo.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtAttentionTo"), "textbox", "Enter value in the Attention To field of the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtAttentionTo"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtAttentionTo.Style.Add("border", "1px solid gray");
            txtAttentionTo.ToolTip = "";
        }
        //txtLegalAddress1
        value = txtLegalAddress1.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalAddress1"), "textbox", "Enter a street address in the Address1 field of the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalAddress1"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtLegalAddress1.Style.Add("border", "1px solid gray");
            txtLegalAddress1.ToolTip = "";
        }
        //txtLegalAddress2
        value = txtLegalAddress2.Text;
        if (!string.IsNullOrEmpty(value))
        {
            if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtLegalAddress2"), "textbox", "Invalid Data");
                e.IsValid = false;
            }
            else
            {
                txtLegalAddress2.Style.Add("border", "1px solid gray");
                txtLegalAddress2.ToolTip = "";
            }
        }
        //txtLegalCity
        value = txtLegalCity.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalCity"), "textbox", "Enter a City in the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalCity"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtLegalCity.Style.Add("border", "1px solid gray");
            txtLegalCity.ToolTip = "";
        }
        //ddlLegalState
        value = ddlLegalState.SelectedValue;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("ddlLegalState"), "ddl", "Select a State in the Send Legal Notices To Client at section.");
            e.IsValid = false;
        }
        else
        {
            ddlLegalState.Style.Add("border", "1px solid gray");
            ddlLegalState.ToolTip = "";
        }
        //txtLegalZip
        value = txtLegalZip.Text;
        if (string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalZip"), "textbox", "Enter a 5 digit Zip Code in the Send Legal Notices To Client at section. (ex:#####).");
            e.IsValid = false;
        }
        else if (!value.validateZipCode() && !string.IsNullOrEmpty(value))
        {
            this.setControlProperty(Page.FindControl("txtLegalZip"), "textbox", "Invalid Data");
            e.IsValid = false;
        }
        else
        {
            txtLegalZip.Style.Add("border", "1px solid gray");
            txtLegalZip.ToolTip = "";
        }
        //txtElectronicSign
        if (rbtnApprove.Checked)
        {
            value = txtElectronicSign.Text;
            if (string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtElectronicSign"), "textbox", "Your Electronic Signature is required for Approval");
                e.IsValid = false;
            }
            else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtElectronicSign"), "textbox", "Invalid Data");
                e.IsValid = false;
            }
            else
            {
                txtElectronicSign.Style.Add("border", "1px solid gray");
                txtElectronicSign.ToolTip = "";
            }
        }
        else
        {
            txtElectronicSign.Style.Add("border", "1px solid gray");
            txtElectronicSign.ToolTip = "";
        }
        //txtNotes
        if (rbtnReject.Checked)
        {
            value = txtNotes.Text;
            if (string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtNotes"), "textbox", "Please enter Notes.");
                e.IsValid = false;
            }
            else if (!value.validateJunkCharacters() && !string.IsNullOrEmpty(value))
            {
                this.setControlProperty(Page.FindControl("txtNotes"), "textbox", "Invalid Data");
                e.IsValid = false;
            }
            else
            {
                txtNotes.Style.Add("border", "1px solid gray");
                txtNotes.ToolTip = "";
            }
        }
        else
        {
            txtNotes.Style.Add("border", "1px solid gray");
            txtNotes.ToolTip = "";
        }
    }

    protected void lnkChangeCulture_Click(object sender, EventArgs e)
    {
        if (Request.QueryString.Count == 0 || Request.QueryString == null || Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value] == null)
        {
            Response.Redirect("walgreensNoAccess.htm");
        }
        else
        {
            DataSet ds_contract_agreement = (DataSet)Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value];
            ds_contract_agreement.Tables[0].Rows[0]["clinicAgreementXml"] = this.prepareBusinessUserXml();
            Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value] = ds_contract_agreement;
            bindContractAgreement();
        }
    }
    #endregion

    #region -------------------- PRIVATE METHODS --------------
    /// <summary>
    /// To set control border red
    /// </summary>
    /// <param name="control"></param>
    /// <param name="cont_type"></param>
    /// <param name="tool_tip"></param>
    private void setControlProperty(Control control, string cont_type, string tool_tip)
    {
        switch (cont_type)
        {
            case "textbox":
                ((TextBox)control).Style.Add("border", "1px solid red");
                ((TextBox)control).ToolTip = tool_tip;
                break;
            case "ddl":
                ((DropDownList)control).Style.Add("border", "1px solid red");
                ((DropDownList)control).ToolTip = tool_tip;
                break;
        }
    }

    /// <summary>
    /// Gets and binds contract agreement for business user
    /// </summary>
    private void bindContractAgreement()
    {
        if (Request.QueryString["args"] != null && Request.QueryString.Count > 0)
        {
            EncryptedQueryString args = new EncryptedQueryString(Request.QueryString["args"]);
            if (args.Count() == 2)
            {
                this.hfBusinessUserEmail.Value = args["arg1"].ToString();
                this.hfContactLogPk.Value = args["arg2"].ToString();
                Int32.TryParse(this.hfContactLogPk.Value, out this.contactLogPk);
                DataSet ds_contract_agreement = new DataSet();
                System.Web.HttpBrowserCapabilities browser = Request.Browser;
                Session["identifierString"] = " ContactLogPk: " + this.hfContactLogPk.Value + "; Business User Email: " + this.hfBusinessUserEmail.Value + "; Browser: " + browser.Browser + " " + browser.Version;
                this.logger.Info("Method {0} accessed from {1} page by business user with {2} - START", "bindContractAgreement",
                           System.IO.Path.GetFileNameWithoutExtension(Request.Path), Session["identifierString"].ToString());
        
                if (Session[args["arg1"].ToString() + args["arg2"].ToString()] != null)
                    ds_contract_agreement = (DataSet)Session[args["arg1"].ToString() + args["arg2"].ToString()];
                else
                    ds_contract_agreement = this.dbOperation.getClinicAgreement("Business", args["arg2"].ToString(), args["arg1"].ToString(), 2015, false);

                if ((ds_contract_agreement == null) || (ds_contract_agreement != null && ds_contract_agreement.Tables.Count == 0))
                    Response.Redirect("walgreensNoAccess.htm");
                else
                    this.dtContractAgreement = ds_contract_agreement.Tables[0];
                if (ds_contract_agreement.Tables.Count > 2 && ds_contract_agreement.Tables[2].Rows.Count > 0)
                    this.hfIsModified.Value = "true";

                if (this.dtContractAgreement != null && this.dtContractAgreement.Rows.Count > 0)
                {
                    //Redirect contract agreements created before "05/01/2015" to previous season contract agreement page
                    if (Convert.ToDateTime(this.dtContractAgreement.Rows[0]["dateCreated"]) < Convert.ToDateTime("05/01/2015"))
                    {
                        EncryptedQueryString encryped_link = new EncryptedQueryString();
                        encryped_link["arg1"] = this.hfBusinessUserEmail.Value;
                        encryped_link["arg2"] = this.hfContactLogPk.Value;
                        encryped_link["arg3"] = "BusinessUser";

                        Response.Redirect("walgreensClinicAgreementPrevSeason.aspx?args=" + encryped_link.ToString());
                    }

                    this.xmlContractAgreement.LoadXml(this.dtContractAgreement.Rows[0]["clinicAgreementXml"].ToString());

                    //Set isNoClinic, fluExpiryDate and routineExpiryDate attributes to Clinic Agreement XML
                    XmlNodeList clinic_locations = this.xmlContractAgreement.SelectNodes("//Clinics/clinic");
                    foreach (XmlElement clinic_location_ele in clinic_locations)
                    {
                        if (clinic_location_ele.Attributes["isNoClinic"] == null)
                            clinic_location_ele.SetAttribute("isNoClinic", "0");
                        if (clinic_location_ele.Attributes["fluExpiryDate"] == null)
                            clinic_location_ele.SetAttribute("fluExpiryDate", "");
                        if (clinic_location_ele.Attributes["routineExpiryDate"] == null)
                            clinic_location_ele.SetAttribute("routineExpiryDate", "");
                    }

                    //Update immunization price in the contract agreements created before price update and not yet approved
                    if (!Convert.ToBoolean(this.hfIsModified.Value) && this.dtContractAgreement.Rows[0]["isApproved"].ToString().Trim().Length == 0 || (this.dtContractAgreement.Rows[0]["isApproved"].ToString().Trim().Length != 0 && !((bool)this.dtContractAgreement.Rows[0]["isApproved"])))
                    {
                        DataTable dt_immunizations = this.dbOperation.getImmunizationCheck(2015, "Local");

                        if (this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization") != null)
                        {
                            XmlNodeList immunization_checks = this.xmlContractAgreement.SelectNodes("//Immunizations/Immunization");
                            foreach (XmlNode immunization_check in immunization_checks)
                            {
                                DataRow[] dr_immunization_check = dt_immunizations.Select("immunizationId = '" + immunization_check.Attributes["pk"].Value + "' AND paymentTypeId = '" + immunization_check.Attributes["paymentTypeId"].Value + "'");
                                immunization_check.Attributes["price"].Value = (immunization_check.Attributes["paymentTypeId"].Value == "6") ? dr_immunization_check[0]["directBillPrice"].ToString() : dr_immunization_check[0]["price"].ToString();
                            }

                            this.dtContractAgreement.Rows[0]["clinicAgreementXml"] = this.xmlContractAgreement.OuterXml;
                        }
                    }

                    this.hfOutreachBusinessPk.Value = this.dtContractAgreement.Rows[0]["outreachBusinessPk"].ToString();
                    this.txtElectronicSign.Text = this.dtContractAgreement.Rows[0]["signature"].ToString();
                    this.hfBusinessStoreId.Value = this.dtContractAgreement.Rows[0]["storeId"].ToString();
                    this.contractCreatedDate = Convert.ToDateTime(this.dtContractAgreement.Rows[0]["dateCreated"].ToString());
                    this.hfLastUpdatedDate.Value = this.dtContractAgreement.Rows[0]["updatedDate"].ToString();
                    this.hfStoreState.Value = this.dtContractAgreement.Rows[0]["storeState"].ToString();
                    if (this.hfStoreState.Value == "PR" && this.hfLanguage.Value == "es-MX")
                        this.hfLanguage.Value = "es-PR";
                    else if (this.dtContractAgreement.Rows[0]["storeRegion"].ToString() == "35")
                        this.hfStoreState.Value = "DR";

                    //Bind Client details
                    this.txtClient.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@header").Value.ToString();
                    this.txtClientName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@name").Value.ToString();
                    this.txtClientTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@title").Value.ToString();
                    if (!string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value))
                        this.lblClientDate.Text = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value).ToString("MM/dd/yyyy");

                    //Binds Walgreen CO. details
                    this.lblWalgreenName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@name").Value.ToString();
                    this.lblWalgreenTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@title").Value.ToString();
                    if (!string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value))
                        this.lblWalgreenDate.Text = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value).ToString("MM/dd/yyyy");
                    this.lblDistrictNum.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@districtNumber").Value.ToString();

                    //Binds Legal Notice details
                    this.txtAttentionTo.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@attentionTo").Value.ToString();
                    this.txtLegalAddress1.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address1").Value.ToString();
                    this.txtLegalAddress2.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address2").Value.ToString();
                    this.txtLegalCity.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@city").Value.ToString();
                    this.ddlLegalState.ClearSelection();
                    if (this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString().Trim().Length != 0)
                        this.ddlLegalState.Items.FindByValue(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString()).Selected = true;
                    this.txtLegalZip.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@zipCode").Value.ToString();

                    DataSet ds_clinic_details = new DataSet();
                    //Bind immunizations and payment methods
                    StringReader immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Immunizations").OuterXml);
                    ds_clinic_details.ReadXml(immunizations_reader);
                    if (ds_clinic_details != null && ds_clinic_details.Tables.Count > 0)
                    {
                        this.isFluImmunizationExists = (ds_clinic_details.Tables[0].Select("immunizationName Like 'Influenza%'").Count() > 0);
                        this.isRoutineImmunizationExists = (ds_clinic_details.Tables[0].Select("immunizationName NOT Like 'Influenza%'").Count() > 0);

                        this.grdImmunizationChecks.DataSource = ds_clinic_details.Tables[0];
                        this.grdImmunizationChecks.DataBind();
                    }
                    else
                    {
                        this.isFluImmunizationExists = false;
                        this.isRoutineImmunizationExists = false;
                        Session.Remove(args["arg1"].ToString() + args["arg2"].ToString());
                        Response.Redirect("walgreensNoAccess.htm");
                    }

                    //Bind clinic locations
                    StringReader clinics_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").OuterXml);
                    ds_clinic_details = new DataSet();
                    ds_clinic_details.ReadXml(clinics_reader);
                    if (ds_clinic_details.Tables.Count > 0)
                    {
                        if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
                        {
                            foreach (DataRow row in ds_clinic_details.Tables[0].Rows)
                            {
                                row["ClinicLocation"] = row["ClinicLocation"].ToString().Replace("CLINIC LOCATION", "UBICACIÓN DE LA CLÍNICA");
                            }
                        }
                    }

                    this.grdLocations.DataSource = ds_clinic_details.Tables[0];
                    this.grdLocations.DataBind();
                    Session[args["arg1"].ToString() + args["arg2"].ToString()] = ds_contract_agreement;

                    if (this.dtContractAgreement.Rows[0]["approvedOrRejectedBy"] != DBNull.Value)
                    {
                        this.lblErrorMessage.Text = "The Walgreens Community Off-Site Agreement has already been submitted by " + this.dtContractAgreement.Rows[0]["approvedOrRejectedBy"].ToString();
                    }
                    else
                    {
                        if (ds_contract_agreement.Tables[1] != null)
                        {
                            DataTable dt_approved_contract_details = ds_contract_agreement.Tables[1];
                            if (dt_approved_contract_details != null && dt_approved_contract_details.Rows.Count > 0)
                            {
                                this.lblErrorMessage.Text = String.Format((string)GetGlobalResourceObject("errorMessages", "approvedContractAlreadyExists"), dt_approved_contract_details.Rows[0]["electronicSign"].ToString(), dt_approved_contract_details.Rows[0]["email"].ToString());
                                this.disableBusinessUserControls();
                            }
                        }
                    }

                    if (this.dtContractAgreement.Rows[0]["isApproved"].ToString().Trim().Length != 0)
                    {
                        if (!((bool)this.dtContractAgreement.Rows[0]["isApproved"]))
                        {
                            this.rbtnReject.Checked = true;
                            this.trNotes.Attributes.Add("style", "display:block;");
                            this.txtNotes.Text = this.dtContractAgreement.Rows[0]["notes"].ToString();
                        }
                        else
                        {
                            this.rbtnApprove.Checked = true;
                        }
                        this.disableBusinessUserControls();
                    }
                    else if (Convert.ToBoolean(this.dtContractAgreement.Rows[0]["isPreviousSeason"].ToString()))
                    {
                        this.disableBusinessUserControls();
                        this.rbtnApprove.Checked = false;
                    }
                    this.tblApproval.Visible = true;

                    this.disableControls(true);
                }
                else
                    Response.Redirect("walgreensNoAccess.htm");

                this.logger.Info("Method {0} accessed from {1} page by business user with {2} - END", "bindContractAgreement",
                           System.IO.Path.GetFileNameWithoutExtension(Request.Path), Session["identifierString"].ToString());
        
            }
            else
                Response.Redirect("walgreensNoAccess.htm");
        }
        else
            Response.Redirect("walgreensNoAccess.htm");
    }

    /// <summary>
    /// Binds immunizations to clinic location grid
    /// </summary>
    /// <param name="grd_clinic_immunizations"></param>
    /// <param name="clinic_number"></param>
    private void bindClinicImmunizations(GridView grd_clinic_immunizations, int clinic_number)
    {
        StringReader clinic_immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").ChildNodes[clinic_number].OuterXml);
        DataSet ds_clinic_immunizations = new DataSet();
        ds_clinic_immunizations.ReadXml(clinic_immunizations_reader);

        if (ds_clinic_immunizations.Tables.Count > 1 && ds_clinic_immunizations.Tables[1].Rows.Count > 0)
        {
            grd_clinic_immunizations.DataSource = ds_clinic_immunizations.Tables[1];
            grd_clinic_immunizations.DataBind();
        }
    }

    /// <summary>
    /// Creates Contract Agreement XML document with business user information
    /// </summary>
    /// <returns></returns>
    private string prepareBusinessUserXml()
    {
        DataSet ds_clinic_agreement = new DataSet();
        ds_clinic_agreement = (DataSet)Session[this.hfBusinessUserEmail.Value + this.hfContactLogPk.Value];
        this.xmlContractAgreement.InnerXml = ds_clinic_agreement.Tables[0].Rows[0]["clinicAgreementXml"].ToString();

        //Update voucher expiration date if changed by user
        string voucher_needed = string.Empty;
        string is_copay = string.Empty;
        string txtCoPay = string.Empty;
        string voucher_flu_exp_date = string.Empty;
        string voucher_routine_exp_date = string.Empty;

        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            voucher_needed = ((DropDownList)row.FindControl("ddlVoucher")).SelectedItem.Value;
            if (voucher_needed == "Yes")
            {
                if (((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + ((Label)row.FindControl("lblImmunizationPk")).Text.Trim() + "' and @paymentTypeId='" + ((Label)row.FindControl("lblPaymentTypeId")).Text.Trim() + "']").Attributes["voucherExpirationDate"].Value = ((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy");
                    if (((Label)row.FindControl("lblImmunizationCheck")).Text.Trim().Contains("Influenza"))
                        voucher_flu_exp_date = ((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy");
                    else
                        voucher_routine_exp_date = ((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy");
                }
            }
            is_copay = ((DropDownList)row.FindControl("ddlIsCopay")).SelectedItem.Value;
            txtCoPay = ((TextBox)row.FindControl("txtCoPay")).Text;
            if (is_copay == "" || is_copay == "Select" || is_copay == "0")
                this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + ((Label)row.FindControl("lblImmunizationPk")).Text.Trim() + "' and @paymentTypeId='" + ((Label)row.FindControl("lblPaymentTypeId")).Text.Trim() + "']").Attributes["copay"].Value = "No";
            if (is_copay == "Yes" && string.IsNullOrEmpty(txtCoPay))
                this.xmlContractAgreement.SelectSingleNode("//Immunizations/Immunization[@pk='" + ((Label)row.FindControl("lblImmunizationPk")).Text.Trim() + "' and @paymentTypeId='" + ((Label)row.FindControl("lblPaymentTypeId")).Text.Trim() + "']").Attributes["copay"].Value = "No";
        }

        //Set clinic location lats and longs
        foreach (GridViewRow row in grdLocations.Rows)
        {
            string clinic_location = ((Label)row.FindControl("lblClinicLocation")).Text.Trim().Contains("UBICACIÓN DE LA CLÍNICA") ? ((Label)row.FindControl("lblClinicLocation")).Text.Trim().Replace("UBICACIÓN DE LA CLÍNICA", "CLINIC LOCATION") : ((Label)row.FindControl("lblClinicLocation")).Text.Trim();
            XmlNode business_node = this.xmlContractAgreement.SelectSingleNode("//Clinics/clinic[@clinicLocation='" + clinic_location + "']");
            ((XmlElement)business_node).SetAttribute("naClinicLatitude", ((HiddenField)row.FindControl("hfClinicLatitude")).Value.Trim());
            ((XmlElement)business_node).SetAttribute("naClinicLongitude", ((HiddenField)row.FindControl("hfClinicLongitude")).Value.Trim());

            if (business_node.Attributes["isNoClinic"] != null)
            {
                if (business_node.Attributes["isNoClinic"].Value == "1")
                {
                    if (((PickerAndCalendar)row.FindControl("pcFluExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                        business_node.Attributes["fluExpiryDate"].Value = ((PickerAndCalendar)row.FindControl("pcFluExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy");
                    if (((PickerAndCalendar)row.FindControl("pcRoutineExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                        business_node.Attributes["routineExpiryDate"].Value = ((PickerAndCalendar)row.FindControl("pcRoutineExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy");
                }
                else if (this.rbtnApprove.Checked)
                {
                    if (voucher_flu_exp_date.Length > 0 && voucher_flu_exp_date != "01/01/0001")
                        business_node.Attributes["fluExpiryDate"].Value = voucher_flu_exp_date;
                    if (voucher_routine_exp_date.Length > 0 && voucher_routine_exp_date != "01/01/0001")
                        business_node.Attributes["routineExpiryDate"].Value = voucher_routine_exp_date;
                }
            }
        }

        XmlNode client_node = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client");
        ((XmlElement)client_node).SetAttribute("header", this.txtClient.Text.Trim());
        ((XmlElement)client_node).SetAttribute("name", this.txtClientName.Text.Trim());
        ((XmlElement)client_node).SetAttribute("title", this.txtClientTitle.Text.Trim());
        ((XmlElement)client_node).SetAttribute("date", DateTime.Now.ToString("MM/dd/yyyy"));

        XmlNode legal_notic_address = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress");
        ((XmlElement)legal_notic_address).SetAttribute("attentionTo", this.txtAttentionTo.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("address1", this.txtLegalAddress1.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("address2", this.txtLegalAddress2.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("city", this.txtLegalCity.Text.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("state", this.ddlLegalState.SelectedValue.Trim());
        ((XmlElement)legal_notic_address).SetAttribute("zipCode", this.txtLegalZip.Text.Trim());

        return this.xmlContractAgreement.OuterXml;
    }

    /// <summary>
    /// Disables Business user controls for approved contracts
    /// </summary>
    private void disableBusinessUserControls()
    {
        this.tblApproval.Disabled = true;
        this.trNotes.Disabled = true;
        this.txtElectronicSign.Enabled = false;
        this.txtNotes.Enabled = false;
        this.btnSubmit.Visible = false;
        this.txtClient.Enabled = false;
        this.txtClientName.Enabled = false;
        this.txtClientTitle.Enabled = false;
        this.txtAttentionTo.Enabled = false;
        this.txtLegalAddress1.Enabled = false;
        this.txtLegalAddress2.Enabled = false;
        this.txtLegalCity.Enabled = false;
        this.ddlLegalState.Enabled = false;
        this.txtLegalZip.Enabled = false;
    }

    /// <summary>
    /// Disables controls
    /// </summary>
    /// <param name="disable"></param>
    private void disableControls(bool disable)
    {
        this.disableGridControls(disable, this.grdLocations);
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            GridView grd_view = (GridView)row.FindControl("grdClinicImmunizations");
            this.disableGridControls(disable, grd_view);
        }

        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice = (System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowSendInvoiceTo");
            this.disableCommunityAgreement(disable, row_send_invoice.Controls);
            System.Web.UI.HtmlControls.HtmlTableRow row_voucher_needed = (System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowVoucherNeeded");
            this.disableCommunityAgreement(disable, row_voucher_needed.Controls);
        }
    }

    /// <summary>
    /// Disables grid controls
    /// </summary>
    /// <param name="disable"></param>
    /// <param name="grd_ctrl"></param>
    private void disableGridControls(bool disable, GridView grd_ctrl)
    {
        foreach (GridViewRow row in grd_ctrl.Rows)
        {
            foreach (Control ctrl in row.Controls)
            {
                foreach (Control ctrl1 in ctrl.Controls)
                {
                    if (ctrl1 is TextBox)
                        ((TextBox)ctrl1).Enabled = !disable;

                    if (ctrl1 is DropDownList)
                        ((DropDownList)ctrl1).Enabled = !disable;

                    if (ctrl1 is CheckBox)
                        ((CheckBox)ctrl1).Enabled = !disable;
                }
            }
        }
    }

    /// <summary>
    /// Disables all input controls in the page
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableCommunityAgreement(bool disable, ControlCollection ctrl_main)
    {
        foreach (Control page_ctrl in ctrl_main)
        {
            foreach (Control ctrl1 in page_ctrl.Controls)
            {
                if (ctrl1 is TextBox)
                    ((TextBox)ctrl1).Enabled = !disable;

                if (ctrl1 is RadioButton)
                    ((RadioButton)ctrl1).Enabled = !disable;

                if (ctrl1 is DropDownList)
                    ((DropDownList)ctrl1).Enabled = !disable;

                if (ctrl1 is PickerAndCalendar)
                    ((PickerAndCalendar)ctrl1).Visible = false;

                foreach (Control ctrl in ctrl1.Controls)
                {
                    if (page_ctrl is System.Web.UI.HtmlControls.HtmlTableCell)
                    {
                        if (ctrl1 is TextBox)
                            ((TextBox)ctrl1).Enabled = !disable;

                        if (ctrl1 is DropDownList)
                            ((DropDownList)ctrl1).Enabled = !disable;

                        if (ctrl1 is CheckBox)
                            ((CheckBox)ctrl1).Enabled = !disable;

                        if (ctrl1 is ImageButton)
                            ((ImageButton)ctrl1).Visible = !disable;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Setting image urls based on culture
    /// </summary>
    private void setImageButtons()
    {
        if (!String.IsNullOrEmpty(this.hfLanguage.Value))
        {
            btnSubmit.ImageUrl = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "images/btn_submit_SP.png" : "images/btn_submit_event.png";

            if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
            {
                btnSubmit.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_submit_SP.png');");
                btnSubmit.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_submit_SP_lit.png');");
                btnSubmit.AlternateText = "Enviar Correo Electrónico";
            }
            else
            {
                btnSubmit.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_submit_event.png');");
                btnSubmit.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_submit_event_lit.png');");
                btnSubmit.AlternateText = "Send email";
            }
        }
    }

    /// <summary>
    /// Validates Voucher Expiry dates
    /// </summary>
    /// <returns></returns>
    private bool isValidExpiryDates()
    {
        bool is_valid = true;
        string voucher_needed = string.Empty;
        List<string> lst_flu_expiry_dates = new List<string>();
        List<string> lst_routine_expiry_dates = new List<string>();
        string validation_message = string.Empty;
        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            Label lbl_Immunization = (Label)row.FindControl("lblImmunizationCheck");
            voucher_needed = ((DropDownList)row.FindControl("ddlVoucher")).SelectedItem.Value;
            if (voucher_needed == "Yes")
            {
                string vaccine_expiry_date = ((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString();
                if (Convert.ToDateTime(vaccine_expiry_date) != Convert.ToDateTime("1/1/0001"))
                {
                    if (lbl_Immunization.Text.ToLower().Contains("influ") && !lst_flu_expiry_dates.Contains(vaccine_expiry_date))
                        lst_flu_expiry_dates.Add(vaccine_expiry_date);
                    else if (!lbl_Immunization.Text.ToLower().Contains("influ") && !lst_routine_expiry_dates.Contains(vaccine_expiry_date))
                        lst_routine_expiry_dates.Add(vaccine_expiry_date);
                }
            }
        }
        foreach (GridViewRow row in grdLocations.Rows)
        {
            CheckBox chk_no_clinic = (CheckBox)row.FindControl("chkNoClinic");
            var flu_date_picker = row.FindControl("pcFluExpiryDate");
            string flu_expiry_date = (flu_date_picker != null && chk_no_clinic.Checked) ? ((PickerAndCalendar)flu_date_picker).getSelectedDate.ToString() : string.Empty;
            var routine_date_picker = row.FindControl("pcRoutineExpiryDate");
            string routine_expiry_date = (routine_date_picker != null && chk_no_clinic.Checked) ? ((PickerAndCalendar)routine_date_picker).getSelectedDate.ToString() : string.Empty;

            if (!string.IsNullOrEmpty(flu_expiry_date) && this.isFluImmunizationExists && Convert.ToDateTime(flu_expiry_date) != Convert.ToDateTime("1/1/0001") && !lst_flu_expiry_dates.Contains(flu_expiry_date))
                lst_flu_expiry_dates.Add(flu_expiry_date);

            if (!string.IsNullOrEmpty(routine_expiry_date) && this.isRoutineImmunizationExists && Convert.ToDateTime(routine_expiry_date) != Convert.ToDateTime("1/1/0001") && !lst_routine_expiry_dates.Contains(routine_expiry_date))
                lst_routine_expiry_dates.Add(routine_expiry_date);
        }
        if (lst_flu_expiry_dates.Count > 1)
        {
            validation_message += ((string)GetGlobalResourceObject("errorMessages", "fluExpiryDateMismatchAlert")) + "\\n";
            is_valid = false;
        }
        if (lst_routine_expiry_dates.Count > 1)
        {
            validation_message += ((string)GetGlobalResourceObject("errorMessages", "routineExpiryDateMismatchAlert"));
            is_valid = false;
        }
        if (!string.IsNullOrEmpty(validation_message))
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + validation_message + "')", true);

        return is_valid;
    }

    /// <summary>
    /// Set minimum and maximum dates for date controls.
    /// </summary>
    private void setMinMaxDates()
    {
        foreach (GridViewRow imm_row in grdImmunizationChecks.Rows)
        {
            var ctl_date_picker = imm_row.FindControl("pcVaccineExpirationDate");
            Label lbl_Immunization = (Label)imm_row.FindControl("lblImmunizationCheck");
            if (ctl_date_picker != null)
            {
                if (lbl_Immunization.Text.ToLower().Contains("influ"))
                {
                    ((PickerAndCalendar)ctl_date_picker).MinDate = DateTime.Now.AddDays(-1);
                    ((PickerAndCalendar)ctl_date_picker).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
                }
                else
                {
                    ((PickerAndCalendar)ctl_date_picker).MinDate = DateTime.Now.AddDays(-1);
                    ((PickerAndCalendar)ctl_date_picker).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
                }
            }
        }
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            var flu_exp_date = row.FindControl("pcFluExpiryDate");
            var routine_exp_date = row.FindControl("pcRoutineExpiryDate");
            if (flu_exp_date != null)
            {
                ((PickerAndCalendar)flu_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
            }
            if (routine_exp_date != null)
            {
                ((PickerAndCalendar)routine_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
            }
        }
    }

    /// <summary>
    /// Set Count of business users
    /// </summary>
    /// <param name="user_wag"></param>
    private void businessUsersLog(bool is_add_user = false)
    {
        Dictionary<int, string> wag_user = (Dictionary<int, string>)Application["WagUsers"];
        if (wag_user.Count > 0)
        {
            if (wag_user.ContainsKey(this.contactLogPk))
                user_exists = true;

            if (is_add_user)
            {
                if (!user_exists)
                    wag_user.Add(this.contactLogPk, "Business Users");
            }
            else
            {
                if (user_exists)
                    wag_user.Remove(this.contactLogPk);
            }
            Application["WagUsers"] = wag_user;
        }
        else
        {
            wag_user.Add(this.contactLogPk, "Business Users");
        }
                    
    }

    #endregion

    protected bool isFluImmunizationExists
    {
        get
        {
            bool result = false;

            if (ViewState["isFluImmunizationExists"] != null)
            {
                result = (bool)ViewState["isFluImmunizationExists"];
            }
            return result;
        }
        set
        {
            ViewState["isFluImmunizationExists"] = value;
        }
    }

    protected bool isRoutineImmunizationExists
    {
        get
        {
            bool result = false;

            if (ViewState["isRoutineImmunizationExists"] != null)
            {
                result = (bool)ViewState["isRoutineImmunizationExists"];
            }
            return result;
        }
        set
        {
            ViewState["isRoutineImmunizationExists"] = value;
        }
    }

    #region ---------------- PRIVATE VARIABLES ----------------
    private DBOperations dbOperation = null;
    private DataTable dtContractAgreement = null;
    private XmlDocument xmlContractAgreement;
    private WalgreenEmail walgreensEmail = null;
    private DateTime contractCreatedDate;
    private int contactLogPk;
    private bool user_exists = false;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        //ApplicationSettings.validateSession();
        this.dbOperation = new DBOperations();
        this.xmlContractAgreement = new XmlDocument();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        contractCreatedDate = DateTime.Now;
    }

    protected override void InitializeCulture()
    {
        if (Request.Form["hfLanguage"] != null)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["hfLanguage"]);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["hfLanguage"]);
            Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo("en-US").DateTimeFormat;
        }
        base.InitializeCulture();
    }
}