﻿<%@ WebHandler Language="C#" Class="StoreProfileHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

public class StoreProfileHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        NameValueCollection forms = context.Request.Form;
        List<StoreProfile> objStoreCollection = null;
        string strOperation = forms.Get("oper");
        string file_path = (context.Session != null && context.Session["JSONFilePath"] !=null) ? context.Session["JSONFilePath"].ToString() : "";
        string storeCollection = string.Empty; 
        if (!string.IsNullOrEmpty(file_path) && System.IO.File.Exists(file_path))
            storeCollection = System.IO.File.ReadAllText(file_path) ;
        string strResponse = string.Empty;
        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        
        if(storeCollection.Length<json_serializer.MaxJsonLength)
            objStoreCollection = json_serializer.Deserialize<List<StoreProfile>>(storeCollection);
        Dictionary<string, object> removed_store = new Dictionary<string, object>();

        if (strOperation == null)
        {
            if (objStoreCollection != null)
            {
                var updated_collection = objStoreCollection.AsEnumerable().Where(a => a.isCurrent == true).OrderBy(a=>a.storeId).ToList();
                context.Response.Write(json_serializer.Serialize(updated_collection));
            }
        }
        else if (strOperation == "del")
        {
            var objStore = objStoreCollection.AsEnumerable().FirstOrDefault(a => a.id == Convert.ToInt32(forms.Get("id")));
            if(objStore != null)
            {
                objStore.isCurrent = false;
            }
            strResponse = "Employee record successfully removed";
            context.Response.Write(strResponse);
            var serializedCollection = json_serializer.Serialize(objStoreCollection);
            System.IO.File.WriteAllText(file_path, serializedCollection);
        }
        //else if (strOperation == "edit")
        //{
        //    string strId = string.Empty;
        //    StoreProfile updated_store_profile = new StoreProfile();
        //    var objStore = objStoreCollection.AsEnumerable().FirstOrDefault(a => a.id == Convert.ToInt32(forms.Get("id")));
        //    if (objStore != null)
        //    {
        //        objStore.type = forms.Get("type").ToString();
        //        objStore.updatedValue = forms.Get("updatedValue").ToString();
        //        objStore.storeId = Convert.ToInt32(forms.Get("storeId"));
        //        objStore.address = forms.Get("address").ToString();
        //        objStore.city = forms.Get("city").ToString();
        //        objStore.state = forms.Get("state").ToString();
        //        objStore.zip = forms.Get("zip").ToString();
        //        objStore.districtNumber =  Convert.ToInt32(forms.Get("districtNumber"));
        //       // objStore.districtName = forms.Get("districtName").ToString();
        //        objStore.areaNumber =  Convert.ToInt32(forms.Get("areaNumber"));
        //        //objStore.areaName = forms.Get("areaName").ToString();
        //        objStore.marketNumber = Convert.ToInt32(forms.Get("marketNumber"));
        //       // objStore.marketname = forms.Get("marketname").ToString();

        //    }
        //    var serializedCollection = json_serializer.Serialize(objStoreCollection);
        //    System.IO.File.WriteAllText(file_path, serializedCollection);
        //}
        else if (strOperation == "add")
        {
            var result = objStoreCollection.AsQueryable<StoreProfile>().Select(c => Convert.ToInt32(c.id)).Max();
            int strId = (Convert.ToInt32(result) + 1);
            StoreProfile objStore = new StoreProfile();
            objStore.id = strId;
            //objStore.type = forms.Get("type").ToString();
            objStore.type = "new";
            //objStore.updatedValue = forms.Get("updatedValue").ToString();
            objStore.updatedValue = "";
            objStore.storeId = Convert.ToInt32(forms.Get("storeId"));
            objStore.address = forms.Get("address").ToString();
            objStore.city = forms.Get("city").ToString();
            objStore.state = forms.Get("state").ToString();
            objStore.zip = forms.Get("zip").ToString();
            objStore.districtNumber = Convert.ToInt32(forms.Get("districtNumber"));
            objStore.districtName = forms.Get("districtName").ToString();
            objStore.areaNumber = Convert.ToInt32(forms.Get("areaNumber"));
            objStore.areaName = forms.Get("areaName").ToString();
            objStore.marketNumber = Convert.ToInt32(forms.Get("marketNumber"));
            objStore.isCurrent = true;
            objStore.isStoreUpdated = false;
            objStore.marketname = forms.Get("marketname").ToString();
            if(objStoreCollection.Where(c=>c.storeId==objStore.storeId).Count()>0)
            {
                var store_list = objStoreCollection.Where(c=>c.storeId==objStore.storeId);
                if(store_list.Where(c=>c.isCurrent==true).Count()>0)
                    context.Response.Write("This store Already Exists");
                else if (store_list.Where(c => c.isCurrent == false).Count() > 0)
                {
                    objStoreCollection.Remove(store_list.FirstOrDefault());
                    objStoreCollection.Add(objStore);
                }
            }
            else            
                objStoreCollection.Add(objStore);

            var serializedCollection = json_serializer.Serialize(objStoreCollection);
            System.IO.File.WriteAllText(file_path, serializedCollection);
        }
       
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

   
  
}