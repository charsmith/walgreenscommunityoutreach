﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using TdApplicationLib;
using tdEmailLib;
using TdWalgreens;
using System.Web.Services;
using System.Text;
using System.Xml.XPath;
using Microsoft.Reporting.WebForms;
using System.IO;
using NLog;
using System.Web;

public partial class walgreensCorporateClinicDetails : System.Web.UI.Page
{
    #region ----------------- PROTECTED EVENTS -----------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ddlClinicState.bindStates();

            this.commonAppSession.SelectedStoreSession.SelectedCorporateClinicsTabId = "ClinicInfo";
            if (this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk > 0)
            {
                bool is_scheduled = false;
                this.hfBusinessClinicStoreId.Value = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.hfBusinessClinicPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk.ToString();

                is_scheduled = this.bindClinicDetails(this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk);
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;

                this.displayControlsBasedOnUserTypes();
                this.displayTabs(is_scheduled);

                if (is_scheduled)
                {
                    this.btnHideUnScheduledAppts.Visible = false;
                    this.lblTotalApptScheduledText.Text = @"Total Scheduled";
                    this.getScheduledAppointments();
                    this.bindScheduledAppointments();
                }
                else
                    this.idScheduleAppointment.Visible = false;
            }
            else
                Response.Redirect("walgreensHome.aspx", false);
        }
        else
        {
            string event_args = Request["__EVENTTARGET"];
            if (!string.IsNullOrEmpty(event_args) && event_args != null)
            {
                if (event_args.ToLower() == "maintaincontactstatus")
                {
                    bool maintain_log = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                    if (!maintain_log)
                    {
                        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);

                        if (this.clinicPk > 0)
                            this.dbOperation.removeClinicContactLogs(this.clinicPk, "Corporate");
                    }

                    Session.Remove(this.hfBusinessClinicPk.Value);
                    Session.Remove("scheduledAppts" + this.clinicPk.ToString());
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                }
                else if (event_args.ToLower() == "rescheduleappts" || event_args.ToLower() == "cancelappts")
                {
                    bool reschedule_appts = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                    if (reschedule_appts)
                    {
                        //this.updatedAction = this.hfClinicUpdateAction.Value;
                        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
                        this.doSave(1, true);
                    }
                    else
                    {
                        this.hfIsOverlapAllowed.Value = "false";
                        XmlDocument clinic_details_prev = (XmlDocument)Session[this.hfBusinessClinicPk.Value];
                        this.txtClinicAddress1.Text = clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicAddress1"].Value;
                        this.txtClinicAddress2.Text = clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicAddress2"].Value;
                        //this.dtClinicDate.getSelectedDate = Convert.ToDateTime(clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicDate"].Value);
                        this.txtClinicDate.Text = clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicDate"].Value;
                        this.hfClinicDate.Value = clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicDate"].Value;
                        this.txtClinicStartTime.Text = clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicStartTime"].Value;
                        this.txtClinicEndTime.Text = clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicEndTime"].Value;
                        this.txtRoom.Text = clinic_details_prev.SelectSingleNode(".//clinicDetails/clinicInformation").Attributes["clinicRoom"].Value;
                    }
                }
                else if (event_args.ToLower() == "blockselectedappts")
                {
                    XmlDocument scheduled_appts_xml = new XmlDocument();
                    int return_value = 0;
                    scheduled_appts_xml = this.prepareClinicAppointmentsXML(0);
                    return_value = this.dbOperation.blockUnblockScheduledAppts(1, scheduled_appts_xml);
                    if (return_value == 0)
                    {
                        //Send Reschedule Your Appointment email notification for blocked appointments to the patients
                        if (!this.btnHideUnScheduledAppts.Visible)
                        {
                            this.sendBlockedAppointmentRescheduleEmail(scheduled_appts_xml);
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "scheduledApptsBlocked") + "');", true);
                        }
                        else
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "unScheduledApptsBlocked") + "');", true);
                    }
                }
                else if (event_args.ToLower() == "reloadappointments")
                {
                    this.getScheduledAppointments();
                    this.bindScheduledAppointments();
                }
                else if (event_args.ToLower() == "clinicdetailsupdated")
                {
                    bool reschedule_appts = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                    if (reschedule_appts)
                    {
                        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
                        if (this.ValidateLocations("UpdateClinic"))
                        {
                            if (this.clinicPk > 0 && Session[this.hfBusinessClinicPk.Value] != null)
                            {
                                this.hfClinicUpdateAction.Value = "Submit";
                                this.doSave(0);
                                if (!showAlertMessage)
                                {
                                    Session.Remove(this.hfBusinessClinicPk.Value);
                                    Session.Remove("scheduledAppts" + this.clinicPk.ToString());
                                    Response.Redirect("~/walgreensHome.aspx");
                                }
                            }
                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
                            return;
                        }
                    }
                    else
                        this.hfIsOverlapAllowed.Value = "false";

                    if (!showAlertMessage)
                    {
                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Session.Remove("scheduledAppts" + this.clinicPk.ToString());
                        Response.Redirect("~/walgreensHome.aspx");
                    }
                }
                else if (event_args.ToLower() == "datetimeoverlap")
                {
                    bool overlap_appts = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                    if (overlap_appts)
                    {
                        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
                        this.hfIsOverlapAllowed.Value = "true";
                        this.doSave(0);
                    }
                }
                else if (event_args.ToLower().Contains("continuesaving"))
                {
                    Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);

                    if (this.clinicPk > 0)
                        this.doSave(0, true);
                }
                else if (event_args.ToLower().Contains("continuesubmitting"))
                {
                    Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);

                    if (this.clinicPk > 0)
                        this.doSave(0, true, true);
                }
            }
        }
        this.setMinMaxDates();
        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void ValidateCompanyPhoneFax(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validatePhone();
    }

    protected void validateZipCode(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validateZipCode();
    }

    protected void ValidateStoreId(object sender, ServerValidateEventArgs e)
    {
        string value = this.txtClinicStore.Text;

        string regex_pattern_1 = @"^[0-9]+$";
        e.IsValid = true;
        if (!string.IsNullOrEmpty(value) && (!Regex.IsMatch(value, regex_pattern_1) || Convert.ToInt32(value) == 0))
            e.IsValid = false;
    }

    protected void grdScheduledAppt_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Convert.ToDateTime(((DataRowView)e.Row.DataItem)["apptDate"]) < DateTime.Now)
            {
                CheckBox chk_appt = (CheckBox)e.Row.FindControl("chkScheduledAppt");
                chk_appt.Enabled = false;
            }

            if (Convert.ToInt32(((DataRowView)e.Row.DataItem)["isBlocked"]) != 0)
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#99999f");
            }
        }
    }

    protected void grdScheduledAppt_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
        this.corporateScheduler = (CorporateScheduler)Session["scheduledAppts" + this.clinicPk.ToString()];
        if (!string.IsNullOrEmpty(this.txtSearchAppointments.Text))
        {
            DataRow[] dr_appts = this.corporateScheduler.scheduledAppointments.Select("businessClinic LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR clinicAddress LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR PatientName LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR confirmationId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptAvlTimes LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptDate LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%'");
            this.dtScheduledAppts.Clear();

            if (dr_appts.Count() > 0)
                this.dtScheduledAppts = new DataTable();
        }
        else
            this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;

        this.grdScheduledAppt.PageIndex = e.NewPageIndex;
        this.bindScheduledAppointments();
    }

    protected void grdScheduledAppts_OnSorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);

        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
        this.corporateScheduler = (CorporateScheduler)Session["scheduledAppts" + this.clinicPk.ToString()];
        if (!string.IsNullOrEmpty(this.txtSearchAppointments.Text))
        {
            DataRow[] dr_appts = this.corporateScheduler.scheduledAppointments.Select("PatientName LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR confirmationId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptAvlTimes LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%'");
            this.dtScheduledAppts.Clear();

            if (dr_appts.Count() > 0)
                this.dtScheduledAppts = dr_appts.CopyToDataTable();
        }
        else
            this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;

        this.bindScheduledAppointments();
    }

    protected void doProcess(object sender, CommandEventArgs e)
    {
        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);

        this.hfClinicUpdateAction.Value = e.CommandArgument.ToString();
        bool is_valid_form = true;

        if (this.hfClinicUpdateAction.Value != "Cancel")
        {
            if (this.hfClinicUpdateAction.Value == "Confirmed")
                is_valid_form = this.ValidateLocations("ConfirmClinic");
            else if (this.hfClinicUpdateAction.Value == "Completed")
                is_valid_form = this.ValidateLocations("ClinicCompleted");
            else if (this.hfClinicUpdateAction.Value == "Cancelled")
                is_valid_form = this.ValidateLocations("CancelledClinic");
            else
                is_valid_form = this.ValidateLocations("UpdateClinic");

            if (is_valid_form)
                this.doSave(0);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
        }
        else
        {
            if (!ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails"))
                this.doSave(0);
            else
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                Response.Redirect("~/walgreensHome.aspx");
            }
        }
    }

    protected void doApptsProcess_Click(object sender, CommandEventArgs e)
    {
        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
        switch (e.CommandArgument.ToString())
        {
            case "block":
                if (!this.btnHideUnScheduledAppts.Visible)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showBlockApptsWarning('" + (string)GetGlobalResourceObject("errorMessages", "blockScheduledAppts") + "');", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "doBlockUnscheduledAppts();", true);
                break;
            case "Unblock":
                int return_value = 0;
                return_value = this.dbOperation.blockUnblockScheduledAppts(0, this.prepareClinicAppointmentsXML(1));
                if (return_value == 0)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "scheduledApptsUnblocked") + "');", true);
                break;
            case "Search":
                this.corporateScheduler = (CorporateScheduler)Session["scheduledAppts" + this.clinicPk.ToString()];
                if (this.corporateScheduler != null && this.corporateScheduler.scheduledAppointments != null && this.corporateScheduler.scheduledAppointments.Rows.Count > 0)
                {
                    DataRow[] dr_appts = this.corporateScheduler.scheduledAppointments.Select("PatientName LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR confirmationId LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%' OR apptAvlTimes LIKE '%" + this.txtSearchAppointments.Text.Trim() + "%'");
                    this.dtScheduledAppts.Clear();
                    if (dr_appts.Count() > 0)
                        this.dtScheduledAppts = dr_appts.CopyToDataTable();

                    this.grdScheduledAppt.PageIndex = 0;
                }

                this.bindScheduledAppointments();
                break;
            case "Show All":
                this.getScheduledAppointments();
                this.bindScheduledAppointments();
                break;
            case "Show Unscheduled":
                this.btnShowUnScheduledAppts.Visible = false;
                this.btnHideUnScheduledAppts.Visible = true;
                this.getScheduledAppointments();
                this.bindScheduledAppointments();
                break;
            case "Hide Unscheduled":
                this.btnShowUnScheduledAppts.Visible = true;
                this.btnHideUnScheduledAppts.Visible = false;
                this.getScheduledAppointments();
                this.bindScheduledAppointments();
                break;
            case "View Report":
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = this.clinicPk;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.open('reports/displayScheduledApptsReport.aspx?pk=" + this.clinicPk.ToString() + "', '_blank', 'width=950,height=600,scrollbars=yes');", true);
                break;
        }
    }

    protected void grdClinicUpdatesHistory_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_update_action = (Label)e.Row.FindControl("lblAction");
            if (lbl_update_action.Text.ToLower() == "updated")
            {
                System.Web.UI.HtmlControls.HtmlTableCell update_field = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateField");
                update_field.ColSpan = 2;
                System.Web.UI.HtmlControls.HtmlTableCell update_value = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateValue");
                update_value.Visible = false;
            }
        }
    }
    #endregion

    #region ----------------- PRIVATE METHODS ------------------
    /// <summary>
    /// This method is using to validate page controls
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateLocations(string validation_group)
    {
        bool is_page_valid = true;

        //validation starts 
        is_page_valid = this.txtClinicStore.validateControls("textbox", "number", false, "Store Id is required", "Please enter valid Store Id", this.Page) && is_page_valid;
        is_page_valid = this.txtPlanId.validateControls("textbox", "string", false, "Plan ID is required", "Plan ID: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtGroupId.validateControls("textbox", "string", false, "Group ID is required", "Group ID: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtRecipientId.validateControls("textbox", "string", false, "ID Recipient is required", "ID Recipient: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtCoverageType.validateControls("textbox", "string", false, "Coverage Type is required", "Coverage Type: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtAddlComments.validateControls("textbox", "string", true, "", "Additional Comments Instructions: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtSpecialInstr.validateControls("textbox", "string", true, "", "Special Billing Instructions: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicLocation.validateControls("textbox", "string", true, "", "Clinic Location Name: < > Characters are not allowed", this.Page) && is_page_valid;

        foreach (GridViewRow row in this.grdVaccineInformation.Rows)
        {
            is_page_valid = ((TextBox)row.FindControl("txtEstimateVolume")).validateControls("textbox", "zero", false, "Estimated Volume is required", "Estimated Volume: Please enter a number", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtTotalImm")).validateControls("textbox", "zero", true, "Total Administered is required", "Total Administered: Please enter a number", this.Page) && is_page_valid;
            if (validation_group == "ClinicCompleted")
            {
                is_page_valid = ((TextBox)row.FindControl("txtTotalImm")).validateControls("textbox", "zero", false, "Total Administered is required", "Total Administered: Please enter a number", this.Page) && is_page_valid;
            }
        }

        is_page_valid = this.txtCopay.validateControls("textbox", "string", false, "Copay is required", "Employee Copay: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtFirstContactName.validateControls("textbox", "string", false, "Contact First Name is required", "Local Client Contact Name: , < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtContactPhone.validateControls("textbox", "phone", false, "Contact Phone is required", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        is_page_valid = this.txtContactPhoneExt.validateControls("textbox", "extension", true, "", "Please enter valid phone number extension(ex: #####)", this.Page) && is_page_valid;
        is_page_valid = this.txtContactEmail.validateControls("textbox", "email", false, "Contact Email is required", "Invalid contact email", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicAddress1.validateControls("textbox", "address", false, "Clinic Address1 is required", "Clinic Address1: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicAddress2.validateControls("textbox", "address", true, "", "Clinic Address1: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicCity.validateControls("textbox", "string", false, "Clinic City is required", "Clinic City:Clinic Address1: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicDate.validateControls("textbox", "string", false, "Select the Clinic Date", "", this.Page) && is_page_valid;
        is_page_valid = this.ddlClinicState.validateControls("dropdownlist", "string", false, "Select State", "", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicZip.validateControls("textbox", "zip", false, "ZipCode is required", "Please enter a valid Zip Code (ex: #####)", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicStartTime.validateControls("textbox", "string", false, "Clinic Start Time is required", "Clinic Start Time: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtClinicEndTime.validateControls("textbox", "string", false, "Clinic End Time is required", "Clinic End Time: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtRoom.validateControls("textbox", "string", true, "", "Clinic Room: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtClientIndividual.validateControls("textbox", "string", true, "This clinic cannot be logged as Confirmed until you have entered the \'Name of Client Individual Who Confirmed the Clinic\'.", "Client Individual: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtNameOfRxHost.validateControls("textbox", "string", true, "This clinic cannot be logged as Completed until you have entered \'Name of Rx Host\'.", "Name Of Rx Host: < > Characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtPharmacistPhone.validateControls("textbox", "phone", true, "This clinic cannot be logged as Completed until you have entered \'Phone #\'.", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        is_page_valid = this.txtTotalHoursClinicHeld.validateControls("textbox", "decimal", true, "This clinic cannot be logged as Completed until you have entered \'Total Hours Clinic Held\'.", "Please enter the Total Hours Clinic Held", this.Page) && is_page_valid;
        is_page_valid = this.txtFeedBack.validateControls("textbox", "string", true, "This clinic cannot be logged as Cancelled until you have entered \'Feedback/Notes\'.", "Feed Back: < > Characters are not allowed", this.Page) && is_page_valid;

        if (validation_group == "ClinicCompleted")
        {
            is_page_valid = this.txtNameOfRxHost.validateControls("textbox", "string", false, "This clinic cannot be logged as Completed until you have entered \'Name of Rx Host\'.", "Name Of Rx Host: < > Characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = this.txtPharmacistPhone.validateControls("textbox", "phone", false, "This clinic cannot be logged as Completed until you have entered \'Phone #\'.", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
            is_page_valid = this.txtTotalHoursClinicHeld.validateControls("textbox", "decimal", false, "This clinic cannot be logged as Completed until you have entered \'Total Hours Clinic Held\'.", "Please enter the Total Hours Clinic Held", this.Page) && is_page_valid;
        }

        if (validation_group == "ConfirmClinic" || validation_group == "ClinicCompleted")
            if (this.hfClinicType.Value != "7")
                is_page_valid = this.txtClientIndividual.validateControls("textbox", "string", false, "This clinic cannot be logged as Confirmed/Completed until you have entered the \'Name of Client Individual Who Confirmed the Clinic\'.", "Client Individual: < > Characters are not allowed", this.Page) && is_page_valid;
            else
                is_page_valid = true;
        if (validation_group == "CancelledClinic")
            is_page_valid = this.txtFeedBack.validateControls("textbox", "string", false, "This clinic cannot be logged as Cancelled until you have entered \'Feedback/Notes\'.", "Feed Back: < > Characters are not allowed", this.Page) && is_page_valid;

        if (is_page_valid)
        {
            if ((this.txtClinicAddress1.Text + this.txtClinicAddress2.Text).validateAddress())
                is_page_valid = true;
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert") + "');", true);
                is_page_valid = false;
            }
        }
        return is_page_valid;
        //validation ends
    }

    /// <summary>
    /// Binds selected corporate clinic details
    /// </summary>
    /// <param name="clinic_pk"></param>
    private bool bindClinicDetails(int clinic_pk)
    {
        bool is_scheduled = false;
        DataSet ds_clinic_details = this.dbOperation.getCorporateClinicDetails(clinic_pk, this.commonAppSession.LoginUserInfoSession.UserRole);

        if (ds_clinic_details != null && ds_clinic_details.Tables.Count > 0)
        {
            if (ds_clinic_details.Tables[0].Rows.Count > 0)
            {
                this.txtClinicStore.Text = ds_clinic_details.Tables[0].Rows[0]["clinicStoreId"].ToString();
                this.hfClinicLeadStoreId.Value = (ds_clinic_details.Tables[0].Rows[0]["leadStoreId"] != DBNull.Value) ? ds_clinic_details.Tables[0].Rows[0]["leadStoreId"].ToString() : "";
                this.lblClientName.Text = ds_clinic_details.Tables[0].Rows[0]["businessName"].ToString();
                this.hfClinicType.Value = (ds_clinic_details.Tables[0].Rows[0]["clinicType"] != DBNull.Value) ? ds_clinic_details.Tables[0].Rows[0]["clinicType"].ToString() : "";

                //Billing & Vaccine Information
                this.txtPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                this.lblPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                this.txtCoverageType.Text = (ds_clinic_details.Tables[0].Rows[0]["naClinicCoverageType"] != DBNull.Value) ? ds_clinic_details.Tables[0].Rows[0]["naClinicCoverageType"].ToString() : "";
                this.lblCoverageType.Text = (ds_clinic_details.Tables[0].Rows[0]["naClinicCoverageType"] != DBNull.Value) ? ds_clinic_details.Tables[0].Rows[0]["naClinicCoverageType"].ToString() : "";
                this.txtRecipientId.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();
                this.lblRecipientId.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();
                this.txtGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();
                this.lblGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();

                this.grdVaccineInformation.DataSource = ds_clinic_details.Tables[1];
                this.grdVaccineInformation.DataBind();

                this.txtCopay.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicCopay"].ToString().Replace("''", "'");
                this.lblCopay.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicCopay"].ToString().Replace("''", "'");
                this.txtAddlComments.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicAddlComments"].ToString().Replace("''", "'");
                this.txtSpecialInstr.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicSplBillingInstr"].ToString().Replace("''", "'");
                this.lblSpecialInstr.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicSplBillingInstr"].ToString().Replace("''", "'");

                this.txtFirstContactName.Text = ds_clinic_details.Tables[0].Rows[0]["naContactFirstName"].ToString() + " " + ds_clinic_details.Tables[0].Rows[0]["naContactLastName"].ToString();
                this.lblFirstContactName.Text = ds_clinic_details.Tables[0].Rows[0]["naContactFirstName"].ToString() + " " + ds_clinic_details.Tables[0].Rows[0]["naContactLastName"].ToString();
                this.txtContactPhone.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicContactPhone"].ToString();
                this.lblContactPhone.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicContactPhone"].ToString();
                this.txtContactPhoneExt.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicContactPhoneExt"].ToString();
                this.lblContactPhoneExt.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicContactPhoneExt"].ToString();
                this.txtClinicLocation.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicLocation"].ToString();
                this.txtContactEmail.Text = ds_clinic_details.Tables[0].Rows[0]["naContactEmail"].ToString();
                this.lblContactEmail.Text = ds_clinic_details.Tables[0].Rows[0]["naContactEmail"].ToString();
                this.txtClinicAddress1.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicAddress1"].ToString();
                this.txtClinicAddress2.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicAddress2"].ToString();
                this.txtClinicCity.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicCity"].ToString();
                this.ddlClinicState.SelectedValue = ds_clinic_details.Tables[0].Rows[0]["naClinicState"].ToString();
                this.txtClinicZip.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicZip"].ToString();
                this.txtRoom.Text = ds_clinic_details.Tables[0].Rows[0]["clinicRoom"].ToString();

                if (ds_clinic_details.Tables[0].Rows[0]["naClinicDate"] != DBNull.Value && ds_clinic_details.Tables[0].Rows[0]["naClinicDate"].ToString().Trim().Length != 0)
                {
                    this.txtClinicDate.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicDate"].ToString();
                    this.hfClinicDate.Value = ds_clinic_details.Tables[0].Rows[0]["naClinicDate"].ToString(); ;
                }

                this.txtClinicStartTime.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicStartTime"].ToString();
                this.txtClinicEndTime.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicEndTime"].ToString();

                if (!string.IsNullOrEmpty(this.hfClinicDate.Value) && ds_clinic_details.Tables[0].Rows[0]["naClinicDate"] != DBNull.Value && ds_clinic_details.Tables[0].Rows[0]["naClinicDate"].ToString().Trim().Length != 0)
                {
                    DateTime clinic_date = Convert.ToDateTime(this.hfClinicDate.Value);
                    Int32 start_time = 0;
                    if (this.txtClinicStartTime.Text.Length > 0 && this.txtClinicStartTime.Text.ToUpper().Contains("PM"))
                        start_time = this.txtClinicStartTime.Text.IndexOf(':') > 0 ? Convert.ToInt32(this.txtClinicStartTime.Text.Substring(0, this.txtClinicStartTime.Text.IndexOf(':'))) + 12 : Convert.ToInt32(this.txtClinicStartTime.Text.ToUpper().Replace("PM", "")) + 12;
                    else
                        start_time = this.txtClinicStartTime.Text.IndexOf(':') > 0 ? Convert.ToInt32(this.txtClinicStartTime.Text.Substring(0, this.txtClinicStartTime.Text.IndexOf(':'))) : Convert.ToInt32(this.txtClinicStartTime.Text.ToUpper().Replace("AM", ""));

                    clinic_date = clinic_date.AddHours(Convert.ToDouble(start_time));
                    if (this.txtClinicStartTime.Text.IndexOf(':') > 0)
                        clinic_date = clinic_date.AddMinutes(Convert.ToDouble(this.txtClinicStartTime.Text.Substring(this.txtClinicStartTime.Text.IndexOf(':') + 1, 2))).AddMinutes(60.0);
                    else
                        clinic_date = clinic_date.AddMinutes(60.0);

                    this.hfClinicStartTime.Value = clinic_date.ToShortTimeString();
                }
                if (this.hfClinicType.Value == "7")
                {
                    this.txtClientIndividual.Visible = false;
                    logSubTitle.Visible = false;
                }
                else
                {
                    this.txtClientIndividual.Text = ds_clinic_details.Tables[0].Rows[0]["confirmedClientName"].ToString();
                }
                this.txtNameOfRxHost.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistName"].ToString();
                if (ds_clinic_details.Tables[0].Rows[0]["pharmacistPhone"].ToString().Length == 10)
                    this.txtPharmacistPhone.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistPhone"].ToString().Substring(0, 3) + "-" + ds_clinic_details.Tables[0].Rows[0]["pharmacistPhone"].ToString().Substring(3, 3) + "-" + ds_clinic_details.Tables[0].Rows[0]["pharmacistPhone"].ToString().Substring(6, 4);

                this.txtTotalHoursClinicHeld.Text = ds_clinic_details.Tables[0].Rows[0]["totalHours"].ToString();

                //Show/hide outreach contact status buttons
                this.btnCancelClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                this.btnCancelClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                this.btnConfirmedClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                this.btnConfirmedClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                this.btnClinicCompleted.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                this.btnClinicCompletedDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());

                if (ds_clinic_details.Tables[0].Rows[0]["leadStoreId"] != DBNull.Value && this.hfBusinessClinicStoreId.Value != ds_clinic_details.Tables[0].Rows[0]["clinicStoreId"].ToString() && ds_clinic_details.Tables[0].Rows[0]["clinicStoreId"].ToString() != ds_clinic_details.Tables[0].Rows[0]["leadStoreId"].ToString())
                {
                    this.hfBusinessClinicStoreId.Value = ds_clinic_details.Tables[0].Rows[0]["leadStoreId"].ToString();
                    this.btnClinicCompleted.Visible = false;
                    this.btnClinicCompletedDim.Visible = false;
                }

                //disable edit clinic details facility to specific clients
                bool is_disable_edit_clinic_details = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails");
                if (is_disable_edit_clinic_details)
                {
                    this.btnCancelClinic.Visible = false;
                    this.btnConfirmedClinic.Visible = false;
                    this.btnClinicCompleted.Visible = false;
                    this.btnSubmitDetails.Visible = false;
                    this.btnCancelClinicDim.Visible = true;
                    this.btnConfirmedClinicDim.Visible = true;
                    this.btnClinicCompletedDim.Visible = true;
                    this.btnSubmitDetailsDim.Visible = true;
                }

                //Bind clinic updates history log
                this.grdClinicUpdatesHistory.DataSource = ApplicationSettings.getClinicUpdateHistory(ds_clinic_details.Tables[2], "corporate");
                this.grdClinicUpdatesHistory.DataBind();

                is_scheduled = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isScheduled"].ToString());
                this.hfDesignPk.Value = ds_clinic_details.Tables[0].Rows[0]["designPk"].ToString();
                Session[this.hfBusinessClinicPk.Value] = this.prepareXmlDocument();
                System.Web.HttpBrowserCapabilities browser = Request.Browser;
                //this.Logger.Trace("Client browser(Server-side)::" + browser.Browser + " " + browser.Version+"; Action::Page Load; Username::" + this.commonAppSession.LoginUserInfoSession.UserName + "; StoreId::" + this.hfBusinessClinicStoreId.Value + "; ClinicPk::" + this.hfBusinessClinicPk.Value + "; Clinic Location::" + this.lblClientName.Text);
            }
        }
        return is_scheduled;
    }

    /// <summary>
    /// Shows/hides display controls based on login user role
    /// </summary>
    private void displayControlsBasedOnUserTypes()
    {
        if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.txtContactPhone.Visible = false;
            this.txtContactPhoneExt.Visible = false;
            this.txtFirstContactName.Visible = false;
            this.txtContactEmail.Visible = false;
            this.txtPlanId.Visible = false;
            this.txtCoverageType.Visible = false;
            this.txtGroupId.Visible = false;
            this.txtRecipientId.Visible = false;
            this.txtCopay.Visible = false;

            this.lblClientName.Visible = true;
            this.lblContactPhone.Visible = true;
            this.lblContactPhoneExt.Visible = true;
            this.lblFirstContactName.Visible = true;
            this.lblContactEmail.Visible = true;
            this.lblCoverageType.Visible = true;
            this.lblPlanId.Visible = true;
            this.lblCopay.Visible = true;
            this.lblGroupId.Visible = true;
            this.lblRecipientId.Visible = true;
        }

        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.txtClinicStore.Enabled = true;
            this.txtSpecialInstr.Visible = true;
            this.lblSpecialInstr.Visible = false;
        }

        this.txtClinicStore.Enabled = ApplicationSettings.isAuthorisedToChangeClinicStore(this.commonAppSession.LoginUserInfoSession.UserRole);
    }

    /// <summary>
    /// Displays Appointments tab if clinic is scheduled
    /// </summary>
    /// <param name="is_scheduled"></param>
    private void displayTabs(bool is_scheduled)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<li class='profileTabs' style='width: 150px; height: 30px; vertical-align:middle; text-align:center; '><a id='clinicInfo'  href='#idClinicInfo' runat='server' onclick=setSelectedTab('ClinicInfo')><span style='font-weight:bold'>Clinic Information</span></a></li>");
        if (is_scheduled)
            sb.Append("<li class='profileTabs' style='width: 200px; height: 30px; vertical-align:middle; text-align:center; '><a id='scheduleAppointment'  href='#idScheduleAppointment' runat='server' onclick=setSelectedTab('ScheduleAppointment')><span style='font-weight:bold'>Scheduled Appointments</span></a></li>");
        this.lblTabLinks.Text = sb.ToString();
    }

    /// <summary>
    /// Gets Scheduled/Unscheduled appointments
    /// </summary>
    private void getScheduledAppointments()
    {
        int total_immunizers = 0;
        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
        this.txtSearchAppointments.Text = "";
        this.lblTotalScheduledAppts.Text = "";
        if (this.btnHideUnScheduledAppts.Visible)
        {
            this.lblTotalApptScheduledText.Text = @"Total unscheduled: ";
            this.corporateScheduler.scheduledAppointments = this.dbOperation.getUnScheduledAppointments(0, this.clinicPk, out total_immunizers);
        }
        else
        {
            this.lblTotalApptScheduledText.Text = @"Total Scheduled: ";
            this.corporateScheduler.scheduledAppointments = this.dbOperation.getClinicScheduledAppointments(0, this.clinicPk, out total_immunizers);
        }

        Session["scheduledAppts" + this.clinicPk.ToString()] = this.corporateScheduler;
        ViewState["sortOrder"] = null;
        this.grdScheduledAppt.PageIndex = 0;

        this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;
        this.lblTotalImmunizers.Text = total_immunizers.ToString();
    }

    /// <summary>
    /// Binds Scheduled/Unscheduled appointments
    /// </summary>
    private void bindScheduledAppointments()
    {
        bool has_rows = false;

        if (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0)
        {
            has_rows = true;
            if (ViewState["sortOrder"] != null)
                this.dtScheduledAppts.DefaultView.Sort = ViewState["sortOrder"].ToString();

            this.lblTotalScheduledAppts.Text = (this.dtScheduledAppts.Select("isBlocked = 0").Count() > 0) ? this.dtScheduledAppts.Select("isBlocked = 0").Count().ToString() : "";
        }

        this.grdScheduledAppt.DataSource = this.dtScheduledAppts;
        this.grdScheduledAppt.DataBind();

        this.lblTotalApptScheduledText.Visible = has_rows;
        this.lblTotalScheduledAppts.Visible = has_rows;
        this.btnBlockCheckedAppts.Enabled = (has_rows && (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0 && this.dtScheduledAppts.Select("isBlocked = 0").Count() > 0)) ? true : false; ;
        this.btnUnBlockCheckedAppts.Enabled = (has_rows && (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0 && this.dtScheduledAppts.Select("isBlocked = 1 or isBlocked = 2").Count() > 0)) ? true : false;
    }

    /// <summary>
    /// Prepares clinic details xml document
    /// </summary>
    /// <returns></returns>
    private XmlDocument prepareXmlDocument()
    {
        XmlDocument clinic_details = new XmlDocument();
        XmlElement clinic_details_ele = clinic_details.CreateElement("clinicDetails");
        XmlElement clinic_info = clinic_details.CreateElement("clinicInformation");
        XmlElement clinic_immunizations = clinic_details.CreateElement("Immunizations");

        clinic_info.SetAttribute("contactFirstName", (this.commonAppSession.LoginUserInfoSession.IsAdmin ? this.txtFirstContactName.Text.Trim() : this.lblFirstContactName.Text.Trim()));
        clinic_info.SetAttribute("contactPhone", (this.commonAppSession.LoginUserInfoSession.IsAdmin ? this.txtContactPhone.Text : this.lblContactPhone.Text).Replace("(", "").Replace(")", "").Replace(" ", "").Trim());
        clinic_info.SetAttribute("contactPhoneExt", (this.commonAppSession.LoginUserInfoSession.IsAdmin ? this.txtContactPhoneExt.Text : this.lblContactPhoneExt.Text).Replace("(", "").Replace(")", "").Replace(" ", "").Trim());
        clinic_info.SetAttribute("contactEmail", (this.commonAppSession.LoginUserInfoSession.IsAdmin ? this.txtContactEmail.Text.Trim() : this.lblContactEmail.Text.Trim()));

        clinic_info.SetAttribute("clinicLocation", this.txtClinicLocation.Text.Trim().Replace("'", "''").Trim());
        clinic_info.SetAttribute("clinicAddress1", this.txtClinicAddress1.Text.Trim().Trim());
        clinic_info.SetAttribute("clinicDate", this.hfClinicDate.Value);
        clinic_info.SetAttribute("clinicAddress2", this.txtClinicAddress2.Text.Trim());
        clinic_info.SetAttribute("clinicStartTime", this.txtClinicStartTime.Text.Trim().ToUpper());
        clinic_info.SetAttribute("clinicCity", this.txtClinicCity.Text.Trim());
        clinic_info.SetAttribute("clinicEndTime", this.txtClinicEndTime.Text.Trim().ToUpper());
        clinic_info.SetAttribute("clinicState", ddlClinicState.SelectedValue.Trim());
        clinic_info.SetAttribute("clinicZip", txtClinicZip.Text.Trim());
        clinic_info.SetAttribute("clinicPlanId", (this.commonAppSession.LoginUserInfoSession.IsAdmin ? this.txtPlanId.Text.Trim() : this.lblPlanId.Text.Trim()));
        clinic_info.SetAttribute("clinicCoverageType", this.txtCoverageType.Text.Trim());

        clinic_info.SetAttribute("clinicGroupId", (this.commonAppSession.LoginUserInfoSession.IsAdmin ? this.txtGroupId.Text.Trim() : this.lblGroupId.Text.Trim()));
        //this.lblCopay.Text = this.lblCopay.Text.Trim().Length == 0 ? "0" : this.lblCopay.Text;
        clinic_info.SetAttribute("clinicCopay", (this.commonAppSession.LoginUserInfoSession.IsAdmin ? this.txtCopay.Text.Trim().Replace("'", "''") : this.lblCopay.Text.Trim()));
        clinic_info.SetAttribute("clinicAddlComments", txtAddlComments.Text.Trim().Replace("'", "''"));
        clinic_info.SetAttribute("clinicSplBillingInstr", this.txtSpecialInstr.Text.Trim().Replace("'", "''"));

        clinic_info.SetAttribute("recipientId", this.txtRecipientId.Text.Trim());
        clinic_info.SetAttribute("confirmedClientName", this.txtClientIndividual.Text.Trim());
        clinic_info.SetAttribute("pharmacistName", this.txtNameOfRxHost.Text.Trim());
        clinic_info.SetAttribute("pharmacistPhone", (!string.IsNullOrEmpty(this.txtPharmacistPhone.Text) ? this.txtPharmacistPhone.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim() : string.Empty));
        clinic_info.SetAttribute("totalHours", this.txtTotalHoursClinicHeld.Text.Trim());
        clinic_info.SetAttribute("clinicRoom", this.txtRoom.Text.Trim());
        clinic_info.SetAttribute("feedBack", this.txtFeedBack.Text.Trim());
        if (ApplicationSettings.isAuthorisedToChangeClinicStore(this.commonAppSession.LoginUserInfoSession.UserRole))
        {
            int new_store_id, current_store_id, lead_store_id;
            Int32.TryParse(this.hfBusinessClinicStoreId.Value, out current_store_id);
            Int32.TryParse(this.hfClinicLeadStoreId.Value, out lead_store_id);
            Int32.TryParse(this.txtClinicStore.Text, out new_store_id);

            clinic_info.SetAttribute("clinicStoreId", (new_store_id > 0 && new_store_id != current_store_id && new_store_id != lead_store_id) ? new_store_id.ToString() : "");
        }
        else
            clinic_info.SetAttribute("clinicStoreId", "");

        foreach (GridViewRow row in this.grdVaccineInformation.Rows)
        {
            XmlElement immunization_ele = clinic_details.CreateElement("Immunization");
            immunization_ele.SetAttribute("pk", ((Label)row.FindControl("lblImmunizationPk")).Text);
            immunization_ele.SetAttribute("immunizationName", ((Label)row.FindControl("lblVaccinesCovered")).Text);
            immunization_ele.SetAttribute("estimatedQuantity", ((TextBox)row.FindControl("txtEstimateVolume")).Text);
            immunization_ele.SetAttribute("totalImmAdministered", ((TextBox)row.FindControl("txtTotalImm")).Text);
            clinic_immunizations.AppendChild(immunization_ele);
        }

        clinic_info.AppendChild(clinic_immunizations);
        clinic_details_ele.AppendChild(clinic_info);
        clinic_details.AppendChild(clinic_details_ele);
        return clinic_details;
    }

    /// <summary>
    /// Saves clinic details
    /// </summary>
    /// <param name="is_block"></param>
    private void doSave(int is_block, bool is_override = false, bool is_continue = false)
    {
        string last_contact_status = string.Empty, email_body = string.Empty, error_message = string.Empty;//, billing_email = string.Empty;
        //bool send_emailto_clinical_contract = false;
        int store_id, new_store_id, lead_store_id, return_value = 0;
        bool is_clinicdate_past = false, clinic_date_changed_2weeks = false, est_qnt_increased = false;
        Int32.TryParse(this.hfBusinessClinicStoreId.Value, out store_id);
        Int32.TryParse(this.hfClinicLeadStoreId.Value, out lead_store_id);
        Int32.TryParse(this.txtClinicStore.Text, out new_store_id);
        string max_qty_error = string.Empty;
        bool has_max_qty = false;
        if (new_store_id == 0 || new_store_id == store_id || new_store_id == lead_store_id || store_id == lead_store_id)
            new_store_id = 0;

        if (Session[this.hfBusinessClinicPk.Value] != null && this.clinicPk > 0 && this.commonAppSession.LoginUserInfoSession != null)
        {
            //Check clinic rooms and filter
            if (!string.IsNullOrEmpty(this.txtRoom.Text))
            {
                if (this.txtRoom.Text.Trim().Replace(", ", ",").Split(',').Count() > 1)
                {
                    List<string> lst_rooms = new List<string>();
                    this.txtRoom.Text.Trim().Replace(", ", ",").ToString()
                                            .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                                     .ToList()
                                                     .ForEach(room => lst_rooms.Add(room.Trim().removeJunkCharacters()));

                    this.txtRoom.Text = string.Empty;
                    foreach (string room in lst_rooms.Distinct(StringComparer.CurrentCultureIgnoreCase))
                    {
                        if (!string.IsNullOrEmpty(room.Trim().removeJunkCharacters()))
                            this.txtRoom.Text += (!string.IsNullOrEmpty(this.txtRoom.Text) ? ", " + room.removeJunkCharacters() : room.removeJunkCharacters());
                    }
                }
                else
                    this.txtRoom.Text = this.txtRoom.Text.Trim().removeJunkCharacters();
            }

            XmlDocument new_clinic_details_xml = this.prepareXmlDocument();
            bool is_equal = ApplicationSettings.compareXMLDocuments(new_clinic_details_xml, ((XmlDocument)Session[this.hfBusinessClinicPk.Value]));

            //this.Logger.Trace("Action: " + this.hfClinicUpdateAction.Value + "; Username::" + this.commonAppSession.LoginUserInfoSession.UserName + "; StoreId::" + this.hfBusinessClinicStoreId.Value + "; ClinicPk::" + this.hfBusinessClinicPk.Value + "; Clinic Location::" + this.lblClientName.Text);
            //Warning to save changes before returning to home page
            switch (this.hfClinicUpdateAction.Value.ToLower())
            {
                case "cancel":
                    if (!is_equal)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showUpdateClinicWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "clinicDetailsChanged")) + "');", true);
                        return;
                    }
                    else
                    {
                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Response.Redirect("~/walgreensHome.aspx");
                    }
                    break;
                case "submit":
                    if (is_equal)
                        return;
                    break;
                case "confirmed":
                case "completed":
                case "cancelled":
                    if (new_store_id > 0)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showStoreChangeWithContactStatusWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "storeChangedWithContactStatusUpdate")) + "');", true);
                        return;
                    }
                    break;
            }
            //Commenting this logic as we are not restricting this for 2017-18 season
            //Show alert if the clinic doesn't meet 75 minimum estimated shots. But do not restrict while completing or cancelling the clinic
            //if (this.hfClinicUpdateAction.Value.ToLower() != "completed" && this.hfClinicUpdateAction.Value.ToLower() != "cancelled")
            //{
            //    XPathNavigator xpath_navigator = new_clinic_details_xml.CreateNavigator();
            //    var total_est_shots = (double)xpath_navigator.Evaluate("sum(//Immunizations/Immunization/@estimatedQuantity)");
            //    if (total_est_shots < 75 && this.hfClinicType.Value == "3")
            //    {
            //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('The total quantity of Estimated Shots per Immunization does not meet the 75 shot minimum requirement to perform a clinic.');", true);
            //        return;
            //    }
            //}

            var clinic_dates = from dates in XDocument.Parse(new_clinic_details_xml.InnerXml).Descendants("clinicInformation")
                               select dates.Attribute("clinicDate").Value;
            XmlDocument old_clinic_details_xml = ((XmlDocument)Session[this.hfBusinessClinicPk.Value]);
            var old_imm_list = old_clinic_details_xml.SelectNodes("/clinicDetails/clinicInformation/Immunizations/Immunization/@estimatedQuantity").Cast<XmlNode>().ToList();
            var new_imm_list = new_clinic_details_xml.SelectNodes("/clinicDetails/clinicInformation/Immunizations/Immunization/@estimatedQuantity").Cast<XmlNode>().ToList();
            var imm_name_list = new_clinic_details_xml.SelectNodes("/clinicDetails/clinicInformation/Immunizations/Immunization/@immunizationName").Cast<XmlNode>().ToList();
            if (Convert.ToDateTime(clinic_dates.FirstOrDefault()).Date < DateTime.Now.Date.AddDays(14) && !is_override)
            {
                if (old_clinic_details_xml != null && !string.IsNullOrEmpty(old_clinic_details_xml.SelectSingleNode("/clinicDetails/clinicInformation/@clinicDate").Value))
                {
                    DateTime previous_clinic_date = Convert.ToDateTime(old_clinic_details_xml.SelectSingleNode("/clinicDetails/clinicInformation/@clinicDate").Value);

                    if (previous_clinic_date != Convert.ToDateTime(clinic_dates.FirstOrDefault()))
                    {
                        clinic_date_changed_2weeks = true;
                    }
                }
            }
            if (old_imm_list != new_imm_list)
            {
                for (int i = 0; i < old_imm_list.Count; i++)
                {
                    int old_est_shots, new_est_shots;
                    int.TryParse(old_imm_list[i].Value, out old_est_shots);
                    int.TryParse(new_imm_list[i].Value, out new_est_shots);
                    if (new_est_shots > old_est_shots && Convert.ToDateTime(clinic_dates.FirstOrDefault()).Date < DateTime.Now.Date.AddDays(14) && !is_override)
                        est_qnt_increased = true;
                    if (new_est_shots != old_est_shots && new_est_shots > 250)
                    {
                        has_max_qty = true;
                        string location_number = this.lblClientName.Text.Split('-')[1].Trim().Replace("Clinic ", "");
                        max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), new_est_shots, imm_name_list[i].Value, location_number);

                    }
                }
            }


            if (this.hfClinicUpdateAction.Value.ToLower() != "completed")
            {
                string validation_summary = string.Empty;
                List<string> error_list = new List<string>();
                bool is_having_override = true;
                if (clinic_date_changed_2weeks)
                {
                    error_list.Add((string)GetGlobalResourceObject("errorMessages", "clinicChangedBefore2Weeks"));
                    if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                        is_having_override = false;
                }
                if (est_qnt_increased)
                {
                    error_list.Add("<b style=\"color: red;\">" + (string)GetGlobalResourceObject("errorMessages", "immQtyIncreasedBefore2Weeks") + "</b>");
                    if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
                        is_having_override = false;
                }

                if (error_list.Count > 0)
                {
                    validation_summary = "<ul>";
                    foreach (var item in error_list)
                    {
                        validation_summary += "<li style=\"text-align:left\">" + item.Replace("\n", "<br />").Trim() + "</li><br/>";
                    }
                    validation_summary += "</ul>";
                    this.showAlertMessage = true;
                    if (is_having_override)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryWarning('" + validation_summary + "','continuesaving');", true);
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryAlert('" + validation_summary + "','continuesaving');", true);
                    validation_summary = "";
                    error_list.Clear();
                    return;
                }
                if (has_max_qty && !is_continue)
                {
                    this.showAlertMessage = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showWarning('WARNING','" + max_qty_error.Substring(6) + "','continuesubmitting');", true);
                    return;
                }
            }

            Dictionary<string, string> updated_values = ApplicationSettings.getUpdatedClinicDetails(new_clinic_details_xml, old_clinic_details_xml, "corporate");
            email_body = updated_values["emailBody"];
            //billing_email = updated_values["billingEmail"];

            if (!string.IsNullOrEmpty(updated_values["historyLog"]))
            {
                //Prepare updated fields xml and append to clinic details xml document
                XmlDocumentFragment xml_frag = new_clinic_details_xml.CreateDocumentFragment();
                xml_frag.InnerXml = updated_values["historyLog"];
                new_clinic_details_xml.DocumentElement.AppendChild(xml_frag);

                //send_emailto_clinical_contract = CommonExtensionsMethods.sendEmailToClinicalContract(updated_values["historyLog"]);
            }

            //bool can_reassign_clinic = !(ApplicationSettings.isRestrictedStoreState("MO", this.commonAppSession.LoginUserInfoSession.UserRole));
            return_value = this.dbOperation.updateClinicDetails(this.clinicPk, new_clinic_details_xml, is_block, this.commonAppSession.LoginUserInfoSession.UserID, (this.commonAppSession.LoginUserInfoSession.IsAdmin ? 1 : 0), this.hfClinicUpdateAction.Value, false, !string.IsNullOrEmpty(this.hfIsOverlapAllowed.Value) ? Convert.ToBoolean(this.hfIsOverlapAllowed.Value) : false, out last_contact_status, out error_message, out is_clinicdate_past, out this.dtScheduledAppts);

            if (return_value == -2)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + string.Format((string)GetGlobalResourceObject("errorMessages", "storeIdDoNotExists"), this.txtClinicStore.Text) + "');", true);
                return;
            }
            else if (return_value == -3)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showCancelClinicWarning('" + (string)GetGlobalResourceObject("errorMessages", "cancelClinicWarning") + "');", true);
                return;
            }
            else if (return_value == -4)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showCancelClinicWarning('" + (string)GetGlobalResourceObject("errorMessages", "cancelPastClinicWarning") + "');", true);
                return;
            }
            else if (return_value == -5)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showChangeClinicDetailsWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "changeClinicDetailsWarning"), error_message.Trim().TrimEnd(',')) + "');", true);
                return;
            }
            else if (return_value == -6)
            {
                if (commonAppSession.LoginUserInfoSession.IsAdmin)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showChangeClinicDetailsWarning('" + (string)GetGlobalResourceObject("errorMessages", "changeClinicRoomWarning") + "');", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "changeClinicRoomError") + "');", true);

                return;
            }
            else if (return_value == -7 || return_value == -8)
            {
                this.showAlertMessage = true;
                string error_template = this.isOverlapDBAuthorized ? (string)GetGlobalResourceObject("errorMessages", "datetimeOverlapSingleConflictWarning") : (string)GetGlobalResourceObject("errorMessages", "datetimeOverlapSingleConflictDetails");
                string validation_message = String.Format(error_template, error_message.Split('<')[1].Trim(), new_clinic_details_xml.SelectNodes("/clinicDetails/clinicInformation")[0].Attributes["clinicDate"].Value, new_clinic_details_xml.SelectNodes("/clinicDetails/clinicInformation")[0].Attributes["clinicStartTime"].Value, new_clinic_details_xml.SelectNodes("/clinicDetails/clinicInformation")[0].Attributes["clinicEndTime"].Value, error_message.Split('<')[0].Trim());
                if (this.isOverlapDBAuthorized)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDateTimeStampValidationWarning('" + validation_message + "','Single Conflict Alert');", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDateTimeStampValidationMessage('" + validation_message + "','Single Conflict Alert');", true);
                return;
            }
            else if (return_value == -9 && !string.IsNullOrEmpty(error_message))//Assigned to Restricted store state
            {
                this.showAlertMessage = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
                return;
            }
            else if (return_value == 0)
            {
                EmailOperations email_operations = new EmailOperations();

                //Send appointment reschedule emails to scheduled patients
                if (this.dtScheduledAppts != null && this.dtScheduledAppts.Rows.Count > 0 && !is_clinicdate_past)
                {
                    if (this.hfClinicUpdateAction.Value.ToLower() != "cancelled")
                        email_operations.sendRescheduleAppointmentEmail(this.dtScheduledAppts, this.hfDesignPk.Value);
                    else
                    {
                        //Send clinic cancellation notification to admin group                    
                        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                            email_operations.sendCancelClinicApptsEmail(this.dtScheduledAppts);
                        else
                            email_operations.sendClinicCancelNotificationToAdmin(this.dtScheduledAppts, is_clinicdate_past, this.commonAppSession.LoginUserInfoSession.UserName, this.commonAppSession.LoginUserInfoSession.IsAdmin);
                    }
                }

                DataTable dt_default_clinic_user = dbOperation.getStoreUsersEmails(new_store_id > 0 ? new_store_id : store_id);

                if (dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
                {
                    //Sending clinic details changed email 'Business Information'...
                    //if (!string.IsNullOrEmpty(email_body) || !string.IsNullOrEmpty(billing_email))
                    if (!string.IsNullOrEmpty(email_body))
                        email_operations.sendClinicDetailsChangedEmail(dt_default_clinic_user, "Corporate", email_body, string.Empty, this.lblClientName.Text, this.clinicPk.ToString(), "0", (store_id != new_store_id) ? new_store_id.ToString() : store_id.ToString(), false);

                    //Sending store reassignment email...
                    if ((new_store_id > 0) && (ApplicationSettings.isAuthorisedToChangeClinicStore(this.commonAppSession.LoginUserInfoSession.UserRole)))
                    {
                        //Send revised store assignment email to new store user
                        string clinic_time, clinic_full_address;

                        clinic_full_address = this.txtClinicAddress1.Text.Trim() + " " + this.txtClinicAddress2.Text.Trim() + this.txtClinicCity.Text.Trim();
                        clinic_full_address = (string.IsNullOrEmpty(clinic_full_address.Trim())) ? clinic_full_address + this.ddlClinicState.SelectedValue + this.txtClinicZip.Text.Trim() : clinic_full_address + ", " + this.ddlClinicState.SelectedValue + this.txtClinicZip.Text.Trim();

                        clinic_time = this.txtClinicStartTime.Text.Trim();
                        clinic_time = (!string.IsNullOrEmpty(clinic_time)) ? clinic_time + ((!string.IsNullOrEmpty(this.txtClinicEndTime.Text)) ? " - " + this.txtClinicEndTime.Text.Trim() : "") : clinic_time + this.txtClinicEndTime.Text.Trim();

                        email_operations.sendRevisedStoreAssignments(dt_default_clinic_user, new_store_id, this.clinicPk, this.lblClientName.Text.Trim(), clinic_time, clinic_full_address, this.hfClinicDate.Value, ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtPlanId.Text.Trim() : this.lblPlanId.Text.Trim()), ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtGroupId.Text.Trim() : this.lblGroupId.Text.Trim()));

                        //Sending store reassigned email to old store user
                        dt_default_clinic_user = dbOperation.getStoreUsersEmails(store_id);
                        if (dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
                            email_operations.sendCorporateClinicStoreReassignmentEmail(dt_default_clinic_user, this.lblClientName.Text);

                        if (!string.IsNullOrEmpty(last_contact_status) && this.hfClinicType.Value != "7")
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showMaintainContactLogWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "maintainContactLogCorporate"), last_contact_status) + "');", true);
                            return;
                        }
                        else
                        {
                            Session.Remove(this.hfBusinessClinicPk.Value);
                            Session.Remove("scheduledAppts" + this.clinicPk.ToString());
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                        }
                    }
                }

                int clinic_pk = 0;
                int.TryParse(this.hfBusinessClinicPk.Value, out clinic_pk);

                if (!string.IsNullOrEmpty(this.hfClinicUpdateAction.Value) && this.hfClinicUpdateAction.Value.ToLower() == "confirmed")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "confirmedClinic") + "');", true);
                else if (!string.IsNullOrEmpty(this.hfClinicUpdateAction.Value) && this.hfClinicUpdateAction.Value.ToLower() == "completed")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "clinicCompleted") + "');", true);
                else if (!string.IsNullOrEmpty(this.hfClinicUpdateAction.Value) && this.hfClinicUpdateAction.Value.ToLower() == "cancelled")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showBlockUnblockApptsSuccessMesg('" + (string)GetGlobalResourceObject("errorMessages", "clinicCancelled") + "');", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); ", true);

                bool is_scheduled = false;

                is_scheduled = this.bindClinicDetails(clinic_pk);
                if (is_scheduled)
                {
                    this.btnHideUnScheduledAppts.Visible = false;
                    this.lblTotalApptScheduledText.Text = @"Total Scheduled";
                    this.getScheduledAppointments();
                    this.bindScheduledAppointments();
                }
                else
                    this.idScheduleAppointment.Visible = false;
            }
        }
        else
        {
            Session.Remove(this.hfBusinessClinicPk.Value);
            Response.Redirect("~/walgreensHome.aspx");
        }
    }

    /// <summary>
    /// Creates block/unblock appointments xml document
    /// </summary>
    /// <param name="action_type"></param>
    /// <returns></returns>
    private XmlDocument prepareClinicAppointmentsXML(int action_type)
    {
        XmlDocument appts_xml_doc = new XmlDocument();
        XmlElement appts_ele = appts_xml_doc.CreateElement("appointments");

        foreach (GridViewRow row_appt in this.grdScheduledAppt.Rows)
        {
            CheckBox chk_appt = (CheckBox)row_appt.FindControl("chkScheduledAppt");
            Label lbl_is_blocked = (Label)row_appt.FindControl("lblIsBlocked");
            if (chk_appt.Checked && (action_type == 1 ? (lbl_is_blocked.Text == "1" || lbl_is_blocked.Text == "2") : lbl_is_blocked.Text == action_type.ToString()))
            {
                XmlElement appt_ele = appts_xml_doc.CreateElement("appointment");
                appt_ele.SetAttribute("clinicPk", this.grdScheduledAppt.DataKeys[row_appt.RowIndex]["clinicPk"].ToString());
                appt_ele.SetAttribute("apptPk", this.grdScheduledAppt.DataKeys[row_appt.RowIndex]["apptPk"].ToString());
                appt_ele.SetAttribute("apptTimePk", this.grdScheduledAppt.DataKeys[row_appt.RowIndex]["apptTimePk"].ToString());

                appts_ele.AppendChild(appt_ele);
            }
        }
        appts_xml_doc.AppendChild(appts_ele);

        return appts_xml_doc;
    }

    /// <summary>
    /// Sends rescheduled appointment email to blocked appointments
    /// </summary>
    private void sendBlockedAppointmentRescheduleEmail(IXPathNavigable scheduled_appts_xml)
    {
        Int32.TryParse(this.hfBusinessClinicPk.Value, out this.clinicPk);
        if (this.walgreensEmail != null && Session["scheduledAppts" + this.clinicPk.ToString()] != null)
        {
            string email_body, subject_line, email_path, email_to, client_logo = string.Empty, banner_bg_color = string.Empty;
            int clinic_design_pk = 0, has_clinics = 0;
            EncryptQueryStringAES args = new EncryptQueryStringAES();

            this.corporateScheduler = (CorporateScheduler)Session["scheduledAppts" + this.clinicPk.ToString()];
            this.dtScheduledAppts = this.corporateScheduler.scheduledAppointments;

            XmlDocument clinic_scheduler_design_xml = new XmlDocument();
            Int32.TryParse(this.hfDesignPk.Value, out clinic_design_pk);
            clinic_scheduler_design_xml.LoadXml(this.dbOperation.getCorporateSchedulerDesign(clinic_design_pk, out has_clinics));
            client_logo = clinic_scheduler_design_xml.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value;
            banner_bg_color = clinic_scheduler_design_xml.SelectSingleNode(".//logoAndStyles").Attributes["bannerColor"].Value;

            foreach (XmlNode appt_node in ((XmlDocument)scheduled_appts_xml).SelectNodes(".//appointment"))
            {
                DataRow dr_appt = this.dtScheduledAppts.Select("clinicPk = " + appt_node.Attributes["clinicPk"].Value + " And apptPk = " + appt_node.Attributes["apptPk"].Value).ElementAt(0);

                email_to = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dr_appt["apptEeEmail"].ToString());
                email_path = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();
                args["arg1"] = clinic_design_pk.ToString();
                args["arg2"] = appt_node.Attributes["apptPk"].Value;
                args["arg3"] = "walgreensSchedulerRegistration.aspx";
                args["arg4"] = appt_node.Attributes["clinicPk"].Value;

                subject_line = (string)GetGlobalResourceObject("errorMessages", "rescheduleApptSubjectLine");

                email_body = "<table width='40%' border='0' cellspacing='0' cellpadding='4'><tr><td width='15%' align='right' valign='top' nowrap='nowrap' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>Time:</td>";
                email_body += "<td width='85%' align='left' valign='top' nowrap='nowrap' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: red; font-weight:bold'>" + dr_appt["apptDate"].ToString() + " " + dr_appt["apptAvlTimes"].ToString() + "</td></tr>";
                email_body += "<tr><td align='right' valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;'>Location:</td>";
                email_body += "<td align='left' valign='top' nowrap='nowrap' style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; color: red; font-weight:bold'>" + dr_appt["clinicAddress"].ToString() + "</td></tr></table>";

                this.walgreensEmail.sendBlockedAppointmentRescheduleEmail(email_path + "corporateScheduler/walgreensSchedulerLanding.aspx?args=" + args.ToString(), Server.MapPath("~/emailTemplates/rescheduleAppointmentEmail.html"), subject_line, email_body, email_to, "", true, email_path + ApplicationSettings.EmailLogoImagePath() + "/" + client_logo, banner_bg_color);
            }
        }
    }

    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }
    /// <summary>
    /// Set minimum and maximum dates for date controls.
    /// </summary>
    private void setMinMaxDates()
    {
        if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
            this.minClinicDate = DateTime.Now.AddDays(14);
        else
            this.minClinicDate = DateTime.Now.AddDays(-1);
    }
    #endregion

    #region ----------------- PUBLIC FUNCTIONS -----------------
    [WebMethod]
    public static void setSelectedTabId(string home_tab_id)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        common_app_session.SelectedStoreSession.SelectedCorporateClinicsTabId = home_tab_id;
    }

    [WebMethod]
    public static void logJSerrors(string log_details)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        Logger logger = LogManager.GetLogger("ClinicDetailsLogger");
        logger.Info("Clinic Details Update - Corporate ; Username::" + common_app_session.LoginUserInfoSession.UserName + "; " + log_details);
    }
    #endregion

    #region ----------------- PRIVATE VARIABLES ----------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private WalgreenEmail walgreensEmail = null;
    private ApplicationSettings appSetting = null;
    private DataTable dtScheduledAppts;
    private CorporateScheduler corporateScheduler;
    private int clinicPk;
    private bool showAlertMessage = false;
    public DateTime minClinicDate = new DateTime();
    //Logger Logger = LogManager.GetCurrentClassLogger();
    private bool isOverlapDBAuthorized
    {
        get
        {
            return ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "AccessOverlapDBForCorporateClinics");
        }
    }
    #endregion

    #region ------- Web Form Designer generated code -------
    protected void Page_Init(object sender, EventArgs e)
    {

        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        this.appSetting = new ApplicationSettings();
        this.dtScheduledAppts = new DataTable();
        this.corporateScheduler = new CorporateScheduler();
        this.clinicPk = 0;
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelected != null)
            this.minClinicDate = ApplicationSettings.getOutreachStartDate;
        else
            Response.Redirect("auth/sessionTimeout.aspx");
    }
    #endregion
}