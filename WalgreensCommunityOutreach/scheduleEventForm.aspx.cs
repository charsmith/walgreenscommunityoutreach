﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using TdWalgreens;
using System.Collections.Generic;

public partial class scheduleEventForm : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //this.dtScheduledEvent.getSelectedDate = DateTime.Today.Date;
            this.doClearFields();
            this.bindEventDefaultFields();
            this.getScheduleEvent();
        }
        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        //if (!Page.IsValid) return;
        //if (string.IsNullOrEmpty(this.txtEventDate.Text))
        //{
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert(\"Date of Event is required\");", true);
        //    return;
        //}

        int return_value = 0;
        return_value = this.dbOperation.createScheduleEvent(this.prepareStoreProfileXml());

        if (return_value != -2)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "eventScheduled") + "'); window.location.href = '" + this.commonAppSession.SelectedStoreSession.referrerPath + "';", true);
        }
    }
    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.referrerPath))
        {
            Response.Redirect(this.commonAppSession.SelectedStoreSession.referrerPath);
        }
        else
            Response.Redirect("walgreensHome.aspx");
    }

    #region --------- PRIVATE METHODS --------------
    /// <summary>
    /// Binds event scheduled default fields
    /// </summary>
    private void bindEventDefaultFields()
    {
        DataTable dt_event_fields = this.dbOperation.getEventScheduledFields;
        DataView dv_event_types = dt_event_fields.DefaultView;
        dv_event_types.RowFilter = "columnName = 'EventType'";

        DataTable dt_event_types = dv_event_types.Table.Clone();
        foreach (DataRowView drowview in dv_event_types)
            dt_event_types.ImportRow(drowview.Row);

        this.rbtnEventType.DataTextField = "description";
        this.rbtnEventType.DataValueField = "fieldValue";
        this.rbtnEventType.DataSource = dt_event_types;
        this.rbtnEventType.DataBind();

        ListItem lst_other = this.rbtnEventType.Items.FindByText("Other");
        if (lst_other != null)
            lst_other.Text = "Other &nbsp;<span id='txtOthers'></span>";

        DataView dv_walgreen_role = dt_event_fields.DefaultView;
        dv_walgreen_role.RowFilter = "columnName = 'WalgreensRole'";

        DataTable dt_walgreen_role = dv_walgreen_role.Table.Clone();
        foreach (DataRowView drowview in dv_walgreen_role)
            dt_walgreen_role.ImportRow(drowview.Row);

        this.rbtnWalgreenRole.DataTextField = "description";
        this.rbtnWalgreenRole.DataValueField = "fieldValue";
        this.rbtnWalgreenRole.DataSource = dt_walgreen_role;
        this.rbtnWalgreenRole.DataBind();

        lst_other = this.rbtnWalgreenRole.Items.FindByText("Other");
        if (lst_other != null)
            lst_other.Text = "Other &nbsp;<span id='txtRoleOthers'></span>";

        DataView dv_funding = dt_event_fields.DefaultView;
        dv_funding.RowFilter = "columnName = 'FundingOfGiveaways'";

        DataTable dt_funding = dv_funding.Table.Clone();
        foreach (DataRowView drowview in dv_funding)
            dt_funding.ImportRow(drowview.Row);

        this.ddlFundingOfGiveaways.DataTextField = "description";
        this.ddlFundingOfGiveaways.DataValueField = "fieldValue";
        this.ddlFundingOfGiveaways.DataSource = dt_funding;
        this.ddlFundingOfGiveaways.DataBind();
        this.ddlFundingOfGiveaways.Items.Insert(0, new ListItem("Select if Applicable", "0"));
        this.ddlFundingOfGiveaways.ClearSelection();
    }

    /// <summary>
    /// This function will be used to get schedule event
    /// </summary>
    private void getScheduleEvent()
    {
        bool is_previous_season_event = false;
        DataTable schedule_event;
        bool with_new_template = true;

        schedule_event = this.dbOperation.getScheduleEvent(this.commonAppSession.SelectedStoreSession.SelectedContactLogPk, out is_previous_season_event);
        this.setNewTemplate(schedule_event);
        
        if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk != 0 && contacted_date < ApplicationSettings.newSOTemplateDate)
        {
            with_new_template = false;
        }

        if (schedule_event.Rows.Count > 0)
        {
            //this.dtScheduledEvent.getSelectedDate = ((schedule_event.Rows[0]["dateOfEvent"] is DBNull) ? DateTime.Today : Convert.ToDateTime(schedule_event.Rows[0]["dateOfEvent"].ToString()));
            this.txtEventDate.Text = schedule_event.Rows[0]["dateOfEvent"].ToString();
            this.hfEventDate.Value = schedule_event.Rows[0]["dateOfEvent"].ToString();
            this.txtEventStartTime.Text = (schedule_event.Rows[0]["eventStartTime"] is DBNull) ? "" : schedule_event.Rows[0]["eventStartTime"].ToString();
            this.txtEventEndTime.Text = (schedule_event.Rows[0]["eventEndTime"] is DBNull) ? "" : schedule_event.Rows[0]["eventEndTime"].ToString();

            if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["eventTypeId"].ToString())))
                this.rbtnEventType.Items.FindByValue(schedule_event.Rows[0]["eventTypeId"].ToString()).Selected = true;

            if (!with_new_template)
            {
                if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["refreshments"].ToString())))
                {
                    if (Convert.ToBoolean(schedule_event.Rows[0]["refreshments"].ToString())) this.rbtnRefreshments1.Checked = true; else this.rbtnRefreshments2.Checked = true;
                }
            }
            else
            {
                if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["provideEducationalMaterial"].ToString())))
                {
                    if (Convert.ToBoolean(schedule_event.Rows[0]["provideEducationalMaterial"].ToString())) this.rbtnRefreshments1.Checked = true; else this.rbtnRefreshments2.Checked = true;
                }
            }
            if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["eventDescription"].ToString())))
            {
                this.txtOther.Text = Server.HtmlDecode(schedule_event.Rows[0]["eventDescription"].ToString());
            }
            if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["walgreensRoleId"].ToString())) && (schedule_event.Rows[0]["walgreensRoleId"].ToString() != "0"))
                this.rbtnWalgreenRole.Items.FindByValue(schedule_event.Rows[0]["walgreensRoleId"].ToString()).Selected = true;
            this.txtRoleOther.Text = Server.HtmlDecode(((schedule_event.Rows[0]["walgreensRoleDescription"] is DBNull) ? "" : schedule_event.Rows[0]["walgreensRoleDescription"].ToString()));

            this.txtNoOfAttendees.Text = Server.HtmlDecode(((schedule_event.Rows[0]["numberOfAttendes"] is DBNull) ? "" : schedule_event.Rows[0]["numberOfAttendes"].ToString()));
            this.ddlFundingOfGiveaways.ClearSelection();
            if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["fundingId"].ToString())))
                this.ddlFundingOfGiveaways.Items.FindByValue(schedule_event.Rows[0]["fundingId"].ToString()).Selected = true;

            this.txtDescriptionOfGiveaway.Text = Server.HtmlDecode(((schedule_event.Rows[0]["description"] is DBNull) ? "" : schedule_event.Rows[0]["description"].ToString()));
            this.txtTotalCost.Text = Server.HtmlDecode(((schedule_event.Rows[0]["totalCost"] is DBNull) ? "" : schedule_event.Rows[0]["totalCost"].ToString()));

            this.btnCancel.Visible = true;
        }
        else if (Session["eventContactLog_" + this.commonAppSession.SelectedStoreSession.storeID + "_" + this.commonAppSession.LoginUserInfoSession.UserID.ToString()] != null)
        {
            this.eventContactLog = (Dictionary<string, string>)Session["eventContactLog_" + this.commonAppSession.SelectedStoreSession.storeID + "_" + this.commonAppSession.LoginUserInfoSession.UserID.ToString()];
            ViewState["eventContactLog"] = this.eventContactLog;
            Session.Remove("eventContactLog_" + this.commonAppSession.SelectedStoreSession.storeID + "_" + this.commonAppSession.LoginUserInfoSession.UserID.ToString());
        }

        if (is_previous_season_event)
        {
            this.disableControls(false);
            this.btnCancel.Visible = true;
            this.btnSubmit.Visible = false;
        }
    }


    private void setNewTemplate(DataTable schedule_event)
    {
        if (schedule_event.Rows.Count > 0)
        {
            this.contacted_date = Convert.ToDateTime(schedule_event.Rows[0]["contactDate"].ToString());
            if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk != 0 && contacted_date >= ApplicationSettings.newSOTemplateDate)
            {
                this.setNewTemplateControls();
                if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["provideEducationalMaterial"].ToString())))
                {
                    if (Convert.ToBoolean(schedule_event.Rows[0]["provideEducationalMaterial"].ToString())) this.rbtnRefreshments1.Checked = true; else this.rbtnRefreshments2.Checked = true;
                }
            }
        }
        else
        {
            this.setNewTemplateControls();
        }
    }
    private void setNewTemplateControls()
    {
        lblRefreshments.Text = "Provide Educational Material";
        trFunding.Visible = false;
        lblDescription.Text = "Description of Educational Materials";
        trTotalCost.Visible = false;
    }
    /// <summary>
    /// Disables all input controls in the page
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableControls(bool disable)
    {
        foreach (Control ctrl in from Control c in Page.Controls from Control ctrl in c.Controls select ctrl)
        {
            if (ctrl is TextBox)
                ((TextBox)ctrl).Enabled = disable;

            if (ctrl is RadioButton)
                ((RadioButton)ctrl).Enabled = disable;

            if (ctrl is RadioButtonList)
                ((RadioButtonList)ctrl).Enabled = disable;

            if (ctrl is DropDownList)
                ((DropDownList)ctrl).Enabled = disable;

            if (ctrl is PickerAndCalendar)
                ((PickerAndCalendar)ctrl).Visible = false;
        }
    }

    /// <summary>
    /// Clear all fields
    /// </summary>
    private void doClearFields()
    {
        foreach (Control ctrl in from Control c in Page.Controls from Control ctrl in c.Controls select ctrl)
            if (ctrl is TextBox)
                ((TextBox)ctrl).Text = "";

            else if (ctrl is DropDownList)
                ((DropDownList)ctrl).SelectedIndex = 0;

            else if (ctrl is CheckBox)
                ((CheckBox)ctrl).Checked = false;

            else if (ctrl is CheckBoxList)
                ((CheckBoxList)ctrl).ClearSelection();
    }

    /// <summary>
    /// This function returns the xml structure to DB for creating the Store Profile
    /// </summary>
    /// <returns></returns>
    private XmlDocument prepareStoreProfileXml()
    {
        bool with_new_template = true;
        if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk != 0 && contacted_date < ApplicationSettings.newSOTemplateDate)
        {
            with_new_template = false;
        }
        XmlDocument profile_doc = new XmlDocument();
        XmlElement store_profile_element = store_profile_element = profile_doc.CreateElement("storeprofile");
        XmlElement fields_element = profile_doc.CreateElement("fields");

        XmlElement create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "contactLogPk");
        create_store.SetAttribute("value", this.commonAppSession.SelectedStoreSession.SelectedContactLogPk.ToString());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "dateOfEvent");
        create_store.SetAttribute("value", (Convert.ToDateTime(this.hfEventDate.Value)).ToString());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "eventStartTime");
        create_store.SetAttribute("value", this.txtEventStartTime.Text.ToString());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "eventEndTime");
        create_store.SetAttribute("value", this.txtEventEndTime.Text.ToString());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "eventTypeId");
        create_store.SetAttribute("value", this.rbtnEventType.SelectedValue);
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "eventDescription");
        create_store.SetAttribute("value", (!string.IsNullOrEmpty(this.rbtnEventType.SelectedValue) && this.rbtnEventType.SelectedValue.ToLower() == "8") ? this.txtOther.Text.Trim().Replace("'", "''") : null);
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "walgreensRoleId");
        create_store.SetAttribute("value", this.rbtnWalgreenRole.SelectedValue);
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "numberOfAttendes");
        create_store.SetAttribute("value", this.txtNoOfAttendees.Text.Trim().Replace("'", "''"));
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "refreshments");
        create_store.SetAttribute("value", with_new_template ? "" : ((this.rbtnRefreshments1.Checked) ? "1" : (this.rbtnRefreshments2.Checked) ? "0" : ""));
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "provideEducationalMaterial");
        create_store.SetAttribute("value", with_new_template ? ((this.rbtnRefreshments1.Checked) ? "1" : (this.rbtnRefreshments2.Checked) ? "0" : "") : "");
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "fundingId");
        create_store.SetAttribute("value", this.ddlFundingOfGiveaways.SelectedItem.Value);
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "description");
        create_store.SetAttribute("value", this.txtDescriptionOfGiveaway.Text.Trim().Replace("'", "''"));
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "totalCost");
        create_store.SetAttribute("value", this.txtTotalCost.Text.Trim());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "walgreensRoleDescription");
        create_store.SetAttribute("value", (!string.IsNullOrEmpty(this.rbtnWalgreenRole.SelectedValue) && this.rbtnWalgreenRole.SelectedValue.ToLower() == "3") ? this.txtRoleOther.Text.Trim().Replace("'", "''") : null);
        fields_element.AppendChild(create_store);

        if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk == 0 && ViewState["eventContactLog"] != null)
        {
            this.eventContactLog = (Dictionary<string, string>)ViewState["eventContactLog"];
            create_store = profile_doc.CreateElement("field");
            create_store.SetAttribute("name", "storeId");
            create_store.SetAttribute("value", this.eventContactLog["storeId"]);
            fields_element.AppendChild(create_store);

            create_store = profile_doc.CreateElement("field");
            create_store.SetAttribute("name", "businessPk");
            create_store.SetAttribute("value", this.eventContactLog["businessPk"]);
            fields_element.AppendChild(create_store);

            create_store = profile_doc.CreateElement("field");
            create_store.SetAttribute("name", "firstName");
            create_store.SetAttribute("value", this.eventContactLog["firstName"]);
            fields_element.AppendChild(create_store);

            create_store = profile_doc.CreateElement("field");
            create_store.SetAttribute("name", "lastName");
            create_store.SetAttribute("value", this.eventContactLog["lastName"]);
            fields_element.AppendChild(create_store);

            create_store = profile_doc.CreateElement("field");
            create_store.SetAttribute("name", "contactDate");
            create_store.SetAttribute("value", this.eventContactLog["contactDate"]);
            fields_element.AppendChild(create_store);

            create_store = profile_doc.CreateElement("field");
            create_store.SetAttribute("name", "feedback");
            create_store.SetAttribute("value", this.eventContactLog["feedback"]);
            fields_element.AppendChild(create_store);

            create_store = profile_doc.CreateElement("field");
            create_store.SetAttribute("name", "createdBy");
            create_store.SetAttribute("value", this.eventContactLog["createdBy"]);
            fields_element.AppendChild(create_store);
        }

        store_profile_element.AppendChild(fields_element);
        profile_doc.AppendChild(store_profile_element);

        return profile_doc;
    }

    #endregion

    #region ----------- PRIVATE VARIABLES ----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private Dictionary<string, string> eventContactLog = null;
    private DateTime contacted_date
    {
        get
        {
            DateTime result = new DateTime();
            if (ViewState["ContactDate"] != null)
            {
                result = Convert.ToDateTime(ViewState["ContactDate"]);
            }
            return result;
        }
        set
        {
            ViewState["ContactDate"] = value;
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.eventContactLog = new Dictionary<string, string>();
    }
}