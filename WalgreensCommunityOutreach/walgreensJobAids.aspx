﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensJobAids.aspx.cs"
    Inherits="walgreensJobAids" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#popupDialog").hide();
        });

        function showPdf(file_type, width, height) {
            if (confirm('The Immunizations Sales Sheet was sent in Sign Pack 25 and will be in stores on 6/18/14.')) {
                window.open('controls/ResourceFileHandler.ashx?Path=2014_15/14WG0020_SellSheet_English.pdf', '_blank');
            }
        }

    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td colspan="2" valign="top" class="pageTitle">
                            Resources for Small Business Outreach Efforts: Job Aids & Marketing Materials
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="598">
                            <table width="100%" border="0" cellspacing="12">
                                <tr>
                                  <td colspan="2" align="left" valign="top" class="bestPracticesTopic">Training Videos</td>
								</tr>
								<!--<tr>
                                  <td colspan="2" align="left" valign="top" class="bestPracticesText">
								  <span style="color:red;">
                                        <b>NOTE: If you are using Internet Explorer 11, there is currently an issue playing some of the Training Videos related to recent browser updates. We are working to resolve this issue, thank you for your patience.</b>
                                     </span></td>
                                </tr>-->
                                <tr>
                                    <td align="left" valign="top" width="50%" style="text-align:left" > <img id="v2" onclick="window.open('walgreensVideoPlayer.html?v=v2')" class="videothumb" 
                                          src="images/contractingMultiplePaymentMethods_thumb.png" title="Contracting Multiple Payment Methods for the Same Immunization" style="cursor:pointer;">
                                        <br />
                                      <span class="bestPracticesText">
                                        Contracting Multiple Payment Methods<br/>for the Same Immunization
                                     </span></td>
                                    <td colspan="2" valign="top" width="50%" style="text-align:left" >
                                      <img id="v1" onclick="window.open('walgreensVideoPlayer.html?v=v3')" class="videothumb" 
                                          src="images/charityProgram_thumb.png" title="Creating a Charity (HHS Voucher) Clinic" style="cursor:pointer;">
                                        <br />
                                      <span class="bestPracticesText">
                                        Creating a Charity (HHS Voucher) Clinic
                                     </span>                                      
                                  </td>
                                </tr>
                                <tr>
									<td valign="top" width="50%" style="text-align:left" >
                                      <img id="v5" onclick="window.open('walgreensVideoPlayer.html?v=v5')" class="videothumb" 
                                          src="images/contractDirectBillVoucher.png" title="Contract Direct Bill Voucher" style="cursor:pointer;">
                                        <br />
                                      <span class="bestPracticesText">
                                        Contract Direct Bill Voucher
                                     </span>                                      
									</td>
                                    <td valign="top" width="50%" style="text-align:left" >
                                      <img id="v4" onclick="window.open('walgreensVideoPlayer.html?v=v4')" class="videothumb" 
                                          src="images/reviewAndConfirmClinicDetails.png" title="Review & Confirm Clinic Details" style="cursor:pointer;">
                                        <br />
                                      <span class="bestPracticesText">
                                        Review & Confirm Clinic Details
                                     </span>                                      
                                  </td>
                                </tr>
								<tr>
								<td valign="top" width="50%" style="text-align:left" >
                                      <img id="v1" onclick="window.open('walgreensVideoPlayer.html?v=v1')" class="videothumb" 
                                          src="images/completingClinicVideo_thumb.png" title="Completing a Clinic" style="cursor:pointer;">
                                        <br />
                                      <span class="bestPracticesText">
                                        Completing a Clinic
                                     </span>                                      
                                  </td>
                                    <td valign="top" width="50%" style="text-align:left" >
                                      <img id="v1" onclick="window.open('walgreensVideoPlayer.html?v=v6')" class="videothumb" 
                                          src="images/addClinicLocations_thumb.png" title="Adding Clinic Locations to an Existing Contract" style="cursor:pointer;">
                                        <br />
                                      <span class="bestPracticesText">
                                        Adding Clinic Locations to an Existing Contract
                                     </span>                                      
                                  </td>
								</tr>
								<tr>
                                    <td width="715" colspan="2" align="left" valign="top" style="padding-top:24px;" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Prepare</span>
                                        <ul>
                                            <li>Review the content of <a href="http://www.walgreens.com/pharmacy/immunization/seasonal_flu.jsp" target="_blank">Walgreens.com/pharmacy/immunization/seasonal_flu.jsp</a>,
                                                in detail.</li>
                                            <li>Print copies of the <a href="controls/ResourceFileHandler.ashx?Path=2017_18/ImmunizationsSalesSheet.pdf" target="_blank">Immunization Services sell sheet.</a> to bring to potential clients.</li>
                                        </ul>
                                    </td>
                                </tr>
								<tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Job Aids and Training Videos</span>
                                        <ul>
                                            <li>Additional collateral material is available to print on StoreNet and the Opportunity Directory.</li>
                                            <li>Review the list of corporately contracted worksites to prevent duplication of efforts.</li>
                                            <li>Work with your district manager to keep track of businesses that have been contacted by store managers in your community to decrease duplicate efforts and avoid confusion.</li>
                                        </ul>
                                    </td>
                                </tr>
								<tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Billing/Invoicing</span>
                                        <ul>
                                            <li>If a check is received from the client please forward it to PO Box 90478Chicago, IL 60696-0478, including the client and group number information to ensure everything is matched appropriately.</li>
                                            <li>For any questions related to Invoicing issues please email the accounting team at <a href="mailto:DirectBillteam@walgreens.com" >DirectBillteam@walgreens.com</a></li>
                                        </ul>
                                    </td>
                                </tr>
								<tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Provide Updates</span>
                                        <ul>
                                            <li>Be proactive and provide updates as you go. District and Region leadership will
                                                be checking the status on your outreach efforts.</li>
                                            <li>Consider splitting your business list between store and pharmacy management. Work
                                                together on completing the initial outreach efforts and share ‘Job Aids’ on
                                                what approach worked best.</li>
                                            <li>Schedule weekly meetings to review past week’s status and develop a completion plan
                                                to reach out to remaining businesses.</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText">
                                        <span class="bestPracticesTopic">Practice (See sample call script below) </span>
                                        <ul>
                                            <li>In opening a phone conversation, identify your name, title and store location. Immediately
                                                identify your purpose for calling, explaining the Walgreens Immunization program
                                                and how their business can benefit from Flu as well as other immunization services.</li>
                                            <li>Ask for a few minutes of their very busy time and be prepared to stress the important
                                                messages in the <a href="controls/ResourceFileHandler.ashx?Path=2017_18/ImmunizationsSalesSheet.pdf" target="_blank">Immunization Services sell sheet</a> as to why Walgreens is the ideal
                                                provider for a small business such as theirs.</li>
                                            <li>Ask if they‘ve ever explored options to provide an onsite benefit for their employees.
                                                Refer them to <a href="http://www.walgreens.com/pharmacy/immunization/seasonal_flu.jsp" target="_blank">Walgreens.com/pharmacy/immunization/seasonal_flu.jsp</a> to understand the benefit of immunizing their employees.</li>
                                            <li>Let them know we can bill an existing employee benefit, or set-up an employer-funded/Direct Bill  program.</li>
                                            <li>Attempt to schedule a follow-up appointment to review the materials and program options.</li>
                                            <li>If a formal meeting is not a possible, try to schedule a future follow-up phone
                                                call and mail the <a href="controls/ResourceFileHandler.ashx?Path=2017_18/ImmunizationsSalesSheet.pdf" target="_blank">Immunization Services sell sheet</a> to the business.</li>
                                        </ul>
                                    </td>
                                </tr>
                                
                                
                                
                                <tr>
                                  <td colspan="2" align="left" valign="top" class="bestPracticesText"><a name="callScript" id="callScript"></a>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                                    <tr>
                                      <td><table width="100%" border="0" cellspacing="20">
                                        <tr>
                                          <td width="715" align="left" valign="top" class="bestPracticesSubTitle"> Sample Call Script </td>
                                        </tr>
                                        <tr>
                                          <td width="715" colspan="2" align="left" valign="top" class="bestPracticesText"><span class="bestPracticesTopic">Initial contact with business:</span>
                                            <ul>
                                              <li>Hi, my name is _____and I’m a Walgreens Store Manager/Pharmacy
                                                Manager at ___ (intersection of your store).</li>
                                            </ul>
                                            <ul>
                                              <li>Can I please speak to ______ (contact name provided in business directory).</li>
                                            </ul>
                                            <ul>
                                              <li>I’d like to tell you about our flu shot clinic program and other available immunizations
                                                for small businesses. </li>
                                            </ul>
                                            <ul>
                                              <li>Ask for a few minutes of their very busy time and be prepared to stress four important
                                                reasons why Walgreens is the ideal flu shot provider for a small business:
                                                <ul style="list-style-type: circle;">
                                                  <li>We offer a flu shot service, which can be coupled with other immunization services.</li>
                                                  <li>With Walgreens immunization solutions, you’ll have convenient access to preventive
                                                    care for employees, spouses and dependents.</li>
                                                  <li>Walgreens may be able to process immunizations as a pharmacy benefit or a medical
                                                    benefit.</li>
                                                  <li>Online technologies allow you to easily schedule on-site health center events or
                                                    create custom marketing materials, such as posters and flyers, to promote this new
                                                    benefit.</li>
                                                </ul>
                                              </li>
                                            </ul>
                                            <ul>
                                              <li>If they have time to talk, continue with the conversation below. If they do not
                                                have time, attempt to schedule a follow up appointment to review the materials and
                                                program options. If a formal meeting is not possible, try to schedule a future follow-up
                                                call.</li>
                                            </ul></td>
                                        </tr>
                                        <tr>
                                          <td colspan="2" align="left" valign="top" class="bestPracticesText"><p> <span class="bestPracticesTopic">If the employer agrees to conversation, continue through
                                            remaining questions below</span>.</p>
                                            <p> Let them know we can bill an existing employee benefit, or set-up an employer-funded
                                              program. </p>
                                            <ul>
                                              <li><strong>Insurance Billing:</strong> Walgreens can bill patient’s insurance directly
                                                and the patient would be responsible for the copay. Walgreens can bill many pharmacy
                                                and medical plans.</li>
                                            </ul>
                                            <ul>
                                              <li><strong>Cash Payment:</strong> Payment for the vaccine can be collected from the
                                                patient receiving the immunization at the time of vaccination by cash or via check
                                                from the employer/location.</li>
                                            </ul>
                                            <ul>
                                              <li><strong>Direct Bill:</strong> Employer/Location will pay for employee flu shots.
                                                They will receive an invoice directly from Walgreens for the number of immunizations
                                                provided. No payment will be collected at the time of the immunization.</li>
                                            </ul></td>
                                        </tr>
                                        <tr>
                                          <td colspan="2" align="left" valign="top" class="bestPracticesText"><span class="bestPracticesTopic">Ask them if they are interested in proceeding with
                                            Walgreens Immunizations Clinic.</span>
                                            <ul>
                                              <li>If yes, explain the process of completing the community off-site agreement and next
                                                steps.</li>
                                            </ul>
                                            <ul>
                                              <li>If no, refer them to <a href="http://www.walgreens.com/pharmacy/immunization/seasonal_flu.jsp" target="_blank">Walgreens.com/pharmacy/immunization/seasonal_flu.jsp</a> to try our new flu ROI estimator tool and
                                                review the FAQs on the site explaining Walgreens program.</li>
                                            </ul></td>
                                        </tr>
                                        <tr>
                                          <td colspan="2" align="left" valign="top" class="bestPracticesText"><span class="bestPracticesTopic">Provide your contact information for future follow-up
                                            or questions and thank them for their time.</span></td>
                                        </tr>
                                      </table></td>
                                    </tr>
                                  </table></td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-top: 12px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="12">
                                            <tr>
                                                <td width="715" align="left" valign="top" class="resourcesTitle">
                                                    Marketing Materials
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="715" colspan="2" align="left" valign="top" class="class3"> 
													<a href="controls/ResourceFileHandler.ashx?Path=2017_18/ImmunizationsSalesSheet.pdf" target="_blank">Immunization Sell Sheet (pdf)</a>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/FY18_Flyer_ENG_BW.pdf" target="_blank">Flu Shot Flyer (B/W) - English (pdf)</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/FY18_Flyer_Spanish_BW.pdf" target="_blank">Flu Shot Flyer (B/W) - Spanish (pdf)</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/FY18_Flyer_DuaneReade_BW.pdf" target="_blank">Flu Shot Flyer (B/W) - Duane Reade (pdf)</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/FY18_Flyer_ENG_COLOR.pdf" target="_blank">Flu Shot Flyer (Color) - English (pdf)</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/FY18_Flyer_Spanish_COLOR.pdf" target="_blank">Flu Shot Flyer (Color) - Spanish (pdf)</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/FY18_Flyer_DuaneReade_COLOR.pdf" target="_blank">Flu Shot Flyer (Color) - Duane Reade (pdf)</a></p>
                                                    <p><a href="#callScript">Sample Call Script</a></p>
                                                </td>
                                            </tr>
											 <tr>
                                                <td colspan="2" align="left" valign="top" class="resourcesTitle">Training Videos</td>
                                             </tr>
                                              <tr align="center">
                                                <td width="715" colspan="2" align="left" valign="top" class="class3">
                                                    <a href="controls/ResourceFileHandler.ashx?Path=videos/ContractingMultiplePaymentMethods.mp4" target="_blank">Contracting Multiple Payment Methods<br/>for the Same Immunization</a>
													<p><a href="controls/ResourceFileHandler.ashx?Path=videos/CharityClinic.mp4" target="_blank">Creating a Charity Program Clinic</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=videos/directBillVouchers.mp4" target="_blank">Contract Direct Bill Voucher</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=videos/reviewAndConfirmClinicDetails.mp4" target="_blank">Review & Confirm Clinic Details</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=videos/CompletingClinic.mp4" target="_blank">Completing a Clinic</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=videos/AddClinicLocationsToAnApprovedContract.mp4" target="_blank">Adding Clinic Locations to an Existing Contract</a></p>
                                                </td>
                                              </tr>
											<tr>
                                                <td colspan="2" align="left" valign="top" class="resourcesTitle">Beneficial Resources</td>
                                            </tr>
											<tr align="center">
                                                <td width="715" colspan="2" align="left" valign="top" class="class3">
                                                    <a href="controls/ResourceFileHandler.ashx?Path=2017_18/W9form2017.pdf" target="_blank">Walgreens W9 (pdf)</a>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2015_16/Pharmacy Offsite Checklist.pdf" target="_blank">Pharmacy Offsite Checklist (pdf)</a></p>
												</td>
                                              </tr>											
                                            <tr>
                                                <td width="715" align="left" valign="top" class="resourcesTitle">Job Aids</td>
                                            </tr>
                                            <tr>
                                                <td width="715" colspan="2" align="left" valign="top" class="class3">
                                                    <a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Adding a New Business.pdf" target="_blank">Adding a New Business (pdf)</a>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Adding Multiple Locations and Assigning Based on Location.pdf" target="_blank">Adding Multiple Locations and Assigning Based on Location (pdf)</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Completing a Clinic.pdf" target="_blank">Completing a Clinic (pdf)</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Confirming an Assigned Corporate Clinic.pdf" target="_blank">Confirming an Assigned Corporate Clinic (pdf)</a></p>
													<p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Creating a Community Outreach Event Clinic.pdf" target="_blank">Creating a Community Outreach Event Clinic (pdf)</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/HowToEditAnInitiatedContract.pdf" target="_blank">How to Edit an Initiated Contract (pdf)</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Initiating a Contract.pdf" target="_blank">Initiating a Contract (pdf)</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Reassigning a Potential Local Business.pdf" target="_blank">Reassigning a Potential Local Business (pdf)</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Recording Charity Voucher Distribution and Clinics.pdf" target="_blank">Recording Charity Voucher Distribution</br>and Clinics (pdf)</a></p>
                                                    <p><a href="controls/ResourceFileHandler.ashx?Path=2017_18/jobAids/Requesting a Direct Bill Voucher for a Client.pdf" target="_blank">Requesting a Direct Bill Voucher for a Client (pdf)</a></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>