﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensFollowupEmail.aspx.cs" Inherits="walgreensFollowupEmail" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register src="controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>

     <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/theme.css"  />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css"  />
     <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
     <link href="../css/theme.css" rel="stylesheet" type="text/css" />
     <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />  
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/ui.jqgrid.css" />
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tblFollowUp").focus();
        });
    </script>
     
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" >
<asp:HiddenField ID="hfContactLogPk" runat="server" />
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
<tr>
    <td colspan="2">
        <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF" align="center">
     <table width="935" border="0" cellspacing="22" cellpadding="0">
                    
                    <tr>
                      <td valign="middle"  height="300px" align="center">
                      <div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error : " ShowMessageBox="true" ShowSummary="false" />

        <table width="500" border="0" cellspacing="0" cellpadding="0" >
          <tr>
              <td class="backgroundGradientLog" align="left"><table width="500" border="0" cellspacing="8" cellpadding="0" id="tblFollowUp">
                <tr>
                  <td colspan="2" class="logTitle">Schedule A<asp:Label ID="lblOutreachEffort" runat="server"></asp:Label>Follow-up Email Reminder</td>
                </tr>
                <tr>
                  <td width="100" nowrap="nowrap"><span class="profileTitles">Email To:</span></td>
                  <td width="376" class="logSubTitles">
                    <asp:TextBox ID="txtEmails" runat="server" Width="200px" CssClass="formFields" ></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="txtEmails" ID="txtEmailsReqFV" runat="server" Display="None" ErrorMessage="Email is required" ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ControlToValidate="txtEmails" ID="txtEmailsRegEV" runat="server" Display="None" ErrorMessage="Invalid email" ValidationExpression="^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$">
                    </asp:RegularExpressionValidator>
                  </td>
                </tr>
                <tr>
                  <td width="100" nowrap="nowrap"><span class="profileTitles">Reminder Date:</span></td>
                  <td width="376" class="logSubTitles">
                  <uc1:PickerAndCalendar ID="dtFollowUpEmail" runat="server" />
                  </td>
                </tr>
                <tr>
                  <td valign="top" nowrap="nowrap" class="profileTitles">&nbsp;</td>
                  <td>
                    <asp:CheckBox ID="chkEmailReminder" runat ="server" Text="Do not send me an email reminder" CssClass="formFields" />
                   </td>
                </tr>
                <tr>
                  <td colspan="2" align="right" valign="bottom" style="padding-top:20px">
                  <asp:ImageButton ID="btnSubmit" runat="server" AlternateText="Submit" CausesValidation="true" ImageUrl="images/btn_submit_event.png"  onclick="btnSubmit_Click" />
                  &nbsp; 
                  <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" 
                          Visible="false" CausesValidation="false" ImageUrl="images/btn_cancel_event.png" 
                          onclick="btnCancel_Click" />
                  </td>
                </tr>
                </table></td>
  </tr>
</table>
    </div>
                      </td>
                    </tr>
                    <tr>
                      <td valign="middle"   align="center">&nbsp;</td>
                    </tr>                   
                </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
</form>
</body>
</html>
