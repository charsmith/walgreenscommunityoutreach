﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensClinicAgreementPrevSeason.aspx.cs"   Inherits="walgreensClinicAgreementPrevSeason" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" />
    <link href="css/calendarStyle.css" rel="stylesheet" type="text/css" />

    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/jquery.printelement.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .ui-timepicker-list li
        {
            color: #000000;
            font-family: "Times New Roman",Times,serif;
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            // for part of the page
            $("#lnkPrintContract").click(function () {
                $("#trContractBodyText").printElement({ printMode: 'popup' });
            });
        });
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfContactLogPk" runat="server" />
    <asp:HiddenField ID="hfUserEmail" runat="server" />
    <asp:HiddenField ID="hfUserType" runat="server" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="left">
              <table width="935" border="0" cellspacing="22" cellpadding="0">
                  <tr>
                      <td colspan="2" valign="top" class="pageTitle">Walgreens Community Off-Site Agreement</td>
                  </tr>
                  <tr>
                      <td valign="top" width="599" ><table width="599" border="0" cellpadding="0">
                        <tr>
                          <td>
                            <table width="612" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td  class="contractBodyText" style="padding-bottom:6px">
                                        <table  align="right">
                                            <tr>
                                                <td><asp:LinkButton ID="lnkChangeCulture" runat="server" Visible="true" onclick="lnkChangeCulture_Click">Spanish Version</asp:LinkButton></td>
                                                <td><asp:LinkButton ID="lnkPrintContract" runat="server" Visible="false">Print Contract</asp:LinkButton></td>
                                            </tr>
                                        </table>
                                     </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="612" border="0" cellspacing="0" cellpadding="36" id="trContractBodyText" style="border:#CCC solid 1px" >
                                            <tr>
                                                <td class="contractBodyText">
                                                    <p align="center">
                                                        <b>
                                                            <img src="images/contract_logo.gif" width="191" height="37" alt="Walgreens" /><br />
                                                            COMMUNITY OFF-SITE CLINIC AGREEMENT</b></p>
                                                    <p align="justify">
                                                        This <b><i>COMMUNITY OFF-SITE CLINIC AGREEMENT</i></b> (“<b>Agreement</b>”) by and
                                                        between the party indicated below (“<b>Client</b>”), and Walgreen Co., on behalf
                                                        of itself and all of its subsidiaries and affiliates (“<b>Walgreens</b>”) is made
                                                        and entered into on the date last signed by an authorized representative of both
                                                        the Client and Walgreens (the “<b>Effective Date</b>”).</p>
                                                    <p align="justify">
                                                        For good and valuable consideration, the receipt and sufficiency of which are hereby 
                                                        acknowledged, Client and Walgreens, by their signatures below, hereby agree that 
                                                        (i) Walgreens will provide the <b>Immunizations</b> listed below, consisting of dispensing and 
                                                        administering of a certain vaccine or vaccines to participants (“<b>Participants</b>”) at 
                                                        mutually agreed upon dates and times at the Client’s facility(ies) listed below (“<b>Covered Services</b>”); 
                                                        and (ii) it will comply with the terms and conditions of this Agreement, as shown on the following pages.</p>
                                                    <table width="580" border="0" align="center" cellpadding="0" cellspacing="5" id="tblBusinesses" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                                <b>Client Facility Location(s)*:</b>
                                                            </td>
                                                            <td align="right"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:GridView ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" DataKeyNames="state,clinicDate" >
                                                                    <Columns>
                                                                        <asp:BoundField DataField="state" Visible="false" />
                                                                        <asp:BoundField DataField="clinicDate" Visible="false" />                                                            
                                                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="100%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                            <table width="100%"  style="border-style:solid; border-width:0px; border-color:Black">
                                                                              <tr>
                                                                                <td colspan="5" align="left">
                                                                                    <asp:Label ID="lblClinicLocation" Text='<%# Bind("clinicLocation") %>' runat="server" style="font-weight:bold"></asp:Label>
                                                                                </td>
                                                                                <%--<td colspan="3" align="right">
                                                                                    <asp:Label ID="lblIsReassignClinic" runat="server" Text='<%# Bind("isReassign") %>' Visible="false" ></asp:Label>                                                                       
                                                                                    <asp:CheckBox ID="chkReassignClinic" runat="server" Text="Re-assign based on geography*" Checked='<%# Convert.ToInt32(Eval("isReassign")) == 1 ? true : false %>' />
                                                                                </td>--%>
                                                                              </tr>
                                                                            </table>
                                                                            <table width="100%"  style="border-style:solid; border-width:1px; border-color:Black">
                                                                                 <tr>
                                                                                    <td><b> Local Contact Name</b></td>
                                                                                    <td><b> Local Contact Phone</b></td>
                                                                                    <td colspan="3"><b> Local Contact Email</b></td>                                                                        
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><asp:Label ID="lblLocalContactName" runat="server" Text='<%# Bind("localContactName") %>' CssClass="contractBodyText"  Width="100px" ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblLocalContactPhone" runat="server" Text='<%# Bind("LocalContactPhone") %>' CssClass="contractBodyText" Width="100px" ></asp:Label></td>
                                                                                    <td colspan="3"><asp:Label ID="lblLocalContactEmail" runat="server" Text='<%# Bind("LocalContactEmail") %>'  CssClass="contractBodyText" Width="100px" ></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><b> Address1</b></td>
                                                                                    <td><b> Address2</b></td>
                                                                                    <td><b> City</b></td>
                                                                                    <td><b> State</b></td>
                                                                                    <td><b> Zip</b></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><asp:Label ID="lblAddress1" runat="server" Text='<%# Bind("Address1") %>' CssClass="contractBodyText" Width="100px" ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblAddress2" runat="server" Text='<%# Bind("Address2") %>' CssClass="contractBodyText" Width="100px" ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblCity" runat="server" Text='<%# Bind("city") %>'  CssClass="contractBodyText" Width="100px" MaxLength="100" ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblState" runat="server"  CssClass="contractBodyText" Width="95%" Text='<%# Bind("state") %>' ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblZipCode" runat="server" CssClass="contractBodyText" Width="95%" Text='<%# Bind("zipCode") %>' ></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><b> Clinic Date</b></td>
                                                                                    <td><b> Start Time</b></td>
                                                                                    <td><b> End Time</b></td>
                                                                                    <td><b> Est. Shots:</b></td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><asp:Label ID="lblClinicDateTime" runat="server" Text='<%# Bind("clinicDate") %>' CssClass="contractBodyText" ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblStartTime" runat="server"  Text='<%# Bind("startTime") %>' CssClass="contractBodyText" Width="55px" ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblEndTime" runat="server" Text='<%# Bind("endTime") %>' CssClass="contractBodyText" Width="55px" ></asp:Label></td>
                                                                                    <td><asp:Label ID="lblEstShots" runat="server" Text='<%# Bind("EstShots") %>' CssClass="contractBodyText" Width="55px" ></asp:Label></td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                            <br />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>                                                    
                                                            </td>
                                                        </tr>
                                                        <tr id="rowReassignClinicStore" runat="server">
                                                            <td colspan="2">*Clinics will be re-assigned to another store if the clinic location is over 10 miles from the contracting store or crosses state boundaries.</td>
                                                        </tr>                      
                                                    </table>
                                                    <table id="tblAgreement" runat="server" width="100%" border="0"><tr><td>
                                                    <p align="center">
                                                        <b>IN WITNESS WHEREOF</b>, Client and Walgreens have electronically executed this Agreement, as of the Effective Date.</p>
                                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
                                                        <tr>
                                                            <td width="46" align="left" valign="middle">
                                                                <b>CLIENT:</b>
                                                            </td>
                                                            <td width="210" align="left" valign="middle">
                                                                <asp:Label ID="lblClient" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                            </td>
                                                            <td colspan="2" align="left" valign="middle">
                                                                <b><u>WALGREEN CO.</u></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="middle">
                                                                NAME:
                                                            </td>
                                                            <td align="left" valign="middle">
                                                                <asp:Label ID="lblClientName" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                            </td>
                                                            <td width="46" align="left" valign="middle">
                                                                NAME:
                                                            </td>
                                                            <td width="210" align="left" valign="middle">
                                                                <asp:Label ID="lblWalgreenName" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="middle">
                                                                TITLE:
                                                            </td>
                                                            <td align="left" valign="middle">
                                                                <asp:Label ID="lblClientTitle" runat="server" Width="200px" CssClass="contractBodyText"></asp:Label>
                                                            </td>
                                                            <td align="left" valign="middle">
                                                                TITLE:
                                                            </td>
                                                            <td align="left" valign="middle">
                                                                <asp:Label ID="lblWalgreenTitle" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="middle">
                                                                DATE:
                                                            </td>
                                                            <td align="left" valign="middle">
                                                                <asp:Label ID="lblClientDate" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                            </td>
                                                            <td align="left" valign="middle">
                                                                DATE:
                                                            </td>
                                                            <td align="left" valign="middle">
                                                                <asp:Label ID="lblWalgreenDate" runat="server" Width="200px" CssClass="contractBodyText" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="left" valign="middle">
                                                                <u>Send Legal Notices To Client At:</u>
                                                            </td>
                                                            <td colspan="2" align="left" valign="middle">
                                                                DISTRICT NUMBER:&nbsp;
                                                                <asp:Label ID="lblDistrictNum" runat="server" Width="103px" CssClass="contractBodyText" ></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td colspan="2" align="left" valign="middle" rowspan="3" >
                                                            <table width="100%" border="0">
                                                            <tr>
                                                           <td>Attention to:</td>
                                                                <td align="left"><asp:Label ID="lblLegalAttentionTo" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                               </td>
                                                              </tr>
                                                                    <tr>
                                                                <td>Address1:</td>
                                                                 <td  align="left">
                                                                        <asp:Label ID="lblLegalAddress1" runat="server" CssClass="contractBodyText" ></asp:Label>
                                                                 </td>
                                                             </tr>
                                                      
                                                                 <tr>
                                                                   <td>Address2:</td>
                                                                   <td align="left">
                                                                <asp:Label ID="lblLegalAddress2" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                                    </td></tr>
                                                                    <tr>
                                                                    <td>City:</td>
                                                               <td align="left"> <asp:Label ID="lblLegalCity" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                                    </td>
                                                                    </tr>
                                                                    <tr>
                                                                     <td>State:</td> 
                                                             <td  align="left"><asp:Label ID="lblLegalState" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                             </td> 
                                                                   </tr>
                                                                    <tr> <td>Zip Code:</td>
                                                                <td  align="left"><asp:Label ID="lblLegalZipCode" runat="server"  CssClass="contractBodyText" ></asp:Label>
                                                                    </td> </tr>  </table>
                                                            </td>
                                                            </tr>
                                         
                                                        <tr>
                                                            <td colspan="2" align="left" valign="middle">
                                                                <u>Send Legal Notices To Walgreens At:</u>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="left" valign="top">
                                                              Healthcare Innovations Group<br />
                                                              200 Wilmot Rd<br />
                                                              MS2222<br />
                                                              Deerfield, IL 60015<br />
                                                              Attn: Health Law – Divisional Vice President<br />
                                                              cc: clinicalcontracts@walgreens.com</td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" align="left" valign="top">
                                                                <asp:GridView ID="grdImmunizationChecks" runat="server" AutoGenerateColumns="False" GridLines="None" onrowdatabound="grdImmunizationChecks_RowDataBound" Width="100%">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="ImmunizationName" Visible="false" />
                                                                        <asp:TemplateField HeaderText="<u>Immunization</u>" HeaderStyle-Width="70%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblImmunizationCheck" runat="server" Text='<%# Bind("name") %>'/>
                                                                                <asp:Label ID="lblImmunizationPk" runat="server" Text='<%# Bind("pk") %>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<u>Price*</u>" HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" >
                                                                            <ItemTemplate>
                                                                                <asp:Label  style="text-align:left" ID="lblValue"  runat="server" Text='<%# Bind("price") %>' Width="50px" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <br />
                                                                <table border="0" cellpadding="0" cellspacing="0" width="95%">
                                                                    <tr>
                                                                        <td><asp:Label ID="lblImmunizationDisclaimer" runat="server" Text="*Price includes vaccine and administration."></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>**The influenza price is based on following minimum <br />number to be invoiced.The price will remain even if <br />the number of immunizations exceeds the minimum.</td>
                                                                    </tr>
                                                                    <tr><td><asp:Label ID="lblMinimumInvoiced" runat="server" Visible="false"></asp:Label></td></tr>
                                                                </table>                                                    
                                                            </td>                                                 
                                                            <td colspan="2" align="left" valign="top">
                                                                <table width="100%" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:GridView ID="grdPaymentTypes" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" onrowdatabound="grdPaymentTypes_RowDataBound"  >
                                                                              <Columns>
                                                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="90%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                    <table id="Table1" width="100%" runat="server" border="0" >
                                                                                        <tr><td colspan="2"><b>PAYMENT TYPE: </b>
                                                                                            <asp:Label ID="lblPaymentType" CssClass="contractBodyText" runat="server" Text='<%# Bind("paymentType") %>' ></asp:Label>
                                                                                            <asp:Label ID="lblPaymentTypeId" Text='<%# Bind("paymentId") %>' runat="server" Visible="false"></asp:Label>
                                                                                        </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <table width="100%" id="tblPaymentInfo" runat="server" border="0" visible="false" >
                                                                                                    <tr><td colspan="2"> Send Invoice To:</td></tr>
                                                                                                    <tr>
                                                                                                        <td>Name:</td>
                                                                                                        <td><asp:Label ID="lblPmtName" CssClass="contractBodyText" runat="server" Text='<%# Bind("name") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Address1:</td>
                                                                                                        <td><asp:Label ID="lblPmtAddress1" CssClass="contractBodyText" runat="server" Text='<%# Bind("address1") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Address2:</td>
                                                                                                        <td><asp:Label ID="lblPmtAddress2" CssClass="contractBodyText" runat="server" Text='<%# Bind("address2") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>City:</td>
                                                                                                        <td><asp:Label ID="lblPmtCity" CssClass="contractBodyText" runat="server" Text='<%# Bind("city") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>State:</td>
                                                                                                        <td><asp:Label ID="lblPmtState" CssClass="contractBodyText" runat="server" Text='<%# Bind("state") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Zip Code:</td>
                                                                                                        <td><asp:Label ID="lblPmtZipCode" CssClass="contractBodyText" runat="server" Text='<%# Bind("zip") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Phone:</td>
                                                                                                        <td><asp:Label ID="lblPmtPhone" CssClass="contractBodyText" runat="server" Text='<%# Bind("phone") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Email:</td>
                                                                                                        <td><asp:Label ID="lblPmtEmail" CssClass="contractBodyText" runat="server" Text='<%# Bind("email") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2">Is Employer Tax Exempt?<asp:Label ID="lblPmtTaxExempt" CssClass="contractBodyText" runat="server" Text='<%# Bind("tax") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2">Will Patient Pay a Portion of the Cost - Is There a Copay?<br />
                                                                                                            <asp:Label ID="lblIsCopay" CssClass="contractBodyText" runat="server" Text='<%# Bind("copay") %>'></asp:Label>
                                                                                                            <asp:Label ID="lblDollar" runat="server" Text="$"></asp:Label>
                                                                                                            <asp:Label CssClass="contractBodyText" ID="lblCopy" runat="server" Text='<%# Bind("copayValue") %>'></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2">Voucher Needed:<asp:Label ID="lblVocher" CssClass="contractBodyText" runat="server" Text='<%# Bind("isVoucherNeeded") %>'></asp:Label></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <br />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                              </Columns>
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="left" valign="middle">&nbsp;</td>
                                                            <td colspan="2" align="left" valign="middle">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <p align="center">
                                                        <b>WALGREENS COMMUNITY OFF-SITE CLINIC AGREEMENT
                                                            <br />
                                                            TERMS AND CONDITIONS</b></p>
                                                    <table width="540" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="47%" align="left" valign="top">
                                                                <p align="justify">
                                                                    <b>I. Walgreens’ Responsibilities</b><br />
                                                                    <u>Covered Vaccine Services.</u> Subject to the limitations or restrictions imposed
                                                                    by federal and state contracts, laws, and regulations, and the availability of the
                                                                    appropriate Vaccine, Walgreens will provide the Covered Vaccine Services to Participants.
                                                                    With respect to such Covered Vaccine Services, the parties will comply with the
                                                                    procedures set forth herein.
                                                                </p>
                                                                <p align="justify">
                                                                    <u>Provision of Health Care Professionals.</u> Walgreens will provide Client with
                                                                    the appropriate number of qualified health care professionals and technicians to
                                                                    provide Covered Vaccine Services.<br />
                                                                    <br />
                                                                    <u>Professional Judgment.</u> Walgreens may withhold Covered Vaccine Services to
                                                                    a Participant for good cause, including but not necessarily limited to, the Participant’s
                                                                    failure to pay for Covered Vaccine Services rendered; requests by Participant for
                                                                    services inconsistent with the legal and regulatory requirements; or where, in the
                                                                    professional judgment of the health care professional, the services should not be
                                                                    rendered.</p>
                                                                <p align="justify">
                                                                    <b>II. Client’s Responsibilities</b><br />
                                                                    <u>Coordination.</u> Client will provide Participants with notice of the time and location
                                                                     in which Covered Vaccine Services will be provided and provide a private, clean room location,
                                                                     tables and chairs for Walgreens’ personnel and Participants. If applicable, Client will provide 
                                                                     Participants with Walgreens-approved vouchers, which Participants may redeem at a 
                                                                     participating Walgreens store location.
                                                                </p>
                                                                <p align="justify">
                                                                    <u>Access.</u> Client hereby grants to Walgreens, and to no other person or entity,
                                                                    access to its designated room or areas for the provision of Covered Vaccine Services
                                                                    for the time and date(s) mutually agreed upon by the parties, in accordance with
                                                                    the provisions of this Agreement.
                                                                </p>
                                                                <p align="justify">
                                                                    <u>Payment.</u> Prior to the provision of Covered Vaccine Services, Participant must 
                                                                    provide evidence of coverage under a third-party insurance or a government funded program 
                                                                    (e.g., Medicare). If such evidence is presented by the Participant and Walgreens is 
                                                                    contracted with the third-party insurance or government funded program, Walgreens will 
                                                                    submit the claim for that Participant and any copayment, coinsurance, deductible owed by 
                                                                    the Participant will be billed at a later date.  If such evidence is not provided at the 
                                                                    time of service, either Client or Participant shall be responsible to compensate Walgreens 
                                                                    at the lesser of the prices stated herein or the Usual and Customary Charge for the Vaccine 
                                                                    at the time of administration.  Payments made by Client are due within thirty (30) days from 
                                                                    receipt of the monthly invoice and must be sent to the remittance address stated on the invoice. 
                                                                    The invoice will contain the following data elements, and no further information will be provided: 
                                                                    Group ID, store number, prescription number, patient name, recipient number, physician name, cost, 
                                                                    service fee, copayment amount, sales tax, total charge, date of service, and drug name/NDC. 
                                                                    As used in this Agreement, “Usual and Customary Charge” shall refer to the amount charged 
                                                                    to a cash customer by the administering pharmacy, exclusive of sales tax or other amounts claimed. </p>
                                                                <p align="justify">
                                                                    <b>III. Term and Termination</b><br />
                                                                    <u>Term and Termination</u> This Agreement will commence as of the Effective Date
                                                                    and will continue for one year. Either party may terminate this Agreement upon prior
                                                                    written notice to the other party.</p>
                                                                <p align="justify">
                                                                    <u>Effect of Termination.</u> Termination will have no effect upon the rights or
                                                                    obligations of the parties arising out of any transactions occurring prior to the
                                                                    effective date of such termination.</p>
                                                                <p align="justify">
                                                                    <b>IV. Indemnification</b><br />
                                                                    <u>Indemnification.</u> To the extent permitted by law, each party will indemnify,
                                                                    defend, and hold harmless the other party, including its employees and agents, from
                                                                    and against any and all claims or liabilities arising from the negligence or wrongful
                                                                    act of the indemnifying party, its employees, or agents in carrying out its duties
                                                                    and obligations under the terms of this Agreement. This <u>section</u> will survive
                                                                    the termination of this Agreement.</p>
                                                            </td>
                                                            <td width="6%" align="left" valign="middle">&nbsp;
                                                    
                                                            </td>
                                                            <td width="47%" colspan="2" align="left" valign="top">
                                                                <p align="justify">
                                                                    <b>V. Insurance</b><br />
                                                                    <u>Insurance.</u> Each party will self-insure or maintain at its sole expense, and
                                                                    in amounts consistent with industry standards, Commercial General Liability Insurance
                                                                    and Professional Liability Insurance and such other insurance as may be necessary
                                                                    to insure each respective party, its employees, and agents against any claim or
                                                                    claims for damages arising out of or in connection with its duties and obligations
                                                                    under this Agreement. If Client requires Walgreens to name Client as Additional Insured
                                                                    under its Commercial General Liability policy, such Client will automatically be
                                                                    named as per the terms of Walgreens’ insurance policy. Evidence of such insurance
                                                                    can be obtained by downloading the Walgreens Memorandum of Liability Insurance and
                                                                    Memorandum of Professional Liability Insurance and other relevant information regarding
                                                                    Walgreens’ insurance program at <a href="http://www.walgreens.com/Insurance">www.walgreens.com/Insurance</a>.
                                                                    <br />
                                                                    <br />
                                                                    <b>VI. General Terms</b><br />
                                                                    <u>Confidentiality of PHI.</u> Both parties warrant that they will maintain and protect
                                                                    the confidentiality of all individually identifiable health information specifically
                                                                    relating to Participants (“Protected Health Information” or “PHI”) in accordance
                                                                    with the Health Insurance Portability and Accountability Act of 1996 and all applicable
                                                                    federal and state laws and regulations. However, nothing herein will limit either
                                                                    party’s use of any aggregated Participant information that does not contain PHI.
                                                                    This section will survive the termination of this Agreement.</p>
                                                                <p align="justify">
                                                                    <u>Advertising.</u> Neither party may advertise or use any trademarks, service marks,
                                                                    or symbols of the other party without first receiving the written consent of the
                                                                    party owning the mark and/or symbol with the following exceptions: Client may use
                                                                    the name and the addresses of Walgreens' locations in materials to inform Participants
                                                                    and the general public that Walgreens provides Covered Vaccine Services. Any other
                                                                    reference to Walgreens in any Client materials must be pre-approved, in writing,
                                                                    by Walgreens.
                                                                </p>
                                                                <p align="justify">
                                                                    <u>Force Majeure.</u> The performance by either party hereunder will be excused to
                                                                    the extent of circumstances beyond such party’s reasonable control, such as flood,
                                                                    tornado, earthquake, or other natural disaster, epidemic, war, material destruction
                                                                    of facilities, fire, acts of terrorism, acts of God, etc. In such event, the parties
                                                                    will use their best efforts to resume performance as soon as reasonably possible
                                                                    under the circumstances giving rise to the party’s failure to perform.</p>
                                                                <p align="justify">
                                                                    <u>Compliance.</u> The parties will comply with all applicable laws, rules, and regulations
                                                                    for each jurisdiction in which Covered Services are provided under this Agreement.
                                                                    Each party will cooperate with reasonable requests by the other party for information
                                                                    that is needed for its compliance with applicable laws, rules, and/or regulations.
                                                                </p>
                                                                <p align="justify">
                                                                    <u>Notices.</u> All notices provided for herein must be in writing sent by U.S. certified
                                                                    mail, return receipt requested, postage prepaid, or by overnight delivery service
                                                                    providing proof of receipt to the address set forth following the signature blocks.
                                                                    Notices will be deemed delivered upon receipt or upon refusal to accept delivery.</p>
                                                                <p align="justify">
                                                                    <u>Entire Agreement.</u> This Agreement, which includes any and all attachments,
                                                                    exhibits, riders, and other documents referenced herein, constitutes the entire
                                                                    and full agreement between the parties hereto and supersedes any previous contract
                                                                    and no changes, amendments, or alterations will be effective unless reduced to a
                                                                    writing signed by a representative of each party. Any prior agreements, documents,
                                                                    understandings, or representations relating to the subject matter of this Agreement
                                                                    not expressly set forth herein or referred to or incorporated herein by reference
                                                                    are of no force or effect.
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">&nbsp;
                                                    
                                                            </td>
                                                            <td align="left" valign="middle">&nbsp;
                                                    
                                                            </td>
                                                          <%--  <td colspan="2" align="right" valign="top">
                                                                <b>Customer Initials</b>
                                                                <asp:TextBox ID="txtInitials2" runat="server" Width="50px" CssClass="contractBodyText" style="background-color: #FF0"  MaxLength="50" ></asp:TextBox>
                                                    
                                                            </td>--%>
                                                        </tr>
                                                    </table>
                                                    <p align="right">
                                                        <span style="font-size: 10px;">©2015 Walgreen Co. All rights reserved.</span>
                                                    </p>
                                                    </td></tr></table>
                                       
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                          </td>
                        </tr>
                      </table></td>
                      <td valign="top" width="268" style="padding-top:20px"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                            <tr>
                              <td>
                                 <table border="0" cellspacing="12">
                                    <tr>
                                         <td  align="left" valign="top">
                                             <asp:Label ID="lblErrorMessage" runat="server" class="bestPracticesText" ForeColor="Red" Visible="false"></asp:Label>
                                         </td>
                                    </tr>
                                    <tr>
                                    <td colspan="2" align="left" valign="top">
                                        <table runat="server" id="tblEmails" visible="false" ><tr><td><span class="bestPracticesText">
                                            <b>Email Agreement to:
                                                <asp:TextBox ID="txtEmails" runat="server"  CssClass="bestPracticesText" ValidationGroup="WalgreensUser" Width="205px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtEmails" ID="txtEmailsReqFV" runat="server" Display="None" ErrorMessage="Enter an Email Address in the 'Email Agreement to:' field" ValidationGroup="WalgreensUser" ></asp:RequiredFieldValidator>
                                                <%--<asp:RegularExpressionValidator ControlToValidate="txtEmails" ID="txtEmailsRegEV" runat="server" Display="None" ErrorMessage="The 'Email Agreement to:' email is Invalid." ValidationGroup="WalgreensUser" ValidationExpression="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},?)+$" ></asp:RegularExpressionValidator>--%>
                                            </b></span><br />
                                            <span class="noteText">Multiple email addresses may be entered by placing a comma between addresses.</span>
                                            </td></tr>
                                        </table>
                                        <table border="0" width="100%" runat="server" id="tblBusinessUserMsg">
                                            <tr>
                                              <td colspan="2" align="left" valign="top" style="font-size:12px; color:#333; font-family:Arial, Helvetica, sans-serif">
                                                Carefully review the Community Off-Site Agreement. If you agree to the conditions of the contract, please check "Approve" below and type your name into the Electronic Signature field. If there are any discrepancies in the Agreement, reject the Agreement and provide corrections in the notes field.
                                              </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px">
                                        <table border="0" cellspacing="0" cellpadding="1" id="tblApproval" runat="server" visible="false">
                                            <tr>
                                                <td>
                                                    <b><asp:RadioButton ID="rbtnApprove" CssClass="formFields" runat="server" Text="Approve" GroupName="agreement" /></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" id="tdElectronicSignature">Electronic Signature &nbsp;
                                                    <asp:TextBox ID="txtElectronicSign" runat="server" MaxLength="500" CssClass="bestPracticesText" Style="background-color: #FF0" Width="90%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b><asp:RadioButton ID="rbtnReject" CssClass="formFields" runat="server" Text="Reject" GroupName="agreement" /></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trNotes" runat="server" visible="false">
                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px">Notes:<br />
                                        <label for="textarea"></label>
                                        <asp:TextBox ID="txtNotes" class="formFields" runat="server" Columns="75" Rows="5" TextMode="MultiLine" Width="210px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" valign="top" class="bestPracticesText">
                                        <asp:ImageButton ID="btnSendMail" ImageUrl="images/btn_send_email.png" CausesValidation="true" runat="server" AlternateText="Send email" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_send_email.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_send_email_lit.png');" 
                                            onclick="btnSendMail_Click" ValidationGroup="WalgreensUser" ImageAlign="Middle" Visible="false" />
                                        <%--<asp:ImageButton ID="btnSendLater" ImageUrl="images/btn_save_send_later.png" CausesValidation="true" runat="server" AlternateText="Send Later" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_send_later.png');"
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_send_later_lit.png');" 
                                            onclick="btnSendLater_Click"  ValidationGroup="SendLater" ImageAlign="Middle" />--%>
                                        <asp:ImageButton ID="btnCancel" ImageUrl="images/btn_cancel_mini.png" CausesValidation="false" runat="server" AlternateText="Cancel" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_mini.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_mini_lit.png');" 
                                            onclick="btnCancel_Click" ImageAlign="Middle" Visible="false" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trNoteText" visible="false">
                                    <td colspan="2" align="left" valign="top"><span class="noteText">* Please note, once a contract has been signed by the client, it cannot be edited. It will have to be deleted and re-initiated. 
                                    To delete a contract/clinic open the Clinic Details  and  click "Delete Clinic Agreement".</span>
                                    </td>
                                </tr>
                            </table>
                              </td>
                            </tr>
                      </table></td>
                    </tr>                   
                </table>
            </td>
        </tr>
    </table>
	<ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>