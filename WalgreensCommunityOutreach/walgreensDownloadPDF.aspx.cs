﻿using PdfSharp.Drawing;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using TdWalgreens;

public partial class walgreensFluForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WagContractPdfMaker contract_pdf_maker = new WagContractPdfMaker();
        byte[] new_pdf_bytes = null;
        try
        {
            this.dbOperation = new DBOperations();
            DataSet ds_Voucher_Data = new DataSet();
            string form_value = Session["downloadType"].ToString();
            int clinic_pk = Convert.ToInt32(Session["clinicPk"]);
            int outreach_business_pk = Convert.ToInt32(Session["outReachBusinessPk"]);
            ds_Voucher_Data = this.dbOperation.getContractVoucherVarFormData(outreach_business_pk, clinic_pk);
            if (this.commonAppSession.LoginUserInfoSession.UserID > 0 && ds_Voucher_Data.Tables.Count > 0 && ds_Voucher_Data.Tables[0].Rows.Count > 0)
            {
                new_pdf_bytes = contract_pdf_maker.getVoucherFormBytes(ds_Voucher_Data, form_value);
                if (new_pdf_bytes != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", new_pdf_bytes.Length.ToString());
                    Response.BinaryWrite(new_pdf_bytes);
                }
            }
        
            else
            {
                Response.Redirect("~/auth/auth.aspx", false);
            }
        }
        catch
        {
            Response.Redirect("~/auth/auth.aspx", false);
        }
    }

    #region ----------------- PRIVATE VARIABLES ----------------
    private DBOperations dbOperation = null;
    #endregion

    #region ----------------- PROTECTED VARIABLES ----------------
    protected AppCommonSession commonAppSession = null;
    #endregion

    #region ----------------- Web Form Designer generated code -----------------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}