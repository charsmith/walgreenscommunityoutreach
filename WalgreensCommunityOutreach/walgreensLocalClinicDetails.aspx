﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensLocalClinicDetails.aspx.cs"
    Inherits="walgreensLocalClinicDetails" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta content="IE=8" http-equiv="X-UA-Compatible" />
    <title>Walgreens Community Outreach</title>
    <link rel="stylesheet" href="css/ddcolortabs.css" type="text/css" />
    <link rel="stylesheet" href="css/wags.css?v=10202015" type="text/css" />
    <link rel="stylesheet" href="css/theme.css" type="text/css" />
    <link rel="stylesheet" href="css/calendarStyle.css" type="text/css" />
    <%--<link rel="stylesheet" href="themes/jquery-ui-1.8.17.custom.css" type="text/css" />--%>
    <link rel="stylesheet" href="css/jquery.timepicker.css" type="text/css" />
    <link rel="stylesheet"  href="css/jquery-ui.css" type="text/css" />

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>

    <script src="javascript/commonFunctions.js?v=10202015" type="text/javascript"></script>
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>

    <style type="text/css">
       .wagButtonDim {
	        -moz-box-shadow: 0px 0px 0px 0px #ffffff;
	        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;
	        box-shadow: 0px 0px 0px 0px #ffffff;
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #CFECEC), color-stop(1, #B7CEEC));
	        background:-moz-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:-webkit-linear-gradient(top,#CFECEC 5%, #B7CEEC 100%);
	        background:-o-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:-ms-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:linear-gradient(to bottom, #CFECEC 5%, #B7CEEC 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#CFECEC', endColorstr='#B7CEEC',GradientType=0);
	        background-color:#CFECEC;
	        -moz-border-radius:3px;
	        -webkit-border-radius:3px;
	        border-radius:3px;
	        border:1px solid #B7CEEC;
	        display:inline-block;
	        cursor:pointer;
	        color:#ffffff;
	        font-family:Arial;
	        font-size:12px;
	        padding:6px 12px;
	        text-decoration:none;
	        text-shadow:0px -1px 0px #B7CEEC;
	        behavior:url(PIE.htc);
        }
        
        .wagButton {
	        -moz-box-shadow: 0px 0px 0px 0px #ffffff;
	        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;
	        box-shadow: 0px 0px 0px 0px #ffffff;
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #57a1d3), color-stop(1, #2f6fa7));
	        background:-moz-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-webkit-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-o-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-ms-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:linear-gradient(to bottom, #57a1d3 5%, #2f6fa7 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#57a1d3', endColorstr='#2f6fa7',GradientType=0);
	        background-color:#57a1d3;
	        -moz-border-radius:3px;
	        -webkit-border-radius:3px;
	        border-radius:3px;
	        border:1px solid #2f6fa7;
	        display:inline-block;
	        cursor:pointer;
	        color:#ffffff;
	        font-family:Arial;
	        font-size:12px;
	        padding:6px 12px;
	        text-decoration:none;
	        text-shadow:0px -1px 0px #2f6fa7;
	        behavior:url(PIE.htc);
        }
        .wagButton:hover {
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #2f6fa7), color-stop(1, #57a1d3));
	        background:-moz-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-webkit-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-o-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-ms-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:linear-gradient(to bottom, #2f6fa7 5%, #57a1d3 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#2f6fa7', endColorstr='#57a1d3',GradientType=0);
	        background-color:#2f6fa7;
        }
        .wagButton:active {
	        position:relative;
	        top:1px;
        }
        
        .ui-timepicker-list li
        {
            color: #333333;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
        }
        
        .wrapword
        { 
            white-space: pre-wrap;       /* css-3 */
            white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */        
            white-space: -pre-wrap;      /* Opera 4-6 */ 
            white-space: -o-pre-wrap;    /* Opera 7 */ 
            /*word-wrap: break-word;*/       /* Internet Explorer 5.5+ */        
            /*-ms-word-break: break-all;
            word-break: break-all;
            text-overflow: break-all;*/
        }
        
        .ui-dialog-titlebar-close {
            visibility: hidden;
        }
        .ui-widget-header
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 13px;
            font-weight: bold;
        }        
        .ui-widget
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
        }
        .tooltipText
        {
            font-weight:bold;
            font-size:11px;
            color:red;
            position:absolute;
            white-space: pre-wrap;
            max-width:180px;
            padding:2px;
            border-radius:0px;
            -webkit-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            -moz-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
        }
        .formatButtonList
        {
          margin-right: 5px;
        }
    </style>
    <script type="text/javascript">
        var logMessage = "";
        var clinicDetailsInfo = "";

        function fnGetBrowserVersion() {
            var N = navigator.appName, ua = navigator.userAgent, tem;
            var M = ua.match(/(chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
            if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
            M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
            return M[0] + ' ' + M[1] + ' ';
        }
        //setting control css
        var setInvalidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "background-color": "yellow" });

            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.tooltip({
                tooltipClass: "tooltipText"
            });
            ctrl.focus(function (evt) {
                $(evt.currentTarget).tooltip("close");
            });
            ctrl.tooltip("enable");

        }
        var setValidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "border": "1px solid gray" });
            ctrl.css({ "background-color": "" });
            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.removeAttr("title");
            ctrl.tooltip({
                disabled: true
            });
            ctrl.on("click",function () {
                ctrl.data("title", ctrl.attr("title")).removeAttr("title");                
            }, function () {
                var title = ctrl.data("title");
                ctrl.tooltip("option", "content", title || ctrl.attr("title"));
            });
            

        }
        $(document).ready(function () {

            $('.formFields').keypress(function (key) {
                if (key.charCode == 63) return false;
            });

            if ($.browser.msie) {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keydown(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keydown(function (event) { event.preventDefault(); });
            }
            else {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keypress(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keypress(function (event) { event.preventDefault(); });
            }

            $("#grdImmunizationChecks").find("input[type=text][id*=txtEstimatedQuantity]").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#grdImmunizationChecks").find("input[type=text][id*=txtTotalImmAdministered]").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            var maxLength = 140;
            var returnValue = false;
            $("#idCount").text(maxLength);
            $("#idLabel").text(maxLength);
            $("#txtFeedBack").keypress(function (e) {
                if (($("#txtFeedBack").val().length < maxLength) || (e.keyCode == 8 || e.keyCode == 46 || (e.keyCode >= 35 && e.keyCode <= 40))) {
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                    returnValue = true;
                }
                else {
                    $("#idCount").text(0);
                    returnValue = false;
                }
                return returnValue;
            });

            $("#txtFeedBack").focusout(function () {
                if ($("#txtFeedBack").val().length > maxLength) {
                    $("#txtFeedBack").val($("#txtFeedBack").val().substring(0, maxLength));
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                }
                else {
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                }

                $("#txtFeedBack").val($("#txtFeedBack").val().replace("<", "&lt;").replace(">", "&gt;"));
            });

            $('#grdLocations tr:has(td)').each(function () {
                if ($(this).find('input[type=text][id*=txtAddress1]')[0] != undefined) {
                    if ($(this).find('input[type=hidden][id*=hfClinicLatitude]').val() == "" || $(this).find('input[type=hidden][id*=hfClinicLongitude]').val() == "")
                        setClinicLatsLongs($(this).find('input[type=text][id*=txtAddress1]')[0]);
                }
            });
            
            //Disable Add Button when location is not selected in dropdown for Previous MO State clinics.
            disableAddBtnForMOPrevious();
            $("#<%= grdLocations.ClientID %> input[id*='chkNoClinic']").each(function () {
                var ctl_id = $(this)[0].id;
                showHideVoucherDetails(ctl_id);
            });
            $("#<%= grdLocations.ClientID %> input[id*='chkNoClinic']").click(function () {
                var ctl_id = $(this)[0].id;
                showHideVoucherDetails(ctl_id);
                $("[id*=cellDownloadBtns]").toggle();
            });
        });

        function disableAddBtnForMOPrevious()
        {
            if ($("#hfIsMoPrevious").val().toString().toLowerCase() == "true")
            {
                var btnAddPreviousClinic = $("#grdLocations").find("input[type=image][id*=imgBtnAddLocation]")[0];
                var ddlClinicLocations = $("#grdLocations").find("input[type=select][id*=ddlClinicLocations]")[0];
                if (ddlClinicLocations != null)
                {
                    if (typeof ddlClinicLocations !== "undefined")
                    {
                        if (ddlClinicLocations.selectedIndex == 0)
                            btnAddPreviousClinic.disabled = true;
                        else
                            btnAddPreviousClinic.disabled = false;
                    }
                }
            }
        }
        function AlertMessage(alert_message, clinic_location, message) {
            if (alert_message != "")
                alert_message = "\r\n" + clinic_location + ": " + message;
            else
                alert_message = clinic_location + ": " + message;

            return alert_message;
        }

        function CustomValidatorForLocations(validationGroup, source_ctrl) {
            try {
                clinicDetailsInfo = '';
                logMessage = "; In try block; Client browser::" + fnGetBrowserVersion();
                logMessage += "; Source Control ID::" + (typeof source_ctrl === 'string' ? source_ctrl : source_ctrl.id);
                logMessage += "; StoreId::" + $("#hfBusinessStoreId").val() + "; ClinicPk::" + $("#hfBusinessClinicPk").val();
                var validating_fields = $("form :text, textarea, select");
                var validating_field_names = {

                    'txtDefaultClinicStoreId': { 'isRequired': false, 'when': 'always', 'name': 'Store Id', 'type': 'numeric', 'requiredMessage': 'Store Id is required', 'invalidMessage': 'Please enter valid Store Id' },
                    'txtFirstContactName': { 'isRequired': true, 'when': 'always', 'name': 'Contact First Name', 'type': 'junk', 'requiredMessage': 'Contact First Name is required', 'invalidMessage': 'Contact First Name: , < > characters are not allowed' },
                    'txtLocalContactPhonePrimary': { 'isRequired': true, 'when': 'always', 'name': 'Contact Phone', 'type': 'phone', 'requiredMessage': 'Contact Phone number is required', 'invalidMessage': 'Valid phone number is required(ex: ###-###-####)' },
                    'txtLastContactName': { 'isRequired': true, 'when': 'always', 'name': 'Contact Last Name', 'type': 'junk', 'requiredMessage': 'Contact Last Name is required', 'invalidMessage': 'Contact Last Name: , < > characters are not allowed' },
                    'txtLocalContactEmailPrimary': { 'isRequired': false, 'when': 'always', 'name': 'Contact Email', 'type': 'email', 'requiredMessage': '', 'invalidMessage': 'Please enter a valid Email address' },
                    'txtContactJobTitle': { 'isRequired': false, 'when': 'always', 'name': 'Contact Job Title', 'type': 'junk', 'requiredMessage': 'Contact Job Title is required', 'invalidMessage': 'Contact Job Title: , < > characters are not allowed' },
                    'txtPlanId': { 'isRequired': true, 'when': 'always', 'name': 'Plan ID', 'type': 'junk', 'requiredMessage': 'Plan ID is required', 'invalidMessage': 'Plan ID: < > characters are not allowed' },
                    'txtGroupId': { 'isRequired': true, 'when': 'always', 'name': 'Group ID', 'type': 'junk', 'requiredMessage': 'Group ID is required', 'invalidMessage': 'Group ID: < > characters are not allowed' },
                    'txtCopyGroupIdFlu': { 'isRequired': true, 'when': 'always', 'name': 'Copay Group ID Flu', 'type': 'junk', 'requiredMessage': 'Copay group id for Flu is required', 'invalidMessage': 'Group ID: < > characters are not allowed' },
                    'txtCopyGroupIdRoutine': { 'isRequired': true, 'when': 'always', 'name': 'Copay Group ID Routine', 'type': 'junk', 'requiredMessage': 'Copay group id for Routine is required', 'invalidMessage': 'Group ID: < > characters are not allowed' },
                    'txtIdRecipient': { 'isRequired': true, 'when': 'always', 'name': 'ID Recipient', 'type': 'junk', 'requiredMessage': 'ID Recipient is required', 'invalidMessage': 'ID Recipient: < > characters are not allowed' },

                    //********************** ImmunizationChecks Grid Control Section Start *****************
                    'txtEstimatedQuantity': { 'isRequired': true, 'when': 'always', 'name': 'Estimated Shots', 'type': 'numeric', 'requiredMessage': 'Please enter the number of Estimated Vol', 'invalidMessage': 'Please enter valid number of Estimated Vol (ex: #####)' },
                    'txtTotalImmAdministered': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Total Immunizations Administered', 'type': 'numeric', 'requiredMessage': 'Please enter number of Total Immunizations Administered', 'invalidMessage': 'Please enter valid number of Total Immunizations Administered (ex: #####)' },
                    //********************** ImmunizationChecks Grid Control Section End *****************

                    //********************** Locations Grid Control Section Start *****************                
                    'txtContactFirstName': { 'isRequired': true, 'when': 'always', 'name': 'Contact First Name', 'type': 'junk', 'requiredMessage': 'Contact First Name is required', 'invalidMessage': 'Contact First Name: < > characters are not allowed' },
                    'txtContactLastName': { 'isRequired': true, 'when': 'always', 'name': 'Contact Last Name', 'type': 'junk', 'requiredMessage': 'Contact Last Name is required', 'invalidMessage': 'Contact Last Name: < > characters are not allowed' },
                    'txtLocalContactPhone': { 'isRequired': true, 'when': 'always', 'name': 'Contact Phone', 'type': 'phone', 'requiredMessage': 'Contact Phone is required', 'invalidMessage': 'Valid phone number is required(ex: ###-###-####)' },
                    'txtLocalContactEmail': { 'isRequired': true, 'when': 'always', 'name': 'Contact Email', 'type': 'email', 'requiredMessage': 'Contact  Email is required', 'invalidMessage': 'Please enter a valid Email address' },
                    'txtAddress1': { 'isRequired': true, 'when': 'always', 'name': 'Address1', 'type': 'address', 'requiredMessage': 'Address1 is required', 'invalidMessage': 'Clinic Address1: < > characters are not allowed' },
                    'txtAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'address', 'requiredMessage': '', 'invalidMessage': 'Clinic Address2: < > characters are not allowed' },
                    'txtCity': { 'isRequired': true, 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': 'City is required', 'invalidMessage': 'Clinic City: < > characters are not allowed' },
                    'txtZipCode': { 'isRequired': true, 'when': 'always', 'name': 'ZipCode', 'type': 'zipcode', 'requiredMessage': 'Zip Code is required', 'invalidMessage': 'Please enter a valid Zip Code (ex: #####)' },
                    'txtConfirmedClientName': { 'isRequired': true, 'when': 'confirmclinic', 'name': 'Name of Client Individual Who Confirmed the Clinic', 'type': 'junk', 'requiredMessage': 'This clinic cannot be logged as Confirmed until you have entered the "Name of Individual"', 'invalidMessage': 'Name of Individual: < > characters are not allowed' },
                    'txtClinicStore': { 'isRequired': false, 'when': 'always', 'name': 'Clinic Store', 'type': 'numeric', 'requiredMessage': '', 'invalidMessage': 'Please enter valid Store Id' },
                    'txtEstShots': { 'isRequired': false, 'when': 'always', 'name': 'Estimated Shots', 'type': 'numeric', 'requiredMessage': '', 'invalidMessage': 'Please enter valid Estimated Shots (ex: #####)' },
                    'txtTotalImm': { 'isRequired': false, 'when': 'always', 'name': 'Total Immunizations Administered', 'type': 'numeric', 'requiredMessage': '', 'invalidMessage': 'Please enter valid number of Total Immunizations Administered (ex: #####)' },

                    'txtStartTime': { 'isRequired': true, 'when': 'always', 'name': 'Clinic Start Time', 'type': '', 'requiredMessage': 'Clinic Start time is required', 'invalidMessage': '' },
                    'txtEndTime': { 'isRequired': true, 'when': 'always', 'name': 'Clinic End Time', 'type': '', 'requiredMessage': 'Clinic End time is required', 'invalidMessage': '' },
                    'ddlState': { 'isRequired': true, 'when': 'always', 'name': 'State', 'type': 'select', 'requiredMessage': 'State is required', 'invalidMessage': '' },
                    'PickerAndCalendarFrom': { 'isRequired': true, 'when': 'always', 'name': 'Clinic Date', 'type': '', 'requiredMessage': 'Clinic Date is required', 'invalidMessage': '' },
                    //********************** Locations Grid Control Section End *****************

                    //**********************  Pharmacist Section Start ********************
                    'txtRxHost': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Name Of Rx Host', 'type': 'junk', 'requiredMessage': 'This clinic cannot be logged as Completed until you have entered \'Name of Rx Host\'', 'invalidMessage': 'Name Of Rx Host: < > characters are not allowed' },
                    'txtRxHostPhone': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Phone number', 'type': 'phone', 'requiredMessage': 'This clinic cannot be logged as Completed until you have entered \'Phone #\'', 'invalidMessage': 'Valid Phone number is required(ex: ###-###-####)' },
                    'txtTotalHoursClinicHeld': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Total Hours', 'type': 'decimal', 'requiredMessage': 'This clinic cannot be logged as Completed until you have entered \'Total Hours Clinic Held\'', 'invalidMessage': 'Please enter the valid Total Hours Clinic Held' },
                    'txtFeedBack': { 'isRequired': true, 'when': 'cancelledclinic', 'name': 'Feedback', 'type': 'junk', 'requiredMessage': 'This clinic cannot be logged as Cancelled until you have entered \'Feedback Notes\'', 'invalidMessage': 'Feedback: < > characters are not allowed' }
                    //**********************  Pharmacist Section End ********************    
                };
                
                var first_identified_control = "";
                var is_valid = true;
                var ctrl_Id;
                var indexOf_Underscore;
                var trimmedValue;
                var clinic_fields = ['PickerAndCalendarFrom', 'txtStartTime', 'txtEndTime'];
                var is_required = false;
                $.each(validating_fields, function (index, ctrl) {

                    if ($(ctrl).is(':disabled') == false) {
                        setValidControlCss($(ctrl), false);
						$(ctrl).attr({ "title": "" });
                    }
                    ctrl_Id = $(ctrl).attr('id');
                    clinicDetailsInfo += (clinicDetailsInfo != '') ? ' ' + ctrl_Id : ctrl_Id;
                    if (((ctrl_Id.indexOf('grdLocations') > -1) || (ctrl_Id.indexOf('grdImmunizationChecks') > -1)) && (ctrl_Id.lastIndexOf('_') > -1)) {

                        if (ctrl_Id.indexOf('grdLocations') > -1)
                            is_required = !$("input[type=checkbox][id=" + ctrl_Id.substring(0, ctrl_Id.indexOf('_', ctrl_Id.indexOf('_') + 1)) + "_chkNoClinic]")[0].checked;

                        if (ctrl_Id.toLowerCase().indexOf('_picker') > -1) {
                            var pos = 0;
                            var count = 0;
                            while (true) {
                                pos = ctrl_Id.indexOf('_', pos + 1);
                                ++count;
                                if (count == 2) {
                                    break;
                                }
                            }
                            ctrl_Id = ctrl_Id.substring(pos + 1, ctrl_Id.indexOf('_', pos + 1))
                        }
                        else {
                            indexOf_Underscore = ctrl_Id.lastIndexOf('_');
                            if (ctrl_Id.length > indexOf_Underscore + 1) {
                                ctrl_Id = ctrl_Id.substring(indexOf_Underscore + 1);
                            }
                        }

                        if (validating_field_names[ctrl_Id] != undefined && validating_field_names[ctrl_Id].isRequired != is_required)
                        {
                            if (ctrl_Id == 'PickerAndCalendarFrom' || ctrl_Id == 'txtStartTime' || ctrl_Id == 'txtEndTime')
                                validating_field_names[ctrl_Id].isRequired = is_required;
                        }
                    }
                    trimmedValue = $.trim($(ctrl).val());
                    clinicDetailsInfo += ':' + trimmedValue + '; ';
                    if ((validating_field_names[ctrl_Id] != undefined) && validating_field_names[ctrl_Id].isRequired &&
                    (validating_field_names[ctrl_Id].when == 'always' || validating_field_names[ctrl_Id].when == validationGroup.toLowerCase()) &&
                    trimmedValue == "") {
                        if (validating_field_names[ctrl_Id].type == 'select') {
                            if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 6.") != -1) || (navigator.userAgent.indexOf("Trident") != -1)) {                               
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            }
                            else {
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            }
                        }
                        else {
                            setInvalidControlCss($(ctrl), false);
                            if (ctrl_Id == 'PickerAndCalendarFrom')
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            else
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;
                    }
                    else if ((validating_field_names[ctrl_Id] != undefined) && trimmedValue != "") {

                        if (!validateData(validating_field_names[ctrl_Id].type, trimmedValue, ((validating_field_names[ctrl_Id].name == 'Estimated Shots' || validating_field_names[ctrl_Id].name == 'Total Immunizations Administered') ? false : true))) {
                            setInvalidControlCss($(ctrl), false);
                            if (ctrl_Id == 'PickerAndCalendarFrom')
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                            else
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                            if (first_identified_control == '')
                                first_identified_control = $(ctrl);
                            is_valid = false;
                        }
                        else if (validating_field_names[ctrl_Id].type == 'address') {
                            if ((!(validatePO(trimmedValue))) && ($(ctrl)[0].disabled == false))
                            {
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).attr({ "title": '<%= HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert") %>' });
                                if (first_identified_control == '')
                                    first_identified_control = $(ctrl);
                                is_valid = false;
                            }
                        }
                    }
                });
               
                if (!is_valid) {
                    alert("Highlighted input fields are required/invalid. Please update and submit.");
                    first_identified_control[0].focus();
                    return false;
                }
                else {
                    logMessage += "; Clinic Location::" + $("#lblClientName").text() + "; Clinic Details::" + clinicDetailsInfo;
                    //logJSerrors(logMessage.replace(/'/g, ""));
                    return true;
                }
            }
            catch (err) {
                var errorMessage = typeof (err.message) == 'undefined' ? err : err.message;
                logMessage += "; In catch block; StoreId::" + $("#hfBusinessStoreId").val() + "; ClinicPk::" + $("#hfBusinessClinicPk").val() + "; Clinic Location::" + $("#lblClientName").text() + "; Error Message::" + errorMessage + "; Clinic Details::" + clinicDetailsInfo;
                logJSerrors(logMessage.replace(/'/g, ""));
                alert('An error occurred, please contact administrator');
                return false;
            }
        }

        window.onerror = function (error_msg, url, line_number) {
            logMessage += "StoreId::" + $("#hfBusinessStoreId").val() + "; ClinicPk::" + $("#hfBusinessClinicPk").val() + "; Clinic Location::" + $("#lblClientName").text() + "; Error Message::" + error_msg + "; Line Number::" + line_number;
            logJSerrors(logMessage.replace(/'/g, ""));
            return false;
        }
        
        function logJSerrors(log_details) {
            $.ajax({
                url: 'walgreensLocalClinicDetails.aspx/logJSerrors',
                type: 'POST',
                data: "{'log_details':'" + log_details + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                }
            });
        }

        function setClinicLatsLongs(grd_row) {
            var grd_location = grd_row.id.substr(0, grd_row.id.lastIndexOf("_") + 1);

            if ($("#" + grd_location + "txtAddress1").val() != "" && $("#" + grd_location + "txtCity").val() != "" && $("#" + grd_location + "ddlState").val() != "" && $("#" + grd_location + "txtZipCode").val() != "") {
                var clinicaddress;
                clinicaddress = $("#" + grd_location + "txtAddress1").val() + " " + $("#" + grd_location + "txtAddress2").val() + ", " + $("#" + grd_location + "txtCity").val() + ", " + $("#" + grd_location + "ddlState").val() + " " + $("#" + grd_location + "txtZipCode").val();
                getClinicLocationLatLong(clinicaddress, grd_location);
            }
        }

        var geocoder;
        function initialize() {
            geocoder = new google.maps.Geocoder();
        }

        $(window).load(function () {
            google.maps.event.addDomListener(window, 'load', initialize);
        });

        var latitude;
        var longitude;
        function getClinicLocationLatLong(clinic_address, grd_location) {
            //clinicaddress = "";
            //clinicaddress = $(obj).find('input[type=text][id*=txtAddress1]').val() + " " + $(obj).find('input[type=text][id*=txtAddress2]').val() + ", " + $(obj).find('input[type=text][id*=txtCity]').val() + ", " + $(obj).find('select[id$=ddlState]').find('option:selected').val() + " " + $(obj).find('input[type=text][id*=txtZipCode]').val();

            geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': clinic_address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    latitude = results[0].geometry.location.lat();
                    longitude = results[0].geometry.location.lng();
                    $("#" + grd_location + "hfClinicLatitude").val(latitude);
                    $("#" + grd_location + "hfClinicLongitude").val(longitude);

                    //if ($(obj).find('[id*=hfClinicLatitude]')[0] != undefined)
                    //$(obj).find('[id*=hfClinicLatitude]').val(latitude);
                    //if ($(obj).find('[id*=hfClinicLongitude]')[0] != undefined)
                    //$(obj).find('[id*=hfClinicLongitude]').val(longitude);
                }
            });
        }
        function addTime(oldTime) {
            var time = oldTime.split(":");
            var hours = time[0];
            var ampm = time[1].substring(2, 4);
            var minutes = time[1].substring(0, 2);
            var time_new = '';
            if (+minutes >= 30) {
                hours = (+hours + 1) % 24;
            }
            minutes = (+minutes + 30) % 60;
            if (hours >= 12) {
                time_new = hours - 12 + ':' + minutes + 'pm';
            }
            else
                time_new = hours + ':' + minutes + ampm;
            return time_new;
        }

        function showMaintainContactLogWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "Reset contact status",
                    buttons: {
                        'Maintain Status': function () {
                            __doPostBack("maintainContactStatus", 1);
                            $(this).dialog('close');
                        },
                        'Contact Client': function () {
                            __doPostBack("maintainContactStatus", 0);
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function removeClinicAgreement() {
            var warning_mesg = '<%=(string)GetGlobalResourceObject("errorMessages", "deleteClinicAgreementWarning") %>';
            $(function () {
                $("#divConfirmDialog").html(warning_mesg);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: "Delete Community Off-site Agreement",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("deleteClinicAgreement");
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
            return false;
        }

        function showStoreChangeWithContactStatusWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "Store change with contact status warning",
                    buttons: {
                        'Ok': function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function disableAddLocations() {
            var is_disable = false;
            if ($("#hfDisableAddLocations").val() != "") {
                var store_message = $("#hfDisableAddLocations").val();
                is_disable = true;
                alert(store_message);
            }

            return !is_disable;
        }        
        function showInformationMessage(alert, handler) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 425,
                    title: "IMPORTANT REMINDER",
                    buttons: {
                            'Ok': function () {
                                __doPostBack(handler, 1);
                                $(this).dialog('close');
                            },
                            'Cancel': function () {
                                $(this).dialog('close');
                            }
                    },
                    modal: true
                });
            });
            return false;
        }
        function showHideVoucherDetails(ctrl) {
            var ctl_id = ctrl.replace("chkNoClinic", "");
            if ($("#" + ctrl + "").attr('checked') == 'checked') {
               $("#<%= grdLocations.ClientID %> tr[id^='" + ctl_id + "'] .clinicFields").hide();
                $("#<%= grdLocations.ClientID %> tr[id^='" + ctl_id + "'] .voucherFields").show();
                $("#" + ctl_id + "lblConfirmedClientName").html("Name of Individual <br />You Provided Vouchers To:");
                //$("#" + ctl_id + "txtConfirmedClientName")[0].title.replace("Name of Client Individual Who Confirmed the Clinic", "Name of Individual You Provided Vouchers To");
            } else {
               $("#<%= grdLocations.ClientID %> tr[id^='" + ctl_id + "'] .clinicFields").show();
                $("#<%= grdLocations.ClientID %> tr[id^='" + ctl_id + "'] .voucherFields").hide();
                $("#" + ctl_id + "lblConfirmedClientName").html("Name of Client Individual<br />Who Confirmed the Clinic:");
                //$("#" + ctl_id + "txtConfirmedClientName")[0].title.replace("Name of Individual You Provided Vouchers To", "Name of Client Individual Who Confirmed the Clinic");
            }
        }

        function navigateForm(value) {
                window.open("walgreensDownloadPDF.aspx", '_blank');
                return false;
        }
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdMaxClinicNumber" runat="server" />
    <asp:HiddenField ID="hfBusinessClinicPk" runat="server" />
    <asp:HiddenField ID="hfContactLogPk" runat="server" />
    <asp:HiddenField ID="hfBusinessStoreId" runat="server" />
    <asp:HiddenField ID="hfIsPreviousSeasonLog" runat="server" />
    <asp:HiddenField ID="hfContractedStoreId" runat="server" />
    <asp:HiddenField ID="hfScheduledDate" runat="server" />
    <asp:HiddenField id="hfIsMoPrevious" runat="server" Value="false" />
    <asp:HiddenField ID="hfDisableAddLocations" runat="server" />
    <asp:HiddenField ID="hfCopayGroupIdFlu" runat="server" />
    <asp:HiddenField ID="hfCopayGroupIdRoutine" runat="server" />
    <asp:HiddenField ID="hfOutReachBusinessPk" runat="server" />
    <asp:HiddenField ID="hfRegionNumber" runat="server" />
    <asp:HiddenField ID="hfIsCancelled" runat="server" />        
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="center" style="padding-top: 24px; padding-bottom: 24px">
                <table width="935" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="padding: 12px">
                            <table width="935" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="logTitle" align="left">Clinic Details</td>
                                    <td align="right">
                                        <asp:ImageButton ID="imgbtnContractPageNav" runat="server" ImageUrl="~/images/btn_view_agreement.png" OnClick="lnkContractPageNav_Click" />&nbsp;
                                        <asp:ImageButton ID="imgbtnRemoveClinicAgreement" runat="server" ImageUrl="~/images/btn_delete_agreement.png" OnClientClick="javascript:return removeClinicAgreement();" />
                                        <asp:ImageButton ID="imgbtnModifyAgreement" runat="server" ImageUrl="~/images/btn_modify_agreement.png" OnClick="imgbtnModifyAgreement_Click" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table width="935" border="0" cellspacing="8" cellpadding="0">
                                <tr>
                                    <td colspan="2" class="formFields" style="text-align: left; vertical-align: middle">  
                                        <%--<asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="UpdateClinic" />
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="ConfirmClinic" />
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="CompleteClinic" />
                                        <asp:ValidationSummary ID="ValidationSummary4" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="CancelledClinic" />--%>   
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <table width="938" border="0" cellspacing="12" cellpadding="0" bgcolor="#d7ecc1" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="3" class="subTitle" align="left">
                                                    Store Information
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="125" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">
                                                    Assigned Store:
                                                </td>
                                                <td width="275" valign="middle" align="left">
                                                    <asp:TextBox ID="txtDefaultClinicStoreId" runat="server" Enabled="false" CssClass="formFields" Width="100px" MaxLength="5" TabIndex="1"></asp:TextBox>
                                                </td>
                                                <td width="438" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">
                                                    <asp:CheckBox ID="chkReassignClinicPrimary" runat="server" Text="Re-assign based on geography" TabIndex="2" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="left" class="formFields">
                                                    *Clinics will be re-assigned to another store if the clinic location is over 10 miles from the contracting store or crosses state boundaries.
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <table width="938" border="0" cellspacing="12" cellpadding="0" bgcolor="#c1dfec" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="4" class="subTitle" align="left">
                                                    Business Information
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="125" style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Client Name:</td>
                                                <td width="275" style="text-align: left; vertical-align: top;" class="formFields" ><b><asp:Label ID="lblClientName" runat="server" CssClass="formFields"></asp:Label></b></td>
                                                <td width="148" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">&nbsp;</td>
                                                <td width="280" valign="middle">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="125" style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact First Name:</td>
                                                <td width="275" style="text-align: left; vertical-align: top;">
                                                    <asp:TextBox ID="txtFirstContactName" runat="server" TabIndex="3" Width="200" CssClass="formFields" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact Phone #:</td>
                                                <td style="text-align: left; vertical-align: top;">
                                                    <asp:TextBox ID="txtLocalContactPhonePrimary" runat="server" CssClass="formFields" Width="200" MaxLength="14" onblur="textBoxOnBlur(this);" TabIndex="5"></asp:TextBox>                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="125"style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact Last Name:</td>
                                                <td style="text-align: left; vertical-align: top;" >
                                                    <asp:TextBox ID="txtLastContactName" runat="server" TabIndex="4" Width="200" CssClass="formFields" MaxLength="50"></asp:TextBox>                                                    
                                                </td>
                                                <td style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact Email:</td>
                                                <td style="text-align: left; vertical-align: top;">
                                                    <asp:TextBox ID="txtLocalContactEmailPrimary" runat="server" CssClass="formFields" Width="200" MaxLength="256" TabIndex="6"></asp:TextBox>                                                                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="125" style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Contact Job Title:</td>
                                                <td style="text-align: left; vertical-align: top;">
                                                    <asp:TextBox ID="txtContactJobTitle" runat="server" TabIndex="7" Width="200" CssClass="formFields" MaxLength="100"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">&nbsp;</td>
                                                <td valign="middle">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="938" border="0" cellspacing="12" cellpadding="0" bgcolor="#f2eebb" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="4" align="left" class="formFields">
                                                    <asp:Label ID="lblClinicDateAlert" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td colspan="4" align="left" class="subTitle">Billing &amp; Vaccine Information</td>
                                            </tr>
                                            <tr>
                                                <td width="125" style="vertical-align: top; text-align: left" nowrap="nowrap" class="logSubTitles">Plan ID:</td>
                                                <td width="300" style="vertical-align: top; text-align: left" >
                                                    <asp:TextBox ID="txtPlanId" MaxLength="256" runat="server" TabIndex="8" Width="350px" CssClass="formFields" Visible="false"></asp:TextBox>
                                                    <asp:Label ID="lblPlanId" runat="server" CssClass="formFields"></asp:Label>
                                                </td>
                                                <td width="105"  style="vertical-align: top; text-align: left" nowrap="nowrap" class="logSubTitles">Group ID:</td>
                                                <td style="vertical-align: top; text-align: left"  >
                                                    <asp:TextBox ID="txtGroupId" MaxLength="256" runat="server" TabIndex="9" TextMode="MultiLine" Rows="4" Columns="45" Width="200px" CssClass="formFields" Visible="false"></asp:TextBox>
                                                    <asp:Label ID="lblGroupId" runat="server" CssClass="formFields"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="125" style="text-align: left; vertical-align: top;" class="logSubTitles">ID Recipient:<br />
                                                    <span style="font-weight: normal; font-size: 11px;">(Cash and Corporate<br />to Invoice Claims)</span>
                                                </td>
                                                <td width="300" style="text-align: left; vertical-align: top;" >
                                                    <asp:TextBox ID="txtIdRecipient" CssClass="formFields" runat="server" Width="350px" TabIndex="10" Visible="false" MaxLength="256"></asp:TextBox>
                                                    <asp:Label ID="lblIdRecipient" runat="server" CssClass="formFields"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <table width="100%" id="tblCopayGroupId" runat="server" visible="false">
                                                        <tr id="trCoPayFlu" runat="server" visible="false">
                                                            <td  width="105"  style="vertical-align: top; text-align: left"  class="logSubTitles">Copay Group ID (Flu):</td>
                                                            <td style="vertical-align: top; text-align: left" >
                                                                <asp:TextBox ID="txtCopyGroupIdFlu"  runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="trCoPayRoutine" runat="server" visible="false">
                                                            <td  width="105"  style="vertical-align: top; text-align: left"  class="logSubTitles">Copay Group ID (Routine):</td>
                                                            <td style="vertical-align: top; text-align: left" >
                                                                <asp:TextBox ID="txtCopyGroupIdRoutine"  runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: left; vertical-align: top;" nowrap="nowrap" >
                                                    <asp:GridView ID="grdImmunizationChecks" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" onrowdatabound="grdImmunizationChecks_RowDataBound" >
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Immunization" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="clinicDetailsHeader" ItemStyle-CssClass="clinicDetailsRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="200px" ItemStyle-Wrap="true" >
                                                            <ItemTemplate>
                                                                <table id="tblImmunization" width="200px" border="0">
                                                                    <tr>
                                                                        <td colspan="2" valign="top" style="max-width:175px;" >
                                                                            <asp:Label ID="lblImmunizationCheck" runat="server" Text='<%# Bind("immunizationName") %>' CssClass="wrapword" Width="200px" ></asp:Label>
                                                                            <asp:Label ID="lblImmunizationPk" runat="server" Text='<%# Bind("immunizationPk") %>' Visible="false"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                 </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Rates" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="clinicDetailsHeader" ItemStyle-CssClass="clinicDetailsRow" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <table id="tblPrice" width="100%" border="0">
                                                                    <tr>
                                                                        <td colspan="2" valign="top" style="text-align: right; ">
                                                                            <asp:Label ID="lblValue" runat="server" Text='<%# Bind("price") %>' ></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Payment Method" HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="clinicDetailsHeader" ItemStyle-CssClass="clinicDetailsRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="30%">
                                                            <ItemTemplate>
                                                                <table id="tblPaymentTypes" width="100%" border="0">
                                                                    <tr>
                                                                    <td colspan="2" valign="top">
                                                                        <asp:Label ID="lblPaymentType" Text='<%# Bind("paymentTypeName") %>' runat="server" ></asp:Label>
                                                                        <asp:Label ID="lblPaymentTypeId" Text='<%# Bind("paymentTypeId") %>' runat="server" Visible="false"></asp:Label>
                                                                    </td>
                                                                    </tr>
                                                                    <tr id="rowSendInvoiceTo" runat="server" visible="false">
                                                                    <td colspan="2">
                                                                        <table width="100%" id="tblPaymentInfo" border="0">
                                                                            <tr><td colspan="2">Send Invoice To:</td></tr>
                                                                            <tr><td>Name:</td>
                                                                                <td><asp:TextBox ID="txtPmtName" Text='<%# Bind("name") %>' CssClass="formFields" runat="server" MaxLength="256" Enabled="false"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Address1:</td>
                                                                                <td><asp:TextBox ID="txtPmtAddress1" Text='<%# Bind("address1") %>' CssClass="formFields" runat="server" MaxLength="256" Enabled="false"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Address2:</td>
                                                                                <td><asp:TextBox ID="txtPmtAddress2" Text='<%# Bind("address2") %>' CssClass="formFields" runat="server" MaxLength="256" Enabled="false"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>City:</td>
                                                                                <td><asp:TextBox ID="txtPmtCity" Text='<%# Bind("city") %>' CssClass="formFields" runat="server" MaxLength="100" Enabled="false"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>State:</td>
                                                                                <td><asp:Label ID="lblPmtState" Text='<%# Bind("state") %>' runat="server"  CssClass="formFields" Width="95%" Enabled="false" ></asp:Label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Zip Code:</td>
                                                                                <td><asp:TextBox ID="txtPmtZipCode" Text='<%# Bind("zip") %>' CssClass="formFields" runat="server" MaxLength="5" Enabled="false"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Phone:</td>
                                                                                <td><asp:TextBox ID="txtPmtPhone" Text='<%# Bind("phone") %>' CssClass="formFields" runat="server" MaxLength="14" Enabled="false"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email:</td>
                                                                                <td><asp:TextBox ID="txtPmtEmail" Text='<%# Bind("email") %>' CssClass="formFields" runat="server" MaxLength="256" Enabled="false"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">Is Employer Tax Exempt?
                                                                                    <asp:DropDownList ID="ddlTaxExempt" runat="server" CssClass="formFields" Enabled="false">
                                                                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">Will Patient Pay a Portion of the Cost - Is There a Copay?<br />
                                                                                    <asp:DropDownList ID="ddlIsCopay" runat="server" CssClass="formFields" Enabled="false">
                                                                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <asp:Label ID="lblDollar" runat="server" Text="$"></asp:Label> <asp:TextBox CssClass="formFields" ID="txtCoPay" Text='<%# Bind("copayValue") %>' runat="server" MaxLength="8" Enabled="false"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    </tr>
                                                                    <tr id="rowVoucherNeeded" runat="server" visible="false">
                                                                    <td colspan="2">
                                                                        <table>
                                                                        <tr>
                                                                            <td>Voucher Needed:
                                                                                <asp:DropDownList ID="ddlVoucher" CssClass="formFields" runat="server" Enabled="false">
                                                                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowExpirationDate" runat="server">
                                                                            <td>Expiration Date:
                                                                                <asp:TextBox ID="txtVaccineExpirationDate" runat="server" CssClass="formFields" Width="100px" Enabled="false"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        </table>
                                                                    </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Est Vol" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="clinicDetailsHeader" ItemStyle-CssClass="clinicDetailsRow" ItemStyle-VerticalAlign="Top" ItemStyle-Width="15%">
                                                            <ItemTemplate>
                                                                <table id="tblImmunization" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="vertical-align: top; text-align: right; ">
                                                                            <asp:TextBox ID="txtEstimatedQuantity" runat="server" Text='<%# Bind("estimatedQuantity") %>' align="right" CssClass="inputFieldRightAlign" Width="75" MaxLength="5"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Total Administered" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="clinicDetailsHeader" ItemStyle-CssClass="clinicDetailsRow" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Top" ItemStyle-Width="20%">
                                                            <ItemTemplate>
                                                                <table id="tblImmunization" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="vertical-align: top; text-align: right">
                                                                            <asp:TextBox ID="txtTotalImmAdministered" runat="server" Text='<%# Bind("totalImmAdministered") %>' CssClass="inputFieldRightAlign" Width="75" MaxLength="5"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                 </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: left; vertical-align: top; padding-top: 10px;" nowrap="nowrap" class="logSubTitles">
                                                    Additional Comments/Instructions:<br />
                                                    <asp:TextBox ID="txtComments" CssClass="formFields" runat="server" MaxLength="500" Width="100%" TextMode="MultiLine" Rows="5" Columns="45" Height="60px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                   <td align="center" style="padding-bottom: 24px">
                                    <table width="938" border="0" cellspacing="0" cellpadding="0" >
                                    <tr>
                                        <td align="center" >
                                            <asp:GridView ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None"  Width="100%" OnRowDataBound="grdLocations_RowDataBound" DataKeyNames="naClinicState,clinicDate,clinicScheduledOn,fluExpiryDate,routineExpiryDate">
                                                <Columns>
                                                    <asp:BoundField DataField="naClinicState" Visible="false" />
                                                    <asp:BoundField DataField="clinicDate" Visible="false" />
                                                    <asp:BoundField DataField="clinicScheduledOn" Visible="false" />
                                                    <asp:BoundField DataField="fluExpiryDate" Visible="false" />
                                                    <asp:BoundField DataField="routineExpiryDate" Visible="false" />
                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="100%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hfClinicLatitude" runat="server" Value='<%# Bind("clinicLatitude") %>' />
                                                            <asp:HiddenField ID="hfClinicLongitude" runat="server" Value='<%# Bind("clinicLongitude") %>' />
                                                            <table width="938" border="0" cellspacing="0" cellpadding="5" bgcolor="#f0debc" style="border: 1px solid #999;">
                                                                <tr id="rowAddClinicLocation" runat="server">
                                                                    <td style="text-align: left; vertical-align: top; font-family: Arial, Helvetica, Sans-Serif; font-size: 14px; font-weight: bold; padding-bottom: 10px; padding-top: 8px;" colspan="2" >
                                                                        Clinic Information
                                                                    </td>
                                                                    <td style="text-align: right; vertical-align: top; padding-bottom: 10px; padding-top: 8px;" colspan="2">
                                                                        <asp:DropDownList ID="ddlClinicLocations" runat="server" onchange ="disableAddBtnForMOPrevious()" CssClass="formFields" style="width:120px; text-align:right;" Visible="false" ></asp:DropDownList>&nbsp;&nbsp;
                                                                        <asp:ImageButton ID="imgBtnAddLocation" ImageAlign="Right" CausesValidation="false" runat="server" AlternateText="Add Clinic Location" 
                                                                                ImageUrl="images/btn_add_location.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_location.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_location_lit.png');" OnClientClick="return disableAddLocations()"  OnClick="btnAddLocation_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="rowIsNoClinic" runat="server">
                                                                    <td style="text-align: left; vertical-align: top; font-family: Arial, Helvetica, Sans-Serif; font-size: 14px; font-weight: bold; padding-bottom: 10px; padding-top: 8px;" colspan="2">
                                                                        <asp:CheckBox ID="chkNoClinic" runat="server"  CssClass="formFields" Text="No Clinic (Voucher Distribution Only)" Checked='<%# Convert.ToInt32(Eval("isNoClinic")) == 1 && this.isCTIExists ? true : false %>' 
                                                                            Enabled='<%# this.isCTIExists %>' OnCheckedChanged="chkNoClinic_CheckedChanged" AutoPostBack="true"/>
                                                                    </td>

                                                                    <td style="text-align: right; vertical-align: top; padding-bottom: 10px; padding-top: 8px;" id="cellDownloadBtns" runat="server"  colspan="2">
                                                                        <asp:ImageButton ID="imgBtnVarEng"  CausesValidation="false" CssClass="formatButtonList" runat="server" AlternateText="VAR Form English" 
                                                                                ImageUrl="images/btn_download_VAR.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_download_VAR.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_download_VAR_lit.png');"
                                                                                CommandArgument="VarEn" OnCommand="download_Click"/>
                                                                    
                                                                        <asp:ImageButton ID="imgBtnFluVoucher"  CssClass="formatButtonList" CausesValidation="false" runat="server" AlternateText="Flu Voucher" 
                                                                                ImageUrl="images/btn_download_flu_voucher.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_download_flu_voucher.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_download_flu_voucher_lit.png');"
                                                                                CommandArgument="FluEn" OnCommand="download_Click"/>
                                                                                
                                                                        <asp:ImageButton ID="imgBtnRoutineVoucher"  CssClass="formatButtonList" CausesValidation="false" runat="server" AlternateText="Routine Voucher" 
                                                                                ImageUrl="images/btn_download_enhanced_voucher.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_download_enhanced_voucher.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_download_enhanced_voucher_lit.png');"
                                                                                CommandArgument="RoutineEn" OnCommand="download_Click"/>
                                                                                
                                                                        <br />
                                                                        
                                                                         <asp:ImageButton ID="imgBtnVarSpa"  CausesValidation="false" CssClass="formatButtonList" runat="server" AlternateText="VAR Form Spanish" 
                                                                                ImageUrl="images/btn_download_VAR_spa.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_download_VAR_spa.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_download_VAR_spa_lit.png');"
                                                                                CommandArgument="VarSp" OnCommand="download_Click"/>

                                                                        <asp:ImageButton ID="imgBtnFluVoucherSpa"  CssClass="formatButtonList" CausesValidation="false" runat="server" AlternateText="Flu Voucher Spanish" 
                                                                                ImageUrl="~/images/btn_download_flu_voucher_spa.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_download_flu_voucher_spa.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_download_flu_voucher_spa_lit.png');"
                                                                                CommandArgument="FluSp" OnCommand="download_Click"/>
                                                                                
                                                                         <asp:ImageButton ID="imgBtnRoutineVoucherSpa"  CssClass="formatButtonList" CausesValidation="false" runat="server" AlternateText="Routine Voucher Spanish" 
                                                                                ImageUrl="images/btn_download_enhanced_voucher_spa.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_download_enhanced_voucher_spa.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_download_enhanced_voucher_spa_lit.png');"
                                                                             CommandArgument="RoutineSp" OnCommand="download_Click"/>
                                                                                                                                               
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:left; vertical-align: top;" width="125" class="logSubTitles">Clinic Location:</td>
                                                                    <td width="275" style="text-align:left; vertical-align: top;" class="formFields">
                                                                        <b><asp:Label ID="lblClinicLocation" Text='<%# Bind("naClinicLocation") %>' runat="server" Width="100%"></asp:Label></b>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td style="text-align: right; vertical-align: top;">
                                                                        <asp:ImageButton ID="imgBtnRemoveLocation" ImageAlign="Right" CausesValidation="false" runat="server" AlternateText="Remove Clinic Location"
                                                                                ImageUrl="images/btn_remove.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');" 
                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" OnClick="imgBtnRemoveLocation_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:left; vertical-align: top;" width="125" class="logSubTitles">Contact First Name:</td>
                                                                    <td style="text-align:left; vertical-align: top;">
                                                                        <asp:TextBox ID="txtContactFirstName" runat="server" CssClass="formFields" Width="200" MaxLength="50" Text='<%# Bind("naContactFirstName") %>'></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align:left; vertical-align: top;"  width="163" class="logSubTitles">Contact Phone:</td>
                                                                    <td style="text-align:left; vertical-align: top;">
                                                                        <asp:TextBox ID="txtLocalContactPhone" runat="server" Text='<%# Bind("naClinicContactPhone") %>' CssClass="formFields" Width="200" MaxLength="14" onblur="textBoxOnBlur(this);"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles">Contact Last Name:</td>
                                                                    <td style="text-align:left; vertical-align: top;">
                                                                        <asp:TextBox ID="txtContactLastName" runat="server" CssClass="formFields" Width="200" MaxLength="50" Text='<%# Bind("naContactLastName") %>'></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles">Contact Email:</td>
                                                                    <td style="text-align:left; vertical-align: top;">
                                                                        <asp:TextBox ID="txtLocalContactEmail" runat="server" Text='<%# Bind("naContactEmail") %>' CssClass="formFields" Width="200" MaxLength="256"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <span id="rowStoreAssignment" runat="server">
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles">Assigned Store:</td>
                                                                    <td style="text-align:left; vertical-align: top;" width="275">
                                                                        <asp:TextBox ID="txtClinicStore" runat="server" CssClass="formFields" Width="100px" MaxLength="5" Text='<%# Bind("clinicStoreId") %>'></asp:TextBox>
                                                                    </td>
                                                                    </span>
                                                                    <span id="rowReAssignment" runat="server">
                                                                    <td style="text-align:left; vertical-align: top;" colspan="2" class="logSubTitles">
                                                                        <asp:Label ID="lblIsReassignClinic" runat="server" Visible="false" Text='<%# Bind("isReassign") %>'></asp:Label>
                                                                        <asp:CheckBox ID="chkReassignClinic" runat="server" Text="Re-assign based on geography*" />
                                                                    </td>
                                                                    </span>
                                                                </tr>
                                                                <tr id="rowClinicDate" runat="server">
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles">Clinic Address1:</td>
                                                                    <td style="text-align:left; vertical-align: top;">
                                                                        <asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("naClinicAddress1") %>' CssClass="formFields" Width="200" MaxLength="500" onblur="javascript:setClinicLatsLongs(this);"></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles clinicFields">Clinic Date:</td>
                                                                    <td class="clinicFields">
                                                                    <table border="0" cellpadding="0" cellspacing="0"><tr><td runat="server" id="tdPicker">
                                                                        <uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" />
                                                                        <asp:TextBox ID="txtCalenderFrom" runat="server" Visible="false" CssClass="formFields" Width="100px"></asp:TextBox>
                                                                        </td></tr></table>
                                                                    </td>
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles voucherFields" runat="server" Visible='<%# this.isFluImmunizationExists %>'>Voucher Flu Exp.:</td>
                                                                    <td class="voucherFields">
                                                                        <table border="0" cellpadding="0" cellspacing="0"><tr><td runat="server" id="td1">
                                                                            <uc1:PickerAndCalendar ID="pcFluExpiryDate" runat="server" Visible='<%# this.isFluImmunizationExists %>' />
                                                                                <asp:TextBox ID="txtFluExpiryDate" runat="server" Visible="False" CssClass="formFields" Width="100px"></asp:TextBox>
                                                                        </td></tr></table>
                                                                    </td>
                                                                </tr>
                                                                <tr id="rowClinicStartTime" runat="server">
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles">Clinic Address2:</td>
                                                                    <td style="text-align:left; vertical-align: top;">
                                                                        <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("naClinicAddress2") %>' CssClass="formFields" Width="200" MaxLength="50" onblur="javascript:setClinicLatsLongs(this);"></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles clinicFields">Clinic Start Time:</td>
                                                                    <td style="text-align:left; vertical-align: top;" class="clinicFields">
                                                                        <asp:TextBox ID="txtStartTime" runat="server" onpaste="return false" Text='<%# Bind("naClinicStartTime") %>' CssClass="formFields" Width="100px" MaxLength="7" OnLoad="displayTime_Picker"></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles voucherFields" runat="server" Visible='<%# this.isRoutineImmunizationExists %>'>Voucher Routine Exp.:</td>
                                                                    <td class="voucherFields">
                                                                        <table border="0" cellpadding="0" cellspacing="0"><tr><td runat="server" id="td2">
                                                                            <uc1:PickerAndCalendar ID="pcRoutineExpiryDate" runat="server" Visible='<%# this.isRoutineImmunizationExists %>' />
                                                                                <asp:TextBox ID="txtRoutineExpiryDate" runat="server" Visible="False" CssClass="formFields" Width="100px"></asp:TextBox>
                                                                        </td></tr></table>
                                                                    </td>   
                                                                </tr>
                                                                <tr id="rowClinicEndTime" runat="server">
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles">City:</td>
                                                                    <td style="text-align:left; vertical-align: top;">
                                                                        <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("naClinicCity") %>' CssClass="formFields" Width="200" MaxLength="100" onblur="javascript:setClinicLatsLongs(this);"></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align:left; vertical-align: top;" class="logSubTitles clinicFields">Clinic End Time:</td>
                                                                    <td style="text-align:left; vertical-align: top;" class="clinicFields">
                                                                        <asp:TextBox ID="txtEndTime" runat="server" onpaste="return false" Text='<%# Bind("naClinicEndTime") %>' CssClass="formFields" Width="100px" MaxLength="7"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="text-align:left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td style="text-align:left; vertical-align: top;" width="42%" >
                                                                                <table border="0" cellpadding="0" cellspacing="0"><tr><td>State:&nbsp;&nbsp;</td>
                                                                                    <td id="tdState"  runat="server"><asp:DropDownList ID="ddlState" runat="server" CssClass="formFields" Width="100px" onchange="javascript:setClinicLatsLongs(this);"   AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"></asp:DropDownList>
                                                                                </td></tr></table></td>
                                                                                <td style="text-align:left; vertical-align: top;" width="58%">Zip Code:&nbsp;&nbsp;
                                                                                    <asp:TextBox ID="txtZipCode" runat="server" CssClass="formFields" Width="100px" Text='<%# Bind("naClinicZip") %>' MaxLength="5" onblur="javascript:setClinicLatsLongs(this);"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td colspan="2" style="text-align:left; vertical-align: top;" class="logSubTitles">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblConfirmedClientName" runat="server">
                                                                          <tr>
                                                                            <td style="text-align:left; vertical-align: top;" width="34%" id="lblConfirmedClientName" >Name of Client Individual<br />Who Confirmed the Clinic:</td>
                                                                            <td style="text-align:left; vertical-align: top;" width="66%">
                                                                                <asp:TextBox ID="txtConfirmedClientName" runat="server" Text='<%# Bind("confirmedClientName") %>' CssClass="formFields" Width="200px" MaxLength="100"></asp:TextBox>                                                                                
                                                                            </td>
                                                                          </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr id="rowClinicImmunizations" runat="server" >
                                                                    <td colspan="5">
                                                                        <asp:GridView ID="grdClinicImmunizations" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" >
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblImmunizationPk" runat="server" Text='<%# Bind("pk") %>' Visible="false"></asp:Label>
                                                                                    <asp:Label ID="lblPaymentTypeId" runat="server" Text='<%# Bind("paymentTypeId") %>' Visible="false"></asp:Label>
                                                                                    <b class="formFields">Estimated <asp:Label ID="lblImmunizationCheck" runat="server" Text='<%# Bind("immunizationName") %>' CssClass="formFields" ></asp:Label> Shots:<b/>
                                                                                    <asp:TextBox ID="txtEstimatedQuantity" runat="server" Text='<%# Bind("estimatedQuantity") %>' CssClass="inputFieldRightAlign" MaxLength="5" Width="75"></asp:TextBox><br />
                                                                                    (<asp:Label ID="lblPaymentType" runat="server" Text='<%# Bind("paymentTypeName") %>' CssClass="formFields" Font-Bold="false" ></asp:Label>)
																					<asp:TextBox ID="txtTotalImmAdministered" runat="server" CssClass="inputFieldRightAlign" Width="75" MaxLength="5" Visible="false"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr id="rowClinicDateAlert" runat="server" visible="false" >
                                                                    <td colspan="5" class="formFields">
                                                                        <asp:Label ID="lblClinicDateAlert" runat="server" ForeColor="Red"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <br />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                   </table>
                                  </td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding-bottom: 24px">
                                        <table width="938" border="0" cellspacing="12" cellpadding="0" bgcolor="#f3d8d9" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="4" class="subTitle" style="text-align:left; vertical-align: top;">Pharmacist &amp; Post Clinic Information</td>
                                            </tr>
                                            <tr>
                                                <td width="125" style="text-align:left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Name of Rx Host:</td>
                                                <td width="275" style="text-align:left; vertical-align: top;" class="formFields">
                                                    <asp:TextBox ID="txtRxHost" runat="server" CssClass="formFields" Width="200" MaxLength="100"></asp:TextBox>
                                                </td>
                                                <td width="125" style="text-align:left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Total Hours Clinic Held:</td>
                                                <td style="text-align:left; vertical-align: top;">
                                                    <asp:TextBox ID="txtTotalHoursClinicHeld" runat="server" CssClass="formFields" MaxLength="5"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Phone #:</td>
                                                <td style="text-align:left; vertical-align: top;" class="formFields" >
                                                    <asp:TextBox ID="txtRxHostPhone" runat="server" Width="200" MaxLength="14" CssClass="formFields" onblur="textBoxOnBlur(this);"></asp:TextBox>
                                                </td>
                                                <td width="125" style="text-align:left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">Feedback/Notes:</td>
                                                <td width="275" style="text-align:left; vertical-align: top;" class="formFields">
                                                    <asp:TextBox ID="txtFeedBack" runat="server" TextMode="MultiLine" Columns="30" CssClass="formFields" Rows="5" MaxLength="256"></asp:TextBox>
                                                    <table align="left" width="85%" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left"><span style="font-size: x-small">*<asp:Label ID="idLabel" runat="server"></asp:Label> 
                                                                    characters allowed</span></td>
                                                                <td align="right" class="logSubTitles"><asp:Label ID="idCount" runat="server"></asp:Label></td>
                                                            </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                     <td colspan="2" bgcolor="#FFFFFF" align="center" style="padding-bottom: 20px">
                                        <%--<asp:ImageButton ID="btnConfirmedClinicDim" ImageUrl="~/images/btn_confirmed_clinic_dim.png" CausesValidation="false" runat="server" AlternateText="Confirmed Clinic" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnConfirmedClinicDim" Text="Confirmed Clinic" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Confirmed Clinic" OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnConfirmedClinic" ImageUrl="~/images/btn_confirmed_clinic.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_confirmed_clinic.png');"
                                             CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_confirmed_clinic_lit.png');"
                                             runat="server" AlternateText="Confirmed Clinic" CommandArgument="Confirmed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('ConfirmClinic');" />&nbsp;--%>
                                        <asp:LinkButton ID="btnConfirmedClinic" Text="Confirmed Clinic" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Confirmed Clinic" 
                                            CommandArgument="Confirmed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('ConfirmClinic',this);" />&nbsp;
                                        <%--<asp:ImageButton ID="btnClinicCompletedDim" ImageUrl="~/images/btn_clinic_completed_dim.png" CausesValidation="false" runat="server" AlternateText="Clinic Completed" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnClinicCompletedDim" Text="Clinic Completed" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Clinic Completed" OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnClinicCompleted" ImageUrl="~/images/btn_clinic_completed.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_clinic_completed.png');"
                                             CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_clinic_completed_lit.png');"
                                             runat="server" AlternateText="Clinic Completed" CommandArgument="Completed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('CompleteClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnClinicCompleted" Text="Clinic Completed" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Clinic Completed" 
                                            CommandArgument="Completed" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('CompleteClinic',this);"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnCancelClinicDim" ImageUrl="~/images/btn_cancel_clinic_dim.png" CausesValidation="false" runat="server" AlternateText="Cancelled Clinic" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnCancelClinicDim" Text="Cancel Clinic" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Cancelled Clinic" OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnCancelClinic" ImageUrl="~/images/btn_cancel_clinic.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_clinic.png');" 
                                             CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_clinic_lit.png');"
                                             runat="server" AlternateText="Cancelled Clinic" CommandArgument="Cancelled" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('CancelledClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnCancelClinic" Text="Cancel Clinic" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Cancelled Clinic" 
                                            CommandArgument="Cancelled" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('CancelledClinic',this);"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnSubmit" ImageUrl="~/images/btn_submit_changes.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_changes.png');"
                                            CausesValidation="true" ValidationGroup="UpdateClinic" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_changes_lit.png');"
                                            runat="server" AlternateText="Submit" CommandArgument="Submit" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnSubmit" Text="Submit Changes" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Submit" 
                                            CommandArgument="Submit" OnCommand="doProcess_Click" OnClientClick="return CustomValidatorForLocations('UpdateClinic',this);"/>
                                        <asp:LinkButton ID="btnSubmitDim" Text="Submit Changes" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Submit" 
                                            OnClientClick="javascript: return false;" Visible="false"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnCancel" ImageUrl="~/images/btn_return_home.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_return_home.png');"
                                            CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_return_home_lit.png');"
                                            runat="server" AlternateText="Cancel" CommandArgument="Cancel" OnCommand="doProcess_Click"/>--%>
                                        <asp:LinkButton ID="btnCancel" Text="Return To Home" CssClass="wagButton" CausesValidation="false" runat="server" AlternateText="Cancel" CommandArgument="Cancel" OnCommand="doProcess_Click"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" bgcolor="#FFFFFF" align="center"  style="padding-bottom: 20px">
                                        <table width="935" border="0" cellspacing="12" cellpadding="0" bgcolor="#eeeeee" style="border: 1px solid #999;">
                                            <tr>
                                                <td width="400" colspan="4" class="subTitle" align="left">History Log</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="grdClinicUpdatesHistory" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdClinicUpdatesHistory_OnRowDataBound">
                                                         <Columns>
                                                             <asp:TemplateField HeaderText="">
                                                                <HeaderTemplate>
                                                                <tr>
                                                                    <td width="12%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Date</td>
                                                                    <td width="13%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Action</td>
                                                                    <td width="30%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Updated Field</td>
                                                                    <td width="30%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Update</td>
                                                                    <td width="15%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">User</td>
                                                                </tr>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                <tr>  
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblHistorydate" runat="server" Text='<% #Bind("updatedOn", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblAction" runat="server" Text='<%#Bind("updateAction") %>'></asp:Label>
                                                                    </td>
                                                                    <td id="tdClinicUpdateField" runat="server" class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdatedField" runat="server" Text='<%#Bind("updatedField") %>'></asp:Label>
                                                                    </td>
                                                                    <td id="tdClinicUpdateValue" runat="server" class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdate" runat="server" Text='<%#Bind("updatedValue") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Bind("updatedBy") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                </ItemTemplate>
                                                             </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>  
                                                           <table border="0" width="100%" >
                                                                <tr>
                                                                    <td width="12%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Date</td>
                                                                    <td width="13%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Action</td>
                                                                    <td id="tdClinicUpdateField" runat="server" width="18%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Updated Field</td>
                                                                    <td id="tdClinicUpdateValue" runat="server" width="36%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Update</td>
                                                                    <td width="21%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">User</td>
                                                                </tr>
                                                                <tr>  
                                                                    <td colspan="5" class="clinicDetailsRow" style="text-align: center; vertical-align: middle; height:25px; ">No Data Found</td>
                                                                </tr>
                                                            </table> 
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div id="divConfirmDialog" style="display: none; font-family: Arial; font-size: 12px; text-align: left; font-weight: normal;"></div>
            </td>
        </tr>
    </table>    
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
