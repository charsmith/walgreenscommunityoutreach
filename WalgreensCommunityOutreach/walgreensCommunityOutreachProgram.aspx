﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensCommunityOutreachProgram.aspx.cs" Inherits="walgreensCommunityOutreachProgram" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="uc2" %>
<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link rel="stylesheet" type="text/css" href="css/wags.css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css" />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css" />

    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/jquery.printelement.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />

    <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>

    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
    <style type="text/css">
        .ui-dialog-titlebar-close {
            visibility: hidden;
        }

        .ui-timepicker-list li {
            color: #000000;
            font-family: "Times New Roman",Times,serif;
            font-size: 11px;
        }

        .ui-widget-header {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 13px;
            font-weight: bold;
        }

        .ui-widget {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
            font-weight: normal;
        }

        .tooltipText {
            font-weight: bold;
            font-size: 11px;
            color: red;
            position: absolute;
            white-space: pre-wrap;
            max-width: 180px;
            padding: 2px;
            border-radius: 0px;
            -webkit-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            -moz-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            //Disable copay textbox when isCopay selected is either No or none
            $("#grdImmunizationChecks").find('select[id$=ddlIsCopay]').each(function () {
                enableCoPay($(this)[0]);
            });


            if ($.browser.msie) {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keydown(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keydown(function (event) { event.preventDefault(); });
            }
            else {
                $("#grdLocations").find("input[type=text][id*=txtStartTime]").keypress(function (event) { event.preventDefault(); });
                $("#grdLocations").find("input[type=text][id*=txtEndTime]").keypress(function (event) { event.preventDefault(); });
            }
            $("#<%= grdLocations.ClientID %> input[id*='rbtnOutreachType']").each(function () {
                setTxtOthers($(this));
            });
            $("#<%= grdLocations.ClientID %> input[id*='rbtnOutreachType']").click(function () {
                setTxtOthers($(this));
            });
        });
        function enableCoPay(e) {
            var txtCoPay = e.id;
            txtCoPay = txtCoPay.replace("ddlIsCopay", "txtCoPay");
            if ($(e).val() == "Yes") {
                $("#" + txtCoPay).removeAttr('disabled');
            }
            else {
                $("#" + txtCoPay).attr('disabled', 'disabled');
                $("#" + txtCoPay).val("");
            }

            if ($('#txtwalgreenname').is(':disabled') == true) {
                $("#" + txtcopay).attr('disabled', 'disabled');
            }
        }

        function displayPaymentMethods(selected_imm_id) {
            paymentMethods = $.parseJSON('<%= this.paymentMethods %>');

            if ($("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').length > 0) {
                $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').find("option").remove();
                $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').append('<option value=""><%= GetLocalResourceObject("SelectPaymentType").ToString() %></option>');
            }

            $.each(paymentMethods, function (key, val) {
                if (val["immunizationId"] == selected_imm_id && val["isPaymentSelected"] == "False")
                    $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').append('<option value="' + val["paymentTypeId"] + '">' + val["<%= GetLocalResourceObject("PaymentTypeNameKey").ToString() %>"] + '</option>');
            });
        }
        var setInvalidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "background-color": "yellow" });

            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.tooltip({
                tooltipClass: "tooltipText"
            });
            ctrl.focus(function (evt) {
                $(evt.currentTarget).tooltip("close");
            });
            ctrl.tooltip("enable");
        }
        var setValidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "border": "1px solid gray" });
            ctrl.css({ "background-color": "" });
            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.removeAttr("title");
            ctrl.tooltip({
                disabled: true
            });
            ctrl.on("click", function () {
                ctrl.data("title", ctrl.attr("title")).removeAttr("title");
            }, function () {
                var title = ctrl.data("title");
                ctrl.tooltip("option", "content", title || ctrl.attr("title"));
            });

        }
        function validateImmSelection() {
            if ($("#grdImmunizationChecks").find('select[id$=ddlImmunizationCheck]').length > 0 && $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').length > 0) {
                if ($("#grdImmunizationChecks").find('select[id$=ddlImmunizationCheck]').val() == "" || $("#grdImmunizationChecks").find('select[id$=ddlPaymentType]').val() == "") {
                    alert('<%= GetLocalResourceObject("SelectPaymentType").ToString() %>');
                    return false;
                }
            }
        }

        function validateImmunizationPayments() {
            //var isLinkVisible = $("#lnkAddImmunization").prop("disabled");
            var is_add_imm_disabled = $("#lnkAddImmunization").attr("disabled");
            if (is_add_imm_disabled != undefined) {
                if ($("#grdImmunizationChecks tr").length > 5) {
                    showImmConfirmationWarning('<%= GetLocalResourceObject("ImmunizationSelectionValidationMessage1").ToString() %>', false);
                }
                else {
                    showImmConfirmationWarning('<%= GetLocalResourceObject("ImmunizationSelectionValidationMessage2").ToString() %>', true);
                }
                return false;
            }
        }

        function getBool(val) {
            var num = +val;
            return !isNaN(num) ? !!num : !!String(val).toLowerCase().replace(!!0, '');
        }

        function showImmConfirmationWarning(alert, hideClearAction) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    },
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "<%= GetLocalResourceObject("ImmunizationPaymentTypeConfirm").ToString() %>",
                    buttons: {
                        'Ok': function () {
                            $(this).dialog('close');
                            __doPostBack("confirmImmunization", 1);
                        },
                        'Clear': function () {
                            $(this).dialog('close');
                            __doPostBack("confirmImmunization", 0);
                        }
                    },
                    modal: true
                });
                if (getBool(hideClearAction))
                    $('.ui-dialog-buttonpane').find('button:last').css('visibility', 'hidden');
            });
        }
        function CustomValidatorForLocations(source, arguments) {
            var validating_fields = $("form :text, textarea, select");
            var validationGroup = source;
            var is_MO_state = $('#hfisMOState').val().toLowerCase() === 'true';
            var is_required = true;
            var validating_field_names = {
                //Payment type details 
                'txtEmails': { 'isRequired': is_required, 'when': 'always', 'name': 'Email', 'type': 'AgreementToEmails', 'requiredMessage': '<%= GetLocalResourceObject("EmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("EmailInvalid").ToString() %>' },
                'txtPmtName': { 'isRequired': is_required, 'when': 'always', 'name': 'Pmt Name', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("NameSendToIvoiceRequire").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("NameSendToIvoiceInvalid").ToString() %>' },
                'txtPmtAddress1': { 'isRequired': is_required, 'when': 'always', 'name': 'Address1', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("Address1Required").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("Address1Invalid").ToString() %>' },
                'txtPmtAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': '<%= GetLocalResourceObject("Address2Invalid").ToString() %>' },
                'txtPmtCity': { 'isRequired': is_required, 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("CityRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("CityInvalid").ToString() %>' },
                'txtPmtZipCode': { 'isRequired': is_required, 'when': 'always', 'name': 'Zip Code', 'type': 'zipcode', 'requiredMessage': '<%= GetLocalResourceObject("ZipCodeRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ZipCodeInvalid").ToString() %>' },
                'txtPmtPhone': { 'isRequired': is_required, 'when': 'always', 'name': 'Phone', 'type': 'phone', 'requiredMessage': '<%= GetLocalResourceObject("PhoneRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("PhoneInvalid").ToString() %>' },
                'txtPmtEmail': { 'isRequired': is_required, 'when': 'always', 'name': 'email', 'type': 'email', 'requiredMessage': '<%= GetLocalResourceObject("UserEmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("UserEmailInvalid").ToString() %>' },
                'txtPmtVerifyEmail': { 'isRequired': is_required, 'when': 'always', 'name': 'VeriyEmail', 'type': 'email', 'requiredMessage': '<%= GetLocalResourceObject("UserVerifyEmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("UserVerifyEmailInvalid").ToString() %>' },
                'txtCoPay': { 'isRequired': is_required, 'when': 'CopayYes', 'name': 'Copay', 'type': 'decimal', 'requiredMessage': '<%= GetLocalResourceObject("CopayRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("CopayInvalid").ToString() %>' },
                'ddlImmunizationCheck': { 'isRequired': is_required, 'when': 'always', 'name': 'Immunizations', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("ddlImmCheckRequired").ToString() %>', 'invalidMessage': '' },
                'ddlPaymentType': { 'isRequired': is_required, 'when': 'always', 'name': 'Payment Type', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("ddlPaymentTypeRequired").ToString() %>', 'invalidMessage': '' },
                'ddlPmtState': { 'isRequired': is_required, 'when': 'always', 'name': 'Pmt state', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("StateRequired").ToString() %>', 'invalidMessage': '' },
                'ddlIsCopay': { 'isRequired': is_required, 'when': 'always', 'name': 'Is copay', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("ddlCopayRequired").ToString() %>', 'invalidMessage': '' },
                'ddlTaxExempt': { 'isRequired': is_required, 'when': 'always', 'name': 'Tax Exempt', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("TaxExemptRequired").ToString() %>', 'invalidMessage': '' },


                //Clinic Location details
                'txtLocalContactName': { 'isRequired': is_required, 'when': 'always', 'name': 'Contact Name', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("LocalContactNameRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("LocalContactNameInvalid").ToString() %>' },
                'txtAddress1': { 'isRequired': is_required, 'when': 'always', 'name': 'Address1', 'type': 'address', 'requiredMessage': '<%= GetLocalResourceObject("Address1Required").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("Address1Invalid").ToString() %>' },
                'txtAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'address', 'requiredMessage': '', 'invalidMessage': '<%= GetLocalResourceObject("Address2Invalid").ToString() %>' },
                'txtCity': { 'isRequired': is_required, 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("CityRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("CityInvalid").ToString() %>' },
                'txtLocalContactPhone': { 'isRequired': is_required, 'when': 'always', 'name': 'Contact Phone', 'type': 'phone', 'requiredMessage': 'Phone is required', 'invalidMessage': 'Valid Phone number is required(ex: ###-###-####)' },
                'txtZipCode': { 'isRequired': is_required, 'when': 'always', 'name': 'ZipCode', 'type': 'zipcode', 'requiredMessage': '<%= GetLocalResourceObject("ZipCodeRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ZipCodeInvalid").ToString() %>' },
                'txtLocalContactEmail': { 'isRequired': is_required, 'when': 'always', 'name': 'Contact Email', 'type': 'email', 'requiredMessage': '<%= GetLocalResourceObject("UserEmailRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("UserEmailInvalid").ToString() %>' },
                'txtClinicImmunizationShots': { 'isRequired': is_required, 'when': 'always', 'name': 'Estimated shots', 'type': 'numeric', 'requiredMessage': '<%= GetLocalResourceObject("ClinicImmunizationShotsRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ClinicImmunizationShotsInvalid").ToString() %>' },
                'ddlState': { 'isRequired': is_required, 'when': 'always', 'name': 'state', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("StateRequired").ToString() %>', 'invalidMessage': '' },
                'txtStartTime': { 'isRequired': is_required, 'when': 'always', 'name': 'Start Time', 'type': '', 'requiredMessage': '<%= GetLocalResourceObject("StartTimeRequired").ToString() %>', 'invalidMessage': '' },
                'txtEndTime': { 'isRequired': is_required, 'when': 'always', 'name': 'End Time', 'type': '', 'requiredMessage': '<%= GetLocalResourceObject("EndTimeRequired").ToString() %>', 'invalidMessage': '' },
                'PickerAndCalendarFrom': { 'isRequired': is_required, 'when': 'always', 'name': 'Clinic Date', 'type': '', 'requiredMessage': '<%= GetLocalResourceObject("CliniDateRequired").ToString() %>', 'invalidMessage': '' },
                'rbtnOutreachType': { 'isRequired': is_required, 'when': 'always', 'name': 'Outreach Type', 'type': '', 'requiredMessage': '<%= GetLocalResourceObject("OutreachTypeRequired").ToString() %>', 'invalidMessage': '' },
                'txtOther': { 'isRequired': is_required, 'when': 'always', 'name': 'Outreach Type Description', 'type': 'junk', 'requiredMessage': 'Outreach Type Description is required', 'invalidMessage': 'Outreach Type Description: < > characters are not allowed' }
            };
            var first_identified_control = "";
            var is_valid = true;
            var ctrl_Id;
            var indexOf_Underscore;
            var txtPmtEmail;
            var trimmedValue;
            $.each(validating_fields, function (index, ctrl) {
                setValidControlCss($(ctrl), false);
                $(ctrl).attr({ "title": "" });
                ctrl_Id = $(ctrl).attr('id');

                if ((ctrl_Id.indexOf('grdImmunizationChecks') > -1 || ctrl_Id.indexOf('grdLocations') > -1 || ctrl_Id.indexOf('grdClinicImmunizations') > -1) && (ctrl_Id.lastIndexOf('_') > -1)) {

                    if (ctrl_Id.toLowerCase().indexOf('_picker') > -1) {
                        var pos = 0;
                        var count = 0;
                        while (true) {
                            pos = ctrl_Id.indexOf('_', pos + 1);
                            ++count;
                            if (count == 2) {
                                break;
                            }
                        }
                        ctrl_Id = ctrl_Id.substring(pos + 1, ctrl_Id.indexOf('_', pos + 1))
                    }
                    else {
                        indexOf_Underscore = ctrl_Id.lastIndexOf('_');
                        if (ctrl_Id.length > indexOf_Underscore + 1) {
                            ctrl_Id = ctrl_Id.substring(indexOf_Underscore + 1);
                        }
                    }


                }
                trimmedValue = $.trim($(ctrl).val());
                //storing for email comparison
                if (ctrl_Id == 'txtPmtEmail') {
                    txtPmtEmail = $(ctrl);
                }
                if ((validating_field_names[ctrl_Id] != undefined) && validating_field_names[ctrl_Id].isRequired &&
                    (validating_field_names[ctrl_Id].when == 'always' || $(ctrl).attr('disabled') != 'disabled') &&
                    trimmedValue == "" && $(ctrl).is(":visible")) {
                    if (validating_field_names[ctrl_Id].type == 'select') {
                        if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 6.") != -1) || (navigator.userAgent.indexOf("Trident") != -1)) {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                        else {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                    }
                    else {
                        setInvalidControlCss($(ctrl), false);
                        if (ctrl_Id == 'PickerAndCalendarFrom')
                            $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        else
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                    }
                    if (first_identified_control == '')
                        first_identified_control = $(ctrl);
                    is_valid = false;
                }

                else if ((validating_field_names[ctrl_Id] != undefined) && trimmedValue != "") {
                    if (!validateData(validating_field_names[ctrl_Id].type, trimmedValue, (validating_field_names[ctrl_Id].name == 'Estimated shots') ? false : true)) {
                        setInvalidControlCss($(ctrl), false);
                        if (ctrl_Id == 'PickerAndCalendarFrom')
                            $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                        else
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;
                    }
                    else if (validating_field_names[ctrl_Id].type == 'address') {
                        if ((!(validatePO(trimmedValue))) && ($(ctrl)[0].disabled == false)) {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": '<%= HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert") %>' });
                            if (first_identified_control == '')
                                first_identified_control = $(ctrl);
                            is_valid = false;
                        }
                        else
                            setValidControlCss($(ctrl), false);
                    }

                if (ctrl_Id == 'txtPmtVerifyEmail') {
                    if (trimmedValue != txtPmtEmail.val()) {
                        setInvalidControlCss($(ctrl), false);
                        $(ctrl).attr({ "title": '<%= GetLocalResourceObject("VerifyEmailMessage").ToString() %>' });
                        is_valid = false;
                    }
                }
            }
            });
    //Outreach Type Validations
    var is_rbtn_checked = false;
    $("#<%= grdLocations.ClientID %> input[id*='rbtnOutreachType']").parent().parent().each(function () {
        is_rbtn_checked = false;
        $.each($(this).find("input"), function (index, ctrl) {
           
            if ($(this)[0].checked) {
                is_rbtn_checked = true;
                return;
            }
        });
        if (is_rbtn_checked) {
                setValidControlCss($(this), false);
                $(this).attr({ "title": "" });
                $(this).css({ "border": "1px gray" });
        }
        else
        {
            setInvalidControlCss($(this), false);
            $(this).attr({ "title": '<%= GetLocalResourceObject("OutreachTypeRequired").ToString() %>' });
             if (first_identified_control == '')
                 first_identified_control = $(this);
             is_valid = false;

        }
    });
    if (!is_valid) {
        alert("Highlighted input fields are required/invalid. Please update and submit.");
        first_identified_control[0].focus();
        arguments.IsValid = false;
        return false;
    }
    else {
        arguments.IsValid = true;
        return true;
    }
}
function addTime(oldTime) {
    var time = oldTime.split(":");
    var hours = time[0];
    var ampm = time[1].substring(2, 4);
    var minutes = time[1].substring(0, 2);
    var time_new = '';
    if (+minutes >= 30) {
        hours = (+hours + 1) % 24;
    }
    minutes = (+minutes + 30) % 60;
    if (hours >= 12) {
        time_new = hours - 12 + ':' + minutes + 'pm';
    }
    else
        time_new = hours + ':' + minutes + ampm;
    return time_new;
}
function disableAddLocations() {
    var is_disable = false;
    if ($("#hfDisableAddLocations").val() != "") {
        var store_message = $("#hfDisableAddLocations").val();
        is_disable = true;
        alert(store_message);
    }

    return !is_disable;
}
function showClinicDateReminder(alert, handler) {
    $(function () {
        $("#divConfirmDialog").html(alert);
        $("#divConfirmDialog").dialog({
            closeOnEscape: false,
            beforeclose: function (event, ui) { return false; },
            dialogClass: "noclose",
            height: 200,
            width: 425,
            title: "IMPORTANT REMINDER",
            buttons: {
                'Ok': function () {
                    __doPostBack(handler);
                    $(this).dialog('close');
                }
            },
            modal: true
        });
    });
    return false;
}
function setTxtOthers(ctrl) {
    var lbl_other = $("label[for='" + ctrl[0].id + "']");
    var txt_other = $("#" + ctrl[0].id.match(/([^_]*_){2}/)[0] + "txtOther");
    if (ctrl[0].checked && ctrl[0].value == "4") {
        txt_other.show();
        txt_other.insertAfter(lbl_other);
    }
    else {
        txt_other.hide();
    }
}

    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hfcontactLogPk" runat="server" />
        <asp:HiddenField ID="hfBusinessName" runat="server" />
        <asp:HiddenField ID="hfisMOState" runat="server" Value="false" />
        <asp:HiddenField ID="hfDisableAddLocations" runat="server" />
        <asp:HiddenField ID="hfUnconfirmedPayment" runat="server" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server"
            DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="WalgreensUser" />
        <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
            <tr>
                <td colspan="2">
                    <uc2:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#FFFFFF" align="left">
                    <table width="935" border="0" cellspacing="22" cellpadding="0">
                        <tr>
                            <td colspan="2" valign="top" class="pageTitle">Walgreens Community Outreach</td>
                        </tr>
                        <tr>
                            <td valign="top" width="599">
                                <table width="599" border="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="612" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="612" border="0" cellspacing="0" cellpadding="12" id="trContractBodyText" style="border: #CCC solid 1px">
                                                            <tr>
                                                                <td class="contractBodyText">
                                                                    <p align="center">
                                                                        <b>
                                                                            <img src="images/contract_logo.gif" width="191" height="37" alt="Walgreens" /><br />
                                                                            COMMUNITY OUTREACH PROGRAM</b>
                                                                    </p>
                                                                    <table border="0" align="center" cellpadding="0" cellspacing="5" id="tblBusinesses" runat="server">
                                                                        <tr>
                                                                            <td align="left">&nbsp;</td>
                                                                            <td colspan="5" align="right">
                                                                                <table width="220" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td width="200" align="right" valign="top">
                                                                                            <asp:LinkButton ID="lnkAddImmunization" runat="server" OnClick="imgBtnAddImmunization_Click" meta:resourcekey="lnkAddImmunizationResource1">Add Immunization</asp:LinkButton>&nbsp;
                                                                                        </td>
                                                                                        <td width="20" align="right" valign="top">
                                                                                            <asp:ImageButton ID="imgBtnAddImmunization" ImageUrl="images/btn_add_business.png"
                                                                                                runat="server" AlternateText="Add Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_business.png');"
                                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_business_lit.png');" OnClick="imgBtnAddImmunization_Click" meta:resourcekey="imgBtnAddImmunizationResource1" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:GridView ID="grdImmunizationChecks" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdImmunizationChecks_RowDataBound"
                                                                                    meta:resourcekey="grdImmunizationChecksResource1">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Immunization" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="contractHeaderRow"
                                                                                            ItemStyle-CssClass="contractRow" FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="35%"
                                                                                            FooterStyle-Height="25px" meta:resourcekey="TemplateFieldResource1">
                                                                                            <ItemTemplate>
                                                                                                <table id="tblImmunization" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top">
                                                                                                            <asp:Label ID="lblImmunizationCheck" runat="server" Text='<%# Bind("immunizationName") %>' meta:resourcekey="lblImmunizationCheckResource1"></asp:Label>
                                                                                                            <asp:Label ID="lblImmunizationPk" runat="server" Text='<%# Bind("immunizationId") %>' Visible="False" meta:resourcekey="lblImmunizationPkResource1"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="ddlImmunizationCheck" runat="server" CssClass="contractBodyText" Height="20px" Width="210px" AutoPostBack="false"
                                                                                                                onchange="javascript:displayPaymentMethods(this.value);" meta:resourcekey="ddlImmunizationCheckResource1">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </FooterTemplate>
                                                                                            <FooterStyle CssClass="contractRow" Height="25px"></FooterStyle>
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="contractHeaderRow" Width="35%"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="contractRow" Width="35%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Payment Method" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="contractHeaderRow"
                                                                                            ItemStyle-CssClass="contractRow" FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="40%"
                                                                                            meta:resourcekey="TemplateFieldResource2">
                                                                                            <ItemTemplate>
                                                                                                <table id="tblPaymentTypes" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top">
                                                                                                            <asp:Label ID="lblPaymentType" Text='<%# Bind("paymentTypeName") %>' runat="server" meta:resourcekey="lblPaymentTypeResource1"></asp:Label>
                                                                                                            <asp:Label ID="lblPaymentTypeId" Text='<%# Bind("paymentTypeId") %>' runat="server" Visible="False" meta:resourcekey="lblPaymentTypeIdResource1"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="rowSendInvoiceTo" runat="server" visible="false">
                                                                                                        <td colspan="2">
                                                                                                            <table width="100%" border="0">
                                                                                                                <tr>
                                                                                                                    <td colspan="2" runat="server">
                                                                                                                        <asp:Label ID="lblSendInvoiceTo" runat="server" Text="Send Invoice To:" meta:resourcekey="lblSendInvoiceToResource2"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblName" runat="server" Text="Name:" meta:resourcekey="lblNameResource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtName" Text='<%# Bind("name") %>' CssClass="contractBodyText" runat="server" MaxLength="256"
                                                                                                                            meta:resourcekey="txtPmtNameResource1"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblAddress1" runat="server" Text="Address1:" meta:resourcekey="lblAddress1Resource2"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtAddress1" Text='<%# Bind("address1") %>' CssClass="contractBodyText" runat="server" MaxLength="256"
                                                                                                                            meta:resourcekey="txtPmtAddress1Resource1"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblAddress2" runat="server" Text="Address2:" meta:resourcekey="lblAddress2Resource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtAddress2" Text='<%# Bind("address2") %>' CssClass="contractBodyText" runat="server" MaxLength="256"
                                                                                                                            meta:resourcekey="txtPmtAddress2Resource1"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblCity" runat="server" Text="City:" meta:resourcekey="lblCityResource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtCity" Text='<%# Bind("city") %>' CssClass="contractBodyText" runat="server" MaxLength="100"
                                                                                                                            meta:resourcekey="txtPmtCityResource1"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblState" runat="server" Text="State:" meta:resourcekey="lblStateResource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlPmtState" runat="server" CssClass="contractBodyText"
                                                                                                                                        meta:resourcekey="ddlPmtStateResource1">
                                                                                                                                    </asp:DropDownList></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr runat="server">
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblZip" runat="server" Text="Zip Code:" meta:resourcekey="lblZipResource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtZipCode" Text='<%# Bind("zip") %>' CssClass="contractBodyText" runat="server" MaxLength="5"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblPhone" runat="server" Text="Phone:" meta:resourcekey="lblPhoneResource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtPhone" Text='<%# Bind("phone") %>' CssClass="contractBodyText" runat="server" onblur="textBoxOnBlur(this);"
                                                                                                                            MaxLength="14" meta:resourcekey="txtPmtPhoneResource1"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblEmail" runat="server" Text="Email:" meta:resourcekey="lblEmailResource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtEmail" Text='<%# Bind("email") %>' CssClass="contractBodyText" runat="server" MaxLength="256"
                                                                                                                            meta:resourcekey="txtPmtEmailResource1"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblVerifyEmail" runat="server" Text="Verify Email:" meta:resourcekey="lblVerifyEmailResource1"></asp:Label></td>
                                                                                                                    <td runat="server">
                                                                                                                        <asp:TextBox ID="txtPmtVerifyEmail" Text='<%# Bind("email") %>' CssClass="contractBodyText" runat="server" MaxLength="256"
                                                                                                                            meta:resourcekey="txtPmtVerifyEmailResource1"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr runat="server">
                                                                                                                    <td colspan="2" runat="server">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="lblIsEmpTxExp" runat="server" Text=""
                                                                                                                                        meta:resourcekey="lblIsEmpTxExpResource1"></asp:Label></td>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlTaxExempt" runat="server" CssClass="contractBodyText">
                                                                                                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                                                                                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                                                                    </asp:DropDownList></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr runat="server">
                                                                                                                    <td colspan="2" runat="server">
                                                                                                                        <asp:Label ID="lblCopayExixts" runat="server" Text="" meta:resourcekey="lblCopayExixtsResource1"></asp:Label><br />
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlIsCopay" runat="server" CssClass="contractBodyText" onChange="javascript:enableCoPay(this);">
                                                                                                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                                                                                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                                                                    </asp:DropDownList></td>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="lblDollar" runat="server" Text="$"></asp:Label>
                                                                                                                                    <asp:TextBox CssClass="contractBodyText" ID="txtCoPay" Text='<%# Bind("copayValue") %>' runat="server" MaxLength="8"></asp:TextBox></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="contractBodyText" Height="20px" Width="245px" meta:resourcekey="ddlPaymentTypeResource1"></asp:DropDownList>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </FooterTemplate>
                                                                                            <FooterStyle CssClass="contractRow"></FooterStyle>
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="contractHeaderRow" Width="40%"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="contractRow" Width="40%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Rates" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="contractHeaderRow" ItemStyle-CssClass="contractRow"
                                                                                            FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%" meta:resourcekey="TemplateFieldResource3">
                                                                                            <ItemTemplate>
                                                                                                <table id="tblPrice" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top" style="text-align: right;">
                                                                                                            <asp:Label ID="lblValue" runat="server" CssClass="contractBodyText" meta:resourcekey="lblValueResource1"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:ImageButton ID="imgBtnImmunizationRemove" ImageUrl="images/btn_remove.png" ImageAlign="Right" ToolTip="Remove Immunization"
                                                                                                    runat="server" AlternateText="Remove Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');"
                                                                                                    onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" CommandArgument="newImmunization" OnCommand="imgBtnRemoveImmunization_Click"
                                                                                                    meta:resourcekey="imgBtnImmunizationRemoveResource1" />
                                                                                            </FooterTemplate>
                                                                                            <FooterStyle CssClass="contractRow"></FooterStyle>
                                                                                            <HeaderStyle HorizontalAlign="Center" CssClass="contractHeaderRow" Width="10%"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Top" CssClass="contractRow" Width="10%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="contractHeaderRow" ItemStyle-CssClass="contractRow"
                                                                                            FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="right" ItemStyle-VerticalAlign="Top" ItemStyle-Width="5%" meta:resourcekey="TemplateFieldResource4">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="imgBtnImmunizationRemove" ImageUrl="images/btn_remove.png"
                                                                                                    runat="server" AlternateText="Remove Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');"
                                                                                                    onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" CommandArgument="existingImmunization" OnCommand="imgBtnRemoveImmunization_Click"
                                                                                                    meta:resourcekey="imgBtnImmunizationRemoveResource2" />
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:ImageButton ID="imgBtnImmunizationOk" ImageUrl="images/btn_ok.png" CausesValidation="true"
                                                                                                    runat="server" AlternateText="Add Immunization" onmouseout="javascript:MouseOutImage(this.id,'images/btn_ok.png');"
                                                                                                    onmouseover="javascript:MouseOverImage(this.id,'images/btn_ok_lit.png');" CommandArgument="immunization" OnCommand="imgBtnImmunizationOk_Click"
                                                                                                    OnClientClick="return validateImmSelection(this);" meta:resourcekey="imgBtnImmunizationOkResource1" />
                                                                                            </FooterTemplate>
                                                                                            <FooterStyle CssClass="contractRow"></FooterStyle>
                                                                                            <HeaderStyle HorizontalAlign="Right" CssClass="contractHeaderRow" Width="5%"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Top" CssClass="contractRow" Width="5%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <EmptyDataTemplate>
                                                                                        <table width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; background: #fff; border-collapse: collapse; text-align: left;">
                                                                                            <tr>
                                                                                                <th width="35%" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">
                                                                                                    <asp:Label ID="lblImmunizationSelection" runat="server" Text=" Immunization" meta:resourcekey="lblImmunizationSelectionResource1"></asp:Label>
                                                                                                </th>
                                                                                                <th width="40%" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">
                                                                                                    <asp:Label ID="lblPaymentMethodSelection" runat="server" Text="Payment Method" meta:resourcekey="lblPaymentMethodSelectionResource1"></asp:Label>
                                                                                                </th>
                                                                                                <th width="10%" align="center" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">
                                                                                                    <asp:Label ID="lblemptyRatecolHeader" runat="server" meta:resourcekey="lblemptyRatecolHeaderResource1"></asp:Label>
                                                                                                </th>
                                                                                                <th width="5%" align="right" valign="top" class="contractBodyText" style="font-weight: bold; padding: 2px; border-bottom: 1px solid #000;" scope="col">&nbsp;</th>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="contractRow" style="height: 25px; text-align: left; vertical-align: middle;">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:DropDownList ID="ddlImmunizationCheck" runat="server" CssClass="contractBodyText" AutoPostBack="false" onchange="javascript:displayPaymentMethods(this.value);"
                                                                                                                    Height="20px" Width="210px" meta:resourcekey="ddlImmunizationCheckResource2">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="contractRow" style="height: 25px; text-align: left; vertical-align: middle;">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="contractBodyText" Height="20px" Width="245px" meta:resourcekey="ddlPaymentTypeResource2"></asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="contractRow" style="text-align: left; vertical-align: middle;">&nbsp;</td>
                                                                                                <td class="contractRow" style="text-align: right; vertical-align: middle;">
                                                                                                    <asp:ImageButton ID="imgBtnImmunizationOk" ImageUrl="images/btn_ok.png" CausesValidation="true" runat="server" AlternateText="Add Immunization"
                                                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_ok.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_ok_lit.png');" CommandArgument="immunization"
                                                                                                        OnCommand="imgBtnImmunizationOk_Click" OnClientClick="return validateImmSelection(this);" meta:resourcekey="imgBtnImmunizationOkResource2" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </EmptyDataTemplate>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="lblImmDisclaimer" runat="server" Text="*Rates includes vaccine and administration." meta:resourcekey="lblImmDisclaimerResource1"></asp:Label></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="left">
                                                                                <b>Client Facility Location(s)*:</b>
                                                                            </td>
                                                                            <td colspan="4" align="right">
                                                                                <table width="100" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td width="82" align="right" valign="middle">
                                                                                            <asp:LinkButton ID="lnkAddLocation" runat="server" OnClientClick="return disableAddLocations()" OnClick="btnAddLocation_Click">Add Locations</asp:LinkButton>&nbsp;
                                                                                        </td>
                                                                                        <td width="18" align="right" valign="middle">

                                                                                            <asp:ImageButton ID="btnAddLocation" ImageUrl="images/btn_add_business.png" CausesValidation="true"
                                                                                                runat="server" AlternateText="Add Location" onmouseout="javascript:MouseOutImage(this.id,'images/btn_add_business.png');"
                                                                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_add_business_lit.png');"
                                                                                                OnClick="btnAddLocation_Click" OnClientClick="return disableAddLocations()" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="6">
                                                                                <asp:Label ID="lblClinicDateAlert" runat="server" Visible ="false" ForeColor="Red"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="6">
                                                                                <asp:GridView ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None" style="border-collapse: separate;"
                                                                                    Width="100%" OnRowDataBound="grdLocations_RowDataBound" DataKeyNames="state,clinicDate,coOutreachTypeId">
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="state" Visible="false" />
                                                                                        <asp:BoundField DataField="clinicDate" Visible="false" />
                                                                                        <asp:BoundField DataField="coOutreachTypeId" Visible="false" />
                                                                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="90%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblClinicLocation" Text='<%# Bind("clinicLocation") %>' runat="server" Style="font-weight: bold"></asp:Label>

                                                                                                <table width="100%" style="border-style: solid; border-width: 1px; border-color: Black">
                                                                                                    <tr>
                                                                                                        <td colspan="5" style="text-align: left;" class="contractBodyText">
                                                                                                            <asp:GridView ID="grdClinicImmunizations" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdClinicImmunizations_RowDataBound"
                                                                                                                meta:resourcekey="grdClinicImmunizationsResource1">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField HeaderText="<b>Estimated Shots per Immunization</b>" meta:resourcekey="TemplateFieldResource5">
                                                                                                                        <ItemTemplate>
                                                                                                                            <table width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <asp:Label ID="lblImmunizationId" runat="server" Text='<%# Bind("pk") %>' Visible="False" meta:resourcekey="lblImmunizationIdResource1"></asp:Label>
                                                                                                                                    <asp:Label ID="lblPaymentTypeId" runat="server" Text='<%# Bind("paymentTypeId") %>' Visible="False" meta:resourcekey="lblPaymentTypeIdResource2"></asp:Label>
                                                                                                                                    <td style="text-align: left; vertical-align: middle; width: 50px;">
                                                                                                                                        <asp:TextBox ID="txtClinicImmunizationShots" runat="server" Text='<%# Bind("estimatedQuantity") %>' CssClass="contractBodyText" MaxLength="5"
                                                                                                                                            Width="35px" meta:resourcekey="txtClinicImmunizationShotsResource1"></asp:TextBox></td>
                                                                                                                                    <td style="text-align: left; vertical-align: top; font-weight: bold;">
                                                                                                                                        <asp:Label ID="lblClinicImmunization" runat="server" CssClass="contractBodyText" meta:resourcekey="lblClinicImmunizationResource1"></asp:Label><br />
                                                                                                                                        (<asp:Label ID="lblClinicPaymentType" runat="server" Font-Bold="False" meta:resourcekey="lblClinicPaymentTypeResource1"></asp:Label>)
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><b>Local Contact Name</b></td>
                                                                                                        <td><b>Local Contact Phone</b></td>
                                                                                                        <td colspan="3"><b>Local Contact Email</b></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtLocalContactName" runat="server" Text='<%# Bind("localContactName") %>' CssClass="contractBodyText" Width="130px" MaxLength="100"></asp:TextBox></td>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtLocalContactPhone" runat="server" Text='<%# Bind("LocalContactPhone") %>' CssClass="contractBodyText" Width="100px" MaxLength="14" onblur="textBoxOnBlur(this);"></asp:TextBox></td>
                                                                                                        <td colspan="3">
                                                                                                            <asp:TextBox ID="txtLocalContactEmail" runat="server" Text='<%# Bind("LocalContactEmail") %>' CssClass="contractBodyText" Width="170px" MaxLength="256"></asp:TextBox></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><b>Address1</b></td>
                                                                                                        <td><b>Address2</b></td>
                                                                                                        <td><b>City</b></td>
                                                                                                        <td><b>State</b></td>
                                                                                                        <td><b>Zip</b></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("Address1") %>' CssClass="contractBodyText" Width="130px" MaxLength="256"></asp:TextBox></td>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("Address2") %>' CssClass="contractBodyText" Width="100px" MaxLength="50"></asp:TextBox></td>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("city") %>' CssClass="contractBodyText" Width="100px" MaxLength="50"></asp:TextBox></td>
                                                                                                        <td>
                                                                                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="contractBodyText" Width="95%" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"></asp:DropDownList></td>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtZipCode" runat="server" CssClass="contractBodyText" Width="85%" Text='<%# Bind("zipCode") %>' MaxLength="5"></asp:TextBox></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><b>Clinic Date</b></td>
                                                                                                        <td><b>Start Time</b></td>
                                                                                                        <td><b>End Time</b></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" />
                                                                                                            <asp:TextBox ID="txtCalenderFrom" runat="server" Visible="false" CssClass="contractBodyText" Width="100px"></asp:TextBox></td>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtStartTime" runat="server" Text='<%# Bind("startTime") %>' CssClass="contractBodyText" Width="100px" MaxLength="7" OnLoad="displayTime_Picker"></asp:TextBox></td>
                                                                                                        <td>
                                                                                                            <asp:TextBox ID="txtEndTime" runat="server" Text='<%# Bind("endTime") %>' CssClass="contractBodyText" Width="100px" MaxLength="7"></asp:TextBox></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><b>Outreach Type</b></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="5">
                                                                                                            <table style="width: 100%;">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:RadioButtonList ID="rbtnOutreachType" runat="server" RepeatLayout="Table" RepeatColumns="4" CssClass="contractBodyText" GroupName="outreachType"></asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox MaxLength="250" ID="txtOther" runat="server" CssClass="contractBodyText" Text='<%# Bind("coOutreachTypeDesc") %>' style ="margin-left : 5px;"> </asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            <%--<asp:RequiredFieldValidator ID="rbtnOutreachTypeFV" runat="server" ControlToValidate="rbtnOutreachType" Display="None" ErrorMessage="Outreach Type is required"></asp:RequiredFieldValidator>--%>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <br />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="imgBtnRemoveLocation"
                                                                                                    ImageUrl="images/btn_remove.png" CausesValidation="true"
                                                                                                    runat="server" AlternateText="Remove Location" onmouseout="javascript:MouseOutImage(this.id,'images/btn_remove.png');"
                                                                                                    onmouseover="javascript:MouseOverImage(this.id,'images/btn_remove_lit.png');" OnClick="imgBtnRemoveLocation_Click" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table id="tblAgreement" runat="server" width="100%" border="0">
                                                                        <tr></tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" width="268" style="padding-top: 20px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                                    <tr>
                                        <td>
                                            <table cellspacing="5" cellpadding="0" align="center">
                                                <tr>
                                                    <td colspan="2" align="center" valign="top" class="bestPracticesText">
                                                        <asp:ImageButton ID="btnScheduleClinic" ImageUrl="images/btn_submit_details.png" CausesValidation="true"
                                                            runat="server" AlternateText="Submit" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_details.png');"
                                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_details_lit.png');" OnClientClick="return CustomValidatorForLocations('WalgreensUser',this);"
                                                            OnClick="btnScheduleClinic_Click" ValidationGroup="WalgreensUser" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center" valign="top" class="bestPracticesText">
                                                        <asp:ImageButton ID="btnCancel" ImageUrl="images/btn_cancel_mini.png" CausesValidation="false"
                                                            runat="server" AlternateText="Cancel" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_mini.png');"
                                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_mini_lit.png');"
                                                            OnClick="btnCancel_Click" />
                                                    </td>
                                                </tr>   
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div id="divConfirmDialog" style="display: none; font-family: Arial; font-size: 13px; text-align: left;">
        </div>
         <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
