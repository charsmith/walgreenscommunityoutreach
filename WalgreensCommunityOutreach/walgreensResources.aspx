﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensResources.aspx.cs"
    Inherits="walgreensResources" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="WFooter" TagPrefix="ucFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="WHeader" TagPrefix="ucHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="javaScript/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucHeader:WHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td colspan="2" valign="top" class="pageTitle">
                            Resources
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="598" ><table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                          <tr>
                                <td><table width="100%" border="0" cellspacing="20px">
                                  <tr>
                                    <td width="50%" align="left" valign="top" class="class3"><span class="resourcesTitle">Outreach Materials</span><p><a href="controls/ResourceFileHandler.ashx?Path=2014_15/senior_outreach_job_aid.pdf"
                                                        target="_blank">Senior Outreach - Job Aid<br />
                                      Talking Points (pdf)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/senior_outreach_letter.pdf"
                                                            target="_blank">Senior Outreach Letter (pdf)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/AARP State Office Contact List April 2014.pdf"
                                                            target="_blank">AARP State Office Contact List (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/AARP State Office Contact List April 2014.xlsx"> (xlsx)</a></p></td>
															<td width="50%" rowspan="3" align="left" valign="top" class="class3" ><span class="resourcesTitle">Senior Presentations (continued)</span>
															
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2017_18/FallPrevention03292017.pdf"
                                                            target="_blank">Fall Prevention (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/FallPrevention03292017SpeakersNotes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/FallPrevention03292017.pptx">
                                                                    (pptx*)</a></p>
                                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/General Senior Issues.pdf"
                                                            target="_blank">General Senior Issues (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/General Senior Issues_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/General Senior Issues.pptx">
                                                                    (pptx*)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Hypertension 09082104.pdf"
                                                            target="_blank">Hypertension (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Hypertension 09082104_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Hypertension 09082104.pptx">
                                                                    (pptx*)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Immunizations.pdf"
                                                            target="_blank">Immunizations (pdf)</a>  <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Immunizations_notes.pdf" target="_blank" >(pdf notes)</a><a href="controls/ResourceFileHandler.ashx?Path=2015_16/Immunizations.pptx">
                                                                (pptx*)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Medication Adherence 09022014.pdf"
                                                            target="_blank">Medication Adherence (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Medication Adherence 09022014_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Medication Adherence 09022014.pptx">
                                                                    (pptx*)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Medication Safety 09022014.pdf"
                                                            target="_blank">Medication Safety (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Medication Safety 09022014_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Medication Safety 09022014.pptx">
                                                                    (pptx*)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition and Exercise 08262014C.pdf"
                                                            target="_blank">Nutrition-Exercise (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition and Exercise 08262014C_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition and Exercise 08262014C.pptx">
                                                                    (pptx*)</a></p>
                                                    <!--<p><a href="controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition-Exercise - WAG_JB.pdf" target="_blank" >Nutrition-Exercise (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition-Exercise - WAG_JB.pptx" >(pptx*)</a></p>-->
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Osteoarthritis Presentation.pdf"
                                                            target="_blank">Osteoarthritis (pdf)</a>  <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Osteoarthritis Presentation.pptx">
                                                                    (pptx*)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Osteoporosis 20140730C.pdf"
                                                            target="_blank">Osteoporosis Prevention and<br />
                                                            Management (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Osteoporosis 20140730C_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Osteoporosis 20140730C.pptx">
                                                                    (pptx*)</a></p>
                                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Parkinsons Disease.pdf"
                                                            target="_blank">Parkinson's Disease (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Parkinsons Disease_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Parkinsons Disease.pptx">
                                                                    (pptx*)</a></p>
                                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/presentation_kiosks.pdf" target="_blank">
                                                            Photo Kiosk (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/presentation_kiosks.pptx">
                                                                (pptx)</a></p>
                                                                <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/PoisonPrevention.pdf"
                                                            target="_blank">Poison Prevention (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/PoisonPrevention_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/PoisonPrevention.pptx">
                                                                    (pptx*)</a></p>
                                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/CommunityEducationQuitSmokingNow.pdf" target="_blank">
                                                            Quit Smoking Now (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/CommunityEducationQuitSmokingNow.pptx">
                                                                (pptx)</a></p>
                                                                <p><a href="controls/ResourceFileHandler.ashx?Path=2016_17/RecommendedAdultImmunizations.pdf" target="_blank">Recommended Adult Immunizations (pdf)</a></p>
                                                                <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2016_17/safemeddisposal_3252016.pdf"
                                                            target="_blank">Safe Medication Disposal (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2016_17/safemeddisposalwithnotes_3252016.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2016_17/safemeddisposal_3252016.pptx">
                                                                    (pptx*)</a></p>
                                                                <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Skin Care for Seniors.pdf"
                                                            target="_blank">Skin Care for Seniors (pdf)</a></p>
                                                   <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Stress Reduction in Elderly.pdf"
                                                            target="_blank">Stress Relief in Older Adults (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Stress Reduction in Elderly_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Stress Reduction in Elderly.pptx">
                                                                    (pptx*)</a></p>
                                                                    <p>
                                                    <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Scams on Customers.pdf" target="_blank">
                                                            Tips to Avoid Being Scammed (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Scams on Customers.pptx">
                                                                (pptx)</a></p>
                                                                <p><a href="controls/ResourceFileHandler.ashx?Path=2016_17/Walgreens App.pdf" target="_blank"  >Walgreens Mobile App: Features To Make Your Life Easier (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2016_17/Walgreens App.pptx"  >(pptx)</a></p>
                                                    <p>
                                                        <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Vitamins_Herbals_Supplements 20140730C.pdf"
                                                            target="_blank">Vitamins, Herbals &amp;<br />
                                                            Supplements (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Vitamins_Herbals_Supplements 20140730C.pptx">
                                                                (pptx)</a></p></p></td>
                                  </tr>
                                  
                                  <tr>
                                    <td  align="left" valign="top" class="class3"><span class="resourcesTitle">Med-B Materials</span><p><a href="controls/ResourceFileHandler.ashx?Path=Medicare_Part-B_Patient_FAQ.pdf"
                                                        target="_blank">Med-B Patient FAQ (pdf)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=Medicare_Part_B_Diabetes_Testing_Supplies_Presentation.pdf"
                                                            target="_blank">Med-B Diabetes Testing Supplies Presentation (pdf)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=Medicare_Part-B_Flyer.pdf" target="_blank"> Med-B Flyer (pdf)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=Medicare_Part-B_Flyer(SP).pdf" target="_blank"> Med-B Flyer (Spanish) (pdf)</a></p></td>
                                  </tr>
                                  <tr>
                                    <td  align="left" valign="top" class="class3"><span class="resourcesTitle">Senior Presentations</span>
									  <p> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017.pdf"
                                                            target="_blank">The ABCD's of Medicare (pdf) </a> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017_notes.pdf" target="_blank"> (pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017.pptx"> (pptx*)</a></p>
									  <p> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017ShortVersion.pdf"
                                                            target="_blank">The ABCD's of Medicare Short Version (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017ShortVersion_notes.pdf" target="_blank"> (pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017ShortVersion.pptx"> (pptx*)</a></p>
									  <p><a href="controls/ResourceFileHandler.ashx?Path=2014_15/Alzheimers and Dementia08252014.pdf"
                                                        target="_blank">Alzheimers &amp; Dementia (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Alzheimers and Dementia08252014_notes.pdf"
                                                            target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Alzheimers and Dementia08252014.pptx"> (pptx*)</a></p>
                                      
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Blood Pressure and Cholesterol 08262014C.pdf"
                                                            target="_blank">Blood Pressure and Cholesterol<br />
                                        Management (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Blood Pressure and Cholesterol 08262014C_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Blood Pressure and Cholesterol 08262014C.pptx"> (pptx*)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Cough and Cold Elderly.pdf"
                                                            target="_blank">Cough and Cold in the Elderly (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Cough and Cold Elderly_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Cough and Cold Elderly.pptx"> (pptx*)</a></p>
                                      <!-- <p> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Common Vitamins and Herbals - WAG_JB.pdf"
                                                            target="_blank">Common Vitamins &amp; Herbals (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Common Vitamins and Herbals - WAG_JB_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Common Vitamins and Herbals - WAG_JB.pptx"> (pptx*)</a></p>-->
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Cost Saving Medications.pdf"
                                                            target="_blank">Cost Saving Medications (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Cost Saving Medications_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Cost Saving Medications.pptx"> (pptx*)</a></p>
                                      <p><a href="controls/ResourceFileHandler.ashx?Path=2015_16/Depression in elderly SLIDES.pdf" target="_blank" >Depression in Eldery (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Depression in elderly SCRIPT.pdf" target="_blank" >(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Depression in elderly.pptx" >(pptx*)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Diabetes management basics 07292014C.pdf"
                                                            target="_blank">Diabetes Management Basics (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Diabetes management basics 07292014C_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Diabetes management basics 07292014C.pptx"> (pptx*)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Drug Interactions 08262014B.pdf"
                                                            target="_blank">Drug Interactions (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Drug Interactions 08262014B_notes.pdf"
                                                                target="_blank">(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Drug Interactions 08262014B.pptx"> (pptx*)</a></p>
                                      <p> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Eating for Healthy Aging.pdf"
                                                            target="_blank">Eating for Healthy Aging (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2015_16/Eating for Healthy Aging.pptx"> (pptx)</a></p>
															<p><a href="controls/ResourceFileHandler.ashx?Path=2014_15/Exercise Physical Activity.pdf" target="_blank" >Exercise and Physical Activity (pdf)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Exercise Physical Activity_notes.pdf" target="_blank" >(pdf notes)</a> <a href="controls/ResourceFileHandler.ashx?Path=2014_15/Exercise Physical Activity.pptx" >(pptx*)</a></p>
                                    <p>&nbsp;</p></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2" align="left" valign="top" style="color: #3096D8; font-family: Arial, Helvetica, sans-serif;
                                                    font-size: 11px;">* Includes speaker notes</td>
                                  </tr>
                                </table></td>
                          </tr>
                            </table>
                        </td>
                        <td valign="top" width="268" ><table width="100%" border="0" cellspacing="20" cellpadding="0">
                          <tr>
                            <td class="bestPracticesText"><span class="resourcesTitle">Approved Materials Only</span> <br />
                              Only corporate-approved powerpoint presentations
                              and materials may be used and distributed during community outreach activities.
                              You may not use materials you create or modify unless you first receive approval
                              from MaryAnn Desecki by emailing the materials to <span class="class3"><a href="mailto:MedicarePart-D@Walgreens.com"
                                            class="class3">MedicarePart-D@Walgreens.com</a></span>. </td>
                          </tr>
                          <!--<tr>
                                    <td class="bestPracticesText">
                                        <p>
                                            <b>Refreshments and Giveaways.</b> Any refreshments and/or giveaways you offer at
                                            a community outreach activity must comply with the following rules:</p>
                                        <ul>
                                            <li>&quot;Refreshment&quot; means non-alcoholic beverages (e.g., water, coffee, soda)
                                                or snack foods (e.g., pretzels, granola bars, apples).<br />
                                            </li>
                                            <li>A &quot;giveaway&quot; means an item that has a retail value of no more than a few
                                                dollars (e.g., pen, magnet, pillbox). It does not include cash, check, gift card,
                                                coupon, voucher or other item that must be presented at the store in order to be
                                                redeemed or used.</li>
                                            <li>The refreshment/giveaway must be offered in connection with making a presentation
                                                or staffing an information booth. The refreshment/giveaway cannot simply be dropped
                                                off at a location, even if you include brochures or other informational materials.</li>
                                            <li>The refreshment/giveaway must be available to anyone who attends the presentation
                                                or stops by the information booth. Receiving the refreshment/giveaway cannot be
                                                conditioned on any other action or activity by the individual (e.g., transferring
                                                a prescription, speaking with a pharmacist, enrolling in Balance Rewards, visiting
                                                the store, providing contact information).</li>
                                            <li>The combined retail value of the refreshment and/or giveaway cannot exceed $5 per
                                                person.</li>
                                            <li>You may only offer a refreshment/giveaway once per month, per location.</li>
                                        </ul>
                                    </td>
                                </tr>-->
                          <tr>
                            <td class="bestPracticesText"> Any additional questions, please email <span class="class3"><a href="mailto:MedicarePart-D@Walgreens.com"
                                            class="class3">MedicarePart-D@Walgreens.com</a></span></td>
                          </tr>
                        </table></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p>&nbsp;
        </p>
    <ucFooter:WFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
