﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensLandingPage.aspx.cs" Inherits="walgreensLandingPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script src="javaScript/jquery-ui.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.history.forward(0);
        $(document).ready(function () {
            $("#communityOutreach").show();
            $("#smallBusiness").hide();
            $("#seniorOutreach").hide();
            $("#fluImmunization").hide();

            $("#lnkSmallBusiness").mouseover(function () {
                $("#smallBusiness").show();
                $("#communityOutreach").hide();
            }).mouseout(function () {
                $("#smallBusiness").hide();
                $("#communityOutreach").show();
            });

            $("#lnkSeniorOutreach").mouseover(function () {
                $("#seniorOutreach").show();
                $("#communityOutreach").hide();
            }).mouseout(function () {
                $("#seniorOutreach").hide();
                $("#communityOutreach").show();
            });

            $("#lnkImmunizationProgram").mouseover(function () {
                $("#communityOutreach").hide();
                $("#fluImmunization").show();
            }).mouseout(function () {
                $("#fluImmunization").hide();
                $("#communityOutreach").show();
            });

        });
        function openExcelFile() {
            window.open("controls/ResourceFileHandler.ashx?Path=Outreach_Calendar.pdf");
        }         
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form runat="server">
<table width="936" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
  	<td class="landingHeadBkgd">
     <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="332" align="left" valign="top" ><img src="images/wags_landing_logo.png" width="312" alt="Walgreens Community Outreach Logo" /><br/>
                <span width="312" runat="server" id="lblOutreachTitle" class="outreachTitle">Community Outreach</span></td>
            <td width="593" colspan="2" rowspan="2" align="right" valign="top"><img src="images/spacer.gif" width="1" height="302" alt="spacer" /></td>
        </tr>
        <tr>
    		<td align="left" valign="bottom" style="padding:0px 0px 12px 20px;"><asp:Label ID="lblStoreName" runat="server" Text="" class="wagsAddress"></asp:Label></td>
  		</tr>
        <tr>
        <td colspan="3" align="right" valign="bottom" class="navBar">
        <asp:ImageButton ID="imgOutreachCal" ImageUrl="~/images/btn_calendar_landing.png"
                onmouseout="javascript:MouseOutImage(this.id,'images/btn_calendar_landing.png');" 
                CausesValidation="false" 
                onmouseover="javascript:MouseOverImage(this.id,'images/btn_calendar_landing_lit.png');" runat="server"
            AlternateText="Senior Outreach Calendar" OnClientClick="openExcelFile();"/><asp:ImageButton ID="imgSignOut" PostBackUrl="~/thankYou.aspx" ImageUrl="~/images/btn_sign_out_landing.png"
            onmouseout="javascript:MouseOutImage(this.id,'images/btn_sign_out_landing.png');" CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_sign_out_landing_lit.png');" runat="server"
            AlternateText="Sign Out" /></td></tr>
        <tr>
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="336" valign="top" bgcolor="#f9f9f9">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" valign="top" bgcolor="#f9f9f9" class="navTitle" >Select An Outreach Campaign
									<!--</br><p><span style="color:red;">The site is currently being updated, please check back in 30 minutes.</span></p>-->
									</td>
                                </tr>
                                <tr>
                                <td align="left" valign="top" bgcolor="#f9f9f9" class="navLanding" >
                                    <asp:PlaceHolder ID="plsOutreachEfforts" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                                <tr style="height:auto;border:0;">
                                    <td align="left" valign="top" bgcolor="#f9f9f9" class="navLanding" style='font-family: Arial, Helvetica, sans-serif;'>
                                         <div style='width:336px;'">
                                        <asp:Label ID="lblCustomMessage"  runat="server" Text="Label"></asp:Label>                                                                                     
                                              </div>
                                    </td>
                                </tr>                                   
                            </table>
                        </td>
                        <td width="600" valign="top" bgcolor="#ebecf1" >
                             <table width="100%" cellpadding="0" cellspacing="0" id="communityOutreach">
                                <tr bgcolor="#7bc045">
                                    <td width="225" align="left" valign="bottom" class="subheadGreen" >Community Outreach</td>
                                    <td align="right" valign="bottom" style="padding:0px 24px 0px 12px" ><img src="images/landing_photo_outreach_top.jpg" width="271" height="26" alt="Community Outreach" /></td>
                                </tr>
                                <tr>
                                    <td width="225" align="left" valign="top" bgcolor="#ebecf1" class="landingDescriptions" ><p>Welcome to the Community Outreach website.  Please select an outreach campaign on the left.</p></td>
                                    <td align="right" valign="top" bgcolor="#ebecf1" style="padding:0px 24px 24px 12px" ><img src="images/landing_photo_outreach_btm.jpg" width="271" height="284" alt="Community Outreach" /></td>
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0" id="seniorOutreach">
                                <tr bgcolor="#7bc045">
                                    <td width="225" align="left" valign="bottom" class="subheadGreen" >Senior Outreach</td>
                                    <td align="right" valign="bottom" style="padding:0px 24px 0px 12px" ><img src="images/landing_photo_outreach_top.jpg" width="271" height="26" alt="Senior Outreach" /></td>
                                </tr>
                                <tr>
                                    <td width="225" align="left" valign="top" bgcolor="#ebecf1" class="landingDescriptions" ><p><b>Outreach Objective:</b>  Develop relationships with people over 60 through regularly scheduled outreach.  Raise awareness of all the health related services Walgreens offers.  Encourage seniors to visit the store and meet the pharmacy staff.</p>
                                    <p><b>Target Locations:</b> Active senior locations such as apartment complexes, churches, libraries, senior service organizations.</p>
                                    <p>All senior outreach efforts must follow the events guidelines in the resource section<strong> </strong>or found on the Medicare Part-D page on Storenet.</p></td>
                                    <td align="right" valign="top" bgcolor="#ebecf1" style="padding:0px 24px 24px 12px" ><img src="images/landing_photo_outreach_btm.jpg " width="271" height="284" alt="Senior Outreach" /></td>
                                </tr>
                            </table>
                             <table width="100%" cellpadding="0" cellspacing="0" id="fluImmunization">
                                <tr bgcolor="#7bc045">
                                    <td width="225" align="left" valign="bottom" class="subheadGreen" >Immunization Program</td>
                                    <td align="right" valign="bottom" style="padding:0px 24px 0px 12px" ><img src="images/landing_photo_outreach_top.jpg" width="271" height="26" alt="Immunization Program" /></td>
                                </tr>
                                <tr>
                                    <td width="225" align="left" valign="top" bgcolor="#ebecf1" class="landingDescriptions" ><p>Objective: To manage Immunization related programs with small businesses as well as the corporately contracted worksites.</p>
                                    </td>
                                    <td align="right" valign="top" bgcolor="#ebecf1" style="padding:0px 24px 24px 12px" ><img src="images/landing_photo_outreach_btm.jpg " width="271" height="284" alt="Immunization Program" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>       
    </table></td>
  </tr>
</table>
</form>
</body>
</html>
