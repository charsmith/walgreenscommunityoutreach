﻿using System;
using System.Web.UI;
using System.Web.Security;

public partial class ErrorPage : Page
{
    #region ---------- PROTECTED EVENTS -----------
    protected void Page_Load(object sender, EventArgs e)
    {
        string displayMessage = (Session["displayErrorMessage"] != null) ? Session["displayErrorMessage"].ToString() : "";

        if (!string.IsNullOrEmpty(displayMessage))
        {
            this.lblError.Visible = true;
            this.lblError.Text = displayMessage;
            Session.Remove("displayErrorMessage");
        }
    }
    protected void lnkLogOut_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        FormsAuthentication.SignOut();
        Response.Redirect("walgreensLandingPage.aspx");
    }
    #endregion
}
