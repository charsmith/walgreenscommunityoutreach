﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="passwordRecovery.aspx.cs" Inherits="auth_passwordRecovery" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>   
     <link href="../css/wags.css" rel="stylesheet" type="text/css" />   
    <link href="../css/tdGlobal.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="936" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
  	    <td class="landingHeadBkgd">
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
            <tr>
                <td width="312" align="left" valign="top" style="padding:12px 0px 0px 16px;"><img src="../images/wags_landing_logo.png" width="312" alt="Walgreens Community Outreach Logo"/><br />
                <span class="outreachTitle">Community Outreach</span></td>
                <td width="593" colspan="2" rowspan="2" align="right" valign="top"><img src="../images/spacer.gif" width="1" height="302" alt="spacer" /></td>
            </tr>
            <tr>
    		    <td align="left" valign="bottom" style="padding:0px 0px 12px 20px;">&nbsp;</td>
  		    </tr>
            <tr>
    		    <td colspan="3" align="left" valign="top" class="navBar"><img src="../images/spacer.gif" alt="" name="" width="1" height="25" border="0" id="home" /></td>
  		    </tr>
            <tr>
                <td colspan="3" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0" height="300">
                    <tr><td valign="top">
                      <table width="935" border="0" cellspacing="22" cellpadding="0">
                        <tr>
                          <td width="400" rowspan="3" align="left" valign="top" class="backgroundGradientLog"><table width="268" border="0" cellspacing="8" cellpadding="0">
                        <tr>
                          <td align="left" ><asp:Label ID="lblMessage" runat="server" class="logTitle" >Walgreens Community Outreach Link Recovery</asp:Label></td>
                        </tr>
                        <tr>
                          <td class="formFields"><asp:Label ID="lblForgotPass" runat="server">Need a new link to access this site? Enter the email address from which you received your original link below, click Submit and a new link will be sent to you.</asp:Label></td>
                        </tr>
                        <tr>
                          <td><table  border="0" cellpadding="0" cellspacing="0" width="400">                           
                            <tr>
                              <td align="left" style="padding-left: 10px; padding-top: 6px;"
                                  valign="top"><asp:Label ID="lblUserName" runat="server" AssociatedControlID="txtUserName" CssClass="logSubTitles" nowrap="nowrap">Email: </asp:Label>
                                <br />
                                <asp:TextBox ID="txtUserName" runat="server" Width="350px"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="txtUserName"
                                ErrorMessage="Email Address is required." ToolTip="Email Address is required." ValidationGroup="PasswordRecovery1" CssClass="formFields2">* Please enter the email address from which you received your original link.</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                              <td align="left" class="formFields" style="padding-left: 10px; padding-top: 6px;color: red" ><asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top"><table>
                                <tr>
                                  <td align="left" style="padding-left: 10px"><asp:ImageButton ID="btnSubmit" ImageUrl="../images/btn_submit.png" 
                                    runat="server" CommandName="Submit" Text="Submit" OnClick ="SubmitButton_Click"
                                    ValidationGroup="PasswordRecovery1" /></td>
                                </tr>
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td></td>
                        </tr>
                      </table></td>
                            <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr class="pageTitle">
                          <td valign="top">&nbsp;</td>
                        </tr>
                   
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                    </table>
                    </td></tr>
                </table>
                </td>
            </tr>       
        </table>
    </td>
  </tr>
</table>
    <p>&nbsp;</p>
    </form>
</body>
</html>




