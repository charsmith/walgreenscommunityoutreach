﻿using System;
using TdApplicationLib;
using System.Web.UI;
using System.Web.Security;

public partial class sessionTimeout : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.RemoveAll();
        FormsAuthentication.SignOut();
    }   
}
