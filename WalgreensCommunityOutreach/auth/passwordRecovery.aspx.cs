﻿using System;
using System.Web.UI;
using System.Configuration;
using TdApplicationLib;
using System.Data;
using tdEmailLib;
using TdWalgreens;

public partial class auth_passwordRecovery : Page
{
    #region ---------- PAGE EVENTS --------------

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void SubmitButton_Click(object sender, ImageClickEventArgs e)
    {
        bool is_valid_email = false;

        if (!string.IsNullOrEmpty(txtUserName.Text.Trim()))
        {
            this.loginSession = LoginUserInfo.getLoginUserInfo(txtUserName.Text.Trim());

            if (this.loginSession != null)
            {
                is_valid_email = true;
                string salutation = string.Empty, mail_link = string.Empty;
                string image_path = (!ApplicationSettings.emailPath().EndsWith("/")) ? ApplicationSettings.emailPath() + "/" : ApplicationSettings.emailPath();

                EncryptedQueryString args = new EncryptedQueryString();
                args["arg1"] = txtUserName.Text.Trim();
                args["arg2"] = ApplicationSettings.walgreensAppPwd();

                switch (this.loginSession.UserRole.ToLower())
                {
                    case "admin":
                        mail_link = "Admin_" + this.loginSession.UserName + "_Outreach_Status";
                        salutation = this.loginSession.FirstName + " " + this.loginSession.LastName;
                        break;
                    case "district manager":
                    case "regional healthcare director":
                    case "regional vice president":
                    case "director – rx & retail ops":
                    case "healthcare supervisor":
                        mail_link = this.loginSession.LocationType + "_" + this.loginSession.AssignedTo + "_Outreach_Status";
                        salutation = this.loginSession.UserRole + " for " + this.loginSession.LocationType + " " + this.loginSession.AssignedTo;
                        break;
                    default:
                        DataTable user_stores = this.dbOperation.getUserStores(this.loginSession.UserID, string.Empty);
                        if (user_stores != null && user_stores.Rows.Count > 0)
                            mail_link = "Store_" + user_stores.Rows[0][0].ToString() + "_Outreach_Status";

                        salutation = this.loginSession.UserRole;
                        break;
                }

                if (this.walgreensEmail != null)
                {
                    this.walgreensEmail.sendForgotPasswordMail(args.ToString(), "OldUser", salutation, mail_link, image_path, Server.MapPath("~/emailTemplates/mailTemplate.htm"),  "Walgreens Community Outreach Portal Access Link", ((string)GetGlobalResourceObject("errorMessages", "mailBodyTextForOldUser")), "walgreensLandingPage.aspx?args=", ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : this.txtUserName.Text.Trim(), true);

                    lblMessage.Text = @"A link has been sent to your email. Please use this link to login.";
                }
            }
        }

        if (!is_valid_email)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "invalidUserName") + "');", true);
    }

    protected void CancelButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/auth/auth.aspx", false);
    }
    #endregion

    #region --------- PRIVATE VARIABLES ---------
    private LoginUserInfo loginSession = null;
    private DBOperations dbOperation = null;
    private WalgreenEmail walgreensEmail = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.dbOperation = new DBOperations();
        this.walgreensEmail = ApplicationSettings.emailSettings();
    }
    #endregion
}