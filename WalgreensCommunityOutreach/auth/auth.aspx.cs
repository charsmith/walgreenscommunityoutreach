﻿using System;
using TdApplicationLib;
using System.Web.Security;
using System.Web.UI;
using TdWalgreens;
using NLog.Config;
using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;
using System.Collections.Generic;

public partial class auth : Page
{
    #region ------- PAGE EVENT --------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        string args_value = Request.QueryString["args"];

        EncryptedQueryString args = new EncryptedQueryString(args_value);
        if (string.IsNullOrEmpty(args_value))
        {
            this.lblMessage.Text = (String)GetGlobalResourceObject("errorMessages", "unAuthorizedUser");
            return;
        }

        string user_name = string.Empty;
        if (args.Count > 0)
        {
            user_name = args["arg1"];
            if (!user_name.Contains("@"))
            {
                this.lblMessage.Text = (String)GetGlobalResourceObject("errorMessages", "unAuthorizedUser");
                return;
            }
        }
        else
        {
            this.lblMessage.Text = (String)GetGlobalResourceObject("errorMessages", "unAuthorizedUser");
            return;
        }


        Session["logString"] = "UserName: " + user_name + "; Server Name: " + Request.ServerVariables["SERVER_NAME"] + "; Host Address: " + Request.ServerVariables["REMOTE_HOST"];
        this.commonAppSession.LoginUserInfoSession = LoginUserInfo.getLoginUserInfo(user_name);
        if (this.commonAppSession.LoginUserInfoSession != null && !string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserName) && !string.IsNullOrEmpty(user_name))
        {
            Dictionary<int, string> wag_user = (Dictionary<int, string>)Application["WagUsers"];
            bool user_exists = false;

            if (wag_user.Count > 0)
            {
                if (wag_user.ContainsKey(this.commonAppSession.LoginUserInfoSession.UserID))
                    user_exists = true;
            }

            if (!user_exists)
            {
                wag_user.Add(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
                Application["WagUsers"] = wag_user;
            }

            //this.createLogFile(user_name);
            FormsAuthentication.RedirectFromLoginPage(user_name, true);
        }
        else
        {
            lblMessage.Text = (String)GetGlobalResourceObject("errorMessages", "cannotAccess");
            this.lblMessage.Text = (String)GetGlobalResourceObject("errorMessages", "cannotAccess");
            return;
        }
    }

    protected void lnkForgetPassword_Click(object sender, EventArgs e)
    {
        ViewState["LoginErrors"] = 1;
        Response.Redirect("passwordRecovery.aspx", false);
    }

    //private void createLogFile(string user_name)
    //{
    //    var target = (FileTarget)LogManager.Configuration.FindTargetByName("dailyFile");
    //    if (user_name.Length > 0)
    //        target.FileName = "${basedir}/Logs/Walgreens-" + user_name.Substring(0, user_name.IndexOf('@')) + "-${shortdate}.log";
    //    else
    //        target.FileName = "${basedir}/Logs/Walgreens-${shortdate}.log";

    //}
    #endregion --------------------------

    #region ---------- PRIVATE VARIABLE -------------
    private AppCommonSession commonAppSession = null;
    #endregion


    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
    #endregion
}
