﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="auth.aspx.cs" Inherits="auth" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/tdGlobal.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    </head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="936" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
  	    <td class="landingHeadBkgd">
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="312" align="left" valign="top" style="padding:12px 0px 0px 16px;"><img src="../images/wags_landing_logo.png" width="312" alt="Walgreens Community Outreach Logo" /><br />
                <span class="outreachTitle">Community Outreach</span></td>
                <td width="593" colspan="2" rowspan="2" align="right" valign="top"><img src="../images/spacer.gif" width="1" height="302" alt="spacer" /></td>
            </tr>
            <tr>
    		    <td align="left" valign="bottom" style="padding:12px 0px 0px 16px;">&nbsp;</td>
  		    </tr>
            <tr>
    		    <td colspan="3" align="left" valign="top" class="navBar"><img src="../images/spacer.gif" alt="" name="" width="1" height="25" border="0" id="home" /></td>
  		    </tr>
            <tr>
                <td colspan="3" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0" height="300">
                <tr class="pageTitle"><td valign="top" align="left">
                         <table width="100%" border="0" cellspacing="22" cellpadding="0">
                            <tr class="pageTitle">
                                <td valign="top"><asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></td>
                            </tr>
                   
                            <tr>
                                <td align="left" valign="top" class="formFields"><asp:LinkButton ID="lnkForgetPassword" runat="server" onclick="lnkForgetPassword_Click">Lost Your Walgreens Community Outreach Link?</asp:LinkButton></td>
                            </tr>
                        </table>
                        </td>
                        </tr>
                     </table>
                </td>
            </tr>       
        </table>
    </td>
  </tr>
</table>
    <p>&nbsp;</p>
    </form>
</body>
</html>



