﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensVoteVaxAgreementForUser.aspx.cs" Inherits="walgreensVoteVaxAgreementForUser" meta:resourcekey="PageResource1" %>

<%@ Register Src="controls/PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/theme.css" />
    <link rel="stylesheet" type="text/css" href="css/calendarStyle.css" />
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script src="javaScript/jquery.printelement.min.js" type="text/javascript"></script>
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>


    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
        var latLongTimer;
        var loopCount = 0;
        $(document).ready(function () {
            $("#txtOthers1").html($("#txtOther1"));
            $("#txtOthers2").html($("#txtOther2"));
            //showing notes text area if Reject radio selects
            if ($("#rbtnReject").is(":checked")) {
                $("#trNotes").show();
            }
            else {
                $("#trNotes").hide();
            }

            $("#rbtnApprove").change(function () {
                if (typeof (Page_Validators) != 'undefined') {
                    for (i = 0; i <= Page_Validators.length; i++) {
                        if (Page_Validators[i] != null) {
                            if (Page_Validators[i].controlToValidate == "txtNotes" || Page_Validators[i].controltovalidate == "txtNotes")
                                ValidatorEnable(Page_Validators[i], false);
                        }
                    }
                };
                if (this.checked) {
                    $("#trNotes").hide();
                }
                else {
                    $("#trNotes").show();
                }
            });

            // Print contract agreement part of the page
            $("#lnkPrintContract").click(function () {
                var printContents = document.getElementById('trContractBodyText').innerHTML;
                myWindow = window.open("", "", "location=1,status=1,scrollbars=1,width=650,height=600");
                myWindow.document.write(printContents);
                myWindow.document.close();
                myWindow.focus();
                myWindow.print();
            });

            $("#rbtnReject").change(function () {
                if (this.checked) {
                    $("#trNotes").show();
                    $("#txtElectronicSign").val("");
                    if (typeof (Page_Validators) != 'undefined') {
                        for (i = 0; i <= Page_Validators.length; i++) {
                            if (Page_Validators[i] != null) {
                                if (Page_Validators[i].controlToValidate == "txtNotes" || Page_Validators[i].controltovalidate == "txtNotes")
                                    ValidatorEnable(Page_Validators[i], true);
                            }
                        }
                    };
                }
                else {
                    $("#trNotes").hide();
                    ValidatorEnable(document.getElementById('txtElectronicSignRFV'), true);
                    if (typeof (Page_Validators) != 'undefined') {
                        for (i = 0; i <= Page_Validators.length; i++) {
                            if (Page_Validators[i] != null) {
                                if (Page_Validators[i].controlToValidate == "txtNotes" || Page_Validators[i].controltovalidate == "txtNotes")
                                    ValidatorEnable(Page_Validators[i], false);
                            }
                        }
                    };
                }
            });

            //highlight voucher expiration date picker
            $("#grdImmunizationChecks").find('input[type=text][id*=pcVaccineExpirationDate]').each(function () {
                $(this).css("background-color", "yellow");
            });

            if (loopCount <= 5)
                latLongTimer = setInterval(function setDelay() { SetClinicLatLong() }, loopCount * 5000);
        });

        //Get latitude and longitude for each clinic location        
        function SetClinicLatLong() {
            var index = 0;
            var clinicLocationCount = 0;
            var clinicAddress = "";
            var clinic = {};
            var clinicLocations = [];
            var clinicLocations10Each = [];
            var obj;
            $('#grdLocations tr').each(function () {
                if ($(this).find('input[id*=hfClinic]').length > 0) {
                    if ($('#' + $(this).find('input[id*=hfClinic]')[0].id).val() == "" || $('#' + $(this).find('input[id*=hfClinic]')[0].id).val() == undefined) {
                        clinic["Idx"] = clinicLocationCount;
                        clinic["ClinicLatitudeId"] = $(this).find('input[id*=hfClinic]')[0].id;
                        clinic["ClinicLongitudeId"] = $(this).find('input[id*=hfClinic]')[1].id;

                        $(this).find('span[id*=lblAddress1]').parent().parent().find('td span').each(function () {
                            clinicAddress += $(this).text();
                            clinicAddress += (index == 3) ? ' ' : ((index != 3 && index != 4 && $(this).text() != "") ? ',' : '');
                            index++;
                        });

                        clinic["ClinicAddress"] = clinicAddress;
                        clinicLocationCount++;
                        clinicAddress = "";
                        index = 0;
                        clinicLocations.push(clinic);
                        clinic = {};
                    }
                }
            });

            if (clinicLocations != "") {
                getClinicLocationLatLong(clinicLocations);
                loopCount++;
            }
            else
                clearInterval(latLongTimer);
        }

        var geocoderTimer;
        var geocoder;
        var latitude;
        var longitude;
        function getClinicLocationLatLong(clinic_locations) {
            geocoder = new google.maps.Geocoder();
            for (var i = 0; i < clinic_locations.length; i++) {
                (function (idx, clinic) {
                    geocoderTimer = setInterval(function () {
                        geocoder.geocode({ 'address': clinic.ClinicAddress }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                latitude = results[0].geometry.location.lat();
                                longitude = results[0].geometry.location.lng();

                                $('#' + clinic.ClinicLatitudeId).val(latitude);
                                $('#' + clinic.ClinicLongitudeId).val(longitude);

                                clearInterval(geocoderTimer);
                            }
                        })
                    }, 500);

                })(i, clinic_locations[i]);
            }
        }

        //        function CustomValidatorForClientDate(source, arguments) {
        //            arguments.IsValid = false;
        //            if ($('#PickerAndCalendarClientDate_Picker1_picker').length > 0) {
        //                if ($('#PickerAndCalendarClientDate_Picker1_picker').val() != "") {
        //                    arguments.IsValid = true;
        //                }
        //            }
        //            else
        //                arguments.IsValid = true;
        //        }
        function CustomValidatorForClientData(validationGroup) {

            var validating_fields = $("form :text, textarea, select");
            var validating_field_names = {
                'txtClient': { 'isRequired': true, 'when': 'always', 'Client': 'Email', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("BusinessNameRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("BusinessDataInvalid").ToString() %>' },
                'txtClientName': { 'isRequired': true, 'when': 'always', 'name': 'Client Name', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("ClientNameRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ClientNameInvalid").ToString() %>' },
                'txtClientTitle': { 'isRequired': true, 'when': 'always', 'name': 'Client title', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("ClientTitleRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ClientTitleInvalid").ToString() %>' },
                'txtAttentionTo': { 'isRequired': true, 'when': 'always', 'name': 'Attention to', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("AttentionToRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("AttentionToInvalid").ToString() %>' },
                'txtLegalAddress1': { 'isRequired': true, 'when': 'always', 'name': 'Address1', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("Address1Required").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("Address1Invalid").ToString() %>' },
                'txtLegalAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Address2', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': '<%= GetLocalResourceObject("Address2Invalid").ToString() %>' },
                'txtLegalCity': { 'isRequired': true, 'when': 'always', 'name': 'City', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("CityRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("CityInvalid").ToString() %>' },
                'ddlLegalState': { 'isRequired': true, 'when': 'always', 'name': 'State', 'type': 'select', 'requiredMessage': '<%= GetLocalResourceObject("StateRequired").ToString() %>', 'invalidMessage': '' },
                'txtLegalZip': { 'isRequired': true, 'when': 'always', 'name': 'Zip', 'type': 'zipcode', 'requiredMessage': '<%= GetLocalResourceObject("ZipRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("ZipInvalid").ToString() %>' },
                'txtElectronicSign': { 'isRequired': true, 'when': 'Approve', 'name': 'Signature', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("ESignatureRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("EsignInvalid").ToString() %>' },
                'txtNotes': { 'isRequired': true, 'when': 'Reject', 'name': 'Notes', 'type': 'junk', 'requiredMessage': '<%= GetLocalResourceObject("NotesRequired").ToString() %>', 'invalidMessage': '<%= GetLocalResourceObject("NotesInvalid").ToString() %>' }
            };

            var first_identified_control = "";
            var is_valid = true;
            var ctrl_Id;
            var indexOf_Underscore;
            var txtPmtEmail;
            $.each(validating_fields, function (index, ctrl) {

                $(ctrl).css({ "border": "1px solid gray" });
                $(ctrl).removeAttr("title");
                ctrl_Id = $(ctrl).attr('id');

                if ((ctrl_Id.indexOf('grdImmunizationChecks') > -1) && (ctrl_Id.lastIndexOf('_') > -1)) {
                    indexOf_Underscore = ctrl_Id.lastIndexOf('_');
                    if (ctrl_Id.length > indexOf_Underscore + 1) {
                        ctrl_Id = ctrl_Id.substring(indexOf_Underscore + 1);
                    }
                }


                if ((validating_field_names[ctrl_Id] != undefined) && validating_field_names[ctrl_Id].isRequired &&
                    $(ctrl).val() == "") {
                    var canValidate = true;
                    if (validating_field_names[ctrl_Id].when == 'Approve') {
                        canValidate = $("#rbtnApprove").is(":checked");
                    }
                    else if (validating_field_names[ctrl_Id].when == 'Reject') {
                        canValidate = $("#rbtnReject").is(":checked");
                    }

                    if (canValidate) {
                        if (validating_field_names[ctrl_Id].type == 'select') {
                            if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 6.") != -1)) {
                                $(ctrl).parent().css({ "border": "1px solid red" });
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            }
                            else {
                                $(ctrl).css({ "border": "1px solid red" });
                                $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            }
                        }
                        else {
                            $(ctrl).css({ "border": "1px solid red" });
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;
                    }
                }

                else if ((validating_field_names[ctrl_Id] != undefined) && $(ctrl).val() != "") {
                    if (!validateData(validating_field_names[ctrl_Id].type, $(ctrl).val(), true)) {
                        $(ctrl).css({ "border": "1px solid red" });
                        $(ctrl).attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;

                    }
                }
            });

            if (!is_valid) {
                alert("Highlighted input fields are required/invalid. Please update and submit.");
                first_identified_control[0].focus();
                return false;
            }
            else {
                return true;
            }
        }
        function setCurrentCulture(culture) {
            if (culture.outerHTML.indexOf("English") >= 0) {
                $("#hfLanguge").val("en-US");
            }
            else {
                if ($("#hfStoreState").val() == "PR")
                    $("#hfLanguge").val("es-PR");
                else
                    $("#hfLanguge").val("es-MX");
            }
        }
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hfBusinessUserEmail" runat="server" />
        <asp:HiddenField ID="hfContactLogPk" runat="server" />
        <asp:HiddenField ID="hfBusinessStoreId" runat="server" />
        <asp:HiddenField ID="hfBusinessName" runat="server" />
        <asp:HiddenField ID="hfLanguge" runat="server" Value="en-US" />
        <asp:HiddenField ID="hfThankYouPageReqValues" runat="server" Value="" />
        <asp:HiddenField ID="hfLastUpdatedDate" runat="server" Value="" />
        <asp:HiddenField ID="hfStoreState" runat="server" Value="" />
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" HeaderText="Error: " ShowMessageBox="True" ShowSummary="False" ValidationGroup="BusinessUser" meta:resourcekey="ValidationSummary2Resource1" />
        <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
            <tr>
                <td width="484" align="left" valign="bottom" bgcolor="#FFFFFF" style="padding: 12px 0px 10px 16px;">
                    <img id="imgLogo" runat="server" src="~/images/wags_logo.png" width="225" height="52" /><br />
                </td>
                <td width="451" align="right" valign="middle" bgcolor="#FFFFFF" class="outreachTitleRight" style="padding-right: 10px;"><span id="lblOutreachProgram" runat="server">Immunization Program</span></td>

                <td rowspan="2" align="right" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="left" valign="top" bgcolor="#57a1d3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#FFFFFF" align="left">
                    <table width="935" border="0" cellspacing="22" cellpadding="0">
                        <tr>
                            <td colspan="2" valign="top" class="pageTitle">
                                <asp:Label ID="lblAgreementTitle" runat="server" Text="Walgreens Community Off-Site Agreement" meta:resourcekey="lblAgreementTitleResource1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" width="599">
                                <table width="599" border="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="612" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="contractBodyText" style="padding-bottom: 6px">
                                                        <table align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkChangeCulture" runat="server" OnClientClick="return setCurrentCulture(this);" OnClick="lnkChangeCulture_Click" Text="Spanish Version" meta:resourcekey="lnkChangeCultureResource1"></asp:LinkButton>
                                                                </td>
                                                                <td>|</td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkPrintContract" runat="server" meta:resourcekey="lnkPrintContractResource1">Print Contract</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="612" border="0" cellspacing="0" cellpadding="36" id="trContractBodyText" style="border: #CCC solid 1px">
                                                            <tr>
                                                                <td class="contractBodyText">
                                                                    <p align="center">
                                                                        <b>
                                                                            <img src="images/contract_logo.gif" width="191" height="37" alt="Walgreens" /><br />
                                                                            <asp:Label ID="lblAgreementName" runat="server" meta:resourcekey="lblAgreementNameResource1"> COMMUNITY OFF-SITE CLINIC AGREEMENT</asp:Label></b>
                                                                    </p>
                                                                    <p align="justify">
                                                                        <asp:Label ID="lblAgreementFirstPara" runat="server" meta:resourcekey="lblAgreementFirstParaResource1">This <b><i>COMMUNITY OFF-SITE CLINIC AGREEMENT</i></b> (“<b>Agreement</b>”) by and between the party indicated below (“<b>Client</b>”), 
                                        and Walgreen Co., on behalf of itself and all of its subsidiaries and affiliates (“<b>Walgreens</b>”) is made and entered into on the 
                                        date last signed by an authorized representative of both the Client and Walgreens (the “<b>Effective Date</b>”).
                                                                        </asp:Label>
                                                                    </p>
                                                                    <p align="justify">
                                                                        <asp:Label ID="lblAgreementSecondPara" runat="server" meta:resourcekey="lblAgreementSecondParaResource1">
                                        For good and valuable consideration, the receipt and sufficiency of which are hereby acknowledged, Client and Walgreens, 
                                        by their signatures below, hereby agree that (i) Walgreens will provide the <b>Immunizations</b> Immunizations listed below, consisting 
                                        of dispensing and administering of a certain vaccine or vaccines to participants (“<b>Participants</b>”) at mutually agreed upon dates 
                                        and times at the Client’s facility(ies) listed below (“<b>Covered Services</b>”); and (ii) it will comply with the terms and conditions 
                                        of this Agreement, as shown on the following pages.
                                                                        </asp:Label>
                                                                    </p>
                                                                    <p align="justify">
                                                                        <asp:Label ID="lblAgreementThirdPara" runat="server" meta:resourcekey="lblAgreementThirdParaResource1"></asp:Label>
                                                                    </p>
                                                                    <table width="580" border="0" align="center" cellpadding="0" cellspacing="5" id="tblBusinesses" runat="server">
                                                                        <tr>
                                                                            <td align="left">&nbsp;</td>
                                                                            <td align="right">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:GridView ID="grdImmunizationChecks" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdImmunizationChecks_RowDataBound" meta:resourcekey="grdImmunizationChecksResource1">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Immunization" HeaderStyle-Width="35%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="contractHeaderRow" ItemStyle-CssClass="contractRow" FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="35%" FooterStyle-Height="25px" meta:resourcekey="TemplateFieldResource1">
                                                                                            <ItemTemplate>
                                                                                                <table id="tblImmunization" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top">
                                                                                                            <asp:Label ID="lblImmunizationCheck" runat="server" Text='<%# Bind("immunizationName") %>' meta:resourcekey="lblImmunizationCheckResource1"></asp:Label>
                                                                                                            <asp:Label ID="lblImmunizationPk" runat="server" Text='<%# Bind("pk") %>' Visible="False" meta:resourcekey="lblImmunizationPkResource1"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>

                                                                                            <FooterStyle CssClass="contractRow" Height="25px"></FooterStyle>

                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="contractHeaderRow" Width="35%"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="contractRow" Width="35%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Payment Method" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="contractHeaderRow" ItemStyle-CssClass="contractRow" FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="40%" meta:resourcekey="TemplateFieldResource2">
                                                                                            <ItemTemplate>
                                                                                                <table id="tblPaymentTypes" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top">
                                                                                                            <asp:Label ID="lblPaymentType" Text='<%# Bind("paymentTypeName") %>' runat="server" meta:resourcekey="lblPaymentTypeResource1"></asp:Label>
                                                                                                            <asp:Label ID="lblPaymentTypeId" Text='<%# Bind("paymentTypeId") %>' runat="server" Visible="False" meta:resourcekey="lblPaymentTypeIdResource1"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="rowSendInvoiceTo" runat="server" visible="False">
                                                                                                        <td colspan="2" runat="server">
                                                                                                            <table width="100%" id="tblPaymentInfo" border="0">
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <asp:Label ID="lblSendInvoiceTo" runat="server" Text="Send Invoice To:" meta:resourcekey="lblSendInvoiceToResource2"></asp:Label></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblName" runat="server" Text="Name:" meta:resourcekey="lblNameResource1" /></td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtPmtName" Text='<%# Bind("name") %>' CssClass="contractBodyText" runat="server" MaxLength="256"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblAddress1" runat="server" Text="Address1:" meta:resourcekey="lblAddress1Resource2" /></td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtPmtAddress1" Text='<%# Bind("address1") %>' CssClass="contractBodyText" runat="server" MaxLength="256"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblAddress2" runat="server" Text="Address2:" meta:resourcekey="lblAddress2Resource1" /></td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtPmtAddress2" Text='<%# Bind("address2") %>' CssClass="contractBodyText" runat="server" MaxLength="256"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblCity" runat="server" Text="City:" meta:resourcekey="lblCityResource1"></asp:Label></td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtPmtCity" Text='<%# Bind("city") %>' CssClass="contractBodyText" runat="server" MaxLength="100"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblState" runat="server" Text="State:" meta:resourcekey="lblStateResource1"></asp:Label></td>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblPmtState" Text='<%# Bind("state") %>' runat="server" CssClass="contractBodyText" Width="95%"></asp:Label></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblZip" runat="server" Text="Zip Code:" meta:resourcekey="lblZipResource1"></asp:Label></td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtPmtZipCode" Text='<%# Bind("zip") %>' CssClass="contractBodyText" runat="server" MaxLength="5"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblPhone" runat="server" Text="Phone:" meta:resourcekey="lblPhoneResource1" /></td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtPmtPhone" Text='<%# Bind("phone") %>' CssClass="contractBodyText" runat="server" onblur="textBoxOnBlur(this);" MaxLength="14"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblEmail" runat="server" Text="Email:" meta:resourcekey="lblEmailResource1" /></td>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtPmtEmail" Text='<%# Bind("email") %>' CssClass="contractBodyText" runat="server" MaxLength="256"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <asp:Label ID="lblIsEmpTxExp" runat="server" Text="" meta:resourcekey="lblIsEmpTxExpResource1"></asp:Label>
                                                                                                                        <asp:DropDownList ID="ddlTaxExempt" runat="server" CssClass="contractBodyText">
                                                                                                                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <asp:Label ID="lblCopayExixts" runat="server" Text="" meta:resourcekey="lblCopayExixtsResource1"></asp:Label><br />
                                                                                                                        <asp:DropDownList ID="ddlIsCopay" runat="server" CssClass="contractBodyText">
                                                                                                                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:Label ID="lblDollar" runat="server" Text="$"></asp:Label>
                                                                                                                        <asp:TextBox CssClass="contractBodyText" ID="txtCoPay" Text='<%# Bind("copayValue") %>' runat="server" MaxLength="8"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="rowVoucherNeeded" runat="server" visible="False">
                                                                                                        <td colspan="2" runat="server">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:Label ID="lblVoucherNeeded" runat="server" Text="" meta:resourcekey="lblVoucherNeededResource1"></asp:Label>
                                                                                                                        <asp:DropDownList ID="ddlVoucher" CssClass="contractBodyText" runat="server">
                                                                                                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="rowExpirationDate" runat="server">
                                                                                                                    <td runat="server">
                                                                                                                        <asp:Label ID="lblExirationDate" runat="server" Text="Expiration Date:" meta:resourcekey="lblExirationDateResource1"></asp:Label>
                                                                                                                        <uc1:PickerAndCalendar ID="pcVaccineExpirationDate" runat="server" />
                                                                                                                        <asp:TextBox ID="txtVaccineExpirationDate" runat="server" Visible="False" CssClass="contractBodyText" Width="100px"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>

                                                                                            <FooterStyle CssClass="contractRow"></FooterStyle>

                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="contractHeaderRow" Width="40%"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="contractRow" Width="40%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Rates" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="contractHeaderRow" ItemStyle-CssClass="contractRow" FooterStyle-CssClass="contractRow" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%" meta:resourcekey="TemplateFieldResource3">
                                                                                            <ItemTemplate>
                                                                                                <table id="tblPrice" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top" style="text-align: right; padding-right: 0px;">
                                                                                                            <asp:Label ID="lblValue" runat="server" Text='<%# Bind("price") %>' CssClass="contractBodyText" meta:resourcekey="lblValueResource1"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </ItemTemplate>

                                                                                            <FooterStyle CssClass="contractRow"></FooterStyle>

                                                                                            <HeaderStyle HorizontalAlign="Right" CssClass="contractHeaderRow" Width="10%"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Top" CssClass="contractRow" Width="10%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="lblImmDisclaimer" runat="server" Text="*Rates includes vaccine and administration." meta:resourcekey="lblImmDisclaimerResource1"></asp:Label></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <b>
                                                                                    <asp:Label ID="lblClineLocations" runat="server" Text="Client Facility Location(s)*:" meta:resourcekey="lblClineLocationsResource1"></asp:Label></b>
                                                                            </td>
                                                                            <td align="right"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:GridView ID="grdLocations" runat="server" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="grdLocationsRowdatabound"
                                                                                    Width="100%" DataKeyNames="state,clinicDate" meta:resourcekey="grdLocationsResource1">
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="state" Visible="false" meta:resourcekey="BoundFieldResource1" />
                                                                                        <asp:BoundField DataField="clinicDateTime" Visible="false" meta:resourcekey="BoundFieldResource2" />
                                                                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource5">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblClinicLocation" Text='<%# Bind("clinicLocation") %>' runat="server" Style="font-weight: bold" meta:resourcekey="lblClinicLocationResource1"></asp:Label>
                                                                                                <asp:HiddenField ID="hfClinicLatitude" runat="server" />
                                                                                                <asp:HiddenField ID="hfClinicLongitude" runat="server" />
                                                                                                <table width="100%" style="border-style: solid; border-width: 1px; border-color: Black">
                                                                                                    <tr>
                                                                                                        <td colspan="5" style="text-align: left;" class="contractBodyText">
                                                                                                            <asp:GridView ID="grdClinicImmunizations" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdClinicImmunizations_RowDataBound" meta:resourcekey="grdClinicImmunizationsResource1">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField HeaderText="<b>Estimated Shots per Immunization</b>" meta:resourcekey="TemplateFieldResource4">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lblImmunizationId" runat="server" Text='<%# Bind("pk") %>' Visible="False" meta:resourcekey="lblImmunizationIdResource1"></asp:Label>
                                                                                                                            <asp:Label ID="lblPaymentTypeId" runat="server" Text='<%# Bind("paymentTypeId") %>' Visible="False" meta:resourcekey="lblPaymentTypeIdResource2"></asp:Label>
                                                                                                                            <table width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td style="text-align: left; vertical-align: middle; width: 50px;">
                                                                                                                                        <asp:TextBox ID="txtClinicImmunizationShots" runat="server" Text='<%# Bind("estimatedQuantity") %>' CssClass="contractBodyText" MaxLength="5" Width="35px" meta:resourcekey="txtClinicImmunizationShotsResource1"></asp:TextBox></td>
                                                                                                                                    <td style="text-align: left; vertical-align: top; font-weight: bold;">
                                                                                                                                        <asp:Label ID="lblClinicImmunization" runat="server" CssClass="contractBodyText" meta:resourcekey="lblClinicImmunizationResource1"></asp:Label><br />
                                                                                                                                        (<asp:Label ID="lblClinicPaymentType" runat="server" Font-Bold="False" meta:resourcekey="lblClinicPaymentTypeResource1"></asp:Label>)
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                                                    </asp:TemplateField>
                                                                                                                </Columns>
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactName" runat="server" Text="Local Contact Name" meta:resourcekey="lblContactNameResource1"></asp:Label></b></td>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactPhone" runat="server" Text="Local Contact Phone" meta:resourcekey="lblContactPhoneResource1"></asp:Label></b></td>
                                                                                                        <td colspan="3"><b>
                                                                                                            <asp:Label ID="lblContactEmail" runat="server" Text="Local Contact Email" meta:resourcekey="lblContactEmailResource1"></asp:Label></b></td>

                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblLocalContactName" runat="server" Text='<%# Bind("localContactName") %>' CssClass="contractBodyText" Width="100px" MaxLength="500" meta:resourcekey="lblLocalContactNameResource1"></asp:Label></td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblLocalContactPhone" runat="server" Text='<%# Bind("LocalContactPhone") %>' CssClass="contractBodyText" Width="100px" MaxLength="14" meta:resourcekey="lblLocalContactPhoneResource1"></asp:Label></td>
                                                                                                        <td colspan="3">
                                                                                                            <asp:Label ID="lblLocalContactEmail" runat="server" Text='<%# Bind("LocalContactEmail") %>' CssClass="contractBodyText" Width="100px" MaxLength="500" meta:resourcekey="lblLocalContactEmailResource1"></asp:Label></td>

                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactAddress1" runat="server" Text="Address1" meta:resourcekey="lblContactAddress1Resource1"></asp:Label></b></td>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblConatctAddress2" runat="server" Text="Address2" meta:resourcekey="lblConatctAddress2Resource1"></asp:Label></b></td>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactCity" runat="server" Text="City" meta:resourcekey="lblContactCityResource1"></asp:Label></b></td>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactState" runat="server" Text="State" meta:resourcekey="lblContactStateResource1"></asp:Label></b></td>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactZip" runat="server" Text="Zip" meta:resourcekey="lblContactZipResource1"></asp:Label></b></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblAddress1" runat="server" Text='<%# Bind("Address1") %>' CssClass="contractBodyText" Width="100px" MaxLength="500"></asp:Label></td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblAddress2" runat="server" Text='<%# Bind("Address2") %>' CssClass="contractBodyText" Width="100px" MaxLength="500"></asp:Label></td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblCity" runat="server" Text='<%# Bind("city") %>' CssClass="contractBodyText" Width="100px" MaxLength="100"></asp:Label></td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblState" runat="server" CssClass="contractBodyText" Width="95%" Text='<%# Bind("state") %>'></asp:Label></td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblZipCode" runat="server" CssClass="contractBodyText" Width="95%" Text='<%# Bind("zipCode") %>' MaxLength="5" meta:resourcekey="lblZipCodeResource1"></asp:Label></td>

                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactClinicDate" runat="server" Text="Clinic Date" meta:resourcekey="lblContactClinicDateResource1"></asp:Label></b></td>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactSTime" runat="server" Text="Start Time" meta:resourcekey="lblContactSTimeResource1"></asp:Label></b></td>
                                                                                                        <td><b>
                                                                                                            <asp:Label ID="lblContactETime" runat="server" Text="End Time" meta:resourcekey="lblContactETimeResource1"></asp:Label></b></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                        <td>&nbsp;</td>

                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="hdClinicDateTime" runat="server" Text='<%# Bind("clinicDate") %>' Visible="False" meta:resourcekey="hdClinicDateTimeResource1"></asp:Label><asp:Label ID="lblClinicDateTime" runat="server" CssClass="contractBodyText" Text='<%# Bind("clinicDate") %>' meta:resourcekey="lblClinicDateTimeResource1"></asp:Label></td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblStartTime" runat="server" Text='<%# Bind("startTime") %>' CssClass="contractBodyText" Width="55px" MaxLength="5" meta:resourcekey="lblStartTimeResource1"></asp:Label></td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblEndTime" runat="server" Text='<%# Bind("endTime") %>' CssClass="contractBodyText" Width="55px" MaxLength="5" meta:resourcekey="lblEndTimeResource1"></asp:Label></td>
                                                                                                        <td>&nbsp;</td>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <br />
                                                                                            </ItemTemplate>

                                                                                            <HeaderStyle HorizontalAlign="Left" Width="30%"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                    <table id="tblAgreement" runat="server">
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <p>
                                                                                                <asp:Label ID="lblLocationDiscliamer2" runat="server" Text="&lt;b&gt;IN WITNESS WHEREOF&lt;/b&gt;, Client and Walgreens have electronically executed this Agreement, as of the Effective Date." meta:resourcekey="lblLocationDiscliamer2Resource1"></asp:Label>
                                                                                            </p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
                                                                                    <tr>
                                                                                        <td width="46" align="left" valign="middle">
                                                                                            <b>
                                                                                                <asp:Label ID="lblClinet" runat="server" Text="CLIENT:" meta:resourcekey="lblClinetResource1"></asp:Label></b>
                                                                                        </td>
                                                                                        <td width="210" align="left" valign="middle">
                                                                                            <asp:TextBox ID="txtClient" runat="server" Width="200px" CssClass="contractBodyText" ValidationGroup="BusinessUser" MaxLength="256" Style="background-color: #FF0" meta:resourcekey="txtClientResource1"></asp:TextBox>
                                                                                        </td>
                                                                                        <td colspan="2" align="left" valign="middle">
                                                                                            <b><u>
                                                                                                <asp:Label ID="Label1" runat="server" Text="WALGREEN CO." meta:resourcekey="Label1Resource1"></asp:Label></u></b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblClientUserName" runat="server" Text="NAME:" meta:resourcekey="lblClientUserNameResource1"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:TextBox ID="txtClientName" runat="server" Width="200px" CssClass="contractBodyText" ValidationGroup="BusinessUser" MaxLength="256" Style="background-color: #FF0" meta:resourcekey="txtClientNameResource1"></asp:TextBox>
                                                                                        </td>
                                                                                        <td width="46" align="left" valign="middle">
                                                                                            <asp:Label ID="lblWGUserName" runat="server" Text="NAME:" meta:resourcekey="lblWGUserNameResource1"></asp:Label>
                                                                                        </td>
                                                                                        <td width="210" align="left" valign="middle">
                                                                                            <asp:Label ID="lblWalgreenName" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblWalgreenNameResource1"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblClinetUserTitle" runat="server" Text="TITLE:" meta:resourcekey="lblClinetUserTitleResource1"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:TextBox ID="txtClientTitle" runat="server" Width="200px"
                                                                                                CssClass="contractBodyText" ValidationGroup="BusinessUser" MaxLength="256" Style="background-color: #FF0" meta:resourcekey="txtClientTitleResource1"></asp:TextBox>
                                                                                        </td>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblWGUserTitle" runat="server" Text="TITLE:" meta:resourcekey="lblWGUserTitleResource1"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblWalgreenTitle" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblWalgreenTitleResource1"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblClientUserDate" runat="server" Text="DATE:" meta:resourcekey="lblClientUserDateResource1"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblClientDate" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblClientDateResource1"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblWGUserDate" runat="server" Text="DATE:" meta:resourcekey="lblWGUserDateResource1"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" valign="middle">
                                                                                            <asp:Label ID="lblWalgreenDate" runat="server" Width="200px" CssClass="contractBodyText" meta:resourcekey="lblWalgreenDateResource1"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="left" valign="middle">
                                                                                            <u>
                                                                                                <asp:Label ID="lblSendLegalNotices" runat="server" Text="Send Legal Notices To Client At:" meta:resourcekey="lblSendLegalNoticesResource1"></asp:Label></u>
                                                                                        </td>
                                                                                        <td colspan="2" align="left" valign="middle">
                                                                                            <asp:Label ID="lblDistrictNumber" runat="server" Text="DISTRICT NUMBER:" meta:resourcekey="lblDistrictNumberResource1"></asp:Label>&nbsp;
                                                <asp:Label ID="lblDistrictNum" runat="server" Width="103px" CssClass="contractBodyText" meta:resourcekey="lblDistrictNumResource1"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="left" valign="middle">

                                                                                            <table width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblAttentionTo" runat="server" Text="Attention to:" meta:resourcekey="lblAttentionToResource1"></asp:Label></td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtAttentionTo" runat="server" Width="180px"
                                                                                                            CssClass="contractBodyText" ValidationGroup="BusinessUser" MaxLength="256" Style="background-color: #FF0" meta:resourcekey="txtAttentionToResource1"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblWalgreenAddress1" runat="server" Text="Address1:" meta:resourcekey="lblWalgreenAddress1Resource1"></asp:Label></td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtLegalAddress1" runat="server" Width="180px"
                                                                                                            CssClass="contractBodyText" ValidationGroup="BusinessUser" MaxLength="256" Style="background-color: #FF0" meta:resourcekey="txtLegalAddress1Resource1"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblWalgreenAddress2" runat="server" Text="Address2:" meta:resourcekey="lblWalgreenAddress2Resource1"></asp:Label></td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtLegalAddress2" runat="server" Width="180px"
                                                                                                            CssClass="contractBodyText" MaxLength="256" Style="background-color: #FF0" meta:resourcekey="txtLegalAddress2Resource1"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblWalgreenCity" runat="server" Text="City:" meta:resourcekey="lblWalgreenCityResource1"></asp:Label></td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtLegalCity" runat="server" Width="180px" Style="background-color: #FF0"
                                                                                                            CssClass="contractBodyText" ValidationGroup="BusinessUser" MaxLength="256" meta:resourcekey="txtLegalCityResource1"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblWalgreenState" runat="server" Text="State:" meta:resourcekey="lblWalgreenStateResource1"></asp:Label></td>
                                                                                                    <td align="left">
                                                                                                        <asp:DropDownList ID="ddlLegalState" runat="server" CssClass="contractBodyText" Style="background-color: #FF0" meta:resourcekey="ddlLegalStateResource1"></asp:DropDownList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblWalgreenZip" runat="server" Text="Zip Code:" meta:resourcekey="lblWalgreenZipResource1"></asp:Label></td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="txtLegalZip" runat="server" Width="180px" Style="background-color: #FF0"
                                                                                                            CssClass="contractBodyText" ValidationGroup="BusinessUser" MaxLength="5" meta:resourcekey="txtLegalZipResource1"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>


                                                                                        </td>
                                                                                        <td colspan="2" align="left" valign="top">
                                                                                            <table width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td align="left" valign="top"><u>
                                                                                                        <asp:Label ID="lblWalgreenSendLegalNoticesTo1" runat="server" Text="Send Legal Notices To Walgreens At:" meta:resourcekey="lblWalgreenSendLegalNoticesTo1Resource1"></asp:Label></u></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" align="left" valign="top">Healthcare Innovations Group<br />
                                                                                                        200 Wilmot Rd<br />
                                                                                                        MS2222<br />
                                                                                                        Deerfield, IL 60015<br />
                                                                                                        Attn: Health Law – Divisional Vice President<br />
                                                                                                        cc: clinicalcontracts@walgreens.com</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="left" valign="middle">&nbsp;
                                                    
                                                                                        </td>
                                                                                        <td colspan="2" align="left" valign="middle">&nbsp;
                                                    
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <p align="center">
                                                                                    <b>
                                                                                        <asp:Label ID="lblAgreementTC" runat="server" meta:resourcekey="lblAgreementTCResource1"></asp:Label></b>
                                                                                </p>
                                                                                <table width="540" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td width="47%" align="left" valign="top">
                                                                                            <p style="text-align: justify">
                                                                                                <asp:Label ID="lblTC1" runat="server" meta:resourcekey="lblTC1Resource1"></asp:Label>
                                                                                            </p>
                                                                                            <p style="text-align: justify">
                                                                                                <asp:Label ID="lblTC2" runat="server" meta:resourcekey="lblTC2Resource1"></asp:Label>
                                                                                            </p>
                                                                                            <p style="text-align: justify">
                                                                                                <asp:Label ID="lblTC3" runat="server" meta:resourcekey="lblTC3Resource1"></asp:Label>
                                                                                            </p>

                                                                                        </td>
                                                                                        <td width="6%" align="left" valign="middle">&nbsp;</td>
                                                                                        <td width="47%" colspan="2" align="left" valign="top">
                                                                                            <p style="text-align: justify">
                                                                                                <asp:Label ID="lblTC4" runat="server" meta:resourcekey="lblTC4Resource1"></asp:Label>
                                                                                            </p>
                                                                                            <p style="text-align: justify">
                                                                                                <asp:Label ID="lblTC5" runat="server" meta:resourcekey="lblTC5Resource1"></asp:Label>
                                                                                            </p>
                                                                                            <p style="text-align: justify">
                                                                                                <asp:Label ID="lblTC6" runat="server" meta:resourcekey="lblTC6Resource1"></asp:Label>
                                                                                            </p>
                                                                                            <p style="text-align: justify">
                                                                                                <asp:Label ID="lblTC7" runat="server" meta:resourcekey="lblTC7Resource1"></asp:Label>
                                                                                            </p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" valign="top">&nbsp;</td>
                                                                                        <td align="left" valign="middle">&nbsp;</td>
                                                                                    </tr>
                                                                                </table>
                                                                                <p align="right">
                                                                                    <span style="font-size: 10px;">
                                                                                        <asp:Label ID="lblCopyRights" runat="server" Text="©2015 Walgreen Co. All rights reserved." meta:resourcekey="lblCopyRightsResource1"></asp:Label></span>
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" width="268" style="padding-top: 20px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wagsRoundedCorners">
                                    <tr>
                                        <td>
                                            <table border="0" cellspacing="12" width="100%">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblErrorMessage" runat="server" class="bestPracticesText" ForeColor="Red" meta:resourcekey="lblErrorMessageResource1"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="left" valign="top" style="font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif">
                                                        <asp:Label ID="lblUserMessage" runat="server" meta:resourcekey="lblUserMessageResource1">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="1" id="tblApproval" runat="server" visible="false">
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <asp:RadioButton ID="rbtnApprove" CssClass="formFields" Checked="True" runat="server"
                                                                            Text="Approve" GroupName="agreement" meta:resourcekey="rbtnApproveResource1" />
                                                                    </b>
                                                                    <label for="checkbox11">
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblElectronicSignature" runat="server" meta:resourcekey="lblElectronicSignatureResource1"></asp:Label>
                                                                    <asp:TextBox ID="txtElectronicSign" runat="server" MaxLength="100"
                                                                        CssClass="bestPracticesText" Style="background-color: #FF0" Width="177px" meta:resourcekey="txtElectronicSignResource1"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <asp:RadioButton ID="rbtnReject" CssClass="formFields" runat="server" Text="Reject"
                                                                            GroupName="agreement" meta:resourcekey="rbtnRejectResource1"></asp:RadioButton>
                                                                    </b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trNotes" style="display: none;" runat="server">
                                                    <td colspan="2" align="left" valign="top" class="bestPracticesText" style="padding-left: 12px">
                                                        <asp:Label ID="lblNotes" runat="server" meta:resourcekey="lblNotesResource1"></asp:Label><br />
                                                        <label for="textarea">
                                                        </label>
                                                        <asp:TextBox ID="txtNotes" class="formFields" runat="server" Columns="75" Rows="5" TextMode="MultiLine" Width="200px" meta:resourcekey="txtNotesResource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center" valign="top" class="bestPracticesText">

                                                        <asp:ImageButton ID="btnSubmit" ImageUrl="images/btn_submit_event.png"
                                                            runat="server" AlternateText="Send email" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_event.png');"
                                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_event_lit.png');"
                                                            OnClick="btnSubmit_Click" ValidationGroup="BusinessUser" OnClientClick="return CustomValidatorForClientData('BusinessUser');" meta:resourcekey="btnSubmitResource1" />
                                                        <asp:CustomValidator ID="ClinicAgreementUserCV" runat="server" ValidationGroup="BusinessUser" OnServerValidate="ValidateData" meta:resourcekey="ClinicAgreementUserCVResource1"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
