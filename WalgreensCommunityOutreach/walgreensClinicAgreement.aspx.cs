﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using System.IO;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Text;
using TdWalgreens;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;
using System.Web;
using System.Configuration;
using System.Xml.Linq;
using NLog;

public partial class walgreensClinicAgreement : Page
{
    #region ------------ PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk > 0)
            {
                this.contactLogPk = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk;
                this.hfContactLogPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk.ToString();
                this.hfBusinessStoreId.Value = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.hfBussinessClinicPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk.ToString();
                Session["identifierString"] = " ContactLogPk: " + this.hfContactLogPk.Value;
                this.bindContractAgreement();
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = 0;
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
            }
            else
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("walgreensNoAccess.htm");
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(this.hfContactLogPk.Value))
                this.contactLogPk = Convert.ToInt32(this.hfContactLogPk.Value);

            string event_target = Request["__EVENTTARGET"];
            if (event_target.ToLower() == "confirmimmunization")
            {
                bool confirm_immunization = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                if (!confirm_immunization)
                {
                    if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
                    {
                        this.grdImmunizationChecks.FooterRow.Visible = false;
                        this.grdImmunizationChecks.ShowFooter = false;

                        this.imgBtnAddImmunization.Enabled = true;
                        this.lnkAddImmunization.Enabled = true;
                        IPostBackEventHandler send_email = (IPostBackEventHandler)this.btnSendMail;
                        send_email.RaisePostBackEvent(string.Empty);
                    }
                }
                else
                {
                    ImageButton img_immunization = null;
                    if (this.grdImmunizationChecks.Rows.Count == 0)
                        img_immunization = (ImageButton)grdImmunizationChecks.Controls[0].Controls[0].FindControl("imgBtnImmunizationOk");
                    else
                        img_immunization = (ImageButton)grdImmunizationChecks.FooterRow.FindControl("imgBtnImmunizationOk");


                    this.imgBtnImmunizationOk_Click(img_immunization, EventArgs.Empty);
                }
            }
            else if (event_target.ToLower() == "continuesending")
            {
                string confirmation_type = (Request["__EVENTARGUMENT"]).ToString();
                if (confirmation_type == "1") //Continue with 250+ qty
                {
                    this.sendEmailHandler(true, true);
                }
                else
                {
                    if (this.isClinicAfter2Weeks)
                    {
                        string clinic_date_reminder_alert = this.hfLanguage.Value == "es-MX" ? "clinicDateReminderAfter2WeeksMX" : (this.hfLanguage.Value == "es-PR" ? "clinicDateReminderAfter2WeeksPR" : "clinicDateReminderAfter2WeeksEN");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueReminder');", true);
                        //this.isClinicAfter2Weeks = false;
                    }
                    else
                        this.sendEmailHandler(true);
                }
            }
            else if (event_target.ToLower() == "continuereminder")
            {
                this.sendEmailHandler(true);
            }
            this.setMinMaxDates();
        }
        this.setImageButtons();
        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void btnSendMail_Click(object sender, ImageClickEventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "btnSendMail_Click",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);

        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            if (this.ValidateAgreement("WalgreensUser"))
            {
                DataTable dt_contract_agreement = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[0];
                this.isContractApproved = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["isApproved"].ToString() : "";
                this.isContractApproved = string.IsNullOrEmpty(this.isContractApproved) ? false.ToString() : this.isContractApproved;
                this.businessName = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["businessName"].ToString() : "";
                //this.isContractModified = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["isModified"].ToString() : "";
                this.isContractModified = string.IsNullOrEmpty(this.isContractModified) ? false.ToString() : this.isContractModified;

                if ((!Convert.ToBoolean(this.isContractApproved) && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1) || this.commonAppSession.SelectedStoreSession.isModifyAgreement)
                {
                    this.ValidationsHandler();
                    return;
                }
                this.logger.Info("Clinic Agreement for {0} was validated and ready to send email by the user {1}", this.businessName, this.commonAppSession.LoginUserInfoSession.UserName);
                this.sendEmailHandler();
            }
            else if (this.paymentMethods == "[]")
            {
                this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
                this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();
            }
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.location.href = 'walgreensHome.aspx';", true);

        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "btnSendMail_Click",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    protected void btnSendLater_Click(object sender, ImageClickEventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "btnSendLater_Click",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            if (this.ValidateAgreement("SendLater"))
            {
                this.logger.Info("Clinic Agreement for {0} was validated and ready to save by the user {1}", this.businessName, this.commonAppSession.LoginUserInfoSession.UserName);
                this.saveSendLaterHandler();
            }
            else if (this.paymentMethods == "[]")
            {
                this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
                this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();
            }
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.location.href = 'walgreensHome.aspx';", true);

        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "btnSendLater_Click",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);

    }

    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        string referrer_path = string.Empty;
        int business_clinic_pk, store_id;

        Session.Remove("contractAgreement_" + this.hfContactLogPk.Value);
        if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.referrerPath))
        {
            Int32.TryParse(this.hfBussinessClinicPk.Value.Trim(), out business_clinic_pk);
            Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);

            this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;
            this.commonAppSession.SelectedStoreSession.storeID = store_id;
            referrer_path = this.commonAppSession.SelectedStoreSession.referrerPath;
            this.commonAppSession.SelectedStoreSession.referrerPath = string.Empty;
        }
        else
            referrer_path = "walgreensHome.aspx";

        Response.Redirect(referrer_path);
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
        DataTable dt_contract_agreement = ds_contract_agreement.Tables[0];
        this.dtImmunizations = ds_contract_agreement.Tables[1];
        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(-1);
        this.createNewClinicLocation(false);

        if (this.isRestrictedStoreState)
        {
            //Set business address details to default clinic location
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address1"].Value = dt_contract_agreement.Rows[0]["address"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address2"].Value = dt_contract_agreement.Rows[0]["address2"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["city"].Value = dt_contract_agreement.Rows[0]["city"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["state"].Value = dt_contract_agreement.Rows[0]["state"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["zipCode"].Value = dt_contract_agreement.Rows[0]["zip"].ToString();
            this.isAddressDisabled = true;
        }

        this.bindImmunizationChecks();
        this.bindClinicLocations();
    }

    protected void imgBtnRemoveLocation_Click(object sender, EventArgs e)
    {
        ImageButton img_btn_remove = (ImageButton)sender;
        GridViewRow grd_row = (GridViewRow)img_btn_remove.NamingContainer;

        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(grd_row.RowIndex);
        this.bindImmunizationChecks();
        this.bindClinicLocations();
        if (this.isMOPreviousSeasonBusiness)
        {
            this.bindClinicAgreemtPreviousLocationToDropdown();
        }
    }

    protected void grdLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk_reassign_clinics = (CheckBox)e.Row.FindControl("chkReassignClinic");

            if (this.hasMutlipleClinics)
                chk_reassign_clinics.Visible = this.hasMutlipleClinics;
            //else if (!string.IsNullOrEmpty(this.isContractApproved))
            //    chk_reassign_clinics.Visible = Convert.ToBoolean(this.isContractApproved);

            DropDownList ddl_states = (DropDownList)e.Row.FindControl("ddlState");
            string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate");
            string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate");
            if (Convert.ToDateTime(restriction_start_date) < DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
            {
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString(), ApplicationSettings.getStoreStateRestrictions("storeState"));
            }
            else
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString(), "");

            if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
            {
                ddl_states.Items[0].Text = "-- Seleccione --";
            }
            if (ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString()) != null)
                ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString()).Selected = true;

            if (e.Row.RowIndex == 0)
            {
                ImageButton btn_remove_location = (ImageButton)e.Row.FindControl("imgBtnRemoveLocation");
                btn_remove_location.Visible = false;
            }
            var date_control = e.Row.FindControl("PickerAndCalendarFrom");
            this.setClinicDates(e.Row, true);

            if (this.dtImmunizations.Select("isSelected = 'True'").Count() > 0)
            {
                GridView grd_clinic_immunizations = (GridView)e.Row.FindControl("grdClinicImmunizations");
                this.bindClinicImmunizations(grd_clinic_immunizations, e.Row.RowIndex);
            }

            if (this.isAddressDisabled)
            {
                ((TextBox)e.Row.FindControl("txtAddress1")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtAddress2")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtCity")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtZipCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlState")).Enabled = false;
                ((CheckBox)e.Row.FindControl("chkReassignClinic")).Enabled = false;
                ((CheckBox)e.Row.FindControl("chkReassignClinic")).Visible = false;
            }

            var flu_exp_date = e.Row.FindControl("pcFluExpiryDate");
            //if (!string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString()) && grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString().Trim().Length != 0)
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"] != null && !string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString()))
            {
                DateTime dt_flu_exp_date = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString());
                ((PickerAndCalendar)flu_exp_date).getSelectedDate = dt_flu_exp_date;
                ((PickerAndCalendar)flu_exp_date).MinDate = (dt_flu_exp_date.Date >= DateTime.Today) ? DateTime.Now.AddDays(-1) : dt_flu_exp_date.AddDays(-1);
                ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
                ((TextBox)e.Row.FindControl("txtFluExpiryDate")).Text = dt_flu_exp_date.ToString("MM/dd/yyyy");
            }
            else
            {
                ((PickerAndCalendar)flu_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)flu_exp_date).getSelectedDate = ApplicationSettings.getVoucherMaxExpDate;
                ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
                ((TextBox)e.Row.FindControl("txtFluExpiryDate")).Text = ApplicationSettings.getVoucherMaxExpDate.ToString("MM/dd/yyyy");
            }

            var routine_exp_date = e.Row.FindControl("pcRoutineExpiryDate");
            //if (!string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString()) && grdLocations.DataKeys[e.Row.RowIndex].Values["pcFluExpiryDate"].ToString().Trim().Length != 0)
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"] != null && !string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString()))
            {
                DateTime dt_routine_exp_date = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString());
                ((PickerAndCalendar)routine_exp_date).getSelectedDate = dt_routine_exp_date;
                ((PickerAndCalendar)routine_exp_date).MinDate = (dt_routine_exp_date.Date >= DateTime.Today) ? DateTime.Now.AddDays(-1) : dt_routine_exp_date.AddDays(-1);
                ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
                ((TextBox)e.Row.FindControl("txtRoutineExpiryDate")).Text = dt_routine_exp_date.ToString("MM/dd/yyyy");
            }
            else
            {
                ((PickerAndCalendar)routine_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)routine_exp_date).getSelectedDate = DateTime.Today.AddYears(1);
                ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
                ((TextBox)e.Row.FindControl("txtRoutineExpiryDate")).Text = DateTime.Today.AddYears(1).ToString("MM/dd/yyyy");
            }
        }
    }

    protected void grdClinicImmunizations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string immunization_key = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "immunizationSpanishName" : "immunizationName";
            string payment_type_key = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "paymentTypeSpanishName" : "paymentTypeName";
            Label lbl_clinic_immunization = (Label)e.Row.FindControl("lblClinicImmunization");
            Label lbl_clinic_payment_type = (Label)e.Row.FindControl("lblClinicPaymentType");
            lbl_clinic_immunization.Text = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblImmunizationId")).Text))[0][immunization_key].ToString();
            lbl_clinic_payment_type.Text = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblImmunizationId")).Text) + " AND paymentTypeId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblPaymentTypeId")).Text))[0][payment_type_key].ToString();
        }
    }

    protected void imgBtnAddImmunization_Click(object sender, EventArgs e)
    {
        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(-1);
        this.bindImmunizationChecks();
        this.bindClinicLocations();
        this.grdImmunizationChecks.ShowFooter = true;
        this.grdImmunizationChecks.FooterRow.Visible = true;
        if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
        {
            DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
            DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");
            this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
            this.imgBtnAddImmunization.Enabled = false;
            this.lnkAddImmunization.Enabled = false;
        }
    }

    protected void imgBtnImmunizationOk_Click(object sender, EventArgs e)
    {
        ImageButton img_immunization = (ImageButton)sender;
        GridViewRow grd_immunization_row = (GridViewRow)img_immunization.NamingContainer;
        DropDownList ddl_immunization = (DropDownList)grd_immunization_row.FindControl("ddlImmunizationCheck");
        DropDownList ddl_payment_method = (DropDownList)grd_immunization_row.FindControl("ddlPaymentType");
        if (ddl_payment_method.SelectedValue == "" && hfUnconfirmedPayment.Value != "")
        {
            ddl_payment_method.SelectedIndex = ddl_payment_method.Items.IndexOf(ddl_payment_method.Items.FindByValue(hfUnconfirmedPayment.Value));
            hfUnconfirmedPayment.Value = "";
        }

        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];

        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(-1);

        if (ddl_immunization.SelectedIndex != 0 && ddl_payment_method.SelectedIndex != 0)
        {
            DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + ddl_immunization.SelectedValue + " AND paymentTypeId = " + ddl_payment_method.SelectedValue + " AND isSelected = 'False' AND isPaymentSelected = 'False'");
            if (dr_immunization != null && dr_immunization.Count() > 0)
            {
                dr_immunization[0]["isSelected"] = "True";
                dr_immunization[0]["isPaymentSelected"] = "True";

                XmlNodeList clinic_locations = this.xmlContractAgreement.SelectNodes("Clinics/clinic");
                foreach (XmlElement clinic in clinic_locations)
                {
                    XmlElement est_shots = this.xmlContractAgreement.CreateElement("immunization");
                    if (clinic.SelectSingleNode("./immunization[@pk = '" + ddl_immunization.SelectedValue + "' and @paymentTypeId = '" + ddl_payment_method.SelectedValue + "']") == null)
                    {
                        est_shots.SetAttribute("pk", ddl_immunization.SelectedValue);
                        est_shots.SetAttribute("paymentTypeId", ddl_payment_method.SelectedValue);
                        est_shots.SetAttribute("estimatedQuantity", string.Empty);
                        clinic.AppendChild(est_shots);
                    }
                }
            }

            if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                this.grdImmunizationChecks.ShowFooter = false;
                this.grdImmunizationChecks.FooterRow.Visible = false;
            }
            else if (this.grdImmunizationChecks.ShowFooter)
                this.grdImmunizationChecks.ShowFooter = false;
        }

        this.bindImmunizationChecks();
        this.bindClinicLocations();
    }

    protected void imgBtnRemoveImmunization_Click(object sender, CommandEventArgs e)
    {
        GridViewRow grid_row = (GridViewRow)((ImageButton)sender).NamingContainer;
        if (e.CommandArgument.ToString().ToLower() == "newimmunization")
        {
            if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                this.grdImmunizationChecks.FooterRow.Visible = false;
                this.grdImmunizationChecks.ShowFooter = false;

                this.imgBtnAddImmunization.Enabled = true;
                this.lnkAddImmunization.Enabled = true;
            }
            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.bindClinicLocations();
        }
        else
        {
            Label immunization_pk = (Label)this.grdImmunizationChecks.Rows[grid_row.RowIndex].FindControl("lblImmunizationPk");
            Label payment_pk = (Label)this.grdImmunizationChecks.Rows[grid_row.RowIndex].FindControl("lblPaymentTypeId");

            if (!string.IsNullOrEmpty(immunization_pk.Text))
            {
                this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
                DataRow[] immunization_selected = this.dtImmunizations.Select("immunizationId = '" + immunization_pk.Text + "' AND paymentTypeId = '" + payment_pk.Text + "'");
                if (immunization_selected.Count() > 0)
                {
                    immunization_selected[0]["isSelected"] = "False";
                    immunization_selected[0]["isPaymentSelected"] = "False";

                    //Clear previous data in the payment method
                    if (Convert.ToInt32(immunization_selected[0]["paymentTypeId"]) == 6)
                    {
                        immunization_selected[0]["name"] = null;
                        immunization_selected[0]["address1"] = null;
                        immunization_selected[0]["address2"] = null;
                        immunization_selected[0]["city"] = null;
                        immunization_selected[0]["state"] = null;
                        immunization_selected[0]["zip"] = null;
                        immunization_selected[0]["phone"] = null;
                        immunization_selected[0]["email"] = null;
                        immunization_selected[0]["tax"] = null;
                        immunization_selected[0]["copay"] = null;
                        immunization_selected[0]["copayValue"] = null;
                        immunization_selected[0]["isVoucherNeeded"] = null;
                        immunization_selected[0]["voucherExpirationDate"] = null;
                    }
                }
            }

            if (this.grdImmunizationChecks.FooterRow == null)
            {
                this.imgBtnAddImmunization.Enabled = true;
                this.lnkAddImmunization.Enabled = true;
            }
            else if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
                DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");
                this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
            }

            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.bindImmunizationChecks();
            this.bindClinicLocations();
        }
    }

    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationPk");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");

            DataRow[] row_payment_types = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(lbl_immunization_id.Text) + " AND paymentTypeId = " + Convert.ToInt32(lbl_payment_type_id.Text) + " AND isSelected = 'True' AND isPaymentSelected = 'True'");
            if (row_payment_types.Count() > 0)
            {
                if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
                {
                    Label lbl_Immunization = (Label)e.Row.FindControl("lblImmunizationCheck");
                    Label lbl_payment_type = (Label)e.Row.FindControl("lblPaymentType");
                    lbl_Immunization.Text = ((DataRowView)(e.Row.DataItem)).Row["immunizationSpanishName"].ToString();
                    lbl_payment_type.Text = ((DataRowView)(e.Row.DataItem)).Row["paymentTypeSpanishName"].ToString();
                }
                if (Convert.ToInt32(row_payment_types[0]["paymentTypeId"]) == 6)
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice_to = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowSendInvoiceTo");
                    System.Web.UI.HtmlControls.HtmlTableRow row_voucher_needed = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowVoucherNeeded");
                    System.Web.UI.HtmlControls.HtmlTableRow row_voucher_exp_date = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowExpirationDate");
                    DropDownList ddl_tax_exempt = (DropDownList)e.Row.FindControl("ddlTaxExempt");
                    DropDownList ddl_iscopay = (DropDownList)e.Row.FindControl("ddlIsCopay");
                    DropDownList ddl_voucher = (DropDownList)e.Row.FindControl("ddlVoucher");
                    DropDownList ddl_payment_state = (DropDownList)e.Row.FindControl("ddlPmtState");
                    //adding spanish list items if culture is es-MX or es-PR
                    if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
                    {
                        List<ListItem> items_voucher = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };
                        List<ListItem> items_iscopayr = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };
                        List<ListItem> items_tax_exempt = new List<ListItem>() { new ListItem("Seleccione", ""), new ListItem("Sí", "Yes"), new ListItem("No", "No") };

                        ddl_voucher.Items.Clear();
                        ddl_voucher.Items.AddRange(items_voucher.ToArray());
                        ddl_iscopay.Items.Clear();
                        ddl_iscopay.Items.AddRange(items_iscopayr.ToArray());
                        ddl_tax_exempt.Items.Clear();
                        ddl_tax_exempt.Items.AddRange(items_tax_exempt.ToArray());
                    }

                    var voucher_exp_date = (PickerAndCalendar)e.Row.FindControl("pcVaccineExpirationDate");
                    row_send_invoice_to.Visible = true;
                    row_voucher_needed.Visible = true;
                    ddl_payment_state.bindStates();

                    if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
                    {
                        ddl_payment_state.Items[0].Text = "-- Seleccione --";
                    }

                    if (this.commonAppSession.SelectedStoreSession.isModifyAgreement)
                    {
                        TextBox txt_pmt_name = ((TextBox)e.Row.FindControl("txtPmtName"));
                        TextBox txt_pmt_address1 = ((TextBox)e.Row.FindControl("txtPmtAddress1"));
                        TextBox txt_pmt_address2 = ((TextBox)e.Row.FindControl("txtPmtAddress2"));
                        TextBox txt_pmt_city = ((TextBox)e.Row.FindControl("txtPmtCity"));
                        TextBox txt_pmt_zip_code = ((TextBox)e.Row.FindControl("txtPmtZipCode"));
                        if (string.IsNullOrEmpty(row_payment_types[0]["name"].ToString()) && string.IsNullOrEmpty(row_payment_types[0]["address1"].ToString()) && string.IsNullOrEmpty(row_payment_types[0]["state"].ToString()))
                        {
                            txt_pmt_name.Text = this.lblLegalAttentionTo.Text;
                            txt_pmt_address1.Text = this.lblLegalAddress1.Text;
                            txt_pmt_address2.Text = this.lblLegalAddress2.Text;
                            txt_pmt_city.Text = this.lblLegalCity.Text;
                            if (ddl_payment_state.Items.FindByValue("" + this.lblLegalState.Text + "") != null)
                                ddl_payment_state.Items.FindByValue("" + this.lblLegalState.Text + "").Selected = true;
                            txt_pmt_zip_code.Text = this.lblLegalZipCode.Text;
                        }
                        else
                        {
                            if (ddl_payment_state.Items.FindByValue("" + row_payment_types[0]["state"].ToString() + "") != null)
                                ddl_payment_state.Items.FindByValue("" + row_payment_types[0]["state"].ToString() + "").Selected = true;
                        }
                    }
                    else
                    {
                        if (ddl_payment_state.Items.FindByValue("" + row_payment_types[0]["state"].ToString() + "") != null)
                            ddl_payment_state.Items.FindByValue("" + row_payment_types[0]["state"].ToString() + "").Selected = true;
                    }

                    if (ddl_tax_exempt.Items.FindByValue("" + row_payment_types[0]["tax"].ToString() + "") != null)
                        ddl_tax_exempt.Items.FindByValue("" + row_payment_types[0]["tax"].ToString() + "").Selected = true;

                    if (ddl_iscopay.Items.FindByValue("" + row_payment_types[0]["copay"].ToString() + "") != null)
                        ddl_iscopay.Items.FindByValue("" + row_payment_types[0]["copay"].ToString() + "").Selected = true;

                    if (ddl_voucher.Items.FindByValue("" + row_payment_types[0]["isVoucherNeeded"].ToString() + "") != null)
                        ddl_voucher.Items.FindByValue("" + row_payment_types[0]["isVoucherNeeded"].ToString() + "").Selected = true;

                    //Set default max and min voucher expiration dates
                    if (!string.IsNullOrEmpty(row_payment_types[0]["voucherExpirationDate"].ToString()))
                    {
                        ((PickerAndCalendar)voucher_exp_date).getSelectedDate = Convert.ToDateTime(row_payment_types[0]["voucherExpirationDate"].ToString());
                        ((TextBox)e.Row.FindControl("txtVaccineExpirationDate")).Text = Convert.ToDateTime(row_payment_types[0]["voucherExpirationDate"].ToString()).ToString("MM/dd/yyyy");

                        if (DateTime.Now < Convert.ToDateTime(row_payment_types[0]["voucherExpirationDate"].ToString()))
                            ((PickerAndCalendar)voucher_exp_date).MinDate = DateTime.Now.AddDays(-1);
                        else
                            ((PickerAndCalendar)voucher_exp_date).MinDate = Convert.ToDateTime(row_payment_types[0]["voucherExpirationDate"].ToString()).AddDays(-1);
                    }
                    else
                    {
                        ((PickerAndCalendar)voucher_exp_date).getSelectedDate = (row_payment_types[0]["immunizationName"].ToString().Contains("Influenza")) ? ApplicationSettings.getVoucherMaxExpDate : DateTime.Today.AddYears(1);
                        ((PickerAndCalendar)voucher_exp_date).MinDate = DateTime.Now.AddDays(-1);
                    }

                    ((PickerAndCalendar)voucher_exp_date).MaxDate = (row_payment_types[0]["immunizationName"].ToString().Contains("Influenza")) ? ApplicationSettings.getVoucherMaxExpDate.AddDays(1) : DateTime.Today.AddYears(1).AddDays(1);

                    if (ddl_voucher.SelectedValue == "Yes")
                        row_voucher_exp_date.Attributes.CssStyle.Add("display", "block");
                    else
                        row_voucher_exp_date.Attributes.CssStyle.Add("display", "none");

                    lbl_price.Text = "$ " + row_payment_types[0]["directBillPrice"].ToString();
                }
                else
                    lbl_price.Text = (row_payment_types[0]["price"] == DBNull.Value) ? "N/A" : "$ " + row_payment_types[0]["price"].ToString();
            }
        }
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        // to clear the date in picker if we select MO or DC state by non admin user
        //calling bind clinic to set min dates dates for date pickers
        DropDownList ddl_state_clinic_location = (DropDownList)sender;
        if (ddl_state_clinic_location != null)
        {
            GridViewRow row = (GridViewRow)ddl_state_clinic_location.Parent.Parent;
            if (row != null)
            {
                var date_control = row.FindControl("PickerAndCalendarFrom");
                DateTime seleted_date = ((PickerAndCalendar)date_control).getSelectedDate;

                if ((ddl_state_clinic_location.SelectedValue == "MO" || ddl_state_clinic_location.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin && seleted_date < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Parse("01/01/0001");
                }
            }
            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.bindImmunizationChecks();
            this.bindClinicLocations();
        }
    }

    /// <summary>
    /// Binds Time Picker
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void displayTime_Picker(object sender, EventArgs e)
    {
        StringBuilder script_text = new StringBuilder();
        GridViewRow gvr = (GridViewRow)((sender as TextBox).NamingContainer);
        TextBox txt_end_time = new TextBox();
        txt_end_time = (TextBox)gvr.FindControl("txtEndTime");
        script_text = ApplicationSettings.displayTimePicker((sender as TextBox).ClientID, txt_end_time.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DateScript" + gvr.RowIndex, script_text.ToString(), true);
    }

    protected void lnkChangeCulture_Click(object sender, EventArgs e)
    {
        int business_clinic_pk, store_id;
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        Int32.TryParse(this.hfBussinessClinicPk.Value.Trim(), out business_clinic_pk);
        Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);

        this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = this.contactLogPk;
        this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;
        this.commonAppSession.SelectedStoreSession.storeID = store_id;

        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            DataSet ds_contract_agreement = new DataSet();
            ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
            this.prepareClinicAggreementXML(true);
            ds_contract_agreement.Tables[0].Rows[0]["clinicAgreementXml"] = this.xmlContractAgreement.OuterXml;
            bindContractAgreement();
            if (this.isMOPreviousSeasonBusiness)
            {
                this.bindClinicAgreemtPreviousLocationToDropdown();
            }
            Session["contractAgreement_" + this.contactLogPk.ToString()] = ds_contract_agreement;
        }
        else
            Response.Redirect("walgreensLandingPage.aspx");
    }

    protected void imgbtnAddPreviousClinic_Click(object sender, ImageClickEventArgs e)
    {
        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        this.updatePaymentTypeData();
        this.bindImmunizationChecks();
        // add selected clinic location to xmlContractAgreement by comparing address fields in the previousClinicLocation data table 
        if (this.ddlClinicLocations.SelectedIndex != 0)
        {
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.createNewClinicLocation(false);
            //Bind clinic locations
            this.bindClinicLocations();
            //update the clinic locations dropdown
            this.bindClinicAgreemtPreviousLocationToDropdown();
        }
        else
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "MOStateAddClinicValidation") + "'); ", true);
            DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
            DataTable dt_contract_agreement = ds_contract_agreement.Tables[0];
            this.dtImmunizations = ds_contract_agreement.Tables[1];
            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.createNewClinicLocation(false);
            //XmlNode newNode = xmlContractAgreement.LastChild;
            if (this.isRestrictedStoreState)
            {
                //Set business address details to default clinic location
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address1"].Value = dt_contract_agreement.Rows[0]["address"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address2"].Value = dt_contract_agreement.Rows[0]["address2"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["city"].Value = dt_contract_agreement.Rows[0]["city"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["state"].Value = dt_contract_agreement.Rows[0]["state"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["zipCode"].Value = dt_contract_agreement.Rows[0]["zip"].ToString();
                this.isAddressDisabled = true;
            }
            this.bindImmunizationChecks();
            this.bindClinicLocations();
        }
    }
    #endregion

    #region ------------ PRIVATE METHODS -------------
    /// <summary>
    /// Set control properties
    /// </summary>
    /// <param name="control"></param>
    /// <param name="control_type"></param>
    /// <param name="tool_tip"></param>
    private void setControlProperty(Control control, string control_type, string tool_tip, bool is_invalid)
    {
        switch (control_type.ToLower())
        {
            case "textbox":
                if (is_invalid)
                {
                    ((TextBox)control).Attributes.Add("title", tool_tip);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((TextBox)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }

                break;
            case "dropdownlist":
                if (is_invalid)
                {
                    ((DropDownList)control).ToolTip = tool_tip;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((DropDownList)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }

                break;
        }
    }

    /// <summary>
    /// Validate Immunization Values and Clinic Locations
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateAgreement(string validation_group)
    {
        string error_message = string.Empty;
        bool hide_clear_immunization = false;

        bool is_valid = this.ValidateImmunizations(validation_group, ref error_message, ref hide_clear_immunization);
        is_valid = this.ValidateLocations(validation_group) && is_valid;

        if (!string.IsNullOrEmpty(error_message))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showImmConfirmationWarning('" + error_message + "','" + hide_clear_immunization + "');", true);
            is_valid = false;
        }
        else
        {
            if (!is_valid)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
        }

        return is_valid;
    }

    /// <summary>
    /// Validate clinic locations
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateLocations(string validation_group)
    {
        bool is_valid = true;
        string value = string.Empty;

        if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(this.txtEmails.Text))
        {
            this.setControlProperty(this.txtEmails, "textbox", "'Email Agreement to:' field is required", true);
            is_valid = false;
        }
        else if (!string.IsNullOrEmpty(this.txtEmails.Text) && !this.txtEmails.Text.validateMultipleEmails())
        {
            this.setControlProperty(this.txtEmails, "textbox", "'Email Agreement to:' Please enter valid Email", true);
            is_valid = false;
        }
        else
            this.setControlProperty(this.txtEmails, "textbox", string.Empty, false);

        //Validate clinic locations
        foreach (GridViewRow row in grdLocations.Rows)
        {
            bool is_required = !((CheckBox)row.FindControl("chkNoClinic")).Checked;

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text) && !(((TextBox)row.FindControl("txtLocalContactName")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name: < > characters are not allowed", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1 is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && !(((TextBox)row.FindControl("txtAddress1")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1: < > characters are not allowed", true);
                is_valid = false;
            }
            else if (validation_group == "WalgreensUser" && !string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && ((TextBox)row.FindControl("txtAddress1")).Enabled && !(((TextBox)row.FindControl("txtAddress1")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", string.Empty, false);

            if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && !(((TextBox)row.FindControl("txtAddress2")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", "Address2: < > characters are not allowed", true);
                is_valid = false;
            }
            else if (validation_group == "WalgreensUser" && !string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && ((TextBox)row.FindControl("txtAddress2")).Enabled && !(((TextBox)row.FindControl("txtAddress2")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                is_valid = false;
            }
            else
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", string.Empty, false);
            }
            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text) && !(((TextBox)row.FindControl("txtCity")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City: < > characters are not allowed", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtCity"), "textbox", string.Empty, false);

            value = ((TextBox)row.FindControl("txtLocalContactPhone")).Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(value))
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Phone is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(value) && !value.validatePhone())
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Valid Phone number is required(ex: ###-###-####)", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Zip Code is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text) && !(((TextBox)row.FindControl("txtZipCode")).Text.validateZipCode()))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Invalid Zip Code", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Email is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text) && !(((TextBox)row.FindControl("txtLocalContactEmail")).Text.validateEmail()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Invalid Email", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && ((DropDownList)row.FindControl("ddlState")).SelectedItem.Value.Trim() == "")
            {
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", "Select State", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", string.Empty, false);

            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", "Start Time is required", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && is_required && string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", "End Time is required", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", string.Empty, false);

            value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");
            ComponentArt.Web.UI.Calendar calendarctrl = (ComponentArt.Web.UI.Calendar)((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).FindControl("picker1");
            if (value == "01/01/0001" && validation_group == "WalgreensUser" && is_required)
            {
                calendarctrl.BorderColor = System.Drawing.Color.Red;
                calendarctrl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                calendarctrl.BorderWidth = Unit.Parse("1px");
                calendarctrl.ToolTip = "Clinic Date is required";
                is_valid = false;
            }
            else
            {
                calendarctrl.Style.Add("border", "1px solid gray");
                calendarctrl.ToolTip = string.Empty;
            }

            GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");

            foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
            {
                if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text))
                {
                    this.setControlProperty(imm_row.FindControl("txtClinicImmunizationShots"), "textbox", "Estimated Shots is required", true);
                    is_valid = false;
                }
                else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text) && !(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text.validateEstimatedShots()))
                {
                    this.setControlProperty(imm_row.FindControl("txtClinicImmunizationShots"), "textbox", "Invalid Estimated Shots value", true);
                    is_valid = false;
                }
                else
                    this.setControlProperty(imm_row.FindControl("txtClinicImmunizationShots"), "textbox", string.Empty, false);
            }
        }

        //Validating WALGREENS CO DATA
        if (!string.IsNullOrEmpty(txtWalgreenName.Text) && !txtWalgreenName.Text.validateJunkCharacters())
        {
            this.setControlProperty(Page.FindControl("txtWalgreenName"), "textbox", "Walgreens Co. Name:: , < > characters are not allowed", true);
            is_valid = false;
        }
        if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(txtWalgreenTitle.Text))
        {
            this.setControlProperty(Page.FindControl("txtWalgreenTitle"), "textbox", "Enter the title for the Walgreens Co. representative named in the Name field", true);
            is_valid = false;
        }
        else if (!string.IsNullOrEmpty(txtWalgreenTitle.Text) && !txtWalgreenTitle.Text.validateJunkCharacters())
        {
            this.setControlProperty(Page.FindControl("txtWalgreenTitle"), "textbox", "Walgreens Co. Title:: , < > characters are not allowed", true);
            is_valid = false;
        }
        else
            this.setControlProperty(Page.FindControl("txtWalgreenTitle"), "textbox", string.Empty, false);

        if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(txtDistrictNum.Text))
        {
            this.setControlProperty(Page.FindControl("txtDistrictNum"), "textbox", "Enter the Walgreens Co. District number", true);
            is_valid = false;
        }
        else if (!string.IsNullOrEmpty(txtDistrictNum.Text) && !txtDistrictNum.Text.validateIsNumeric())
        {
            this.setControlProperty(Page.FindControl("txtDistrictNum"), "textbox", "'Walgreens Co.:: The District number should be a numeric value", true);
            is_valid = false;
        }
        else
            this.setControlProperty(Page.FindControl("txtDistrictNum"), "textbox", string.Empty, false);

        return is_valid;
    }

    /// <summary>
    /// Validate Immunization values
    /// </summary>
    /// <param name="validation_group"></param>
    /// <param name="error_message"></param>
    /// <param name="hide_clear_immunization"></param>
    /// <returns></returns>
    private bool ValidateImmunizations(string validation_group, ref string error_message, ref bool hide_clear_immunization)
    {
        bool is_valid = true;
        string value = string.Empty;

        if (validation_group == "WalgreensUser" && this.grdImmunizationChecks.Rows.Count == 0)
        {
            string immunization = ((DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck")).SelectedItem.Value;
            string payment_type = ((DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType")).SelectedItem.Value;

            this.setControlProperty(this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck"), "dropdownlist", (immunization == "" ? "Immunization is required" : string.Empty), (immunization == "" ? true : false));
            this.setControlProperty(this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType"), "dropdownlist", (payment_type == "" ? "Payment Type is required" : string.Empty), (payment_type == "" ? true : false));

            if (immunization != "" && payment_type != "")
            {
                error_message = "Your selections for Immunization and Payment Method have not been confirmed. Click \"OK\" to confirm your selections. The contract will not be emailed until you have entered an Estimated Volume for confirmed immunizations.";
                this.hfUnconfirmedPayment.Value = payment_type;
                hide_clear_immunization = true;
            }

            return false;
        }

        if ((validation_group == "WalgreensUser") && (this.grdImmunizationChecks.Rows.Count > 0) && (this.grdImmunizationChecks.FooterRow != null) && (this.grdImmunizationChecks.FooterRow.Visible))
        {
            DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
            DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");

            if ((ddl_immunization_checks != null && ddl_immunization_checks.SelectedItem.Value != "") && (ddl_payment_type != null && ddl_payment_type.SelectedItem.Value != ""))
            {
                error_message = "Your selections for Immunization and Payment Method have not been confirmed. Click \"OK\" to confirm your selections or \"Clear\" to clear the unconfirmed selections. The contract will not be emailed until you have entered an Estimated Volume for confirmed immunizations.";
                this.hfUnconfirmedPayment.Value = ddl_payment_type.SelectedItem.Value;
            }
        }
        else
        {
            foreach (GridViewRow imm_row in this.grdImmunizationChecks.Rows)
            {
                System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice_to = ((System.Web.UI.HtmlControls.HtmlTableRow)imm_row.FindControl("rowSendInvoiceTo"));

                if (row_send_invoice_to.Visible)
                {
                    if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtName")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtName"), "textbox", "Name to Send Invoice is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtName")).Text) && !(((TextBox)imm_row.FindControl("txtPmtName")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtName"), "textbox", "Name: , < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtName")), "textbox", string.Empty, false);

                    if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtAddress1")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtAddress1"), "textbox", "Address1 is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtAddress1")).Text) && !(((TextBox)imm_row.FindControl("txtPmtAddress1")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtAddress1"), "textbox", " Address1: , < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtAddress1")), "textbox", string.Empty, false);

                    if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtAddress2")).Text) && !(((TextBox)imm_row.FindControl("txtPmtAddress2")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtAddress2"), "textbox", " Address2: , < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtAddress2")), "textbox", string.Empty, false);

                    if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtCity")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtCity"), "textbox", "City is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtCity")).Text) && !(((TextBox)imm_row.FindControl("txtPmtCity")).Text).validateJunkCharacters())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtCity"), "textbox", "City: < > characters are not allowed", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtCity")), "textbox", string.Empty, false);

                    if (validation_group == "WalgreensUser" && ((DropDownList)imm_row.FindControl("ddlPmtState")).SelectedItem.Value.Trim() == "")
                    {
                        this.setControlProperty(imm_row.FindControl("ddlPmtState"), "dropdownlist", "State is required", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((DropDownList)imm_row.FindControl("ddlPmtState")), "dropdownlist", string.Empty, false);

                    if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtZipCode")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtZipCode"), "textbox", "Zip Code is required", true);
                        is_valid = false;
                    }
                    else if (!(((TextBox)imm_row.FindControl("txtPmtZipCode")).Text).validateZipCode() && !string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtZipCode")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtZipCode"), "textbox", "Invalid Zip Code", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtZipCode")), "textbox", string.Empty, false);

                    value = ((TextBox)imm_row.FindControl("txtPmtPhone")).Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
                    if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(value))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtPhone"), "textbox", "Phone number is required", true);
                        is_valid = false;
                    }
                    else if (!value.validatePhone() && !string.IsNullOrEmpty(value))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtPhone"), "textbox", "Valid Phone number is required(ex: ###-###-####)", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtPhone")), "textbox", string.Empty, false);

                    if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtEmail")).Text))
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtEmail"), "textbox", "Email is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtEmail")).Text) && !(((TextBox)imm_row.FindControl("txtPmtEmail")).Text).validateEmail())
                    {
                        this.setControlProperty(imm_row.FindControl("txtPmtEmail"), "textbox", "Invalid Email", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtEmail")), "textbox", string.Empty, false);

                    if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text))
                    {
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", "Verfiy Email is required", true);
                        is_valid = false;
                    }
                    else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text) && !(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text).validateEmail())
                    {
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", "Invalid Verfiy Email", true);
                        is_valid = false;
                    }
                    else
                    {
                        string email = ((TextBox)imm_row.FindControl("txtPmtEmail")).Text;
                        if (email != "" && email.validateEmail())
                        {
                            if (string.Compare(email, ((TextBox)imm_row.FindControl("txtPmtVerifyEmail")).Text, false) != 0)
                            {
                                this.setControlProperty(imm_row.FindControl("txtPmtVerifyEmail"), "textbox", "Email and Verify Email should match", true);
                                is_valid = false;
                            }
                            else
                                this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", string.Empty, false);
                        }
                        else
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtPmtVerifyEmail")), "textbox", string.Empty, false);
                    }

                    if (validation_group == "WalgreensUser" && ((DropDownList)imm_row.FindControl("ddlTaxExempt")).SelectedItem.Value == "")
                    {
                        this.setControlProperty(imm_row.FindControl("ddlTaxExempt"), "dropdownlist", "Select Is Employer Tax Exempt", true);
                        is_valid = false;
                    }
                    else
                        this.setControlProperty(((DropDownList)imm_row.FindControl("ddlTaxExempt")), "dropdownlist", string.Empty, false);

                    if (validation_group == "WalgreensUser" && ((DropDownList)imm_row.FindControl("ddlIsCopay")).SelectedItem.Value == "")
                    {
                        this.setControlProperty(imm_row.FindControl("ddlIsCopay"), "dropdownlist", "Select IsCopay", true);
                        is_valid = false;
                    }
                    else if (((DropDownList)imm_row.FindControl("ddlIsCopay")).SelectedItem.Value.Trim() == "Yes")
                    {
                        if (validation_group == "WalgreensUser" && ((TextBox)imm_row.FindControl("txtCoPay")).Text.Trim().Length == 0)
                        {
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", "Copay is required", true);
                            is_valid = false;
                        }
                        else if (((TextBox)imm_row.FindControl("txtCoPay")).Text.Trim().Length != 0 && !((TextBox)imm_row.FindControl("txtCoPay")).Text.validateDecimal())
                        {
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", "Invalid Copay", true);
                            is_valid = false;
                        }
                        else
                            this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", string.Empty, false);
                    }
                    else
                    {
                        this.setControlProperty(((DropDownList)imm_row.FindControl("ddlIsCopay")), "dropdownlist", string.Empty, false);
                        this.setControlProperty(((TextBox)imm_row.FindControl("txtCoPay")), "textbox", string.Empty, false);
                    }

                    if (validation_group == "WalgreensUser" && (((DropDownList)imm_row.FindControl("ddlVoucher")).SelectedItem.Value == "" || ((DropDownList)imm_row.FindControl("ddlVoucher")).SelectedItem.Value.Trim() == "0"))
                    {
                        this.setControlProperty(imm_row.FindControl("ddlVoucher"), "dropdownlist", "Select Voucher Needed", true);
                        is_valid = false;
                    }
                    //else if (validation_group == "WalgreensUser" && ((DropDownList)imm_row.FindControl("ddlVoucher")).SelectedItem.Value.Trim() == "0")
                    //{
                    //    this.setControlProperty(imm_row.FindControl("ddlVoucher"), "dropdownlist", "Select Voucher Needed", true);
                    //    is_valid = false;
                    //}
                    else
                        this.setControlProperty(((DropDownList)imm_row.FindControl("ddlVoucher")), "dropdownlist", string.Empty, false);
                }
            }
        }

        return is_valid;
    }

    /// <summary>
    /// Check if all the immunizations are added to clinics
    /// </summary>
    /// <returns></returns>
    private bool checkClinicImmunizations()
    {
        bool is_valid = true;
        XmlNodeList clinic_locations = this.xmlContractAgreement.SelectNodes("ClinicAgreement/Clinics/clinic");
        XmlNodeList contract_immunizations = this.xmlContractAgreement.SelectNodes("ClinicAgreement/Immunizations/Immunization");

        foreach (XmlElement clinic in clinic_locations)
        {
            if (clinic.SelectNodes("./immunization").Count != contract_immunizations.Count)
            {
                is_valid = false;
                foreach (XmlElement immunization in contract_immunizations)
                {
                    if (clinic.SelectSingleNode("./immunization[@pk = '" + immunization.Attributes["pk"].Value + "' and @paymentTypeId = '" + immunization.Attributes["paymentTypeId"].Value + "']") == null)
                    {
                        XmlElement est_shots = this.xmlContractAgreement.CreateElement("immunization");
                        est_shots.SetAttribute("pk", immunization.Attributes["pk"].Value);
                        est_shots.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
                        est_shots.SetAttribute("estimatedQuantity", string.Empty);
                        clinic.AppendChild(est_shots);
                    }
                }
            }
        }

        return is_valid;
    }

    /// <summary>
    /// Creates new clinic location
    /// </summary>
    /// <param name="is_default"></param>
    private void createNewClinicLocation(bool is_default)
    {
        double clinics_count = 0;
        XmlElement clinic_locations_node;
        if (is_default)
            clinic_locations_node = this.xmlContractAgreement.CreateElement("Clinics");
        else
            clinic_locations_node = (XmlElement)this.xmlContractAgreement.SelectSingleNode("Clinics");

        clinics_count = (is_default) ? 0 : clinic_locations_node.SelectNodes("clinic").Count;

        XmlElement clinic_ele = this.xmlContractAgreement.CreateElement("clinic");
        string address1, address2, city, state, zip;
        address1 = string.Empty;
        address2 = string.Empty;
        city = string.Empty;
        state = string.Empty;
        zip = string.Empty;
        if (this.ddlClinicLocations.Visible && this.ddlClinicLocations.SelectedItem.Value != "0")
        {
            DataRow added_row = this.previousClinicLocation.Select("rowId='" + this.ddlClinicLocations.SelectedItem.Value + "'").FirstOrDefault();
            address1 = added_row["naClinicAddress1"].ToString();
            address2 = added_row["naClinicAddress2"].ToString();
            city = added_row["naClinicCity"].ToString();
            state = added_row["naClinicState"].ToString();
            zip = added_row["naClinicZip"].ToString();
        }
        clinic_ele.SetAttribute("clinicLocation", ((this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "UBICACIÓN DE LA CLÍNICA " : "CLINIC LOCATION ") + ((clinics_count >= 26) ? ((Char)(65 + (clinics_count % 26 == 0 ? Math.Ceiling(clinics_count / 26) - 1 : Math.Ceiling(clinics_count / 26) - 2))).ToString() + "" + ((Char)(65 + (clinics_count % 26))).ToString() : ((Char)(65 + clinics_count % 26)).ToString()));
        clinic_ele.SetAttribute("localContactName", string.Empty);
        clinic_ele.SetAttribute("LocalContactEmail", string.Empty);
        clinic_ele.SetAttribute("LocalContactPhone", string.Empty);
        clinic_ele.SetAttribute("clinicDate", string.Empty);
        clinic_ele.SetAttribute("startTime", string.Empty);
        clinic_ele.SetAttribute("endTime", string.Empty);
        clinic_ele.SetAttribute("Address1", address1);
        clinic_ele.SetAttribute("Address2", address2);
        clinic_ele.SetAttribute("city", city);
        clinic_ele.SetAttribute("state", state);
        clinic_ele.SetAttribute("zipCode", zip);
        clinic_ele.SetAttribute("isReassign", "0");
        clinic_ele.SetAttribute("isRemoved", "0");
        clinic_ele.SetAttribute("isNoClinic", "0");
        clinic_ele.SetAttribute("fluExpiryDate", string.Empty);
        clinic_ele.SetAttribute("routineExpiryDate", string.Empty);

        //Add selected immunizations to the new clinic location
        if (!is_default)
        {
            XmlNodeList clinic_immunizations = clinic_locations_node.FirstChild.ChildNodes;

            foreach (XmlElement immunization in clinic_immunizations)
            {
                XmlElement estshots_node;
                estshots_node = this.xmlContractAgreement.CreateElement("immunization");
                estshots_node.SetAttribute("pk", immunization.Attributes["pk"].Value);
                estshots_node.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
                estshots_node.SetAttribute("estimatedQuantity", string.Empty);
                clinic_ele.AppendChild(estshots_node);
            }
        }

        clinic_locations_node.AppendChild(clinic_ele);
        this.xmlContractAgreement.AppendChild(clinic_locations_node);
    }

    /// <summary>
    /// Gets and binds contract agreement
    /// </summary>
    private void bindContractAgreement()
    {
        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        this.logger.Info("Method {0} accessed from {1} page by user {2} with contactlogpk: {3}; Browser: {4}; - START", "bindContractAgreement",
                   System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName, this.contactLogPk.ToString()
                   , browser.Browser + " " + browser.Version);

        DataSet ds_contract_agreement = new DataSet();
        DataTable dt_contract_agreement = new DataTable();

        //Get contract agreement from session if redirected from Spanish version
        if (Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
            ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
        else
            ds_contract_agreement = dbOperation.getClinicAgreement("Walgreens", this.contactLogPk.ToString(), this.commonAppSession.LoginUserInfoSession.Email.ToString(), 2015, true);

        Session["contractAgreement_" + this.contactLogPk.ToString()] = ds_contract_agreement;

        dt_contract_agreement = ds_contract_agreement.Tables[0]; //Contract agreement details

        //When contract agreement is deleted redirect user to home page for users coming from old community off-site clinic agreement email
        if ((dt_contract_agreement == null) || (dt_contract_agreement != null && dt_contract_agreement.Rows.Count == 0))
            Response.Redirect("walgreensHome.aspx");
        else
        {
            //Store Immunization checks and payment types in session
            this.dtImmunizations = ds_contract_agreement.Tables[1];

            if (ApplicationSettings.isRestrictedStoreState(dt_contract_agreement.Rows[0]["storeState"].ToString(), this.commonAppSession.LoginUserInfoSession.UserRole))
            {
                this.previousClinicLocation = dbOperation.GetAllPreviousClinicLocations(Convert.ToInt32(dt_contract_agreement.Rows[0]["businessPk"].ToString()));
                //Remove from dropdown if only business location is added
                if (this.previousClinicLocation.Rows.Count == 1 && this.previousClinicLocation.Rows[0]["naClinicAddress"].ToString() == getBusinessLocationAddress())
                    this.previousClinicLocation.Rows.RemoveAt(0);
                if (this.previousClinicLocation.Rows.Count > 0)
                    this.isMOPreviousSeasonBusiness = true;
                this.isRestrictedStoreState = true;
                this.isAddressDisabled = true;
                if (!string.IsNullOrEmpty(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString()))
                {
                    this.xmlContractAgreement.LoadXml(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString());

                    foreach (XmlNode addedNode in this.xmlContractAgreement.SelectNodes("//Clinics/clinic"))
                    {
                        if ((addedNode.Attributes["isRemoved"] != null && addedNode.Attributes["isRemoved"].Value == "1") || !isPreviousSeasonClinic(addedNode))
                            addLocationToDropdown((XmlElement)addedNode);
                    }
                }
            }

            //Check if contract agreement is new            
            if (string.IsNullOrEmpty(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString()))
            {
                //Create default clinic location
                this.createNewClinicLocation(true);

                //Set business address details to default clinic location
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["Address1"].Value = dt_contract_agreement.Rows[0]["address"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["Address2"].Value = dt_contract_agreement.Rows[0]["address2"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["city"].Value = dt_contract_agreement.Rows[0]["city"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["state"].Value = dt_contract_agreement.Rows[0]["state"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["zipCode"].Value = dt_contract_agreement.Rows[0]["zip"].ToString();
                if (this.isMOPreviousSeasonBusiness)
                    this.bindClinicAgreemtPreviousLocationToDropdown();

                this.txtDistrictNum.Text = dt_contract_agreement.Rows[0]["districtNumber"].ToString();
                this.txtWalgreenTitle.Text = this.commonAppSession.LoginUserInfoSession.UserRole;
                this.PickerAndCalendarWalgreensDate.getSelectedDate = DateTime.Now;
                this.PickerAndCalendarWalgreensDate.MinDate = DateTime.Now.AddDays(-1);
            }
            else
            {
                this.isContractApproved = dt_contract_agreement.Rows[0]["isApproved"].ToString();
                //this.isContractModified = dt_contract_agreement.Rows[0]["isModified"].ToString();
                this.isEmailSent = Convert.ToBoolean(dt_contract_agreement.Rows[0]["isEmailSent"].ToString());
                //if (this.isContractModified == "True" && !this.commonAppSession.SelectedStoreSession.isModifyAgreement)
                //    this.xmlContractAgreement.LoadXml(dt_contract_agreement.Rows[0]["origClinicAgreementXml"].ToString());
                //else
                this.xmlContractAgreement.LoadXml(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString());
                this.bindClientAgreementXml();
            }
            this.businessName = dt_contract_agreement.Rows[0]["businessName"].ToString();
            this.hfLastUpdatedDate.Value = dt_contract_agreement.Rows[0]["updatedDate"].ToString();
            this.txtElectronicSign.Text = dt_contract_agreement.Rows[0]["signature"].ToString();

            //Add isReassign attribute if not exists in the clinic locations node
            foreach (XmlNode clinic in this.xmlContractAgreement.SelectNodes("//Clinics/clinic"))
            {
                if (clinic.Attributes["isReassign"] == null)
                    ((XmlElement)clinic).SetAttribute("isReassign", "0");
            }
            this.logger.Info("Binding Clinic locations for {0}", this.businessName);
            //Bind clinic locations, immunization checks and payment types
            this.bindClinicLocations();
            this.logger.Info("Binding immunizations for {0}", this.businessName);
            this.bindImmunizationChecks();
            if (this.isMOPreviousSeasonBusiness)
                bindClinicAgreemtPreviousLocationToDropdown();

            //setting previous clinics clinic agreement controls 
            if (Convert.ToBoolean(dt_contract_agreement.Rows[0]["isPreviousSeason"]) && !this.commonAppSession.SelectedStoreSession.isModifyAgreement)
            {
                this.btnSendLater.Visible = false;
                this.disableControls(true);
                if (!string.IsNullOrEmpty(this.isContractApproved) && Convert.ToBoolean(this.isContractApproved))
                    this.btnSendMail.Visible = true;
                else
                    this.btnSendMail.Visible = false;
            }
            //else 
            if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 2)
            {
                this.disableControls(true);
                if (this.commonAppSession.SelectedStoreSession.isModifyAgreement)
                    this.enableImzSelection();
                else
                {
                    if (!string.IsNullOrEmpty(this.isContractApproved))
                    {
                        if (Convert.ToBoolean(this.isContractApproved))
                        {
                            this.rbtnApprove.Checked = true;
                            this.rbtnApprove.Enabled = false;
                            this.txtElectronicSign.Visible = true;
                            this.txtElectronicSign.Enabled = false;
                            if (!Convert.ToBoolean(dt_contract_agreement.Rows[0]["isCurrent"].ToString()))
                            {
                                this.txtEmails.Enabled = false;
                                this.btnSendMail.Visible = false;
                            }
                            else
                            {
                                this.txtEmails.Enabled = true;
                            }

                            this.lblErrorMessage.Text = "The Walgreens Community Off-Site Agreement has been submitted by " + dt_contract_agreement.Rows[0]["approvedOrRejectedBy"].ToString();
                            this.lblErrorMessage.Visible = true;
                        }
                        else
                        {
                            this.rbtnReject.Checked = true;
                            this.rbtnReject.Enabled = false;
                            this.trNotes.Visible = true;
                            this.txtNotes.Text = dt_contract_agreement.Rows[0]["notes"].ToString();
                            this.txtNotes.Enabled = false;
                            this.txtEmails.Enabled = false;
                            this.btnSendMail.Enabled = false;
                        }
                        this.tblApproval.Visible = true;
                        this.tblApproval.Disabled = true;
                    }
                    else
                    {
                        this.txtEmails.Enabled = false;
                        this.btnSendMail.Enabled = false;
                        this.tblApproval.Visible = false;
                    }
                    this.btnSendLater.Visible = false;
                }
            }
            else if (!string.IsNullOrEmpty(this.isContractApproved))
            {
                if (Convert.ToBoolean(this.isContractApproved))
                {
                    this.disableControls(true);
                    this.rbtnApprove.Checked = true;
                    this.txtElectronicSign.Visible = true;
                    if (!Convert.ToBoolean(dt_contract_agreement.Rows[0]["isCurrent"].ToString()))
                    {
                        this.txtEmails.Enabled = false;
                        this.btnSendMail.Visible = false;
                    }
                    else
                    {
                        this.txtEmails.Enabled = true;
                    }
                    this.lblErrorMessage.Text = "The Walgreens Community Off-Site Agreement has been submitted by " + dt_contract_agreement.Rows[0]["approvedOrRejectedBy"].ToString();
                    this.lblErrorMessage.Visible = true;
                }
                else
                {
                    this.rbtnReject.Checked = true;
                    this.rbtnReject.Enabled = false;
                    this.trNotes.Visible = true;
                    this.txtNotes.Text = dt_contract_agreement.Rows[0]["notes"].ToString();
                    this.txtNotes.Enabled = false;
                    if (ds_contract_agreement.Tables[2].Rows.Count > 0) //contract modified & pharmacy manager opened through email
                    {
                        this.disableControls(true);
                        this.txtEmails.Enabled = false;
                        this.btnSendMail.Enabled = false;
                    }
                }
                this.txtElectronicSign.Enabled = false;
                this.rbtnApprove.Enabled = false;
                this.tblApproval.Visible = true;
                this.tblApproval.Disabled = true;
                this.btnSendLater.Visible = false;
            }
            else if (Convert.ToBoolean(dt_contract_agreement.Rows[0]["isEmailSent"].ToString()))
            {
                this.btnSendLater.Visible = false;
                if (ds_contract_agreement.Tables[2].Rows.Count > 0) //contract modified & pharmacy manager opened through email
                {
                    this.disableControls(true);
                    this.txtEmails.Enabled = false;
                    this.btnSendMail.Enabled = false;
                    this.tblApproval.Visible = false;
                }
                //this.disableControls(true);
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "bindContractAgreement",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds clinic locations to gridview control
    /// </summary>
    private void bindClinicLocations()
    {
        StringReader location_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").OuterXml);
        DataSet ds_locations = new DataSet();
        ds_locations.ReadXml(location_reader);
        DataTable dt_filtered_locations = ds_locations.Tables[0];

        this.hasMutlipleClinics = (ds_locations.Tables[0].Rows.Count > 1) ? true : false;
        this.rowReassignClinicStore.Visible = this.hasMutlipleClinics;

        if (ds_locations.Tables[0].Columns.Contains("isRemoved"))
            dt_filtered_locations = ds_locations.Tables[0].Select("isRemoved=0 OR isRemoved is null").OrderBy(r => r["clinicLocation"]).CopyToDataTable();

        if (!ds_locations.Tables[0].Columns.Contains("isNoClinic"))
        {
            DataColumn dc_no_clinic = new DataColumn("isNoClinic");
            dc_no_clinic.DataType = typeof(Boolean);
            dc_no_clinic.DefaultValue = false;
            dt_filtered_locations.Columns.Add(dc_no_clinic);
        }

        if (!ds_locations.Tables[0].Columns.Contains("fluExpiryDate"))
            dt_filtered_locations.Columns.Add("fluExpiryDate", typeof(string));

        if (!ds_locations.Tables[0].Columns.Contains("routineExpiryDate"))
            dt_filtered_locations.Columns.Add("routineExpiryDate", typeof(string));

        string clinic_date_resource_key = this.hfLanguage.Value == "es-MX" ? "clinicDateAlertBefore2WeeksMX" : (this.hfLanguage.Value == "es-PR" ? "clinicDateAlertBefore2WeeksPR" : "clinicDateAlertBefore2WeeksEN");

        this.lblClinicDateAlert.Text = string.Format((string)GetGlobalResourceObject("errorMessages", clinic_date_resource_key));
        if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
            this.lblClinicDateAlert.Visible = true;

        grdLocations.DataSource = dt_filtered_locations;
        grdLocations.DataBind();
        if (!string.IsNullOrEmpty(this.isContractApproved))
            this.rowReassignClinicStore.Visible = !Convert.ToBoolean(this.isContractApproved);

        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 2) // Clinic Details page
        {
            this.disableControls(true);
            if (this.commonAppSession.SelectedStoreSession.isModifyAgreement) //Modify Agreement
            {
                this.rowReassignClinicStore.Visible = false;
                this.enableImzSelection();
            }
        }
    }

    /// <summary>
    /// Binds immunizations to clinic location grid
    /// </summary>
    /// <param name="grd_clinic_immunizations"></param>
    /// <param name="clinic_number"></param>
    private void bindClinicImmunizations(GridView grd_clinic_immunizations, int clinic_number)
    {
        StringReader clinic_immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").ChildNodes[clinic_number].OuterXml);
        DataSet ds_clinic_immunizations = new DataSet();
        ds_clinic_immunizations.ReadXml(clinic_immunizations_reader);

        if (ds_clinic_immunizations.Tables.Count > 1 && ds_clinic_immunizations.Tables[1].Rows.Count > 0)
        {
            grd_clinic_immunizations.DataSource = ds_clinic_immunizations.Tables[1];
            grd_clinic_immunizations.DataBind();
        }
    }

    /// <summary>
    /// Binds contract agreement details
    /// </summary>
    private void bindClientAgreementXml()
    {
        if (this.xmlContractAgreement.SelectSingleNode("//Immunizations") != null)
        {
            XmlNodeList immunizations_checks = this.xmlContractAgreement.SelectNodes("//Immunizations/Immunization");
            foreach (XmlNode immunization in immunizations_checks)
            {
                DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = '" + immunization.Attributes["pk"].Value + "' AND paymentTypeId = '" + immunization.Attributes["paymentTypeId"].Value + "'");
                dr_immunization[0]["isSelected"] = "True";
                dr_immunization[0]["isPaymentSelected"] = "True";
                dr_immunization[0]["name"] = !string.IsNullOrEmpty(immunization.Attributes["name"].Value) ? immunization.Attributes["name"].Value : null;
                dr_immunization[0]["address1"] = !string.IsNullOrEmpty(immunization.Attributes["address1"].Value) ? immunization.Attributes["address1"].Value : null;
                dr_immunization[0]["address2"] = !string.IsNullOrEmpty(immunization.Attributes["address2"].Value) ? immunization.Attributes["address2"].Value : null;
                dr_immunization[0]["city"] = !string.IsNullOrEmpty(immunization.Attributes["city"].Value) ? immunization.Attributes["city"].Value : null;
                dr_immunization[0]["state"] = !string.IsNullOrEmpty(immunization.Attributes["state"].Value) ? immunization.Attributes["state"].Value : null;
                dr_immunization[0]["zip"] = !string.IsNullOrEmpty(immunization.Attributes["zip"].Value) ? immunization.Attributes["zip"].Value : null;
                dr_immunization[0]["phone"] = !string.IsNullOrEmpty(immunization.Attributes["phone"].Value) ? immunization.Attributes["phone"].Value : null;
                dr_immunization[0]["email"] = !string.IsNullOrEmpty(immunization.Attributes["email"].Value) ? immunization.Attributes["email"].Value : null;
                dr_immunization[0]["tax"] = !string.IsNullOrEmpty(immunization.Attributes["tax"].Value) ? immunization.Attributes["tax"].Value : null;
                dr_immunization[0]["copay"] = !string.IsNullOrEmpty(immunization.Attributes["copay"].Value) ? immunization.Attributes["copay"].Value : null;
                dr_immunization[0]["copayValue"] = !string.IsNullOrEmpty(immunization.Attributes["copayValue"].Value) ? immunization.Attributes["copayValue"].Value : null;
                dr_immunization[0]["isVoucherNeeded"] = !string.IsNullOrEmpty(immunization.Attributes["isVoucherNeeded"].Value) ? immunization.Attributes["isVoucherNeeded"].Value : null;
                dr_immunization[0]["voucherExpirationDate"] = !string.IsNullOrEmpty(immunization.Attributes["voucherExpirationDate"].Value) ? immunization.Attributes["voucherExpirationDate"].Value : null;

                this.updateImmunizationPrice(dr_immunization[0], immunization);
            }
            DataRow[] dr_immunizations_selected = this.dtImmunizations.Select("isSelected = 'True' AND isPaymentSelected = 'True'");
            if (dr_immunizations_selected.Count() > 0)
            {
                DataTable dt_immunizations_selected = dr_immunizations_selected.CopyToDataTable();
                this.isFluImmunizationExists = (dt_immunizations_selected.Select("immunizationName Like 'Influ%' AND paymentTypeId = 6").Count() > 0);
                // this.isFluImmunizationExists = true;
                this.isRoutineImmunizationExists = (dt_immunizations_selected.Select("immunizationName NOT Like 'Influ%' AND paymentTypeId = 6").Count() > 0);
                //this.isRoutineImmunizationExists = true;
                this.isCTIExists = (dt_immunizations_selected.Select("paymentTypeId = 6").Count() > 0);
            }
            else
            {
                this.isFluImmunizationExists = false;
                this.isRoutineImmunizationExists = false;
                this.isCTIExists = false;
            }
        }

        //Bind Client details
        this.lblClient.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@header").Value.ToString();
        this.lblClientName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@name").Value.ToString();
        this.lblClientTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@title").Value.ToString();
        this.lblClientDate.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString();

        //Binds Walgreen CO. details
        this.txtWalgreenName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@name").Value.ToString();
        this.txtWalgreenTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@title").Value.ToString();
        this.PickerAndCalendarWalgreensDate.getSelectedDate = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value.ToString());
        this.txtWalgreensDate.Text = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value.ToString()).ToString("MM/dd/yyyy");
        this.txtDistrictNum.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@districtNumber").Value.ToString();

        //Binds Legal Notice details
        this.lblLegalAttentionTo.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@attentionTo").Value.ToString();
        this.lblLegalAddress1.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address1").Value.ToString();
        this.lblLegalAddress2.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address2").Value.ToString();
        this.lblLegalCity.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@city").Value.ToString();
        this.lblLegalState.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString();
        this.lblLegalZipCode.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@zipCode").Value.ToString();
    }

    /// <summary>
    /// Updates immunization price
    /// </summary>
    /// <param name="dr_immunization"></param>
    /// <param name="immunization"></param>
    private void updateImmunizationPrice(DataRow dr_immunization, XmlNode immunization)
    {
        bool is_exisiting_price_fixed = false;
        if (!string.IsNullOrEmpty(this.isContractApproved) || this.commonAppSession.SelectedStoreSession.isModifyAgreement)
        {
            if (this.commonAppSession.SelectedStoreSession.isModifyAgreement)
            {// openig agreement page by clicking Modify Agreement action
                if (!string.IsNullOrEmpty(immunization.Attributes["price"].Value))
                    is_exisiting_price_fixed = true;
            }
            else if (Convert.ToBoolean(this.isContractApproved) && !string.IsNullOrEmpty(immunization.Attributes["price"].Value))
            {// opening approved contract agreement
                is_exisiting_price_fixed = true;
            }
            if (!this.commonAppSession.SelectedStoreSession.isModifyAgreement && !Convert.ToBoolean(this.isContractApproved) && !string.IsNullOrEmpty(immunization.Attributes["price"].Value))
            {// opening the rejected modify agreement (view agreement)
                is_exisiting_price_fixed = true;
            }
        }
        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 2 && !this.commonAppSession.SelectedStoreSession.isModifyAgreement && !string.IsNullOrEmpty(immunization.Attributes["price"].Value))
        {// opening Modifed Agreement by clicking viewagreement 
            is_exisiting_price_fixed = true;
        }
        else if (((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[2].Rows.Count > 0 && !string.IsNullOrEmpty(immunization.Attributes["price"].Value))
        {//contract modified & pharmacy manager opened through email
            is_exisiting_price_fixed = true;
        }

        if (is_exisiting_price_fixed)
        {
            if (immunization.Attributes["paymentTypeName"].Value == "Corporate to Invoice Employer Directly")
                dr_immunization["directBillPrice"] = Convert.ToDecimal(immunization.Attributes["price"].Value);
            else
                dr_immunization["price"] = Convert.ToDecimal(immunization.Attributes["price"].Value);
        }
    }

    /// <summary>
    /// Binds selected immunization checks to immunization grid
    /// </summary>
    private void bindImmunizationChecks()
    {
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        this.contractCreatedDate = Convert.ToDateTime(((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[0].Rows[0]["dateCreated"].ToString());

        DataTable dt_immunizations_selected = new DataTable();
        if (this.dtImmunizations.Select("isSelected = 'True'").Count() == 0)
        {
            this.imgBtnAddImmunization.Enabled = false;
            this.lnkAddImmunization.Enabled = false;
            this.isFluImmunizationExists = false;
            this.isRoutineImmunizationExists = false;
            this.isCTIExists = false;

            dt_immunizations_selected = this.dtImmunizations.Clone();
            this.grdImmunizationChecks.DataSource = dt_immunizations_selected;
            this.grdImmunizationChecks.DataBind();

            DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck");
            DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType");
            this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
        }
        else
        {
            dt_immunizations_selected = this.dtImmunizations.Select("isSelected = 'True' AND isPaymentSelected = 'True'").CopyToDataTable();

            if (dt_immunizations_selected.Rows.Count > 0)
            {
                this.isFluImmunizationExists = (dt_immunizations_selected.Select("immunizationName Like 'Influenza%' AND paymentTypeId = 6").Count() > 0);
                // this.isFluImmunizationExists = true;
                this.isRoutineImmunizationExists = (dt_immunizations_selected.Select("immunizationName NOT Like 'Influenza%' AND paymentTypeId = 6").Count() > 0);
                //this.isRoutineImmunizationExists = true;
                this.isCTIExists = (dt_immunizations_selected.Select("paymentTypeId = 6").Count() > 0);
            }
            else
            {
                this.isFluImmunizationExists = false;
                this.isRoutineImmunizationExists = false;
                this.isCTIExists = false;
            }

            this.grdImmunizationChecks.DataSource = dt_immunizations_selected;
            this.grdImmunizationChecks.DataBind();

            if (this.dtImmunizations.Select("isSelected = 'False'").Count() == 0)
            {
                this.imgBtnAddImmunization.Enabled = false;
                this.lnkAddImmunization.Enabled = false;
            }
            else
            {
                if (this.grdImmunizationChecks.FooterRow == null || (this.grdImmunizationChecks.FooterRow != null && !this.grdImmunizationChecks.FooterRow.Visible))
                {
                    this.imgBtnAddImmunization.Enabled = true;
                    this.lnkAddImmunization.Enabled = true;
                }
                else if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
                {
                    DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
                    DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");
                    this.bindImmunizationsToDropdown(ddl_immunization_checks, ddl_payment_type);
                }
            }
        }
    }

    /// <summary>
    /// Binds Immunizations and Payment methods to dropdowns
    /// </summary>
    /// <param name="ddl_immunization_checks"></param>
    private void bindImmunizationsToDropdown(DropDownList ddl_immunization_checks, DropDownList ddl_payment_types)
    {
        this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();

        DataView dv_immunizations = new DataView((this.dtImmunizations.Select("isSelected = 'False' AND isActive = 1")).CopyToDataTable());
        DataTable dt_distinct_immunizations = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? dv_immunizations.ToTable(true, "immunizationId", "immunizationSpanishName") : dv_immunizations.ToTable(true, "immunizationId", "immunizationName");

        if (dt_distinct_immunizations.Rows.Count > 0)
        {
            ddl_immunization_checks.DataSource = dt_distinct_immunizations;
            ddl_immunization_checks.DataTextField = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "immunizationSpanishName" : "immunizationName";
            ddl_immunization_checks.DataValueField = "immunizationId";
            ddl_immunization_checks.DataBind();
            ddl_immunization_checks.Items.Insert(0, (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? new ListItem("Seleccione la Vacuna", "") : new ListItem("Select Immunization", ""));
        }

        dt_distinct_immunizations = null;
        dt_distinct_immunizations = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? dv_immunizations.ToTable(true, "paymentTypeId", "paymentTypeSpanishName") : dv_immunizations.ToTable(true, "paymentTypeId", "paymentTypeName");

        ddl_payment_types.DataSource = dt_distinct_immunizations;
        ddl_payment_types.DataTextField = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "paymentTypeSpanishName" : "paymentTypeName";
        ddl_payment_types.DataValueField = "paymentTypeId";
        ddl_payment_types.DataBind();
        ddl_payment_types.Items.Insert(0, (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? new ListItem("Seleccione el Método de pago", "") : new ListItem("Select Payment Method", ""));
    }

    /// <summary>
    /// Updates clinic locations details
    /// </summary>
    /// <param name="remove_clinic_num"></param>
    /// <returns></returns>
    private XmlDocument updateClinicLocations(int remove_clinic_num)
    {
        XmlDocument xdoc_clinic_locations = new XmlDocument();
        XmlElement clinic_locations_node = xdoc_clinic_locations.CreateElement("Clinics");
        XmlElement estshots_node;
        double clinic_location = 0;

        foreach (GridViewRow row in grdLocations.Rows)
        {
            XmlElement clinic_ele = xdoc_clinic_locations.CreateElement("clinic");
            clinic_ele.SetAttribute("clinicLocation", ((this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "UBICACIÓN DE LA CLÍNICA " : "CLINIC LOCATION ") + (clinic_location >= 26 ? ((Char)(65 + (clinic_location % 26 == 0 ? Math.Ceiling(clinic_location / 26) - 1 : Math.Ceiling(clinic_location / 26) - 2))).ToString() + "" + (char)(65 + (clinic_location % 26)) : ((char)(65 + clinic_location % 26)).ToString()));
            clinic_ele.SetAttribute("localContactName", ((TextBox)row.FindControl("txtLocalContactName")).Text.Trim());
            clinic_ele.SetAttribute("LocalContactEmail", ((TextBox)row.FindControl("txtLocalContactEmail")).Text.Trim());
            clinic_ele.SetAttribute("LocalContactPhone", ((TextBox)row.FindControl("txtLocalContactPhone")).Text.Trim());
            if (((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001" && !((CheckBox)row.FindControl("chkNoClinic")).Checked)
                clinic_ele.SetAttribute("clinicDate", ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy"));
            else
                clinic_ele.SetAttribute("clinicDate", "");
            if (!((CheckBox)row.FindControl("chkNoClinic")).Checked)
            {
                clinic_ele.SetAttribute("startTime", ((TextBox)row.FindControl("txtStartTime")).Text);
                clinic_ele.SetAttribute("endTime", ((TextBox)row.FindControl("txtEndTime")).Text);
            }
            else
            {
                clinic_ele.SetAttribute("startTime", "");
                clinic_ele.SetAttribute("endTime", "");
            }
            clinic_ele.SetAttribute("Address1", ((TextBox)row.FindControl("txtAddress1")).Text.Trim());
            clinic_ele.SetAttribute("Address2", ((TextBox)row.FindControl("txtAddress2")).Text.Trim());
            clinic_ele.SetAttribute("city", ((TextBox)row.FindControl("txtCity")).Text.Trim());
            clinic_ele.SetAttribute("state", ((DropDownList)row.FindControl("ddlState")).SelectedValue);
            clinic_ele.SetAttribute("zipCode", ((TextBox)row.FindControl("txtZipCode")).Text);
            clinic_ele.SetAttribute("isReassign", (((CheckBox)row.FindControl("chkReassignClinic")) != null) ? Convert.ToInt32(((CheckBox)row.FindControl("chkReassignClinic")).Checked).ToString() : "0");
            clinic_ele.SetAttribute("isNoClinic", (((CheckBox)row.FindControl("chkNoClinic")) != null) ? Convert.ToInt32(((CheckBox)row.FindControl("chkNoClinic")).Checked).ToString() : "0");

            if (((PickerAndCalendar)row.FindControl("pcFluExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001" && ((CheckBox)row.FindControl("chkNoClinic")).Checked)
                clinic_ele.SetAttribute("fluExpiryDate", ((PickerAndCalendar)row.FindControl("pcFluExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy"));
            else
                clinic_ele.SetAttribute("fluExpiryDate", "");

            if (((PickerAndCalendar)row.FindControl("pcRoutineExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001" && ((CheckBox)row.FindControl("chkNoClinic")).Checked)
                clinic_ele.SetAttribute("routineExpiryDate", ((PickerAndCalendar)row.FindControl("pcRoutineExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy"));
            else
                clinic_ele.SetAttribute("routineExpiryDate", "");

            GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
            if (grd_clinic_immunizations.Rows.Count > 0)
            {
                foreach (GridViewRow clinic_immunization in grd_clinic_immunizations.Rows)
                {
                    DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + ((Label)clinic_immunization.FindControl("lblImmunizationId")).Text + " AND paymentTypeId = " + ((Label)clinic_immunization.FindControl("lblPaymentTypeId")).Text + " AND isSelected = 'True' AND isPaymentSelected = 'True'");
                    if (dr_immunization.Count() > 0)
                    {
                        estshots_node = xdoc_clinic_locations.CreateElement("immunization");
                        estshots_node.SetAttribute("pk", ((Label)clinic_immunization.FindControl("lblImmunizationId")).Text);
                        estshots_node.SetAttribute("paymentTypeId", ((Label)clinic_immunization.FindControl("lblPaymentTypeId")).Text);
                        estshots_node.SetAttribute("estimatedQuantity", !string.IsNullOrEmpty(((TextBox)clinic_immunization.FindControl("txtClinicImmunizationShots")).Text.Trim()) ? Convert.ToInt32(((TextBox)clinic_immunization.FindControl("txtClinicImmunizationShots")).Text).ToString() : "");

                        clinic_ele.AppendChild(estshots_node);
                    }
                }
            }
            if (row.RowIndex != remove_clinic_num)
            {
                clinic_ele.SetAttribute("isRemoved", "0");
                clinic_locations_node.AppendChild(clinic_ele);
                clinic_location++;
            }
            else if (this.isRestrictedStoreState && remove_clinic_num > 0 && !isPreviousSeasonClinic(clinic_ele) && getAddressFromClinicXML(clinic_ele) != getBusinessLocationAddress())
            {
                clinic_ele.SetAttribute("isRemoved", "1");
                clinic_locations_node.AppendChild(clinic_ele);
                addLocationToDropdown(clinic_ele);
            }
        }

        xdoc_clinic_locations.AppendChild(clinic_locations_node);
        return xdoc_clinic_locations;
    }

    /// <summary>
    /// Updates Immunization Payment method details
    /// </summary>
    private void updatePaymentTypeData()
    {
        string copay_value = string.Empty;
        string voucher_needed = string.Empty;
        int immunization_id = 0;
        int payment_type_id = 0;
        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
            foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
            {
                Int32.TryParse(((Label)row.FindControl("lblImmunizationPk")).Text, out immunization_id);
                Int32.TryParse(((Label)row.FindControl("lblPaymentTypeId")).Text, out payment_type_id);

                if (immunization_id > 0 && payment_type_id > 0)
                {
                    DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + immunization_id + " AND paymentTypeId = " + payment_type_id);
                    if (dr_immunization.Count() > 0)
                    {
                        if (((System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowSendInvoiceTo")).Visible == true)
                        {
                            dr_immunization[0]["name"] = ((TextBox)row.FindControl("txtPmtName")).Text.Trim();
                            dr_immunization[0]["address1"] = ((TextBox)row.FindControl("txtPmtAddress1")).Text.Trim();
                            dr_immunization[0]["address2"] = ((TextBox)row.FindControl("txtPmtAddress2")).Text.Trim();
                            dr_immunization[0]["city"] = ((TextBox)row.FindControl("txtPmtCity")).Text.Trim();
                            if (((DropDownList)row.FindControl("ddlPmtState")).SelectedIndex != 0 && ((DropDownList)row.FindControl("ddlPmtState")).SelectedIndex != -1)
                                dr_immunization[0]["state"] = ((DropDownList)row.FindControl("ddlPmtState")).SelectedItem.Value;

                            dr_immunization[0]["zip"] = ((TextBox)row.FindControl("txtPmtZipCode")).Text.Trim();
                            dr_immunization[0]["phone"] = ((TextBox)row.FindControl("txtPmtPhone")).Text.Trim();
                            dr_immunization[0]["email"] = ((TextBox)row.FindControl("txtPmtEmail")).Text.Trim();
                            if (((DropDownList)row.FindControl("ddlTaxExempt")).SelectedIndex != 0 && ((DropDownList)row.FindControl("ddlTaxExempt")).SelectedIndex != -1)
                                dr_immunization[0]["tax"] = ((DropDownList)row.FindControl("ddlTaxExempt")).SelectedItem.Value;

                            if (((DropDownList)row.FindControl("ddlIsCopay")).SelectedIndex != 0 && ((DropDownList)row.FindControl("ddlIsCopay")).SelectedIndex != -1)
                            {
                                copay_value = ((DropDownList)row.FindControl("ddlIsCopay")).SelectedItem.Value;
                                dr_immunization[0]["copay"] = copay_value;
                                dr_immunization[0]["copayValue"] = (!string.IsNullOrEmpty(copay_value) || copay_value != "No") ? ((TextBox)row.FindControl("txtCoPay")).Text.Trim() : string.Empty;
                            }
                        }

                        if (((DropDownList)row.FindControl("ddlVoucher")).SelectedIndex != 0 && ((DropDownList)row.FindControl("ddlVoucher")).SelectedIndex != -1)
                        {
                            voucher_needed = ((DropDownList)row.FindControl("ddlVoucher")).SelectedItem.Value;
                            dr_immunization[0]["isVoucherNeeded"] = voucher_needed;
                        }
                        else
                        {
                            voucher_needed = "No";
                        }

                        if (voucher_needed == "Yes")
                        {
                            if (((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                                dr_immunization[0]["voucherExpirationDate"] = ((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy");
                        }
                        else
                            dr_immunization[0]["voucherExpirationDate"] = "";
                    }
                }
            }
        }
        else
            Response.Redirect("auth/sessionTimeout.aspx");
    }

    /// <summary>
    /// Creates Contract Agreement XML document
    /// </summary>
    private void prepareClinicAggreementXML(bool on_culture_change)
    {
        //Immunizations and Payment types
        this.updatePaymentTypeData();
        this.xmlContractAgreement = new XmlDocument();
        XmlElement clinic_agreement_ele = this.xmlContractAgreement.CreateElement("ClinicAgreement");
        XmlDocument xdoc_clinic_locations = new XmlDocument();
        xdoc_clinic_locations = this.updateClinicLocations(-1);
        XmlElement clinic_locations_node = (XmlElement)xdoc_clinic_locations.SelectSingleNode("Clinics");
        double clinic_location = this.previousClinicLocation.Rows.Count;
        if (this.previousClinicLocation.Rows.Count > 0)
        {
            foreach (DataRow row in this.previousClinicLocation.Select("isRemoved=1"))
            {
                XmlElement clinic_ele = xdoc_clinic_locations.CreateElement("clinic");
                clinic_ele.SetAttribute("clinicLocation", ((this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "UBICACIÓN DE LA CLÍNICA " : "CLINIC LOCATION ") + (clinic_location >= 26 ? ((Char)(65 + (clinic_location % 26 == 0 ? Math.Ceiling(clinic_location / 26) - 1 : Math.Ceiling(clinic_location / 26) - 2))).ToString() + "" + (char)(65 + (clinic_location % 26)) : ((char)(65 + clinic_location % 26)).ToString()));
                clinic_ele.SetAttribute("localContactName", string.Empty);
                clinic_ele.SetAttribute("LocalContactEmail", string.Empty);
                clinic_ele.SetAttribute("LocalContactPhone", string.Empty);
                clinic_ele.SetAttribute("clinicDate", string.Empty);
                clinic_ele.SetAttribute("startTime", string.Empty);
                clinic_ele.SetAttribute("endTime", string.Empty);
                clinic_ele.SetAttribute("Address1", row["naClinicAddress1"].ToString());
                clinic_ele.SetAttribute("Address2", row["naClinicAddress2"].ToString());
                clinic_ele.SetAttribute("city", row["naClinicCity"].ToString());
                clinic_ele.SetAttribute("state", row["naClinicState"].ToString());
                clinic_ele.SetAttribute("zipCode", row["naClinicZip"].ToString());
                clinic_ele.SetAttribute("isReassign", "0");
                clinic_ele.SetAttribute("isRemoved", Convert.ToBoolean(row["isRemoved"]) ? "1" : "0");
                clinic_ele.SetAttribute("isNoClinic", ((row["isNoClinic"] != null) ? (Convert.ToBoolean(row["isNoClinic"]) ? "1" : "0") : "0"));
                clinic_ele.SetAttribute("fluExpiryDate", string.Empty);
                clinic_ele.SetAttribute("routineExpiryDate", string.Empty);
                clinic_locations_node.AppendChild(clinic_ele);
            }
        }

        xdoc_clinic_locations.AppendChild(clinic_locations_node);

        //Update clinic locations details
        XmlDocumentFragment xml_frag = this.xmlContractAgreement.CreateDocumentFragment();
        xml_frag.InnerXml = xdoc_clinic_locations.OuterXml;
        clinic_agreement_ele.AppendChild(xml_frag);

        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        DataRow[] dr_selected_immunizations = this.dtImmunizations.Select("isSelected = 'True' AND isPaymentSelected = 'True'");
        XmlElement immunizations = this.xmlContractAgreement.CreateElement("Immunizations");
        foreach (DataRow row in dr_selected_immunizations)
        {
            XmlElement immunization = this.xmlContractAgreement.CreateElement("Immunization");

            immunization.SetAttribute("pk", row["immunizationId"].ToString());
            immunization.SetAttribute("immunizationName", row["immunizationName"].ToString());
            immunization.SetAttribute("immunizationSpanishName", row["immunizationSpanishName"].ToString());
            immunization.SetAttribute("price", ((Convert.ToInt32(row["paymentTypeId"].ToString()) == 6) ? row["directBillPrice"].ToString() : row["price"].ToString()));
            immunization.SetAttribute("paymentTypeId", row["paymentTypeId"].ToString());
            immunization.SetAttribute("paymentTypeName", row["paymentTypeName"].ToString());
            immunization.SetAttribute("paymentTypeSpanishName", row["paymentTypeSpanishName"].ToString());
            immunization.SetAttribute("name", row["name"] != DBNull.Value ? row["name"].ToString() : string.Empty);
            immunization.SetAttribute("address1", row["address1"] != DBNull.Value ? row["address1"].ToString() : string.Empty);
            immunization.SetAttribute("address2", row["address2"] != DBNull.Value ? row["address2"].ToString() : string.Empty);
            immunization.SetAttribute("city", row["city"] != DBNull.Value ? row["city"].ToString() : string.Empty);
            immunization.SetAttribute("state", row["state"] != DBNull.Value ? row["state"].ToString() : string.Empty);
            immunization.SetAttribute("zip", row["zip"] != DBNull.Value ? row["zip"].ToString() : string.Empty);
            immunization.SetAttribute("phone", row["phone"] != DBNull.Value ? row["phone"].ToString() : string.Empty);
            immunization.SetAttribute("email", row["email"] != DBNull.Value ? row["email"].ToString() : string.Empty);
            immunization.SetAttribute("tax", row["tax"] != DBNull.Value ? row["tax"].ToString() : string.Empty);
            immunization.SetAttribute("copay", row["copay"] != DBNull.Value ? row["copay"].ToString() : string.Empty);
            immunization.SetAttribute("copayValue", row["copayValue"] != DBNull.Value ? row["copayValue"].ToString() : string.Empty);
            immunization.SetAttribute("isVoucherNeeded", row["isVoucherNeeded"] != DBNull.Value ? row["isVoucherNeeded"].ToString() : string.Empty);
            immunization.SetAttribute("voucherExpirationDate", row["voucherExpirationDate"] != DBNull.Value ? row["voucherExpirationDate"].ToString() : string.Empty);

            immunizations.AppendChild(immunization);
        }
        clinic_agreement_ele.AppendChild(immunizations);

        XmlElement client = this.xmlContractAgreement.CreateElement("Client");
        client.SetAttribute("header", this.lblClient.Text.Trim());
        client.SetAttribute("name", this.lblClientName.Text.Trim());
        client.SetAttribute("title", this.lblClientTitle.Text.Trim());
        client.SetAttribute("date", this.lblClientDate.Text.ToString());
        clinic_agreement_ele.AppendChild(client);

        XmlElement walgreens_co = this.xmlContractAgreement.CreateElement("WalgreenCo");
        walgreens_co.SetAttribute("name", this.txtWalgreenName.Text.Trim());
        walgreens_co.SetAttribute("title", this.txtWalgreenTitle.Text.Trim());
        walgreens_co.SetAttribute("date", this.PickerAndCalendarWalgreensDate.getSelectedDate.ToString());
        walgreens_co.SetAttribute("districtNumber", this.txtDistrictNum.Text.Trim());
        clinic_agreement_ele.AppendChild(walgreens_co);

        XmlElement legal_notic_address = this.xmlContractAgreement.CreateElement("LegalNoticeAddress");
        legal_notic_address.SetAttribute("attentionTo", this.lblLegalAttentionTo.Text.Trim());
        legal_notic_address.SetAttribute("address1", this.lblLegalAddress1.Text.Trim());
        legal_notic_address.SetAttribute("address2", this.lblLegalAddress2.Text.Trim());
        legal_notic_address.SetAttribute("city", this.lblLegalCity.Text.Trim());
        legal_notic_address.SetAttribute("state", this.lblLegalState.Text);
        legal_notic_address.SetAttribute("zipCode", this.lblLegalZipCode.Text.Trim());
        clinic_agreement_ele.AppendChild(legal_notic_address);

        this.xmlContractAgreement.AppendChild(clinic_agreement_ele);

        if (!on_culture_change)
        {
            //Change clinic location name from Spanish to English
            foreach (XmlNode clinic in this.xmlContractAgreement.SelectNodes("//Clinics/clinic"))
            {
                clinic.Attributes["clinicLocation"].Value = clinic.Attributes["clinicLocation"].Value.Replace("UBICACIÓN DE LA CLÍNICA", "CLINIC LOCATION");
            }
        }
    }

    /// <summary>
    /// Disables controls
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableControls(bool is_disable)
    {
        this.disableCommunityAgreement(is_disable, this.tblApproval.Controls);
        this.disableCommunityAgreement(is_disable, this.tblAgreement.Controls);
        this.disableGridControls(is_disable, this.grdLocations);
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            GridView grd_view = (GridView)row.FindControl("grdClinicImmunizations");
            this.disableGridControls(is_disable, grd_view);
        }

        this.disableGridControls(is_disable, this.grdImmunizationChecks);
        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_voucher_exp_date = (System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowExpirationDate");
            this.disableCommunityAgreement(is_disable, row_voucher_exp_date.Controls);
        }

        this.lblClinicDateAlert.Visible = !is_disable;
        this.btnSendLater.Visible = !is_disable;
        this.lnkAddLocation.Visible = !is_disable;
        this.btnAddLocation.Visible = !is_disable;
        this.lnkAddImmunization.Visible = !is_disable;
        this.imgBtnAddImmunization.Visible = !is_disable;
        this.grdImmunizationChecks.Enabled = !is_disable;
        this.ddlClinicLocations.Visible = this.ddlClinicLocations.Visible ? !is_disable : this.ddlClinicLocations.Visible;
        this.imgbtnAddPreviousClinic.Visible = this.imgbtnAddPreviousClinic.Visible ? !is_disable : this.imgbtnAddPreviousClinic.Visible;
    }

    /// <summary>
    /// Disables all input controls in the page
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableCommunityAgreement(bool disable, ControlCollection ctrl_main)
    {
        foreach (Control page_ctrl in ctrl_main)
        {
            foreach (Control ctrl1 in page_ctrl.Controls)
            {
                foreach (Control ctrl in ctrl1.Controls)
                {
                    this.disableBasedOnType(ctrl, disable);
                }

                if (page_ctrl is System.Web.UI.HtmlControls.HtmlTableCell)
                {
                    this.disableBasedOnType(page_ctrl, disable);
                }
            }
        }
    }

    /// <summary>
    /// Disables grid controls
    /// </summary>
    /// <param name="disable"></param>
    /// <param name="grd_ctrl"></param>
    private void disableGridControls(bool disable, GridView grd_ctrl)
    {
        foreach (GridViewRow row in grd_ctrl.Rows)
        {
            foreach (Control ctrl in row.Controls)
            {
                foreach (Control ctrl1 in ctrl.Controls)
                {
                    this.disableBasedOnType(ctrl1, disable);
                    if (ctrl1 is System.Web.UI.HtmlControls.HtmlTableRow)
                    {
                        foreach (Control ctrlItem in ctrl1.Controls)
                        {
                            if (ctrlItem is System.Web.UI.HtmlControls.HtmlTableCell)
                            {
                                foreach (Control tblcell in ctrlItem.Controls)
                                {
                                    this.disableBasedOnType(tblcell, disable);
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Disable controls based on the type
    /// </summary>
    /// <param name="ctrl"></param>
    /// <param name="disable"></param>
    private void disableBasedOnType(Control ctrl, bool disable)
    {
        if (ctrl is TextBox)
        {
            ((TextBox)ctrl).Enabled = !disable;
            if (((TextBox)ctrl).ID.Contains("txtVaccineExpirationDate") || ((TextBox)ctrl).ID.Contains("txtCalenderFrom"))
                ((TextBox)ctrl).Visible = disable;
            if (((TextBox)ctrl).ID.Contains("txtFluExpiryDate"))
                ((TextBox)ctrl).Visible = disable && this.isFluImmunizationExists;
            if (((TextBox)ctrl).ID.Contains("txtRoutineExpiryDate"))
                ((TextBox)ctrl).Visible = disable && this.isRoutineImmunizationExists;
            if (((TextBox)ctrl).ID == "txtGroupDate" || ((TextBox)ctrl).ID == "txtWalgreensDate")
            {
                ((TextBox)ctrl).Visible = disable;
                this.lnkPrintContract.Visible = disable;
            }
        }

        if (ctrl is DropDownList)
            ((DropDownList)ctrl).Enabled = !disable;

        if (ctrl is CheckBox)
            ((CheckBox)ctrl).Enabled = !disable;

        if (ctrl is PickerAndCalendar)
            ((PickerAndCalendar)ctrl).Visible = !disable;

        if (ctrl is ImageButton)
            ((ImageButton)ctrl).Visible = !disable;

        if (ctrl is RadioButton)
            ((RadioButton)ctrl).Enabled = !disable;
    }

    /// <summary>
    /// Setting image urls based on culture
    /// </summary>
    private void setImageButtons()
    {
        if (!String.IsNullOrEmpty(this.hfLanguage.Value))
        {
            btnSendMail.ImageUrl = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "images/btn_send_email_SP.png" : "images/btn_send_email.png";
            btnSendLater.ImageUrl = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "images/btn_save_send_later_SP.png" : "images/btn_save_send_later.png";
            btnCancel.ImageUrl = (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR") ? "images/btn_cancel_SP.png" : "images/btn_cancel_mini.png";
            //onmouseout="javascript:MouseOutImage(this.id,'images/btn_send_email.png');" 
            //onmouseover="javascript:MouseOverImage(this.id,'images/btn_send_email_lit.png');"
            if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
            {
                btnSendMail.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_send_email_SP.png');");
                btnSendLater.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_save_send_later_SP.png');");
                btnCancel.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_cancel_SP.png');");
                btnSendMail.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_send_email_SP_lit.png');");
                btnSendLater.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_save_send_later_SP_lit.png');");
                btnCancel.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_cancel_SP_lit.png');");
            }
            else
            {
                btnSendMail.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_send_email.png');");
                btnSendLater.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_save_send_later.png');");
                btnCancel.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_cancel_mini.png');");
                btnSendMail.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_send_email_lit.png');");
                btnSendLater.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_save_send_later_lit.png');");
                btnCancel.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_cancel_mini_lit.png');");
            }
        }
    }

    /// <summary>
    /// Getting Address from Clinic locations XML node
    /// </summary>
    /// <param name="clinic_location"></param>
    /// <returns></returns>
    private string getAddressFromClinicXML(XmlNode clinic_location)
    {
        return (clinic_location.Attributes["Address1"].Value.Length > 0 ? (clinic_location.Attributes["Address1"].Value + ", ") : "") + (clinic_location.Attributes["Address2"].Value.Length > 0 ? (clinic_location.Attributes["Address2"].Value + ", ") : "") + clinic_location.Attributes["city"].Value + ", " + clinic_location.Attributes["state"].Value + ", " + clinic_location.Attributes["zipCode"].Value;
    }

    /// <summary>
    /// check if the current clinic location is a previous season clinic
    /// </summary>
    /// <param name="clinic_location"></param>
    /// <returns></returns>
    private bool isPreviousSeasonClinic(XmlNode clinic_location)
    {
        foreach (DataRow row in this.previousClinicLocation.Rows)
        {
            string address_Prev = row["naClinicAddress"].ToString();
            int count = 0;

            string address_exist = this.getAddressFromClinicXML(clinic_location);
            if (address_exist == address_Prev)
            {
                count = count + 1;
            }
            if (count > 0)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Convert location xml to datarow and add to previous locations table
    /// </summary>
    /// <param name="clinic_location"></param>
    /// <returns></returns>
    private void addLocationToDropdown(XmlElement clinic_location)
    {
        int rowId = this.previousClinicLocation.Rows.Count > 0 ? Convert.ToInt32(this.previousClinicLocation.Compute("Max(rowId)", "")) + 1 : 1;
        DataRow new_row = this.previousClinicLocation.NewRow();
        new_row["naClinicAddress1"] = clinic_location.GetAttribute("Address1").ToString();
        new_row["naClinicAddress2"] = clinic_location.GetAttribute("Address2").ToString();
        new_row["naClinicCity"] = clinic_location.GetAttribute("city").ToString();
        new_row["naClinicState"] = clinic_location.GetAttribute("state").ToString();
        new_row["naClinicZip"] = clinic_location.GetAttribute("zipCode").ToString();
        new_row["naClinicAddress"] = getAddressFromClinicXML(clinic_location);
        new_row["isRemoved"] = (clinic_location.GetAttribute("isRemoved") != null && clinic_location.GetAttribute("isRemoved").ToString() == "1") ? true : false;
        new_row["rowId"] = rowId;
        this.previousClinicLocation.Rows.Add(new_row);
    }

    /// <summary>
    ///  Bind the previous season clinic locations to dropdown
    /// </summary>
    /// <param></param>
    /// <returns></returns>
    private void bindClinicAgreemtPreviousLocationToDropdown()
    {
        if (this.previousClinicLocation.Rows.Count > 0)
        {
            this.ddlClinicLocations.DataSource = this.previousClinicLocation;
            this.ddlClinicLocations.DataTextField = "naClinicAddress";
            this.ddlClinicLocations.DataValueField = "rowId";
            this.ddlClinicLocations.DataBind();
            this.ddlClinicLocations.Items.Insert(0, new ListItem("Select Location", "0"));
            this.ddlClinicLocations.Visible = true;
            this.imgbtnAddPreviousClinic.Visible = true;
            this.lnkAddLocation.Visible = false;
            this.btnAddLocation.Visible = false;
        }
        else
        {
            this.ddlClinicLocations.Visible = false;
            this.imgbtnAddPreviousClinic.Visible = false;
            this.lnkAddLocation.Visible = true;
            this.btnAddLocation.Visible = true;
        }
    }

    /// <summary>
    /// Get the business location address
    /// </summary>
    private string getBusinessLocationAddress()
    {
        DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
        DataTable dt_contract_agreement = ds_contract_agreement.Tables[0];
        string business_address = (dt_contract_agreement.Rows[0]["address"].ToString().Length > 0 ? (dt_contract_agreement.Rows[0]["address"].ToString() + ", ") : "") + (dt_contract_agreement.Rows[0]["address2"].ToString().Length > 0 ? (dt_contract_agreement.Rows[0]["address2"].ToString() + ", ") : "") + dt_contract_agreement.Rows[0]["city"].ToString() + ", " + dt_contract_agreement.Rows[0]["state"].ToString() + ", " + dt_contract_agreement.Rows[0]["zip"].ToString();
        return business_address;
    }

    /// <summary>
    /// Validates contract agreement
    /// </summary>
    private void ValidationsHandler()
    {
        this.logger.Info("Validating the clinic agreement sent by user {0} - START ", this.commonAppSession.LoginUserInfoSession.UserName);
        DateTime clinic_date;
        bool has_clinic_after_2weeks = false, has_clinic_before_2weeks = false;
        bool has_minimum_shots = false;
        string failed_clinic_locations = string.Empty;
        string clinic_date_reminder_alert = "";
        bool is_having_override = true;
        List<string> lst_flu_expiry_dates = new List<string>();
        List<string> lst_routine_expiry_dates = new List<string>();
        bool has_voucher = false, voucher_alert_needed = false;
        string lbl_clinic_location;

        if (String.IsNullOrEmpty(this.businessName))
            this.businessName = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[0].Rows[0]["businessName"].ToString();

        this.prepareClinicAggreementXML(false);

        foreach (GridViewRow imm_row in grdImmunizationChecks.Rows)
        {
            var ctl_date_picker = imm_row.FindControl("pcVaccineExpirationDate");
            bool is_voucher_needed = (((DropDownList)imm_row.FindControl("ddlVoucher")).SelectedItem.Value.ToString() == "Yes");
            string vaccine_expiry_date = (is_voucher_needed && ctl_date_picker != null) ? ((PickerAndCalendar)ctl_date_picker).getSelectedDate.ToString() : string.Empty;
            Label lbl_Immunization = (Label)imm_row.FindControl("lblImmunizationCheck");
            if (!string.IsNullOrEmpty(vaccine_expiry_date) && Convert.ToDateTime(vaccine_expiry_date) != Convert.ToDateTime("1/1/0001"))
            {
                if (lbl_Immunization.Text.ToLower().Contains("influenza") && !lst_flu_expiry_dates.Contains(vaccine_expiry_date))
                    lst_flu_expiry_dates.Add(vaccine_expiry_date);
                else if (!lbl_Immunization.Text.ToLower().Contains("influenza") && !lst_routine_expiry_dates.Contains(vaccine_expiry_date))
                    lst_routine_expiry_dates.Add(vaccine_expiry_date);
            }
            if (is_voucher_needed)
                has_voucher = true;
        }
        foreach (GridViewRow row in grdLocations.Rows)
        {
            bool is_estimated_qnt_exists = false;
            var date_control = row.FindControl("PickerAndCalendarFrom");
            ComponentArt.Web.UI.Calendar calendarctrl = (ComponentArt.Web.UI.Calendar)((PickerAndCalendar)date_control).FindControl("picker1");
            clinic_date = ((PickerAndCalendar)date_control).getSelectedDate;
            CheckBox chk_no_clinic = (CheckBox)row.FindControl("chkNoClinic");
            int total_shots_count = 0, shots_count = 0;

            var flu_date_picker = row.FindControl("pcFluExpiryDate");
            string flu_expiry_date = (flu_date_picker != null && chk_no_clinic.Checked) ? ((PickerAndCalendar)flu_date_picker).getSelectedDate.ToString() : string.Empty;
            var routine_date_picker = row.FindControl("pcRoutineExpiryDate");
            string routine_expiry_date = (routine_date_picker != null && chk_no_clinic.Checked) ? ((PickerAndCalendar)routine_date_picker).getSelectedDate.ToString() : string.Empty;
            lbl_clinic_location = ((Label)row.FindControl("lblClinicLocation")).Text;
            if (!string.IsNullOrEmpty(flu_expiry_date) && this.isFluImmunizationExists && Convert.ToDateTime(flu_expiry_date) != Convert.ToDateTime("1/1/0001") && !lst_flu_expiry_dates.Contains(flu_expiry_date))
                lst_flu_expiry_dates.Add(flu_expiry_date);

            if (!string.IsNullOrEmpty(routine_expiry_date) && this.isRoutineImmunizationExists && Convert.ToDateTime(routine_expiry_date) != Convert.ToDateTime("1/1/0001") && !lst_routine_expiry_dates.Contains(routine_expiry_date))
                lst_routine_expiry_dates.Add(routine_expiry_date);

            if (!chk_no_clinic.Checked) //If not voucher only clinic
            {
                GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
                foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
                {
                    TextBox txt_clinic_immunization_shots = (TextBox)imm_row.FindControl("txtClinicImmunizationShots");
                    string immunization_name = ((Label)imm_row.FindControl("lblClinicImmunization")).Text;
                    if (!string.IsNullOrEmpty(txt_clinic_immunization_shots.Text))
                    {
                        if (this.blockedOutImmunizations.Contains(immunization_name.ToLower().Trim()) && Convert.ToInt32(txt_clinic_immunization_shots.Text) > 0)
                            is_estimated_qnt_exists = true;
                        shots_count = Convert.ToInt32(txt_clinic_immunization_shots.Text);

                        total_shots_count += shots_count;
                    }
                }
                if (clinic_date != Convert.ToDateTime("1/1/0001") && is_estimated_qnt_exists)
                {
                    if (clinic_date >= Convert.ToDateTime("03/15/2018") && clinic_date <= Convert.ToDateTime("07/15/2018"))
                    {
                        failed_clinic_locations += "\\n" + lbl_clinic_location;
                        calendarctrl.BorderColor = System.Drawing.Color.Red;
                        calendarctrl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                        calendarctrl.BorderWidth = Unit.Parse("1px");
                        calendarctrl.ToolTip = "";
                    }
                }

                if (total_shots_count < 25)
                    has_minimum_shots = true;

                //Validates clinic duration between clinic date and current date
                if (clinic_date != Convert.ToDateTime("1/1/0001"))
                {
                    if (clinic_date.Date < DateTime.Now.Date.AddDays(14))
                        has_clinic_before_2weeks = true;
                    else
                        has_clinic_after_2weeks = true;
                }
            }
            else
            {
                if (!has_voucher)
                    voucher_alert_needed = true;
            }
        }

        if (!string.IsNullOrEmpty(failed_clinic_locations))
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "blackoutImmunizationValidationMessage") + failed_clinic_locations.Replace("UBICACIÓN DE LA CLÍNICA", "CLINIC LOCATION"));
            this.bindClientAgreementXml();
            is_having_override = false;
        }
        if (has_minimum_shots)
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "minimumShotsRequiredAlert"));
            if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
                is_having_override = false;
        }
        if (has_clinic_before_2weeks)
        {
            clinic_date_reminder_alert = this.hfLanguage.Value == "es-MX" ? "clinicDateReminderBefore2WeeksMX" : (this.hfLanguage.Value == "es-PR" ? "clinicDateReminderBefore2WeeksPR" : "clinicDateReminderBefore2WeeksEN");
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert));
            if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                is_having_override = false;
        }
        if (voucher_alert_needed)
        {
            this.validationMsgList.Add("Please select at least one vaccine with <b>Voucher needed</b> to conduct Voucher Distribution");
            is_having_override = false;
        }
        if (this.checkClinicImmunizations())
        {
            //checking date and time stamp locally
            XmlNodeList clinic_location_nodes = xmlContractAgreement.SelectNodes("/ClinicAgreement/Clinics/clinic[@isReassign='0']");
            string failed_location = "";
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "localagreement", this.businessName, string.Empty);
            if (!String.IsNullOrEmpty(failed_location))
            {
                this.bindClinicLocations();
                is_having_override = false;
                this.validationMsgList.Add(failed_location);
            }
        }
        if (this.commonAppSession.SelectedStoreSession.isModifyAgreement)
            this.validationMsgList.Clear();

        if (lst_flu_expiry_dates.Count > 1)
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "fluExpiryDateMismatchAlert"));
            is_having_override = false;
        }

        if (lst_routine_expiry_dates.Count > 1)
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "routineExpiryDateMismatchAlert"));
            is_having_override = false;
        }

        if (!this.commonAppSession.SelectedStoreSession.isModifyAgreement)
        {
            if (!has_clinic_before_2weeks && has_clinic_after_2weeks)
            {
                clinic_date_reminder_alert = this.hfLanguage.Value == "es-MX" ? "clinicDateReminderAfter2WeeksMX" : (this.hfLanguage.Value == "es-PR" ? "clinicDateReminderAfter2WeeksPR" : "clinicDateReminderAfter2WeeksEN");
                if (this.validationMsgList.Count == 0)
                {
                    //this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert));
                    //is_confirmation_alert = false;
                    this.isClinicAfter2Weeks = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueReminder');", true);
                    return;
                }
                else
                    this.isClinicAfter2Weeks = true;
            }
            else
                this.isClinicAfter2Weeks = false;
        }
        else
            this.isClinicAfter2Weeks = false;
        this.logger.Info("Validating the clinic agreement sent by user {0} - END ", this.commonAppSession.LoginUserInfoSession.UserName);
        if (this.validationMsgList.Count > 0)
        {
            this.logger.Info("Showing Validation summary..");
            this.showValidationSummary(is_having_override);
        }
        else
        {
            this.logger.Info("Validations successful. Sending email by user {0}", this.commonAppSession.LoginUserInfoSession.UserName);
            this.sendEmailHandler();
        }
    }

    /// <summary>
    /// Sends contract agreement
    /// </summary>
    private void sendEmailHandler(bool is_post_back = false, bool is_continue = false)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} with override: {3} - START", "sendEmailHandler",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName, is_post_back);

        if (this.isClinicAfter2Weeks || is_post_back || (!this.commonAppSession.SelectedStoreSession.isModifyAgreement && !string.IsNullOrEmpty(this.isContractApproved) && Convert.ToBoolean(this.isContractApproved)))
            this.prepareClinicAggreementXML(false);
        if (this.xmlContractAgreement == null || string.IsNullOrEmpty(this.xmlContractAgreement.InnerXml))
            this.prepareClinicAggreementXML(false);

        if (this.checkClinicImmunizations())
        {
            if (!is_continue)
            {
                //Show Confirmation Alert
                bool has_max_qty = false;
                int loc_count = 1;
                string max_qty_error = string.Empty;
                var loc_list = (from locations in XDocument.Parse(this.xmlContractAgreement.InnerXml).Descendants("clinic")
                                where locations.Attribute("isNoClinic").Value != "1"
                                select locations).ToList();
                foreach (var clinic_location in loc_list)
                {
                    var imm_list = this.xmlContractAgreement.SelectNodes("/ClinicAgreement/Clinics/clinic[@isNoClinic='0'][" + loc_count + "]/immunization/@estimatedQuantity").Cast<XmlNode>().ToList();
                    var imm_name_list = this.xmlContractAgreement.SelectNodes("/ClinicAgreement/Immunizations/Immunization/@immunizationName").Cast<XmlNode>().ToList();
                    for (int i = 0; i < imm_list.Count; i++)
                    {
                        int new_est_shots;
                        int.TryParse(imm_list[i].Value, out new_est_shots);
                        if (new_est_shots > 250)
                        {
                            has_max_qty = true;
                            max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), new_est_shots, imm_name_list[i].Value, clinic_location.Attribute("clinicLocation").Value.Replace("CLINIC LOCATION ", ""));

                        }
                    }
                    loc_count++;
                }
                if (has_max_qty)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showWarning('WARNING','" + max_qty_error.Substring(6) + "','continuesending');", true);
                    return;
                }
            }

            string date_time_stamp_message = string.Empty;
            XmlNodeList clinic_location_nodes = xmlContractAgreement.SelectNodes("/ClinicAgreement/Clinics/clinic[@isReassign='0']");
            DataTable dt_contract_agreement = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[0];
            this.isContractApproved = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["isApproved"].ToString() : "";
            this.businessName = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["businessName"].ToString() : "";
            //this.isContractModified = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["isModified"].ToString() : "";
            bool is_modified = false;
            int modified_by = -1;
            if (this.commonAppSession.SelectedStoreSession.isModifyAgreement)
            {
                if (string.IsNullOrEmpty(this.isContractModified) || (!string.IsNullOrEmpty(this.isContractModified) && !Convert.ToBoolean(this.isContractModified)))
                {
                    is_modified = true;
                    modified_by = this.commonAppSession.LoginUserInfoSession.UserID;
                }
            }

            int return_value = this.dbOperation.insertUpdateClinicAgreement(this.contactLogPk, this.txtEmails.Text.Trim(), this.xmlContractAgreement.InnerXml, (!string.IsNullOrEmpty(this.isContractApproved) && Convert.ToBoolean(this.isContractApproved)), this.hfLastUpdatedDate.Value, is_modified, out date_time_stamp_message, modified_by);
            this.logger.Info("Database Operation {0} done with return value {1}", "insertUpdateClinicAgreement", return_value);
            if (return_value >= 0)
            {
                EmailOperations email_operations = new EmailOperations();
                if (email_operations.sendClinicAgreementToBusinessUsers(this.txtEmails.Text, this.contactLogPk, true))
                {
                    Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreementMail") + "'); window.location.href = 'walgreensHome.aspx';", true);
                }
            }
            else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
            {
                this.bindClinicLocations();
                string validation_message = string.Empty;
                ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "localagreement", this.businessName, date_time_stamp_message);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
                return;
            }
            else if (return_value == -5 || return_value == -6)
            {
                Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "AgreementStatusChanged") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
        }
        else
            this.bindClinicLocations();
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "sendEmailHandler",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Saves contract agreement
    /// </summary>
    private void saveSendLaterHandler()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "saveSendLaterHandler",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            if (this.ValidateAgreement("SendLater"))
            {
                this.prepareClinicAggreementXML(false);

                //Check if all the immunizations are added to clinics
                this.checkClinicImmunizations();

                string date_time_stamp_message = string.Empty;

                bool is_modified = false;
                int modified_by = -1;
                if (this.commonAppSession.SelectedStoreSession.isModifyAgreement)
                {
                    if (string.IsNullOrEmpty(this.isContractModified) || (!string.IsNullOrEmpty(this.isContractModified) && !Convert.ToBoolean(this.isContractModified)))
                    {
                        is_modified = true;
                        modified_by = this.commonAppSession.LoginUserInfoSession.UserID;
                    }
                }
                int return_value = this.dbOperation.insertUpdateClinicAgreement(this.contactLogPk, this.txtEmails.Text.Trim(), this.xmlContractAgreement.InnerXml, true, this.hfLastUpdatedDate.Value, is_modified, out date_time_stamp_message, modified_by);
                this.logger.Info("Database Operation {0} done with return value {1}", "insertUpdateClinicAgreement", return_value);

                if (return_value >= 0)
                {
                    Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreement") + "'); window.location.href = 'walgreensHome.aspx';", true);
                }
                else if (return_value == -6)
                {
                    Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "AgreementStatusChanged") + "'); window.location.href = 'walgreensHome.aspx';", true);
                }
            }
            else if (this.paymentMethods == "[]")
            {
                this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
                this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();
            }
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.location.href = 'walgreensHome.aspx';", true);
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "saveSendLaterHandler",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Enable immunizations selection to modify contract agreement
    /// </summary>
    private void enableImzSelection()
    {
        //this.disableCommunityAgreement(false, this.tblApproval.Controls);
        this.disableGridControls(false, this.grdImmunizationChecks);
        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_voucher_exp_date = (System.Web.UI.HtmlControls.HtmlTableRow)row.FindControl("rowExpirationDate");
            this.disableCommunityAgreement(false, row_voucher_exp_date.Controls);
        }
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");

            foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
            {
                TextBox txt_clinic_immunization_shots = (TextBox)imm_row.FindControl("txtClinicImmunizationShots");
                if (txt_clinic_immunization_shots != null)
                {
                    txt_clinic_immunization_shots.Enabled = true;
                }
            }
        }
        this.lnkAddImmunization.Visible = true;
        this.imgBtnAddImmunization.Visible = true;
        this.grdImmunizationChecks.Enabled = true;
        this.tblApproval.Visible = false;
        this.btnSendLater.Visible = true;
    }

    /// <summary>
    /// Shows Validation summary to the user before sending email/saving agreement.
    /// </summary>
    /// <param name="is_confirmation_alert"></param>
    private void showValidationSummary(bool is_confirmation_alert)
    {
        if (this.validationMsgList.Count > 0)
        // if (!string.IsNullOrEmpty(this.validationSummary) && isConfirmationAlert)
        {
            this.validationSummary = "<ul>";
            foreach (var item in this.validationMsgList)
            {
                this.validationSummary += "<li style=\"text-align:left\">" + item.Replace("\n", "<br />").Trim() + "</li><br/>";
            }
            this.validationSummary += "</ul>";
            if (is_confirmation_alert)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryWarning('" + this.validationSummary + "','continuesending');", true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryAlert('" + this.validationSummary + "','continuesending');", true);
            this.validationSummary = "";
            this.validationMsgList.Clear();
        }
    }

    /// <summary>
    /// Set minimum and maximum dates for date controls.
    /// </summary>
    private void setMinMaxDates()
    {
        foreach (GridViewRow imm_row in grdImmunizationChecks.Rows)
        {
            var ctl_date_picker = imm_row.FindControl("pcVaccineExpirationDate");
            Label lbl_Immunization = (Label)imm_row.FindControl("lblImmunizationCheck");
            if (ctl_date_picker != null)
            {
                if (((PickerAndCalendar)ctl_date_picker).getSelectedDate != DateTime.Parse("01/01/0001") && ((PickerAndCalendar)ctl_date_picker).getSelectedDate <= DateTime.Now.AddDays(-1))
                {
                    ((PickerAndCalendar)ctl_date_picker).MinDate = ((PickerAndCalendar)ctl_date_picker).getSelectedDate.AddDays(-1);
                }
                else
                {
                    ((PickerAndCalendar)ctl_date_picker).MinDate = DateTime.Now.AddDays(-1);
                }

                if (lbl_Immunization.Text.ToLower().Contains("influenza"))
                {
                    ((PickerAndCalendar)ctl_date_picker).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
                }
                else
                {
                    ((PickerAndCalendar)ctl_date_picker).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
                }
            }
        }
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            var flu_exp_date = row.FindControl("pcFluExpiryDate");
            var routine_exp_date = row.FindControl("pcRoutineExpiryDate");
            if (flu_exp_date != null)
            {
                ((PickerAndCalendar)flu_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
            }
            if (routine_exp_date != null)
            {
                ((PickerAndCalendar)routine_exp_date).MinDate = DateTime.Now.AddDays(-1);
                ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
            }
            this.setClinicDates(row, false);
        }
    }
    /// <summary>
    /// Sets min/max dates for clinics
    /// </summary>
    /// <param name="row"></param>
    private void setClinicDates(GridViewRow row, bool is_from_grid_load)
    {
        var date_control = row.FindControl("PickerAndCalendarFrom");
        DropDownList ddl_states = (DropDownList)row.FindControl("ddlState");
        if (!string.IsNullOrEmpty(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()) && grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
        {
            string clinic_date = grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString();
            //if (ddl_states.SelectedValue == "MO" && !commonAppSession.LoginUserInfoSession.IsAdmin)
            if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
            {
                if (Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()) >= DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                else if (Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                    ((PickerAndCalendar)date_control).SetMinDate = Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString());
            }
            else if (DateTime.Now < Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()))
            {
                if (DateTime.Now.AddDays(14) < Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString())
                    && !ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(13);
                else if (ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                else
                    ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()).AddDays(-1);
            }
            else
                ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString()).AddDays(-1);

            if (is_from_grid_load)
            {
                ((PickerAndCalendar)date_control).getSelectedDate = Convert.ToDateTime(clinic_date);
                ((TextBox)row.FindControl("txtCalenderFrom")).Text = Convert.ToDateTime(clinic_date).ToString("MM/dd/yyyy");
            }
        }
        else
        {
            //For "MO" stores block out all clinic dates up to September 1, 2016 in the local contract and for charity events
            if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && !commonAppSession.LoginUserInfoSession.IsAdmin)
            {
                ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
            }
            else
            {
                if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Now.AddDays(14);
                else
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
            }
        }
    }

    #endregion

    #region ------------ PRIVATE VARIABLES -----------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private int contactLogPk = 0;
    private bool hasMutlipleClinics = false;
    private string isContractApproved
    {
        get
        {
            if (ViewState["isContractApproved"] != null)
                return ViewState["isContractApproved"].ToString();
            else
                return null;
        }
        set
        {
            ViewState["isContractApproved"] = value;
        }
    }
    private string isContractModified
    {
        get
        {
            if (ViewState["isContractModified"] != null)
                return ViewState["isContractModified"].ToString();
            else
                return null;
        }
        set
        {
            ViewState["isContractModified"] = value;
        }
    }
    private string businessName;
    private bool isEmailSent = false;
    private DataTable dtImmunizations;
    private XmlDocument xmlContractAgreement;
    protected string paymentMethods = "[]";
    private DateTime contractCreatedDate;
    protected bool isFluImmunizationExists
    {
        get
        {
            bool result = false;

            if (ViewState["isFluImmunizationExists"] != null)
            {
                result = (bool)ViewState["isFluImmunizationExists"];
            }
            return result;
        }
        set
        {
            ViewState["isFluImmunizationExists"] = value;
        }
    }
    protected bool isRoutineImmunizationExists
    {
        get
        {
            bool result = false;

            if (ViewState["isRoutineImmunizationExists"] != null)
            {
                result = (bool)ViewState["isRoutineImmunizationExists"];
            }
            return result;
        }
        set
        {
            ViewState["isRoutineImmunizationExists"] = value;
        }
    }
    protected bool isCTIExists
    {
        get
        {
            bool result = false;

            if (ViewState["isCTIExists"] != null)
            {
                result = (bool)ViewState["isCTIExists"];
            }
            return result;
        }
        set
        {
            ViewState["isCTIExists"] = value;
        }
    }
    private string[] blockedOutImmunizations
    {
        get
        {
            if (this.hfLanguage.Value == "es-MX" || this.hfLanguage.Value == "es-PR")
            {
                return new string[] { "influenza - vacuna inyectable trivalente en dosis estándar", "influenza - vacuna inyectable tetravalente en dosis estándar", "influenza - dosis alta" };
            }
            else
            {
                return new string[] { "influenza - standard/pf injectable (trivalent)", "influenza - standard injectable quadrivalent", "influenza - high dose" };
            }
        }
    }
    private bool isMOPreviousSeasonBusiness
    {
        get
        {
            return Convert.ToBoolean(hfIsMoPrevious.Value);
        }
        set
        {
            hfIsMoPrevious.Value = value.ToString();
        }
    }
    private DataTable previousClinicLocation
    {
        get
        {
            DataTable result = new DataTable();

            if (ViewState["previousClinicLocation"] != null)
            {
                result = (DataTable)ViewState["previousClinicLocation"];
            }
            return result;
        }
        set
        {
            ViewState["previousClinicLocation"] = value;
        }
    }
    private bool isAddressDisabled
    {
        get
        {
            bool value = false;
            if (ViewState["isAddressDisabled"] != null)
                value = (bool)ViewState["isAddressDisabled"];
            return value;
        }
        set
        {
            ViewState["isAddressDisabled"] = value;
        }
    }
    private bool isRestrictedStoreState
    {
        get
        {
            bool value = false;
            if (ViewState["isRestrictedStoreState"] != null)
                value = (bool)ViewState["isRestrictedStoreState"];
            return value;
        }
        set
        {
            ViewState["isRestrictedStoreState"] = value;
        }
    }
    private string validationSummary { get; set; }
    private List<string> validationMsgList
    {
        get
        {
            if (!(ViewState["validationSummary"] is List<string>))
            {
                ViewState["validationSummary"] = new List<string>();
            }

            return (List<string>)ViewState["validationSummary"];
        }
    }
    private bool isClinicAfter2Weeks
    {
        get
        {
            bool value = false;
            if (ViewState["isClinicAfter2Weeks"] != null)
                value = (bool)ViewState["isClinicAfter2Weeks"];
            return value;
        }
        set
        {
            ViewState["isClinicAfter2Weeks"] = value;
        }
    }
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        //ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.xmlContractAgreement = new XmlDocument();
        this.contractCreatedDate = DateTime.Now;

        if (Session.IsNewSession)
        {
            FormsAuthentication.SignOut();
            string str = Request.Url.ToString();
            Response.Redirect(str);
        }
    }

    protected override void InitializeCulture()
    {
        if (Request.Form["hfLanguage"] != null)
        {
            Thread.CurrentThread.CurrentCulture =
               CultureInfo.CreateSpecificCulture(Request.Form["hfLanguage"]);
            Thread.CurrentThread.CurrentUICulture = new
                CultureInfo(Request.Form["hfLanguage"]);
            Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo("en-US").DateTimeFormat;
        }
        base.InitializeCulture();
    }
}