﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using TdApplicationLib;
using System.Web.Services;
using System.Configuration;
using tdEmailLib;
using TdWalgreens;
using System.Collections.Generic;
using NLog;


public partial class walgreensContactLog : Page
{
    #region -------------------- PROTECTED EVENTS --------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblMessage.Text = "";

        if (!IsPostBack)
        {
            this.bindContactLogDetails();
        }
        else
        {
            int contact_log_pk = 0;
            string event_target = (this.Request["__EVENTTARGET"] == null) ? string.Empty : this.Request["__EVENTTARGET"];
            string event_argument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];

            if (!string.IsNullOrEmpty(event_argument))
                Int32.TryParse(event_argument, out contact_log_pk);

            if (!string.IsNullOrEmpty(event_target) && event_target.ToLower() == "deletecontactlog" && contact_log_pk > 0)
                this.deleteSelectedContactLog(contact_log_pk);
            else if (!string.IsNullOrEmpty(event_target) && event_target.ToLower() == "showcontract")
            {
                //Displays existing contract agreement
                if (contact_log_pk > 0)
                {
                    this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = contact_log_pk;
                    Response.Redirect("walgreensClinicAgreement.aspx", false);
                }
                else
                {
                    this.lblMessage.Text = (string)GetGlobalResourceObject("errorMessages", "logBusinessContactInserted");
                    //this.assignedBusinessPk = Convert.ToInt32(this.ddlAssignedBusiness.SelectedValue);
                    Int32.TryParse(this.ddlAssignedBusiness.SelectedValue, out this.assignedBusinessPk);
                    this.clearFields();
                    this.ddlAssignedBusinesses.ClearSelection();
                    this.bindContactLogs();
                    this.lastContact();
                }
            }
            else if (!string.IsNullOrEmpty(event_target) && event_target.ToLower() == "showprevseasoncontract" && contact_log_pk > 0)
            {
                EncryptedQueryString encryped_link = new EncryptedQueryString();
                encryped_link["arg1"] = this.commonAppSession.LoginUserInfoSession.Email;
                encryped_link["arg2"] = contact_log_pk.ToString();
                encryped_link["arg3"] = "WalgreensUser";
                Response.Redirect("walgreensClinicAgreementPrevSeason.aspx?args=" + encryped_link.ToString(), false);
            }
        }

        this.PickerAndCalendar1.MaxDate = DateTime.Today.Date.AddDays(1);
        this.commonAppSession.SelectedStoreSession.referrerPath = "walgreensContactLog.aspx";
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate("contactLog");
        if (Page.IsValid)
        {
            string contact_log_pk = "";
            string error_message = "";
            int return_value = 0;
            this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = 0;

            if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 3)
                this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;

            string store_state = this.commonAppSession.SelectedStoreSession.storeState;
            //Do not log Event Scheduled contact outreach status here. Log the contact status along with the Scheduled event details in event scheduled form - CR(02/20/2015-Vishnu)
            if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" && this.ddlOutreach.SelectedItem.Value == @"3")
            {
                Dictionary<string, string> event_contact_log = new Dictionary<string, string>();
                event_contact_log.Add("storeId", this.commonAppSession.SelectedStoreSession.storeID.ToString());
                event_contact_log.Add("businessPk", this.ddlAssignedBusiness.SelectedValue);
                event_contact_log.Add("firstName", this.txtContactFirstName.Text);
                event_contact_log.Add("lastName", this.txtContactLastName.Text);
                event_contact_log.Add("contactDate", this.PickerAndCalendar1.getSelectedDate.ToString());
                event_contact_log.Add("feedback", this.txtFeedBack.Text);
                event_contact_log.Add("createdBy", this.commonAppSession.LoginUserInfoSession.UserID.ToString());

                Session["eventContactLog_" + this.commonAppSession.SelectedStoreSession.storeID + "_" + this.commonAppSession.LoginUserInfoSession.UserID.ToString()] = event_contact_log;
                Response.Redirect("scheduleEventForm.aspx", true);
            }
            bool is_restricted_store_state = ApplicationSettings.isRestrictedStoreState(store_state, this.commonAppSession.LoginUserInfoSession.UserRole);
            return_value = this.dbOperation.insertContactLog(this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId, this.commonAppSession.SelectedStoreSession.storeID, Convert.ToInt32(this.ddlAssignedBusiness.SelectedValue), this.txtContactFirstName.Text, this.txtContactLastName.Text, this.PickerAndCalendar1.getSelectedDate, Convert.ToInt32(this.ddlOutreach.SelectedItem.Value), this.txtFeedBack.Text, this.commonAppSession.LoginUserInfoSession.UserID.ToString(), (this.txtTitle.Visible && this.txtTitle.Text.Length > 0) ? this.txtTitle.Text : null, is_restricted_store_state, out contact_log_pk, out error_message);

            if (return_value == -2)
            {
                if (!string.IsNullOrEmpty(contact_log_pk) && Convert.ToInt32(this.ddlOutreach.SelectedValue) == 3)
                {
                    //string account_type = Convert.ToInt32(this.ddlOutreach.SelectedValue) == 3 ? "Local" : "Vote & Vax";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showContractInitiatedWarning('" + error_message + "', '" + contact_log_pk + "');", true);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
                    this.clearFields();
                }
                return;
            }
            else if (return_value == -1)
            {
                this.clearFields();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Error: Could not log the Business Contact!');", true);
                return;
            }
            //else if (return_value == -3)
            //{
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "disableAddBusinessToStore"), "Missouri", "20") + "');", true);
            //    this.clearFields();
            //    return;
            //}

            if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.ddlOutreach.SelectedItem.Value == @"3")
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
                Response.Redirect("walgreensClinicAgreement.aspx", false);
            }
            else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" && this.ddlOutreach.SelectedItem.Value == @"3")
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
                Response.Redirect("scheduleEventForm.aspx", false);
            }
            else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.ddlOutreach.SelectedItem.Value == @"2")
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
                Response.Redirect("walgreensFollowupEmail.aspx", false);
            }
            else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.ddlOutreach.SelectedItem.Value == @"8")
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
                Response.Redirect("walgreensCharityProgram.aspx", false);
            }
            //else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.ddlOutreach.SelectedItem.Value == @"11")
            //{
            //    this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
            //    Response.Redirect("walgreensVoteVaxAgreement.aspx", false);
            //}
            else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && this.ddlOutreach.SelectedItem.Value == @"12")
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = Convert.ToInt32(contact_log_pk);
                Response.Redirect("walgreensCommunityOutreachProgram.aspx", false);
            }

            //this.assignedBusinessPk = Convert.ToInt32(this.ddlAssignedBusiness.SelectedValue);
            Int32.TryParse(this.ddlAssignedBusiness.SelectedValue, out this.assignedBusinessPk);
            this.ddlAssignedBusinesses.ClearSelection();
            if (this.ddlAssignedBusinesses.Items.FindByValue(this.assignedBusinessPk.ToString()) != null)
                this.ddlAssignedBusinesses.Items.FindByValue(this.assignedBusinessPk.ToString()).Selected = true;
            this.lblMessage.Text = (string)GetGlobalResourceObject("errorMessages", "logBusinessContactInserted");

            this.clearFields();
            this.bindDropdowns();
            this.bindContactLogs();
            this.lastContact();
        }
    }

    protected void ddlAssignedBusinesses_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.tblPreviousSeasonLogs.Visible = false;
        //this.assignedBusinessPk = Convert.ToInt32(this.ddlAssignedBusinesses.SelectedValue);
        Int32.TryParse(this.ddlAssignedBusinesses.SelectedValue, out this.assignedBusinessPk);

        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 3)
            this.bindPreviousContactLog();
        else
            this.bindContactLogs();

        this.clearFields();
    }
    #endregion

    #region -------------------- PRIVATE METHODS ---------------------
    /// <summary>
    /// Displays selected stores compliance and completion information
    /// </summary>
    private void lastContact()
    {
        string last_access = string.Empty;
        string to_goal = string.Empty;
        string to_complete = string.Empty;

        this.dbOperation.getHeaderValues(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, ApplicationSettings.getOutreachStartDate, ApplicationSettings.getCurrentQuarter.ToString(), this.commonAppSession.LoginUserInfoSession.UserID, out last_access, out to_goal, out to_complete); //set the goal message here   

        if (!string.IsNullOrEmpty(last_access))
            this.commonAppSession.SelectedStoreSession.lastStatusProvided = (string)GetGlobalResourceObject("errorMessages", "lastStatusProvided") + " " + last_access;
        else
            this.commonAppSession.SelectedStoreSession.lastStatusProvided = "";

        this.walgreensHeaderCtrl.strStoreName = this.commonAppSession.SelectedStoreSession.storeName;
        this.walgreensHeaderCtrl.strLastStatusProvided = this.commonAppSession.SelectedStoreSession.lastStatusProvided;

        if (string.IsNullOrEmpty(to_goal)) to_goal = "0";
        if (string.IsNullOrEmpty(to_complete)) to_complete = "0";
        Literal lblHeaderDetails = ((Literal)walgreensHeaderCtrl.FindControl("lblHeaderDetails"));
        Literal lblLastStatusProvided = ((Literal)walgreensHeaderCtrl.FindControl("lblLastStatusProvided"));

        if (!this.commonAppSession.SelectedStoreSession.storeName.Contains("Nothing"))
        {
            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                case "regional vice president":
                case "regional healthcare director":
                    lblHeaderDetails.Text = "<span class='wagsAddress'> " + this.commonAppSession.LoginUserInfoSession.LocationType + " " + this.commonAppSession.LoginUserInfoSession.AssignedLocations + "</span><br />";
                    if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                        lblLastStatusProvided.Text = lblHeaderDetails.Text + "<span class='statusLine'>" + String.Format((string)GetGlobalResourceObject("errorMessages", "goalMessage"), to_goal, to_complete, "Q" + ApplicationSettings.getCurrentQuarter.ToString()) + "</span>";
                    break;
                default:
                    lblHeaderDetails.Text = "<span class='wagsAddress'>" + this.walgreensHeaderCtrl.strStoreName + "</span><br />";
                    if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                        lblHeaderDetails.Text = lblHeaderDetails.Text + "<span class='statusLine'>" + String.Format((string)GetGlobalResourceObject("errorMessages", "goalMessage"), to_goal, to_complete, "Q" + ApplicationSettings.getCurrentQuarter.ToString()) + "</span>";
                    lblLastStatusProvided.Text = "";
                    lblLastStatusProvided.Text = "<span class='statusLine'><b>" + this.walgreensHeaderCtrl.strLastStatusProvided + "</b></span>";
                    break;
            }
        }
        this.commonAppSession.SelectedStoreSession.HeaderDetails = "";
        this.commonAppSession.SelectedStoreSession.HeaderDetails = lblHeaderDetails.Text + "|" + lblLastStatusProvided.Text;
    }

    /// <summary>
    /// Binds store businesses and outreach statuses to dropdowns
    /// </summary>
    private void bindDropdowns()
    {
        string store_state;
        this.dataTable = this.dbOperation.getAssignedBusinesses(this.commonAppSession.SelectedStoreSession.storeID, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), out store_state);

        this.ddlAssignedBusiness.Items.Clear();
        this.ddlAssignedBusinesses.Items.Clear();

        DataTable dt_filtered_business = new DataTable();
        DataRow[] businesses = this.dataTable.Select("outreachEffort LIKE '%" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "%' AND businessType=" + this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId + " AND businessName <> ''");
        if (businesses.Count() == 0)
        {
            businesses = this.dataTable.Select("outreachEffort LIKE '%" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "%' AND businessType = 1 AND businessName <> ''");
            if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId != 3)
            {
                this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;
            }
        }
        if (businesses.Count() > 0)
            dt_filtered_business = businesses.OrderBy(row => row["businessName"]).CopyToDataTable();

        this.ddlAssignedBusiness.DataTextField = "businessName";
        this.ddlAssignedBusiness.DataValueField = "businessPk";
        this.ddlAssignedBusiness.DataSource = dt_filtered_business;
        this.ddlAssignedBusiness.DataBind();
        this.ddlAssignedBusiness.Items.Insert(0, new ListItem(" -- Select Business -- ", "0"));
        this.ddlAssignedBusiness = (DropDownList)ApplicationSettings.setItemToolTip(this.ddlAssignedBusiness);

        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 3)
        {
            this.lblContactLabel.Text = "Assisted Clinics";
            this.ddlAssignedBusinesses.DataSource = this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses;
            if (this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses.Rows.Count <= 0)
            {
                this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;
                this.ddlAssignedBusinesses.DataSource = dt_filtered_business;
            }
        }
        else
            this.ddlAssignedBusinesses.DataSource = dt_filtered_business;

        this.ddlAssignedBusinesses.DataTextField = "businessName";
        this.ddlAssignedBusinesses.DataValueField = "businessPk";
        this.ddlAssignedBusinesses.DataBind();
        this.ddlAssignedBusinesses.Items.Insert(0, new ListItem(" -- Select Business -- ", "0"));
        this.ddlAssignedBusinesses = (DropDownList)ApplicationSettings.setItemToolTip(this.ddlAssignedBusinesses);

        if (dt_filtered_business.Rows.Count > 0)
        {
            if (this.assignedBusinessPk > 0)
            {
                if (dt_filtered_business.Select("businessPk = " + this.assignedBusinessPk.ToString()).Count() > 0)
                {
                    DataRow dr = dt_filtered_business.Select("businessPk = " + this.assignedBusinessPk.ToString()).ElementAt(0);

                    if (dr != null)
                    {
                        this.txtContactFirstName.Text = dr["firstName"].ToString();
                        this.txtContactLastName.Text = dr["lastName"].ToString();
                        this.ddlAssignedBusinesses.Items.FindByValue(this.assignedBusinessPk.ToString()).Selected = true;
                        this.ddlAssignedBusiness.Items.FindByValue(this.assignedBusinessPk.ToString()).Selected = true;
                        this.lblBusinessName.Text = this.ddlAssignedBusiness.SelectedItem.Text;
                        this.hfIsExistingBusiness.Value = dr["isExistingBusiness"].ToString();

                        if (!string.IsNullOrEmpty(dr["jobTitle"].ToString()))
                        {
                            this.txtTitle.Text = dr["jobTitle"].ToString();
                        }
                    }
                }
            }
        }
        //else
        //    Response.Redirect("auth/sessionTimeout.aspx");

        this.dataTable = new DataTable();
        if (this.commonAppSession.LoginUserInfoSession != null)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 2)
                this.dataTable = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='AC' AND isActive = 'True'").CopyToDataTable();
            else
                this.dataTable = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "' AND isActive = 'True' AND pk NOT IN (9, 10)").CopyToDataTable();
        }
        else
            Response.Redirect("auth/sessionTimeout.aspx");

        if (this.dataTable.Rows.Count > 0)
        {
            this.ddlOutreach.DataSource = this.dataTable;
            this.ddlOutreach.DataTextField = "outreachStatus";
            this.ddlOutreach.DataValueField = "pk";
            this.ddlOutreach.DataBind();
            this.ddlOutreach.Items.RemoveAt(0);
            this.ddlOutreach.Items.Insert(0, new ListItem("-- Select Outreach Status --", "0"));
        }
    }

    /// <summary>
    /// Deletes selected contact log for the business
    /// </summary>
    /// <param name="contact_log_pk"></param>
    private void deleteSelectedContactLog(int contact_log_pk)
    {
        this.dbOperation.deleteContactLog(contact_log_pk, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId);
        if (this.assignedBusinessPk == 0)
            this.assignedBusinessPk = Convert.ToInt32(this.ddlAssignedBusinesses.SelectedValue);

        this.bindContactLogs();
        this.clearFields();
    }

    /// <summary>
    /// This function will be used to bind previous contact logs
    /// </summary>
    private void bindPreviousContactLog()
    {
        this.lblContactLabel.Text = "Assisted Clinics";
        this.lblCurrentSeasonLogs.Text = "";
        this.lblPreviousSeasonLogs.Text = "";

        if (this.assignedBusinessPk > 0)
        {
            this.dataSet = this.dbOperation.getPreviousContactLogs(this.assignedBusinessPk, this.commonAppSession.SelectedStoreSession.storeID);
            if (this.dataSet.Tables.Count > 0)
            {
                this.lblCurrentSeasonLogs.Text = this.displayContactLogs(this.dataSet.Tables[0], this.dataSet.Tables[1], true).ToString();
            }
        }
        if (this.ddlAssignedBusinesses.Items.FindByValue(this.assignedBusinessPk.ToString()) != null)
            this.ddlAssignedBusinesses.Items.FindByValue(this.assignedBusinessPk.ToString()).Selected = true;

        this.lblBusinessName.Text = this.ddlAssignedBusinesses.SelectedItem.Value != "0" ? this.ddlAssignedBusinesses.SelectedItem.Text : "";
    }

    /// <summary>
    /// This function will display contact logs
    /// </summary>
    /// <param name="tbl_current_contact"></param>
    /// <param name="tbl_current_contact_header"></param>
    /// <param name="is_previous_logs"></param>
    /// <returns></returns>
    private StringBuilder displayContactLogs(DataTable tbl_current_contact, DataTable tbl_current_contact_header, bool is_previous_logs)
    {
        int count = 1, contact_log_pk;
        StringBuilder strbulder = new StringBuilder();
        StringBuilder str_contact = new StringBuilder();
        StringBuilder strSameDateData = new StringBuilder();
        bool check_so = false;

        if (tbl_current_contact_header != null && tbl_current_contact_header.Rows.Count > 0)
        {
            foreach (DataRow rowDate in tbl_current_contact_header.Rows)
            {
                str_contact.Clear();
                int row_height = tbl_current_contact.Rows.Cast<DataRow>().Count(row => Convert.ToDateTime(rowDate["contactDate"]).ToString("MM/dd/yyyy") == Convert.ToDateTime(row["contactDate"]).ToString("MM/dd/yyyy") && rowDate["outreachEffort"].ToString() == row["outreachEffort"].ToString());
                if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == this.commonAppSession.SelectedStoreSession.SelectedContactOutreachEffort)
                    row_height = (row_height > 3) ? 300 : (row_height * 100);
                else
                    row_height = 200;

                count++;

                if (is_previous_logs)
                {
                    str_contact.Append("<div onclick='runAccordion(" + count + "," + row_height + "," + 1 + ");'>");
                    str_contact.Append("<div class='logTitle1' onselectstart='return false;'> <img id='AccordionPrevious" + count + "ContentImg' alt='' src='images/closedArrow.png' /> &nbsp;");
                }
                else
                {
                    str_contact.Append("<div onclick='runAccordion(" + count + "," + row_height + "," + 0 + ");'>");
                    str_contact.Append("<div class='logTitle1' onselectstart='return false;'> <img id='Accordion" + count + "ContentImg' alt='' src='images/closedArrow.png' /> &nbsp;");
                }

                if (rowDate["outreachEffortCode"].ToString() == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 3)
                    str_contact.Append(Convert.ToDateTime(rowDate["contactDate"]).ToString("MMMM") + " " + Convert.ToDateTime(rowDate["contactDate"]).Day + ", " + Convert.ToDateTime(rowDate["contactDate"]).ToString("yyyy"));
                else
                    str_contact.Append(Convert.ToDateTime(rowDate["contactDate"]).ToString("MMMM") + " " + Convert.ToDateTime(rowDate["contactDate"]).Day + ", " + Convert.ToDateTime(rowDate["contactDate"]).ToString("yyyy") + " - " + rowDate["outreachEffort"].ToString());

                str_contact.Append("</div></div>");
                strSameDateData.Clear();

                foreach (DataRow row in tbl_current_contact.Rows.Cast<DataRow>().Where(row => Convert.ToDateTime(rowDate["contactDate"]).ToString("MM/dd/yyyy") == Convert.ToDateTime(row["contactDate"]).ToString("MM/dd/yyyy") && rowDate["outreachEffort"].ToString() == row["outreachEffort"].ToString()))
                {
                    strSameDateData.Append("<b>Outreach Status:</b> ");
                    contact_log_pk = Convert.ToInt32(row["contactLogPk"]);

                    if (is_previous_logs)
                    {
                        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && row["businessType"].ToString() == "1" && rowDate["outreachEffortCode"].ToString() == "1" && row["outreachStatusId"].ToString() == "3" && this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == row["outreachEffortCode"].ToString())
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('scheduleEventForm.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && row["businessType"].ToString() == "1" && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "3" && Convert.ToDateTime(row["contactDate"]) < Convert.ToDateTime("05/21/2015"))
                            strSameDateData.Append(row["outreachStatus"].ToString() + "  -  <a onClick=\"showPrevSeasonContract('walgreensClinicAgreementPrevSeason.aspx', " + row["contactLogPk"].ToString() + ")\" href='#'>Community Off-Site Clinic Agreement</a>");
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && row["businessType"].ToString() == "1" && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "3")
                            strSameDateData.Append(row["outreachStatus"].ToString() + "  -  <a onClick=\"showContactLogDetails('walgreensClinicAgreement.aspx', " + row["contactLogPk"].ToString() + ")\" href='#'>Community Off-Site Clinic Agreement</a>");
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && row["businessType"].ToString() == "1" && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "11")
                            strSameDateData.Append(row["outreachStatus"].ToString() + "  -  <a onClick=\"showContactLogDetails('walgreensVoteVaxAgreement.aspx', " + row["contactLogPk"].ToString() + ")\" href='#'>Community Off-Site Clinic Agreement</a>");
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && row["businessType"].ToString() == "1" && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "12")
                            strSameDateData.Append(row["outreachStatus"].ToString() + "  -  <a onClick=\"showContactLogDetails('walgreensCommunityOutreachProgram.aspx', " + row["contactLogPk"].ToString() + ")\" href='#'>Community Outreach Program</a>");
                        else
                            strSameDateData.Append(row["outreachStatus"].ToString());

                        strSameDateData.Append("<br/>" + "<b>Contact Name:</b> " + row["contactName"].ToString() + "<br />" + "<b>Store ID:</b> " + (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 3 ? row["clinicStoreId"].ToString() : row["storeId"].ToString()) + "<br />" + "<b>Created By:</b> " + row["createdBy"].ToString() + "<br />" + Server.HtmlDecode(row["feedback"].ToString()));
                        strSameDateData.Append("<hr/>");
                        strSameDateData.Append("Entered " + Convert.ToDateTime(row["created"]).ToString("MM/dd/yyyy") + " at " + Convert.ToDateTime(row["created"]).ToString("hh:mm tt") + "<br/><br/>");
                    }
                    else
                    {
                        if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && rowDate["outreachEffortCode"].ToString() == "1" && row["outreachStatusId"].ToString() == "3" && this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == row["outreachEffortCode"].ToString())
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('scheduleEventForm.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && row["outreachStatusId"].ToString() == "2" && this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == row["outreachEffortCode"].ToString())
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensFollowupEmail.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "3" && Convert.ToBoolean(row["isDeleted"]))
                        {
                            strSameDateData.Append("Contract Deleted" + "  -  <a onClick=\"showContactLogDetails('walgreensClinicAgreement.aspx', " + row["contactLogPk"].ToString() + ")\" href='#'>Community Off-Site Clinic Agreement</a>");
                        }
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "3" && Convert.ToBoolean(row["isDeleted"]))
                        {
                            strSameDateData.Append("Contract Deleted");
                            check_so = true;
                        }
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "3")
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensClinicAgreement.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 2 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId.ToString() != row["businessType"].ToString())
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensClinicAgreement.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "8")
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensCharityProgram.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "8" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId.ToString() != row["businessType"].ToString() && this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == row["outreachEffortCode"].ToString())
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensCharityProgram.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        //else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "11")
                        //    strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensVoteVaxAgreement.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        //else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 2 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "11" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId.ToString() != row["businessType"].ToString())
                        //    strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensVoteVaxAgreement.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "12")
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensCommunityOutreachProgram.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 2 && rowDate["outreachEffortCode"].ToString() == "3" && row["outreachStatusId"].ToString() == "12" && this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId.ToString() != row["businessType"].ToString())
                            strSameDateData.Append("<a onClick=\"showContactLogDetails('walgreensCommunityOutreachProgram.aspx', " + contact_log_pk + ")\" href='#' >" + row["outreachStatus"].ToString() + "</a>");
                        else
                            strSameDateData.Append(row["outreachStatus"].ToString());

                        strSameDateData.Append("<br/>" + "<b>Contact Name:</b> " + row["contactName"].ToString() + "<br />" + "<b>Store ID:</b> " + (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 3 ? row["clinicStoreId"].ToString() : row["storeId"].ToString()) + "<br />" + "<b>Created By:</b> " + row["createdBy"].ToString() + "<br />");
                        strSameDateData.Append(!String.IsNullOrEmpty(Server.HtmlDecode(row["feedback"].ToString())) ? Server.HtmlDecode(row["feedback"].ToString()) + "<br />" : null);
                        if ((this.ddlOutreach.Items.FindByText(row["outreachStatus"].ToString()) != null || row["outreachStatus"].ToString() == "Contract Initiated") && Convert.ToBoolean(row["isDeleted"]) && Convert.ToInt32(row["outreachStatusId"]) == 3)
                        {
                            strSameDateData.Append("<b>Deleted By:</b> " + row["deletedBy"].ToString() + "<br />");
                            strSameDateData.Append("<b>Deleted Date:</b> " + row["dateDeleted"].ToString() + "<br />");
                            strSameDateData.Append("<hr/>");
                            if (!check_so)
                                strSameDateData.Append("Entered " + Convert.ToDateTime(row["created"]).ToString("MM/dd/yyyy") + " at " + Convert.ToDateTime(row["created"]).ToString("hh:mm tt") + "<br/><br/>");
                        }
                        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == rowDate["outreachEffortCode"].ToString())
                        {
                            if ((this.ddlOutreach.Items.FindByText(row["outreachStatus"].ToString()) != null || row["outreachStatus"].ToString() == "Vouchers Provided") && !Convert.ToBoolean(row["isDeleted"]))
                            {
                                strSameDateData.Append("<hr/>");
                                strSameDateData.Append("Entered " + Convert.ToDateTime(row["created"]).ToString("MM/dd/yyyy") + " at " + Convert.ToDateTime(row["created"]).ToString("hh:mm tt") + "  <a href='#' onClick='javascript:deleteCatalog(" + contact_log_pk + ");'> Remove Entry </a><br/><br/>");
                            }
                            else if (Convert.ToInt32(row["outreachStatusId"]) == 9 || Convert.ToInt32(row["outreachStatusId"]) == 10)
                            {
                                strSameDateData.Append("<hr/>");
                                strSameDateData.Append("Entered " + Convert.ToDateTime(row["created"]).ToString("MM/dd/yyyy") + " at " + Convert.ToDateTime(row["created"]).ToString("hh:mm tt") + " <br/><br/>");
                            }
                        }
                    }
                }
                if (is_previous_logs)
                    str_contact.Append("<div id='AccordionPrevious" + count + "Content' class='AccordionPreviousContent1'>" + strSameDateData + " </div> ");
                else
                    str_contact.Append("<div id='Accordion" + count + "Content' class='AccordionContent1'>" + strSameDateData + " </div> ");

                strbulder.Append(str_contact.ToString());
            }
        }
        return strbulder;
    }

    /// <summary>
    /// Displays current and previous season business contact logs
    /// </summary>
    private void bindContactLogs()
    {
        this.lblContactLabel.Text = "Contact Log";
        this.lblCurrentSeasonLogs.Text = "";
        this.lblPreviousSeasonLogs.Text = "";

        if (this.assignedBusinessPk > 0)
        {
            this.dataSet = this.dbOperation.getContactLogs(this.assignedBusinessPk, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId);

            if (this.dataSet.Tables.Count > 0)
            {
                //Bind current season contact logs
                if (this.dataSet.Tables[0].Select("isPreviousSeasonLog = 0").Count() > 0)
                    this.lblCurrentSeasonLogs.Text = this.displayContactLogs(this.dataSet.Tables[0].Select("isPreviousSeasonLog = 0").CopyToDataTable(), this.dataSet.Tables[1].Select("isPreviousSeasonLog = 0").CopyToDataTable(), false).ToString();

                if (this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId == 1)
                {
                    //Bind previous season contact logs
                    if (this.dataSet.Tables[0].Select("isPreviousSeasonLog = 1").Count() > 0)
                    {
                        this.tblPreviousSeasonLogs.Visible = true;
                        this.lblPreviousSeasonLogs.Text = this.displayContactLogs(this.dataSet.Tables[0].Select("isPreviousSeasonLog = 1").CopyToDataTable(), this.dataSet.Tables[1].Select("isPreviousSeasonLog = 1").CopyToDataTable(), true).ToString();
                    }
                }
            }

            if (this.ddlAssignedBusinesses.Items.FindByValue(this.assignedBusinessPk.ToString()) != null)
                this.ddlAssignedBusinesses.Items.FindByValue(this.assignedBusinessPk.ToString()).Selected = true;
            this.lblBusinessName.Text = this.ddlAssignedBusinesses.SelectedItem.Value != "0" ? this.ddlAssignedBusinesses.SelectedItem.Text : "";
        }
    }

    /// <summary>
    /// Clear all the controls Once ContactLog information is saved.
    /// </summary>
    private void clearFields()
    {
        this.ddlAssignedBusiness.ClearSelection();
        this.txtFeedBack.Text = "";
        this.txtContactFirstName.Text = "";
        this.txtContactLastName.Text = "";
        this.PickerAndCalendar1.getSelectedDate = DateTime.Today.Date;
        this.ddlOutreach.ClearSelection();
    }

    /// <summary>
    /// Displays contact log details
    /// </summary>
    private void bindContactLogDetails()
    {
        storeId = this.commonAppSession.SelectedStoreSession.storeID;
        this.assignedBusinessPk = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk;
        this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
        this.lblBusinessName.Text = "";

        this.bindDropdowns();

        this.PickerAndCalendar1.getSelectedDate = DateTime.Today.Date;
        this.tblPreviousSeasonLogs.Visible = false;

        this.bindContactLogs();

        this.ddlOutreach.ClearSelection();
        this.ddlOutreach.SelectedValue = this.commonAppSession.SelectedStoreSession.SelectedContactOutreachStatusId.ToString();
    }
    #endregion

    #region -------------------- PUBLIC VARIABLES --------------------
    public static int storeId = 0;
    #endregion

    #region -------------------- PRIVATE VARIABLES -------------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    private DataSet dataSet;
    private DataTable dataTable;
    private int assignedBusinessPk = 0;
    private WalgreenEmail walgreensEmail = null;
    #endregion

    #region -------------------- Web Form Designer generated code --------------------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }

    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        //if (this.walgreensHeaderCtrl.isddlTdVisible)
        //commonAppSession.SelectedStoreSession.storeFullName =this.walgreensHeaderCtrl.selectedStoreOnNonAdmin;
        //commonAppSession.SelectedStoreSession.storeID = Convert.ToInt32(this.walgreensHeaderCtrl.storeId);
        //if (!this.walgreensHeaderCtrl.selectedStoreOnNonAdmin.Contains("Nothing"))
        //    commonAppSession.SelectedStoreSession.storeName = this.walgreensHeaderCtrl.selectedStoreOnNonAdmin;
        this.bindContactLogDetails();
    }
    #endregion

    [WebMethod]
    public static void setSelectedContactLogPk(string contact_log_pk)
    {
        try
        {
            AppCommonSession common_app_session = new AppCommonSession();
            int selected_contact_log_pk = 0;
            Int32.TryParse(contact_log_pk, out selected_contact_log_pk);
            common_app_session.SelectedStoreSession.SelectedContactLogPk = selected_contact_log_pk;
        }
        catch (Exception ex)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Error("Error message: " + ex.Message + " Stack Trace: " + ex.StackTrace);
        }
    }

    /// <summary>
    /// web method to get the values for ContactName, OutreachStatus for ContactLogPage.
    /// </summary>
    /// <param name="store_id"></param>
    /// <param name="business_id"></param>
    /// <param name="wave_id"></param>
    /// <returns></returns>
    [WebMethod]
    public static JqgridInfo getContactName(string store_id, string business_id, string business_type, string outreach_pk)
    {
        JqgridInfo order_info = new JqgridInfo();
        DBOperations db_operation = new DBOperations();
        try
        {
            DataTable dt_businesses = db_operation.getBusinessOutreachStatus(Convert.ToInt32(store_id), Convert.ToInt32(business_id), Convert.ToInt32(business_type), Convert.ToInt32(outreach_pk));
            order_info = (new JqgridInfo
            {
                firstName = dt_businesses.Rows[0]["firstName"].ToString(),
                lastName = dt_businesses.Rows[0]["lastName"].ToString(),
                outreachStatusId = dt_businesses.Rows[0]["outreachStatus"].ToString()
            });
        }
        catch (Exception ex)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Error("Error message: " + ex.Message + " Stack Trace: " + ex.StackTrace);
        }
        return order_info;
    }
}