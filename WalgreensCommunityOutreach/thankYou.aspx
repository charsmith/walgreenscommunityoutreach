﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="thankYou.aspx.cs" Inherits="thankYou" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.history.forward(0);
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:Label ID="lblNewWindow" runat="server" style="display:none"></asp:Label>
<table width="936" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
      <td class="landingHeadBkgd">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="332" align="left" valign="top"><img src="images/wags_landing_logo.png" width="312" /><br />
                    <span width="312" class="outreachTitle" runat="server" id="lblOutreachTitle">Community Outreach</span>
                <%--<asp:Image ID="imgCommunityOutreach" runat="server" width="312" height="40" ImageUrl="images/head_community_outreach.png" />  --%>
                </td>
                <td width="593" colspan="2" rowspan="2" align="right" valign="top"><img src="images/spacer.gif" width="1" height="302" /></td>
            </tr>
            <tr>
    		    <td align="left" valign="bottom" style="padding:0px 0px 12px 20px;">&nbsp;</td>
  		    </tr>
            <tr>
    		    <td colspan="3" align="left" valign="top" class="navBar"><img src="images/spacer.gif" alt="" name="" width="1" height="25" border="0" id="home" /></td>
  		    </tr>
            <tr>
                <td colspan="3">
                     <table id="tableThankYouScheduled" runat="server" width="935" border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td colspan="2" align="left" valign="bottom" >  <asp:Image ID="imgThankYou" runat="server" width="790" height="375" ImageUrl="images/thank_you.jpg" /></td>
                        </tr>
                    </table>
                    <table id="tableThankYouAgreementApproved" runat="server" visible="false" cellspacing="0" cellpadding="0">
                     <tbody>
                        <tr>
                            <td rowspan="2" valign="bottom"><asp:Image ID="imgThankYouPharmacist" runat="server" ImageUrl="~/images/thank_you_pharmacist.jpg" Height="372" Width="261" /></td>
                            <td><asp:Image ID="imgThankYouAgreement" runat="server" ImageUrl="~/images/thank_you_agreement.png" Height="180" Width="673" /></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table width="630px" align="right" cellpadding="0" cellspacing="10">
                                    <tbody>
                                        <tr>
                                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #bbbbbb;"><asp:Label ID="lblVARText" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="class5">
                                                <table >
                                                    <tr>
                                                        <td><a id="lnkbtnVARFormEn"  runat="server" target="_blank" class="class5">Vaccine Administration Record - Informed Consent for Vaccination Form</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a id="lnkbtnVARFormSp" runat="server" target="_blank" class="class5">Vaccine Administration Record - Informed Consent for Vaccination Form (Spanish)</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #bbbbbb;">
                                                <table>
                                                    <tr>
                                                        <td><asp:Label ID="lblCorporateToInvoice" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><asp:Label ID="lblInsurance" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                   </table>
                </td>
            </tr>       
        </table>
    </td>
  </tr>
</table>

</form>
</body>
</html>
