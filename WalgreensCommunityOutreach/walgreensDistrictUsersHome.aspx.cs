﻿using System;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using TdWalgreens;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using NLog;

public partial class walgreensDistrictUsersHome : Page
{
    #region ------------------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.commonAppSession.LoginUserInfoSession.UserRole.Trim().Length > 0)
        {
            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower().Trim())
            {
                case "healthcare supervisor":
                case "director – rx & retail ops":
                    this.rowActionItemsHCSUsers.Visible = false;
                    break;
                case "district manager":
                    this.rowImmunizationSnapshot.Visible = false;
                    this.rowActionItemsHCSUsers.Visible = false;
                    break;
                default:
                    //this.bindHCSUsersData();
                    break;
            }
        }

        if (!IsPostBack)
        {
            this.displayActionReportData();
        }
    }

    protected void btnRefreshActionDropdown_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        this.displayActionReportData();
    }

    #endregion

    #region ------------------- PRIVATE METHODS ------------
    /// <summary>
    /// Displays action items in Action Items control
    /// </summary>
    private void displayActionReportData()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "displayActionReportData",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        
        int selected_user_id;
        int.TryParse(this.txtSelectedUserId.Text.Trim(), out selected_user_id);
        if (selected_user_id == 0)
            selected_user_id = this.commonAppSession.LoginUserInfoSession.UserID;

        this.commonAppSession.SelectedStoreSession.SelectedHCSUserId = selected_user_id;
        this.walgreensActionItems.bindActionReportData(selected_user_id, this.commonAppSession.SelectedStoreSession.storeID);
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "displayActionReportData",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        
    }
    #endregion

    [WebMethod]
    public static void setSelectedTabId(string tab_id, string grid_type)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        if (grid_type == "CorporateClinics")
            common_app_session.SelectedStoreSession.SelectedCorporateClinicsTabId = tab_id;
        else if (grid_type == "DirectB2BMailBusiness")
            common_app_session.SelectedStoreSession.SelectedDirectB2BMailTabId = tab_id;
        else if (grid_type == "LocalLeads")
            common_app_session.SelectedStoreSession.SelectedLocalLeadsTabId = tab_id;
    }

    [WebMethod]
    public static string getFilterData()
    {
        AppCommonSession common_app_session = new AppCommonSession();

        string sql_conn_str = ConfigurationManager.ConnectionStrings["printOnlineConnString"].ConnectionString;
        DataTable dt_assigned_locations = new DataTable();

        using (SqlConnection sql_conn = new SqlConnection(sql_conn_str))
        {
            using (SqlDataAdapter sql_adapter = new SqlDataAdapter("uspGetUserAssignedLocations", sql_conn))
            {
                sql_adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sql_adapter.SelectCommand.Parameters.Clear();
                sql_adapter.SelectCommand.Parameters.AddWithValue("@userId", common_app_session.LoginUserInfoSession.UserID);
                sql_adapter.SelectCommand.Parameters.AddWithValue("@userRole", common_app_session.LoginUserInfoSession.UserRole);
                sql_adapter.Fill(dt_assigned_locations);
            }
        }

        return dt_assigned_locations.getJSONObject();
    }

    [WebMethod]
    public static List<jsonReturnValue> getHCSUsersList()
    {
        AppCommonSession common_app_session = new AppCommonSession();
        List<jsonReturnValue> return_value = new List<jsonReturnValue>();
        bool has_items = false;
        string sql_conn_str = ConfigurationManager.ConnectionStrings["printOnlineConnString"].ConnectionString;

        using (SqlConnection sql_conn = new SqlConnection(sql_conn_str))
        {
            using (SqlCommand sql_cmd = new SqlCommand())
            {
                sql_cmd.CommandText = "uspGetHCSUsersData";
                sql_cmd.Connection = sql_conn;
                sql_conn.Open();
                sql_cmd.CommandType = CommandType.StoredProcedure;
                sql_cmd.Parameters.Clear();
                sql_cmd.Parameters.AddWithValue("@userId", common_app_session.LoginUserInfoSession.UserID);

                SqlDataReader sql_dr = sql_cmd.ExecuteReader();
                while (sql_dr.Read())
                {
                    jsonReturnValue return_item = new jsonReturnValue();
                    return_item.id = sql_dr["pk"].ToString().Trim();
                    return_item.name = sql_dr["HCSUser"].ToString().Trim();

                    return_value.Add(return_item);
                    has_items = true;
                }
            }
        }

        return (has_items) ? return_value : null;
    }

    #region ------------------- PRIVATE VARIABLES ------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();

        if (this.commonAppSession.LoginUserInfoSession != null && (!this.commonAppSession.LoginUserInfoSession.IsAdmin && !this.commonAppSession.LoginUserInfoSession.IsPowerUser))
            Response.Redirect("walgreensLandingPage.aspx");
    }
    #endregion
}