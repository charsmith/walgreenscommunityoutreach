﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;
using TdApplicationLib;
using System.Data;
using System.Xml;
using System.Diagnostics;
using System.Reflection;

public partial class scheduledClinicsValidationReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt_validations = new DBOperations().getScheduleClinicValidationRpt;

            Dictionary<int, string> wag_users = (Dictionary<int, string>)Application["WagUsers"];
            var lst_users = (wag_users.GroupBy(i => i.Value).Select(group => new
            {
                Role = group.Key,
                Count = group.Count()
            })).ToList();
            foreach (var user in lst_users)
            {
                dt_validations.Rows.Add((new Object[] { "Active Users", user.Count, user.Role }));
            }

            this.grdClinicValidationRpt.DataSource = dt_validations;
            this.grdClinicValidationRpt.DataBind();

            this.lblLoggedInWagUsers.Text = "Logged In Walgreens Users: " + wag_users.Count.ToString();

            this.ShowSessionSize();
            //this.ShowSQLServerRAMCPUSize();
        }
    }

    private void ShowSessionSize()
    {
        this.lblSessionStorageSize.Text = "Session Trace Info<br/>";

        long totalSessionBytes = 0;
        long obj_session_bytes = 0;
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter b = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        System.IO.MemoryStream m = null;
        string str = string.Empty;
        foreach (string key in Session)
        {
            obj_session_bytes = 0;
            var sess_obj = Session[key];
            if (sess_obj != null && sess_obj.ToString() != "TdApplicationLib.AppCommonSession") //&& sess_obj.ToString() != "TdApplicationLib.SelectedStoreInfo") //&& sess_obj.ToString() != "TdApplicationLib.LoginUserInfo")
            {
                m = new System.IO.MemoryStream();

                switch (sess_obj.GetType().Name)
                {
                    case "XmlDocument":
                        str = ((XmlDocument)sess_obj).OuterXml;
                        b.Serialize(m, str);
                        obj_session_bytes = m.Length;
                        break;
                    case "LoginUserInfo":
                    case "SelectedStoreInfo":
                    case "CorporateScheduler":
                        Type obj_type = sess_obj.GetType();
                        IList<PropertyInfo> obj_props = new List<PropertyInfo>(obj_type.GetProperties());


                        foreach (PropertyInfo prop_info in obj_props)
                        {
                            Object obj = prop_info.GetValue(sess_obj, null);
                            if (obj != null)
                                b.Serialize(m, obj);

                            obj_session_bytes += m.Length;
                        }
                        break;
                    default:
                        b.Serialize(m, sess_obj);
                        obj_session_bytes = m.Length;
                        break;
                }

                totalSessionBytes += obj_session_bytes;

                if (m != null)
                    this.lblSessionStorageSize.Text += String.Format("{0}: {1:n} kb;<br/>", key, obj_session_bytes / 1024);
            }
        }

        this.lblSessionStorageSize.Text += "<br/>" + String.Format("Total Size of Session Data: {0:n} kb", totalSessionBytes / 1024);
    }

    private void ShowSQLServerRAMCPUSize()
    {
        System.Diagnostics.Process[] p1 = System.Diagnostics.Process.GetProcesses();
        PerformanceCounter ramCounter = null;
        PerformanceCounter cpuCounter = null;
        foreach (System.Diagnostics.Process pro in p1)
        {
            if ((pro.ProcessName.ToUpper().Contains("SQLSERVR")))
            {
                ramCounter = new PerformanceCounter("Process", "Working Set", pro.ProcessName);
                cpuCounter = new PerformanceCounter("Process", "% Processor Time", pro.ProcessName);
                double ram = ramCounter.NextValue();
                double cpu = cpuCounter.NextValue();
                this.lblProcessorUsage.Text = "RAM: " + (ram / 1024 / 1024) + " MB; CPU: " + (cpu) + " %";
            }
        }
    }

}