﻿using System;
using Microsoft.Reporting.WebForms;
using System.Data;
using TdApplicationLib;
using System.Web.UI;
using System.Linq;
using TdWalgreens;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using NLog;

public partial class reportScheduledAppointment : Page
{
    #region --------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindCorporateClients(this.commonAppSession.SelectedStoreSession.schedulerDesignPk);

            this.ddlApptTypes.SelectedValue = "1";
            this.reportBind();
            this.commonAppSession.SelectedStoreSession.schedulerDesignPk = 0;
            this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
        }
    }

    protected void ddlCorporateClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ddlApptTypes.ClearSelection();
        this.ddlClinicDates.ClearSelection();
        this.ddlClinicRooms.ClearSelection();
        this.bindCorporateClients(Convert.ToInt32(this.ddlCorporateClients.SelectedValue));
        this.reportBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }
    #endregion

    #region ----------- PRIVATE METHODS -------------
    /// <summary>
    /// Binds corporate clients
    /// </summary>
    private void bindCorporateClients(int client_id)
    {
        DataSet ds_corporate_clients = new DataSet();
        this.logger.Info("Binding Corporate Clients list to Scheduled Appointments Report {0} - START", this.commonAppSession.LoginUserInfoSession.UserName);
        if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.referrerPath) && this.commonAppSession.SelectedStoreSession.referrerPath == "walgreensLandingPage.aspx")
            ds_corporate_clients = this.dbOperation.getCorporateClientsForReports(this.commonAppSession.LoginUserInfoSession.UserRole, this.commonAppSession.LoginUserInfoSession.UserID, null);
        else
            ds_corporate_clients = this.dbOperation.getCorporateClientsForReports(this.commonAppSession.LoginUserInfoSession.UserRole, this.commonAppSession.LoginUserInfoSession.UserID, client_id);
        this.logger.Info("Binding Corporate Clients list to Scheduled Appointments Report {0} - END", this.commonAppSession.LoginUserInfoSession.UserName);
        List<string> lst_clinics = new List<string>();

        if (ds_corporate_clients.Tables[0].Rows.Count > 0)
        {
            this.ddlCorporateClients.DataSource = ds_corporate_clients.Tables[0];
            this.ddlCorporateClients.DataTextField = "businessName";
            this.ddlCorporateClients.DataValueField = "designPk";
            this.ddlCorporateClients.DataBind();
            if (ds_corporate_clients.Tables[0].Rows.Count > 1)
                this.ddlCorporateClients.Items.Insert(0, new ListItem("All", "0"));
            else
                this.ddlCorporateClients.Items[0].Selected = true;

            foreach (string clinic_dates in ds_corporate_clients.Tables[1].Rows.OfType<DataRow>().Select(row => row["clinicDate"].ToString()).ToList())
            {
                clinic_dates.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToList()
                    .ForEach(clinic_date => lst_clinics.Add(clinic_date));
            }

            this.ddlClinicDates.DataSource = lst_clinics.Distinct().OrderBy(x => x.ToString()).ToList();
            this.ddlClinicDates.DataBind();
            if (this.ddlClinicDates.Items.Count > 1)
                this.ddlClinicDates.Items.Insert(0, new ListItem("All", ""));
            else if (this.ddlClinicDates.Items.Count == 1)
                this.ddlClinicDates.Items[0].Selected = true;

            lst_clinics.Clear();
            foreach (string clinic_rooms in ds_corporate_clients.Tables[1].Rows.OfType<DataRow>().Select(row => row["clinicRoom"].ToString()).ToList())
            {
                clinic_rooms.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToList()
                    .ForEach(clinic_room => lst_clinics.Add(clinic_room.Trim()));
            }

            this.ddlClinicRooms.DataSource = lst_clinics.Distinct().OrderBy(x => x.ToString()).ToList();
            this.ddlClinicRooms.DataBind();
            this.ddlClinicRooms.Items.Insert(0, new ListItem("All", ""));

            if (this.ddlCorporateClients.Items.FindByValue(client_id.ToString()) != null)
                this.ddlCorporateClients.Items.FindByValue(client_id.ToString()).Selected = true;
        }
        else
        {
            this.ddlCorporateClients.Items.Insert(0, new ListItem("-- Select --", ""));
            this.ddlClinicDates.Items.Insert(0, new ListItem("-- Select --", ""));
            this.ddlClinicRooms.Items.Insert(0, new ListItem("-- Select --", ""));
            this.ddlCorporateClients.Enabled = false;
            this.ddlClinicDates.Enabled = false;
            this.ddlClinicRooms.Enabled = false;
            this.ddlApptTypes.Enabled = false;
            this.btnSubmit.Enabled = false;
        }
    }

    /// <summary>
    /// Generates report scheduled appointments report
    /// </summary>
    private void reportBind()
    {
        this.logger.Info("Binding Corporate Clinic Scheduled Appointments Report by user {0} - START", this.commonAppSession.LoginUserInfoSession.UserName);
        DataTable dt_scheduled_appts;
        int design_pk = 0;
        int.TryParse(this.ddlCorporateClients.SelectedValue, out design_pk);
        this.ScheduledAppointmentReport.ProcessingMode = ProcessingMode.Local;

        if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.referrerPath) && this.commonAppSession.SelectedStoreSession.referrerPath == "walgreensLandingPage.aspx")
        {
            this.commonAppSession.SelectedStoreSession.referrerPath = string.Empty;
            dt_scheduled_appts = this.dbOperation.getCorporateClientScheduledApptsReport(this.commonAppSession.LoginUserInfoSession.UserRole, this.commonAppSession.LoginUserInfoSession.UserID,
                design_pk, this.commonAppSession.SelectedStoreSession.schedulerDesignPk, (string.IsNullOrEmpty(this.ddlApptTypes.SelectedValue) ? 0 : Convert.ToInt32(this.ddlApptTypes.SelectedValue))
                , this.ddlClinicRooms.SelectedValue, this.ddlClinicDates.SelectedValue);
        }
        else
            dt_scheduled_appts = this.dbOperation.getCorporateClientScheduledApptsReport(this.commonAppSession.LoginUserInfoSession.UserRole, this.commonAppSession.LoginUserInfoSession.UserID, design_pk, 0
                , (string.IsNullOrEmpty(this.ddlApptTypes.SelectedValue) ? 0 : Convert.ToInt32(this.ddlApptTypes.SelectedValue)), this.ddlClinicRooms.SelectedValue, this.ddlClinicDates.SelectedValue);

        this.logger.Info("Results were fetched from {0} method. Binding the report now..", "getCorporateClientScheduledApptsReport");
        //if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        //    dt_scheduled_appts = this.dbOperation.getCorporateClientScheduledApptsReport(design_pk, 0, (string.IsNullOrEmpty(this.ddlApptTypes.SelectedValue) ? 0 : Convert.ToInt32(this.ddlApptTypes.SelectedValue)), this.ddlClinicRooms.SelectedValue, this.ddlClinicDates.SelectedValue);
        //else
        //    dt_scheduled_appts = this.dbOperation.getCorporateClientScheduledApptsReport(0, design_pk, (string.IsNullOrEmpty(this.ddlApptTypes.SelectedValue) ? 0 : Convert.ToInt32(this.ddlApptTypes.SelectedValue)), this.ddlClinicRooms.SelectedValue, this.ddlClinicDates.SelectedValue);

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblClinicSchedulerEeRegistry";
        rds.Value = dt_scheduled_appts;
        this.ScheduledAppointmentReport.LocalReport.DataSources.Clear();
        this.ScheduledAppointmentReport.LocalReport.DataSources.Add(rds);
        this.ScheduledAppointmentReport.ShowPrintButton = false;
        this.ScheduledAppointmentReport.LocalReport.ReportPath = Server.MapPath("scheduledAppointment.rdlc");

        dt_scheduled_appts = null;
        this.logger.Info("Binding Corporate Clinic Scheduled Appointments Report by user {0} - END", this.commonAppSession.LoginUserInfoSession.UserName);
    }
    #endregion

    #region ----------- PRIVATE VARIABLES------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion

}
