﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportVoteNVaxClinicDetails.aspx.cs" Inherits="reports_reportVoteNVaxClinicDetails" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <style type="text/css">
        .ui-widget {
            font-size: 11px;
        }

        .cart-calendar-month {
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        var minOutreachDate = new Date("<%=outreachStartDate %>");
        var maxOutreachDate = new Date();
        var selectedFromDate = "<%=selectedFromDate %>";
        var selectedToDate = "<%=selectedToDate %>";

        $(document).ready(function () {
            dropdownRepleceText("ddlClinicType", "txtClinicType");
            dropdownRepleceText("ddlClinicStatus", "txtClinicStatus");

            $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
            $("#ReportFrameReportViewer1").css("height", "450px");
            $("#rptVoteNVaxClinicsReport_ctl10").css("width", "850px");
            $("#rptVoteNVaxClinicsReport_ctl10").css("height", "400px");
            $("#rptVoteNVaxClinicsReport_ctl09").css("width", "850px");
            $("#rptVoteNVaxClinicsReport_ctl09").css("height", "400px");

            $("#txtClinicDateFrom").attr("readonly", "readonly");
            $("#txtClinicDateTo").attr("readonly", "readonly");

            createDateCalendar($("#ddlIPSeasons").val());

            $("#ddlIPSeasons").change(function () {
                selectedFromDate = "", selectedToDate = "";
                $('#txtClinicDateFrom').datepicker('destroy');
                $('#txtClinicDateTo').datepicker('destroy');

                createDateCalendar($('option:selected', this).val());
            });

            $("#btnShowReport").click(function () {
                if (new Date($("#txtClinicDateFrom").val()) > new Date($("#txtClinicDateTo").val())) {
                    alert('"From Date" should be less than "To Date"');
                    createDateCalendar($("#ddlIPSeasons").val());
                    return false;
                }
            });

            $("#btnReset").click(function () {
                selectedFromDate = "", selectedToDate = "";
                createDateCalendar($("#ddlIPSeasons").val());
            });
        });

        function createDateCalendar(outreach_season) {
            minOutreachDate = new Date(minOutreachDate.getMonth() + 1 + "/" + minOutreachDate.getDate() + "/" + outreach_season);
            maxOutreachDate = new Date(minOutreachDate.getMonth() + "/" + new Date(minOutreachDate.getFullYear() + 1, minOutreachDate.getMonth(), 0).getDate() + "/" + (minOutreachDate.getFullYear() + 1));

            $("#txtClinicDateFrom").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                maxDate: maxOutreachDate
            }).datepicker("setDate", minOutreachDate);
            $("#txtClinicDateFrom").datepicker("option", "minDate", minOutreachDate);

            $("#txtClinicDateTo").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                maxDate: maxOutreachDate
            }).datepicker("setDate", maxOutreachDate);
            $("#txtClinicDateTo").datepicker("option", "minDate", minOutreachDate);

            if (selectedFromDate != "" && selectedToDate != "") {
                $("#txtClinicDateFrom").val(selectedFromDate);
                $("#txtClinicDateTo").val(selectedToDate);
            }
            else {
                $("#txtClinicDateFrom").datepicker({ defaultDate: minOutreachDate });
                $("#txtClinicDateTo").datepicker({ defaultDate: maxOutreachDate });
            }

            $(".ui-datepicker-trigger").css("margin-bottom", "-6px");
            $(".ui-datepicker-trigger").css("padding-left", "5px");
            $("div[id^=VisibleReportContentvaccinePurchasingReport]").css("height", "450px");
        }
    </script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server" defaultbutton="btnShowReport">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" Style="display: none"></asp:TextBox>
        <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
            <tr>
                <td colspan="2">
                    <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#FFFFFF">
                    <table width="935" border="0" cellspacing="22" cellpadding="0">
                        <tr>
                            <td valign="top" class="pageTitle">Vote & Vax Clinics Report</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <fieldset>
                                                <legend class="logSubTitles">Filter Report</legend>
                                                <table width="75%">
                                                    <tr>
                                                        <td colspan="1" style="width: 100px; text-align: right;"><span class="logSubTitles">Season:</span></td>
                                                        <td colspan="2" style="text-align: left;">
                                                            <asp:DropDownList CssClass="formFields" ID="ddlIPSeasons" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px; text-align: right;">&nbsp;</td>
                                                        <td colspan="3" style="text-align: left;">
                                                            <asp:CheckBox ID="chkIncludeDateRange" runat="server" Text="Filter by date range" CssClass="logSubTitles" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px; text-align: right;" class="logSubTitles">From date:</td>
                                                        <td style="width: 115px; text-align: left;" class="formFields">
                                                            <asp:TextBox ID="txtClinicDateFrom" Text="" runat="server" Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align: right; width: 45px;" class="logSubTitles">To date:</td>
                                                        <td style="width: 115px; text-align: left;">
                                                            <asp:TextBox ID="txtClinicDateTo" Text="" runat="server" Width="80px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px; text-align: right;" class="logSubTitles">Clinic Status:</td>
                                                        <td colspan="3">
                                                            <asp:DropDownList CssClass="formFields" ID="ddlClinicStatus" runat="server" Width="200px"></asp:DropDownList>
                                                            <asp:TextBox ID="txtClinicStatus" CssClass="formFields" Visible="true" runat="server" Width="198px"></asp:TextBox>&nbsp;
                                <asp:Button ID="btnShowReport" runat="server" OnClick="btnShowReport_Click" Text="Show Report" class="logSubTitles" />&nbsp;
                                <asp:Button ID="btnReset" runat="server" Text="Reset" class="logSubTitles" OnClick="btnReset_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="850px" valign="top">
                                <rsweb:ReportViewer ID="rptVoteNVaxClinicsReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3" PageCountMode="Actual"></rsweb:ReportViewer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <ucWFooter:walgreensFooter ID="walgreensFooterCtrl" runat="server" />
    </form>
    <script type="text/javascript">
        if ($.browser.webkit) {
            $("#rptVoteNVaxClinicsReport table").each(function (i, item) {
                $(item).css('display', 'inline-block');
            });
        }
    </script>
</body>
</html>
