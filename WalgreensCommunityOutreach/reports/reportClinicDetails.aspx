﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportClinicDetails.aspx.cs" Inherits="reportClinicDetails" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
        <style type="text/css">
        .ui-widget
        {
            font-size: 11px;
        }
        
        .cart-calendar-month
        {
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        function downloadCSVReport() {
            __doPostBack("typeCSV", "csv");
            theForm.__EVENTARGUMENT.value = "";
        }
        function downloadExcelReport() {
            __doPostBack("typeExcel", "excel");
            theForm.__EVENTARGUMENT.value = "";
        }
        var minOutreachDate = new Date("<%=outreachStartDate %>");
        var maxOutreachDate = new Date();
        var selectedFromDate = "<%=selectedFromDate %>";
        var selectedToDate = "<%=selectedToDate %>";

        $(document).ready(function () {
            if ($("#rptScheduledClinicsReport_ctl06_ctl04_ctl00_Menu") != null && $("#rptScheduledClinicsReport_ctl06_ctl04_ctl00_Menu") != undefined) {
                $('#rptScheduledClinicsReport_ctl06_ctl04_ctl00_Menu').find('div').first().remove();
                //$("#rptScheduledClinicsReport_ctl06_ctl04_ctl00_Menu").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadExcelReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>Excel</a></div>");
                $("#rptScheduledClinicsReport_ctl06_ctl04_ctl00_Menu").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadCSVReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>CSV (Excel)</a></div>");
            }

            if ($("#rptScheduledClinicsReport_ctl05_ctl04_ctl00_Menu") != null && $("#rptScheduledClinicsReport_ctl05_ctl04_ctl00_Menu") != undefined) {
                $('#rptScheduledClinicsReport_ctl05_ctl04_ctl00_Menu').find('div').first().remove();
                //$("#rptScheduledClinicsReport_ctl05_ctl04_ctl00_Menu").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadExcelReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>Excel</a></div>");
                $("#rptScheduledClinicsReport_ctl05_ctl04_ctl00_Menu").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadCSVReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>CSV (Excel)</a></div>");

            }

            dropdownRepleceText("ddlClinicType", "txtClinicType");
            dropdownRepleceText("ddlClinicStatus", "txtClinicStatus");

            $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
            $("#ReportFrameReportViewer1").css("height", "450px");
            $("#rptScheduledClinicsReport_ctl10").css("width", "850px");
            $("#rptScheduledClinicsReport_ctl10").css("height", "400px");
            $("#rptScheduledClinicsReport_ctl09").css("width", "850px");
            $("#rptScheduledClinicsReport_ctl09").css("height", "400px");

            $("#txtClinicDateFrom").attr("readonly", "readonly");
            $("#txtClinicDateTo").attr("readonly", "readonly");

            createDateCalendar($("#ddlIPSeasons").val());

            $("#ddlIPSeasons").change(function () {
                selectedFromDate = "", selectedToDate = "";
                $('#txtClinicDateFrom').datepicker('destroy');
                $('#txtClinicDateTo').datepicker('destroy');

                createDateCalendar($('option:selected', this).val());
            });

            $("#btnShowReport").click(function () {
                if (new Date($("#txtClinicDateFrom").val()) > new Date($("#txtClinicDateTo").val())) {
                    alert('"From Date" should be less than "To Date"');
                    createDateCalendar($("#ddlIPSeasons").val());
                    return false;
                }
            });

            $("#btnReset").click(function () {
                selectedFromDate = "", selectedToDate = "";
                createDateCalendar($("#ddlIPSeasons").val());
            });
        });

        function createDateCalendar(outreach_season) {
            minOutreachDate = new Date(minOutreachDate.getMonth() + 1 + "/" + minOutreachDate.getDate() + "/" + outreach_season);
            maxOutreachDate = new Date(minOutreachDate.getMonth() + 1 + "/" + new Date(minOutreachDate.getFullYear() + 1, minOutreachDate.getMonth() + 1, 0).getDate() + "/" + (minOutreachDate.getFullYear() + 1));

            $("#txtClinicDateFrom").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                maxDate: maxOutreachDate
            }).datepicker("setDate", minOutreachDate);
            $("#txtClinicDateFrom").datepicker("option", "minDate", minOutreachDate);

            $("#txtClinicDateTo").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                maxDate: maxOutreachDate
            }).datepicker("setDate", maxOutreachDate);
            $("#txtClinicDateTo").datepicker("option", "minDate", minOutreachDate);

            if (selectedFromDate != "" && selectedToDate != "") {
                //$("#txtClinicDateFrom").datepicker({ defaultDate: new Date(selectedFromDate) });
                //$("#txtClinicDateTo").datepicker({ defaultDate: new Date(selectedToDate) });
                $("#txtClinicDateFrom").val(selectedFromDate);
                $("#txtClinicDateTo").val(selectedToDate);
            }
            else {
                $("#txtClinicDateFrom").datepicker({ defaultDate: minOutreachDate });
                $("#txtClinicDateTo").datepicker({ defaultDate: maxOutreachDate });
            }

            $(".ui-datepicker-trigger").css("margin-bottom", "-6px");
            $(".ui-datepicker-trigger").css("padding-left", "5px");
            $("div[id^=VisibleReportContentvaccinePurchasingReport]").css("height", "450px");
        }
    </script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnShowReport">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>    
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
      <table width="935" border="0" cellspacing="22" cellpadding="0">
        <tr>
            <td valign="top" class="pageTitle">Scheduled Clinics Report</td>
        </tr>
        <tr>
        <td>
            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                <tr><td></td></tr>
                <tr>
                    <td colspan="3">
                    <fieldset>
                    <legend class="logSubTitles">Filter Report</legend>
                        <table width="75%">
                        <tr>
                            <td colspan="1" style="width: 100px; text-align: right;"><span class="logSubTitles">Season:</span></td>
                            <td colspan="2" style="text-align:left;">
                                <asp:DropDownList CssClass="formFields" ID="ddlIPSeasons" runat="server" Width="100px"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px; text-align: right;">&nbsp;</td>
                            <td colspan="3" style="text-align:left;"><asp:CheckBox ID="chkIncludeDateRange" runat="server" Text="Filter by date range" CssClass="logSubTitles" /></td>
                        </tr>
                        <tr>
                            <td style="width: 100px; text-align: right;" class="logSubTitles">From date:</td>
                            <td style="width: 115px; text-align: left ;" class="formFields"><asp:TextBox ID="txtClinicDateFrom" Text="" runat="server" Width="80px"></asp:TextBox>
                                <%--<uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" />--%>
                            </td>
                            <td style="text-align: right; width: 45px;" class="logSubTitles">To date:</td>
                            <td style="width: 115px; text-align: left;"><asp:TextBox ID="txtClinicDateTo" Text="" runat="server" Width="80px"></asp:TextBox>
                                <%--<uc1:PickerAndCalendar ID="PickerAndCalendarTo" runat="server" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px; text-align: right;" class="logSubTitles">Clinic Type:</td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddlClinicType" class="formFields" runat="server" Width="200px">
                                    <asp:ListItem Value="All">All</asp:ListItem>
                                    <asp:ListItem Value="Local">Local</asp:ListItem>
                                    <asp:ListItem Value="Corporate">Corporate</asp:ListItem>
                                    <asp:ListItem Value="Charity (HHS Voucher)">Charity (HHS Voucher)</asp:ListItem>
                                    <asp:ListItem Value="Vote & Vax">Vote & Vax</asp:ListItem>
                                    <asp:ListItem Value="Community Outreach">Community Outreach</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox id="txtClinicType" CssClass="formFields" visible="true" runat="server" Width="198px" ></asp:TextBox>
                            </td>
                        </tr>   
                        <tr>
                            <td style="width: 100px; text-align: right;" class="logSubTitles">Clinic Status:</td>
                            <td colspan="3">
                                <asp:DropDownList CssClass="formFields" ID="ddlClinicStatus" runat="server" Width="200px" ></asp:DropDownList>
                                <asp:TextBox id="txtClinicStatus" CssClass="formFields" visible="true" runat="server" Width="198px" ></asp:TextBox>&nbsp;
                                <asp:Button ID="btnShowReport" runat="server" OnClick="btnShowReport_Click" Text="Show Report" class="logSubTitles"/>&nbsp;
                                <asp:Button ID="btnReset" runat="server" Text="Reset" class="logSubTitles" onclick="btnReset_Click"/>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>&nbsp;</td>
                            <td colspan="4">
                                <asp:LinkButton ID="lnkBtnDownloadReport" Font-Underline="true" Font-Names="Arial, Helvetica, sans-serif" Font-Size="12px" Font-Bold="true" runat="server" Visible="false" OnClick="lnkBtnDownloadReport_Click" ></asp:LinkButton>
                            </td>
                        </tr>--%>
                        </table>
                    </fieldset>
                    </td>
                    </tr>
                </table>
        </td>
        </tr>
        <tr>
            <td width="850px" valign="top">
                <rsweb:ReportViewer ID="rptScheduledClinicsReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3" PageCountMode="Actual"></rsweb:ReportViewer>
            </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
    <ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
 <script type="text/javascript">
     if ($.browser.webkit) {
         $("#rptScheduledClinicsReport table").each(function (i, item) {
             $(item).css('display', 'inline-block');
         });
     }
</script>    
</body>
</html>
