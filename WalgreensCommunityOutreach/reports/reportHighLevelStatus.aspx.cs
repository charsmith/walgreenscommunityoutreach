﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;

public partial class reportHighLevelStatus : Page
{
    #region --------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!this.commonAppSession.LoginUserInfoSession.IsAdmin && this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
                this.bindDropdown();

            this.reportBind();
        }
    }

    protected void btnGoUser_Click(object sender, EventArgs e)
    {
        this.reportBind();
        this.selectedFromDate = this.txtOutreachFromDate.Text;
        this.selectedToDate = this.txtOutreachToDate.Text;
    }

    protected void btnReset_Click1(object sender, EventArgs e)
    {
        this.clearFields();
    }

    protected void ddlDistrictNames_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.reportBind();
    }

    #endregion

    #region --------------- PRIVATE METHODS ---------------

    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.reportBind();
    }

    /// <summary>
    ///  Bind data to the dropdown
    /// </summary>
    private void bindDropdown()
    {
        this.ddlDistrictNames.DataTextField = "Name";
        this.ddlDistrictNames.DataValueField = "userPk";
        this.ddlDistrictNames.DataSource = this.dbOperation.getMarketDistrictLeads(this.commonAppSession.LoginUserInfoSession.UserID);
        this.ddlDistrictNames.DataBind();
        this.ddlDistrictNames.Items.Insert(0, new ListItem(" -- All Districts -- ", this.commonAppSession.LoginUserInfoSession.UserID.ToString()));
    }

    /// <summary>
    /// Bind data to the report.
    /// </summary>
    private void reportBind()
    {
        this.ddlDistrictNames.Visible = (!this.commonAppSession.LoginUserInfoSession.IsAdmin && this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager");
        this.lblDistricts.Visible = (!this.commonAppSession.LoginUserInfoSession.IsAdmin && this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager");
        this.txtDistrictNames.Visible = (!this.commonAppSession.LoginUserInfoSession.IsAdmin && this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager");
        this.rowDateFilters.Visible = (this.commonAppSession.SelectedStoreSession.OutreachProgramSelected.ToLower() != "so");
        switch (commonAppSession.LoginUserInfoSession.UserRole.ToLower())
        {
            case "regional vice president":
            case "regional healthcare director":
                this.lblHeading.Text = "Region High Level Status Report";
                //this.ddlDistrictNames.Visible = true;
                //this.lblDistricts.Visible = true;
                break;
            case "healthcare supervisor":
            case "director – rx & retail ops":
                this.lblHeading.Text = "Area High Level Status Report";
                //this.ddlDistrictNames.Visible = true;
                //this.lblDistricts.Visible = true;
                break;
            case "district manager":
                this.lblHeading.Text = "District High Level Status Report";
                break;
            default:
                this.lblHeading.Text = "High Level Status Report";
                break;
        }

        string from_date = this.txtOutreachFromDate.Text;
        string to_date = this.txtOutreachToDate.Text;

        this.highLevelStatusReport.ProcessingMode = ProcessingMode.Local;

        //if (this.ddlDistrictNames.SelectedIndex != 0)
        //    this.userId = this.ddlDistrictNames.SelectedItem.Value.ToString();
        //else
        //    this.userId = this.commonAppSession.LoginUserInfoSession.UserID.ToString();

        DataTable dt_report_data = this.dbOperation.getHighLevelContactStatusReportData(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, (this.ddlDistrictNames.Visible ? this.ddlDistrictNames.SelectedItem.Value.ToString() : this.commonAppSession.LoginUserInfoSession.UserID.ToString()), from_date.Trim().Length == 0 ? this.outreachStartDate : from_date, to_date.Trim().Length == 0 ? this.outreachEndDate : to_date);
        
        if (dt_report_data.Rows.Count > 0)
        {
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
            rds.Value = dt_report_data;

            this.highLevelStatusReport.LocalReport.DataSources.Clear();
            this.highLevelStatusReport.LocalReport.DataSources.Add(rds);
            this.highLevelStatusReport.ShowPrintButton = false;
            if (dt_report_data.Rows.Count > 0)
                this.highLevelStatusReport.LocalReport.ReportPath = Server.MapPath("highLevelStatus.rdlc");
        }
    }
    #endregion

    #region --------------- PRIVATE VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    //private string userId = string.Empty;
    public string outreachStartDate = string.Empty;
    public string outreachEndDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
        this.outreachEndDate = ApplicationSettings.getOutreachEndDate.AddMonths(1).AddDays(1).ToString("MM/dd/yyyy");
    }
    #endregion
}
