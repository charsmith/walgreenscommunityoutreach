﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;
using System.Web.Services;
using System.Globalization;
using NLog;

public partial class reportGroupIDAssignments : Page
{
    #region ------------- PROTECTED EVENTS -------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int report_year = DateTime.Today.Year; report_year >= Convert.ToDateTime(this.groupIdLaunchDate).Year; report_year--)
            {
                this.ddlIPSeasons.Items.Add(new ListItem(report_year.ToString() + " - " + (report_year + 1).ToString(), report_year.ToString()));
            }

            if (this.ddlIPSeasons.Items.Count > 0)
                this.ddlIPSeasons.SelectedValue = (Convert.ToDateTime(this.outreachStartDate).Year).ToString();

            this.reportBind(this.outreachStartDate, Convert.ToDateTime(this.outreachStartDate).AddMonths(12).AddDays(-1).ToString("MM/dd/yyyy"));
        }
    }

    /// <summary>
    /// Displayes report data for selected filter conditions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        this.reportBind(this.txtDateFrom.Text, this.txtDateTo.Text);
        this.selectedFromDate = this.txtDateFrom.Text;
        this.selectedToDate = this.txtDateTo.Text;
    }

    /// <summary>
    /// Removes filters and resets report data to default
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.reportBind(this.txtDateFrom.Text, this.txtDateTo.Text);
    }

    #endregion

    #region ------------- PRIVATE METHODS --------------
    /// <summary>
    /// Bind data to the report.
    /// </summary>
    private void reportBind(string from_date, string to_date)
    {
        this.logger.Info("Binding Group ID Assignment Report by user {0} - START", this.commonAppSession.LoginUserInfoSession.UserName);
        
        DataTable data_tbl;
        
        this.groupIDAssignmentsReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getWeeklyGroupIDAssignmentsReport(Convert.ToDateTime(from_date), Convert.ToDateTime(to_date).AddDays(1).AddSeconds(-1));
        
        this.logger.Info("Results were fetched from {0} method. Binding the report now..", "getWeeklyGroupIDAssignmentsReport");
        //Change sent to ParsbugAR Date to Current Date
        DataColumn dc = new DataColumn("sentToParsbugARDate");
        dc.DataType = typeof(DateTime);
        dc.DefaultValue = DateTime.Today;

        data_tbl.Columns.Remove("sentToParsbugARDate");
        data_tbl.Columns.Add(dc);

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "grpIDAssignmentReport";
        rds.Value = data_tbl;

        this.groupIDAssignmentsReport.LocalReport.DataSources.Clear();
        this.groupIDAssignmentsReport.LocalReport.DataSources.Add(rds);
        this.groupIDAssignmentsReport.ShowPrintButton = false;
        this.groupIDAssignmentsReport.LocalReport.ReportPath = Server.MapPath("groupIDAssignmentsReport.rdlc");
        data_tbl = null;
        this.logger.Info("Binding Group ID Assignment Report by user {0} - END", this.commonAppSession.LoginUserInfoSession.UserName);
    }
    #endregion

    #region ------------- PRIVATE & PUBLIC VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    public string groupIdLaunchDate = string.Empty;
    public string outreachStartDate = string.Empty;
    public string outreachEndDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.groupIdLaunchDate = Convert.ToDateTime("05/01/2016").ToString("MM/dd/yyyy");
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
        this.outreachEndDate = ApplicationSettings.getOutreachEndDate.AddMonths(1).AddDays(1).ToString("MM/dd/yyyy");
    }
    #endregion
}
