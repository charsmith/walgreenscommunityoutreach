﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;
using System.Web.Services;
using System.Globalization;
using NLog;

public partial class reportVaccinePurchasingDetails : Page
{
    #region ------------- PROTECTED EVENTS -------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int report_year = Convert.ToDateTime(this.outreachStartDate).Year; report_year >= this.appSettings.getOutreachReportStartYear; report_year--)
            {
                this.ddlIPSeasons.Items.Add(new ListItem(report_year.ToString() + " - " + (report_year + 1).ToString(), report_year.ToString()));
            }

            if (this.ddlIPSeasons.Items.Count > 0)
                this.ddlIPSeasons.SelectedValue = (Convert.ToDateTime(this.outreachStartDate).Year).ToString();

            this.reportBind(this.outreachStartDate, Convert.ToDateTime(this.outreachStartDate).AddMonths(12).ToString("MM/dd/yyyy"));
        }
    }

    /// <summary>
    /// Displayes report data for selected filter conditions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        this.reportBind(this.txtScheduledOnDateFrom.Text, this.txtScheduledOnDateTo.Text);
        this.selectedFromDate = this.txtScheduledOnDateFrom.Text;
        this.selectedToDate = this.txtScheduledOnDateTo.Text;
    }

    /// <summary>
    /// Removes filters and resets report data to default
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.reportBind(this.txtScheduledOnDateFrom.Text, this.txtScheduledOnDateTo.Text);
    }

    #endregion

    #region ------------- PRIVATE METHODS --------------
    /// <summary>
    /// Bind data to the report.
    /// </summary>
    private void reportBind(string from_date, string to_date)
    {
        this.logger.Info("Binding {0} Report by user {1} - START", "Vaccine Purchasing Report", this.commonAppSession.LoginUserInfoSession.UserName);
        DataTable data_tbl;
        this.vaccinePurchasingReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getVaccinePurchasingDetailsReport(Convert.ToDateTime(from_date), Convert.ToDateTime(to_date), (Convert.ToInt32(this.ddlIPSeasons.SelectedValue) != Convert.ToInt32(Convert.ToDateTime(this.outreachStartDate).Year) ? 0 : 1));
        this.logger.Info("Results were fetched from {0} method. Binding the report now..", "getVaccinePurchasingDetails");
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "vaccinePurchasingDataSet";
        rds.Value = data_tbl;

        this.vaccinePurchasingReport.LocalReport.DataSources.Clear();
        this.vaccinePurchasingReport.LocalReport.DataSources.Add(rds);
        this.vaccinePurchasingReport.ShowPrintButton = false;
        this.vaccinePurchasingReport.LocalReport.ReportPath = Server.MapPath("vaccinePurchasingReport.rdlc");
        data_tbl = null;
        this.logger.Info("Binding {0} Report by user {1} - END", "Vaccine Purchasing Report", this.commonAppSession.LoginUserInfoSession.UserName);
    }
    #endregion

    #region ------------- PRIVATE & PUBLIC VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    public string outreachStartDate = string.Empty;
    public string outreachEndDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
        this.outreachEndDate = ApplicationSettings.getOutreachEndDate.AddMonths(1).AddDays(1).ToString("MM/dd/yyyy");
    }
    #endregion
}
