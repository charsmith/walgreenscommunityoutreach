﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using System.Web.UI.WebControls;

public partial class reportActionItemsIP : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            this.reportBind();
        if (Page.IsPostBack)
        {
            string event_args = Request["__EVENTARGUMENT"];
            if (!string.IsNullOrEmpty(event_args) && event_args.ToLower() == "csv")
                this.downloadCSVReport();

            if (!string.IsNullOrEmpty(event_args) && event_args.ToLower() == "excel")
                this.downloadExcelReport();
        }
    }

    protected void lnkRptActionItems_Command(object sender, CommandEventArgs e)
    {
        this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId = Convert.ToInt32(e.CommandArgument);
        this.reportBind();
    }
    #endregion

    #region --------------- PRIVATE FUNCTIONS --------------
    /// <summary>
    /// Creates action items report links
    /// </summary>
    private void createActionItemsReportsLinks()
    {
        this.pnlActionItemRptLinks.Controls.Clear();
        DataTable dt_action_items = this.commonAppSession.SelectedStoreSession.ActionItemsCounts;
        foreach (DataRow dr_row in dt_action_items.Rows)
        {
            LinkButton lnk_action_item_rpt = new LinkButton();
            lnk_action_item_rpt.Text = dr_row["actionItem"].ToString();
            lnk_action_item_rpt.ID = "lnkRptActionItem" + dr_row["actionItemId"].ToString();
            lnk_action_item_rpt.CommandArgument = dr_row["actionItemId"].ToString();
            lnk_action_item_rpt.Command += new CommandEventHandler(lnkRptActionItems_Command);
            lnk_action_item_rpt.Enabled = true;

            this.pnlActionItemRptLinks.Controls.Add(lnk_action_item_rpt);
            if ((pnlActionItemRptLinks.Controls.Count + 1) / 2 <= dt_action_items.Rows.Count - 1)
                this.pnlActionItemRptLinks.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;"));
        }
    }

    /// <summary>
    /// Bind data to the report
    /// </summary>
    private void reportBind()
    {
        this.lblReportTitle.InnerHtml = ((LinkButton)pnlActionItemRptLinks.FindControl("lnkRptActionItem" + this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId.ToString())).Text;
        ((LinkButton)pnlActionItemRptLinks.FindControl("lnkRptActionItem" + this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId.ToString())).Enabled = false;
        rptActionItemsReport.Width = new Unit(835);
        DataTable dt_action_items = new DBOperations().getActionItemsReport(this.commonAppSession.SelectedStoreSession.SelectedHCSUserId, this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId);
        this.rptActionItemsReport.ProcessingMode = ProcessingMode.Local;
        this.rptActionItemsReport.LocalReport.ReportPath = Server.MapPath("actionItemsReport.rdlc");
        this.rptActionItemsReport.LocalReport.DataSources.Clear();
        ReportDataSource rds = new ReportDataSource("tdWalgreensDataSet_ActionItemsReport", dt_action_items);
        this.rptActionItemsReport.LocalReport.DataSources.Add(rds);

        ReportParameter parameter = new ReportParameter("reportId", this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId.ToString(), false);
        this.rptActionItemsReport.ShowPrintButton = false;
        this.rptActionItemsReport.LocalReport.SetParameters(new ReportParameter[] { parameter });

        dt_action_items = null;
    }

    /// <summary>
    /// This method will be use to download Excel report
    /// </summary>
    private void downloadExcelReport()
    {
        byte[] bytes = rptActionItemsReport.LocalReport.Render("Excel");
        this.downloadReport(bytes, "xls");
    }

    /// <summary>
    /// This method will use to download CSV format report
    /// </summary>
    protected void downloadCSVReport()
    {
        using (var dt_action_items = new DBOperations().getActionItemsReport(this.commonAppSession.SelectedStoreSession.SelectedHCSUserId, this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId))
        {
            DataTable dt_report = null;
            if (this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId == 2 || this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId == 3)
                dt_report = dt_action_items.DefaultView.ToTable(false, "storeid", "districtId", "areaId", "regionId", "businessName", "empSize", "contactName", "phone");
            else
                dt_report = dt_action_items.DefaultView.ToTable(false, "storeid", "districtId", "businessName", "ClinicLocation", "contactName", "ClinicDate", "startTime", "endTime");
            // dt_report = dt_action_items.DefaultView.ToTable();
            var export = new ExportToCSV();

            foreach (DataRow row in dt_report.Rows)
            {
                export.AddRow();
                foreach (DataColumn column in dt_report.Columns)
                {
                    export[export.GetColumnName(column.ColumnName)] = row[column].ToString();
                }
            }

            byte[] csv_data = export.ExportToBytes();
            export = null;
            this.downloadReport(csv_data, "csv");
        }
    }

    /// <summary>
    /// This common function for download report
    /// </summary>
    /// <param name="data"></param>
    /// <param name="format"></param>
    private void downloadReport(byte[] data, string format)
    {
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Disposition", "attachment; filename=actionItems." + format);
        Response.AddHeader("Content-Transfer-Encoding", "BINARY");
        Response.AddHeader("Content-Length", data.Length.ToString());
        Response.AddHeader("Connection", "Keep-Alive");
        Response.AddHeader("Cache-Control", "max-age=1");

        Response.BinaryWrite(data);
        data = null;
        GC.Collect();
        Response.End();
    }

    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    private AppCommonSession commonAppSession = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.createActionItemsReportsLinks();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.reportBind();
    }
    #endregion
}