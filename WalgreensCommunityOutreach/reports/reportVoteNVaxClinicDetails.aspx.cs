﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using System.IO;

public partial class reports_reportVoteNVaxClinicDetails : System.Web.UI.Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int report_year = 2016; report_year <= Convert.ToDateTime(this.outreachStartDate).Year; report_year++)
            {
                this.ddlIPSeasons.Items.Add(new ListItem(report_year.ToString() + " - " + (report_year + 1).ToString(), report_year.ToString()));
            }

            if (this.ddlIPSeasons.Items.Count > 0)
                this.ddlIPSeasons.SelectedValue = (Convert.ToDateTime(this.outreachStartDate).Year).ToString();

            this.txtClinicDateFrom.Text = this.outreachStartDate;
            this.txtClinicDateTo.Text = Convert.ToDateTime(this.outreachStartDate).AddYears(1).ToString("MM/dd/yyyy");
            this.bindDropdown();
            this.reportBind();
        }
    }

    /// <summary>
    /// Get report data based on selected filters
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        this.reportBind();
        this.selectedFromDate = this.txtClinicDateFrom.Text;
        this.selectedToDate = this.txtClinicDateTo.Text;
    }

    /// <summary>
    /// Reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.clearFields();
    }

    #endregion

    #region --------------- PRIVATE METHODS ----------------
    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.ddlClinicStatus.ClearSelection();
        this.reportBind();
    }

    /// <summary>
    /// Binds stores to dropdown
    /// </summary>
    private void bindDropdown()
    {
        DataTable data_tbl = new DataTable();
        DataRow[] dr = this.dbOperation.getOutreachStatus.Select("category='AC'");
        data_tbl = dr.CopyToDataTable();

        if (data_tbl.Rows.Count > 0)
        {
            this.ddlClinicStatus.DataSource = data_tbl;
            this.ddlClinicStatus.DataTextField = "outreachStatus";
            this.ddlClinicStatus.DataValueField = "pk";
            this.ddlClinicStatus.DataBind();
            this.ddlClinicStatus.Items.Insert(0, new ListItem("All", "-1"));
        }
        data_tbl = null;
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable dt_vv_report = new DataTable();
        this.rptVoteNVaxClinicsReport.ProcessingMode = ProcessingMode.Local;

        dt_vv_report = this.dbOperation.getVoteVaxClinicDetailsReport(Convert.ToInt32(this.commonAppSession.LoginUserInfoSession.UserID),
                       Convert.ToInt32(ddlClinicStatus.SelectedItem.Value), Convert.ToDateTime(this.txtClinicDateFrom.Text), Convert.ToDateTime(this.txtClinicDateTo.Text),
                       this.chkIncludeDateRange.Checked.ToString());

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblVoteNVaxClinicDetails";
        rds.Value = dt_vv_report;

        this.rptVoteNVaxClinicsReport.LocalReport.DataSources.Clear();
        this.rptVoteNVaxClinicsReport.LocalReport.DataSources.Add(rds);
        this.rptVoteNVaxClinicsReport.ShowPrintButton = false;
        this.rptVoteNVaxClinicsReport.LocalReport.ReportPath = Server.MapPath("voteNVaxClinicDetails.rdlc");

        dt_vv_report = null;
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    public string outreachStartDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
    }
    #endregion
}