﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdWalgreens;
using System.IO;
using TdApplicationLib;

public partial class reports_displayScheduledApptsReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["pk"] != null && !string.IsNullOrEmpty(Request.QueryString["pk"].ToString()))
            {
                int clinic_pk = 0;
                clinic_pk = Convert.ToInt32(Request.QueryString["pk"].ToString());
                DataTable dt_scheduled_appts;
                Warning[] warnings;
                string[] stream_ids;
                string mime_type;
                string encoding;
                string extension;
                string report_name = "ClinicScheduledAppointments";

                ReportViewer report_viewer = new ReportViewer();
                report_viewer.ProcessingMode = ProcessingMode.Local;
                report_viewer.LocalReport.ReportPath = Server.MapPath("scheduledAppointment.rdlc");
                DBOperations db_pperations = new DBOperations();
                dt_scheduled_appts = db_pperations.getCorporateClientScheduledApptsReport("User", null, 0, clinic_pk, 1);

                ReportDataSource rds = new ReportDataSource();
                rds.Name = "tdWalgreensDataSet_tblClinicSchedulerEeRegistry";
                rds.Value = dt_scheduled_appts;
                report_viewer.LocalReport.DataSources.Clear();
                report_viewer.LocalReport.DataSources.Add(rds);
                byte[] bytes = report_viewer.LocalReport.Render("PDF", null, out mime_type, out encoding, out extension, out stream_ids, out warnings);
                //report_filename = "ClinicScheduledAppointments_" + this.commonAppSession.LoginUserInfoSession.UserID.ToString() + "_" + this.clinicPk.ToString() + "_" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss").Replace("/", "").Replace(" ", "_").Replace(":", "") + "." + extension;
                //using (var fs = new FileStream(ApplicationSettings.clinicApptsReportPdfPath() + "/" + report_filename, FileMode.Create))
                //{
                //    fs.Write(bytes, 0, bytes.Length);
                //    fs.Close();
                //}

                Response.Clear();
                Response.ContentType = mime_type;
                Response.AddHeader("Content-Disposition", "inline; filename=" + report_name + "." + extension);
                Response.BinaryWrite(bytes);
                Response.End();
            }
        }
    }
}