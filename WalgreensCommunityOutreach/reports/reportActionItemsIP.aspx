﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportActionItemsIP.aspx.cs" Inherits="reportActionItemsIP" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />    
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>
     <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />
    <script type="text/javascript">
        function downloadCSVReport() {
            __doPostBack("typeCSV", "csv");
            theForm.__EVENTARGUMENT.value = "";
        }
        function downloadExcelReport() {
            __doPostBack("typeExcel", "excel");
            theForm.__EVENTARGUMENT.value = "";
        }
        $(document).ready(function () {
            $("#rptActionItemsReport_ctl10").css("height", "415px");
            $("#rptActionItemsReport_ctl09").css("height", "415px");
            $("#rptActionItemsReport_fixedTable")[0].style.display=null;
            if ($("[id^='rptActionItemsReport'][id$='_Menu']") != null && $("[id^='rptActionItemsReport'][id$='_Menu']") != undefined) {
                $("[id^='rptActionItemsReport'][id$='_Menu']").find('div').first().remove();
                $("[id^='rptActionItemsReport'][id$='_Menu']").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadExcelReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>Excel</a></div>");
                $("[id^='rptActionItemsReport'][id$='_Menu']").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadCSVReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>CSV</a></div>");                
            }
        });
    </script>
    <style type="text/css">
        a
        {
            color: #57a1d3;
        }
        a[disabled=disabled]
        {
            color: black;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">   
    <table width="935" border="0" cellspacing="15" cellpadding="0">
        <tr>
            <td valign="top" class="pageTitle">
                <span id="lblReportTitle" runat="server"></span>
            </td>
        </tr>
        <tr>
            <td valign="top" class="wagsAddress">
                <asp:Panel runat="server" ID="pnlActionItemRptLinks" ViewStateMode="Disabled"></asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="Width:601; padding-left:35px; padding-right:35px; padding-bottom:45px;" valign="top">
                <rsweb:ReportViewer ID="rptActionItemsReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="765px" Height="450px" TabIndex="3" PageCountMode="Actual" AsyncRendering="false"></rsweb:ReportViewer>
            </td>
        </tr>
    </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
<script type="text/javascript">
    if ($.browser.webkit) {
        $("#rptActionItemsReport table").each(function (i, item) {
            $(item).css('display', 'inline-block');
        });
    }
</script>    
</body>
</html>