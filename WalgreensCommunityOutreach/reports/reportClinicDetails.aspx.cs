﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using System.IO;
using NLog;

public partial class reportClinicDetails : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int report_year = this.appSettings.getOutreachReportStartYear; report_year <= Convert.ToDateTime(this.outreachStartDate).Year; report_year++)
            {
                this.ddlIPSeasons.Items.Add(new ListItem(report_year.ToString() + " - " + (report_year + 1).ToString(), report_year.ToString()));
            }

            if (this.ddlIPSeasons.Items.Count > 0)
                this.ddlIPSeasons.SelectedValue = (Convert.ToDateTime(this.outreachStartDate).Year).ToString();

            this.txtClinicDateFrom.Text = this.outreachStartDate;
            this.txtClinicDateTo.Text = Convert.ToDateTime(this.outreachStartDate).AddYears(1).ToString("MM/dd/yyyy");
            this.bindDropdown();

            //this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            //this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            this.reportBind();
        }

        if (Page.IsPostBack)
        {
            string event_args = Request["__EVENTARGUMENT"];
            if (!string.IsNullOrEmpty(event_args) && event_args.ToLower() == "csv")
                this.downloadCSVReport();

            if (!string.IsNullOrEmpty(event_args) && event_args.ToLower() == "excel")
                this.downloadExcelReport();
        }
    }

    /// <summary>
    /// Get report data based on selected filters
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        this.reportBind();
        this.selectedFromDate = this.txtClinicDateFrom.Text;
        this.selectedToDate = this.txtClinicDateTo.Text;
    }

    /// <summary>
    /// Reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.clearFields();
    }

    //protected void lnkBtnDownloadReport_Click(object sender, EventArgs e)
    //{
    //    FileInfo report_file = new FileInfo(Path.Combine(ApplicationSettings.reportsPath, "confirmClinicDetails.xls"));
    //    if (report_file.Exists)
    //    {
    //        Response.Clear();
    //        Response.ClearHeaders();
    //        Response.ClearContent();
    //        Response.AddHeader("content-disposition", "attachment; filename=confirmClinicDetails.xls");
    //        Response.AddHeader("Content-Type", "application/Excel");
    //        Response.ContentType = "application/vnd.xls";
    //        Response.AddHeader("Content-Length", report_file.Length.ToString());
    //        Response.WriteFile(report_file.FullName);
    //        Response.End();
    //    }
    //}
    #endregion

    #region --------------- PRIVATE METHODS ----------------
    /// <summary>
    /// This method will be use to download Excel report
    /// </summary>
    private void downloadExcelReport()
    {
        byte[] bytes = rptScheduledClinicsReport.LocalReport.Render("Excel");
        this.downloadReport(bytes, "xls");
    }

    /// <summary>
    /// This method will use to download CSV format report
    /// </summary>
    protected void downloadCSVReport()
    {
        using (var dt_sc_report = this.dbOperation.getConfirmClinicReport(Convert.ToInt32(this.commonAppSession.LoginUserInfoSession.UserID), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId,
                Convert.ToInt32(ddlClinicStatus.SelectedItem.Value), Convert.ToDateTime(this.txtClinicDateFrom.Text), Convert.ToDateTime(this.txtClinicDateTo.Text), this.ddlClinicType.SelectedItem.Value, this.chkIncludeDateRange.Checked.ToString(), Convert.ToInt32(this.ddlIPSeasons.SelectedValue), (Convert.ToInt32(this.ddlIPSeasons.SelectedValue) == Convert.ToDateTime(this.outreachStartDate).Year ? false : true)))
        {
            DataTable dt_report = null;

            dt_report = dt_sc_report.DefaultView.ToTable(false, "storeid", "marketNumber", "areaNumber", "districtNumber", "BussinessCorporateClient", "BussinessCorporateClientLocation", "ClientLocationState", "ClinicType", "OutreachType",
                                                                    "Industry", "Comments", "LocalContractLocation", "ClinicLocation", "ClinicState", "LocationContactName", "LocationContactPhone", "LocationContactEmail", "ClinicDate",
                                                                    "ClinicMoYr", "StartTime", "EndTime", "EstVol", "naClinicVouchersDist", "ExpirationDate", "actualCorporateEmploymentSize", "WorksiteNeeded", "VaccinesCoveredGlobal",
                                                                    "ClinicCoverageType", "ClinicCopay", "PaymentTypes", "InvoiceName", "InvoiceAddress", "InvoicePhone", "InvoiceEmail", "EmployerTaxExempt", "VoucherNeeded", "AdditionalComments",
                                                                    "SpecialBillingComments", "PlanID", "GroupID", "IDRecipient", "TotalImmunizationsAdministered", "ScheduledOn", "LastContactDate", "LastNotes", "LastOutreachStatus");

            var export = new ExportToCSV();
            
                foreach (DataRow row in dt_report.Rows)
                {
                    export.AddRow();
                    foreach (DataColumn column in dt_report.Columns)
                    {
                        export[export.GetColumnName(column.ColumnName)] = row[column].ToString();
                    }
                }

                byte[] csv_data = export.ExportToBytes();
                export = null;
                this.downloadReport(csv_data, "csv");
           
        }
    }

    /// <summary>
    /// This common function for download report
    /// </summary>
    /// <param name="data"></param>
    /// <param name="format"></param>
    private void downloadReport(byte[] data, string format)
    {
        this.logger.Info("Downloading Scheduled Clinics Report by user {0} with format {1} - START", this.commonAppSession.LoginUserInfoSession.UserName, format);
        
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Disposition", "attachment; filename=confirmClinicDetails." + format);
        Response.AddHeader("Content-Transfer-Encoding", "BINARY");
        Response.AddHeader("Content-Length", data.Length.ToString());
        Response.AddHeader("Connection", "Keep-Alive");
        Response.AddHeader("Cache-Control", "max-age=1");

        Response.BinaryWrite(data);
        this.logger.Info("Downloading Scheduled Clinics Report by user {0} with format {1} - END", this.commonAppSession.LoginUserInfoSession.UserName, format);
        data = null;
        GC.Collect();      
        Response.End();
        
    }

    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.ddlClinicType.ClearSelection();
        this.ddlClinicStatus.ClearSelection();
        this.reportBind();
    }

    /// <summary>
    /// Binds stores to dropdown
    /// </summary>
    private void bindDropdown()
    {
        DataTable data_tbl = new DataTable();
        DataRow[] dr = this.dbOperation.getOutreachStatus.Select("category='AC'");
        data_tbl = dr.CopyToDataTable();

        if (data_tbl.Rows.Count > 0)
        {
            this.ddlClinicStatus.DataSource = data_tbl;
            this.ddlClinicStatus.DataTextField = "outreachStatus";
            this.ddlClinicStatus.DataValueField = "pk";
            this.ddlClinicStatus.DataBind();
            this.ddlClinicStatus.Items.Insert(0, new ListItem("All", "-1"));
        }
        data_tbl = null;
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        this.logger.Info("Binding Scheduled Clinics Report by user {0} - START", this.commonAppSession.LoginUserInfoSession.UserName);
        
        DataTable dt_sc_report = new DataTable();
        this.rptScheduledClinicsReport.ProcessingMode = ProcessingMode.Local;
        dt_sc_report = this.dbOperation.getConfirmClinicReport(Convert.ToInt32(this.commonAppSession.LoginUserInfoSession.UserID), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId,
                       Convert.ToInt32(ddlClinicStatus.SelectedItem.Value), Convert.ToDateTime(this.txtClinicDateFrom.Text), Convert.ToDateTime(this.txtClinicDateTo.Text), this.ddlClinicType.SelectedItem.Value, 
                       this.chkIncludeDateRange.Checked.ToString(), Convert.ToInt32(this.ddlIPSeasons.SelectedValue), (Convert.ToInt32(this.ddlIPSeasons.SelectedValue) == Convert.ToDateTime(this.outreachStartDate).Year ? false : true));
        this.logger.Info("Results were fetched from {0} method. Binding the report now..", "getConfirmClinicReport");
        
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = dt_sc_report;

        this.rptScheduledClinicsReport.LocalReport.DataSources.Clear();
        this.rptScheduledClinicsReport.LocalReport.DataSources.Add(rds);
        this.rptScheduledClinicsReport.ShowPrintButton = false;
        this.rptScheduledClinicsReport.LocalReport.ReportPath = Server.MapPath("confirmClinicDetails.rdlc");

        //if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        //{
        //    if (File.Exists(Path.Combine(ApplicationSettings.reportsPath, "confirmClinicDetails.xls")))
        //    {
        //        DateTime report_generated_date = (new FileInfo(Path.Combine(ApplicationSettings.reportsPath, "confirmClinicDetails.xls"))).LastWriteTime;

        //        this.lnkBtnDownloadReport.Text = "Download Daily Scheduled Clinic Report (Generated at " + report_generated_date.ToString("MM/dd/yyyy hh:mm tt") + " CST)";
        //        this.lnkBtnDownloadReport.Visible = true;
        //    }
        //}

        dt_sc_report = null;
        this.logger.Info("Binding Scheduled Clinics Report by user {0} - END", this.commonAppSession.LoginUserInfoSession.UserName);
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    public string outreachStartDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
    }
    #endregion
    
}
