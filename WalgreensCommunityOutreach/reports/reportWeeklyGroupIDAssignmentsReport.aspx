﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportWeeklyGroupIDAssignmentsReport.aspx.cs" Inherits="reportWeeklyGroupIDAssignmentsReport" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
 <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td valign="top" class="pageTitle">
                            Weekly Group ID Assignments Report
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <table style="text-align: left">
                                            <tr>
                                                <td style="width: 70px; padding: 5px; text-align: right; vertical-align: bottom;"
                                                    class="formFields">
                                                    Season:
                                                </td>
                                                <td align="left" colspan="3">
                                                    <asp:DropDownList CssClass="formFields" ID="ddlIPSeasons" runat="server" Width="100px" AutoPostBack="true" onselectedindexchanged="ddlIPSeasons_SelectedIndexChanged"></asp:DropDownList>
                                                </td>
                                            </tr>                                            
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="601" valign="top" style="padding-left: 45px; padding-right: 45px; padding-bottom: 45px;">
                            <asp:DataList ID="dlGroupIDAssignmentReport" runat="server" RepeatColumns="3" RepeatDirection="Vertical" BorderStyle="None" CellPadding="10"  CellSpacing="0" ItemStyle-CssClass="formFields" DataKeyField="Key" >
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkGroupIDAssignmentReport" runat="server" Text='<%# Eval("Value") %>' OnClick="lnkGroupIDAssignmentReport_Click"></asp:LinkButton>                                    
                                </ItemTemplate>
                            </asp:DataList>   
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
