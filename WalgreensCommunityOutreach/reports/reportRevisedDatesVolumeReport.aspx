﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportRevisedDatesVolumeReport.aspx.cs" Inherits="reports_reportVaccineExceptionsReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            createDateCalendar($("#ddlIPSeasons").val());

            $("#ddlIPSeasons").change(function () {
                createDateCalendar($('option:selected', this).val());
            });

            $("#btnShowReport").click(function () {
                    createDateCalendar($("#ddlIPSeasons").val());
            });

            $("#btnReset").click(function () {
                
                createDateCalendar($("#ddlIPSeasons").val());
            });
        });

        function createDateCalendar(outreach_season) {
            $("div[id^=VisibleReportContentvaccinePurchasingReport]").css("height", "450px");
            $("div[id^=VisibleReportContentvaccinePurchasingReport]").css("width", "850px");
        }
    </script>
    <style type="text/css">
        .ui-widget
        {
            font-size: 11px;
        }
        
        .cart-calendar-month
        {
            font-size: 11px;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td valign="top" class="pageTitle">
                            Revised Clinic Dates & Volumes Report
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <table style="text-align: left">
                                            <tr>
                                                <td style="width: 70px; padding: 5px; text-align: right; vertical-align: bottom;"
                                                    class="formFields">
                                                    Season:
                                                </td>
                                                <td align="left" colspan="3">
                                                    <asp:DropDownList CssClass="formFields" ID="ddlIPSeasons" runat="server" Width="100px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td colspan="3" style="padding-top: 15px;" class="formFields">
                                                    <asp:Button ID="btnShowReport" runat="server" OnClick="btnShowReport_Click" Text="Show Report"
                                                        class="logSubTitles" />&nbsp;&nbsp;
                                                    <asp:Button ID="btnReset" runat="server" Text="Reset" class="logSubTitles" OnClick="btnReset_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="601" valign="top" style="padding-left: 45px; padding-right: 45px; padding-bottom: 45px;">
                            <rsweb:ReportViewer ID="vaccinePurchasingReport" runat="server" Font-Names="Verdana"
                                Font-Size="8pt" Width="850px" Height="450px" AsyncRendering="False" BorderStyle="None"
                                PageCountMode="Actual">
                                <LocalReport ReportPath="vaccinePurchasingReport.rdlc">
                                </LocalReport>
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
    <script type="text/javascript">
        if ($.browser.webkit) {
            $("#vaccinePurchasingReport table").each(function (i, item) {
                $(item).css('display', 'inline-block');
            });
        }
</script>
</body>
</html>

