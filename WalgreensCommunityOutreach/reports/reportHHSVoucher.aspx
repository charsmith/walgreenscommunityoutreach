﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportHHSVoucher.aspx.cs" Inherits="reportClinicDetails" %>
<%@ Register src="../controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <style type="text/css">
       .cart-calendar-month
        {
            font-size: 11px;
        }
        </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />    
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />     
     <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <link href="../themes/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            dropdownRepleceText("ddlClinicType", "txtClinicType");
            dropdownRepleceText("ddlClinicStatus", "txtClinicStatus");
            dropdownRepleceText("ddlPayment", "txtPayment");

            $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
            $("#ReportFrameReportViewer1").css("height", "450px");

        });
    </script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnGoUser">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>    
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    
    <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td valign="top" class="pageTitle">Charity Program Report</td>
                    </tr>
                    <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="5" cellpadding="0">
                            <tr><td>
                            </td></tr>                      
                                <tr>
                                <td colspan="3">
                                <fieldset>
                                <legend class="logSubTitles">Filter Report</legend>
                                    <table>
                                    <tr>
                                        <td style="width: 100px; text-align: right;">&nbsp;</td>
                                        <td colspan="3" style="text-align:left;"><asp:CheckBox ID="chkIncludeDateRange" runat="server" Text="Filter by date range" CssClass="logSubTitles" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: right;">
                                          <span class="logSubTitles">From date:</span>
                                        </td>
                                        <td style="width: 75px;">
                                          <uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" />                                      
                                        </td>
                                        <td style="text-align: right;">
                                          <span class="logSubTitles">To date:</span>
                                        </td>
                                        <td style="width: 75px;">
                                          <uc1:PickerAndCalendar ID="PickerAndCalendarTo" runat="server" />
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: right;">
                                            <span class="logSubTitles">Clinic Status:</span></td>
                                        <td colspan="3">
                                            <asp:DropDownList CssClass="formFields" ID="ddlClinicStatus" runat="server" Width="200px" ></asp:DropDownList>
                                            <asp:TextBox id="txtClinicStatus" CssClass="formFields" visible="true" runat="server" Width="198px" ></asp:TextBox>
                                            <span id="tblStores0" runat="server">&nbsp;
                                            <asp:Button ID="btnGoUser" runat="server" OnClick="btnGoUser_Click" Text="Show Report" class="logSubTitles"/>
                                    &nbsp;<asp:Button ID="btnReset" runat="server" 
                                                Text="Reset" class="logSubTitles" onclick="btnReset_Click"/>
                                    </span>       
                                        </td>
                                    </tr>    
                                    </table>                         
                                </fieldset>  
                                </td>
                                </tr>          
                                                                                                                                      
                            </table>
                    </td>
                    </tr>
                    <tr>
                        <td width="601" valign="top"> <rsweb:ReportViewer ID="storeBusinessFeedbacksReport" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                    Width="850px" Height="450px" TabIndex="3">
                                </rsweb:ReportViewer>   </td>
                    </tr>
                </table>

    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
 <script type="text/javascript">
     if ($.browser.webkit) {
         $("#storeBusinessFeedbacksReport table").each(function (i, item) {
             $(item).css('display', 'inline-block');
         });
     }
</script>    
</body>
</html>
