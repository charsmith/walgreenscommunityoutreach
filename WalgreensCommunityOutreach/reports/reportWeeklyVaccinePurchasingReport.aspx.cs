﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using TdWalgreens;
using System.Configuration;
using Microsoft.Reporting.WebForms;

public partial class reportWeeklyVaccinePurchasingReport : Page
{
    #region ------------- PROTECTED EVENTS -------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int report_year = Convert.ToDateTime(this.outreachStartDate).Year; report_year >= this.appSettings.getOutreachReportStartYear; report_year--)
            {
                this.ddlIPSeasons.Items.Add(new ListItem(report_year.ToString() + " - " + (report_year + 1).ToString(), report_year.ToString()));
            }

            if (this.ddlIPSeasons.Items.Count > 0)
                this.ddlIPSeasons.SelectedValue = (Convert.ToDateTime(this.outreachStartDate).Year).ToString();

            this.bindReportSeasonWeeks();
        }
    }

    protected void ddlIPSeasons_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.dlVaccinePurchaseReport.DataSource = null;
        this.dlVaccinePurchaseReport.DataBind();
        this.bindReportSeasonWeeks();
        //Commented for testing 04/05/3016
        //this.workflowMonitor.setWorkFlowData();
    }

    protected void lnkVaccinePurchasingReport_Click(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] stream_ids;
        string mime_type;
        string encoding;
        string extension;
        string report_filename = string.Empty;
        string report_file_path = string.Empty;

        string report_week = ((LinkButton)sender).Text.Replace(" Vaccine Purchasing", "");
        report_filename = "VPR_" + report_week.Split('-')[0].Trim().Replace("/", "") + "-" + report_week.Split('-')[1].Trim().Replace("/", "") + ".xls";

        FileInfo report_file = new FileInfo(Path.Combine(ApplicationSettings.reportsPath, report_filename));
        if (!report_file.Exists)
        {
            DataTable dt_vaccine_report;
            dt_vaccine_report = this.dbOperation.getVaccinePurchasingDetailsReport(Convert.ToDateTime(report_week.Split('-')[0].Trim()), Convert.ToDateTime(report_week.Split('-')[1].Trim()), (Convert.ToInt32(this.ddlIPSeasons.SelectedValue) != Convert.ToInt32(Convert.ToDateTime(this.outreachStartDate).Year) ? 0 : 1));
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "vaccinePurchasingDataSet";
            rds.Value = dt_vaccine_report;

            ReportViewer report_viewer = new ReportViewer();
            report_viewer.ProcessingMode = ProcessingMode.Local;
            report_viewer.LocalReport.ReportPath = Server.MapPath("vaccinePurchasingReport.rdlc");
            report_viewer.LocalReport.DataSources.Clear();
            report_viewer.LocalReport.DataSources.Add(rds);
            byte[] bytes = report_viewer.LocalReport.Render("Excel", null, out mime_type, out encoding, out extension, out stream_ids, out warnings);

            report_file_path = Path.Combine(ApplicationSettings.reportsPath, report_filename);
            using (var fs = new FileStream(report_file_path, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }
        }

        report_file = new FileInfo(Path.Combine(ApplicationSettings.reportsPath, report_filename));
        if (report_file.Exists)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + report_filename);
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Content-Length", report_file.Length.ToString());
            Response.WriteFile(report_file.FullName);
            Response.End();
        }
    }
    #endregion

    #region ------------- PRIVATE METHODS --------------
    /// <summary>
    /// Displays report season weeks links
    /// </summary>
    private void bindReportSeasonWeeks()
    {
        this.outreachStartDate = Convert.ToDateTime(this.outreachStartDate.Split('/')[0] + "/" + this.outreachStartDate.Split('/')[1] + "/" + this.ddlIPSeasons.SelectedValue).ToString("MM/dd/yyyy");
        bool is_previous_season = (this.ddlIPSeasons.SelectedValue == "2016" || this.ddlIPSeasons.SelectedValue == "2017") ? false : true;
        int first_week_days = 0;
        first_week_days = this.ddlIPSeasons.SelectedValue == "2016" ? 17 : 16;
        if (is_previous_season ? DateTime.Today.Date > Convert.ToDateTime(this.outreachStartDate).AddMonths(3) : DateTime.Today.Date > Convert.ToDateTime(this.outreachStartDate).AddMonths(2).AddDays(first_week_days))
        {
            //DateTime outreach_end_date = Convert.ToDateTime(ApplicationSettings.getVoucherMaxExpDate.ToString("MM/dd/yyyy").Split('/')[0] + "/" + ApplicationSettings.getVoucherMaxExpDate.ToString("MM/dd/yyyy").Split('/')[1] + "/" + this.ddlIPSeasons.SelectedValue).AddYears(1);
            DateTime outreach_end_date = Convert.ToDateTime(this.outreachStartDate).AddYears(1).AddDays(-1);
            outreach_end_date = (outreach_end_date <= DateTime.Now) ? outreach_end_date : DateTime.Now;

            Dictionary<string, string> report_weeks = new Dictionary<string, string>();
            report_weeks.Add(this.outreachStartDate + "-" + (is_previous_season ? this.createRequiredDate(3, 0) : this.createRequiredDate(2, first_week_days)), this.outreachStartDate + " - " + Convert.ToDateTime(is_previous_season ? this.createRequiredDate(3, 0) : this.createRequiredDate(2, first_week_days)).AddDays(-1).ToString("MM/dd/yyyy") + " Vaccine Purchasing");

            DateTime report_week2_end_date = Convert.ToDateTime(is_previous_season ? this.createRequiredDate(3, 0) : this.createRequiredDate(2, first_week_days)).AddDays(7);
            if (report_week2_end_date <= outreach_end_date)
            {
                while (report_week2_end_date.DayOfWeek != DayOfWeek.Monday)
                {
                    report_week2_end_date = report_week2_end_date.AddDays(1);
                }

                report_weeks.Add(Convert.ToDateTime(is_previous_season ? this.createRequiredDate(3, 0) : this.createRequiredDate(2, first_week_days)).ToString("MM/dd/yyyy") + "-" + Convert.ToDateTime(is_previous_season ? this.createRequiredDate(3, 0) : this.createRequiredDate(2, first_week_days)).AddDays(9).ToString("MM/dd/yyyy"), Convert.ToDateTime(is_previous_season ? this.createRequiredDate(3, 0) : this.createRequiredDate(2, first_week_days)).ToString("MM/dd/yyyy") + " - " + report_week2_end_date.AddDays(-1).ToString("MM/dd/yyyy") + " Vaccine Purchasing");
            }

            DateTime week_start_date = report_week2_end_date;
            while (week_start_date.AddDays(6) < outreach_end_date)
            {
                report_weeks.Add(week_start_date.AddDays(1).ToString("MM/dd/yyyy") + "-" + week_start_date.AddDays(7).ToString("MM/dd/yyyy"), week_start_date.ToString("MM/dd/yyyy") + " - " + week_start_date.AddDays(6).ToString("MM/dd/yyyy") + " Vaccine Purchasing");
                week_start_date = week_start_date.AddDays(7);
            }

            //Add last week
            if (outreach_end_date == Convert.ToDateTime(this.outreachStartDate).AddYears(1).AddDays(-1))
                report_weeks.Add(week_start_date.AddDays(1).ToString("MM/dd/yyyy") + "-" + outreach_end_date.ToString("MM/dd/yyyy"), week_start_date.ToString("MM/dd/yyyy") + " - " + outreach_end_date.ToString("MM/dd/yyyy") + " Vaccine Purchasing");

            this.dlVaccinePurchaseReport.DataSource = report_weeks;
            this.DataBind();
        }
    }

    /// <summary>
    /// Returns report weekly date ranges
    /// </summary>
    /// <param name="add_months"></param>
    /// <param name="add_days"></param>
    /// <returns></returns>
    private string createRequiredDate(int add_months, int add_days)
    {
        return Convert.ToDateTime(this.outreachStartDate).AddMonths(add_months).AddDays(add_days).ToString("MM/dd/yyyy");
    }
    #endregion

    #region -------------- PRIVATE & PUBLIC VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    public string outreachStartDate = string.Empty;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        //this.outreachStartDate = Convert.ToDateTime("05/01/2016").ToString("MM/dd/yyyy");
        this.outreachStartDate = ApplicationSettings.getDeploymentDate.ToString("MM/dd/yyyy");
    }
    #endregion
}