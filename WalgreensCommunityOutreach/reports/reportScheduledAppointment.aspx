﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportScheduledAppointment.aspx.cs" Inherits="reportScheduledAppointment" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Corporate Clinic Scheduled Appointments Report</title>

    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />   

    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ScheduledAppointmentReport").css("height", "80%");
            $("#ScheduledAppointmentReport_ctl10").css("height", "450px");
            $("#ScheduledAppointmentReport_ctl10").css("width", "850px");
            $("#ScheduledAppointmentReport_ctl09").css("height", "450px");
            $("#ScheduledAppointmentReport_ctl09").css("width", "850px");

            dropdownRepleceText("ddlCorporateClients", "txtCorporateClients");
            dropdownRepleceText("ddlApptTypes", "txtApptTypes");
            dropdownRepleceText("ddlClinicDates", "txtClinicDates");
            dropdownRepleceText("ddlClinicRooms", "txtClinicRooms");
            
        });
    </script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" >
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
        <table width="935" border="0" cellspacing="22" cellpadding="0">
            <tr>
                <td valign="top" class="pageTitle">Corporate Clinic Scheduled Appointments Report</td>
            </tr>
            <tr >
            <td align="left">
                <table  border="0" cellspacing="5" cellpadding="0" width="100%">
                    <tr id="rowCorporateClientsList" runat="server">
                    <td align="left" valign="top" nowrap="nowrap" width="120px" ><span class="logSubTitles">Corporate Client: </span></td>
                    <td align="left">
                        <asp:DropDownList CssClass="formFields" ID="ddlCorporateClients" runat="server" Width="240px" AutoPostBack="true" OnSelectedIndexChanged="ddlCorporateClients_SelectedIndexChanged" ></asp:DropDownList>
                        <asp:TextBox CssClass="formFields"  ID="txtCorporateClients" runat="server" Width="438px"></asp:TextBox>
                    </td>
                    </tr>
                    <tr id="rowClinicDates" runat="server">
                    <td align="left" valign="top" nowrap="nowrap" width="120px" ><span class="logSubTitles">Clinic Date: </span></td>
                    <td align="left">
                        <asp:DropDownList CssClass="formFields" ID="ddlClinicDates" runat="server" Width="120px" AutoPostBack="false"></asp:DropDownList>
                        <asp:TextBox CssClass="formFields"  ID="txtClinicDates" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    </tr>
                    <tr id="rowClinicRoomsList" runat="server">
                    <td align="left" valign="top" nowrap="nowrap" width="120px" ><span class="logSubTitles">Room: </span></td>
                    <td align="left">
                        <asp:DropDownList CssClass="formFields" ID="ddlClinicRooms" runat="server" Width="120px" AutoPostBack="false"></asp:DropDownList>
                        <asp:TextBox CssClass="formFields"  ID="txtClinicRooms" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    </tr>
                    <tr>
                    <td align="left" valign="top" nowrap="nowrap" width="120px" ><span class="logSubTitles">Appointments: </span></td>
                    <td align="left">
                        <asp:DropDownList CssClass="formFields" ID="ddlApptTypes" runat="server" Width="120px" >
                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Scheduled" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Unscheduled" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox CssClass="formFields"  ID="txtApptTypes" runat="server" Width="120px"></asp:TextBox>
                        &nbsp;&nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="logSubTitles" Text="Submit" OnClick="btnSubmit_Click" />
                    </td>
                    </tr>
                </table>
            </td>
            </tr>
            <tr>
                <td width="601" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;"> 
                    <rsweb:ReportViewer ID="ScheduledAppointmentReport" runat="server"  Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3" PageCountMode="Actual"></rsweb:ReportViewer>
                </td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
<script type="text/javascript">
    if ($.browser.webkit) {
        $("#StoreBusinessContactsReport table").each(function (i, item) {
            $(item).css('display', 'inline-block');
        });
    }
</script>
</body>
</html>
