﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using NLog;

public partial class reportStoreBusinessFeedbackClinic : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            this.bindDropdown();
            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            this.reportBind();
        }
        //this.getStoreControl1.FilePath = "../search.aspx";
        //this.displayStoreSelectionToUser();
    }

    /// <summary>
    /// Assign some filter conditon to get the filterd output report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGoUser_Click(object sender, EventArgs e)
    {
        this.reportBind();
        this.selectedFromDate = this.txtOutreachFromDate.Text;
        this.selectedToDate = this.txtOutreachToDate.Text;
    }

    /// <summary>
    /// Reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click1(object sender, EventArgs e)
    {
        this.clearFields();
    }

    /// <summary>
    /// Event to bind the report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        this.clearFields();
    }

    /// <summary>
    /// Store Selection refresh handler
    /// </summary>
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
        this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
        this.bindDropdown();
        this.reportBind();
    }
    #endregion

    #region --------------- PRIVATE METHODS ----------------

    ///// <summary>
    ///// Binding Store dropdown with out view state
    ///// </summary>
    //private void storeDropDown()
    //{
    //    DataTable user_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
    //    this.ddlStoreList.DataSource = user_stores;
    //    this.ddlStoreList.DataTextField = "address";
    //    this.ddlStoreList.DataValueField = "storeid";
    //    this.ddlStoreList.DataBind();
    //}

    ///// <summary>
    ///// Display store selection drop down to users
    ///// </summary>
    //private void displayStoreSelectionToUser()
    //{
    //    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
    //    {
    //        //this.tblStores.Visible = true;
    //        this.tdStoreSelectionDropDown.Visible = false;

    //    }
    //    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = true;
    //        this.storeDropDown();
    //        if (this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text) != null)
    //            this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text).Selected = true;
    //    }
    //    else
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = false;
    //    }
    //}

    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.ddlContacted.ClearSelection();
        this.ddlOutereach.ClearSelection();
        this.reportBind();
    }

    /// <summary>
    /// Binds stores to dropdown
    /// </summary>
    private void bindDropdown()
    {
        DataTable data_tbl = new DataTable();
        DataRow[] dr = this.dbOperation.getOutreachStatus.Select("category='AC'");
        data_tbl = dr.CopyToDataTable();

        if (data_tbl.Rows.Count > 0)
        {
            this.ddlOutereach.DataSource = data_tbl;
            this.ddlOutereach.DataTextField = "outreachStatus";
            this.ddlOutereach.DataValueField = "pk";
            this.ddlOutereach.DataBind();
            this.ddlOutereach.Items.Insert(0, new ListItem("-- Select Outreach Status --", "-1"));
        }
        data_tbl = null;
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        this.logger.Info("Binding {0} Report by user {1} - START", "Scheduled Clinic Status", this.commonAppSession.LoginUserInfoSession.UserName);

        DataTable data_tbl;
        string from_date = this.txtOutreachFromDate.Text;
        string to_date = this.txtOutreachToDate.Text;

        this.clinicsContactStatusReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getStoreBusinessFeedbackClinic(this.getStoreId, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, Convert.ToInt32(ddlContacted.SelectedItem.Value), from_date, to_date, Convert.ToInt32(this.ddlOutereach.SelectedItem.Value));

        this.logger.Info("Results were fetched from {0} method. Binding the report now..", "getStoreBusinessFeedbackClinic");

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblClinicsContactStatus";
        rds.Value = data_tbl;

        this.clinicsContactStatusReport.LocalReport.DataSources.Clear();
        this.clinicsContactStatusReport.LocalReport.DataSources.Add(rds);
        this.clinicsContactStatusReport.ShowPrintButton = false;
        this.clinicsContactStatusReport.KeepSessionAlive = true;
        this.clinicsContactStatusReport.LocalReport.ReportPath = Server.MapPath("clinicsContactStatus.rdlc");
        data_tbl = null;
        this.logger.Info("Binding {0} Report by user {1} - END", "Scheduled Clinic Status", this.commonAppSession.LoginUserInfoSession.UserName);
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    public string outreachStartDate = string.Empty;
    public string outreachEndDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region --------------- PRIVATE PROPERTIES -------------
    private int getStoreId
    {
        get
        {
            int store_id = 0;
            Int32.TryParse(this.txtStoreId.Text, out store_id);
            return store_id;
        }
    }
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
        this.outreachEndDate = ApplicationSettings.getOutreachEndDate.AddMonths(1).AddDays(1).ToString("MM/dd/yyyy");
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion

}
