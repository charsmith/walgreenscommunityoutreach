﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportLocationComplianceSO.aspx.cs" Inherits="reportLocationComplianceSO" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <style type="text/css">
       .cart-calendar-month
        {
            font-size: 11px;
        }
        </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />   

    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#locationComplianceSOReport_ctl10").css("width", "850px");
            $("#locationComplianceSOReport_ctl10").css("height", "450px");
            $("#locationComplianceSOReport_ctl09").css("width", "850px");
            $("#locationComplianceSOReport_ctl09").css("height", "450px");

            dropdownRepleceText("ddlStoreList", "txtStoreList");
            dropdownRepleceText("ddlYear", "txtYear");
            dropdownRepleceText("ddlQuarter", "txtQuarter");
            
            $("#ddlStoreList").change(function () {
                $("#txtStoreId").val($("#ddlStoreList option:selected").val());
                $("#btnRefresh").click();
            });

            $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
            $("#ReportFrameReportViewer1").css("height", "80%");
         
            $("#txtStoreId").keypress(function(event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#btnGo").click(function() {
                var storeId = $("#txtStoreId").val();
                if (storeId.length == 0) {
                    $("#txtStoreId").focus();
                    alert("Valid store ID is required.");
                    return false;
                }
            });
        });                 
    </script> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnRefresh">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" style="display:none" ImageUrl="~/images/btn_add_business.png" OnClick="btnRefresh_Click" CausesValidation="false" />  
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox> 
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    
   <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td valign="top" class="pageTitle">Store Compliance Report</td>
                    </tr>
                    <tr>
                    <td align="left">
                        <%--<table width="100%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores" > 
                                <tr>                                
                                    <td align="right" valign="top">
                                        <span class="logSubTitles">Store Search: </span>
                                    </td>
                                    <td>
                                        <uc3:getStoreControl ID="getStoreControl1" runat="server" />           
                                    </td>                                   
                                </tr> 
                                <tr>                                
                                    <td align="right" valign="top">
                                        <span class="logSubTitles">Store Name: </span>
                                    </td>
                                    <td>
                                         <asp:Label ID="lblStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="442px"></asp:Label>
                                    </td>                                   
                                </tr>                                                            
                        </table>  --%>                     
                           <%-- <table  border="0" cellspacing="5" cellpadding="0" runat="server" id="tdStoreSelectionDropDown"  width="100%">
                                <tr>                                
                                    <td align="right" valign="top" nowrap="nowrap" width="128px" ><span class="logSubTitles">Store Name: </span></td>
                                    <td align="left" colspan="3">
                                        <asp:DropDownList CssClass="formFields" ID="ddlStoreList" runat="server" EnableViewState="false" Width="440px"></asp:DropDownList>
                                        <asp:TextBox CssClass="formFields"  ID="txtStoreList" runat="server" Width="438px"></asp:TextBox>
                                  </td>                                   
                                </tr>
                             </table>--%>
                            <table cellspacing="5" cellpadding="0" width="100%">
                                <tr>
                                    <td width="140px" >&nbsp;</td>
                                    <td width="150px">
                                        <span class="logSubTitles">Year:</span>
                                        <asp:DropDownList ID="ddlYear" CssClass="formFields" runat="server" Width="100px"></asp:DropDownList>
                                        <asp:TextBox CssClass="formFields" ID="txtYear" Width="98px" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="210px">
                                        <span class="logSubTitles">Quarter:</span>
                                        <asp:DropDownList ID="ddlQuarter" runat="server" DataTextField="Value" DataValueField="Key" Width="150px" ></asp:DropDownList>
                                        <asp:TextBox CssClass="formFields" ID="txtQuarter" Width="98px" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        <span id="tblStores0" runat="server">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="logSubTitles" Text="Submit" OnClick="btnSubmit_Click" />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">
                                        
                                    </td>
                                </tr>
                            </table>
                      </td>
                    </tr>
                    <tr>
                        <td width="601" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;">
                            <rsweb:ReportViewer ID="locationComplianceSOReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3"><LocalReport></LocalReport></rsweb:ReportViewer>
                        </td>
                    </tr>
                </table>

    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
 <script type="text/javascript">
     if ($.browser.webkit) {
         $("#locationComplianceSOReport table").each(function (i, item) {
             $(item).css('display', 'inline-block');
         });
     }
</script> 
</body>
</html>
