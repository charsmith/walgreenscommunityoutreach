﻿using System;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class walgreensLoginReport : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropdown();
            this.reportBind();
            //this.ddlReportSeason.ClearSelection();
            //this.ddlReportSeason.Items[1].Selected = true;
        }   
    }

    protected void ddlDistrictManager_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.reportBind();
    }

    protected void ddlReportSeason_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.reportBind();
    }
    #endregion

    #region ---------------- PRIVATE METHODS ----------------
    /// <summary>
    /// Binding drop down values
    /// </summary>
    private void bindDropdown()
    {
        DataTable data_tbl = new DataTable();
        data_tbl = this.dbOperation.getDistrictManager;
        this.ddlDistrictManager.DataTextField = "districtManager";
        this.ddlDistrictManager.DataValueField = "districtID";
        this.ddlDistrictManager.DataSource = data_tbl;
        this.ddlDistrictManager.DataBind();
        //this.ddlDistrictManager.Items.Insert(0, new ListItem(" -- Select District -- ", "0"));
        data_tbl = null;
    }

    /// <summary>
    /// Generates report for the selected district Manager
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl = null;
        this.ReportViewer1.ProcessingMode = ProcessingMode.Local;

        data_tbl = this.dbOperation.getUserAccessLog(Convert.ToInt32(this.ddlDistrictManager.SelectedItem.Value), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, Convert.ToInt32(this.ddlReportSeason.SelectedItem.Value));
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblUserLoginCount";
        rds.Value = data_tbl;
        this.ReportViewer1.LocalReport.DataSources.Clear();
        this.ReportViewer1.LocalReport.DataSources.Add(rds);
        this.ReportViewer1.ShowPrintButton = false;
        this.ReportViewer1.LocalReport.ReportPath = Server.MapPath("loginUserCount.rdlc");
    }
    #endregion

    #region --------------- PRIVATE VARIABLES ---------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;    
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion   
}
