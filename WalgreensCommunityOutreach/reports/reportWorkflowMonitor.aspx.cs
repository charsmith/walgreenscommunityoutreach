﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using NLog;

public partial class reportWorkflowMonitor : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.reportBind();
        }
    }
    #endregion

    #region --------------- PRIVATE METHODS ----------------
    /// <summary>
    /// Bind data to the report.
    /// </summary>
    private void reportBind()
    {
        this.logger.Info("Binding {0} Report by user {1} - START", "Workflow Monitor Report", this.commonAppSession.LoginUserInfoSession.UserName);
        DataTable data_tbl;
        this.workflowMonitorReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = new DBOperations().getWorkflowMonitorReportData(Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
        this.logger.Info("Results were fetched from {0} method. Binding the report now..", "getWorkflowMonitorReportData");
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblWorkflowMonitorReport";
        rds.Value = data_tbl;

        this.workflowMonitorReport.LocalReport.DataSources.Clear();
        this.workflowMonitorReport.LocalReport.DataSources.Add(rds);
        this.workflowMonitorReport.ShowPrintButton = false;
        this.workflowMonitorReport.LocalReport.ReportPath = Server.MapPath("workflowMonitor.rdlc");
        data_tbl = null;
        this.logger.Info("Binding {0} Report by user {1} - END", "Workflow Monitor Report", this.commonAppSession.LoginUserInfoSession.UserName);
    }    
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    private AppCommonSession commonAppSession = null;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
    #endregion
}
