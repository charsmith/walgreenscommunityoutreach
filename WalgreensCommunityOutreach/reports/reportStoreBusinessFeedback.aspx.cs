﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportStoreBusinessFeedback : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindOutreachStatus();

            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            this.reportBind();
        }
        //this.getStoreControl1.FilePath = "../search.aspx";
        //this.displayStoreSelectionToUser();
    }
    
    protected void btnGoUser_Click(object sender, EventArgs e)
    {
        this.reportBind();
        this.selectedFromDate = this.txtOutreachFromDate.Text;
        this.selectedToDate = this.txtOutreachToDate.Text;
    }

    protected void btnReset_Click1(object sender, EventArgs e)
    {
        this.clearFields();
    }

    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        this.clearFields();
    }

    /// <summary>
    /// Store Selection refresh handler
    /// </summary>
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.bindOutreachStatus();
        this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
        this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
        this.reportBind();
    }
    #endregion

    #region --------------- PRIVATE METHODS ----------------
    ///// <summary>
    ///// Binding Store dropdown without viewstate
    ///// </summary>
    //private void bindStoreDropDown()
    //{
    //    DataTable user_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
    //    this.ddlStoreList.DataSource = user_stores;
    //    this.ddlStoreList.DataTextField = "address";
    //    this.ddlStoreList.DataValueField = "storeid";
    //    this.ddlStoreList.DataBind();
    //}

    ///// <summary>
    ///// Display store selection dropdown to users
    ///// </summary>
    //private void displayStoreSelectionToUser()
    //{
    //    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
    //    {
    //        //this.tblStores.Visible = true;
    //        this.tdStoreSelectionDropDown.Visible = false;
    //    }
    //    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = true;
    //        this.bindStoreDropDown();
    //        if (this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text) != null)
    //            this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text).Selected = true;
    //    }
    //    else
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = false;
    //    }
    //}

    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.ddlContacted.ClearSelection();
        this.ddlOutereach.ClearSelection();
        this.reportBind();
    }

    /// <summary>
    /// Binds Outreach contact status to dropdown
    /// </summary>
    private void bindOutreachStatus()
    {
        DataTable data_tbl = new DataTable();
        DataRow[] dr = this.dbOperation.getOutreachStatus.Select("category='" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "'");
        data_tbl = dr.CopyToDataTable();

        if (data_tbl.Rows.Count > 0)
        {
            this.ddlOutereach.DataSource = data_tbl;
            this.ddlOutereach.DataTextField = "outreachStatus";
            this.ddlOutereach.DataValueField = "pk";
            this.ddlOutereach.DataBind();
            this.ddlOutereach.Items.Insert(0, new ListItem("-- Select Outreach Status --", "-1"));
        }
        data_tbl = null;
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        string from_date = this.txtOutreachFromDate.Text;
        string to_date = this.txtOutreachToDate.Text;

        this.storeBusinessFeedbacksReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getStoreBusinessFeedback(this.getStoreId, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, Convert.ToInt32(ddlContacted.SelectedItem.Value), from_date, to_date, Convert.ToInt32(this.ddlOutereach.SelectedItem.Value));

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.storeBusinessFeedbacksReport.LocalReport.ReportPath = Server.MapPath("storeBusinessFeedbacks.rdlc");
        this.storeBusinessFeedbacksReport.LocalReport.DataSources.Clear();
        this.storeBusinessFeedbacksReport.LocalReport.DataSources.Add(rds);
        this.storeBusinessFeedbacksReport.ShowPrintButton = false;
        this.storeBusinessFeedbacksReport.KeepSessionAlive = true;

        data_tbl = null;
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    public string outreachStartDate = string.Empty;
    public string outreachEndDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    #endregion

    #region --------------- PRIVATE PROPERTIES -------------
    private int getStoreId
    {
        get
        {
            int store_id = 0;
            Int32.TryParse(this.txtStoreId.Text, out store_id);
            return store_id;
        }
    }
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
        this.outreachEndDate = ApplicationSettings.getOutreachEndDate.ToString("MM/dd/yyyy");
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion
}
