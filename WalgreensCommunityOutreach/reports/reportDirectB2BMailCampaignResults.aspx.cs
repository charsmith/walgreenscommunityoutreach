﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using System.IO;
using NLog;

public partial class reportDirectB2BMailCampaignResults : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropdown();
            this.reportBind();
        }

        if (Page.IsPostBack)
        {
            string event_args = Request["__EVENTARGUMENT"];
            if (!string.IsNullOrEmpty(event_args) && event_args.ToLower() == "csv")
                this.downloadCSVReport();

            if (!string.IsNullOrEmpty(event_args) && event_args.ToLower() == "excel")
                this.downloadExcelReport();
        }
    }

    /// <summary>
    /// Get report data based on selected filters
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }

    /// <summary>
    /// Reset all the fields
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.clearFields();
    }

    #endregion

    #region --------------- PRIVATE METHODS ----------------
    /// <summary>
    /// This method will be use to download Excel report
    /// </summary>
    private void downloadExcelReport()
    {
        byte[] bytes = rptDirectB2BMailCampaignResults.LocalReport.Render("Excel");
        this.downloadReport(bytes, "xls");
    }

    /// <summary>
    /// This method will use to download CSV format report
    /// </summary>
    protected void downloadCSVReport()
    {
        using (DataTable dt_b2b_report = this.dbOperation.getDirectB2BMailBusinessReportData(this.commonAppSession.LoginUserInfoSession.UserID, Convert.ToInt32(ddlOutreachStatus.SelectedItem.Value)))
        {
            //DataTable dt_report = null;

            //dt_report = dt_b2b_report.DefaultView.ToTable(false, "businessName", "outreachStatus", "businessAddress", "actualLocationEmploymentSize", "storeId", "districtId", "areaNumber", "regionNumber");

            var export = new ExportToCSV();

            foreach (DataRow row in dt_b2b_report.Rows)
            {
                export.AddRow();
                foreach (DataColumn column in dt_b2b_report.Columns)
                {

                    string caption_name = "";
                    switch (column.ColumnName)
                    {
                        case "businessName":
                            caption_name = "Business Name";
                            break;
                        case "businessAddress":
                            caption_name = "Business Address";
                            break;
                        case "actualLocationEmploymentSize":
                            caption_name = "Employees";
                            break;
                        case "storeId":
                            caption_name = "Store";
                            break;
                        case "districtId":
                            caption_name = "District";
                            break;
                        case "areaNumber":
                            caption_name = "Area";
                            break;
                        case "regionNumber":
                            caption_name = "Region";
                            break;
                        case "outreachStatus":
                            caption_name = "Status";
                            break;
                        case "totalImmAdministered":
                            caption_name = "Total Immunizations Administered";
                            break;
                       case "businessSource":
                            caption_name = "Business Source";
                            break;
                       case "previousClinics":
                            caption_name = "Previous Clinics";
                            break;
                    }
                    export[caption_name] = row[column].ToString().Replace("<br />", "," + System.Environment.NewLine);
                }
            }
            byte[] csv_data = export.ExportToBytes();
            export = null;
            this.downloadReport(csv_data, "csv");
        }
    }

    /// <summary>
    /// This common function for download report
    /// </summary>
    /// <param name="data"></param>
    /// <param name="format"></param>
    private void downloadReport(byte[] data, string format)
    {
        this.logger.Info("Downloading {0} by user {1} with format {2} - START", "Direct B2B Mail Campaign Results", this.commonAppSession.LoginUserInfoSession.UserName, format);
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Disposition", "attachment; filename=directB2BMailCampaignResults." + format);
        Response.AddHeader("Content-Transfer-Encoding", "BINARY");
        Response.AddHeader("Content-Length", data.Length.ToString());
        Response.AddHeader("Connection", "Keep-Alive");
        Response.AddHeader("Cache-Control", "max-age=1");

        Response.BinaryWrite(data);
        data = null;
        GC.Collect();
        this.logger.Info("Downloading {0} by user {1} with format {2} - END", "Direct B2B Mail Campaign Results", this.commonAppSession.LoginUserInfoSession.UserName, format);
        Response.End();
    }

    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.ddlOutreachStatus.ClearSelection();
        this.reportBind();
    }

    /// <summary>
    /// Binds outreach statuses to dropdown
    /// </summary>
    private void bindDropdown()
    {
        DataTable data_tbl = new DataTable();
        DataRow[] dr = this.dbOperation.getOutreachStatus.Select("category='IP' AND isActive = 1");
        data_tbl = dr.CopyToDataTable();

        if (data_tbl.Rows.Count > 0)
        {
            this.ddlOutreachStatus.DataSource = data_tbl;
            this.ddlOutreachStatus.DataTextField = "outreachStatus";
            this.ddlOutreachStatus.DataValueField = "pk";
            this.ddlOutreachStatus.DataBind();
            this.ddlOutreachStatus.Items.Insert(0, new ListItem("Scheduled", "-2"));
            this.ddlOutreachStatus.Items.Insert(0, new ListItem("All", "-1"));
        }
        data_tbl = null;
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        this.logger.Info("Binding {0} Report by user {1} - START", "Direct B2B Mail Campaign Results", this.commonAppSession.LoginUserInfoSession.UserName);
        DataTable dt_b2b_report = new DataTable();
        this.rptDirectB2BMailCampaignResults.ProcessingMode = ProcessingMode.Local;

        dt_b2b_report = this.dbOperation.getDirectB2BMailBusinessReportData(this.commonAppSession.LoginUserInfoSession.UserID, Convert.ToInt32(ddlOutreachStatus.SelectedItem.Value));
        this.logger.Info("Results were fetched from {0} method. Binding the report now..", "getDirectB2BMailBusinessReportData");

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_DirectB2BMailCampaignResults";
        rds.Value = dt_b2b_report;

        this.rptDirectB2BMailCampaignResults.LocalReport.DataSources.Clear();
        this.rptDirectB2BMailCampaignResults.LocalReport.DataSources.Add(rds);
        this.rptDirectB2BMailCampaignResults.ShowPrintButton = false;
        this.rptDirectB2BMailCampaignResults.LocalReport.ReportPath = Server.MapPath("directB2BMailCampaignResults.rdlc");

        dt_b2b_report = null;
        this.logger.Info("Binding {0} Report by user {1} - END", "Direct B2B Mail Campaign Results", this.commonAppSession.LoginUserInfoSession.UserName);
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
    }
    #endregion
}
