﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportLocationComplianceSO : Page
{
    #region ------------------ PROTECTED EVENTS -----------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropDown();
            this.reportBind();
        }
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }
    #endregion

    #region ------------------ PRIVATE METHODS ------------------

    private void bindDropDown()
    {
        this.ddlYear.DataSource = this.appSettings.getOutreachReportYears;
        this.ddlYear.DataBind();

        if (this.ddlYear.Items.Count > 0)
            this.ddlYear.SelectedValue = (ApplicationSettings.getOutreachStartDate.Year + 1).ToString();

        this.ddlQuarter.DataSource = this.appSettings.getReportYearQuarters;
        this.ddlQuarter.DataBind();
        this.ddlQuarter.SelectedValue = ApplicationSettings.getCurrentQuarter.ToString();
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;

        this.highLevelSOReport.ProcessingMode = ProcessingMode.Local;
        DateTime quarter_start_date = Convert.ToDateTime(@"09/01/" + this.ddlYear.SelectedItem.Text).AddYears(-1);
        data_tbl = this.dbOperation.getHighLevelReportSO(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, quarter_start_date, ddlQuarter.SelectedValue);
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.highLevelSOReport.LocalReport.DataSources.Clear();
        this.highLevelSOReport.LocalReport.DataSources.Add(rds);
        this.highLevelSOReport.ShowPrintButton = false;
        if (data_tbl.Rows.Count > 0)
            this.highLevelSOReport.LocalReport.ReportPath = Server.MapPath("highLevelSO.rdlc");
        data_tbl = null;
    }
    #endregion

    #region ----------------- PRIVATE VARIABLES -----------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
    }
    #endregion
    
}
