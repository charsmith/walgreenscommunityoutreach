﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using System.Globalization;
using TdWalgreens;

public partial class reportActivitySO : Page
{
    #region --------------- PROTECTED Events --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.outreachReportStartYear = new ApplicationSettings().getOutreachReportStartYear;

            this.displayStoreFilters();
            this.bindTimePeriod();

            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "admin":
                    this.rowReportFilterBy.Visible = true;
                    this.rowReportFilters.Visible = true;
                    this.regionSelection.Visible = true;
                    this.areaSelection.Visible = true;
                    this.districtSelection.Visible = true;
                    this.bindDropDown("", 0, 0);
                    break;
                case "regional vice president":
                case "regional healthcare director":
                    this.rowReportFilterBy.Visible = true;
                    this.rowReportFilters.Visible = true;
                    this.areaSelection.Visible = true;
                    this.districtSelection.Visible = true;
                    this.bindDropDown("Region", 0, 0);
                    break;
                case "healthcare supervisor":
                case "director – rx & retail ops":
                    this.rowReportFilters.Visible = true;
                    this.districtSelection.Visible = true;
                    this.bindDropDown("Area", 0, 0);
                    break;
            }
            this.reportBind();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }

    protected void ddlMarkets_OnChanged(object sender, EventArgs e)
    {
        this.bindDropDown("Region", Convert.ToInt32(this.ddlMarkets.SelectedValue), 0);
    }

    protected void ddlAreas_OnChanged(object sender, EventArgs e)
    {
        this.bindDropDown("Area", ((this.regionSelection.Visible) ? Convert.ToInt32(this.ddlMarkets.SelectedValue) : 0), Convert.ToInt32(this.ddlAreas.SelectedValue));
    }

    protected void ddlYear_OnChanged(object sender, EventArgs e)
    {
        this.ddlQuarter.Items.Clear();

        this.bindTimePeriod();
    }

    #endregion

    #region --------------- PRIVATE METHODS ---------------
    /// <summary>
    /// Display store filter options
    /// </summary>
    private void displayStoreFilters()
    {
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.ddlFilterReportBy.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlFilterReportBy.Items.Insert(1, new ListItem("Region", "1"));
            this.ddlFilterReportBy.Items.Insert(2, new ListItem("Area", "2"));
            this.ddlFilterReportBy.Items.Insert(3, new ListItem("District", "3"));
        }
        else if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional vice president" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional healthcare director")
        {
            this.rowReportFilterBy.Visible = true;
            this.rowReportFilters.Visible = true;
            this.ddlFilterReportBy.Items.Insert(0, new ListItem("-- Select --", "0"));
            this.ddlFilterReportBy.Items.Insert(1, new ListItem("Area", "2"));
            this.ddlFilterReportBy.Items.Insert(2, new ListItem("District", "3"));
        }

        for (int int_year = this.outreachReportStartYear; int_year <= this.outreachStartDate.Year + 1; int_year++)
        {
            this.ddlYear.Items.Add(new ListItem(int_year.ToString(), int_year.ToString()));
        }

        if (this.ddlYear.Items.Count > 0)
            this.ddlYear.SelectedValue = (this.outreachStartDate.Year + 1).ToString();
    }

    /// <summary>
    /// Binds Time Period dropdown options
    /// </summary>
    private void bindTimePeriod()
    {
        int months_count = 0;

        //this.ddlQuarter.Items.Insert(0, new ListItem("-- Select --", "0"));
        this.ddlQuarter.Items.Insert(0, new ListItem("Q1", "1"));
        this.ddlQuarter.Items.Insert(1, new ListItem("Q2", "2"));
        this.ddlQuarter.Items.Insert(2, new ListItem("Q3", "3"));
        this.ddlQuarter.Items.Insert(3, new ListItem("Q4", "4"));
        this.ddlQuarter.Items.Insert(4, new ListItem("Fiscal Year to Date", "5"));

        if (Convert.ToInt32(this.ddlYear.SelectedValue) == this.outreachStartDate.Year + 1)
            months_count = (DateTime.Now.Month < 9) ? 13 + DateTime.Now.Month : DateTime.Now.Month + 1;
        else
            months_count = 21;

        for (int i = 9; i < months_count; i++)
            this.ddlQuarter.Items.Insert((i - 4), new ListItem(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((i > 12) ? i - 12 : i), (i - 3).ToString()));

        this.ddlQuarter.SelectedValue = ApplicationSettings.getCurrentQuarter.ToString();
    }

    /// <summary>
    /// Binds markets, districts, report year and quarters list to dropdowns
    /// </summary>
    private void bindDropDown(string filter_type, int region_number, int area_number)
    {
        string user_type = string.Empty;
        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional vice president" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional healthcare director")
            user_type = "Region";
        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "healthcare supervisor" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "director – rx & retail ops")
            user_type = "Area";

        DataSet ds_dist_mrkts = new DataSet();
        ds_dist_mrkts = this.dbOperation.getDistrictsAndMarkets(this.commonAppSession.LoginUserInfoSession.UserID, user_type, region_number, area_number);

        if (ds_dist_mrkts != null && ds_dist_mrkts.Tables.Count == 3)
        {
            if (this.regionSelection.Visible && string.IsNullOrEmpty(filter_type))
            {
                this.ddlMarkets.DataSource = ds_dist_mrkts.Tables[0];
                this.ddlMarkets.DataTextField = "marketName";
                this.ddlMarkets.DataValueField = "marketNumber";
                this.ddlMarkets.DataBind();
                this.ddlMarkets.Items.Insert(0, new ListItem("--Select Region--", "-1"));
            }

            if (this.areaSelection.Visible && filter_type != "Area")
            {
                this.ddlAreas.DataSource = ds_dist_mrkts.Tables[1];
                this.ddlAreas.DataTextField = "areaName";
                this.ddlAreas.DataValueField = "areaNumber";
                this.ddlAreas.DataBind();
                this.ddlAreas.Items.Insert(0, new ListItem("--Select Area--", "-1"));
            }

            if (this.districtSelection.Visible)
            {
                this.ddlDistricts.DataSource = ds_dist_mrkts.Tables[2];
                this.ddlDistricts.DataTextField = "districtName";
                this.ddlDistricts.DataValueField = "districtNumber";
                this.ddlDistricts.DataBind();
                this.ddlDistricts.Items.Insert(0, new ListItem("--Select District--", "-1"));
            }
        }
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;

        this.activityReportSO.ProcessingMode = ProcessingMode.Local;
        DateTime quarter_start_date = Convert.ToDateTime(@"09/01/" + this.ddlYear.SelectedItem.Text).AddYears(-1);

        if (Convert.ToInt32(this.ddlQuarter.SelectedValue) > 5)
        {
            if (Convert.ToInt32(this.ddlQuarter.SelectedValue) > 10)
                quarter_start_date = Convert.ToDateTime(DateTime.ParseExact(this.ddlQuarter.SelectedItem.Text, "MMMM", CultureInfo.InvariantCulture).Month + "/01/" + this.ddlYear.SelectedItem.Text);
            else
                quarter_start_date = Convert.ToDateTime(DateTime.ParseExact(this.ddlQuarter.SelectedItem.Text, "MMMM", CultureInfo.InvariantCulture).Month + "/01/" + this.ddlYear.SelectedItem.Text).AddYears(-1);
        }

        data_tbl = this.dbOperation.getActivityReportSO(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId),
                                                        quarter_start_date, Convert.ToInt32(this.ddlQuarter.SelectedValue), (Convert.ToInt32(this.ddlYear.SelectedValue) == this.outreachStartDate.Year + 1 ? 1 : 0),
                                                        (this.regionSelection.Visible ? ((this.ddlMarkets.SelectedValue == "-1") ? 0 : Convert.ToInt32(this.ddlMarkets.SelectedValue)) : 0),
                                                        (this.areaSelection.Visible ? ((this.ddlAreas.SelectedValue == "-1") ? 0 : Convert.ToInt32(this.ddlAreas.SelectedValue)) : 0),
                                                        (this.districtSelection.Visible ? (this.ddlDistricts.SelectedValue == "-1" ? 0 : Convert.ToInt32(this.ddlDistricts.SelectedValue)) : 0),
                                                        (this.rowReportFilterBy.Visible ? Convert.ToInt32(this.ddlFilterReportBy.SelectedValue) : 0));

        if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
            this.activityReportSO.Width = 700;
        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "store manager" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "pharmacy manager" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
            this.activityReportSO.Width = 570;

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessContacts";
        rds.Value = data_tbl;

        this.activityReportSO.LocalReport.DataSources.Clear();
        this.activityReportSO.LocalReport.DataSources.Add(rds);
        this.activityReportSO.ShowPrintButton = false;
        this.activityReportSO.LocalReport.ReportPath = Server.MapPath("activityReportSO.rdlc");
        data_tbl = null;
    }
    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private DateTime outreachStartDate;
    private int outreachReportStartYear;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate;
    }
    #endregion
}
