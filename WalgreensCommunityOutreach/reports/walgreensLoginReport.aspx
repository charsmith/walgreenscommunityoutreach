﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensLoginReport.aspx.cs" Inherits="walgreensLoginReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <style type="text/css">
       .cart-calendar-month
        {
            font-size: 11px;
        }
        </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            dropdownRepleceText("ddlDistrictManager", "txtDistrictManager");
            dropdownRepleceText("ddlReportSeason", "txtReportSeason");
            $("#ReportFrameReportViewer1").css("height", "80%");

            $("#ReportViewer1_ctl10").css("height", "450px");
            $("#ReportViewer1_ctl10").css("width", "850px");
//            $("#ReportViewer1_ctl05").css("height", "450px");
//            $("#ReportViewer1_ctl05").css("width", "850px");
            $("#ReportViewer1_ctl09").css("height", "450px");
            $("#ReportViewer1_ctl09").css("width", "850px");
        });  
                       
    </script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
<tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    
    <table width="935" border="0" cellspacing="22" cellpadding="0">
                <tr>
                    <td valign="top" class="pageTitle">
                        User Access Log Report
                    </td>
                </tr>
                <tr>
                      <td valign="top" >
                          <table width="100%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores" >
                                <tr>                                
                                    <td align="left" valign="top">
                                        <span class="logSubTitles">District:</span>
                                    </td>
                                    <td>
                                        <asp:TextBox id="txtDistrictManager" CssClass="formFields" visible="true" runat="server" Width="248px" ></asp:TextBox>
                                        <asp:DropDownList ID="ddlDistrictManager" runat="server" class="formFields" AutoPostBack="true" Width="250px" onselectedindexchanged="ddlDistrictManager_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>                                   
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <span class="logSubTitles">Season:</span>
                                    </td>
                                    <td>
                                        <asp:TextBox id="txtReportSeason" CssClass="formFields" visible="true" runat="server" Width="248px" ></asp:TextBox>
                                        <asp:DropDownList ID="ddlReportSeason" runat="server" class="formFields" AutoPostBack="true" Width="250px" onselectedindexchanged="ddlReportSeason_SelectedIndexChanged">
                                            <%--<asp:ListItem Text="All" Value="0"></asp:ListItem>--%>
                                            <asp:ListItem Text="2017-18" Value="2017"></asp:ListItem>
                                            <asp:ListItem Text="2016-17" Value="2016"></asp:ListItem>
                                            <asp:ListItem Text="2015-16" Value="2015"></asp:ListItem>
                                            <asp:ListItem Text="2014-15" Value="2014"></asp:ListItem>
                                            <asp:ListItem Text="2013-14" Value="2013"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                </table>
                        </td>
                    </tr>       
                <tr>
                    <td style="Width:auto; padding-left:45px; padding-right:45px; padding-bottom:45px; overflow:auto;" valign="top">
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3"></rsweb:ReportViewer>
                    </td>
                </tr>
                </table>

    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
<script type="text/javascript">
    if ($.browser.webkit) {
        $("#ReportViewer1 table").each(function (i, item) {
            $(item).css('display', 'inline-block');
        });
    }
</script> 
</body>
</html>

