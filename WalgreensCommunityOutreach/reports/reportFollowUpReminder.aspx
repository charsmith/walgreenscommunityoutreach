﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportFollowUpReminder.aspx.cs" Inherits="reportFollowUpReminder" %>
<%@ Register src="../controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>       
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />   
    <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />  
    <link rel="stylesheet" type="text/css" href="../css/theme.css" />
          
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>

    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />  

       <script type="text/javascript">
           var minOutreachDate = new Date("<%=outreachStartDate %>");
           var maxOutreachDate = new Date("<%=outreachEndDate %>");
           var selectedFromDate = "<%=selectedFromDate %>";
           var selectedToDate = "<%=selectedToDate %>";

           $(document).ready(function () {
               $("#txtOutreachFromDate").attr("readonly", "readonly");
               $("#txtOutreachToDate").attr("readonly", "readonly");

               createDateCalendar();

               $("#ddlStoreList").change(function () {
                   $("#txtStoreId").val($("#ddlStoreList option:selected").val());
                   $("#btnRefresh").click();                   
               });

               $("#btnGoUser").click(function () {
                   if (new Date($("#txtOutreachFromDate").val()) > new Date($("#txtOutreachToDate").val())) {
                       alert('"From Date" should be less than "To Date"');
                       createDateCalendar();
                       return false;
                   }
               });

               $("#btnRefresh").click(function () {
                   selectedFromDate = "", selectedToDate = "";
                   createDateCalendar();
               });

               $("#btnReset").click(function () {
                   selectedFromDate = "", selectedToDate = "";
                   createDateCalendar();
               });

               $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
               dropdownRepleceText("ddlStoreList", "txtStoreList");

               $("#ReportFrameReportViewer1").css("height", "80%");
               $("#followUpReport_ctl10").css("width", "800px");
               $("#followUpReport_ctl10").css("height", "450px");
               $("#followUpReport_ctl09").css("width", "800px");
               $("#followUpReport_ctl09").css("height", "450px");
           });

           function createDateCalendar() {
               $("#txtOutreachFromDate").datepicker({
                   showOn: "button",
                   buttonImage: "../images/btn_calendar.gif",
                   buttonImageOnly: true,
                   dateFormat: 'mm/dd/yy',
                   maxDate: maxOutreachDate
               }).datepicker("setDate", minOutreachDate);
               $("#txtOutreachFromDate").datepicker("option", "minDate", minOutreachDate);

               $("#txtOutreachToDate").datepicker({
                   showOn: "button",
                   buttonImage: "../images/btn_calendar.gif",
                   buttonImageOnly: true,
                   dateFormat: 'mm/dd/yy',
                   maxDate: maxOutreachDate
               }).datepicker("setDate", maxOutreachDate);
               $("#txtOutreachToDate").datepicker("option", "minDate", minOutreachDate);

               if (selectedFromDate != "" && selectedToDate != "") {
                   $("#txtOutreachFromDate").val(selectedFromDate);
                   $("#txtOutreachToDate").val(selectedToDate);
               }
               else {
                   $("#txtScheduledOnDateFrom").datepicker({ defaultDate: minOutreachDate });
                   $("#txtScheduledOnDateTo").datepicker({ defaultDate: maxOutreachDate });
               }

               $(".ui-datepicker-trigger").css("margin-bottom", "-6px");
               $(".ui-datepicker-trigger").css("padding-left", "5px");
           }  
        </script>
        <style type="text/css">
        .ui-widget
        {
            font-size: 11px;
        }
       .cart-calendar-month
        {
            font-size: 11px;
        }
        </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnRefresh">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" style="display:none" OnClick="btnRefresh_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />  
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>   
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    
    <table width="935" border="0" cellspacing="22" cellpadding="0">
    <tr>
        <td valign="top" class="pageTitle">Follow-up Reminder Report</td>
    </tr>
    <tr>
    <td align="left">
        <%--<table width="100%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores" > 
            <tr>                                
                <td align="left" valign="top" class="logSubTitles" width="105px">Store Search: </td>
                <td>
                    <uc3:getStoreControl ID="getStoreControl1" runat="server" />           
                </td>                                   
            </tr> 
            <tr>                                
                <td align="left" valign="top" class="logSubTitles">Store Name: </td>
                <td>
                    <asp:Label ID="lblStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="442px"></asp:Label>
                </td>                                   
            </tr>                                                            
        </table>   --%>                    
        <%--<table  border="0" cellspacing="5" cellpadding="0" runat="server" id="tdStoreSelectionDropDown"  width="100%">
            <tr>                                
                <td align="left" valign="top" nowrap="nowrap" width="105px" class="logSubTitles">Store Name: </td>
                <td align="left">
                    <asp:DropDownList CssClass="formFields" ID="ddlStoreList" runat="server" EnableViewState="false" Width="440px" ></asp:DropDownList>
                    <asp:TextBox CssClass="formFields"  ID="txtStoreList" runat="server" Width="438px"></asp:TextBox>
                </td>                                   
            </tr>                                 
        </table>--%>
        <table cellspacing="5" cellpadding="0" width="65%">
        <tr>
        <td style="width: 105px; text-align: right;">&nbsp;</td>
        <td colspan="4" style="text-align:left;"><asp:CheckBox ID="chkIncludeDateRange" runat="server" Text="Filter by date range" CssClass="logSubTitles" /></td>
        </tr>
        <tr>
            <td style="width: 105px; text-align: right;">&nbsp;</td>
            <td width="75px" class="logSubTitles">From date:</td>
            <td align="left" valign="bottom" >
                <asp:TextBox ID="txtOutreachFromDate" CssClass="formFields" Text="" runat="server" Width="80px" ></asp:TextBox>
            </td>
            <td align="right" class="logSubTitles">To date:</td>
            <td align="left" valign="bottom" >
                <asp:TextBox ID="txtOutreachToDate" CssClass="formFields" Text="" runat="server" Width="80px"></asp:TextBox>
            </td> 
        </tr>
        <tr style="padding-top: 10px;">
            <td>&nbsp;</td>
            <td colspan="3">
                <span id="tblStores0" runat="server">
                    <asp:Button ID="btnGoUser" runat="server" OnClick="btnGoUser_Click" Text="Show Report" CssClass="logSubTitles" />
                    &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="logSubTitles" OnClick="btnReset_Click1" />
                </span>
            </td>
        </tr>
    </table>
            </td>
            </tr>
            <tr>
                <td width="601" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;">
                    <rsweb:ReportViewer ID="followUpReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="800px" Height="450px" TabIndex="3" PageCountMode="Actual" AsyncRendering="true"></rsweb:ReportViewer>
                </td>
            </tr>
        </table>

    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
 <script type="text/javascript">
     if ($.browser.webkit) {
         $("#followUpReport table").each(function (i, item) {
             $(item).css('display', 'inline-block');
         });
     }
</script>    
</body>
</html>
