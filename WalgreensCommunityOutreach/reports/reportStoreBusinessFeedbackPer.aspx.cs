﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportStoreBusinessFeedbackPer : Page
{
    #region --------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            this.reportBind();
        }
        //this.getStoreControl1.FilePath = "../search.aspx";
        //this.displayStoreSelectionToUser();
    }

    /// <summary>
    /// Event to bind the report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        this.reportBind();
    }

    /// <summary>
    /// Store Selection refresh handler
    /// </summary>
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
        this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
        this.reportBind();
    }
    #endregion

    #region --------------- PRIVATE METHODS ---------------
    ///// <summary>
    ///// Binding Store dropdown with out view state
    ///// </summary>
    //private void storeDropDown()
    //{
    //    DataTable user_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
    //    this.ddlStoreList.DataSource = user_stores;
    //    this.ddlStoreList.DataTextField = "address";
    //    this.ddlStoreList.DataValueField = "storeid";
    //    this.ddlStoreList.DataBind();
    //}

    ///// <summary>
    ///// Display store selection drop down to users
    ///// </summary>
    //private void displayStoreSelectionToUser()
    //{
    //    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
    //    {
    //        this.tblStores.Visible = true;
    //        this.tdStoreSelectionDropDown.Visible = false;

    //    }
    //    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
    //    {
    //        this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = true;
    //        this.storeDropDown();
    //        this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text).Selected = true;
    //    }
    //    else
    //    {
    //        this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = false;
    //    }
    //}
    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;

        this.StoreBusinessFeedbacksPerReport.ProcessingMode = ProcessingMode.Local;
        int store_id = 0;
        int.TryParse(this.txtStoreId.Text, out store_id);
        data_tbl = this.dbOperation.getStoreBusinessFeedbackPer(store_id, this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId);
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.StoreBusinessFeedbacksPerReport.LocalReport.DataSources.Clear();
        this.StoreBusinessFeedbacksPerReport.LocalReport.DataSources.Add(rds);
        this.StoreBusinessFeedbacksPerReport.ShowPrintButton = false;
        this.StoreBusinessFeedbacksPerReport.LocalReport.ReportPath = Server.MapPath("storeBusinessFeedbacksPer.rdlc");
        data_tbl = null;
    }
    #endregion

    #region --------------- PRIVATE VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion
}
