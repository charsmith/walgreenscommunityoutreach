﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportePendingEmailReminders : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.reportBind();
        }
    }
    /// <summary>
    /// Assign some filter conditon to get the filterd output report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGoUser_Click(object sender, EventArgs e)
    {
        this.reportBind();
        this.selectedFromDate = this.txtOutreachFromDate.Text;
        this.selectedToDate = this.txtOutreachToDate.Text;
    }

    /// <summary>
    /// Reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click1(object sender, EventArgs e)
    {
        this.clearFields();
    }

    /// <summary>
    /// Event to bind the report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        this.reportBind();
    }
    #endregion

    #region --------------- PRIVATE METHODS ----------------
    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.reportBind();
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;

        string from_date = this.txtOutreachFromDate.Text;
        string to_date = this.txtOutreachToDate.Text;

        this.pendingEmailRemindersReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getPendingEmailReminders(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, from_date.Trim().Length == 0 ? this.outreachStartDate : from_date, to_date.Trim().Length == 0 ? this.outreachEndDate : to_date);

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.pendingEmailRemindersReport.LocalReport.DataSources.Clear();
        this.pendingEmailRemindersReport.LocalReport.DataSources.Add(rds);
        this.pendingEmailRemindersReport.ShowPrintButton = false;

        this.pendingEmailRemindersReport.LocalReport.ReportPath = Server.MapPath("pendingEmailReminders.rdlc");

        data_tbl = null;
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    public string outreachStartDate = string.Empty;
    public string outreachEndDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
        this.outreachEndDate = ApplicationSettings.getOutreachEndDate.ToString("MM/dd/yyyy");
    }
    #endregion

}
