﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdWalgreens;

public partial class reportContractAgreements : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.reportBind();
        }
    }

    #endregion

    #region --------------- PRIVATE METHODS ----------------

    /// <summary>
    /// Bind data to the report.
    /// </summary>
    private void reportBind()
    {
        DataTable dt_CI_data;
        this.contractAgreementsReport.ProcessingMode = ProcessingMode.Local;
        dt_CI_data = new DBOperations().getContractInitiatedReportData;

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_ContractAgreementsData";
        rds.Value = dt_CI_data;

        this.contractAgreementsReport.LocalReport.DataSources.Clear();
        this.contractAgreementsReport.LocalReport.DataSources.Add(rds);
        this.contractAgreementsReport.ShowPrintButton = false;
        this.contractAgreementsReport.LocalReport.ReportPath = Server.MapPath("contractAgreements.rdlc");

        dt_CI_data = null;
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------    
    private AppCommonSession commonAppSession = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
    #endregion
}
