﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportStoreBusinessFeedbackPer.aspx.cs" Inherits="reportStoreBusinessFeedbackPer" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />   

    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            dropdownRepleceText("ddlStoreList", "txtStoreList");
            $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
            $("#ReportFrameReportViewer1").css("height", "80%");

            $("#ddlStoreList").change(function () {
                $("#txtStoreId").val($("#ddlStoreList option:selected").val());
                $("#btnRefresh").click();
            }); 
        });

                      
    </script> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnRefresh">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" style="display:none" OnClick="btnRefresh_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />  
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>    
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    
   <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td valign="top" class="pageTitle">Store Compliance Report</td>
                    </tr>
                    <tr>
                    <td>
                       <%-- <table width="100%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores" > 
                                <tr>                                
                                    <td align="left" valign="top">
                                        <span class="logSubTitles">Store Search: </span>
                                    </td>
                                    <td>
                                        <uc3:getStoreControl ID="getStoreControl1" runat="server" />           
                                    </td>                                   
                                </tr> 
                                <tr>                                
                                    <td align="left" valign="top">
                                        <span class="logSubTitles">Store Name: </span>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px"></asp:Label>
                                    </td>                                   
                                </tr>                                                            
                            </table>--%>
                           <%-- <table  border="0" cellspacing="5" cellpadding="0" runat="server" id="tdStoreSelectionDropDown"  width="100%">
                                <tr>                                
                                    <td align="left" valign="top" nowrap="nowrap" width="128px" ><span class="logSubTitles">
                                        Store Name: </span></td>
                                    <td align="left">
                                        <asp:DropDownList CssClass="formFields" ID="ddlStoreList" runat="server" EnableViewState="false" Width="440px"></asp:DropDownList>
                                        <asp:TextBox CssClass="formFields"  ID="txtStoreList" runat="server" Width="438px"></asp:TextBox>
                                  </td>                                   
                                </tr>                                 
                             </table>  --%>
                    </td>
                    </tr>
                    <tr>
                        <td width="601" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;">
                            <rsweb:ReportViewer ID="StoreBusinessFeedbacksPerReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="837px" Height="450px" TabIndex="3"><LocalReport></LocalReport></rsweb:ReportViewer>
                        </td>
                    </tr>
                </table>

    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
 <script type="text/javascript">
     if ($.browser.webkit) {
         $("#StoreBusinessFeedbacksPerReport table").each(function (i, item) {
             $(item).css('display', 'inline-block');
         });
     }
</script> 
</body>
</html>
