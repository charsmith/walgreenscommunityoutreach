﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportAssignedNationalAccounts : Page
{
    #region ---------------- PROTECTED EVENTS ------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropdown();
        }

        this.reportBind();
    }
    #endregion

    #region -------------------PRIVATE METHODS-------------------
    /// <summary>
    /// Binding drop down information.
    /// </summary>
    private void bindDropdown()
    {
        this.ddlDistrictManager.DataTextField = "districtManager";
            this.ddlDistrictManager.DataValueField = "districtID";
            this.ddlDistrictManager.DataSource = this.dbOperation.getDistrictManager;
            this.ddlDistrictManager.DataBind();
            this.ddlDistrictManager.Items.Insert(0, new ListItem(" -- Select District -- ", "0"));
    }
    /// <summary>
    /// Report Bind depending on filter condition.
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        this.assignedNationalAccountsReport.ProcessingMode = ProcessingMode.Local;
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.tblDistrict.Visible = true;
            data_tbl = this.dbOperation.getAssignedNationalAccounts(Convert.ToInt32(this.commonAppSession.LoginUserInfoSession.UserID), Convert.ToInt32(this.ddlDistrictManager.SelectedItem.Value));
        }
        else
        {
            this.tblDistrict.Visible = false;
            data_tbl = this.dbOperation.getAssignedNationalAccounts(Convert.ToInt32(this.commonAppSession.LoginUserInfoSession.UserID), 0);
        }
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblAssignedNationalAccounts";
        rds.Value = data_tbl;

        this.assignedNationalAccountsReport.LocalReport.DataSources.Clear();
        this.assignedNationalAccountsReport.LocalReport.DataSources.Add(rds);
        this.assignedNationalAccountsReport.ShowPrintButton = false;
        if (data_tbl.Rows.Count > 0)
            this.assignedNationalAccountsReport.LocalReport.ReportPath = Server.MapPath("assignedNationalAccounts.rdlc");
        data_tbl = null;
    }    
    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}
