﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportHighLevel.aspx.cs" Inherits="reportHighLevel" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />    
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">       
        $(document).ready(function() {
            $("#txtStoreId").keypress(function(event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });
        });                 
    </script> 
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    
    <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td valign="top" class="pageTitle">High Level Compliance Report</td>
                    </tr>                   
                    <tr>
                        <td width="601" valign="top" style="padding:45px"> <rsweb:ReportViewer ID="highLevelReport" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                    Width="850px" Height="450px" TabIndex="3">                                   
                                </rsweb:ReportViewer>   </td>
                    </tr>
                </table>

    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
<script type="text/javascript">
    if ($.browser.webkit) {
        $("#highLevelReport table").each(function (i, item) {
            $(item).css('display', 'inline-block');
        });
    }
</script>    
</body>
</html>
