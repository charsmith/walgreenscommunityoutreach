﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensReport.aspx.cs" Inherits="walgreensReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>

    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />   

    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>
   

    <script type="text/javascript">
        $(document).ready(function () {

            $("#ddlStoreList").change(function () {
                $("#txtStoreId").val($("#ddlStoreList option:selected").val());
                $("#btnRefresh").click();
            });
            dropdownRepleceText("ddlStoreList", "txtStoreList");
            $("#lblStoreProfiles").html($("#txtStoreProfiles").val());
            // $("#ReportFrameReportViewer1").css("height", "80%");

            $("#StoreBusinessContactsReport_ctl10").css("width", "850px");
            $("#StoreBusinessContactsReport_ctl10").css("height", "450px");
            $("#StoreBusinessContactsReport_ctl09").css("width", "850px");
            $("#StoreBusinessContactsReport_ctl09").css("height", "450px");
        });
                       
    </script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnRefresh">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" style="display:none" OnClick="btnRefresh_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>    
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
        <table width="935" border="0" cellspacing="22" cellpadding="0">
            <tr>
                <td valign="top" class="pageTitle">Potential Local Business List</td>
            </tr>
            <tr>
            <td align="left">
                <%--<table width="70%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores" > 
                    <tr>
                        <td align="left" valign="top" class="logSubTitles">Store Search: </td>
                        <td>
                            <uc3:getStoreControl ID="getStoreControl1" runat="server" />
                        </td>
                    </tr> 
                    <tr>
                        <td align="left" valign="top" class="logSubTitles">Store Name: </td>
                        <td>
                            <asp:Label ID="lblStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px"></asp:Label>
                        </td>
                    </tr>
                </table>--%>
                <%--<table  border="0" cellspacing="5" cellpadding="0" runat="server" id="tdStoreSelectionDropDown"  width="100%">
                <tr>
                    <td align="left" valign="top" nowrap="nowrap" width="86px" class="logSubTitles">Store Name: </td>
                    <td align="left">
                        <asp:DropDownList CssClass="formFields" ID="ddlStoreList" runat="server" EnableViewState="false" Width="440px" ></asp:DropDownList>
                        <asp:TextBox CssClass="formFields"  ID="txtStoreList" runat="server" Width="438px"></asp:TextBox>
                    </td>
                </tr>
                </table>--%>
            </td>
            </tr>
            <tr>
                <td width="601" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;"> 
                    <rsweb:ReportViewer ID="StoreBusinessContactsReport" runat="server"  Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3" PageCountMode="Actual" AsyncRendering="false" ><LocalReport></LocalReport></rsweb:ReportViewer>
                </td>
            </tr>
        </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
<script type="text/javascript">
    if ($.browser.webkit) {
        $("#StoreBusinessContactsReport table").each(function (i, item) {
            $(item).css('display', 'inline-block');
        });
    }
</script>
</body>
</html>
