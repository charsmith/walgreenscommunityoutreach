﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using DataBaseFactory;
using System.Web.UI;
using TdWalgreens;

public partial class reportDistrictCompliance : Page
{
    #region --------------PROTECTED EVENTS -------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropdown();
            this.reportBind();
        }
    }
    protected void ddlDistrictManager_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.reportBind();
    }
    #endregion

    #region -------------------PRIVATE METHODS-------------------
    /// <summary>
    /// binding drop downs DistrictManager, Wave Information.
    /// </summary>
    private void bindDropdown()
    {
        this.ddlDistrictManager.DataTextField = "districtManager";
            this.ddlDistrictManager.DataValueField = "districtID";
            this.ddlDistrictManager.DataSource = this.dbOperation.getDistrictManager;
            this.ddlDistrictManager.DataBind();
            this.ddlDistrictManager.Items.Insert(0, new ListItem(" -- Select District -- ", "0"));
    }

    /// <summary>
    /// Binding the report depending on filter condition
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        this.ReportViewer1.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getDistrictComplianceReport(Convert.ToInt32(this.ddlDistrictManager.SelectedItem.Value), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId);
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.ReportViewer1.LocalReport.DataSources.Clear();
        this.ReportViewer1.LocalReport.DataSources.Add(rds);
        this.ReportViewer1.ShowPrintButton = false;
        if (data_tbl.Rows.Count > 0)
            this.ReportViewer1.LocalReport.ReportPath = Server.MapPath("districtCompliance.rdlc");
        data_tbl = null;
    }    
    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}
