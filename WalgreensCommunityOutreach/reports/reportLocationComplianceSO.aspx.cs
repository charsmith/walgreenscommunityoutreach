﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportLocationComplianceSO : Page
{
    #region ------------------ PROTECTED EVENTS ----------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropDown();
            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            this.reportBind();
        }
        //this.getStoreControl1.FilePath = "../search.aspx";
        //this.displayStoreSelectionToUser();
    }

    /// <summary>
    /// Event to bind the report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        this.reportBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }

    /// <summary>
    /// Store Selection refresh handler
    /// </summary>
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.bindDropDown();
        this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
        this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
        this.reportBind();
    }
    #endregion

    #region ----------------- PRIVATE METHODS ------------------

    ///// <summary>
    ///// Binding Store dropdown with out view state
    ///// </summary>
    //private void storeDropDown()
    //{
    //    DataTable user_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
    //    this.ddlStoreList.DataSource = user_stores;
    //    this.ddlStoreList.DataTextField = "address";
    //    this.ddlStoreList.DataValueField = "storeid";
    //    this.ddlStoreList.DataBind();
    //}

    ///// <summary>
    ///// Display store selection drop down to users
    ///// </summary>
    //private void displayStoreSelectionToUser()
    //{
    //    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
    //    {
    //        //this.tblStores.Visible = true;
    //        this.tdStoreSelectionDropDown.Visible = false;

    //    }
    //    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = true;
    //        this.storeDropDown();
    //        this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text).Selected = true;
    //    }
    //    else
    //    {
    //       // this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = false;
    //    }
    //}

    private void bindDropDown()
    {
        this.ddlYear.DataSource = this.appSettings.getOutreachReportYears;
        this.ddlYear.DataBind();

        if (this.ddlYear.Items.Count > 0 && this.ddlYear.Items.FindByValue((ApplicationSettings.getOutreachStartDate.Year + 1).ToString()) != null)
            this.ddlYear.SelectedValue = (ApplicationSettings.getOutreachStartDate.Year + 1).ToString();

        this.ddlQuarter.DataSource = this.appSettings.getReportYearQuarters;
        this.ddlQuarter.DataBind();
        this.ddlQuarter.SelectedValue = ApplicationSettings.getCurrentQuarter.ToString();
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;

        this.locationComplianceSOReport.ProcessingMode = ProcessingMode.Local;
        DateTime quarter_start_date = Convert.ToDateTime(@"09/01/" + this.ddlYear.SelectedItem.Text).AddYears(-1);
        data_tbl = this.dbOperation.getLocationComplianceSO(Convert.ToInt32(this.txtStoreId.Text), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, quarter_start_date, (Convert.ToInt32(this.ddlQuarter.SelectedValue) == 5 ? "0" : this.ddlQuarter.SelectedValue));
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.locationComplianceSOReport.LocalReport.DataSources.Clear();
        this.locationComplianceSOReport.LocalReport.DataSources.Add(rds);
        this.locationComplianceSOReport.ShowPrintButton = false;
        this.locationComplianceSOReport.LocalReport.ReportPath = Server.MapPath("locationComplianceSO.rdlc");
        data_tbl = null;
    }
    #endregion

    #region ----------------- PRIVATE VARIABLES ----------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion

}
