﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using TdWalgreens;
using Microsoft.Reporting.WebForms;

public partial class reportWeeklyGroupIDAssignmentsReport : Page
{
    #region ------------- PROTECTED EVENTS -------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int report_year = DateTime.Today.Year; report_year >= Convert.ToDateTime(this.groupIdLaunchDate).Year; report_year--)
            {
                this.ddlIPSeasons.Items.Add(new ListItem(report_year.ToString() + " - " + (report_year + 1).ToString(), report_year.ToString()));
            }
            if (this.ddlIPSeasons.Items.Count > 0)
                this.ddlIPSeasons.SelectedValue = (Convert.ToDateTime(this.outreachStartDate).Year).ToString();

            this.bindReportSeasonWeeks();
        }
    }

    protected void ddlIPSeasons_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.dlGroupIDAssignmentReport.DataSource = null;
        this.dlGroupIDAssignmentReport.DataBind();
        this.bindReportSeasonWeeks();
    }

    protected void lnkGroupIDAssignmentReport_Click(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] stream_ids;
        string mime_type;
        string encoding;
        string extension;
        string report_filename = string.Empty;
        string report_file_path = string.Empty;

        string report_week = ((LinkButton)sender).Text.Replace(" Group ID Assignments", "");
        report_filename = "GRP_ID_" + report_week.Split('-')[0].Trim().Replace("/", "") + "-" + report_week.Split('-')[1].Trim().Replace("/", "") + ".xls";

        FileInfo report_file = new FileInfo(Path.Combine(ApplicationSettings.reportsPath, report_filename));
        if (!report_file.Exists)
        {
            DataTable dt_group_id_report;
            dt_group_id_report = this.dbOperation.getWeeklyGroupIDAssignmentsReport(Convert.ToDateTime(report_week.Split('-')[0].Trim()), Convert.ToDateTime(report_week.Split('-')[1].Trim()).AddDays(1).AddSeconds(-1));
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "grpIDAssignmentReport";
            rds.Value = dt_group_id_report;

            ReportViewer report_viewer = new ReportViewer();
            report_viewer.ProcessingMode = ProcessingMode.Local;
            report_viewer.LocalReport.ReportPath = Server.MapPath("groupIDAssignmentsReport.rdlc");
            report_viewer.LocalReport.DataSources.Clear();
            report_viewer.LocalReport.DataSources.Add(rds);
            byte[] bytes = report_viewer.LocalReport.Render("Excel", null, out mime_type, out encoding, out extension, out stream_ids, out warnings);

            report_file_path = Path.Combine(ApplicationSettings.reportsPath, report_filename);
            using (var fs = new FileStream(report_file_path, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }
        }

        report_file = new FileInfo(Path.Combine(ApplicationSettings.reportsPath, report_filename));
        if (report_file.Exists)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + report_filename);
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Content-Length", report_file.Length.ToString());
            Response.WriteFile(report_file.FullName);
            Response.End();
        }
    }
    #endregion

    #region ------------- PRIVATE METHODS --------------
    /// <summary>
    /// Displays report season weeks links
    /// </summary>
    private void bindReportSeasonWeeks()
    {
        this.groupIdLaunchDate = Convert.ToDateTime(this.groupIdLaunchDate.Split('/')[0] + "/" + this.groupIdLaunchDate.Split('/')[1] + "/" + this.ddlIPSeasons.SelectedValue).ToString("MM/dd/yyyy");
        string selected_season=this.ddlIPSeasons.SelectedValue;
        if (selected_season == "2016" ? DateTime.Today.Date > Convert.ToDateTime(this.groupIdLaunchDate).AddMonths(3).AddDays(21) : DateTime.Today.Date > Convert.ToDateTime(this.groupIdLaunchDate))
        {
            DateTime outreach_end_date = Convert.ToDateTime(this.groupIdLaunchDate).AddYears(1).AddDays(-1);
            outreach_end_date = (outreach_end_date <= DateTime.Now) ? outreach_end_date : DateTime.Now;
            
            Dictionary<string, string> report_weeks = new Dictionary<string, string>();
            if (selected_season == "2016")
				report_weeks.Add(this.groupIdLaunchDate + "-" + this.createRequiredDate(3, 21), this.groupIdLaunchDate + " - " + Convert.ToDateTime(this.createRequiredDate(3, 21)).AddDays(-1).ToString("MM/dd/yyyy") + " Group ID Assignments");

            DateTime report_week2_end_date = Convert.ToDateTime(selected_season == "2016" ? this.createRequiredDate(3, 21) : this.createRequiredDate(0, 0)).AddDays(7);
            if (report_week2_end_date <= outreach_end_date)
            {
                while (report_week2_end_date.DayOfWeek != DayOfWeek.Monday)
                {
                    report_week2_end_date = report_week2_end_date.AddDays(1);
                }

                report_weeks.Add(Convert.ToDateTime(selected_season == "2016" ? this.createRequiredDate(3, 21) : this.createRequiredDate(0, 0)).ToString("MM/dd/yyyy") + "-" + Convert.ToDateTime(selected_season == "2016" ? this.createRequiredDate(3, 21) : this.createRequiredDate(0, 0)).AddDays(9).ToString("MM/dd/yyyy"), Convert.ToDateTime(selected_season == "2016" ? this.createRequiredDate(3, 21) : this.createRequiredDate(0, 0)).ToString("MM/dd/yyyy") + " - " + report_week2_end_date.AddDays(-1).ToString("MM/dd/yyyy") + " Group ID Assignments");
            }

            DateTime week_start_date = report_week2_end_date;
            while (week_start_date.AddDays(6) < outreach_end_date)
            {
                report_weeks.Add(week_start_date.AddDays(1).ToString("MM/dd/yyyy") + "-" + week_start_date.AddDays(7).ToString("MM/dd/yyyy"), week_start_date.ToString("MM/dd/yyyy") + " - " + week_start_date.AddDays(6).ToString("MM/dd/yyyy") + " Group ID Assignments");
                week_start_date = week_start_date.AddDays(7);
            }

            //Add last week
            if (outreach_end_date == Convert.ToDateTime(this.groupIdLaunchDate).AddYears(1).AddDays(-1))
                report_weeks.Add(week_start_date.AddDays(1).ToString("MM/dd/yyyy") + "-" + outreach_end_date.ToString("MM/dd/yyyy"), week_start_date.ToString("MM/dd/yyyy") + " - " + outreach_end_date.ToString("MM/dd/yyyy") + " Group ID Assignments");

            this.dlGroupIDAssignmentReport.DataSource = report_weeks;
            this.DataBind();
        }
    }
    private string createRequiredDate(int add_months, int add_days)
    {
        return Convert.ToDateTime(this.groupIdLaunchDate).AddMonths(add_months).AddDays(add_days).ToString("MM/dd/yyyy");
    }
    #endregion

    #region -------------- PRIVATE & PUBLIC VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    public string outreachStartDate = string.Empty;
    public string groupIdLaunchDate = string.Empty;    
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.groupIdLaunchDate = Convert.ToDateTime("05/01/2016").ToString("MM/dd/yyyy");
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
    }
    #endregion
}