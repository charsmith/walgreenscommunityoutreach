﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportOutreachMetrics.aspx.cs" Inherits="reportOutreachMetrics" EnableEventValidation="false" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>

    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="../javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#rptOutreachMetrics_ctl10").css("height", "415px");
            $("#rptOutreachMetrics_ctl09").css("height", "415px");
        });
    </script>
    <style type="text/css">
        a
        {
            color: #57a1d3;
        }
        a[disabled=disabled]
        {
            color: black;
        }
    </style> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
            <tr>
                <td colspan="2">
                    <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#FFFFFF">
                    <table width="935" border="0" cellspacing="22" cellpadding="0">
                        <tr>
                            <td valign="top" class="pageTitle">
                                <span id="lblReportTitle" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="wagsAddress">
                                <asp:Panel runat="server" ID="pnlMetricsReportLinks" ViewStateMode="Disabled"></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td width="601" valign="top" style="padding-left: 45px; padding-right: 45px; padding-bottom: 45px; ">
                                <rsweb:ReportViewer ID="rptOutreachMetrics" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3" PageCountMode="Actual" AsyncRendering="false">
                                    <LocalReport></LocalReport>
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <ucWFooter:walgreensFooter ID="walgreensFooterCtrl" runat="server" />
    </form>
</body>
</html>
