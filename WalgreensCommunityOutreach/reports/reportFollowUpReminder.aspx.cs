﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportFollowUpReminder : Page
{
    #region --------------- PROTECTED EVENTS ---------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            this.reportBind();
        }
        //this.getStoreControl1.FilePath = "../search.aspx";
        //this.displayStoreSelectionToUser();
    }    
    /// <summary>
    /// Assign some filter conditon to get the filterd output report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGoUser_Click(object sender, EventArgs e)
    {  
        this.reportBind();
        this.selectedFromDate = this.txtOutreachFromDate.Text;
        this.selectedToDate = this.txtOutreachToDate.Text;
    }

    protected void ddlStoreProfiles_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.clearFields();
    }
    /// <summary>
    /// Reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click1(object sender, EventArgs e)
    {
        this.clearFields();
    }

    /// <summary>
    /// Event to bind the report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        this.reportBind();
    }

    /// <summary>
    /// Store Selection refresh handler
    /// </summary>
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
        this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
        this.reportBind();
    }
    #endregion

    #region --------------- PRIVATE METHODS ----------------
    ///// <summary>
    ///// Binding Store dropdown with out view state
    ///// </summary>
    //private void storeDropDown()
    //{
    //    DataTable user_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
    //    this.ddlStoreList.DataSource = user_stores;
    //    this.ddlStoreList.DataTextField = "address";
    //    this.ddlStoreList.DataValueField = "storeid";
    //    this.ddlStoreList.DataBind();
    //}

    ///// <summary>
    ///// Display store selection drop down to users
    ///// </summary>
    //private void displayStoreSelectionToUser()
    //{
    //    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
    //    {
    //        //this.tblStores.Visible = true;
    //        this.tdStoreSelectionDropDown.Visible = false;

    //    }
    //    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = true;
    //        this.storeDropDown();
    //        this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text).Selected = true;
    //    }
    //    else
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = false;
    //    }
    //}

    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.reportBind();
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;

        string from_date = this.txtOutreachFromDate.Text;
        string to_date = this.txtOutreachToDate.Text;

        this.followUpReport.ProcessingMode = ProcessingMode.Local;
        if (this.txtStoreId.Text.Trim().Length != 0)
        {
            data_tbl = this.dbOperation.getFollowUpReminder(Convert.ToInt32(this.txtStoreId.Text), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, from_date.Trim().Length == 0 ? "1900-01-01" : from_date, to_date.Trim().Length == 0 ? "9999-12-31" : to_date, this.chkIncludeDateRange.Checked.ToString());

            ReportDataSource rds = new ReportDataSource();
            rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
            rds.Value = data_tbl;

            this.followUpReport.LocalReport.DataSources.Clear();
            this.followUpReport.LocalReport.DataSources.Add(rds);
            this.followUpReport.ShowPrintButton = false;
            this.followUpReport.LocalReport.ReportPath = Server.MapPath("followUp.rdlc");

            data_tbl = null;
        }
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    public string outreachStartDate = string.Empty;
    public string outreachEndDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
        this.outreachEndDate = ApplicationSettings.getOutreachEndDate.ToString("MM/dd/yyyy");
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion

}
