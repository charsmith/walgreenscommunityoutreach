﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportClinicDetails : Page
{
    #region -------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropdown();
            this.PickerAndCalendarFrom.getSelectedDate = ApplicationSettings.getDeploymentDate;
            this.PickerAndCalendarFrom.MinDate = ApplicationSettings.getDeploymentDate;
            this.PickerAndCalendarTo.getSelectedDate = DateTime.Now;

            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            this.reportBind();
        }
    }
    /// <summary>
    /// Assign some filter conditon to get the filterd output report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGoUser_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }

    protected void ddlStoreProfiles_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.clearFields();
        this.reportBind();
    }

    /// <summary>
    /// Reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.clearFields();
    }
    #endregion

    #region ------------------PRIVATE METHODS------------------
    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.PickerAndCalendarFrom.getSelectedDate = ApplicationSettings.getDeploymentDate;
        this.PickerAndCalendarTo.getSelectedDate = DateTime.Now;
        this.ddlClinicStatus.ClearSelection();
        this.reportBind();
    }

    /// <summary>
    /// Binds stores to dropdown
    /// </summary>
    private void bindDropdown()
    {
        DataTable data_tbl = new DataTable();
        DataRow[] dr = this.dbOperation.getOutreachStatus.Select("category='AC'");
        data_tbl = dr.CopyToDataTable();

        if (data_tbl.Rows.Count > 0)
        {
            this.ddlClinicStatus.DataSource = data_tbl;
            this.ddlClinicStatus.DataTextField = "outreachStatus";
            this.ddlClinicStatus.DataValueField = "pk";
            this.ddlClinicStatus.DataBind();
            this.ddlClinicStatus.Items.Insert(0, new ListItem("All", "-1"));
        }
        data_tbl = null;

        data_tbl = dbOperation.getPaymentType("EN");
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        DateTime from_date = (this.PickerAndCalendarFrom.getSelectedDate != DateTime.MinValue) ? this.PickerAndCalendarFrom.getSelectedDate : DateTime.MinValue;
        DateTime to_date = (this.PickerAndCalendarTo.getSelectedDate != DateTime.MinValue) ? this.PickerAndCalendarTo.getSelectedDate : DateTime.MinValue;
        this.storeBusinessFeedbacksReport.ProcessingMode = ProcessingMode.Local;
        //"0"- payment type,"All"- clinic type, 1- isHHSVoucher
        data_tbl = this.dbOperation.getConfirmClinicReport(Convert.ToInt32(this.commonAppSession.LoginUserInfoSession.UserID), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId,
                                                           Convert.ToInt32(ddlClinicStatus.SelectedItem.Value), from_date, to_date, "All", this.chkIncludeDateRange.Checked.ToString(), 2015, false);

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.storeBusinessFeedbacksReport.LocalReport.DataSources.Clear();
        this.storeBusinessFeedbacksReport.LocalReport.DataSources.Add(rds);
        this.storeBusinessFeedbacksReport.LocalReport.ReportPath = Server.MapPath("hhsVoucherProgram.rdlc");

        data_tbl = null;
    }
    #endregion


    #region --------- PRIVATE VARIABLES----------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}
