﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportHighLevel : Page
{
    #region ------- PROTECTED EVENTS -------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.reportBind();
        }
    }
    #endregion

    #region -------------------PRIVATE METHODS-------------------
    /// <summary>
    /// Bind data to the report.
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        this.highLevelReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getHighLevelReport(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId);
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.highLevelReport.LocalReport.DataSources.Clear();
        this.highLevelReport.LocalReport.DataSources.Add(rds);
        this.highLevelReport.ShowPrintButton = false;
        if (data_tbl.Rows.Count > 0)
            this.highLevelReport.LocalReport.ReportPath = Server.MapPath("highLevel.rdlc");
        data_tbl = null;
    }    
    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}
