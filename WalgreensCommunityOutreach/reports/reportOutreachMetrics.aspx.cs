﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using TdWalgreens;

public partial class reportOutreachMetrics : System.Web.UI.Page
{
    #region --------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            this.reportBind();
    }

    protected void lnkRptMetrics_Command(object sender, CommandEventArgs e)
    {
        this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId = Convert.ToInt32(e.CommandArgument);
        this.reportBind();
    }
    #endregion

    #region --------------- PRIVATE FUNCTION ----------------
    /// <summary>
    /// Creates metrics reports links
    /// </summary>
    private void createMetricsReportsLinks()
    {
        this.pnlMetricsReportLinks.Controls.Clear();
        DataTable dt_metrics = this.commonAppSession.SelectedStoreSession.MetricsCounts;
        foreach (DataRow dr_row in dt_metrics.Rows)
        {
            LinkButton lnk_metric_rpt = new LinkButton();
            lnk_metric_rpt.Text = dr_row["MetricDesc"].ToString();
            lnk_metric_rpt.ID = "lnkRptMetrics" + dr_row["MetricId"].ToString();
            lnk_metric_rpt.CommandArgument = dr_row["MetricId"].ToString();
            lnk_metric_rpt.Command += new CommandEventHandler(lnkRptMetrics_Command);
            lnk_metric_rpt.Enabled = true;

            this.pnlMetricsReportLinks.Controls.Add(lnk_metric_rpt);
            if ((pnlMetricsReportLinks.Controls.Count + 1) / 2 <= dt_metrics.Rows.Count - 1)
                this.pnlMetricsReportLinks.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;"));
        }
    }

    /// <summary>
    /// Generates report for the selected metric
    /// </summary>
    private void reportBind()
    {
        this.lblReportTitle.InnerHtml = ((LinkButton)pnlMetricsReportLinks.FindControl("lnkRptMetrics" + this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId.ToString())).Text;
        ((LinkButton)pnlMetricsReportLinks.FindControl("lnkRptMetrics" + this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId.ToString())).Enabled = false;

        DataTable dt_metrics_report = new DBOperations().getOutreachMetricsReport(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId);

        this.rptOutreachMetrics.ProcessingMode = ProcessingMode.Local;
        this.rptOutreachMetrics.LocalReport.DataSources.Clear();
        ReportDataSource rds = new ReportDataSource();

        if (this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId == 1 || this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId == 4)
        {
            this.rptOutreachMetrics.LocalReport.ReportPath = Server.MapPath("scheduledClinicsMetrics.rdlc");
            rds.Name = "tdWalgreensDataset_ScheduledClinicsMetrics";
        }
        else if (this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId == 2 || this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId == 3)
        {
            this.rptOutreachMetrics.LocalReport.ReportPath = Server.MapPath("contractClinicsMetrics.rdlc");
            rds.Name = "tdWalgreensDataset_ContractClinicsMetrics";
        }

        rds.Value = dt_metrics_report;
        this.rptOutreachMetrics.LocalReport.DataSources.Add(rds);

        ReportParameter parameter = new ReportParameter("reportId", this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId.ToString(), false);
        this.rptOutreachMetrics.ShowPrintButton = false;
        this.rptOutreachMetrics.LocalReport.SetParameters(new ReportParameter[] { parameter });
        
        dt_metrics_report = null;
    }
    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.createMetricsReportsLinks();
    }
    #endregion
}