﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportDistrictCompletion : Page
{
    #region ---------------- PROTECTED EVENTS ------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropdown();
        }

        this.reportBind();
    }
    #endregion

    #region -------------------PRIVATE METHODS-------------------
    /// <summary>
    /// Binding drop down information.
    /// </summary>
    private void bindDropdown()
    {
        this.ddlDistrictManager.DataTextField = "districtManager";
            this.ddlDistrictManager.DataValueField = "districtID";
            this.ddlDistrictManager.DataSource = this.dbOperation.getDistrictManager;
            this.ddlDistrictManager.DataBind();
            this.ddlDistrictManager.Items.Insert(0, new ListItem(" -- Select District -- ", "0"));
    }
    /// <summary>
    /// Report Bind depending on filter condition.
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        this.districtCompletionReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getDistrictCompletionReport(Convert.ToInt32(this.ddlDistrictManager.SelectedItem.Value), this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId);
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_DistrictCompletion";
        rds.Value = data_tbl;

        this.districtCompletionReport.LocalReport.DataSources.Clear();
        this.districtCompletionReport.LocalReport.DataSources.Add(rds);
        this.districtCompletionReport.ShowPrintButton = false;
        if (data_tbl.Rows.Count > 0)
            this.districtCompletionReport.LocalReport.ReportPath = Server.MapPath("districtCompletion.rdlc");
        data_tbl = null;
    }    
    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}
