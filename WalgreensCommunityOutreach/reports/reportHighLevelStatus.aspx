﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportHighLevelStatus.aspx.cs" Inherits="reportHighLevelStatus" %>
<%@ Register src="../controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />    
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <link href="../css/calendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" /> 
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        var minOutreachDate = new Date("<%=outreachStartDate %>");
        var maxOutreachDate = new Date("<%=outreachEndDate %>");
        var selectedFromDate = "<%=selectedFromDate %>";
        var selectedToDate = "<%=selectedToDate %>";

        $(document).ready(function () {
            $("#txtOutreachFromDate").attr("readonly", "readonly");
            $("#txtOutreachToDate").attr("readonly", "readonly");
            $("#VisibleReportContenthighLevelStatusReport_ctl10").attr("width", "620px");
            $("#VisibleReportContenthighLevelStatusReport_ctl09").attr("width", "620px");
            createDateCalendar();

            dropdownRepleceText("ddlDistrictNames", "txtDistrictNames");

            $("#btnGoUser").click(function () {
                if (new Date($("#txtOutreachFromDate").val()) > new Date($("#txtOutreachToDate").val())) {
                    alert('"From Date" should be less than "To Date"');
                    createDateCalendar();
                    return false;
                }
            });

            $("#btnRefresh").click(function () {
                selectedFromDate = "", selectedToDate = "";
                createDateCalendar();
            });

            $("#btnReset").click(function () {
                selectedFromDate = "", selectedToDate = "";
                createDateCalendar();
            });

            $("#txtStoreId").keypress(function (event) {
                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });
        });

        function createDateCalendar() {
            $("#txtOutreachFromDate").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                maxDate: maxOutreachDate
            }).datepicker("setDate", minOutreachDate);
            $("#txtOutreachFromDate").datepicker("option", "minDate", minOutreachDate);

            $("#txtOutreachToDate").datepicker({
                showOn: "button",
                buttonImage: "../images/btn_calendar.gif",
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                maxDate: maxOutreachDate
            }).datepicker("setDate", maxOutreachDate);
            $("#txtOutreachToDate").datepicker("option", "minDate", minOutreachDate);

            if (selectedFromDate != "" && selectedToDate != "") {
                $("#txtOutreachFromDate").val(selectedFromDate);
                $("#txtOutreachToDate").val(selectedToDate);
            }
            else {
                $("#txtScheduledOnDateFrom").datepicker({ defaultDate: minOutreachDate });
                $("#txtScheduledOnDateTo").datepicker({ defaultDate: maxOutreachDate });
            }

            $(".ui-datepicker-trigger").css("margin-bottom", "-6px");
            $(".ui-datepicker-trigger").css("padding-left", "5px");
        }
    </script> 
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <style type="text/css">
        .ui-widget
        {
            font-size: 11px;
        }
       .cart-calendar-month
        {
            font-size: 11px;
        }
        </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    <table width="935" border="0" cellspacing="22" cellpadding="0">
        <tr>
            <td valign="top" class="pageTitle"><asp:Label runat="server" ID="lblHeading"></asp:Label></td>
        </tr>
        <tr >
            <td align="left">
                <table cellspacing="5" cellpadding="0" width="55%">
                    <tr>
                    <td width="128px">
                        <span class="logSubTitles"><asp:Label runat="server" Text= "Districts:" ID="lblDistricts"></asp:Label></span>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList runat="server" ID="ddlDistrictNames"  CssClass="formFields" Width="165px" AutoPostBack="true" 
                            onselectedindexchanged="ddlDistrictNames_SelectedIndexChanged" ></asp:DropDownList>
                        <asp:TextBox id="txtDistrictNames" CssClass="formFields" visible="true" runat="server"  Width="163px"></asp:TextBox></td>
                </tr>
                <tr id="rowDateFilters" runat="server" >
                  <td colspan="2">
                    <table cellspacing="5" cellpadding="0" width="450px">
                      <tr>
                        <td width="128px" class="logSubTitles">From date:</td>
                        <td align="left" valign="bottom" >
                            <asp:TextBox ID="txtOutreachFromDate" CssClass="formFields" Text="" runat="server" Width="80px"  ></asp:TextBox>
                        </td> 
                        <td align="right" class="logSubTitles">To date:</td>
                        <td align="left" valign="bottom" class="formFields">
                            <asp:TextBox ID="txtOutreachToDate" CssClass="formFields" Text="" runat="server" Width="80px"></asp:TextBox>
                        </td>
                      </tr>
                      <tr style="padding-top: 15px;">
                        <td>&nbsp;</td>
                        <td colspan="3">
                            <asp:Button ID="btnGoUser" runat="server" OnClick="btnGoUser_Click" Text="Show Report" CssClass="logSubTitles" />&nbsp;
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="logSubTitles" OnClick="btnReset_Click1" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td width="628px" valign="top" style="padding:45px"> 
                <rsweb:ReportViewer ID="highLevelStatusReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="623px" Height="450px" TabIndex="3" PageCountMode="Actual" AsyncRendering="false"></rsweb:ReportViewer>
            </td>
        </tr>
    </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
<script type="text/javascript">
    if ($.browser.webkit) {
        $("#highLevelStatusReport table").each(function (i, item) {
            $(item).css('display', 'inline-block');
        });
    }
</script>    
</body>
</html>
