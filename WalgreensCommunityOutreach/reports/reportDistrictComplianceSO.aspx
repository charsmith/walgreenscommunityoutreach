﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportDistrictComplianceSO.aspx.cs" Inherits="reportDistrictCompliance" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register src="../controls/walgreensHeader.ascx" tagname="walgreensHeader" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />    
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" /> 
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            dropdownRepleceText("ddlDistrictManager", "txtDistrictManager");
            dropdownRepleceText("ddlYear", "txtYear");
            dropdownRepleceText("ddlQuarter", "txtQuarter");

//            $("#ReportFrameReportViewer1").css("height", "80%");
//            $("#districtComplianceSOReport_ctl10").css("height", "60%");
//            $("#districtComplianceSOReport_ctl10").css("width", "850px");
//            $("#districtComplianceSOReport_ctl05").css("height", "60%");
//            $("#districtComplianceSOReport_ctl05").css("width", "850px");

            $("#txtDistrictManager").hide();
            $("#txtStoreId").keypress(function (event) {

                var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (Key == 8 || Key == 13 || (Key >= 48 && Key <= 57)) return true;
                else return false;
            });

            $("#btnGo").click(function () {
                var storeId = $("#txtStoreId").val();
                if (storeId.length == 0) {
                    $("#txtStoreId").focus();
                    alert("Valid store ID is required.");
                    return false;
                }
            });
        });
    </script>         
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" >
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
 <tr>
    <td colspan="2">
        <uc2:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
    
     <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                      <td valign="top" class="pageTitle">District Report (Compliance)</td>
                    </tr>                   
                    <tr>
                      <td valign="top" align="left" style="padding-left:45px; ">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="note" >
                            <tr>
                                    <td align="left" class="formFields" colspan="3"><p>This compliance report will report Senior Events in the quarter that the event was scheduled, not in the quarter of the event date.</p></td>
                           </tr>
                         </table>
                                
                          <table width="60%" border="0" cellspacing="5" cellpadding="0" runat="server" id="tblStores" >
                                <tr>                                
                                    <td align="left" valign="top" width="10%">
                                        <span class="logSubTitles">District: </span>
                                    </td>
                                    <td>
                                        <asp:TextBox id="txtDistrictManager" CssClass="formFields" visible="true" runat="server" Width="250px" ></asp:TextBox>
                                        <asp:DropDownList ID="ddlDistrictManager" runat="server" class="formFields" Width="252px" ></asp:DropDownList>                                       
                                    </td>                                   
                                </tr>  
                                <tr>
                                <td class="logSubTitles">Year:</td>
                                <td>
                                    <table width="100%" border="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlYear" CssClass="formFields" runat="server" Width="100px" ></asp:DropDownList>
                                            <asp:TextBox CssClass="formFields" ID="txtYear" Width="98px" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" valign="top" class="logSubTitles">
                                            Quarter:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlQuarter" runat="server" DataTextField="Value" DataValueField="Key" Width="150px" ></asp:DropDownList>
                                            <asp:TextBox CssClass="formFields" ID="txtQuarter" Width="98px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="logSubTitles" Text="Submit" OnClick="btnSubmit_Click" />
                                        </td>
                                    </tr>
                                </table>
                                </td>
                                </tr>                                                
                            </table>
                        </td>
                    </tr>                   
                    <tr>
                        <td width="601" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;">
                            <rsweb:ReportViewer ID="districtComplianceSOReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3">                                   
                            </rsweb:ReportViewer>   
                        </td>
                    </tr>
                </table>

    </td>
  </tr>
</table>
</form>
<script type="text/javascript">
    if ($.browser.webkit) {
        $("#districtComplianceSOReport table").each(function (i, item) {
            $(item).css('display', 'inline-block');
        });
    }
</script>       
</body>
</html>