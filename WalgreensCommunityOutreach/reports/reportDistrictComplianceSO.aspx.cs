﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;

public partial class reportDistrictCompliance : Page
{
    #region --------------PROTECTED EVENTS -------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropdown();
            this.reportBind();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }

    #endregion

    #region -------------------PRIVATE METHODS-------------------
    /// <summary>
    /// binding drop downs DistrictManager, Wave Information.
    /// </summary>
    private void bindDropdown()
    {
        this.ddlDistrictManager.DataTextField = "districtManager";
        this.ddlDistrictManager.DataValueField = "districtID";
        this.ddlDistrictManager.DataSource = this.dbOperation.getDistrictManager;
        this.ddlDistrictManager.DataBind();
        this.ddlDistrictManager.Items.Insert(0, new ListItem(" -- Select District -- ", "0"));

        if (this.ddlDistrictManager.Items.Count > 0)
            this.ddlDistrictManager.Items[1].Selected = true;

        this.ddlYear.DataSource = this.appSettings.getOutreachReportYears;
        this.ddlYear.DataBind();

        if (this.ddlYear.Items.Count > 0)
            this.ddlYear.SelectedValue = (ApplicationSettings.getOutreachStartDate.Year + 1).ToString();

        this.ddlQuarter.DataSource = this.appSettings.getReportYearQuarters;
        this.ddlQuarter.DataBind();
        this.ddlQuarter.SelectedValue = ApplicationSettings.getCurrentQuarter.ToString();
    }

    /// <summary>
    /// Binding the report depending on filter condition
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        this.districtComplianceSOReport.ProcessingMode = ProcessingMode.Local;
        DateTime quarter_start_date = Convert.ToDateTime(@"09/01/" + this.ddlYear.SelectedItem.Text).AddYears(-1);
        data_tbl = this.dbOperation.getDistrictComplianceReportSO(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, quarter_start_date, ddlQuarter.SelectedValue, ddlDistrictManager.SelectedItem.Value);

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = data_tbl;

        this.districtComplianceSOReport.LocalReport.DataSources.Clear();
        this.districtComplianceSOReport.LocalReport.DataSources.Add(rds);
        this.districtComplianceSOReport.ShowPrintButton = false;
        if (data_tbl.Rows.Count > 0)
            this.districtComplianceSOReport.LocalReport.ReportPath = Server.MapPath("districtComplianceSO.rdlc");
        data_tbl = null;
    }
    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
    }
    #endregion

}
