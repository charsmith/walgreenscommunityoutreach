﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using TdWalgreens;

public partial class reports_reportVaccineExceptionsReport : System.Web.UI.Page
{
    #region ------------- PROTECTED EVENTS -------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int report_year = Convert.ToDateTime(this.outreachStartDate).Year; report_year >= Convert.ToDateTime(this.groupIdLaunchDate).Year; report_year--)
            {
                this.ddlIPSeasons.Items.Add(new ListItem(report_year.ToString() + " - " + (report_year + 1).ToString(), report_year.ToString()));
            }

            if (this.ddlIPSeasons.Items.Count > 0)
                this.ddlIPSeasons.SelectedValue = (Convert.ToDateTime(this.outreachStartDate).Year).ToString();

            this.reportBind();
        }
    }

    /// <summary>
    /// Displayes report data for selected filter conditions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        this.reportBind();
        
    }

    /// <summary>
    /// Removes filters and resets report data to default
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }

    #endregion

    #region ------------- PRIVATE METHODS --------------
    /// <summary>
    /// Bind data to the report.
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        this.vaccinePurchasingReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getVaccinePurchasingExceptionDetailsReport((Convert.ToInt32(this.ddlIPSeasons.SelectedValue) != Convert.ToInt32(Convert.ToDateTime(this.outreachStartDate).Year) ? 0 : 1));

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "vaccinePurchasingDataSet";
        rds.Value = data_tbl;

        this.vaccinePurchasingReport.LocalReport.DataSources.Clear();
        this.vaccinePurchasingReport.LocalReport.DataSources.Add(rds);
        this.vaccinePurchasingReport.ShowPrintButton = false;
        this.vaccinePurchasingReport.LocalReport.ReportPath = Server.MapPath("revisedClinicDatesAndVolumes.rdlc");
        data_tbl = null;
    }
    /// <summary>
    /// Returns report weekly date ranges
    /// </summary>
    /// <param name="add_months"></param>
    /// <param name="add_days"></param>
    /// <returns></returns>
    private string createRequiredDate(int add_months, int add_days)
    {
        return Convert.ToDateTime(this.outreachStartDate).AddMonths(add_months).AddDays(add_days).ToString("MM/dd/yyyy");
    }
    #endregion

    #region ------------- PRIVATE & PUBLIC VARIABLES -------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    public string outreachStartDate = string.Empty;
    public string groupIdLaunchDate = string.Empty;
    public string selectedFromDate = string.Empty;
    public string selectedToDate = string.Empty;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
        this.groupIdLaunchDate = Convert.ToDateTime("05/01/2017").ToString("MM/dd/yyyy");
        this.outreachStartDate = ApplicationSettings.getOutreachStartDate.ToString("MM/dd/yyyy");
    }
    #endregion
}