﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportScheduledEvents.aspx.cs" Inherits="reportScheduledEvents" %>
<%@ Register src="../controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" /> 
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="../javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            dropdownRepleceText("ddlYear", "txtYear");
            dropdownRepleceText("ddlQuarter", "txtQuarter");

            $("#ReportFrameReportViewer1").css("height", "80%");
            $("#scheduledEventsReport_ctl10").css("width", "850px");
            $("#scheduledEventsReport_ctl10").css("height", "450px");
            $("#scheduledEventsReport_ctl09").css("width", "850px");
            $("#scheduledEventsReport_ctl09").css("height", "450px");
    });
    </script>

    <style type="text/css">
        .cart-calendar-month
        {
            font-size: 11px;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">    
      <table width="935" border="0" cellspacing="22" cellpadding="0">
        <tr>
            <td valign="top" class="pageTitle">Scheduled Events Report</td>
        </tr>
        <tr>
            <td align="left">
                <table width="65%" border="0" cellspacing="5" cellpadding="0" align="left">
                    <tr>
                        <td align="left" valign="top" class="logSubTitles" width="10%">Year:</td>
                        <td align="left" valign="top" width="25%">
                            <asp:DropDownList ID="ddlYear" CssClass="formFields" runat="server" Width="100px" ></asp:DropDownList>
                            <asp:TextBox CssClass="formFields" ID="txtYear" Width="98px" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" valign="top" class="logSubTitles" width="10%">Quarter:</td>
                        <td align="left" valign="top" width="25%">
                            <asp:DropDownList ID="ddlQuarter" runat="server" DataTextField="Value" DataValueField="Key" Width="150px" ></asp:DropDownList>
                            <asp:TextBox CssClass="formFields" ID="txtQuarter" Width="98px" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" valign="top" width="30%">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="logSubTitles" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="601" valign="top" style="padding-left:45px; padding-right:45px; padding-bottom:45px;">
                <rsweb:ReportViewer ID="scheduledEventsReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="850px" Height="450px" TabIndex="3" PageCountMode="Actual" AsyncRendering="false"></rsweb:ReportViewer>
            </td>
        </tr>
     </table>
    </td>
  </tr>
</table>
<ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
 <script type="text/javascript">
     if ($.browser.webkit) {
         $("#scheduledEventsReport table").each(function (i, item) {
             $(item).css('display', 'inline-block');
         });
     }
</script>    
</body>
</html>
