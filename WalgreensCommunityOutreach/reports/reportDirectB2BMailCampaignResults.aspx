﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportDirectB2BMailCampaignResults.aspx.cs" Inherits="reportDirectB2BMailCampaignResults" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/theme.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
        <style type="text/css">
        .ui-widget
        {
            font-size: 11px;
        }
        
        .cart-calendar-month
        {
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        function downloadCSVReport() {
            __doPostBack("typeCSV", "csv");
            theForm.__EVENTARGUMENT.value = "";
        }
        function downloadExcelReport() {
            __doPostBack("typeExcel", "excel");
            theForm.__EVENTARGUMENT.value = "";
        }

        $(document).ready(function () {
            $("#rptDirectB2BMailCampaignResults_ctl10").css("height", "415px");
            $("#rptDirectB2BMailCampaignResults_ctl09").css("height", "415px");
            $("#rptDirectB2BMailCampaignResults_ctl10").css("width", "800px");
            $("#rptDirectB2BMailCampaignResults_ctl09").css("width", "800px");

            $("#rptDirectB2BMailCampaignResults_fixedTable")[0].style.display = null;

            if ($("[id^='rptDirectB2BMailCampaignResults'][id$='_Menu']") != null && $("[id^='rptDirectB2BMailCampaignResults'][id$='_Menu']") != undefined) {
                $("[id^='rptDirectB2BMailCampaignResults'][id$='_Menu']").find('div').first().remove();
                $("[id^='rptDirectB2BMailCampaignResults'][id$='_Menu']").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadExcelReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>Excel</a></div>");
                $("[id^='rptDirectB2BMailCampaignResults'][id$='_Menu']").prepend("<div style='border:1px transparent Solid;'><a onclick='downloadCSVReport();' href='javascript:void(0)' style='color:#3366CC;font-family:Verdana;font-size:8pt;padding:3px 8px 3px 8px;display:block;white-space:nowrap;text-decoration:none;'>CSV</a></div>");
            }            
        });

    </script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
<form id="form1" runat="server" defaultbutton="btnShowReport">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>    
<table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
  <tr>
    <td colspan="2">
        <ucWHeader:walgreensheader ID="walgreensHeaderCtrl" runat="server" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF">
      <table width="935" border="0" cellspacing="22" cellpadding="0">
        <tr>
            <td valign="top" class="pageTitle">Direct B2B Mail Campaign Results</td>
        </tr>
        <tr>
        <td width="850px" valign="top" style="padding-left: 45px;">
            <table width="80%" border="0" cellspacing="5" cellpadding="0">
                <tr><td></td></tr>
                <tr>
                    <td colspan="3">
                    <fieldset>
                    <legend class="logSubTitles">Filter Report</legend>
                        <table width="75%">
                          
                        <tr>
                            <td style="width: 100px; text-align: right;" class="logSubTitles">Outreach Status:</td>
                            <td colspan="3">
                                <asp:DropDownList CssClass="formFields" ID="ddlOutreachStatus" runat="server" Width="200px" ></asp:DropDownList>
                                <asp:Button ID="btnShowReport" runat="server" OnClick="btnShowReport_Click" Text="Show Report" class="logSubTitles"/>&nbsp;
                                <asp:Button ID="btnReset" runat="server" Text="Reset" class="logSubTitles" onclick="btnReset_Click"/>
                            </td>
                        </tr>
                        </table>
                    </fieldset>
                    </td>
                    </tr>
                </table>
        </td>
        </tr>
        <tr>
            <td width="850px" valign="top" style="padding-left: 45px; padding-right: 45px; padding-bottom: 45px; ">
                <rsweb:ReportViewer ID="rptDirectB2BMailCampaignResults" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="800px" Height="450px" TabIndex="3" PageCountMode="Actual"></rsweb:ReportViewer>
            </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
    <ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
</form>
 <script type="text/javascript">
     if ($.browser.webkit) {
         $("#rptDirectB2BMailCampaignResults table").each(function (i, item) {
             $(item).css('display', 'inline-block');
         });
     }
</script>    
</body>
</html>
