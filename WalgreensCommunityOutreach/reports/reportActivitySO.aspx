﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportActivitySO.aspx.cs"
    Inherits="reportActivitySO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="../controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="../controls/walgreensHeader.ascx" TagName="walgreensheader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../themes/jquery-ui-1.8.17.custom.css" />
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="../javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            dropdownRepleceText("ddlFilterReportBy", "txtFilterReportBy");
            dropdownRepleceText("ddlMarkets", "txtMarkets");
            dropdownRepleceText("ddlAreas", "txtAreas");            
            dropdownRepleceText("ddlDistricts", "txtDistricts");
            dropdownRepleceText("ddlMarketDistricts", "txtMarketDistricts");
            dropdownRepleceText("ddlYear", "txtYear");
            dropdownRepleceText("ddlQuarter", "txtQuarter");

            $("#ReportFrameReportViewer1").css("height", "80%");
            $("#VisibleReportContentactivityReportSO_ctl09").css("height", "450px");
            $("#VisibleReportContentactivityReportSO_ctl10").css("height", "450px");
            $("#activityReportSO_ctl10").css("width", "780px");
            $("#activityReportSO_ctl09").css("width", "780px");

        });                 
    </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensheader id="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0">
                    <tr>
                        <td valign="top" class="pageTitle">Activity Report</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="5" cellpadding="0" align="left">
                                <tr runat="server" id="rowReportFilterBy" visible="false">
                                    <td>
                                        <table width="695px" border="0" cellspacing="5" cellpadding="0">
                                            <tr>
                                                <td style="text-align: left; vertical-align: top; width: 75px;" nowrap="nowrap" class="logSubTitles">Filter By:</td>
                                                <td style="text-align: left; vertical-align: top; width: 160px;" nowrap="nowrap">
                                                    <asp:DropDownList ID="ddlFilterReportBy" runat="server" CssClass="formFields" Width="150px" AutoPostBack="false"></asp:DropDownList>
                                                    <asp:TextBox CssClass="formFields" ID="txtFilterReportBy" Width="98px" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="text-align: left; vertical-align: top; width: 460px;" nowrap="nowrap" >&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="rowReportFilters" visible="false">
                                    <td>
                                        <table width="695px" border="0" cellspacing="5" cellpadding="0">
                                            <tr>
                                                <td width="240px" runat="server" id="regionSelection" visible="false" >
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                        <td style="text-align: left; vertical-align: top; width: 80px;" nowrap="nowrap" class="logSubTitles">Region:</td>
                                                        <td style="text-align: left; vertical-align: top; width: 160px;" nowrap="nowrap">
                                                            <asp:DropDownList ID="ddlMarkets" CssClass="formFields" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlMarkets_OnChanged"></asp:DropDownList>
                                                            <asp:TextBox CssClass="formFields" ID="txtMarkets" Width="150px" runat="server"></asp:TextBox>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                </td>
                                                <td width="240px" runat="server" id="areaSelection" visible="false" >
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                        <td style="text-align: left; vertical-align: top; width: 80px;" nowrap="nowrap" class="logSubTitles">Area:</td>
                                                        <td style="text-align: left; vertical-align: top; width: 160px;" nowrap="nowrap">
                                                            <asp:DropDownList ID="ddlAreas" CssClass="formFields" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlAreas_OnChanged"></asp:DropDownList>
                                                            <asp:TextBox CssClass="formFields" ID="txtAreas" Width="150px" runat="server"></asp:TextBox>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                </td>                                         
                                                <td runat="server" id="districtSelection" visible="false" >
                                                    <table width="260px" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                        <td style="text-align: left; vertical-align: top; width: 70px;" nowrap="nowrap" class="logSubTitles">District:</td>
                                                        <td style="text-align: left; vertical-align: top; width: 160px;" nowrap="nowrap">
                                                            <asp:DropDownList ID="ddlDistricts" CssClass="formFields" runat="server" Width="150px"></asp:DropDownList>
                                                            <asp:TextBox CssClass="formFields" ID="txtDistricts" Width="150px" runat="server"></asp:TextBox>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                </td>                                                
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <table width="695px" border="0" cellspacing="5" cellpadding="0" align="left">
                                            <tr>
                                                <td style="text-align: left; vertical-align: top; width: 75px;" nowrap="nowrap" class="logSubTitles">Fiscal Year:</td>
                                                <td style="text-align: left; vertical-align: top; width: 160px;" nowrap="nowrap">
                                                    <asp:DropDownList ID="ddlYear" CssClass="formFields" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_OnChanged"></asp:DropDownList>
                                                    <asp:TextBox CssClass="formFields" ID="txtYear" Width="100px" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="text-align: left; vertical-align: top; width: 75px;" nowrap="nowrap" class="logSubTitles">Time Period:</td>
                                                <td style="text-align: left; vertical-align: top; width: 160px;" nowrap="nowrap">
                                                    <asp:DropDownList ID="ddlQuarter" runat="server" Width="150px"></asp:DropDownList>
                                                    <asp:TextBox CssClass="formFields" ID="txtQuarter" Width="150px" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="text-align: left; vertical-align: top; width: 225px;" nowrap="nowrap" colspan="2">
                                                    <asp:Button ID="btnSubmit" runat="server" CssClass="logSubTitles" Text="Submit" OnClick="btnSubmit_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="750px" valign="top" style="padding-bottom: 45px; padding-top: 10px; padding-left: 10px; padding-right: 25px">
                            <rsweb:ReportViewer ID="activityReportSO" runat="server" Font-Names="Verdana" Font-Size="8pt" PageCountMode="Actual" AsyncRendering="false" Width="780px" BorderStyle="None" Height="450px" TabIndex="3">
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter id="walgreensFooterCtrl" runat="server" />
    </form>
    <script type="text/javascript">
        if ($.browser.webkit) {
            $("#activityReportSO table").each(function (i, item) {
                $(item).css('display', 'inline-block');
            });
        }
</script>
</body>
</html>
