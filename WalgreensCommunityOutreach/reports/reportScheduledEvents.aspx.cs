﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;
using TdWalgreens;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;

public partial class reportScheduledEvents : Page
{
    #region -------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindDropDown();
            this.reportBind();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        this.reportBind();
    }

    #endregion

    #region -------------- PRIVATE METHODS ---------------
    /// <summary>
    /// Binds report year and quarters options to dropdowns
    /// </summary>
    private void bindDropDown()
    {
        this.ddlYear.DataSource = this.appSettings.getOutreachReportYears;
        this.ddlYear.DataBind();

        if (this.ddlYear.Items.Count > 0)
            this.ddlYear.SelectedValue = (ApplicationSettings.getOutreachStartDate.Year + 1).ToString();

        this.ddlQuarter.DataSource = this.appSettings.getReportYearQuarters;
        this.ddlQuarter.DataBind();
        this.ddlQuarter.SelectedValue = ApplicationSettings.getCurrentQuarter.ToString();
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl = new DataTable();
        DateTime quarter_start_date = Convert.ToDateTime(@"09/01/" + this.ddlYear.SelectedItem.Text).AddYears(-1);

        this.scheduledEventsReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getScheduledEvents(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole, this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId,
                                                       quarter_start_date, this.ddlQuarter.SelectedValue, (Convert.ToInt32(this.ddlYear.SelectedValue) == ApplicationSettings.getOutreachStartDate.Year + 1 ? 1 : 0));

        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
        rds.Value = this.setNewtemplateChanges(data_tbl);

        this.scheduledEventsReport.LocalReport.DataSources.Clear();
        this.scheduledEventsReport.LocalReport.DataSources.Add(rds);
        this.scheduledEventsReport.ShowPrintButton = false;
        this.scheduledEventsReport.LocalReport.ReportPath = Server.MapPath("scheduledEvents.rdlc");

        ReportParameter parameter = new ReportParameter("year", this.ddlYear.SelectedValue, false);
        this.scheduledEventsReport.LocalReport.SetParameters(new ReportParameter[] { parameter });

        data_tbl = null;
    }
    private DataTable setNewtemplateChanges(DataTable scheduled_events)
    {
        foreach (DataRow row in scheduled_events.Rows)
        {
            if (Convert.ToDateTime(row["contactDate"].ToString()) >= ApplicationSettings.newSOTemplateDate)
            {
                row["refreshments"] = "";
            }
            else
            {
                row["provideEducationalMaterial"] = "";
            }
        }
        return scheduled_events;
    }
    #endregion

    #region ------------- PRIVATE VARIABLES --------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.appSettings = new ApplicationSettings();
    }
    #endregion
}
