﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using TdApplicationLib;
using System.IO;
using System.Data;
using System.Xml;
using TdWalgreens;

public partial class walgreensEditBusinessProfile : Page
{
    #region ----------------- PROTECTED EVENTS -----------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (ViewState["storeBusinesses"] == null)
            {
                this.dtBusinesses = this.dbOperation.getStoreBusinessesDetails(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId);
                ViewState["storeBusinesses"] = this.dtBusinesses;
            }

            this.bindLocalBusinesses();
            //this.bindStates();
            this.ddlState.bindStates();
            this.bindCheckBoxes();
            //this.bindDropDown();
            if (this.ddlEditInformation.SelectedValue == "1")
            {
                this.chkbIPEffort.Enabled = false;
                this.chkbSOEffort.Enabled = false;
                this.txtSOStore.Enabled = false;
                this.txtIPStore.Enabled = false;
            }
        }
        else
        {
            string event_target = (this.Request["__EVENTTARGET"] == null) ? string.Empty : this.Request["__EVENTTARGET"];
            if (!string.IsNullOrEmpty(event_target) && (event_target.ToLower() == "canreassignbusiness" || event_target.ToLower() == "canassigntootherstate"))
            {
                this.updateBusinessProfile(true, false);
            }
            else if (!string.IsNullOrEmpty(event_target) && event_target.ToLower() == "addbusiness")
            {
                this.updateBusinessProfile(false, false);
            }
        }
        // to hide the outreach program message every time 
        this.lblnonEditMessage.Visible = false;
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }
    #endregion

    #region ----------------- PROTECTED ACCESSORS --------------
    protected void doProcess(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLower())
        {
            case "save":
                this.updateBusinessProfile(false, true);
                break;
            case "cancel":
                Response.Redirect("walgreensHome.aspx", false);
                break;
        }
    }

    protected void ddlLocalBusinesses_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ddlEditInformation.ClearSelection();
        this.bindingDataToPage();
        this.enableDisableOutreachBlock();
    }

    protected void validatePhone(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validatePhone();
    }

    protected void validateZipCode(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validateZipCode();
    }

    protected void validateAddress(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = (this.txtAddress1.Text.Trim() + this.txtAddress2.Text.Trim()).validateAddress();
    }

    protected void ddlEditInformation_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.bindingDataToPage();
        this.enableDisableOutreachBlock();
    }

    /// <summary>
    /// Store Selection refresh handler
    /// </summary>
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.dtBusinesses = this.dbOperation.getStoreBusinessesDetails(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId);
        ViewState["storeBusinesses"] = this.dtBusinesses;

        this.bindLocalBusinesses();
        //this.bindStates();
        this.ddlState.bindStates();
        this.bindCheckBoxes();
        if (this.ddlEditInformation.SelectedValue == "1")
        {
            this.chkbIPEffort.Enabled = false;
            this.chkbSOEffort.Enabled = false;
            this.txtSOStore.Enabled = false;
            this.txtIPStore.Enabled = false;
        }
    }

    #endregion

    #region ----------------- PRIVATE METHODS ------------------
    /// <summary>
    /// Displays selected business profile information
    /// </summary>
    private void bindingDataToPage()
    {
        int business_id = 0;
        this.hdIPStore.Value = string.Empty;
        this.hdSOStore.Value = string.Empty;
        this.txtIPStore.Enabled = false;
        this.txtSOStore.Enabled = false;

        this.clearFields();
        Int32.TryParse(this.ddlLocalBusinesses.SelectedValue, out business_id);
        if (business_id > 0)
        {
            this.displaySelectedBusinessDetails(business_id);
            DataTable dt_outreach = dbOperation.getBusinessOutreach(commonAppSession.SelectedStoreSession.storeID, business_id);

            this.chkbSOEffort.Checked = false;
            this.chkbIPEffort.Checked = false;
            this.txtSOStoreReqFV.Enabled = false;
            this.txtIPStoreReqFV.Enabled = false;
            this.txtSOStore.Text = string.Empty;
            this.txtIPStore.Text = string.Empty;

            foreach (DataRow dr in dt_outreach.Rows)
            {
                if (dr["outreachPk"].ToString() == "1")
                {
                    this.chkbSOEffort.Checked = true;
                    this.txtSOStore.Text = dr["storeid"].ToString();
                    this.hdSOStore.Value = dr["storeid"].ToString();
                    this.txtSOStoreReqFV.Enabled = true;
                    this.txtSOStoreReqFV.Visible = true;
                    this.txtSOStore.Enabled = true;
                    this.chkbxHhsProgram.Enabled = false;
                    this.chkCOProgram.Enabled = false;
                }
                else if (dr["outreachPk"].ToString() == "3")
                {
                    this.chkbIPEffort.Checked = true;
                    this.txtIPStore.Text = dr["storeid"].ToString();
                    this.hdIPStore.Value = dr["storeid"].ToString();
                    this.txtIPStoreReqFV.Enabled = true;
                    this.txtIPStoreReqFV.Visible = true;
                    this.txtIPStore.Enabled = true;
                    this.chkbxHhsProgram.Enabled = (this.chkbxHhsProgram.Checked || this.chkCOProgram.Checked) ? false : true;
                    this.chkCOProgram.Enabled = (this.chkbxHhsProgram.Checked || this.chkCOProgram.Checked) ? false : true;

                    if (dr["defaultOutreachStatus"].ToString().Length != 0 && (dr["defaultOutreachStatus"].ToString().ToLower() == "charity program" || dr["defaultOutreachStatus"].ToString().ToLower() == "community outreach"))
                    {
                        this.chkbxHhsProgram.Enabled = false;
                        this.chkbxHhsProgram.Checked = dr["defaultOutreachStatus"].ToString().ToLower() == "charity program" ? true : false;
                        this.chkCOProgram.Enabled = false;
                        this.chkCOProgram.Checked = dr["defaultOutreachStatus"].ToString().ToLower() == "community outreach" ? true : false;
                    }

                }
            }
            if (!ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "RemoveOutreachProgramForBusiness"))
            {
                this.lblnonEditMessage.Visible = true;
            }
        }
    }

    /// <summary>
    /// Enables or disables controls based on outreach selected
    /// </summary>
    private void enableDisableOutreachBlock()
    {
        if (this.ddlEditInformation.SelectedValue == "1")
        {
            this.chkbIPEffort.Enabled = false;
            this.chkbSOEffort.Enabled = false;
            this.txtSOStore.Enabled = false;
            this.txtIPStore.Enabled = false;

            this.txtFirstName.Enabled = true;
            this.txtLastName.Enabled = true;
            this.txtTitle.Enabled = true;
            this.txtBusinessName.Enabled = true;
            this.txtAddress1.Enabled = true;
            this.txtAddress2.Enabled = true;
            this.txtCounty.Enabled = true;
            this.txtCity.Enabled = true;
            this.ddlState.Enabled = true;
            this.txtZip.Enabled = true;
            this.txtZip4.Enabled = true;
            this.txtPhone.Enabled = true;
            this.txtTollFree.Enabled = true;
            this.txtIndustry.Enabled = true;
            this.txtWebUrl.Enabled = true;
            this.txtEmpSize.Enabled = true;

            this.chkbxHhsProgram.Enabled = false;
        }
        else
        {
            if (chkbIPEffort.Checked && this.txtIPStore.Text.Length != 0)
                this.txtIPStore.Enabled = true;
            else
                this.txtIPStore.Enabled = false;

            if (chkbSOEffort.Checked && this.txtSOStore.Text.Length != 0)
                this.txtSOStore.Enabled = true;
            else
                this.txtSOStore.Enabled = false;

            this.chkbIPEffort.Enabled = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "RemoveOutreachProgramForBusiness") ? true : (hdIPStore.Value == "" ? true : false);
            this.chkbSOEffort.Enabled = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "RemoveOutreachProgramForBusiness") ? true : (hdSOStore.Value == "" ? true : false);

            this.txtFirstName.Enabled = false;
            this.txtLastName.Enabled = false;
            this.txtTitle.Enabled = false;
            this.txtBusinessName.Enabled = false;
            this.txtAddress1.Enabled = false;
            this.txtAddress2.Enabled = false;
            this.txtCounty.Enabled = false;
            this.txtCity.Enabled = false;
            this.ddlState.Enabled = false;
            this.txtZip.Enabled = false;
            this.txtZip4.Enabled = false;
            this.txtPhone.Enabled = false;
            this.txtTollFree.Enabled = false;
            this.txtIndustry.Enabled = false;
            this.txtWebUrl.Enabled = false;
            this.txtEmpSize.Enabled = false;
            this.chkbxHhsProgram.Enabled = false;
            this.chkCOProgram.Enabled = false;
        }
        string store_state = this.commonAppSession.SelectedStoreSession.storeState;

        if (ApplicationSettings.isRestrictedStoreState(store_state, this.commonAppSession.LoginUserInfoSession.UserRole) && !this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.txtAddress1.Enabled = false;
            this.txtAddress2.Enabled = false;
            this.txtAddress1RegEV.Enabled = false;
            this.txtAddress2RegEV.Enabled = false;
            this.txtAddress1ReqFV.Enabled = false;
            this.txtAddressCV.Enabled = false;
            this.txtCity.Enabled = false;
            this.txtCityRegEV.Enabled = false;
            this.txtCityReqFV.Enabled = false;
            this.ddlState.Enabled = false;
            this.ddlStateReqFV.Enabled = false;
            this.txtZip.Enabled = false;
            this.txtZip4.Enabled = false;
            this.txtZip4RegEV.Enabled = false;
            this.txtZipCV.Enabled = false;
            this.txtZipReqFV.Enabled = false;
        }
    }

    /// <summary>
    /// This method will be used to display selected business details
    /// </summary>
    /// <param name="business_id"></param>
    private void displaySelectedBusinessDetails(int business_id)
    {
        if (ViewState["storeBusinesses"] != null)
            this.dtBusinesses = (DataTable)ViewState["storeBusinesses"];
        else
        {
            this.dtBusinesses = this.dbOperation.getStoreBusinessesDetails(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId);
            ViewState["storeBusinesses"] = this.dtBusinesses;
        }

        if (this.dtBusinesses.Rows.Count > 0)
        {
            DataRow[] dr_business = this.dtBusinesses.Select("businessId = '" + business_id + "'");

            if (dr_business != null)
            {
                this.txtFirstName.Text = (dr_business[0]["firstName"] is DBNull) ? "" : dr_business[0]["firstName"].ToString();
                this.txtLastName.Text = (dr_business[0]["lastName"] is DBNull) ? "" : dr_business[0]["lastName"].ToString();
                this.txtTitle.Text = (dr_business[0]["jobTitle"] is DBNull) ? "" : dr_business[0]["jobTitle"].ToString();
                this.txtBusinessName.Text = (dr_business[0]["businessName"] is DBNull) ? "" : dr_business[0]["businessName"].ToString().Replace("&amp;", "&");
                this.txtAddress1.Text = (dr_business[0]["address"] is DBNull) ? "" : dr_business[0]["address"].ToString();
                this.txtAddress2.Text = (dr_business[0]["address2"] is DBNull) ? "" : dr_business[0]["address2"].ToString();
                this.txtCounty.Text = (dr_business[0]["county"] is DBNull) ? "" : dr_business[0]["county"].ToString();
                this.txtCity.Text = (dr_business[0]["city"] is DBNull) ? "" : dr_business[0]["city"].ToString();
                string state = !(dr_business[0]["state"] is DBNull) ? dr_business[0]["state"].ToString() : "";

                if (!string.IsNullOrEmpty(state))
                    this.ddlState.bindStatesWithRestriction(state, ApplicationSettings.getStoreStateRestrictions("storeState"));

                if (!string.IsNullOrEmpty(state) && this.ddlState.Items.FindByValue(state) != null)
                    this.ddlState.Items.FindByValue(state).Selected = true;

                this.txtZip.Text = (dr_business[0]["zip"] is DBNull) ? "" : dr_business[0]["zip"].ToString();
                if (dr_business[0]["zip4"].ToString() == "0")
                    this.txtZip4.Text = string.Empty;
                else
                    this.txtZip4.Text = (dr_business[0]["zip4"] is DBNull) ? "" : dr_business[0]["zip4"].ToString();
                this.txtPhone.Text = (dr_business[0]["phone"] is DBNull) ? "" : dr_business[0]["phone"].ToString();
                this.txtTollFree.Text = (dr_business[0]["tollfree"] is DBNull) ? "" : dr_business[0]["tollfree"].ToString();
                this.txtIndustry.Text = (dr_business[0]["industry"] is DBNull) ? "" : dr_business[0]["industry"].ToString();
                this.txtWebUrl.Text = (dr_business[0]["website"] is DBNull) ? "" : dr_business[0]["website"].ToString();
                this.txtEmpSize.Text = (dr_business[0]["actualLocationEmploymentSize"] is DBNull) ? "" : dr_business[0]["actualLocationEmploymentSize"].ToString();

                if (Convert.ToBoolean(dr_business[0]["isCharityInitiated"]) || Convert.ToBoolean(dr_business[0]["isCommunityInitiated"]))
                {
                    this.chkbxHhsProgram.Checked = (Convert.ToBoolean(dr_business[0]["isCharityInitiated"].ToString()));
                    this.chkCOProgram.Checked = (Convert.ToBoolean(dr_business[0]["isCommunityInitiated"].ToString()));
                    this.chkbxHhsProgram.Enabled = (this.chkbxHhsProgram.Checked || this.chkCOProgram.Checked) ? false : true;
                    this.chkCOProgram.Enabled = (this.chkbxHhsProgram.Checked || this.chkCOProgram.Checked) ? false : true;
                }
                else
                {
                    this.chkbxHhsProgram.Checked = false;
                    this.chkbxHhsProgram.Enabled = false;
                    this.chkCOProgram.Checked = false;
                    this.chkCOProgram.Enabled = true;
                }
            }
        }
    }

    /// <summary>
    /// This method will be used to bind local business
    /// </summary>
    private void bindLocalBusinesses()
    {
        //DataRow[] local_businesses = this.commonAppSession.SelectedStoreSession.getAssignedBusinesses.Select("businessType = '1'");
        //this.ddlLocalBusinesses.DataSource = local_businesses.CopyToDataTable();
        this.ddlLocalBusinesses.DataSource = (DataTable)ViewState["storeBusinesses"];
        this.ddlLocalBusinesses.DataTextField = "businessName";
        this.ddlLocalBusinesses.DataValueField = "businessId";
        this.ddlLocalBusinesses.DataBind();
        this.ddlLocalBusinesses.Items.Insert(0, new ListItem(" -- Select Local Business -- ", ""));
    }

    /// <summary>
    /// This function returns the xml structure to DB for creating the Store Profile
    /// </summary>
    /// <returns></returns>
    private XmlDocument prepareStoreProfileXml()
    {
        XmlDocument profile_doc = new XmlDocument();
        XmlElement store_profile_element = store_profile_element = profile_doc.CreateElement("storeprofile");
        XmlElement fields_element = profile_doc.CreateElement("fields");

        this.userProfileElement("pk", this.ddlLocalBusinesses.SelectedValue, true, ref fields_element, ref profile_doc);
        this.userProfileElement("firstName", this.txtFirstName.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("lastName", this.txtLastName.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("jobTitle", this.txtTitle.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("businessName", this.txtBusinessName.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("address", this.txtAddress1.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("address2", this.txtAddress2.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("county", this.txtCounty.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("city", this.txtCity.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("state", ddlState.SelectedValue.Trim(), true, ref fields_element, ref profile_doc);
        this.userProfileElement("zip", this.txtZip.Text.Trim(), true, ref fields_element, ref profile_doc);
        this.userProfileElement("zip4", this.txtZip4.Text.Trim(), true, ref fields_element, ref profile_doc);
        this.userProfileElement("phone", this.txtPhone.Text.Trim().Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", ""), true, ref fields_element, ref profile_doc);
        this.userProfileElement("tollfree", this.txtTollFree.Text.Trim(), true, ref fields_element, ref profile_doc);
        this.userProfileElement("industry", this.txtIndustry.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("website", this.txtWebUrl.Text.Trim().Replace("'", "`"), true, ref fields_element, ref profile_doc);
        this.userProfileElement("actualLocationEmploymentSize", this.txtEmpSize.Text.Trim(), true, ref fields_element, ref profile_doc);
        this.userProfileElement("userPk", this.commonAppSession.LoginUserInfoSession.UserID.ToString(), true, ref fields_element, ref profile_doc);
        this.userProfileElement("existingStoreID", this.commonAppSession.SelectedStoreSession.storeID.ToString(), true, ref fields_element, ref profile_doc);
        this.userProfileElement("immunizationID", "3", true, ref fields_element, ref profile_doc);
        this.userProfileElement("seniorOutreachID", "1", true, ref fields_element, ref profile_doc);

        store_profile_element.AppendChild(fields_element);
        profile_doc.AppendChild(store_profile_element);

        return profile_doc;
    }

    /// <summary>
    /// Creates the business information field element for profile XML document
    /// </summary>
    /// <param name="field_name"></param>
    /// <param name="field_value"></param>
    /// <param name="is_table_column"></param>
    /// <param name="fields_element"></param>
    /// <param name="profile_doc"></param>
    private void userProfileElement(string field_name, string field_value, bool is_table_column, ref XmlElement fields_element, ref XmlDocument profile_doc)
    {
        XmlElement create_store;
        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", field_name);
        create_store.SetAttribute("value", field_value);
        if (!is_table_column)
            create_store.SetAttribute("isTableColumn", "0");

        switch (field_name)
        {
            case "immunizationID":
                create_store.SetAttribute("checked", (this.chkbIPEffort.Checked && !string.IsNullOrEmpty(txtIPStore.Text)) ? "true" : "false");
                create_store.SetAttribute("existingStoreID", this.hdIPStore.Value.Trim());
                create_store.SetAttribute("newStoreID", txtIPStore.Text.Trim());
                create_store.SetAttribute("isHHSVoucher", (this.chkbxHhsProgram.Checked) ? "1" : "0");
                create_store.SetAttribute("isCommunityOutReach", (this.chkCOProgram.Checked) ? "1" : "0");
                create_store.SetAttribute("removeContactLog", (string.IsNullOrEmpty(hdImmunizationRemove.Value)) ? "false" : "true");
                break;
            case "seniorOutreachID":
                create_store.SetAttribute("checked", (this.chkbSOEffort.Checked && !string.IsNullOrEmpty(txtSOStore.Text)) ? "true" : "false");
                create_store.SetAttribute("existingStoreID", this.hdSOStore.Value.Trim());
                create_store.SetAttribute("newStoreID", txtSOStore.Text.Trim());
                create_store.SetAttribute("isHHSVoucher", "0");
                create_store.SetAttribute("isCommunityOutReach", "0");
                create_store.SetAttribute("removeContactLog", (string.IsNullOrEmpty(hdSeniorOutreachRemove.Value)) ? "false" : "true");
                break;
        }
        fields_element.AppendChild(create_store);
    }

    /// <summary>
    /// Update business profile
    /// </summary>
    /// <param name="can_reassign_business"></param>
    /// <param name="is_check_dnc"></param>
    private void updateBusinessProfile(bool can_reassign_business, bool is_check_dnc)
    {
        int return_value = 0;
        int contactlog_pk = 0;
        string error_message = string.Empty;
        string store_state = string.Empty;
        store_state = this.commonAppSession.SelectedStoreSession.storeState;

        if (ApplicationSettings.isRestrictedStoreState(store_state, this.commonAppSession.LoginUserInfoSession.UserRole) && !can_reassign_business)
        {
            if (string.IsNullOrEmpty(this.hdIPStore.Value) && this.chkbIPEffort.Checked && !string.IsNullOrEmpty(this.txtIPStore.Text))
            {
                if (this.commonAppSession.SelectedStoreSession.storeID == Convert.ToInt32(this.txtIPStore.Text))
                {
                    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showAddBusinessDisableToMOstateAlert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "disableAddBusinessToStore"), (store_state == "MO" ? "Missouri" : "District of Columbia"), (store_state == "MO" ? "20" : "15")) + "');", true);
                    }
                    else
                    {
                        this.hdIPStore.Value = string.Empty;
                        this.txtIPStore.Text = string.Empty;
                        this.chkbIPEffort.Checked = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "disableAddBusinessToStore"), (store_state == "MO" ? "Missouri" : "District of Columbia"), (store_state == "MO" ? "20" : "15")) + "');", true);
                    }
                    return;
                }
            }
        }

        return_value = this.dbOperation.updateBusinessProfile(this.prepareStoreProfileXml(), this.commonAppSession.LoginUserInfoSession.UserID, (this.ddlEditInformation.SelectedValue == "1" ? true : false), is_check_dnc, can_reassign_business, out error_message, out contactlog_pk);

        if (return_value == -1 || return_value == 0)
        {
            this.dtBusinesses = this.dbOperation.getStoreBusinessesDetails(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId);
            ViewState["storeBusinesses"] = this.dtBusinesses;
            this.bindLocalBusinesses();
            this.clearFields();
            if (contactlog_pk > 0)
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = contactlog_pk;
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = contactlog_pk;
                if (hdSelectedBusinessType.Value == "charity" && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');window.location.href='walgreensCharityProgram.aspx';", true);
                    return;
                }
                else if (hdSelectedBusinessType.Value == "CommunityOutreach" && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');window.location.href='walgreensCommunityOutreachProgram.aspx';", true);
                    return;
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessUpdated") + "');", true);
            }
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessUpdated") + "');", true);
        }

        else if (return_value == -3 || return_value == -4)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
            if (error_message.Contains("(IMZ)") && error_message.Contains("(SO)"))
            {
                this.chkbIPEffort.Checked = true;
                this.txtIPStore.Text = this.hdIPStore.Value;
                this.chkbSOEffort.Checked = true;
                this.txtSOStore.Text = this.hdSOStore.Value;
            }
            else if (error_message.Contains("(IMZ)"))
            {
                this.chkbIPEffort.Checked = true;
                this.txtIPStore.Text = this.hdIPStore.Value;
            }
            else if (error_message.Contains("(SO)"))
            {
                this.chkbSOEffort.Checked = true;
                this.txtSOStore.Text = this.hdSOStore.Value;
            }
        }
        else if (return_value == -5)
        {
            if (error_message.Contains("(IMZ)") && error_message.Contains("(SO)"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "displayConfirmation('" + error_message + "','1#3');", true);
            }
            else if (error_message.Contains("(IMZ)"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "displayConfirmation('" + error_message + "','3');", true);
            }
            else if (error_message.Contains("(SO)"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "displayConfirmation('" + error_message + "','1');", true);
            }
        }

        else if (return_value == -6)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "','3');", true);
            this.chkbIPEffort.Checked = true;
            this.txtIPStore.Text = this.hdIPStore.Value;
        }
        else if (return_value == -7)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "','3');", true);
            this.chkbxHhsProgram.Checked = false;
            this.chkCOProgram.Checked = false;
        }
        else if (return_value == -8)//Assigned to Restricted store state
        {
            if (string.IsNullOrEmpty(this.hdIPStore.Value) && this.chkbIPEffort.Checked && !string.IsNullOrEmpty(this.txtIPStore.Text))
            {
                this.hdIPStore.Value = string.Empty;
                this.txtIPStore.Text = string.Empty;
                this.chkbIPEffort.Checked = false;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
            }
            else if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showAddBusinessDisableToMOstateAlert('" + error_message + "');", true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "disableAddBusinessToStore"), (store_state == "MO" ? "Missouri" : "District of Columbia"), (store_state == "MO" ? "20" : "15")) + "');", true);
            }
        }
        else if (return_value == -2)
        {
            string last_string = "";
            error_message = error_message.TrimStart(',').TrimEnd(',');
            if (error_message.LastIndexOf(',').ToString() != "-1")
            {
                last_string = error_message.Substring(error_message.LastIndexOf(','));
                error_message = error_message.Replace(last_string, last_string.Replace(",", " and"));
            }

            error_message = String.Format((string)GetGlobalResourceObject("errorMessages", "duplicateBusiness"), error_message);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message.Replace("'", "`") + "');", true);
        }
        else if (return_value == -9)
        {
            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDNCMatchWarning('" + (string)GetGlobalResourceObject("errorMessages", "doNotCallDupBusiness") + "');", true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "doNotCallDupBusiness") + "');", true);
        }
        else if (return_value == -10)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showWarning('Store Assignment','" + (string)GetGlobalResourceObject("errorMessages", "BusinessAssignmentToOtherStateAlert") + "','CanAssignToOtherState');", true);
        else
        {
            error_message = String.Format((string)GetGlobalResourceObject("errorMessages", "duplicateBusiness"), error_message);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message.Replace("'", "`") + "');", true);
        }
    }

    /// <summary>
    /// Enable and disable outreach related checkbox, textbox depending on role.
    /// </summary>
    private void bindCheckBoxes()
    {
        this.txtIPStore.Enabled = false;
        this.txtSOStore.Enabled = false;
        this.chkbIPEffort.Enabled = false;
        this.chkbSOEffort.Enabled = false;

        if (commonAppSession.LoginUserInfoSession.IsAdmin || commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district Manager")
        {
            this.txtIPStore.Enabled = true;
            this.txtSOStore.Enabled = true;
            this.chkbIPEffort.Enabled = true;
            this.chkbSOEffort.Enabled = true;
        }
    }

    /// <summary>
    /// Clear all the fields
    /// </summary>
    private void clearFields()
    {
        this.txtFirstName.Text = string.Empty;
        this.txtLastName.Text = string.Empty;
        this.txtTitle.Text = string.Empty;
        this.txtBusinessName.Text = string.Empty;
        this.txtAddress1.Text = string.Empty;
        this.txtAddress2.Text = string.Empty;
        this.txtCounty.Text = string.Empty;
        this.txtCity.Text = string.Empty;
        this.ddlState.ClearSelection();
        this.txtZip.Text = string.Empty;
        this.txtZip4.Text = string.Empty;
        this.txtPhone.Text = string.Empty;
        this.txtTollFree.Text = string.Empty;
        this.txtIndustry.Text = string.Empty;
        this.txtWebUrl.Text = string.Empty;
        this.txtEmpSize.Text = string.Empty;
        this.chkbIPEffort.Checked = false;
        this.chkbSOEffort.Checked = false;
        this.txtSOStore.Text = string.Empty;
        this.txtIPStore.Text = string.Empty;
        this.chkbxHhsProgram.Checked = false;
        this.chkCOProgram.Checked = false;
    }
    #endregion

    #region ----------------- PRIVATE VARIABLES ----------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private DataTable dtBusinesses = null;
    #endregion

    #region ----------------- Web Form Designer generated code -----------------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion
}