﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using System.IO;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Text;
using TdWalgreens;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;
using System.Web;
using System.Configuration;

public partial class walgreensVoteVaxAgreement : Page
{
    #region ------------ PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedContactLogPk > 0)
            {
                this.contactLogPk = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk;
                this.hfContactLogPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk.ToString();
                this.hfBusinessStoreId.Value = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.hfBussinessClinicPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk.ToString();
                this.ddlLegalState.bindStates();
                this.bindContractAgreement();
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = 0;
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
            }
            else
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("walgreensNoAccess.htm");
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(this.hfContactLogPk.Value))
                this.contactLogPk = Convert.ToInt32(this.hfContactLogPk.Value);

            string event_args = Request["__EVENTTARGET"];
            if (event_args.ToLower() == "confirmimmunization")
            {
                bool confirm_immunization = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                if (!confirm_immunization)
                {
                    if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
                    {
                        this.grdImmunizationChecks.FooterRow.Visible = false;
                        this.grdImmunizationChecks.ShowFooter = false;

                        IPostBackEventHandler send_email = (IPostBackEventHandler)this.btnSendMail;
                        send_email.RaisePostBackEvent(string.Empty);
                    }
                }
                else
                {
                    ImageButton img_immunization = null;
                    if (this.grdImmunizationChecks.Rows.Count == 0)
                        img_immunization = (ImageButton)grdImmunizationChecks.Controls[0].Controls[0].FindControl("imgBtnImmunizationOk");
                    else
                        img_immunization = (ImageButton)grdImmunizationChecks.FooterRow.FindControl("imgBtnImmunizationOk");


                    this.imgBtnImmunizationOk_Click(img_immunization, EventArgs.Empty);
                }
            }
            else if (event_args.ToLower() == "continuesaving")
            {
                this.saveSendLaterHandler();
            }
            else if (event_args.ToLower() == "continuesending")
            {
                this.sendEmailHandler();
            }
            else if (event_args.ToLower() == "continuesavenocontract")
            {
                this.saveNoContractHandler();
            }
        }
        this.setImageButtons();
        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false; 
    }

    protected void btnSendMail_Click(object sender, ImageClickEventArgs e)
    {
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);

        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            if (this.ValidateAgreement("WalgreensUser"))
            {
                DataTable dt_contract_agreement = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[0];
                this.isContractApproved = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["isApproved"].ToString() : "";
                this.isContractApproved = string.IsNullOrEmpty(this.isContractApproved) ? false.ToString() : this.isContractApproved;

                if (!Convert.ToBoolean(this.isContractApproved))
                {
                    int alert_type = this.validateClinicDates();
                    string clinic_date_reminder_alert = "";

                    if (alert_type == 0)
                        this.sendEmailHandler();
                    else
                    {
                        DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
                        this.dtImmunizations = ds_contract_agreement.Tables[1];
                        this.updatePaymentTypeData();
                        this.xmlContractAgreement = this.updateClinicLocations(-1);
                        this.bindImmunizationChecks();
                        this.bindClinicLocations();

                        if (alert_type == 1)
                        {
                            clinic_date_reminder_alert = hfLanguge.Value == "es-MX" ? "clinicDateReminderBefore2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderBefore2WeeksPR" : "clinicDateReminderBefore2WeeksEN");
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminderWarning('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueSending');", true);
                        }
                        else
                        {
                            clinic_date_reminder_alert = hfLanguge.Value == "es-MX" ? "clinicDateReminderAfter2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderAfter2WeeksPR" : "clinicDateReminderAfter2WeeksEN");
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueSending');", true);
                        }
                    }
                }
                else
                    this.sendEmailHandler();
            }
            else if (this.paymentMethods == "[]")
            {
                this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
                this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();
            }
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.location.href = 'walgreensHome.aspx';", true);
    }

    protected void btnSaveNoContract_Click(object sender, ImageClickEventArgs e)
    {
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);

        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            if (this.ValidateAgreement("WalgreensUser", true))
            {
                int alert_type = this.validateClinicDates();
                string clinic_date_reminder_alert = "";

                if (alert_type == 0)
                    this.saveNoContractHandler();
                else
                {
                    DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
                    this.dtImmunizations = ds_contract_agreement.Tables[1];
                    this.updatePaymentTypeData();
                    this.xmlContractAgreement = this.updateClinicLocations(-1);
                    this.bindImmunizationChecks();
                    this.bindClinicLocations();

                    if (alert_type == 1)
                    {
                        clinic_date_reminder_alert = hfLanguge.Value == "es-MX" ? "clinicDateReminderBefore2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderBefore2WeeksPR" : "clinicDateReminderBefore2WeeksEN");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminderWarning('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueSaveNoContract');", true);
                    }
                    else
                    {
                        clinic_date_reminder_alert = hfLanguge.Value == "es-MX" ? "clinicDateReminderAfter2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderAfter2WeeksPR" : "clinicDateReminderAfter2WeeksEN");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueSaveNoContract');", true);
                    }
                }
            }
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.location.href = 'walgreensHome.aspx';", true);
    }

    protected void btnSendLater_Click(object sender, ImageClickEventArgs e)
    {
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            int alert_type = this.validateClinicDates();
            string clinic_date_reminder_alert = "";

            if (alert_type == 0)
                this.saveSendLaterHandler();
            else
            {
                DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
                this.dtImmunizations = ds_contract_agreement.Tables[1];
                this.updatePaymentTypeData();
                this.xmlContractAgreement = this.updateClinicLocations(-1);
                this.bindImmunizationChecks();
                this.bindClinicLocations();

                if (alert_type == 1)
                {
                    clinic_date_reminder_alert = hfLanguge.Value == "es-MX" ? "clinicDateReminderBefore2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderBefore2WeeksPR" : "clinicDateReminderBefore2WeeksEN");
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminderWarning('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueSaving');", true);
                }
                else
                {
                    clinic_date_reminder_alert = hfLanguge.Value == "es-MX" ? "clinicDateReminderAfter2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderAfter2WeeksPR" : "clinicDateReminderAfter2WeeksEN");
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showClinicDateReminder('" + (string)GetGlobalResourceObject("errorMessages", clinic_date_reminder_alert) + "','ContinueSaving');", true);
                }
            }
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "window.location.href = 'walgreensHome.aspx';", true);
    }

    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        string referrer_path = string.Empty;
        int business_clinic_pk, store_id;

        Session.Remove("contractAgreement_" + this.hfContactLogPk.Value);
        if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.referrerPath))
        {
            Int32.TryParse(this.hfBussinessClinicPk.Value.Trim(), out business_clinic_pk);
            Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);

            this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;
            this.commonAppSession.SelectedStoreSession.storeID = store_id;
            referrer_path = this.commonAppSession.SelectedStoreSession.referrerPath;
            this.commonAppSession.SelectedStoreSession.referrerPath = string.Empty;
        }
        else
            referrer_path = "walgreensHome.aspx";

        Response.Redirect(referrer_path);
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
        DataTable dt_contract_agreement = ds_contract_agreement.Tables[0];

        this.dtImmunizations = ds_contract_agreement.Tables[1];
        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(-1);
        this.createNewClinicLocation(false);

        if (this.isRestrictedStoreState)
        {
            //Set business address details to default clinic location
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address1"].Value = dt_contract_agreement.Rows[0]["address"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address2"].Value = dt_contract_agreement.Rows[0]["address2"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["city"].Value = dt_contract_agreement.Rows[0]["city"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["state"].Value = dt_contract_agreement.Rows[0]["state"].ToString();
            this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["zipCode"].Value = dt_contract_agreement.Rows[0]["zip"].ToString();
            this.isAddressDisabled = true;
        }

        this.bindClinicLocations();
        this.bindImmunizationChecks();
    }

    protected void imgBtnRemoveLocation_Click(object sender, EventArgs e)
    {
        ImageButton img_btn_remove = (ImageButton)sender;
        GridViewRow grd_row = (GridViewRow)img_btn_remove.NamingContainer;

        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(grd_row.RowIndex);

        this.bindClinicLocations();
        this.bindImmunizationChecks();
        if (this.isMOPreviousSeasonBusiness)
        {
            this.bindClinicAgreemtPreviousLocationToDropdown();
        }
    }

    protected void grdLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk_reassign_clinics = (CheckBox)e.Row.FindControl("chkReassignClinic");

            if (this.hasMutlipleClinics)
                chk_reassign_clinics.Visible = this.hasMutlipleClinics;
            //else if (!string.IsNullOrEmpty(this.isContractApproved))
            //    chk_reassign_clinics.Visible = Convert.ToBoolean(this.isContractApproved);

            DropDownList ddl_states = (DropDownList)e.Row.FindControl("ddlState");
            string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate");
            string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate");
            if (Convert.ToDateTime(restriction_start_date) < DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
            {
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString(), ApplicationSettings.getStoreStateRestrictions("storeState"));
            }
            else
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString(), "");

            if (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR")
            {
                ddl_states.Items[0].Text = "-- Seleccione --";
            }
            if (ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString()) != null)
                ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["state"].ToString()).Selected = true;

            if (e.Row.RowIndex == 0)
            {
                ImageButton btn_remove_location = (ImageButton)e.Row.FindControl("imgBtnRemoveLocation");
                btn_remove_location.Visible = false;
            }

            var date_control = e.Row.FindControl("PickerAndCalendarFrom");
            if (!string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString()) && grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
            {
                if (ddl_states.SelectedValue == "MO" && !commonAppSession.LoginUserInfoSession.IsAdmin)
                {
                    if (Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString()) >= DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                        ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                    else if (Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString()) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                        ((PickerAndCalendar)date_control).SetMinDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString());
                }
                else if (DateTime.Now < Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString()))
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                else
                    ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString()).AddDays(-1);

                ((PickerAndCalendar)date_control).getSelectedDate = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString());
                ((TextBox)e.Row.FindControl("txtCalenderFrom")).Text = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString()).ToString("MM/dd/yyyy");
            }
            else
            {
                //For "MO" stores block out all clinic dates up to September 1, 2016 in the local contract and for charity events
                if (ddl_states.SelectedValue == "MO" && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && !commonAppSession.LoginUserInfoSession.IsAdmin)
                {
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                }
                else
                {
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                    //((PickerAndCalendar)date_control).getSelectedDate = DateTime.Now;
                }
            }

            if (this.dtImmunizations.Select("isSelected = 'True'").Count() > 0)
            {
                GridView grd_clinic_immunizations = (GridView)e.Row.FindControl("grdClinicImmunizations");
                this.bindClinicImmunizations(grd_clinic_immunizations, e.Row.RowIndex);
            }

            if (this.isAddressDisabled)
            {
                ((TextBox)e.Row.FindControl("txtAddress1")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtAddress2")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtCity")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtZipCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlState")).Enabled = false;
                ((CheckBox)e.Row.FindControl("chkReassignClinic")).Enabled = false;
                ((CheckBox)e.Row.FindControl("chkReassignClinic")).Visible = false;
            }
            DateTime clinic_date = ((PickerAndCalendar)date_control).getSelectedDate;
            if (clinic_date >= DateTime.Now.Date)
            {
                string clinic_date_resource_key = "";
                Label lbl_clinic_date_Alert = (Label)e.Row.FindControl("lblClinicDateAlert");
                TimeSpan gap = clinic_date - DateTime.Now.Date;

                if (gap.Days <= 14)
                    clinic_date_resource_key = hfLanguge.Value == "es-MX" ? "clinicDateReminderBefore2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderBefore2WeeksPR" : "clinicDateReminderBefore2WeeksEN");
                else
                    clinic_date_resource_key = hfLanguge.Value == "es-MX" ? "clinicDateReminderAfter2WeeksMX" : (hfLanguge.Value == "es-PR" ? "clinicDateReminderAfter2WeeksPR" : "clinicDateReminderAfter2WeeksEN");

                lbl_clinic_date_Alert.Text = string.Format((string)GetGlobalResourceObject("errorMessages", clinic_date_resource_key));
                lbl_clinic_date_Alert.Visible = true;
            }
        }
    }

    protected void grdClinicImmunizations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string immunization_key = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "immunizationSpanishName" : "immunizationName";
            string payment_type_key = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "paymentTypeSpanishName" : "paymentTypeName";
            Label lbl_clinic_immunization = (Label)e.Row.FindControl("lblClinicImmunization");
            Label lbl_clinic_payment_type = (Label)e.Row.FindControl("lblClinicPaymentType");
            lbl_clinic_immunization.Text = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblImmunizationId")).Text))[0][immunization_key].ToString();
            lbl_clinic_payment_type.Text = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblImmunizationId")).Text) + " AND paymentTypeId = " + Convert.ToInt32(((Label)e.Row.FindControl("lblPaymentTypeId")).Text))[0][payment_type_key].ToString();
        }
    }

    protected void imgBtnAddImmunization_Click(object sender, EventArgs e)
    {
        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(-1);
        this.bindImmunizationChecks();
        this.bindClinicLocations();
        this.grdImmunizationChecks.ShowFooter = true;
        this.grdImmunizationChecks.FooterRow.Visible = true;
        if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
        {
            Label lbl_immunization_check = (Label)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("lblImmunizationCheck");
            Label lbl_payment_type = (Label)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("lblPaymentType");
            lbl_immunization_check.Text = "";
            lbl_payment_type.Text = "";
        }
    }

    protected void imgBtnImmunizationOk_Click(object sender, EventArgs e)
    {
        ImageButton img_immunization = (ImageButton)sender;
        GridViewRow grd_immunization_row = (GridViewRow)img_immunization.NamingContainer;
        //Label lbl_immunization = (Label)grd_immunization_row.FindControl("lblImmunizationCheck");
        //Label lbl_payment_method = (Label)grd_immunization_row.FindControl("lblPaymentType");
        Label lbl_immunization_check_id = (Label)grd_immunization_row.FindControl("lblImmunizationId");
        Label lbl_payment_type_id = (Label)grd_immunization_row.FindControl("lblPaymentTypeId");
        //if (ddl_payment_method.SelectedValue == "" && hfUnconfirmedPayment.Value != "")
        //{
        //    ddl_payment_method.SelectedIndex = ddl_payment_method.Items.IndexOf(ddl_payment_method.Items.FindByValue(hfUnconfirmedPayment.Value));
        //    hfUnconfirmedPayment.Value = "";
        //}

        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];

        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(-1);

        //if (ddl_immunization.SelectedIndex != 0 && ddl_payment_method.SelectedIndex != 0)
        //{
        DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + lbl_immunization_check_id.Text + " AND paymentTypeId = " + lbl_payment_type_id.Text + " AND isSelected = 'False' AND isPaymentSelected = 'False'");
        if (dr_immunization != null && dr_immunization.Count() > 0)
        {
            dr_immunization[0]["isSelected"] = "True";
            dr_immunization[0]["isPaymentSelected"] = "True";

            XmlNodeList clinic_locations = this.xmlContractAgreement.SelectNodes("Clinics/clinic");
            foreach (XmlElement clinic in clinic_locations)
            {
                XmlElement est_shots = this.xmlContractAgreement.CreateElement("immunization");
                if (clinic.SelectSingleNode("./immunization[@pk = '" + lbl_immunization_check_id.Text + "' and @paymentTypeId = '" + lbl_payment_type_id.Text + "']") == null)
                {
                    est_shots.SetAttribute("pk", lbl_immunization_check_id.Text);
                    est_shots.SetAttribute("paymentTypeId", lbl_payment_type_id.Text);
                    est_shots.SetAttribute("estimatedQuantity", string.Empty);
                    clinic.AppendChild(est_shots);
                }
            }
        }

        if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
        {
            this.grdImmunizationChecks.ShowFooter = false;
            this.grdImmunizationChecks.FooterRow.Visible = false;
        }
        else if (this.grdImmunizationChecks.ShowFooter)
            this.grdImmunizationChecks.ShowFooter = false;
        //}

        //this.bindImmunizationChecks();
        //this.bindClinicLocations();
    }

    protected void imgBtnRemoveImmunization_Click(object sender, CommandEventArgs e)
    {
        GridViewRow grid_row = (GridViewRow)((ImageButton)sender).NamingContainer;
        if (e.CommandArgument.ToString().ToLower() == "newimmunization")
        {
            if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                this.grdImmunizationChecks.FooterRow.Visible = false;
                this.grdImmunizationChecks.ShowFooter = false;

            }
            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.bindClinicLocations();
        }
        else
        {
            Label immunization_pk = (Label)this.grdImmunizationChecks.Rows[grid_row.RowIndex].FindControl("lblImmunizationPk");
            Label payment_pk = (Label)this.grdImmunizationChecks.Rows[grid_row.RowIndex].FindControl("lblPaymentTypeId");

            if (!string.IsNullOrEmpty(immunization_pk.Text))
            {
                this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
                DataRow[] immunization_selected = this.dtImmunizations.Select("immunizationId = '" + immunization_pk.Text + "' AND paymentTypeId = '" + payment_pk.Text + "'");
                if (immunization_selected.Count() > 0)
                {
                    immunization_selected[0]["isSelected"] = "False";
                    immunization_selected[0]["isPaymentSelected"] = "False";
                }
            }


            if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
            {
                Label lbl_immunization_check = (Label)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("lblImmunizationCheck");
                Label lbl_payment_type = (Label)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("lblPaymentType");
                lbl_immunization_check.Text = "";
                lbl_payment_type.Text = "";
            }

            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.bindImmunizationChecks();
            this.bindClinicLocations();
        }
    }

    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationPk");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");



            DataRow[] row_payment_types = this.dtImmunizations.Select("immunizationId = " + Convert.ToInt32(lbl_immunization_id.Text) + " AND paymentTypeId = " + Convert.ToInt32(lbl_payment_type_id.Text) + " AND isSelected = 'True' AND isPaymentSelected = 'True'");
            if (row_payment_types.Count() > 0)
            {
                if (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR")
                {
                    Label lbl_Immunization = (Label)e.Row.FindControl("lblImmunizationCheck");
                    Label lbl_payment_type = (Label)e.Row.FindControl("lblPaymentType");
                    lbl_Immunization.Text = ((DataRowView)(e.Row.DataItem)).Row["immunizationSpanishName"].ToString();
                    lbl_payment_type.Text = ((DataRowView)(e.Row.DataItem)).Row["paymentTypeSpanishName"].ToString();
                }

                // lbl_price.Text = (row_payment_types[0]["price"] == DBNull.Value) ? "N/A" : "$ " + row_payment_types[0]["price"].ToString();
                lbl_price.Text = "N/A";
            }

        }
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        // to clear the date in picker if we select MO state by non admin user
        //calling bind clinic to set min dates dates for date pickers
        DropDownList ddl_state_clinic_location = (DropDownList)sender;
        if (ddl_state_clinic_location != null)
        {
            GridViewRow row = (GridViewRow)ddl_state_clinic_location.Parent.Parent;
            if (row != null)
            {
                var date_control = row.FindControl("PickerAndCalendarFrom");
                DateTime seleted_date = ((PickerAndCalendar)date_control).getSelectedDate;

                if (ddl_state_clinic_location.SelectedValue == "MO" && !commonAppSession.LoginUserInfoSession.IsAdmin && seleted_date < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Parse("01/01/0001");
                }
            }
            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.bindImmunizationChecks();
            this.bindClinicLocations();
        }
    }

    /// <summary>
    /// Binds Time Picker
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void displayTime_Picker(object sender, EventArgs e)
    {
        StringBuilder script_text = new StringBuilder();
        GridViewRow gvr = (GridViewRow)((sender as TextBox).NamingContainer);
        TextBox txt_end_time = new TextBox();
        txt_end_time = (TextBox)gvr.FindControl("txtEndTime");
        script_text = ApplicationSettings.displayTimePicker((sender as TextBox).ClientID, txt_end_time.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DateScript" + gvr.RowIndex, script_text.ToString(), true);
    }

    protected void lnkChangeCulture_Click(object sender, EventArgs e)
    {
        int business_clinic_pk, store_id;
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        Int32.TryParse(this.hfBussinessClinicPk.Value.Trim(), out business_clinic_pk);
        Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);

        this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = this.contactLogPk;
        this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;
        this.commonAppSession.SelectedStoreSession.storeID = store_id;

        if (Session != null && Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
        {
            DataSet ds_contract_agreement = new DataSet();
            ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
            this.prepareClinicAggreementXML(true);
            ds_contract_agreement.Tables[0].Rows[0]["clinicAgreementXml"] = this.xmlContractAgreement.OuterXml;
            bindContractAgreement();
            if (this.isMOPreviousSeasonBusiness)
            {
                this.bindClinicAgreemtPreviousLocationToDropdown();
            }
            Session["contractAgreement_" + this.contactLogPk.ToString()] = ds_contract_agreement;
        }
        else
            Response.Redirect("walgreensLandingPage.aspx");
    }

    protected void imgbtnAddPreviousClinic_Click(object sender, ImageClickEventArgs e)
    {
        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        this.updatePaymentTypeData();
        this.bindImmunizationChecks();
        // add selected clinic location to xmlContractAgreement by comparing address fields in the previousClinicLocation data table 
        if (ddlClinicLocations.SelectedIndex != 0)
        {
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.createNewClinicLocation(false);
            //Bind clinic locations
            this.bindClinicLocations();
            //update the clinic locations dropdown
            this.bindClinicAgreemtPreviousLocationToDropdown();
        }
        else
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "MOStateAddClinicValidation") + "'); ", true);
            DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
            DataTable dt_contract_agreement = ds_contract_agreement.Tables[0];
            this.dtImmunizations = ds_contract_agreement.Tables[1];
            this.updatePaymentTypeData();
            this.xmlContractAgreement = this.updateClinicLocations(-1);
            this.createNewClinicLocation(false);
            //XmlNode newNode = xmlContractAgreement.LastChild;
            if (this.isRestrictedStoreState)
            {
                //Set business address details to default clinic location
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address1"].Value = dt_contract_agreement.Rows[0]["address"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["Address2"].Value = dt_contract_agreement.Rows[0]["address2"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["city"].Value = dt_contract_agreement.Rows[0]["city"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["state"].Value = dt_contract_agreement.Rows[0]["state"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic[position() = last()]").Attributes["zipCode"].Value = dt_contract_agreement.Rows[0]["zip"].ToString();
                this.isAddressDisabled = true;
            }
            this.bindImmunizationChecks();
            this.bindClinicLocations();

        }
    }


    #endregion

    #region ------------ PRIVATE METHODS -------------
    /// <summary>
    /// Set control properties
    /// </summary>
    /// <param name="control"></param>
    /// <param name="control_type"></param>
    /// <param name="tool_tip"></param>
    private void setControlProperty(Control control, string control_type, string tool_tip, bool is_invalid)
    {
        switch (control_type.ToLower())
        {
            case "textbox":
                if (is_invalid)
                {
                    ((TextBox)control).Attributes.Add("title", tool_tip);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((TextBox)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }

                break;
            case "dropdownlist":
                if (is_invalid)
                {
                    ((DropDownList)control).ToolTip = tool_tip;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setInvalidControlCssFor" + control.ClientID, "setInvalidControlCss('" + control.ClientID + "',true);", true);
                }
                else
                {
                    if (control != null)
                    {
                        ((DropDownList)control).ToolTip = string.Empty;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setValidControlCss" + control.ClientID, "setValidControlCss('" + control.ClientID + "',true);", true);
                    }
                }

                break;
        }
    }

    /// <summary>
    /// Validate Immunization Values and Clinic Locations
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateAgreement(string validation_group, bool is_save = false)
    {
        string error_message = string.Empty;
        bool hide_clear_immunization = false;

        bool is_valid = this.ValidateImmunizations(validation_group, ref error_message, ref hide_clear_immunization);
        is_valid = this.ValidateLocations(validation_group, is_save) && is_valid;

        if (!string.IsNullOrEmpty(error_message))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showImmConfirmationWarning('" + error_message + "','" + hide_clear_immunization + "');", true);
            is_valid = false;
        }
        else
        {
            if (!is_valid)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
        }

        return is_valid;
    }

    /*
    private bool validateImmunizationBlackedOutDates()
    {
        bool is_valid = true;
        string value = string.Empty;
        string failed_clinic_locations = string.Empty;
        if (isImmunizationsBlackoutValidationRequired)
        {
            foreach (GridViewRow row in grdLocations.Rows)
            {
                bool is_estimated_qnt_exists = false;
                value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");
                //validate immunization blackedout dates
                ComponentArt.Web.UI.Calendar calendarctrl = (ComponentArt.Web.UI.Calendar)((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).FindControl("picker1");

                GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");

                foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
                {
                    string immunization_name = ((Label)imm_row.FindControl("lblClinicImmunization")).Text;
                    if (blackedOutImmunizations.Contains(immunization_name.ToLower().Trim()) && !string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text.Trim()))
                    {
                        is_estimated_qnt_exists = Convert.ToInt32(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text) == 0 ? false : true;
                    }
                }
                if (value != "01/01/0001" && is_estimated_qnt_exists)
                {
                    if (Convert.ToDateTime(value) >= Convert.ToDateTime("04/15/2016") && Convert.ToDateTime(value) <= Convert.ToDateTime("07/15/2016"))
                    {
                        failed_clinic_locations += "\\n" + ((Label)row.FindControl("lblClinicLocation")).Text;
                        calendarctrl.BorderColor = System.Drawing.Color.Red;
                        calendarctrl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                        calendarctrl.BorderWidth = Unit.Parse("1px");
                        calendarctrl.ToolTip = "";
                        is_valid = false;
                    }
                }
            }
            if (!is_valid)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "blackoutImmunizationValidationMessage") + failed_clinic_locations.Replace("UBICACIÓN DE LA CLÍNICA", "CLINIC LOCATION") + "');", true);
            }
        }
        return is_valid;
    }
    */

    /// <summary>
    /// Validate clinic locations
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateLocations(string validation_group, bool is_save = false)
    {
        bool is_valid = true;
        string value = string.Empty;

        if (validation_group == "WalgreensUser" && !is_save && string.IsNullOrEmpty(this.txtEmails.Text))
        {
            this.setControlProperty(this.txtEmails, "textbox", "'Email Agreement to:' field is required", true);
            is_valid = false;
        }
        else if (!string.IsNullOrEmpty(this.txtEmails.Text) && !this.txtEmails.Text.validateMultipleEmails())
        {
            this.setControlProperty(this.txtEmails, "textbox", "'Email Agreement to:' Please enter valid Email", true);
            is_valid = false;
        }
        else
            this.setControlProperty(this.txtEmails, "textbox", string.Empty, false);

        //Validate clinic locations
        foreach (GridViewRow row in grdLocations.Rows)
        {
            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactName")).Text) && !(((TextBox)row.FindControl("txtLocalContactName")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", "Contact Name: < > characters are not allowed", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactName"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1 is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && !(((TextBox)row.FindControl("txtAddress1")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", "Address1: < > characters are not allowed", true);
                is_valid = false;
            }
            else if (validation_group == "WalgreensUser" && !string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress1")).Text) && ((TextBox)row.FindControl("txtAddress1")).Enabled && !(((TextBox)row.FindControl("txtAddress1")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtAddress1"), "textbox", string.Empty, false);

            if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && !(((TextBox)row.FindControl("txtAddress2")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", "Address2: < > characters are not allowed", true);
                is_valid = false;
            }
            else if (validation_group == "WalgreensUser" && !string.IsNullOrEmpty(((TextBox)row.FindControl("txtAddress2")).Text) && ((TextBox)row.FindControl("txtAddress2")).Enabled && !(((TextBox)row.FindControl("txtAddress2")).Text.validateAddress()))
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert").ToString(), true);
                is_valid = false;
            }
            else
            {
                this.setControlProperty(row.FindControl("txtAddress2"), "textbox", string.Empty, false);
            }
            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtCity")).Text) && !(((TextBox)row.FindControl("txtCity")).Text.validateJunkCharacters()))
            {
                this.setControlProperty(row.FindControl("txtCity"), "textbox", "City: < > characters are not allowed", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtCity"), "textbox", string.Empty, false);

            value = ((TextBox)row.FindControl("txtLocalContactPhone")).Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(value))
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Phone is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(value) && !value.validatePhone())
            {
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", "Valid Phone number is required(ex: ###-###-####)", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactPhone"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Zip Code is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtZipCode")).Text) && !(((TextBox)row.FindControl("txtZipCode")).Text.validateZipCode()))
            {
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", "Invalid Zip Code", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtZipCode"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Email is required", true);
                is_valid = false;
            }
            else if (!string.IsNullOrEmpty(((TextBox)row.FindControl("txtLocalContactEmail")).Text) && !(((TextBox)row.FindControl("txtLocalContactEmail")).Text.validateEmail()))
            {
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", "Invalid Email", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtLocalContactEmail"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && ((DropDownList)row.FindControl("ddlState")).SelectedItem.Value.Trim() == "")
            {
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", "Select State", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("ddlState"), "dropdownlist", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtStartTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", "Start Time is required", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtStartTime"), "textbox", string.Empty, false);

            if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)row.FindControl("txtEndTime")).Text))
            {
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", "End Time is required", true);
                is_valid = false;
            }
            else
                this.setControlProperty(row.FindControl("txtEndTime"), "textbox", string.Empty, false);

            value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");
            ComponentArt.Web.UI.Calendar calendarctrl = (ComponentArt.Web.UI.Calendar)((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).FindControl("picker1");
            if (value == "01/01/0001" && validation_group == "WalgreensUser")
            {
                calendarctrl.BorderColor = System.Drawing.Color.Red;
                calendarctrl.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                calendarctrl.BorderWidth = Unit.Parse("1px");
                calendarctrl.ToolTip = "Clinic Date is required";
                is_valid = false;
            }
            else
            {
                calendarctrl.Style.Add("border", "1px solid gray");
                calendarctrl.ToolTip = string.Empty;
            }

            GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");

            foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
            {
                if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text))
                {
                    this.setControlProperty(imm_row.FindControl("txtClinicImmunizationShots"), "textbox", "Estimated Shots is required", true);
                    is_valid = false;
                }
                else if (!string.IsNullOrEmpty(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text) && !(((TextBox)imm_row.FindControl("txtClinicImmunizationShots")).Text.validateEstimatedShots()))
                {
                    this.setControlProperty(imm_row.FindControl("txtClinicImmunizationShots"), "textbox", "Invalid Estimated Shots value", true);
                    is_valid = false;
                }
                else
                    this.setControlProperty(imm_row.FindControl("txtClinicImmunizationShots"), "textbox", string.Empty, false);
            }
        }

        //Validating WALGREENS CO DATA
        if (!string.IsNullOrEmpty(txtWalgreenName.Text) && !txtWalgreenName.Text.validateJunkCharacters())
        {
            this.setControlProperty(Page.FindControl("txtWalgreenName"), "textbox", "Walgreens Co. Name:: , < > characters are not allowed", true);
            is_valid = false;
        }
        if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(txtWalgreenTitle.Text))
        {
            this.setControlProperty(Page.FindControl("txtWalgreenTitle"), "textbox", "Enter the title for the Walgreens Co. representative named in the Name field", true);
            is_valid = false;
        }
        else if (!string.IsNullOrEmpty(txtWalgreenTitle.Text) && !txtWalgreenTitle.Text.validateJunkCharacters())
        {
            this.setControlProperty(Page.FindControl("txtWalgreenTitle"), "textbox", "Walgreens Co. Title:: , < > characters are not allowed", true);
            is_valid = false;
        }
        else
            this.setControlProperty(Page.FindControl("txtWalgreenTitle"), "textbox", string.Empty, false);

        if (validation_group == "WalgreensUser" && string.IsNullOrEmpty(txtDistrictNum.Text))
        {
            this.setControlProperty(Page.FindControl("txtDistrictNum"), "textbox", "Enter the Walgreens Co. District number", true);
            is_valid = false;
        }
        else if (!string.IsNullOrEmpty(txtDistrictNum.Text) && !txtDistrictNum.Text.validateIsNumeric())
        {
            this.setControlProperty(Page.FindControl("txtDistrictNum"), "textbox", "'Walgreens Co.:: The District number should be a numeric value", true);
            is_valid = false;
        }
        else
            this.setControlProperty(Page.FindControl("txtDistrictNum"), "textbox", string.Empty, false);

        return is_valid;
    }

    /// <summary>
    /// Validate Immunization values
    /// </summary>
    /// <param name="validation_group"></param>
    /// <param name="error_message"></param>
    /// <param name="hide_clear_immunization"></param>
    /// <returns></returns>
    private bool ValidateImmunizations(string validation_group, ref string error_message, ref bool hide_clear_immunization)
    {
        bool is_valid = true;

        if (validation_group == "WalgreensUser" && this.grdImmunizationChecks.Rows.Count == 0)
        {
            string immunization = ((DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck")).SelectedItem.Value;
            string payment_type = ((DropDownList)this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType")).SelectedItem.Value;

            this.setControlProperty(this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlImmunizationCheck"), "dropdownlist", (immunization == "" ? "Immunization is required" : string.Empty), (immunization == "" ? true : false));
            this.setControlProperty(this.grdImmunizationChecks.Controls[0].Controls[0].FindControl("ddlPaymentType"), "dropdownlist", (payment_type == "" ? "Payment Type is required" : string.Empty), (payment_type == "" ? true : false));

            if (immunization != "" && payment_type != "")
            {
                error_message = "Your selections for Immunization and Payment Method have not been confirmed. Click \"OK\" to confirm your selections. The contract will not be emailed until you have entered an Estimated Volume for confirmed immunizations.";
                this.hfUnconfirmedPayment.Value = payment_type;
                hide_clear_immunization = true;
            }

            return false;
        }

        if ((validation_group == "WalgreensUser") && (this.grdImmunizationChecks.Rows.Count > 0) && (this.grdImmunizationChecks.FooterRow != null) && (this.grdImmunizationChecks.FooterRow.Visible))
        {
            DropDownList ddl_immunization_checks = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlImmunizationCheck");
            DropDownList ddl_payment_type = (DropDownList)this.grdImmunizationChecks.FooterRow.FindControl("ddlPaymentType");

            if ((ddl_immunization_checks != null && ddl_immunization_checks.SelectedItem.Value != "") && (ddl_payment_type != null && ddl_payment_type.SelectedItem.Value != ""))
            {
                error_message = "Your selections for Immunization and Payment Method have not been confirmed. Click \"OK\" to confirm your selections or \"Clear\" to clear the unconfirmed selections. The contract will not be emailed until you have entered an Estimated Volume for confirmed immunizations.";
                this.hfUnconfirmedPayment.Value = ddl_payment_type.SelectedItem.Value;
            }
        }
        else
        {
            foreach (GridViewRow imm_row in this.grdImmunizationChecks.Rows)
            {
                if (blackedOutImmunizations.Contains(((Label)imm_row.FindControl("lblImmunizationCheck")).Text.ToLower().Trim()))
                {
                    isImmunizationsBlackoutValidationRequired = true;
                }
            }
        }

        return is_valid;
    }

    /// <summary>
    /// Check if all the immunizations are added to clinics
    /// </summary>
    /// <returns></returns>
    private bool checkClinicImmunizations()
    {
        bool is_valid = true;
        XmlNodeList clinic_locations = this.xmlContractAgreement.SelectNodes("ClinicAgreement/Clinics/clinic");
        XmlNodeList contract_immunizations = this.xmlContractAgreement.SelectNodes("ClinicAgreement/Immunizations/Immunization");

        foreach (XmlElement clinic in clinic_locations)
        {
            if (clinic.SelectNodes("./immunization").Count != contract_immunizations.Count)
            {
                is_valid = false;
                foreach (XmlElement immunization in contract_immunizations)
                {
                    if (clinic.SelectSingleNode("./immunization[@pk = '" + immunization.Attributes["pk"].Value + "' and @paymentTypeId = '" + immunization.Attributes["paymentTypeId"].Value + "']") == null)
                    {
                        XmlElement est_shots = this.xmlContractAgreement.CreateElement("immunization");
                        est_shots.SetAttribute("pk", immunization.Attributes["pk"].Value);
                        est_shots.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
                        est_shots.SetAttribute("estimatedQuantity", string.Empty);
                        clinic.AppendChild(est_shots);
                    }
                }
            }
        }

        return is_valid;
    }

    /// <summary>
    /// Creates new clinic location
    /// </summary>
    /// <param name="is_default"></param>
    private void createNewClinicLocation(bool is_default)
    {
        double clinics_count = 0;
        XmlElement clinic_locations_node;
        if (is_default)
            clinic_locations_node = this.xmlContractAgreement.CreateElement("Clinics");
        else
            clinic_locations_node = (XmlElement)this.xmlContractAgreement.SelectSingleNode("Clinics");

        clinics_count = (is_default) ? 0 : clinic_locations_node.SelectNodes("clinic").Count;

        XmlElement clinic_ele = this.xmlContractAgreement.CreateElement("clinic");
        string address1, address2, city, state, zip;
        address1 = string.Empty;
        address2 = string.Empty;
        city = string.Empty;
        state = string.Empty;
        zip = string.Empty;
        if (ddlClinicLocations.Visible && ddlClinicLocations.SelectedItem.Value != "0")
        {
            DataRow added_row = this.previousClinicLocation.Select("rowId='" + ddlClinicLocations.SelectedItem.Value + "'").FirstOrDefault();
            address1 = added_row["naClinicAddress1"].ToString();
            address2 = added_row["naClinicAddress2"].ToString();
            city = added_row["naClinicCity"].ToString();
            state = added_row["naClinicState"].ToString();
            zip = added_row["naClinicZip"].ToString();
        }
        clinic_ele.SetAttribute("clinicLocation", ((hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "UBICACIÓN DE LA CLÍNICA " : "CLINIC LOCATION ") + ((clinics_count >= 26) ? ((Char)(65 + (clinics_count % 26 == 0 ? Math.Ceiling(clinics_count / 26) - 1 : Math.Ceiling(clinics_count / 26) - 2))).ToString() + "" + ((Char)(65 + (clinics_count % 26))).ToString() : ((Char)(65 + clinics_count % 26)).ToString()));
        clinic_ele.SetAttribute("localContactName", string.Empty);
        clinic_ele.SetAttribute("LocalContactEmail", string.Empty);
        clinic_ele.SetAttribute("LocalContactPhone", string.Empty);
        clinic_ele.SetAttribute("clinicDate", string.Empty);
        clinic_ele.SetAttribute("startTime", string.Empty);
        clinic_ele.SetAttribute("endTime", string.Empty);
        clinic_ele.SetAttribute("Address1", address1);
        clinic_ele.SetAttribute("Address2", address2);
        clinic_ele.SetAttribute("city", city);
        clinic_ele.SetAttribute("state", state);
        clinic_ele.SetAttribute("zipCode", zip);
        clinic_ele.SetAttribute("isReassign", "0");
        clinic_ele.SetAttribute("isRemoved", "0");

        //Add selected immunizations to the new clinic location
        if (!is_default)
        {
            XmlNodeList clinic_immunizations = clinic_locations_node.FirstChild.ChildNodes;

            foreach (XmlElement immunization in clinic_immunizations)
            {
                XmlElement estshots_node;
                estshots_node = this.xmlContractAgreement.CreateElement("immunization");
                estshots_node.SetAttribute("pk", immunization.Attributes["pk"].Value);
                estshots_node.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
                estshots_node.SetAttribute("estimatedQuantity", string.Empty);
                clinic_ele.AppendChild(estshots_node);
            }
        }

        clinic_locations_node.AppendChild(clinic_ele);
        this.xmlContractAgreement.AppendChild(clinic_locations_node);
    }

    /// <summary>
    /// Gets and binds contract agreement
    /// </summary>
    private void bindContractAgreement()
    {
        DataSet ds_contract_agreement = new DataSet();
        DataTable dt_contract_agreement = new DataTable();

        //Get contract agreement from session if redirected from Spanish version
        if (Session["contractAgreement_" + this.contactLogPk.ToString()] != null)
            ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
        else
            ds_contract_agreement = dbOperation.getClinicAgreement("Walgreens", this.contactLogPk.ToString(), this.commonAppSession.LoginUserInfoSession.Email.ToString(), 2016, true, "Vote & Vax");

        Session["contractAgreement_" + this.contactLogPk.ToString()] = ds_contract_agreement;

        dt_contract_agreement = ds_contract_agreement.Tables[0]; //Contract agreement details

        //When contract agreement is deleted redirect user to home page for users coming from old community off-site clinic agreement email
        if ((dt_contract_agreement == null) || (dt_contract_agreement != null && dt_contract_agreement.Rows.Count == 0))
            Response.Redirect("walgreensHome.aspx");
        else
        {
            //Store Immunization checks and payment types in session
            this.dtImmunizations = ds_contract_agreement.Tables[1];

            if (ApplicationSettings.isRestrictedStoreState(dt_contract_agreement.Rows[0]["storeState"].ToString(), this.commonAppSession.LoginUserInfoSession.UserRole))
            {
                //this.previousClinicLocation = dbOperation.GetAllPreviousClinicLocations(Convert.ToInt32(dt_contract_agreement.Rows[0]["businessPk"].ToString()));

                //Remove from dropdown if only business location is added
                if (this.previousClinicLocation.Rows.Count == 1 && this.previousClinicLocation.Rows[0]["naClinicAddress"].ToString() == getBusinessLocationAddress())
                    this.previousClinicLocation.Rows.RemoveAt(0);
                if (this.previousClinicLocation.Rows.Count > 0)
                    this.isMOPreviousSeasonBusiness = true;
                this.isRestrictedStoreState = true;
                this.isAddressDisabled = true;
                if (!string.IsNullOrEmpty(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString()))
                {
                    this.xmlContractAgreement.LoadXml(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString());

                    foreach (XmlNode addedNode in this.xmlContractAgreement.SelectNodes("//Clinics/clinic"))
                    {
                        if ((addedNode.Attributes["isRemoved"] != null && addedNode.Attributes["isRemoved"].Value == "1") || (isMOPreviousSeasonBusiness && !isPreviousSeasonClinic(addedNode)))
                            addLocationToDropdown((XmlElement)addedNode);
                    }
                }
            }

            //Check if contract agreement is new            
            if (string.IsNullOrEmpty(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString()))
            {

                //Create default clinic location
                this.createNewClinicLocation(true);

                //Set business address details to default clinic location
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["Address1"].Value = dt_contract_agreement.Rows[0]["address"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["Address2"].Value = dt_contract_agreement.Rows[0]["address2"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["city"].Value = dt_contract_agreement.Rows[0]["city"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["state"].Value = dt_contract_agreement.Rows[0]["state"].ToString();
                this.xmlContractAgreement.SelectSingleNode("Clinics/clinic").Attributes["zipCode"].Value = dt_contract_agreement.Rows[0]["zip"].ToString();
                if (this.isMOPreviousSeasonBusiness)
                    this.bindClinicAgreemtPreviousLocationToDropdown();

                this.txtDistrictNum.Text = dt_contract_agreement.Rows[0]["districtNumber"].ToString();
                this.txtWalgreenTitle.Text = this.commonAppSession.LoginUserInfoSession.UserRole;
                this.PickerAndCalendarWalgreensDate.getSelectedDate = DateTime.Now;
                this.PickerAndCalendarWalgreensDate.MinDate = DateTime.Now.AddDays(-1);
                this.lblClientDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                //this.PickerAndCalendarClientDate.getSelectedDate = DateTime.Now;
                //this.PickerAndCalendarClientDate.MinDate = DateTime.Now.AddDays(-1);

            }
            else
            {
                this.isContractApproved = dt_contract_agreement.Rows[0]["isApproved"].ToString();
                this.isEmailSent = Convert.ToBoolean(dt_contract_agreement.Rows[0]["isEmailSent"].ToString());

                this.xmlContractAgreement.LoadXml(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString());
                this.bindClientAgreementXml();
            }

            this.hfLastUpdatedDate.Value = dt_contract_agreement.Rows[0]["updatedDate"].ToString();

            this.txtElectronicSign.Text = dt_contract_agreement.Rows[0]["signature"].ToString();
            this.hfBusinessName.Value = ds_contract_agreement.Tables[0].Rows[0]["businessName"].ToString(); //business name

            //Add isReassign attribute if not exists in the clinic locations node
            foreach (XmlNode clinic in this.xmlContractAgreement.SelectNodes("//Clinics/clinic"))
            {
                if (clinic.Attributes["isReassign"] == null)
                    ((XmlElement)clinic).SetAttribute("isReassign", "0");
            }

            //Bind clinic locations, immunization checks and payment types
            this.bindClinicLocations();
            this.bindImmunizationChecks();
            if (this.isMOPreviousSeasonBusiness)
                bindClinicAgreemtPreviousLocationToDropdown();

            //setting previous clinics clinic agreement controls 
            if (Convert.ToBoolean(dt_contract_agreement.Rows[0]["isPreviousSeason"].ToString()))
            {
                this.btnSaveNoContract.Visible = false;
                this.btnSendLater.Visible = false;
                this.disableControls(true);
                if (!string.IsNullOrEmpty(this.isContractApproved) && Convert.ToBoolean(this.isContractApproved))
                    this.btnSendMail.Visible = true;
                else
                    this.btnSendMail.Visible = false;
            }
            //Set controls visiblity based on Contract Agreement Email Sent or Approved/Rejected status
            else if (!string.IsNullOrEmpty(this.isContractApproved))
            {
                //Contract Agreement approved
                if (Convert.ToBoolean(this.isContractApproved))
                {
                    this.disableControls(true);
                    this.rbtnApprove.Checked = true;
                }
                else
                {
                    this.rbtnReject.Checked = true;
                    this.trNotes.Visible = true;
                    this.txtNotes.Text = dt_contract_agreement.Rows[0]["notes"].ToString();
                    this.txtNotes.Enabled = false;
                }

                this.txtElectronicSign.Enabled = false;
                this.tblApproval.Visible = true;
                this.tblApproval.Disabled = true;
                this.rbtnApprove.Enabled = false;
                this.rbtnReject.Enabled = false;
                this.btnSaveNoContract.Visible = false;
                this.btnSendLater.Visible = false;
            }
            else if (Convert.ToBoolean(dt_contract_agreement.Rows[0]["isEmailSent"].ToString()))
            {
                this.btnSaveNoContract.Visible = false;
                this.btnSendLater.Visible = false;
                //this.disableControls(true);
            }
        }
    }

    /// <summary>
    /// Binds clinic locations to gridview control
    /// </summary>
    private void bindClinicLocations()
    {
        StringReader location_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").OuterXml);
        DataSet location_dataset = new DataSet();
        location_dataset.ReadXml(location_reader);

        this.hasMutlipleClinics = (location_dataset.Tables[0].Rows.Count > 1) ? true : false;
        this.rowReassignClinicStore.Visible = this.hasMutlipleClinics;
        if (!string.IsNullOrEmpty(this.isContractApproved))
            this.rowReassignClinicStore.Visible = !Convert.ToBoolean(this.isContractApproved);

        grdLocations.DataSource = location_dataset.Tables[0].Columns.Contains("isRemoved") ? location_dataset.Tables[0].Select("isRemoved=0 OR isRemoved is null").OrderBy(r => r["clinicLocation"]).CopyToDataTable() : location_dataset.Tables[0];
        grdLocations.DataBind();
    }

    /// <summary>
    /// Binds immunizations to clinic location grid
    /// </summary>
    /// <param name="grd_clinic_immunizations"></param>
    /// <param name="clinic_number"></param>
    private void bindClinicImmunizations(GridView grd_clinic_immunizations, int clinic_number)
    {
        StringReader clinic_immunizations_reader = new StringReader(this.xmlContractAgreement.SelectSingleNode("//Clinics").ChildNodes[clinic_number].OuterXml);
        DataSet ds_clinic_immunizations = new DataSet();
        ds_clinic_immunizations.ReadXml(clinic_immunizations_reader);

        if (ds_clinic_immunizations.Tables.Count > 1 && ds_clinic_immunizations.Tables[1].Rows.Count > 0)
        {
            grd_clinic_immunizations.DataSource = ds_clinic_immunizations.Tables[1];
            grd_clinic_immunizations.DataBind();
        }
    }

    /// <summary>
    /// Binds contract agreement details
    /// </summary>
    private void bindClientAgreementXml()
    {
        if (this.xmlContractAgreement.SelectSingleNode("//Immunizations") != null)
        {
            XmlNodeList immunizations_checks = this.xmlContractAgreement.SelectNodes("//Immunizations/Immunization");
            foreach (XmlNode immunization in immunizations_checks)
            {
                DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = '" + immunization.Attributes["pk"].Value + "' AND paymentTypeId = '" + immunization.Attributes["paymentTypeId"].Value + "'");
                dr_immunization[0]["isSelected"] = "True";
                dr_immunization[0]["isPaymentSelected"] = "True";
                dr_immunization[0]["name"] = !string.IsNullOrEmpty(immunization.Attributes["name"].Value) ? immunization.Attributes["name"].Value : null;
                dr_immunization[0]["address1"] = !string.IsNullOrEmpty(immunization.Attributes["address1"].Value) ? immunization.Attributes["address1"].Value : null;
                dr_immunization[0]["address2"] = !string.IsNullOrEmpty(immunization.Attributes["address2"].Value) ? immunization.Attributes["address2"].Value : null;
                dr_immunization[0]["city"] = !string.IsNullOrEmpty(immunization.Attributes["city"].Value) ? immunization.Attributes["city"].Value : null;
                dr_immunization[0]["state"] = !string.IsNullOrEmpty(immunization.Attributes["state"].Value) ? immunization.Attributes["state"].Value : null;
                dr_immunization[0]["zip"] = !string.IsNullOrEmpty(immunization.Attributes["zip"].Value) ? immunization.Attributes["zip"].Value : null;
                dr_immunization[0]["phone"] = !string.IsNullOrEmpty(immunization.Attributes["phone"].Value) ? immunization.Attributes["phone"].Value : null;
                dr_immunization[0]["email"] = !string.IsNullOrEmpty(immunization.Attributes["email"].Value) ? immunization.Attributes["email"].Value : null;
                dr_immunization[0]["tax"] = !string.IsNullOrEmpty(immunization.Attributes["tax"].Value) ? immunization.Attributes["tax"].Value : null;
                dr_immunization[0]["copay"] = !string.IsNullOrEmpty(immunization.Attributes["copay"].Value) ? immunization.Attributes["copay"].Value : null;
                dr_immunization[0]["copayValue"] = !string.IsNullOrEmpty(immunization.Attributes["copayValue"].Value) ? immunization.Attributes["copayValue"].Value : null;
                dr_immunization[0]["isVoucherNeeded"] = !string.IsNullOrEmpty(immunization.Attributes["isVoucherNeeded"].Value) ? immunization.Attributes["isVoucherNeeded"].Value : null;
                dr_immunization[0]["voucherExpirationDate"] = !string.IsNullOrEmpty(immunization.Attributes["voucherExpirationDate"].Value) ? immunization.Attributes["voucherExpirationDate"].Value : null;
                if (!string.IsNullOrEmpty(this.isContractApproved))
                {
                    if (Convert.ToBoolean(this.isContractApproved) && !string.IsNullOrEmpty(immunization.Attributes["price"].Value))
                    {
                        if (immunization.Attributes["paymentTypeName"].Value == "Corporate to Invoice Employer Directly")
                            dr_immunization[0]["directBillPrice"] = Convert.ToDecimal(immunization.Attributes["price"].Value);
                        else
                            dr_immunization[0]["price"] = Convert.ToDecimal(immunization.Attributes["price"].Value);
                    }
                }
                else if (this.isEmailSent && !string.IsNullOrEmpty(immunization.Attributes["price"].Value))
                {
                    if (immunization.Attributes["paymentTypeName"].Value == "Corporate to Invoice Employer Directly")
                        dr_immunization[0]["directBillPrice"] = Convert.ToDecimal(immunization.Attributes["price"].Value);
                    else
                        dr_immunization[0]["price"] = Convert.ToDecimal(immunization.Attributes["price"].Value);
                }
            }
        }

        //Bind Client details
        this.txtClient.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@header").Value.ToString();
        this.txtClientName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@name").Value.ToString();
        this.txtClientTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@title").Value.ToString();
        //this.PickerAndCalendarClientDate.getSelectedDate = !string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString())? Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString()):DateTime.Today;
        //this.txtClientDate.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString();

        this.lblClientDate.Text = (!string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString()) && !string.IsNullOrEmpty(this.isContractApproved) && Convert.ToBoolean(this.isContractApproved) ? Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString()) : DateTime.Today).ToString("MM/dd/yyyy");

        //Binds Walgreen CO. details
        this.txtWalgreenName.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@name").Value.ToString();
        this.txtWalgreenTitle.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@title").Value.ToString();
        this.PickerAndCalendarWalgreensDate.getSelectedDate = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value.ToString());
        this.txtWalgreensDate.Text = Convert.ToDateTime(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value.ToString()).ToString("MM/dd/yyyy");
        this.txtDistrictNum.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@districtNumber").Value.ToString();

        //Binds Legal Notice details
        this.txtAttentionTo.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@attentionTo").Value.ToString();
        this.txtLegalAddress1.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address1").Value.ToString();
        this.txtLegalAddress2.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address2").Value.ToString();
        this.txtLegalCity.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@city").Value.ToString();
        //if (!string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString()))
        if (this.ddlLegalState.Items.FindByValue(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString()) != null)
            this.ddlLegalState.Items.FindByValue(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString()).Selected = true;
        //this.ddlLegalState.SelectedItem.Text = !string.IsNullOrEmpty(this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString()) ? this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString() : "-- Select --";
        this.txtLegalZip.Text = this.xmlContractAgreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@zipCode").Value.ToString();

        //this.PickerAndCalendarWalgreensDate.MinDate = DateTime.Now.AddDays(-1);
        //this.PickerAndCalendarClientDate.MinDate = DateTime.Now.AddDays(-1);
    }

    /// <summary>
    /// Binds selected immunization checks to immunization grid
    /// </summary>
    private void bindImmunizationChecks()
    {
        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        this.updatePaymentTypeData();
        this.xmlContractAgreement = this.updateClinicLocations(-1);

        //DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + lbl_immunization_check_id.Text + " AND paymentTypeId = " + lbl_payment_type_id.Text + " AND isSelected = 'False' AND isPaymentSelected = 'False'");
        DataRow[] dr_immunization = this.dtImmunizations.Select();
        if (dr_immunization != null && dr_immunization.Count() > 0)
        {
            dr_immunization[0]["isSelected"] = "True";
            dr_immunization[0]["isPaymentSelected"] = "True";

            XmlNodeList clinic_locations = this.xmlContractAgreement.SelectNodes("Clinics/clinic");
            foreach (XmlElement clinic in clinic_locations)
            {
                XmlElement est_shots = this.xmlContractAgreement.CreateElement("immunization");
                if (clinic.SelectSingleNode("./immunization[@pk = '" + dr_immunization[0]["immunizationId"].ToString() + "' and @paymentTypeId = '" + dr_immunization[0]["paymentTypeId"].ToString() + "']") == null)
                {
                    est_shots.SetAttribute("pk", dr_immunization[0]["immunizationId"].ToString());
                    est_shots.SetAttribute("paymentTypeId", dr_immunization[0]["paymentTypeId"].ToString());
                    est_shots.SetAttribute("estimatedQuantity", string.Empty);
                    clinic.AppendChild(est_shots);
                }
            }
        }

        if (this.grdImmunizationChecks.FooterRow != null && this.grdImmunizationChecks.FooterRow.Visible)
        {
            this.grdImmunizationChecks.ShowFooter = false;
            this.grdImmunizationChecks.FooterRow.Visible = false;
        }
        else if (this.grdImmunizationChecks.ShowFooter)
            this.grdImmunizationChecks.ShowFooter = false;

        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        this.contractCreatedDate = Convert.ToDateTime(((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[0].Rows[0]["dateCreated"].ToString());

        DataTable dt_immunizations_selected = new DataTable();

        dt_immunizations_selected = this.dtImmunizations.Select("isSelected = 'True' AND isPaymentSelected = 'True'").CopyToDataTable();
        this.grdImmunizationChecks.DataSource = dt_immunizations_selected;
        this.grdImmunizationChecks.DataBind();
        bindClinicLocations();
    }

    /// <summary>
    /// Binds Immunizations and Payment methods to dropdowns
    /// </summary>
    /// <param name="ddl_immunization_checks"></param>
    private void bindImmunizationsToDropdown()
    {
        DropDownList ddl_immunization_checks = new DropDownList();
        DropDownList ddl_payment_types = new DropDownList();
        this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();

        DataView dv_immunizations = new DataView((this.dtImmunizations.Select("isSelected = 'False' AND isActive = 1")).CopyToDataTable());
        DataTable dt_distinct_immunizations = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? dv_immunizations.ToTable(true, "immunizationId", "immunizationSpanishName") : dv_immunizations.ToTable(true, "immunizationId", "immunizationName");

        if (dt_distinct_immunizations.Rows.Count > 0)
        {
            ddl_immunization_checks.DataSource = dt_distinct_immunizations;
            ddl_immunization_checks.DataTextField = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "immunizationSpanishName" : "immunizationName";
            ddl_immunization_checks.DataValueField = "immunizationId";
            ddl_immunization_checks.DataBind();
            ddl_immunization_checks.SelectedIndex = ddl_immunization_checks.Items.IndexOf(ddl_immunization_checks.Items.FindByValue("Influenza – Standard/PF Injectable"));
        }

        dt_distinct_immunizations = null;
        dt_distinct_immunizations = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? dv_immunizations.ToTable(true, "paymentTypeId", "paymentTypeSpanishName") : dv_immunizations.ToTable(true, "paymentTypeId", "paymentTypeName");

        ddl_payment_types.DataSource = dt_distinct_immunizations;
        ddl_payment_types.DataTextField = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "paymentTypeSpanishName" : "paymentTypeName";
        ddl_payment_types.DataValueField = "paymentTypeId";
        ddl_payment_types.DataBind();
        ddl_payment_types.Items.Insert(0, (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? new ListItem("Seleccione el Método de pago", "") : new ListItem("Select Payment Method", ""));
    }

    /// <summary>
    /// Updates clinic locations details
    /// </summary>
    /// <param name="remove_clinic_num"></param>
    /// <returns></returns>
    private XmlDocument updateClinicLocations(int remove_clinic_num)
    {
        XmlDocument xdoc_clinic_locations = new XmlDocument();
        XmlElement clinic_locations_node = xdoc_clinic_locations.CreateElement("Clinics");
        XmlElement estshots_node;
        double clinic_location = 0;

        foreach (GridViewRow row in grdLocations.Rows)
        {

            XmlElement clinic_ele = xdoc_clinic_locations.CreateElement("clinic");
            clinic_ele.SetAttribute("clinicLocation", ((hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "UBICACIÓN DE LA CLÍNICA " : "CLINIC LOCATION ") + (clinic_location >= 26 ? ((Char)(65 + (clinic_location % 26 == 0 ? Math.Ceiling(clinic_location / 26) - 1 : Math.Ceiling(clinic_location / 26) - 2))).ToString() + "" + (char)(65 + (clinic_location % 26)) : ((char)(65 + clinic_location % 26)).ToString()));
            clinic_ele.SetAttribute("localContactName", ((TextBox)row.FindControl("txtLocalContactName")).Text.Trim());
            clinic_ele.SetAttribute("LocalContactEmail", ((TextBox)row.FindControl("txtLocalContactEmail")).Text.Trim());
            clinic_ele.SetAttribute("LocalContactPhone", ((TextBox)row.FindControl("txtLocalContactPhone")).Text.Trim());
            if (((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                clinic_ele.SetAttribute("clinicDate", ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy"));
            else
                clinic_ele.SetAttribute("clinicDate", "");

            clinic_ele.SetAttribute("startTime", ((TextBox)row.FindControl("txtStartTime")).Text);
            clinic_ele.SetAttribute("endTime", ((TextBox)row.FindControl("txtEndTime")).Text);
            clinic_ele.SetAttribute("Address1", ((TextBox)row.FindControl("txtAddress1")).Text.Trim());
            clinic_ele.SetAttribute("Address2", ((TextBox)row.FindControl("txtAddress2")).Text.Trim());
            clinic_ele.SetAttribute("city", ((TextBox)row.FindControl("txtCity")).Text.Trim());
            clinic_ele.SetAttribute("state", ((DropDownList)row.FindControl("ddlState")).SelectedValue);
            clinic_ele.SetAttribute("zipCode", ((TextBox)row.FindControl("txtZipCode")).Text);
            clinic_ele.SetAttribute("isReassign", (((CheckBox)row.FindControl("chkReassignClinic")) != null) ? Convert.ToInt32(((CheckBox)row.FindControl("chkReassignClinic")).Checked).ToString() : "0");

            GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
            if (grd_clinic_immunizations.Rows.Count > 0)
            {
                foreach (GridViewRow clinic_immunization in grd_clinic_immunizations.Rows)
                {
                    DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + ((Label)clinic_immunization.FindControl("lblImmunizationId")).Text + " AND paymentTypeId = " + ((Label)clinic_immunization.FindControl("lblPaymentTypeId")).Text + " AND isSelected = 'True' AND isPaymentSelected = 'True'");
                    if (dr_immunization.Count() > 0)
                    {
                        estshots_node = xdoc_clinic_locations.CreateElement("immunization");
                        estshots_node.SetAttribute("pk", ((Label)clinic_immunization.FindControl("lblImmunizationId")).Text);
                        estshots_node.SetAttribute("paymentTypeId", ((Label)clinic_immunization.FindControl("lblPaymentTypeId")).Text);
                        estshots_node.SetAttribute("estimatedQuantity", !string.IsNullOrEmpty(((TextBox)clinic_immunization.FindControl("txtClinicImmunizationShots")).Text.Trim()) ? Convert.ToInt32(((TextBox)clinic_immunization.FindControl("txtClinicImmunizationShots")).Text).ToString() : "");

                        clinic_ele.AppendChild(estshots_node);
                    }
                }
            }

            if (row.RowIndex != remove_clinic_num)
            {
                clinic_ele.SetAttribute("isRemoved", "0");
                clinic_locations_node.AppendChild(clinic_ele);
                clinic_location++;
            }
            else if (this.isRestrictedStoreState && remove_clinic_num > 0 && !isPreviousSeasonClinic(clinic_ele) && getAddressFromClinicXML(clinic_ele) != getBusinessLocationAddress())
            {
                clinic_ele.SetAttribute("isRemoved", "1");
                clinic_locations_node.AppendChild(clinic_ele);
                addLocationToDropdown(clinic_ele);
            }
        }
        xdoc_clinic_locations.AppendChild(clinic_locations_node);
        return xdoc_clinic_locations;
    }

    /// <summary>
    /// Updates Immunization Payment method details
    /// </summary>
    private void updatePaymentTypeData()
    {
        string voucher_needed = string.Empty;
        int immunization_id = 0;
        int payment_type_id = 0;

        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        foreach (GridViewRow row in this.grdImmunizationChecks.Rows)
        {
            Int32.TryParse(((Label)row.FindControl("lblImmunizationPk")).Text, out immunization_id);
            Int32.TryParse(((Label)row.FindControl("lblPaymentTypeId")).Text, out payment_type_id);

            if (immunization_id > 0 && payment_type_id > 0)
            {
                DataRow[] dr_immunization = this.dtImmunizations.Select("immunizationId = " + immunization_id + " AND paymentTypeId = " + payment_type_id);
                if (dr_immunization.Count() > 0)
                {

                    if (voucher_needed == "Yes")
                    {
                        if (((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                            dr_immunization[0]["voucherExpirationDate"] = ((PickerAndCalendar)row.FindControl("pcVaccineExpirationDate")).getSelectedDate.ToString("MM/dd/yyyy");
                    }
                    else
                        dr_immunization[0]["voucherExpirationDate"] = "";
                }
            }
        }
    }

    /// <summary>
    /// Creates Contract Agreement XML document
    /// </summary>
    private void prepareClinicAggreementXML(bool on_culture_change)
    {
        //Immunizations and Payment types
        this.updatePaymentTypeData();

        XmlElement clinic_agreement_ele = this.xmlContractAgreement.CreateElement("ClinicAgreement");
        XmlDocument xdoc_clinic_locations = new XmlDocument();
        xdoc_clinic_locations = this.updateClinicLocations(-1);
        XmlElement clinic_locations_node = (XmlElement)xdoc_clinic_locations.SelectSingleNode("Clinics");
        double clinic_location = this.previousClinicLocation.Rows.Count;
        if (this.previousClinicLocation.Rows.Count > 0)
        {
            foreach (DataRow row in this.previousClinicLocation.Select("isRemoved=1"))
            {
                XmlElement clinic_ele = xdoc_clinic_locations.CreateElement("clinic");
                clinic_ele.SetAttribute("clinicLocation", ((hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "UBICACIÓN DE LA CLÍNICA " : "CLINIC LOCATION ") + (clinic_location >= 26 ? ((Char)(65 + (clinic_location % 26 == 0 ? Math.Ceiling(clinic_location / 26) - 1 : Math.Ceiling(clinic_location / 26) - 2))).ToString() + "" + (char)(65 + (clinic_location % 26)) : ((char)(65 + clinic_location % 26)).ToString()));
                clinic_ele.SetAttribute("localContactName", string.Empty);
                clinic_ele.SetAttribute("LocalContactEmail", string.Empty);
                clinic_ele.SetAttribute("LocalContactPhone", string.Empty);
                clinic_ele.SetAttribute("clinicDate", string.Empty);
                clinic_ele.SetAttribute("startTime", string.Empty);
                clinic_ele.SetAttribute("endTime", string.Empty);
                clinic_ele.SetAttribute("Address1", row["naClinicAddress1"].ToString());
                clinic_ele.SetAttribute("Address2", row["naClinicAddress2"].ToString());
                clinic_ele.SetAttribute("city", row["naClinicCity"].ToString());
                clinic_ele.SetAttribute("state", row["naClinicState"].ToString());
                clinic_ele.SetAttribute("zipCode", row["naClinicZip"].ToString());
                clinic_ele.SetAttribute("isReassign", "0");
                clinic_ele.SetAttribute("isRemoved", Convert.ToBoolean(row["isRemoved"]) ? "1" : "0");
                clinic_locations_node.AppendChild(clinic_ele);
            }
        }


        xdoc_clinic_locations.AppendChild(clinic_locations_node);

        //Update clinic locations details
        XmlDocumentFragment xml_frag = this.xmlContractAgreement.CreateDocumentFragment();
        xml_frag.InnerXml = xdoc_clinic_locations.OuterXml;
        clinic_agreement_ele.AppendChild(xml_frag);

        this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
        DataRow[] dr_selected_immunizations = this.dtImmunizations.Select("isSelected = 'True' AND isPaymentSelected = 'True'");
        XmlElement immunizations = this.xmlContractAgreement.CreateElement("Immunizations");
        foreach (DataRow row in dr_selected_immunizations)
        {
            XmlElement immunization = this.xmlContractAgreement.CreateElement("Immunization");

            immunization.SetAttribute("pk", row["immunizationId"].ToString());
            immunization.SetAttribute("immunizationName", row["immunizationName"].ToString());
            immunization.SetAttribute("immunizationSpanishName", row["immunizationSpanishName"].ToString());
            immunization.SetAttribute("price", ((Convert.ToInt32(row["paymentTypeId"].ToString()) == 6) ? row["directBillPrice"].ToString() : row["price"].ToString()));
            immunization.SetAttribute("paymentTypeId", row["paymentTypeId"].ToString());
            immunization.SetAttribute("paymentTypeName", row["paymentTypeName"].ToString());
            immunization.SetAttribute("paymentTypeSpanishName", row["paymentTypeSpanishName"].ToString());
            immunization.SetAttribute("name", row["name"] != DBNull.Value ? row["name"].ToString() : string.Empty);
            immunization.SetAttribute("address1", row["address1"] != DBNull.Value ? row["address1"].ToString() : string.Empty);
            immunization.SetAttribute("address2", row["address2"] != DBNull.Value ? row["address2"].ToString() : string.Empty);
            immunization.SetAttribute("city", row["city"] != DBNull.Value ? row["city"].ToString() : string.Empty);
            immunization.SetAttribute("state", row["state"] != DBNull.Value ? row["state"].ToString() : string.Empty);
            immunization.SetAttribute("zip", row["zip"] != DBNull.Value ? row["zip"].ToString() : string.Empty);
            immunization.SetAttribute("phone", row["phone"] != DBNull.Value ? row["phone"].ToString() : string.Empty);
            immunization.SetAttribute("email", row["email"] != DBNull.Value ? row["email"].ToString() : string.Empty);
            immunization.SetAttribute("tax", row["tax"] != DBNull.Value ? row["tax"].ToString() : string.Empty);
            immunization.SetAttribute("copay", row["copay"] != DBNull.Value ? row["copay"].ToString() : string.Empty);
            immunization.SetAttribute("copayValue", row["copayValue"] != DBNull.Value ? row["copayValue"].ToString() : string.Empty);
            immunization.SetAttribute("isVoucherNeeded", row["isVoucherNeeded"] != DBNull.Value ? row["isVoucherNeeded"].ToString() : string.Empty);
            immunization.SetAttribute("voucherExpirationDate", row["voucherExpirationDate"] != DBNull.Value ? row["voucherExpirationDate"].ToString() : string.Empty);

            immunizations.AppendChild(immunization);
        }
        clinic_agreement_ele.AppendChild(immunizations);

        XmlElement client = this.xmlContractAgreement.CreateElement("Client");
        client.SetAttribute("header", this.txtClient.Text.Trim());
        client.SetAttribute("name", this.txtClientName.Text.Trim());
        client.SetAttribute("title", this.txtClientTitle.Text.Trim());
        //client.SetAttribute("date", this.PickerAndCalendarClientDate.getSelectedDate.ToString());
        client.SetAttribute("date", lblClientDate.Text);
        clinic_agreement_ele.AppendChild(client);

        XmlElement walgreens_co = this.xmlContractAgreement.CreateElement("WalgreenCo");
        walgreens_co.SetAttribute("name", this.txtWalgreenName.Text.Trim());
        walgreens_co.SetAttribute("title", this.txtWalgreenTitle.Text.Trim());
        walgreens_co.SetAttribute("date", this.PickerAndCalendarWalgreensDate.getSelectedDate.ToString());
        walgreens_co.SetAttribute("districtNumber", this.txtDistrictNum.Text.Trim());
        clinic_agreement_ele.AppendChild(walgreens_co);

        XmlElement legal_notic_address = this.xmlContractAgreement.CreateElement("LegalNoticeAddress");
        legal_notic_address.SetAttribute("attentionTo", this.txtAttentionTo.Text.Trim());
        legal_notic_address.SetAttribute("address1", this.txtLegalAddress1.Text.Trim());
        legal_notic_address.SetAttribute("address2", this.txtLegalAddress2.Text.Trim());
        legal_notic_address.SetAttribute("city", this.txtLegalCity.Text.Trim());
        legal_notic_address.SetAttribute("state", this.ddlLegalState.SelectedItem.Value);
        legal_notic_address.SetAttribute("zipCode", this.txtLegalZip.Text.Trim());
        clinic_agreement_ele.AppendChild(legal_notic_address);

        this.xmlContractAgreement.AppendChild(clinic_agreement_ele);

        if (!on_culture_change)
        {
            //Change clinic location name from Spanish to English
            foreach (XmlNode clinic in this.xmlContractAgreement.SelectNodes("//Clinics/clinic"))
            {
                clinic.Attributes["clinicLocation"].Value = clinic.Attributes["clinicLocation"].Value.Replace("UBICACIÓN DE LA CLÍNICA", "CLINIC LOCATION");
            }
        }
    }

    /// <summary>
    /// Disables controls
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableControls(bool is_disable)
    {
        this.disableCommunityAgreement(is_disable, this.tblApproval.Controls);
        this.disableCommunityAgreement(is_disable, this.tblAgreement.Controls);
        this.disableGridControls(is_disable, this.grdLocations);
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            GridView grd_view = (GridView)row.FindControl("grdClinicImmunizations");
            this.disableGridControls(is_disable, grd_view);

            Label lbl_clinic_date = (Label)row.FindControl("lblClinicDateAlert");
            if (lbl_clinic_date != null)
            {
                lbl_clinic_date.Visible = false;
            }
        }

        this.disableGridControls(is_disable, this.grdImmunizationChecks);

        this.btnSaveNoContract.Visible = !is_disable;
        this.btnSendLater.Visible = !is_disable;
        this.lnkAddLocation.Visible = !is_disable;
        this.btnAddLocation.Visible = !is_disable;
        this.grdImmunizationChecks.Enabled = !is_disable;
        this.ddlClinicLocations.Visible = this.ddlClinicLocations.Visible ? !is_disable : this.ddlClinicLocations.Visible;
        this.imgbtnAddPreviousClinic.Visible = this.imgbtnAddPreviousClinic.Visible ? !is_disable : this.imgbtnAddPreviousClinic.Visible;
    }

    /// <summary>
    /// Disables all input controls in the page
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableCommunityAgreement(bool disable, ControlCollection ctrl_main)
    {
        foreach (Control page_ctrl in ctrl_main)
        {
            foreach (Control ctrl1 in page_ctrl.Controls)
            {
                foreach (Control ctrl in ctrl1.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        ((TextBox)ctrl).Enabled = !disable;
                        if (((TextBox)ctrl).ID == "txtGroupDate" || ((TextBox)ctrl).ID == "txtWalgreensDate" /*|| ((TextBox)ctrl).ID == "txtClientDate"*/)
                        {
                            ((TextBox)ctrl).Visible = true;
                            this.lnkPrintContract.Visible = true;
                        }
                    }

                    if (ctrl is RadioButton)
                        ((RadioButton)ctrl).Enabled = !disable;

                    if (ctrl is DropDownList)
                        ((DropDownList)ctrl).Enabled = !disable;

                    if (ctrl is PickerAndCalendar)
                        ((PickerAndCalendar)ctrl).Visible = false;
                }

                if (page_ctrl is System.Web.UI.HtmlControls.HtmlTableCell)
                {
                    if (ctrl1 is TextBox)
                    {
                        ((TextBox)ctrl1).Enabled = !disable;
                        if (((TextBox)ctrl1).ID.Contains("txtVaccineExpirationDate"))
                            ((TextBox)ctrl1).Visible = true;
                    }

                    if (ctrl1 is DropDownList)
                        ((DropDownList)ctrl1).Enabled = !disable;

                    if (ctrl1 is CheckBox)
                        ((CheckBox)ctrl1).Enabled = !disable;

                    if (ctrl1 is PickerAndCalendar)
                        ((PickerAndCalendar)ctrl1).Visible = false;

                    if (ctrl1 is ImageButton)
                        ((ImageButton)ctrl1).Visible = !disable;
                }
            }
        }
    }

    /// <summary>
    /// Disables grid controls
    /// </summary>
    /// <param name="disable"></param>
    /// <param name="grd_ctrl"></param>
    private void disableGridControls(bool disable, GridView grd_ctrl)
    {
        foreach (GridViewRow row in grd_ctrl.Rows)
        {
            foreach (Control ctrl in row.Controls)
            {
                foreach (Control ctrl1 in ctrl.Controls)
                {
                    if (ctrl1 is TextBox)
                    {
                        ((TextBox)ctrl1).Enabled = !disable;
                        if (((TextBox)ctrl1).ID.Contains("txtCalenderFrom"))
                            ((TextBox)ctrl1).Visible = true;
                    }

                    if (ctrl1 is DropDownList)
                        ((DropDownList)ctrl1).Enabled = !disable;

                    if (ctrl1 is CheckBox)
                        ((CheckBox)ctrl1).Enabled = !disable;

                    if (ctrl1 is PickerAndCalendar)
                        ((PickerAndCalendar)ctrl1).Visible = false;
                    if (ctrl1 is ImageButton)
                        ((ImageButton)ctrl1).Visible = !disable;
                }
            }
        }
    }

    /// <summary>
    /// Setting image urls based on culture
    /// </summary>
    private void setImageButtons()
    {
        if (!String.IsNullOrEmpty(hfLanguge.Value))
        {
            btnSendMail.ImageUrl = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "images/btn_send_email_SP.png" : "images/btn_send_email.png";
            btnSaveNoContract.ImageUrl = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "images/btn_save_no_contract_SP.png" : "images/btn_save_no_contract.png";
            btnCancel.ImageUrl = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "images/btn_cancel_SP.png" : "images/btn_cancel_mini.png";
            btnSendLater.ImageUrl = (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR") ? "images/btn_save_send_later_SP.png" : "images/btn_save_send_later.png";
            //onmouseout="javascript:MouseOutImage(this.id,'images/btn_send_email.png');" 
            //onmouseover="javascript:MouseOverImage(this.id,'images/btn_send_email_lit.png');"
            if (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR")
            {
                btnSendMail.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_send_email_SP.png');");
                btnSaveNoContract.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_save_no_contract_SP.png');");
                btnCancel.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_cancel_SP.png');");
                btnSendLater.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_save_send_later_SP.png');");
                btnSendMail.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_send_email_SP_lit.png');");
                btnSaveNoContract.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_save_no_contract_SP_lit.png');");
                btnCancel.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_cancel_SP_lit.png');");
                btnSendLater.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_save_send_later_SP_lit.png');");
            }
            else
            {
                btnSendMail.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_send_email.png');");
                btnSaveNoContract.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_save_no_contract.png');");
                btnCancel.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_cancel_mini.png');");
                btnSendLater.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'images/btn_save_send_later.png');");
                btnSendMail.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_send_email_lit.png');");
                btnSaveNoContract.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_save_no_contract_lit.png');");
                btnCancel.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_cancel_mini_lit.png');");
                btnSendLater.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'images/btn_save_send_later_lit.png');");
            }

        }
    }

    /// <summary>
    /// Getting Address from Clinic locations XML node
    /// </summary>
    /// <param name="clinic_location"></param>
    /// <returns></returns>
    private string getAddressFromClinicXML(XmlNode clinic_location)
    {
        return (clinic_location.Attributes["Address1"].Value.Length > 0 ? (clinic_location.Attributes["Address1"].Value + ", ") : "") + (clinic_location.Attributes["Address2"].Value.Length > 0 ? (clinic_location.Attributes["Address2"].Value + ", ") : "") + clinic_location.Attributes["city"].Value + ", " + clinic_location.Attributes["state"].Value + ", " + clinic_location.Attributes["zipCode"].Value;
    }

    /// <summary>
    /// Check if the current clinic location is a previous season clinic
    /// </summary>
    /// <param name="clinic_location"></param>
    /// <returns></returns>
    private bool isPreviousSeasonClinic(XmlNode clinic_location)
    {
        foreach (DataRow row in this.previousClinicLocation.Rows)
        {
            string address_Prev = row["naClinicAddress"].ToString();
            int count = 0;

            string address_exist = this.getAddressFromClinicXML(clinic_location);
            if (address_exist == address_Prev)
            {
                count = count + 1;
            }
            if (count > 0)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Convert location xml to datarow and add to previous locations table
    /// </summary>
    /// <param name="clinic_location"></param>
    /// <returns></returns>
    private void addLocationToDropdown(XmlElement clinic_location)
    {
        int rowId = this.previousClinicLocation.Rows.Count > 0 ? Convert.ToInt32(this.previousClinicLocation.Compute("Max(rowId)", "")) + 1 : 1;
        DataRow new_row = this.previousClinicLocation.NewRow();
        new_row["naClinicAddress1"] = clinic_location.GetAttribute("Address1").ToString();
        new_row["naClinicAddress2"] = clinic_location.GetAttribute("Address2").ToString();
        new_row["naClinicCity"] = clinic_location.GetAttribute("city").ToString();
        new_row["naClinicState"] = clinic_location.GetAttribute("state").ToString();
        new_row["naClinicZip"] = clinic_location.GetAttribute("zipCode").ToString();
        new_row["naClinicAddress"] = getAddressFromClinicXML(clinic_location);
        new_row["isRemoved"] = (clinic_location.GetAttribute("isRemoved") != null && clinic_location.GetAttribute("isRemoved").ToString() == "1") ? true : false;
        new_row["rowId"] = rowId;
        this.previousClinicLocation.Rows.Add(new_row);
    }

    /// <summary>
    ///  Bind the previous season clinic locations to dropdown
    /// </summary>
    /// <param></param>
    /// <returns></returns>
    private void bindClinicAgreemtPreviousLocationToDropdown()
    {
        if (this.previousClinicLocation.Rows.Count > 0)
        {
            ddlClinicLocations.DataSource = this.previousClinicLocation;
            ddlClinicLocations.DataTextField = "naClinicAddress";
            ddlClinicLocations.DataValueField = "rowId";
            ddlClinicLocations.DataBind();
            ddlClinicLocations.Items.Insert(0, new ListItem("Select Location", "0"));
            ddlClinicLocations.Visible = true;
            imgbtnAddPreviousClinic.Visible = true;
            lnkAddLocation.Visible = false;
            btnAddLocation.Visible = false;

        }
        else
        {
            ddlClinicLocations.Visible = false;
            imgbtnAddPreviousClinic.Visible = false;
            lnkAddLocation.Visible = true;
            btnAddLocation.Visible = true;
        }
    }

    /// <summary>
    /// Get the business location address
    /// </summary>
    private string getBusinessLocationAddress()
    {
        DataSet ds_contract_agreement = (DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()];
        DataTable dt_contract_agreement = ds_contract_agreement.Tables[0];
        string business_address = (dt_contract_agreement.Rows[0]["address"].ToString().Length > 0 ? (dt_contract_agreement.Rows[0]["address"].ToString() + ", ") : "") + (dt_contract_agreement.Rows[0]["address2"].ToString().Length > 0 ? (dt_contract_agreement.Rows[0]["address2"].ToString() + ", ") : "") + dt_contract_agreement.Rows[0]["city"].ToString() + ", " + dt_contract_agreement.Rows[0]["state"].ToString() + ", " + dt_contract_agreement.Rows[0]["zip"].ToString();
        return business_address;
    }

    /// <summary>
    /// Validates clinic duration between clinic date and scheduled date
    /// </summary>
    /// <returns></returns>
    private int validateClinicDates()
    {
        int alert_type = 0;
        DateTime clinic_date;
        bool has_clinic_after_2weeks = false;

        foreach (GridViewRow row in grdLocations.Rows)
        {
            var date_control = row.FindControl("PickerAndCalendarFrom");
            clinic_date = ((PickerAndCalendar)date_control).getSelectedDate;

            if (clinic_date != Convert.ToDateTime("1/1/0001") && clinic_date <= DateTime.Now.AddDays(14))
            {
                alert_type = 1;
                break;
            }
            else if (clinic_date != Convert.ToDateTime("1/1/0001") && clinic_date > DateTime.Now.AddDays(14))
                has_clinic_after_2weeks = true;
        }

        if (alert_type == 0 && has_clinic_after_2weeks)
            alert_type = 2;

        return alert_type;
    }

    /// <summary>
    /// to handle the send later button click
    /// </summary>
    private void saveSendLaterHandler()
    {
        if (this.ValidateAgreement("SendLater"))
        {
            this.prepareClinicAggreementXML(false);

            //Check if all the immunizations are added to clinics
            this.checkClinicImmunizations();

            string date_time_stamp_message = string.Empty;
            int return_value = this.dbOperation.insertUpdateClinicAgreement(this.contactLogPk, this.txtEmails.Text.Trim(), this.xmlContractAgreement.InnerXml, true, this.hfLastUpdatedDate.Value, false, out date_time_stamp_message);
            if (return_value >= 0)
            {
                Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreement") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
            else if (return_value == -6)
            {
                Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "AgreementStatusChanged") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
        }
        else if (this.paymentMethods == "[]")
        {
            this.dtImmunizations = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[1];
            this.paymentMethods = this.dtImmunizations.getImmunizationPaymentMethodsJSONObject();
        }
    }

    /// <summary>
    /// to handle the send Email button click
    /// </summary>
    private void sendEmailHandler()
    {
        this.prepareClinicAggreementXML(false);

        if (this.checkClinicImmunizations())
        {
            string date_time_stamp_message = string.Empty;

            //checking date and time stamp locally
            XmlNodeList clinic_location_nodes = xmlContractAgreement.SelectNodes("/ClinicAgreement/Clinics/clinic[@isReassign='0']");
            string failed_location = "";
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "localagreement", this.hfBusinessName.Value, string.Empty);
            if (!String.IsNullOrEmpty(failed_location))
            {
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", failed_location, true);
                return;
            }

            DataTable dt_contract_agreement = ((DataSet)Session["contractAgreement_" + this.contactLogPk.ToString()]).Tables[0];
            this.isContractApproved = (dt_contract_agreement.Rows.Count > 0) ? dt_contract_agreement.Rows[0]["isApproved"].ToString() : "";
            int return_value = this.dbOperation.insertUpdateClinicAgreement(this.contactLogPk, this.txtEmails.Text.Trim(), this.xmlContractAgreement.InnerXml, (!string.IsNullOrEmpty(this.isContractApproved) && Convert.ToBoolean(this.isContractApproved)), this.hfLastUpdatedDate.Value,false, out date_time_stamp_message);
            if (return_value >= 0)
            {
                EmailOperations email_operations = new EmailOperations();
                if (email_operations.sendClinicAgreementToBusinessUsers(this.txtEmails.Text, this.contactLogPk, true, "VoteVax"))
                {
                    Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreementMail") + "'); window.location.href = 'walgreensHome.aspx';", true);
                }
            }
            else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
            {
                this.bindClinicLocations();
                string validation_message = string.Empty;
                ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "localagreement", this.hfBusinessName.Value, date_time_stamp_message);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
                return;
            }
            else if (return_value == -5 || return_value == -6)
            {
                Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "AgreementStatusChanged") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
        }
        else
            this.bindClinicLocations();
    }

    /// <summary>
    /// to handle the Save no contract button click
    /// </summary>
    private void saveNoContractHandler()
    {
        this.prepareClinicAggreementXML(false);
        if (this.checkClinicImmunizations())
        {
            //Validate clinic date and time overlap between clinics in this contract
            string date_time_stamp_message = string.Empty;
            XmlNodeList clinic_location_nodes = xmlContractAgreement.SelectNodes("/ClinicAgreement/Clinics/clinic[@isReassign='0']");
            string failed_location = "";
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "localagreement", this.hfBusinessName.Value, string.Empty);
            if (!String.IsNullOrEmpty(failed_location))
            {
                this.bindClinicLocations();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", failed_location, true);
                return;
            }

            DataTable dt_approved_contract_details;
            int return_value = this.dbOperation.insertNoContractClinicAgreement(this.contactLogPk, this.xmlContractAgreement.InnerXml, this.commonAppSession.LoginUserInfoSession.UserName, out dt_approved_contract_details, out date_time_stamp_message);

            if (return_value == 0 || return_value == -3)
            {
                EmailOperations email_operations = new EmailOperations();
                DataTable dt_user_emails = dbOperation.getStoreUsersEmails(Convert.ToInt32(this.hfBusinessStoreId.Value));
                if (dt_user_emails.Rows.Count > 0)
                    email_operations.sendClinicAgreementToWalgreensUser(dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString(), dt_user_emails.Rows[0]["storeManagerEmail"].ToString(), this.hfBusinessName.Value, Convert.ToInt32(hfContactLogPk.Value), "VoteVax");

                //Sends clinic store re-assignment emails
                if (return_value == -3)
                {
                    if (dt_approved_contract_details != null && dt_approved_contract_details.Rows.Count > 0)
                    {
                        //Sends clinic store re-assigned email to contracted store user
                        email_operations.sendClinicStoreAutoGeoCodeReassignmentEmail(dt_user_emails, dt_approved_contract_details, "VoteVax");

                        //Sends clinic assigned email to the assigned store user
                        email_operations.sendLocalClinicStoreAssignmentEmail(dt_approved_contract_details, hfContactLogPk.Value);
                    }
                }

                Session.Remove("contractAgreement_" + this.contactLogPk.ToString());
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreement") + "'); window.location.href = 'walgreensHome.aspx';", true);

            }
            else if (return_value == -2 && !String.IsNullOrEmpty(date_time_stamp_message))
            {
                this.bindClinicLocations();
                string validation_message = string.Empty;
                ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "localagreement", this.hfBusinessName.Value, date_time_stamp_message);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
                return;
            }
        }
    }
    #endregion

    #region ------------ PRIVATE VARIABLES -----------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private int contactLogPk = 0;
    private bool hasMutlipleClinics = false;
    private string isContractApproved = string.Empty;
    private bool isEmailSent = false;
    private DataTable dtImmunizations;
    private XmlDocument xmlContractAgreement;
    protected string paymentMethods = "[]";
    private DateTime contractCreatedDate;
    //it will be true if any one of the immunizations has seleted from the following list 
    //Influenza - Standard/PF Injectable (trivalent)
    //Influenza - Standard injectable Quadrivalent
    //Influenza - High Dose
    private bool isImmunizationsBlackoutValidationRequired = false;
    private string[] blackedOutImmunizations
    {
        get
        {
            if (hfLanguge.Value == "es-MX" || hfLanguge.Value == "es-PR")
            {
                return new string[] { "influenza - vacuna inyectable trivalente en dosis estándar", "influenza - vacuna inyectable tetravalente en dosis estándar", "influenza - dosis alta" };
            }
            else
            {
                return new string[] { "influenza - standard/pf injectable (trivalent)", "influenza - standard injectable quadrivalent", "influenza - high dose" };
            }
        }
    }
    private bool isMOPreviousSeasonBusiness
    {
        get
        {
            return Convert.ToBoolean(hfIsMoPrevious.Value);
        }
        set
        {
            hfIsMoPrevious.Value = value.ToString();
        }
    }
    private DataTable previousClinicLocation
    {
        get
        {
            DataTable result = new DataTable();

            if (ViewState["previousClinicLocation"] != null)
            {
                result = (DataTable)ViewState["previousClinicLocation"];
            }
            return result;
        }
        set
        {
            ViewState["previousClinicLocation"] = value;
        }
    }
    private bool isAddressDisabled
    {
        get
        {
            bool value = false;
            if (ViewState["isAddressDisabled"] != null)
                value = (bool)ViewState["isAddressDisabled"];
            return value;
        }
        set
        {
            ViewState["isAddressDisabled"] = value;
        }
    }
    private bool isRestrictedStoreState
    {
        get
        {
            bool value = false;
            if (ViewState["isRestrictedStoreState"] != null)
                value = (bool)ViewState["isRestrictedStoreState"];
            return value;
        }
        set
        {
            ViewState["isRestrictedStoreState"] = value;
        }
    }
    #endregion
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        //ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.xmlContractAgreement = new XmlDocument();
        this.contractCreatedDate = DateTime.Now;

        if (Session.IsNewSession)
        {
            FormsAuthentication.SignOut();
            string str = Request.Url.ToString();
            Response.Redirect(str);
        }
    }

    protected override void InitializeCulture()
    {
        if (Request.Form["hfLanguge"] != null)
        {
            Thread.CurrentThread.CurrentCulture =
               CultureInfo.CreateSpecificCulture(Request.Form["hfLanguge"]);
            Thread.CurrentThread.CurrentUICulture = new
                CultureInfo(Request.Form["hfLanguge"]);
            Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo("en-US").DateTimeFormat;
        }
        base.InitializeCulture();
    }
}