﻿<%@ Application Language="C#" %>
<%@ Import Namespace="TdWalgreens" %>
<script RunAt="server">
    protected void Application_Start(object sender, EventArgs e)
    {
        Dictionary<int, string> wag_user = new Dictionary<int, string>();
        Application["WagUsers"] = wag_user;
    }

    protected void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        if (Server.GetLastError().GetBaseException() != null)
        {
            CustomExceptionHandler cust_exce = new CustomExceptionHandler();
            string log_string = string.Empty;
            HttpContext context = HttpContext.Current;

            if (context != null && context.Session != null)
            {
                if (Session["logString"] != null)
                {
                    log_string = Session["logString"].ToString();
                    if (Session["identifierString"] != null)
                        log_string += Session["identifierString"].ToString();
                }

            }
            string displayErrorMessage = cust_exce.getErrorMessage(Server.GetLastError().GetBaseException(), log_string);
            if (!string.IsNullOrEmpty(displayErrorMessage))
            {
                string[] folder = Request.Url.ToString().Split('/');

                HttpContext.Current.Session["displayErrorMessage"] = displayErrorMessage;
                if (folder.Contains("Admin") || folder.Contains("reports") || folder.Contains("corporateScheduler") 
                    || folder.Contains("corporateUploader") || folder.Contains("auth"))
                    Response.Redirect("../ErrorPage.aspx");
                else
                    Response.Redirect("ErrorPage.aspx");
            }

        }
    }

    protected void Application_AuthenticateRequest(object sender, System.EventArgs e)
    {
        Uri ReqUrl = HttpContext.Current.Request.Url;
        string[] ReqSegment = ReqUrl.Segments;
        string RequestPage = ReqSegment[ReqSegment.Length - 1];

        if (RequestPage.Length == 9 && Regex.IsMatch(RequestPage.Substring(3), "\\d+"))
        {
            RequestPage = "scheduledAppointmentPage";
        }

        if (ReqSegment.Where(x => x.Contains("walgreensClinicsCountService.asmx")).Count() > 0)
        {
            HttpContext.Current.SkipAuthorization = true;
        }
        switch (RequestPage.ToLower().Trim())
        {
            case "onsiteschedulerauth.ashx":
            case "passwordrecovery.aspx":
            case "thankyou.aspx":
            case "sessiontimeout.aspx":
            case "encryptedloginlink.aspx":
            case "scheduledclinicsvalidationreport.aspx":
            case "search.aspx":
            case "walgreensresolveduplication.aspx":
            case "walgreensclinicagreementforuser.aspx":
            case "walgreensclinicagreementprevseason.aspx":
            case "walgreensclinicagreementprevseason_sp.aspx":
            case "walgreensvotevaxagreementforuser.aspx":
            case "pickerandcalendar":
            case "walgreensnoaccess.htm":
            case "resourcefilehandler.ashx":
            case "displayimage.ashx":
            case "walgreensschedulerlanding.aspx":
            case "walgreensschedulerconfirmation.aspx":
            case "walgreensschedulerregistration.aspx":
            case "displayscheduledapptsreport.aspx":
            case "clientschedulerappttracker.aspx":
            case "reportscheduledappointmentforclients.aspx":
            case "clientschedulerthankyou.aspx":
            case "prefilledvarform.aspx":
            case "scheduledappointment.rdlc":
            case "reserved.reportviewerwebcontrol.axd":
                HttpContext.Current.SkipAuthorization = true;
                break;
            case "scheduledappointmentpage":
                Response.Redirect("~/onsiteSchedulerAuth.ashx?args=" + ReqSegment[ReqSegment.Count() - 1]);
                break;
        }
    }       
</script>
