<%@ Control Src="PickerAndCalendarWithTime.ascx.cs" AutoEventWireup="false" Inherits="PickerAndCalendarWithTime" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<script type="text/javascript">
  //<![CDATA[
  function onSelectionChanged(sender, eventArgs) {  
    var clickedControl = sender;
    var clickedControlId = sender.get_clientControlId();
    var associatedControlId = window.controlMappings[clickedControlId];
    var associatedControl = window[associatedControlId];
     var new_date = getDateString(clickedControl.getSelectedDate());  
    associatedControl.setSelectedDate(new Date(new_date));
  }
  function buttonOnClick(element, event)
  {
    var calendarId = window.controlMappings[element.id];
    var calendar = window[calendarId];
    if (calendar.get_popUpShowing())
    {
      calendar.hide();
    }
    else
    {
      var pickerId = window.controlMappings[calendarId];
      var picker = window[pickerId];
      if(picker.getSelectedDate()==null)
        calendar.setSelectedDate(new Date());
      else
      {
          var new_date = getDateString(picker.getSelectedDate());   
           calendar.setSelectedDate(new Date(new_date));
         }
     
      calendar.show(element);
    }
  }
  function buttonOnMouseUp(element, event)
  {
    var calendarId = window.controlMappings[element.id];
    var calendar = window[calendarId];
    if (calendar.get_popUpShowing())
    {
      event.cancelBubble = true;
      event.returnValue = false;
      return false;
    }
    else
    {
      return true;
    }

}
//function to set the date and time to the picket control ::added by seneca-Ticket 1665
function getDateString(date_selected)
  {
    var set_date = date_selected;
    var month = (set_date.getMonth()< 10) ? '0'+ (set_date.getMonth()+1) : (set_date.getMonth()+1);    
    var date = (set_date.getDate()< 10) ? '0'+ set_date.getDate() : set_date.getDate();
    var current_date = new Date();
    var hours = current_date.getHours();
    var minutes = current_date.getMinutes();
    var seconds = current_date.getSeconds();
    var new_date = month + "/" + date + "/" + set_date.getFullYear()+ " " + hours + ":" + minutes + ":" + seconds;
    return new_date;
  }
  
function disableDateBox() 
{ 
<%= Picker1.ClientID %>.PickerTokens[0].Unit = null; 
<%= Picker1.ClientID %>.PickerTokens[2].Unit = null; 
<%= Picker1.ClientID %>.PickerTokens[4].Unit = null; 
} 


  //]]>
</script>
<table cellspacing="1" cellpadding="0" border="0">
  <tr>
    <td runat="server" id="Td1" onmouseup="buttonOnMouseUp(this, event)">
    <ComponentArt:Calendar 
      id="Picker1" 
      runat="server" 
      PickerFormat="Custom" 
      PickerCustomFormat="MM/dd/yyyy HH:mm" 
      ControlType="Picker" 
      PickerCssClass="formFields2"
      DisabledDayActiveCssClass="datedisabled"
      DisabledDayCssClass="datedisabled"
      DisabledDayHoverCssClass="datedisabled" >
      <ClientEvents>
        <SelectionChanged EventHandler="onSelectionChanged" />
        <Load EventHandler="disableDateBox" />
      </ClientEvents>
      </ComponentArt:Calendar>
    </td>
    <td>
    <img runat="server" id="Img1" alt="" 
      onmouseup="buttonOnMouseUp(this, event)" 
      onclick="buttonOnClick(this, event)" 
      class="calendar_button" src="../images/btn_calendar.gif" width="25" height="22" /></td>
  </tr>

</table>
<ComponentArt:Calendar runat="server"
  id="Calendar1" 
  AllowMultipleSelection="false"
  AllowWeekSelection="false"
  AllowMonthSelection="false"
  ControlType="Calendar"
  PickerCustomFormat="MM/dd/yyyy HH:mm" 
  PopUp="Custom"
  DayNameFormat="FirstTwoLetters"
  ReactOnSameSelection ="true"
  AutoTheming="true"
  Width="150"
  Height="120">
                        
  <ClientEvents>
    <SelectionChanged EventHandler="onSelectionChanged" />
  </ClientEvents>
</ComponentArt:Calendar>

<asp:Literal runat="server" ID="ControlMappingScriptSpot">
</asp:Literal>