using System;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for WebForm1.
/// </summary>
public class PickerAndCalendarWithTime : System.Web.UI.UserControl
{
    protected ComponentArt.Web.UI.Calendar Calendar1;
    protected ComponentArt.Web.UI.Calendar Picker1;
    protected System.Web.UI.HtmlControls.HtmlImage Img1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Td1;
    protected System.Web.UI.WebControls.Literal ControlMappingScriptSpot;
    private Hashtable controlMappings = new Hashtable();


    #region ---------------- PUBLIC METHODS -----------
    public DateTime MaxDate
    {
        set
        {
            // required to control calendar control call Bill before changing
            this.Calendar1.VisibleDate = value;
            this.Calendar1.MaxDate = DateTime.Today.Date;
            DateTime date = DateTime.Today.Date;
            for (int d = 1; d < 32; d++)
            {
                this.Calendar1.DisabledDates.Add(date.AddDays(1 * d));
            }
            this.Calendar1.DisabledDayActiveCssClass = "datedisabled";
            this.Calendar1.DisabledDayHoverCssClass = "datedisabled";
            this.Calendar1.DisabledDayCssClass = "datedisabled";
        }
    }
    public DateTime AddDisabledDatesCollection
    {
        set
        {
            this.Calendar1.DisabledDates.Add(value);
            this.Picker1.DisabledDates.Add(value);
        }
    }
    public DateTime getSelectedDate
    {
        get
        {
            return Picker1.SelectedDate;
        }
        set
        {
            Calendar1.SelectedDate = value;
            Picker1.SelectedDate = value;
        }
    }

    public DateTime MinDate
    {
        set
        {
            // required to control calendar control call Bill before changing
            this.Calendar1.VisibleDate = value.AddDays(1);
            this.Calendar1.MinDate = value;
            this.Picker1.MinDate = value;
            DateTime date = value.AddDays(1);
            for (int d = 1; d < 32; d++)
            {
                this.Calendar1.DisabledDates.Add(date.AddDays(-1 * d));
            }
            this.Calendar1.DisabledDayActiveCssClass = "datedisabled";
            this.Calendar1.DisabledDayHoverCssClass = "datedisabled";
            this.Calendar1.DisabledDayCssClass = "datedisabled";
            this.Calendar1.MinDate = value;
            this.Picker1.MinDate = value;
        }
    }
    #endregion

    #region --------------- PROTECTED EVENTS ------------------
    protected void onSelectionChanged(Object sender, EventArgs e)
    {
        string message = "";

        // Iterate through the SelectedDates collection and display the
        // dates selected in the Calendar control.
        foreach (DateTime day in Calendar1.SelectedDates)
        {
            message = day.Date.ToShortDateString() + "<br />";
        }
    }
    #endregion

    #region ------------------ PRIVATE METHODS/EVENTS ------------
    private void Page_Load(object sender, EventArgs e)
    {
        this.controlMappings[this.Calendar1.ClientObjectId] = this.Picker1.ClientObjectId;
        this.controlMappings[this.Picker1.ClientObjectId] = this.Calendar1.ClientObjectId;
        this.controlMappings[this.Td1.ClientID] = this.Calendar1.ClientObjectId;
        this.controlMappings[this.Img1.ClientID] = this.Calendar1.ClientObjectId;
        this.ControlMappingScriptSpot.Text = this.GetControlMappingsScript();

    }

    private string GetControlMappingsScript()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<script type=\"text/javascript\">");
        sb.AppendLine("if (!window.controlMappings) window.controlMappings = {};");
        foreach (DictionaryEntry keyvalue in this.controlMappings)
        {
            string key = (string)keyvalue.Key;
            string value = (string)keyvalue.Value;
            sb.Append("window.controlMappings.").Append(key).Append(" = '").Append(value).Append("';\n");
        }
        sb.AppendLine("</script>");
        return sb.ToString();
    }
    #endregion

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);
    }
    #endregion



}
