﻿using System;
using System.Web.UI.WebControls;
using TdApplicationLib;

public partial class ActionItems : System.Web.UI.UserControl
{
    #region ------------------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void doProcess_Click(Object sender, CommandEventArgs e)
    {
        LinkButton lnk_store_count = (LinkButton)sender;
        if (lnk_store_count.CommandName.ToLower() == "store")
        {
            this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 2;
            Response.Redirect("walgreensHome.aspx", false);
        }
        else
        {
            this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId = Convert.ToInt32(e.CommandArgument.ToString());
            Response.Redirect("reports/reportActionItemsIP.aspx");
        }
    }

    protected void grdActionItems_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_action_itemid = (Label)e.Row.FindControl("lblActionItemId");
            Label lbl_action_item = (Label)e.Row.FindControl("lblActionItem");
            LinkButton lnk_hcs_count = (LinkButton)e.Row.FindControl("lnkBtnHCSCounts");
            LinkButton lnk_store_count = (LinkButton)e.Row.FindControl("lnkBtnStoreCounts");

            if (Convert.ToInt32(lbl_action_itemid.Text) == 1 || Convert.ToInt32(lbl_action_itemid.Text) == 3)
                lnk_store_count.Text = "";

            if (lnk_hcs_count.Text.Trim() == "0")
            {
                lnk_hcs_count.Enabled = false;
                lnk_hcs_count.CssClass = "matricsreportMonitorItems";
                lbl_action_item.CssClass = "matricsreportMonitorItems";
            }

            if (lnk_store_count.Text.Trim() == "0")
            {
                lnk_store_count.Enabled = false;
                lnk_store_count.CssClass = "matricsreportMonitorItems";
            }

            if (lnk_hcs_count.Text.Trim() == "0" && lnk_store_count.Text.Trim() == "0")
                lbl_action_item.CssClass = "matricsreportMonitorItems";
        }
    }
    #endregion

    #region ------------------- PRIVATE FUNCTIONS -----------
    /// <summary>
    /// Binds Action items data
    /// </summary>
    /// <param name="user_id"></param>
    /// <param name="store_id"></param>
    public void bindActionReportData(int user_id, int store_id)
    {
        this.commonAppSession.SelectedStoreSession.ActionItemsCounts = new DBOperations().getActionItemsCounts(user_id, store_id);
        this.grdActionItems.DataSource = this.commonAppSession.SelectedStoreSession.ActionItemsCounts;
        this.grdActionItems.DataBind();
    }
    #endregion

    #region ------------------- PRIVATE VARIABLES -----------
    private AppCommonSession commonAppSession = null;
    #endregion

    #region -------- Web Form Designer generated code -------
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
    #endregion
}