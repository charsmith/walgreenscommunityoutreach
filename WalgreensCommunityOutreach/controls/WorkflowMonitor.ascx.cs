﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using System.Data;
using System.Web.Security;
using TdWalgreens;

public partial class WorkflowMonitor : System.Web.UI.UserControl
{
    public delegate void OnButtonClick();
    public event OnButtonClick btnRefreshHandler;
    protected void Page_Load(object sender, EventArgs e)
    {
        switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
        {
            case "admin":
            case "store manager":
            case "pharmacy manager":
                this.grdWorkFlowMonitor.Columns[2].Visible = false;
                break;
            case "regional vice president":
            case "regional healthcare director":
                this.grdWorkFlowMonitor.Columns[2].HeaderText = "Region";
                break;
            case "healthcare supervisor":
            case "director – rx & retail ops":
                this.grdWorkFlowMonitor.Columns[2].HeaderText = "Area";
                break;
        }

        if (!IsPostBack)
        {
            this.grdWorkFlowMonitor.DataSource = new DBOperations().getWorkflowMonitorStatus(Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
            this.grdWorkFlowMonitor.DataBind();

            if (this.grdWorkFlowMonitor.Rows.Count == 0)
            {
                this.rowWorkflowMonitor.Visible = false;
            }
        }
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
        {
            this.rowWorkflowMonitor.Visible = false;
        }
    }

    protected void lnkBtnRefreshWorkflow_Click(object sender, EventArgs e)
    {
        this.setWorkFlowData();
        if (Request.Url.AbsoluteUri.Contains("walgreensHome.aspx"))
            btnRefreshHandler();
    }

    protected void lnkBtnShowStoreHomePage_Click(object sender, EventArgs e)
    {
        LinkButton lnk_outreach_status = (LinkButton)sender;

        GridViewRow grid_row = (GridViewRow)(lnk_outreach_status).NamingContainer;
        Label lnk_outreachstatus = (Label)(grid_row.FindControl("lblOutreachStatus"));

        if (lnk_outreachstatus.Text == "Follow-up")
            this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;
        else
            this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 2;

        string[] folder = Request.Url.ToString().Split('/');
        if (lnk_outreach_status.ID == "lnkOutreachStatusStore")
        {
            if (folder.Contains("Admin") || folder.Contains("reports") || folder.Contains("corporateUploader"))
                Response.Redirect("../walgreensHome.aspx", false);
            else
                Response.Redirect("walgreensHome.aspx", false);
        }
        else
        {
            if (folder.Contains("Admin") || folder.Contains("reports") || folder.Contains("corporateUploader"))
                Response.Redirect("../reports/reportWorkflowMonitor.aspx", false);
            else
                Response.Redirect("reports/reportWorkflowMonitor.aspx", false);
        }
    }

    #region -------------------- PRIVATE METHODS ----------------------
    /// <summary>
    /// This function will use to set workflow data
    /// </summary>
    public void setWorkFlowData()
    {
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId != "1")
        {
            this.rowWorkflowMonitor.Visible = true;
            this.grdWorkFlowMonitor.DataSource = new DBOperations().getWorkflowMonitorStatus(Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
            this.grdWorkFlowMonitor.DataBind();

            if (this.grdWorkFlowMonitor.Rows.Count == 0)
                this.rowWorkflowMonitor.Visible = false;
        }
        else
            this.rowWorkflowMonitor.Visible = false;
    }
    #endregion

    #region -------------------- PRIVATE VARIABLES --------------------
    private AppCommonSession commonAppSession = null;
    #endregion

    #region -------------------- Web Form Designer generated code --------------------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
    #endregion
}