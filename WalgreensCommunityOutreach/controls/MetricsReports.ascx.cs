﻿using System;
using System.Web.UI.WebControls;
using TdApplicationLib;
using NLog;

public partial class MetricsReports : System.Web.UI.UserControl
{
    #region ------------------- PRIVATE VARIABLES ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "MetricsCounts",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
            this.commonAppSession.SelectedStoreSession.MetricsCounts = new DBOperations().getMetricsCounts(this.commonAppSession.LoginUserInfoSession.UserID);
            this.grdMetricsReports.DataSource = this.commonAppSession.SelectedStoreSession.MetricsCounts;
            this.grdMetricsReports.DataBind();
            this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "MetricsCounts",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        }
    }

    protected void doProcess_Click(Object sender, CommandEventArgs e)
    {
        this.commonAppSession.SelectedStoreSession.SelectedOutreachSnapshotReportId = Convert.ToInt32(e.CommandArgument.ToString());
        Response.Redirect("reports/reportOutreachMetrics.aspx");
    }

    protected void grdMetrics_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkbtn_metric_id = (LinkButton)e.Row.FindControl("lnkBtnMetricReportId");
            Label lbl_metric_count = (Label)e.Row.FindControl("lblMetricCount");

            if (lbl_metric_count.Text.Trim() == "0")
                lnkbtn_metric_id.Enabled = false;
        }
    }
    #endregion

    #region ------------------- PRIVATE VARIABLES ------------
    private AppCommonSession commonAppSession = null;
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region -------- Web Form Designer generated code --------
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
    }
    #endregion
}