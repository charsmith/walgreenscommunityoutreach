﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocalLeads.ascx.cs" Inherits="controls_LocalLeads" %>
<%@ Register Src="PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<input type="hidden" id="hfUnassignedLocalLeadsflag" value="false" runat="server" />
<input type="hidden" id="hfAssignedDistrictLocalLeadsflag" value="false" runat="server" />
<input type="hidden" id="hfAssignedStoreLocalLeadsflag" value="false" runat="server" />
<table width="97%" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <div id="tabsLocalLeads" style="min-height: 200px; vertical-align: top;">
                <ul style="font-size: 70%">
                <asp:Literal ID="lblLocalLeadsTabs" runat="server" Text=""></asp:Literal>
                </ul>
                <!-- Local Lead - Unassigned tab -->
                <div id="idUnassignedLocalLeads">
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowUnassignedLocalLeadsNoRecords">
                        <tr>
                            <td style="text-align: center; vertical-align: top;" >
                                <span class="formFields"><asp:Literal ID="ltlUnassignedLocalLeadsNoRecords" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowUnassignedLocalLeads">                                        
                        <tr>
                            <td style="vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; text-align:left;" >
                                <span class="formFields"><asp:Label ID="ltlUnassignedLocalLeadsNote" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                          <td style="text-align: right; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr align="right">
                                    <td style="float: left">
                                        <table>
                                            <tr>
                                                <td>Filter:</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlUnassignedLocalLeadsFilters" runat="server" CssClass="ddlFilter"></asp:DropDownList>
                                                </td>
                                                <td>ID #:</td>
                                                <td>
                                                    <asp:TextBox ID="txtUnassignedLocalLeadsFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                    <asp:Button ID="btnUnassignedLocalLeads" runat="server" Style="display: none;" OnCommand="btnLocalLeads_Command" CommandArgument="Unassigned" />
                                                </td>
                                                <td>
                                                  <div>
                                                    <asp:CheckBox ID="chkSelectAllUnassignedLocalLeadsInDistrict" Text="Select All in District" runat="server"  />
                                                  </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><asp:Panel ID="pnlUnassignedLocalLeads" runat="server" DefaultButton="hfImgBtnUnassignedLocalLeadsSearch">
                                            <asp:TextBox ID="txtUnassignedLocalLeadsSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" ></asp:TextBox>
                                            <asp:ImageButton ID="hfImgBtnUnassignedLocalLeadsSearch" runat="server" CommandArgument="UnassignedLocalLeads" OnCommand="btnSearchRecords_Click" Style="display:none;" />
                                        </asp:Panel>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgBtnUnassignedLocalLeadsSearch" runat="server" AlternateText="Unassigned Local Leads Search" ImageUrl="~/images/btn_search.png" 
                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" 
                                        CommandArgument="UnassignedLocalLeads" OnCommand="btnSearchRecords_Click" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgBtnUnassignedLocalLeadsInstr" runat="server" AlternateText="Unassigned Local Leads instructions" />
                                    </td>
                                </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 10px; text-align:left;" class="formFields2" >
                                <asp:GridView ID="grdUnassignedLocalLeads" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" GridLines="None" Width="100%" AllowPaging="true" 
                                AllowSorting="true" PageSize="10" OnRowDataBound="grdUnassignedLocalLeads_RowDataBind" OnSorting="grdUnassignedLocalLeads_sorting" OnRowEditing="grdUnassignedLocalLeads_RowEditing" 
                                OnRowUpdating="grdUnassignedLocalLeads_RowUpdating" OnRowCancelingEdit="grdUnassignedLocalLeads_OnRowCancelingEdit" OnPageIndexChanging="grdUnassignedLocalLeads_PageIndexChanging"                                 
                                AlternatingRowStyle-BackColor="#E9E9E9" >
                                    <FooterStyle CssClass="footerGrey" />
                                    <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                    <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="selectUnassignedLocalLeads" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAllUnassignedLocalLeads" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelectUnassignedLocalLeads" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StoreId" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>
                                                <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                                <asp:Label ID="lblOutreachBusinessPk" runat="server" Text='<%# Bind("outreachBusinessPk") %>'></asp:Label>
                                                <asp:Label ID="lblDistrictId" runat="server" Text='<%# Bind("districtId") %>'></asp:Label>
                                                <asp:Label ID="lblLastContactDate" runat="server" Text=''></asp:Label>
                                                <asp:Label ID="lblOutreachStatusId" runat="server" Text='<%# Bind("outreachStatusId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Name & Number" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" HeaderStyle-BorderColor="#336699" 
                                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label><br />
                                                <asp:Label ID="lblBusinessPhone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Address" HeaderStyle-HorizontalAlign="Center" SortExpression="businessAddress" HeaderStyle-BorderColor="#336699" 
                                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessAddress" runat="server" Text='<%# Bind("businessAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Emp" HeaderStyle-HorizontalAlign="Center" SortExpression="actualLocationEmploymentSize" HeaderStyle-BorderColor="#336699" 
                                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblActualLocationEmploymentSize" runat="server" Text='<%# Eval("actualLocationEmploymentSize").ToString() != "" ? Eval("actualLocationEmploymentSize") : "N/A" %>' Width="50px" class="wrapword" 
                                                Style="text-align: center"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="District" HeaderStyle-HorizontalAlign="Center" SortExpression="districtId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDistrict" runat="server" Text='<%# Bind("districtId") %>' Width="50px" Style="text-align: center"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Closest Store" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLLStoreId" runat="server" Text='<%# Bind("storeId") %>' Width="50px" Style="text-align: center"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <table id="tblStores" cellpadding="0" cellspacing="0" border="0">
                                                    <tr id="rowDistricts" runat="server">
                                                        <td>
                                                            <b class="formFields">District:</b><br />
                                                            <asp:DropDownList ID="ddlDistrictsAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField="" 
                                                            OnSelectedIndexChanged="ddlDistrictsAvailable_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="formFields">Store:</b><br />
                                                            <asp:DropDownList ID="ddlStoresAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList><br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Contact" HeaderStyle-HorizontalAlign="Center" SortExpression="LastContact" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastContact" runat="server" Text='<%# Eval("lastContactDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <uc1:pickerandcalendar id="PickerAndCalendarFrom" runat="server" />
                                                <asp:TextBox ID="txtCalenderFrom" runat="server" Visible="False" CssClass="contractBodyText" Width="100px" meta:resourcekey="txtCalenderFromResource1"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Center" SortExpression="Status" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOutreachStatus" runat="server" Text='<%# Bind("outreachStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" Width="100" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList><br />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');" 
                                                CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update" runat="server" AlternateText="Save" />
                                                <br /><br />
                                                <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');" 
                                                CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel" runat="server" AlternateText="Cancel" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');" CausesValidation="true" 
                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit" runat="server" AlternateText="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <center>
                                            <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                        </center>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="LightBlue" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                          <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">Use the Search and Filter functions for more results.</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 12px;">
                                <asp:ImageButton ID="imgBtnApproveUnassignedLocalLeadsDistrictAssignment" ImageUrl="~/images/btn_assign_send_dm.png" runat="server" CausesValidation="true" 
                                AlternateText="Assign &amp; Send Selected to District Manager" CommandArgument="UnassignedLocalLeads" OnCommand="btnApproveDistrictAssignment_Click" 
                                onmouseout="javascript:MouseOutImage(this.id,'images/btn_assign_send_dm.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_assign_send_dm_lit.png');" />&nbsp;&nbsp;
                                <asp:ImageButton ID="imgBtnApproveUnassignedLocalLeadsStoreAssignment" ImageUrl="~/images/btn_approve_send.png" runat="server" CausesValidation="true" 
                                AlternateText="Approve &amp; Send Selected Store Assignments" CommandArgument="UnassignedLocalLeads" OnCommand="btnApproveStoreAssignment_Click" 
                                onmouseout="javascript:MouseOutImage(this.id,'images/btn_approve_send.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_approve_send_lit.png');" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Local Lead - Assigned to District tab -->
                <div id="idAssignedToDistrictLocalLeads">
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowAssignedDistrictLocalLeadsNoRecords">
                      <tr>
                        <td valign="top" align="center" ><span class="formFields"><asp:Literal ID="ltlAssignedDistrictLocalLeadsNoRecords" runat="server"></asp:Literal></span></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowAssignedDistrictLocalLeads" >
                        <tr>
                            <td style="vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; text-align:left;">
                                <span class="formFields">
                                    <asp:Label ID="ltlAssignedDistrictLocalLeadsNote" runat="server" Style="display: none;"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2" >
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr align="right">
                                    <td style="float: left">
                                        <table>
                                            <tr>
                                                <td>Filter:</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAssignedDistrictLocalLeadsFilters" runat="server" CssClass="ddlFilter" ></asp:DropDownList>
                                                </td>
                                                <td>ID #:</td>
                                                <td>
                                                    <asp:TextBox ID="txtAssignedDistrictLocalLeadsFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                    <asp:Button ID="btnAssignedDistrictLocalLeads" runat="server" Style="display: none;"  OnCommand="btnLocalLeads_Command" CommandArgument="assignedtodist"  />
                                                </td>
                                                <td>
                                                    <div>
                                                    <asp:CheckBox ID="chkSelectAllAssignedDistrictLocalLeadsInDistrict" Text="Select All in District" runat="server" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><asp:Panel ID="pnlAssignedDistrictLocalLeads" runat="server" DefaultButton="hfImgBtnAssignedDistrictLocalLeadsSearch">
                                        <asp:TextBox ID="txtAssignedDistrictLocalLeadsSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" ></asp:TextBox>
                                        <asp:ImageButton ID="hfImgBtnAssignedDistrictLocalLeadsSearch" runat="server" CommandArgument="AssignedDistrictLocalLeads" OnCommand="btnSearchRecords_Click" Style="display:none;" />
                                    </asp:Panel></td>
                                    <td>
                                        <asp:ImageButton ID="imgBtnAssignedDistrictLocalLeadsSearch" runat="server" AlternateText="Assigned District Local Leads Search" ImageUrl="~/images/btn_search.png" 
                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" 
                                        CommandArgument="AssignedDistrictLocalLeads" OnCommand="btnSearchRecords_Click" />
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgBtnAssignedDistrictLocalLeadsInstr" runat="server" AlternateText="Assigned to District Local Leads instructions" />
                                    </td>
                                </tr>
                            </table>
                            </td>                                         
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top:5px; padding-left:22px; padding-right: 22px; padding-bottom: 5px;" class="formFields2">
                            <asp:GridView ID="grdAssignedToDistrictLocalLeads" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" GridLines="None" Width="100%" AllowPaging="true" AllowSorting="true" 
                            PageSize="10" OnRowDataBound="grdAssignedToDistrictLocalLeads_RowDataBound" OnRowCancelingEdit="grdAssignedToDistrictLocalLeads_OnRowCancelingEdit" OnSorting="grdAssignedToDistrictLocalLeads_sorting" 
                            OnRowEditing="grdAssignedToDistrictLocalLeads_RowEditing" OnRowUpdating="grdAssignedToDistrictLocalLeads_RowUpdating" OnPageIndexChanging="grdAssignedToDistrictLocalLeads_PageIndexChanging" AlternatingRowStyle-BackColor="#E9E9E9" >
                                    <FooterStyle CssClass="footerGrey" />
                                    <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                    <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="selectUnassignedLocalLeads" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAllDistrictLocalLeads" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelectDistrictLocalLeads" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StoreId" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>
                                                <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                                <asp:Label ID="lblOutreachBusinessPk" runat="server" Text='<%# Bind("outreachBusinessPk") %>'></asp:Label>
                                                <asp:Label ID="lblDistrictId" runat="server" Text='<%# Bind("districtId") %>'></asp:Label>
                                                <asp:Label ID="lblLastContactDate" runat="server" Text=''></asp:Label>
                                                <asp:Label ID="lblOutreachStatusId" runat="server" Text='<%# Bind("outreachStatusId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Name & Number" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" HeaderStyle-BorderColor="#336699" 
                                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label><br />
                                                <asp:Label ID="lblBusinessPhone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Address" HeaderStyle-HorizontalAlign="Center" SortExpression="businessAddress" HeaderStyle-BorderColor="#336699" 
                                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessAddress" runat="server" Text='<%# Bind("businessAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Emp" HeaderStyle-HorizontalAlign="Center" SortExpression="actualLocationEmploymentSize" HeaderStyle-BorderColor="#336699" 
                                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblActualLocationEmploymentSize" runat="server" Text='<%# Eval("actualLocationEmploymentSize").ToString() != "" ? Eval("actualLocationEmploymentSize") : "N/A" %>' Width="50px" class="wrapword" 
                                                Style="text-align: center"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="District" HeaderStyle-HorizontalAlign="Center" SortExpression="districtId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDistrict" runat="server" Text='<%# Bind("districtId") %>' Width="50px" Style="text-align: center"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Closest Store" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLLStoreId" runat="server" Text='<%# Bind("storeId") %>' Width="50px" Style="text-align: center"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <table id="tblStores" cellpadding="0" cellspacing="0" border="0">
                                                    <tr id="rowDistricts" runat="server">
                                                        <td>
                                                            <b class="formFields">District:</b><br />
                                                            <asp:DropDownList ID="ddlDistrictsAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField="" 
                                                            OnSelectedIndexChanged="ddlDistrictsAvailable_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="formFields">Store:</b><br />
                                                            <asp:DropDownList ID="ddlStoresAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList><br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Contact" HeaderStyle-HorizontalAlign="Center" SortExpression="LastContact" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastContact" runat="server" Text='<%# Eval("lastContactDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <uc1:pickerandcalendar id="PickerAndCalendarFrom" runat="server" />
                                                <asp:TextBox ID="txtCalenderFrom" runat="server" Visible="False" CssClass="contractBodyText" Width="100px" meta:resourcekey="txtCalenderFromResource1"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Center" SortExpression="Status" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOutreachStatus" runat="server" Text='<%# Bind("outreachStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" Width="100" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList><br />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');" 
                                                CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update" runat="server" AlternateText="Save" />
                                                <br /><br />
                                                <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');" 
                                                CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel" runat="server" AlternateText="Cancel" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');" CausesValidation="true" 
                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit" runat="server" AlternateText="Edit"  />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <center>
                                            <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                        </center>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="LightBlue" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                          <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">Use the Search and Filter functions for more results.</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top:5px; padding-left:22px; padding-right: 22px; padding-bottom: 22px;" >
                            <asp:ImageButton ID="imgBtnAssignedDistrictDistrictAssignment" ImageUrl="~/images/btn_assign_send_dm.png" runat="server" CausesValidation="true"  
                            AlternateText="Assign &amp; Send Selected to District Manager" CommandArgument="AssignedDistrictLocalLeads" OnCommand="btnApproveDistrictAssignment_Click" 
                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_assign_send_dm.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_assign_send_dm_lit.png');" />&nbsp;&nbsp;
                            <asp:ImageButton ID="imgBtnAssignedDistrictStoreAssignment" ImageUrl="~/images/btn_approve_send.png" runat="server" CausesValidation="true"  
                            AlternateText="Approve &amp; Send Selected Store Assignments" CommandArgument="AssignedDistrictLocalLeads" OnCommand="btnApproveStoreAssignment_Click" 
                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_approve_send.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_approve_send_lit.png');" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Local Lead - Assigned to Store tab -->
                <div id="idAssignedToStoresLocalLeads">
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowAssignedStoreLocalLeadsNoRecords">
                        <tr>
                        <td valign="top" align="center" ><span class="formFields"><asp:Literal ID="ltlAssignedStoreLocalLeadsNoRecords" runat="server"></asp:Literal></span></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowAssignedStoreLocalLeads" >
                        <tr>
                            <td style="vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; text-align:left;" >                                
                                <span class="formFields">
                                    <asp:Label ID="ltlAssignedStoreLocalLeadsNote" runat="server" Style="display: none;"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top:12px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2" >
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr align="right">
                                    <td style="float: left">
                                        <table>
                                            <tr>
                                                <td>Filter:</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAssignedStoreLocalLeadsFilters" runat="server"></asp:DropDownList>
                                                </td>
                                                <td>ID #:</td>
                                                <td>
                                                    <asp:TextBox ID="txtAssignedStoreLocalLeadsFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                    <asp:Button ID="btnAssignedStoreLocalLeads" runat="server" Style="display: none;"  OnCommand="btnLocalLeads_Command" CommandArgument="assigned" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><asp:Panel ID="pnlAssignedStoreLocalLeads" runat="server" DefaultButton="hfImgBtnAssignedStoreLocalLeadsSearch">
                                        <asp:TextBox ID="txtAssignedStoreLocalLeadsSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" ></asp:TextBox>
                                        <asp:ImageButton ID="hfImgBtnAssignedStoreLocalLeadsSearch" runat="server" CommandArgument="AssignedStoreLocalLeads" OnCommand="btnSearchRecords_Click" Style="display:none;" />
                                    </asp:Panel></td>
                                    <td >
                                        <asp:ImageButton ID="imgBtnAssignedStoreLocalLeadsSearch" runat="server" AlternateText="Assigned Store Local Leads Search" ImageUrl="~/images/btn_search.png" 
                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" 
                                        CommandArgument="AssignedStoreLocalLeads" OnCommand="btnSearchRecords_Click" />
                                    </td>
                                    <td >
                                        <asp:ImageButton ID="imgBtnAssignedStoreLocalLeadsInstr" runat="server" AlternateText="Assigned to Store Local Leads instructions" />
                                    </td>
                                </tr>
                            </table>
                            </td>                                         
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top:5px; padding-left:12px; padding-right:12px; padding-bottom: 5px;" class="formFields2">
                             <asp:GridView ID="grdAssignedToStoreLocalLeads" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" GridLines="None" Width="100%" AllowPaging="true" 
                             AllowSorting="true" PageSize="10" OnPageIndexChanging="grdAssignedToStoreLocalLeads_PageIndexChanging" OnRowDataBound="grdAssignedToStoreLocalLeads_RowDataBound" 
                             OnSorting="grdAssignedToStoreLocalLeads_sorting" AlternatingRowStyle-BackColor="#E9E9E9">
                                <FooterStyle CssClass="footerGrey" />
                                <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                <Columns>
                                    <asp:TemplateField HeaderText="selectAssignedToStoreLocalLeads" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllAssignedToStoreLocalLeads" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelectAssignedToStoreLocalLeads" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StoreId" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                            <asp:Label ID="lblOutreachBusinessPk" runat="server" Text='<%# Bind("outreachBusinessPk") %>'></asp:Label>
                                            <asp:Label ID="lblCanDelete" runat="server" Text='<%# Bind("canDelete") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Name & Number" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" 
                                        HeaderStyle-Font-Bold="true" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label><br />
                                            <asp:Label ID="lblBusinessPhone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Address" HeaderStyle-HorizontalAlign="Center" SortExpression="businessAddress" HeaderStyle-BorderColor="#336699" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBusinessAddress" runat="server" Text='<%# Bind("businessAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp" HeaderStyle-HorizontalAlign="Center" SortExpression="actualLocationEmploymentSize" HeaderStyle-BorderColor="#336699" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualLocationEmploymentSize" runat="server" 
                                            Text='<%# Eval("actualLocationEmploymentSize").ToString() != "" ? Eval("actualLocationEmploymentSize") : "N/A" %>' Width="50px" class="wrapword" 
                                            Style="text-align: center"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="District" HeaderStyle-HorizontalAlign="Center" SortExpression="districtId" HeaderStyle-BorderColor="#336699" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDistrict" runat="server" Text='<%# Bind("districtId") %>' Width="50px" Style="text-align: center"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assigned Store" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>' Width="40px" Style="text-align: center"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assigned Date" HeaderStyle-HorizontalAlign="Center" SortExpression="dateAdded" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAssignedDate" runat="server" Text='<%# Eval("dateAdded", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Center" SortExpression="OutreachStatus" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOutreachStatus" runat="server" Text='<%# Bind("outreachStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate><center><b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b></center>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="LightBlue" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                          <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">Use the Search and Filter functions for more results.</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top:5px; padding-left:22px; padding-right: 22px; padding-bottom: 22px;" >
                               <asp:ImageButton ID="btnResetToHCS" ImageUrl="~/images/btn_reset_to_HCS.png" runat="server" CausesValidation="true" 
                               AlternateText="Reset To HCS" OnCommand="btnResetToHCS_Command" CommandArgument="AssignedStore" 
                               onmouseout="javascript:MouseOutImage(this.id,'images/btn_reset_to_HCS.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_reset_to_HCS_lit.png');" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>&nbsp;<%--National/Large Account Store Assignment Verification--%></td>
    </tr>
</table>