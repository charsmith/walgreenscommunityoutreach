﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="walgreensHeader.ascx.cs" Inherits="controls_walgreensHeader" %>

<script>
    $(document).ready(function () {
        $("#walgreensHeaderCtrl_ddlStoreList").change(function () {
            $("#walgreensHeaderCtrl_txtStoreId").val($("#walgreensHeaderCtrl_ddlStoreList option:selected").val());
            $("#walgreensHeaderCtrl_btnRefresh").click();
        });
        dropdownRepleceText("walgreensHeaderCtrl_ddlStoreList", "walgreensHeaderCtrl_txtStoreList");
    });
</script>
<script type="text/javascript" src="../javaScript/commonFunctions.js"></script>
<script type="text/javascript">
    var userId;
    $(document).ready(function () {
        userId = '<%=UserId%>';
        storeSearch(userId);
    });
</script>
<asp:ImageButton ID="btnRefresh" runat="server" Visible="true" style="display:none" OnClick="btnRefresh_Click" ImageUrl="~/images/btn_add_business.png" CausesValidation="false" />  
<asp:TextBox ID="txtStoreId" runat="server" class="formFields" MaxLength="5" style="display:none" ></asp:TextBox>
<asp:TextBox ID="txtStoreProfiles" CssClass="formFields" Visible="true" runat="server" Width="440px" style="display:none"></asp:TextBox> 
 <table width="100%" cellpadding="0" cellspacing="0" border="0" class="dropShadow">
  <tr>
    <td width="484"  align="left" valign="bottom" bgcolor="#FFFFFF" style="padding:12px 0px 0px 16px;"><img id="imgLogo" runat="server" src="~/images/wags_logo.png" width="225" height="52" /><br /></td>
    <td width="451" align="right" valign="bottom" bgcolor="#FFFFFF" class="outreachTitleRight"><span id="lblOutreachProgram" runat="server"></span></td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:0px 0px 17px 20px;" class="wagsAddress"><asp:Literal ID="lblHeaderDetails" runat="server"></asp:Literal></td>
    <td align="right" valign="top" bgcolor="#FFFFFF" rowspan="2">
        <table cellpadding="0" cellspacing="0" border="0" width="68%"><tr><td align="right" valign="top" bgcolor="#FFFFFF" style="padding:0px 20px 12px 0px;" class="statusLine">
        <strong><asp:Literal ID="lblLastStatusProvided" runat="server"></asp:Literal></strong>
        </td></tr></table>
    </td>
  </tr>
  <tr>
    <td style="padding:0px 0px 12px 15px; text-align:left;background-color:white" class="wagsAddress" runat="server" id="tdStoreSelectionBlock">
        <table  border="0" cellspacing="5" cellpadding="0" runat="server" id="tdStoreSelectionSearch" width="110%"> 
            <tr>
                <td align="left" valign="top" class="logSubTitles" width="110px">Store Search:</td>
                <td>
                    <input id="storeId" class="formFields" style="width:440px" value="" /><br  /> 
                    <span  class="formFields" style="font-size:8pt; color:#57a1d3;">Search by store id, city or state to select a Walgreens store location.</span>          
                </td>
            </tr> 
        </table>
        <table border="0" cellspacing="5" cellpadding="0" runat="server" id="tdStoreSelectionDropDown"  width="100%">
            <tr>
                <td align="left" valign="top" nowrap="nowrap" width="95px" ><span class="logSubTitles">Store Name: </span></td>
                <td align="left">
                    <asp:DropDownList CssClass="formFields" ID="ddlStoreList" runat="server" EnableViewState="false" Width="440px"></asp:DropDownList>
                    <asp:TextBox CssClass="formFields"  ID="txtStoreList" runat="server" Width="438px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </td>
<%--    <td align="right" valign="top" bgcolor="#FFFFFF">&nbsp;</td>--%>
  </tr>
  <tr>
    <td colspan="2">
    <table cellpadding="0" cellspacing="0" border="0" width="100%"><tr>
    <td colspan="2" align="left" valign="top" bgcolor="#57a1d3">
    <asp:Label ID="lblNewWindow" runat="server" style="display:none"></asp:Label>
    <div id="menuTab" runat="server">
        <div id="colortab" class="ddcolortabs">
            <ul>
                <li><a>
                    <asp:ImageButton ID="btnHome" PostBackUrl="~/walgreensHome.aspx" ImageUrl="~/images/btn_home.gif" CausesValidation="false"
                        runat="server" AlternateText="Home" onmouseout="javascript:MouseOutImage(this.id,'images/btn_home.gif');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_home_lit.gif');" /></a></li>
                <li><a>
                    <asp:ImageButton ID="btnStore" PostBackUrl="~/walgreensHome.aspx" ImageUrl="~/images/btn_store.gif" CausesValidation="false" Visible="false"
                        runat="server" AlternateText="Store" onmouseout="javascript:MouseOutImage(this.id,'images/btn_store.gif');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_store_lit.gif');" /></a></li>
                <li id="contactLogMenu"><a rel="dropmenu1_c">
                    <asp:ImageButton ID="btnContact" PostBackUrl="~/walgreensContactLog.aspx" ImageUrl="~/images/btn_contact_log.gif" CausesValidation="false"
                        runat="server" AlternateText="Contact Log"  onmouseout="javascript:MouseOutImage(this.id,'images/btn_contact_log.gif');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_contact_log_lit.gif');" /></a></li>
                <li><a>
                    <asp:ImageButton ID="btnMap" PostBackUrl="~/walgreensMap.aspx" ImageUrl="~/images/btn_map.gif" CausesValidation="false"
                        runat="server" AlternateText="Map" onmouseout="javascript:MouseOutImage(this.id,'images/btn_map.gif');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_map_lit.gif');" /></a></li>                        
                <li id="lnkJobAids" runat="server" visible="false"><a>
					<%-- Job Aids button changed to Resources button in IP --%>
                    <asp:ImageButton ID="btnJobAids" PostBackUrl="~/walgreensJobAids.aspx" ImageUrl="~/images/btn_Resources.gif" CausesValidation="false"  runat="server"
                        AlternateText="Best Practice" onmouseout="javascript:MouseOutImage(this.id,'images/btn_Resources.gif');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_Resources_lit.gif');" /></a></li>
                <li id="lnkResources" runat="server" visible="false"><a><asp:ImageButton ID="btnResources" PostBackUrl="~/walgreensResources.aspx" ImageUrl="~/images/btn_Resources.gif" onmouseout="javascript:MouseOutImage(this.id,'images/btn_Resources.gif');" CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_Resources_lit.gif');" runat="server"
                        AlternateText="Resources" /></a></li>
                <li id="lnkFaqIP" runat="server" visible="false"><a><asp:ImageButton ID="btnFaqIP" PostBackUrl="~/walgreensFAQsIP.aspx" ImageUrl="~/images/btn_faq.gif" onmouseout="javascript:MouseOutImage(this.id,'images/btn_faq.gif');" CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_faq_lit.gif');" runat="server"
                        AlternateText="FAQ" /></a></li>
                    <li id="lnkFaqSO" runat="server" visible="false"><a><asp:ImageButton ID="btnFaqSO" PostBackUrl="~/walgreensFAQs.aspx" ImageUrl="~/images/btn_faq.gif" onmouseout="javascript:MouseOutImage(this.id,'images/btn_faq.gif');" CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_faq_lit.gif');" runat="server"
                        AlternateText="FAQ" /></a></li>
                <li id="reportsMenu"><a rel="dropmenu1_a"><span>
                    <asp:ImageButton ID="btnReports" PostBackUrl="~/reports/walgreensReport.aspx" ImageUrl="~/images/btn_reports.gif" CausesValidation="false"
                        runat="server" AlternateText="Reports" onmouseout="javascript:enabledropdown();javascript:MouseOutImage(this.id,'images/btn_reports.gif');" onmouseover="javascript:disabledropdown();javascript:MouseOverImage(this.id,'images/btn_reports_lit.gif');"></asp:ImageButton></span></a></li>
                <li id="scheduleSetup"><a rel="dropmenu1_d"><span>
                    <asp:ImageButton ID="imgAdmin" PostBackUrl="~/Admin/UserEditor.aspx" ImageUrl="~/images/btn_admin.gif" onmouseout="javascript:enabledropdown();MouseOutImage(this.id,'images/btn_admin.gif');" CausesValidation="false"
                        onmouseover="javascript:disabledropdown();MouseOverImage(this.id,'images/btn_admin_lit.gif');" runat="server"
                        AlternateText="Admin" /></span></a></li> 
                <li><a><asp:ImageButton ID="imgOutreachPortal" PostBackUrl="~/walgreensLandingPage.aspx" ImageUrl="~/images/btn_outreach_portal.gif"
                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_outreach_portal.gif');" CausesValidation="false"                                
                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_outreach_portal_lit.gif');" runat="server"
                        AlternateText="Outreach Portal" /></a></li>
                <li><a>
                    <asp:ImageButton ID="imgSignOut" ImageUrl="~/images/btn_sign_out.gif"
                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_sign_out.gif');" CausesValidation="false"                                
                        onmouseover="javascript:MouseOverImage(this.id,'images/btn_sign_out_lit.gif');" runat="server"
                        AlternateText="Sign Out" onclick="imgSignOut_Click"/></a></li>    
            </ul>
        </div>
        <div id="dropmenu1_a" class="dropmenudiv_a">
            <a id="lnkStoreBusinessContactReport" runat="server" href="~/reports/reportStoreBusinessContact.aspx" class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();" visible="false" >Potential Local Business List and Contact Status </a> 
            <a id="lnkStoreBusinessReport" runat="server" href="~/reports/walgreensReport.aspx" class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();" visible="false" >Potential Local Business List</a>
            <a id="lnkContactLogReport" runat="server" href="~/reports/reportStoreBusinessFeedback.aspx" class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();" visible= "false" >Potential Local Business Contact Status</a>
            <a id="lnkClinicContactLog" runat="server" href="~/reports/reportStoreBusinessFeedbackClinic.aspx" class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();" visible="false">Scheduled Clinic Status</a>
            <a id="lnkLocationComplianceReport" runat="server"  class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();" visible="false" >Store Compliance Report</a> 
            <a id="lnkHighLevelReport" runat="server"  class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">High Level Compliance Report</a>
            <a id="lnkDistrictCompliance" runat="server"  class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">District Report (Compliance)</a>
            <a id="lnkDistrictCompletion" runat="server"  href="~/reports/reportDistrictCompletion.aspx" class="logSubTitles" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">District Report (Completion)</a>                    
            <a id="lnkAccessLogReport" runat="server" href="~/reports/walgreensLoginReport.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">User Access Log</a>
            <a id="lnkEventScheduleReport" runat="server" href="~/reports/reportEventsScheduled.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Events Scheduled</a>
            <a id="lnkFollowupReminderReport" runat="server" href="~/reports/reportFollowUpReminder.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Follow-up Reminder</a>
            <a id="lnkPendingEmailReminderReport" runat="server" href="~/reports/reportePendingEmailReminders.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Pending Email Reminder</a>
            <a id="lnkHighLevelStatusReport" runat="server" href="~/reports/reportHighLevelStatus.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();"></a>
            <%--<a id="lnkAssignedNationalAccountsReport" runat="server" href="~/reports/reportAssignedNationalAccounts.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Assigned Corporate Worksite Clinics</a>--%>
            <a id="lnkClinicDetailsReport" runat="server" href="~/reports/reportClinicDetails.aspx" class="logSubTitles" visible="true" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Scheduled Clinics Report</a>
            <%--<a id="lnkHHSVoucherReport" runat="server" href="~/reports/reportHHSVoucher.aspx" class="logSubTitles" visible="true" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">HHS Voucher Program Report</a>--%>
            <a id="lnkContractAgreementsReport" runat="server" href="~/reports/reportContractAgreements.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Initiated Contracts Report</a>
            <a id="lnkWorkflowMonitorReport" runat="server" href="~/reports/reportWorkflowMonitor.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Workflow Monitor Report</a>
            <a id="lnkScheduledAppointmentsReport" runat="server" href="~/reports/reportScheduledAppointment.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Scheduled Appointments Report</a>
            <a id="lnkActivityReport" runat="server" href="~/reports/reportActivitySO.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Activity Report</a>
            <a id="lnkScheduledEventsReport" runat="server" href="~/reports/reportScheduledEvents.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Scheduled Events Report</a> 
            <a id="lnkVaccinePurchasingReport" runat="server" href="~/reports/reportVaccinePurchasingDetails.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Vaccine Purchasing Report</a>
            <a id="lnkVaccinePurchasingPage" runat="server" href="~/reports/reportWeeklyVaccinePurchasingReport.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Weekly Vaccine Purchasing Report</a>
            <a id="lnkDirectB2BMailCampaignReport" runat="server" href="~/reports/reportDirectB2BMailCampaignResults.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Direct B2B Mail Campaign Results</a>
            <a id="lnkGrpIdAssignmentReport" runat="server" href="~/reports/reportGroupIDAssignments.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Group ID Assignments Report</a>
            <a id="lnkGrpIdAssignmentPage" runat="server" href="~/reports/reportWeeklyGroupIDAssignmentsReport.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Weekly Group ID Assignments Report</a>                   
            <a id="lnkExceptionShotsPage" runat="server" href="~/reports/reportMinimumShotExceptionsDetails.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Minimum Shot Exceptions Report</a>                   
            <a id="lnkVaccineExceptionPage" runat="server" href="~/reports/reportRevisedDatesVolumeReport.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Revised Clinic Dates & Volumes Report</a>                   
            <a id="lnkImmunizationFinancePage" runat="server" href="~/reports/reportImmunizationFinanceReport.aspx" class="logSubTitles" visible="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();">Immunization Finance Report</a>                   
        </div>
        <div id="dropmenu1_c" class="dropmenudiv_a">
            <asp:LinkButton ID="lnkLocalBusinesses" CommandArgument="1" OnCommand="showContactLog_click" runat="server" Text="Potential Local Businesses" CssClass="logSubTitles" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();"></asp:LinkButton>
            <asp:LinkButton ID="lnkScheduledBusinesses" CommandArgument="2" OnCommand="showContactLog_click" runat="server" Text="Scheduled Clinics" CssClass="logSubTitles" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();" Visible="false"></asp:LinkButton>                    
            <asp:LinkButton ID="lnkAssistedClinics" CommandArgument="3" OnCommand="showContactLog_click" runat="server" Text="Assisted Clinics" CssClass="logSubTitles" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();" Visible="false"></asp:LinkButton>
        </div>
        <div id="dropmenu1_d" class="dropmenudiv_a">
            <asp:LinkButton ID="lnkUserProfiles" runat="server" PostBackUrl="~/Admin/UserEditor.aspx" CssClass="logSubTitles" Text="User Profile" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();"></asp:LinkButton>
            <asp:LinkButton ID="lnkCorporateScheduler" runat="server" PostBackUrl="~/corporateScheduler/schedulerHome.aspx" CssClass="logSubTitles" Text="Scheduler Site Setup" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();"></asp:LinkButton>
			<asp:LinkButton ID="lnkUploadCorporateData" runat="server" PostBackUrl="~/corporateUploader/WalgreensCorporateClinicsUploader.aspx" CssClass="logSubTitles" Text="Upload Scheduled Clinics" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();"></asp:LinkButton>
            <asp:LinkButton ID="lnkStoreProfileUpdate" runat="server" PostBackUrl="~/walgreensMonthlyStoreUpdate.aspx" CssClass="logSubTitles" Text="Store Profile Uploader" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();"></asp:LinkButton>
            <asp:LinkButton ID="lnkCustomMessage" runat="server" PostBackUrl="~/Admin/PostCustomMessage.aspx" CssClass="logSubTitles" Text="Post Custom Message" CausesValidation="false" onmouseover="javascript:disabledropdown();" onmouseout="javascript:enabledropdown();"></asp:LinkButton>
        </div>
        <script type="text/javascript">
            //SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
            tabdropdown.init("colortab", 4)
        </script>
    </div>
    </td>
    </tr></table>
    </td>
  </tr>
</table>