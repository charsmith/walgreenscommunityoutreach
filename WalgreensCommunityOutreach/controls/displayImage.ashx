﻿<%@ WebHandler Language="C#" Class="displayImage" %>

using System;
using System.Web;
using TdApplicationLib;
using TdWalgreens;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

public class displayImage : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string contxt_path = context.Server.MapPath(context.Request.FilePath);
        string file_name = context.Request.QueryString["image"];
        string path_loc = ApplicationSettings.clientLogoImagePath();
        string extension = file_name.Substring(file_name.LastIndexOf('.') + 1);
        string file_path = string.Empty;

        context.Response.ClearHeaders();
        context.Response.Cache.SetNoServerCaching();
        context.Response.Cache.SetNoStore();
        context.Response.ContentType = getContentType(path_loc + "\\" + file_name);

        file_path = path_loc + "\\" + file_name;
        if (File.Exists(file_path))
        {
            Bitmap bit_map = new Bitmap(file_path);
            bit_map.Dispose();
            context.Response.WriteFile(file_path);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    ImageFormat getImageFormat(String path)
    {
        switch (Path.GetExtension(path))
        {
            case ".bmp": return ImageFormat.Bmp;
            case ".gif": return ImageFormat.Gif;
            case ".jpg": return ImageFormat.Jpeg;
            case ".png": return ImageFormat.Png;
            default: break;
        }
        return ImageFormat.Jpeg;
    }

    string getContentType(String path)
    {
        switch (Path.GetExtension(path))
        {
            case ".bmp": return "Image/bmp";
            case ".gif": return "Image/gif";
            case ".jpg": return "Image/jpeg";
            case ".png": return "Image/png";
            default: break;
        }
        return "";
    }

    byte[] getResizedImage(String path, int width, int height)
    {
        Bitmap imgIn = new Bitmap(path);
        double y = imgIn.Height;
        double x = imgIn.Width;

        double factor = 1;
        if (width > 0)
        {
            factor = width / x;
        }
        else if (height > 0)
        {
            factor = height / y;
        }
        System.IO.MemoryStream outStream =
        new System.IO.MemoryStream();
        Bitmap imgOut =
        new Bitmap((int)(x * factor), (int)(y * factor));
        Graphics g = Graphics.FromImage(imgOut);
        g.Clear(Color.White);
        g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x),
        (int)(factor * y)),
        new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

        imgOut.Save(outStream, getImageFormat(path));
        imgIn.Dispose();
        imgOut.Dispose();
        return outStream.ToArray();
    }
}