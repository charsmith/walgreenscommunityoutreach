﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WorkflowMonitor.ascx.cs" Inherits="WorkflowMonitor" %>

<table width="100%" border="0" cellpadding="0" cellspacing="6" class="workflowMonitor" runat="server" id="rowWorkflowMonitor">
    <tr class="workflowMonitorInterior">
        <td width="100%" valign="top">
            <asp:GridView ID="grdWorkFlowMonitor" runat="server" GridLines="None" AutoGenerateColumns="false" Width="100%" AllowPaging="false" AllowSorting="false" Font-Names="Arial, Helvetica, sans-serif" Font-Size="12px" BackColor="#ffffff" >
                <Columns>
                    <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="workflowTextHead" HeaderStyle-Width="75%" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="workflowText" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblOutreachStatus" runat="server" CssClass="workflowMonitorItems" Text='<%# Bind("outreachStatus") %>'></asp:Label>                                    
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Store" HeaderStyle-CssClass="workflowTextHead" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="workflowText" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkOutreachStatusStore" runat="server" CssClass="workflowMonitorItems" Text='<%# Bind("statusCountByStore") %>' OnClick="lnkBtnShowStoreHomePage_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dist" HeaderStyle-CssClass="workflowTextHead" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="workflowText" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkOutreachStatusDist" runat="server" CssClass="workflowMonitorItems" Text='<%# Bind("statusCountByDist") %>' OnClick="lnkBtnShowStoreHomePage_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="workflowTextHead" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="workflowText" ItemStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            <asp:LinkButton ID="lnkBtnRefreshWorkflow" runat="server" OnClick="lnkBtnRefreshWorkflow_Click" CausesValidation="false"><asp:Image ImageUrl="~/images/btn_refresh.png" runat="server" BorderStyle="None" AlternateText="Refresh"/></asp:LinkButton>                                    
                        </HeaderTemplate>                                
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
   