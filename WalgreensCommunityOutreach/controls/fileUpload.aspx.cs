﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using TdWalgreens;
using System.Xml;

public partial class controls_fileUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.uploadLogoFile.HasFile)
        {            
            string client_name = Request.QueryString["clientName"].ToString();
            //string file_name = string.Empty;
            string file_ext = Path.GetExtension(this.uploadLogoFile.FileName);;
            String[] valid_extensions = { ".gif", ".png", ".jpeg", ".jpg" };
            bool is_valid_file = false;

            //Check if the file type is valid
            for (int i = 0; i < valid_extensions.Length; i++)
            {
                if (file_ext.ToLower() == valid_extensions[i])
                    is_valid_file = true;
            }

            if (is_valid_file)
            {
                if (!Directory.Exists(ApplicationSettings.clientLogoImagePath()))
                    Directory.CreateDirectory(ApplicationSettings.clientLogoImagePath());
                else
                {
                    //Check if file exists and delete from _temp folder
                    if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".png"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".png");
                    else if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".gif"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".gif");                    
                    else if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".jpg"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".jpg");
                    else if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".jpeg"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".jpeg");
                    if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".PNG"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".PNG");
                    else if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".GIF"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".GIF");
                    else if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".JPG"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".JPG");
                    else if (File.Exists(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".JPEG"))
                        File.Delete(ApplicationSettings.clientLogoImagePath() + "\\" + client_name + ".JPEG");
                }
                
                string save_file = ApplicationSettings.clientLogoImagePath() + "\\" + client_name + file_ext;

                this.uploadLogoFile.SaveAs(save_file);

                CorporateScheduler corporate_scheduler = new CorporateScheduler();
                corporate_scheduler = (CorporateScheduler)Session["clientDetails"];
                XmlDocument client_scheduler_xml = new XmlDocument();
                client_scheduler_xml.LoadXml(corporate_scheduler.clinicSchedulerXML);
                client_scheduler_xml.SelectSingleNode(".//logoAndStyles").Attributes["mastheadLogo"].Value = "clientLogo";
                client_scheduler_xml.SelectSingleNode(".//logoAndStyles").Attributes["logoFile"].Value = client_name + file_ext;

                corporate_scheduler.clinicSchedulerXML = client_scheduler_xml.OuterXml;
                Session["clientDetails"] = corporate_scheduler;

                Page.ClientScript.RegisterStartupScript(this.GetType(), "window", "displayClientLogo();", true);
            }
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", "alert('Only image files with a '.png', '.gif', '.jpg' or '.jpeg' file extension are allowed.');", true);                
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();
    }
}