﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using TdApplicationLib;
using TdWalgreens;
using System.Xml;
using System.Data;
using NLog;

public partial class controls_LocalLeads : System.Web.UI.UserControl
{
    #region ------------------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ddlUnassignedLocalLeadsFilters.bindDataFilterTypes(commonAppSession.LoginUserInfoSession.UserRole);
            this.ddlAssignedDistrictLocalLeadsFilters.bindDataFilterTypes(commonAppSession.LoginUserInfoSession.UserRole);
            this.ddlAssignedStoreLocalLeadsFilters.bindDataFilterTypes(commonAppSession.LoginUserInfoSession.UserRole);

            this.commonAppSession.SelectedStoreSession.SelectedLocalLeadsTabId = "UnassignedLocalLeads";
            this.commonAppSession.dtLocalLeads = new DataTable();
            this.getLocalLeadsBusinesses(0);
            this.bindLocalLeads();
        }
        else
        {
            string event_target = Request["__EVENTTARGET"];
            string[] event_args = Request["__EVENTARGUMENT"].Split(',');
            if (event_target.ToLower() == "localleadsresettohcs")
            {
                this.setLocalBusinessChecked("AssignedStore");
                this.saveLocalLeadsUpdates("Revert");
            }
            else if (event_target.ToLower() == "assignbusinesstootherstate" && event_args.Contains("LocalLeads"))
            {
                this.isStoreUpdated = true;
                this.dtLocalLeads = this.dtlocalLeadsRecord;
                this.grdAssignedToDistrictLocalLeads.EditIndex = -1;
                this.grdUnassignedLocalLeads.EditIndex = -1;
                this.saveLocalLeadsUpdates(event_args[0].Trim());
                this.bindLocalLeads();
            }
        }

        this.displayTabs();
    }

    protected void btnSearchRecords_Click(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString())
        {
            case "UnassignedLocalLeads":
                this.getLocalLeadsBusinesses(1, this.txtUnassignedLocalLeadsSearch.Text, this.ddlUnassignedLocalLeadsFilters.SelectedItem.Value, this.txtUnassignedLocalLeadsFilter.Text);
                this.bindUnassignedLocalLeads();
                break;
            case "AssignedDistrictLocalLeads":
                this.getLocalLeadsBusinesses(2, this.txtAssignedDistrictLocalLeadsSearch.Text, this.ddlAssignedDistrictLocalLeadsFilters.SelectedItem.Value, this.txtAssignedDistrictLocalLeadsFilter.Text);
                this.bindAssignedToDistrictLocalLeads();
                break;
            case "AssignedStoreLocalLeads":
                this.getLocalLeadsBusinesses(3, this.txtAssignedStoreLocalLeadsSearch.Text, this.ddlAssignedStoreLocalLeadsFilters.SelectedItem.Value, this.txtAssignedStoreLocalLeadsFilter.Text);
                this.bindAssignedStoreLocalLeads();
                break;
        }
    }

    protected void btnLocalLeads_Command(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLower())
        {
            case "unassigned":
                this.getLocalLeadsBusinesses(1, this.txtUnassignedLocalLeadsSearch.Text, this.ddlUnassignedLocalLeadsFilters.SelectedItem.Value, this.txtUnassignedLocalLeadsFilter.Text);
                this.bindUnassignedLocalLeads();
                break;
            case "assignedtodist":
                this.getLocalLeadsBusinesses(2, this.txtAssignedDistrictLocalLeadsSearch.Text, this.ddlAssignedDistrictLocalLeadsFilters.SelectedItem.Value, this.txtAssignedDistrictLocalLeadsFilter.Text);
                this.bindAssignedToDistrictLocalLeads();
                break;
            case "assigned":
                this.getLocalLeadsBusinesses(3, this.txtAssignedStoreLocalLeadsSearch.Text, this.ddlAssignedStoreLocalLeadsFilters.SelectedItem.Value, this.txtAssignedStoreLocalLeadsFilter.Text);
                this.bindAssignedStoreLocalLeads();
                break;
        }
    }

    protected void btnApproveDistrictAssignment_Click(object sender, CommandEventArgs e)
    {
        this.setLocalBusinessChecked(e.CommandArgument.ToString());
        this.saveLocalLeadsUpdates("AssignDistrict");
    }

    protected void btnApproveStoreAssignment_Click(object sender, CommandEventArgs e)
    {
        this.setLocalBusinessChecked(e.CommandArgument.ToString());
        this.saveLocalLeadsUpdates("AssignStore");
    }

    protected void btnResetToHCS_Command(object sender, CommandEventArgs e)
    {
        this.setLocalBusinessChecked(e.CommandArgument.ToString());

        DataRow[] dr_businesses = this.dtLocalLeads.Select("isStoreConfirmed = 1 AND ISNULL(isChecked, 0) = '1' AND outreachStatusId <> 0");
        if (dr_businesses.Length > 0)
        {
            StringBuilder contacted_Stores = new StringBuilder();
            foreach (DataRow row in dr_businesses)
            {
                contacted_Stores.Append(row["businessName"].ToString() + "(" + row["outreachStatus"].ToString() + "), ");
            }

            bool admin_can_override = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "ResetContactedStoreStatus");
            if (admin_can_override)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showConfirmationDialog('Reset To HCS','localLeadsResetToHCS', '' ,'" + GetGlobalResourceObject("errorMessages", "resetcontactedBusinessAdmin") + "\\n" + contacted_Stores.ToString().Replace("'", "\\'").Trim().TrimEnd(',') + "');", true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + GetGlobalResourceObject("errorMessages", "resetcontactedBusiness").ToString() + "\\n" + contacted_Stores.ToString().Replace("'", "\\'").Trim().TrimEnd(',') + "');", true);
        }
        else
            this.saveLocalLeadsUpdates("Revert");
    }

    protected void grdUnassignedLocalLeads_RowDataBind(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox chk_all_unassigned_local_leads = (CheckBox)e.Row.FindControl("chkSelectAllUnassignedLocalLeads");
            if (this.chkSelectAllUnassignedLocalLeadsInDistrict.Checked)
                chk_all_unassigned_local_leads.Checked = true;
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contact_phone = (Label)e.Row.FindControl("lblBusinessPhone");
            if (lbl_contact_phone.Text.Trim().Length == 10)
                lbl_contact_phone.Text = "(" + lbl_contact_phone.Text.Substring(0, 3) + ") " + lbl_contact_phone.Text.Substring(3, 3) + "-" + lbl_contact_phone.Text.Substring(6, 4);

            Label lbl_outreachstatus_id = (Label)e.Row.FindControl("lblOutreachStatusId");
            CheckBox chk_unassigned_local_lead = (CheckBox)e.Row.FindControl("chkSelectUnassignedLocalLeads");
            if (lbl_outreachstatus_id.Text.Trim() == "4" || lbl_outreachstatus_id.Text.Trim() == "5")
                chk_unassigned_local_lead.Enabled = false;

            CheckBox chk_selected_local_lead = (CheckBox)e.Row.FindControl("chkSelectUnassignedLocalLeads");
            if (this.chkSelectAllUnassignedLocalLeadsInDistrict.Checked)
                chk_selected_local_lead.Checked = true;
        }
    }

    protected void grdUnassignedLocalLeads_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdUnassignedLocalLeads.PageIndex = e.NewPageIndex;
        this.grdUnassignedLocalLeads.EditIndex = -1;
        this.bindLocalLeads();
    }

    protected void grdUnassignedLocalLeads_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.grdUnassignedLocalLeads.EditIndex = e.NewEditIndex;
        this.bindLocalLeads();

        DropDownList ddl_stores = (DropDownList)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("ddlStoresAvailable"));
        Label lbl_storeid = (Label)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("lblStoreId"));
        Label lbl_district_id = (Label)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("lblDistrict"));
        Label lbl_last_contact = (Label)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("lblLastContactDate"));
        Label lbl_outreach_status = (Label)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("lblOutreachStatusId"));
        Label lbl_business_pk = (Label)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("lblBusinessPk"));
        DropDownList ddl_outreach_status = (DropDownList)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("ddlStatus"));
        DataTable dt_outreach_status = new DataTable();

        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
        {
            DropDownList ddl_districts = (DropDownList)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("ddlDistrictsAvailable"));
            DataTable dt_districts = this.dbOperations.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, null, null, null);
            ddl_districts.DataSource = dt_districts;
            ddl_districts.DataTextField = dt_districts.Columns[1].ToString();
            ddl_districts.DataValueField = dt_districts.Columns[0].ToString();
            ddl_districts.DataBind();

            if (ddl_districts.Items.FindByValue(lbl_district_id.Text) != null)
                ddl_districts.Items.FindByValue(lbl_district_id.Text).Selected = true;

            dt_outreach_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 10)").CopyToDataTable();
        }
        else
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_districts = (System.Web.UI.HtmlControls.HtmlTableRow)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("rowDistricts"));
            row_districts.Visible = false;
            dt_outreach_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 9)").CopyToDataTable();
        }

        ddl_outreach_status.DataSource = dt_outreach_status;
        ddl_outreach_status.DataTextField = dt_outreach_status.Columns[1].ToString();
        ddl_outreach_status.DataValueField = dt_outreach_status.Columns[0].ToString();
        ddl_outreach_status.DataBind();

        if (!string.IsNullOrEmpty(lbl_outreach_status.Text))
        {
            if (ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text) != null)
                ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text).Selected = true;
        }

        var date_control = (PickerAndCalendar)(this.grdUnassignedLocalLeads.Rows[this.grdUnassignedLocalLeads.EditIndex].FindControl("PickerAndCalendarFrom"));
        if (date_control != null)
        {
            ((PickerAndCalendar)date_control).MaxDate = DateTime.Now.AddDays(1);

            lbl_last_contact.Text = DateTime.Now.ToString("MM/dd/yyyy");
            ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Now;
        }

        this.bindUserStores(ddl_stores, lbl_storeid.Text, lbl_district_id.Text, lbl_business_pk.Text);
    }

    protected void grdUnassignedLocalLeads_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.dtLocalLeads = this.dtlocalLeadsRecord;
        this.updateStoreAssignment(this.grdUnassignedLocalLeads);

        if (this.isStoreUpdated)
        {
            this.grdUnassignedLocalLeads.EditIndex = -1;
            this.saveLocalLeadsUpdates("Update");
            this.getLocalLeadsBusinesses(1, this.txtUnassignedLocalLeadsSearch.Text, this.ddlUnassignedLocalLeadsFilters.SelectedItem.Value, this.txtUnassignedLocalLeadsFilter.Text);
            this.bindLocalLeads();
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showConfirmationDialog('Store Assignment','assignBusinessToOtherState', ['Update', 'LocalLeads'], '" + (string)GetGlobalResourceObject("errorMessages", "BusinessAssignmentToOtherStateAlert") + "');", true);
    }

    protected void grdUnassignedLocalLeads_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdUnassignedLocalLeads.EditIndex = -1;
        this.bindLocalLeads();
    }

    protected void grdUnassignedLocalLeads_sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.grdUnassignedLocalLeads.EditIndex == -1)
        {
            ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
            this.bindLocalLeads();
        }
        else
        {
            this.grdUnassignedLocalLeads.EditIndex = -1;
            this.bindLocalLeads();
        }
    }

    protected void grdAssignedToDistrictLocalLeads_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdAssignedToDistrictLocalLeads.EditIndex = -1;
        this.grdAssignedToDistrictLocalLeads.PageIndex = e.NewPageIndex;
        this.bindLocalLeads();
    }

    protected void grdAssignedToDistrictLocalLeads_sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.grdAssignedToDistrictLocalLeads.EditIndex == -1)
        {
            ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
            this.bindLocalLeads();
        }
        else
        {
            this.grdAssignedToDistrictLocalLeads.EditIndex = -1;
            this.bindLocalLeads();
        }
    }

    protected void grdAssignedToDistrictLocalLeads_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox chk_selectAllRows = (CheckBox)e.Row.FindControl("chkSelectAllDistrictLocalLeads");
            if (this.chkSelectAllAssignedDistrictLocalLeadsInDistrict.Checked)
                chk_selectAllRows.Checked = true;
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contact_phone = (Label)e.Row.FindControl("lblBusinessPhone");
            if (lbl_contact_phone.Text.Trim().Length == 10)
                lbl_contact_phone.Text = "(" + lbl_contact_phone.Text.Substring(0, 3) + ") " + lbl_contact_phone.Text.Substring(3, 3) + "-" + lbl_contact_phone.Text.Substring(6, 4);

            Label lbl_outreachstatus_id = (Label)e.Row.FindControl("lblOutreachStatusId");
            CheckBox chk_district_local_lead = (CheckBox)e.Row.FindControl("chkSelectDistrictLocalLeads");
            if (lbl_outreachstatus_id.Text.Trim() == "4" || lbl_outreachstatus_id.Text.Trim() == "5")
                chk_district_local_lead.Enabled = false;

            CheckBox chk_selected_local_lead = (CheckBox)e.Row.FindControl("chkSelectDistrictLocalLeads");
            if (this.chkSelectAllAssignedDistrictLocalLeadsInDistrict.Checked)
                chk_selected_local_lead.Checked = true;
        }
    }

    protected void grdAssignedToDistrictLocalLeads_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.dtLocalLeads = this.dtlocalLeadsRecord;
        this.updateStoreAssignment(this.grdAssignedToDistrictLocalLeads);

        if (this.isStoreUpdated)
        {
            this.grdAssignedToDistrictLocalLeads.EditIndex = -1;
            this.saveLocalLeadsUpdates("Update");
            this.getLocalLeadsBusinesses(2, this.txtAssignedDistrictLocalLeadsSearch.Text, this.ddlAssignedDistrictLocalLeadsFilters.SelectedItem.Value, this.txtAssignedDistrictLocalLeadsFilter.Text);
            this.bindLocalLeads();
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showConfirmationDialog('Store Assignment','assignBusinessToOtherState', ['Update', 'LocalLeads'], '" + (string)GetGlobalResourceObject("errorMessages", "BusinessAssignmentToOtherStateAlert") + "');", true);
    }

    protected void grdAssignedToDistrictLocalLeads_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.grdAssignedToDistrictLocalLeads.EditIndex = e.NewEditIndex;
        this.bindLocalLeads();

        DropDownList ddl_stores = (DropDownList)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("ddlStoresAvailable"));
        Label lbl_storeid = (Label)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("lblStoreId"));
        Label lbl_district_id = (Label)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("lblDistrict"));
        Label lbl_last_contact = (Label)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("lblLastContactDate"));
        Label lbl_outreach_status = (Label)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("lblOutreachStatusId"));
        Label lbl_business_pk = (Label)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("lblBusinessPk"));
        DropDownList ddl_outreach_status = (DropDownList)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("ddlStatus"));
        DataTable dt_outreach_status = new DataTable();

        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
        {
            DropDownList ddl_districts = (DropDownList)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("ddlDistrictsAvailable"));
            DataTable dt_districts = this.dbOperations.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, null, null, null);
            ddl_districts.DataSource = dt_districts;
            ddl_districts.DataTextField = dt_districts.Columns[1].ToString();
            ddl_districts.DataValueField = dt_districts.Columns[0].ToString();
            ddl_districts.DataBind();

            if (ddl_districts.Items.FindByValue(lbl_district_id.Text) != null)
                ddl_districts.Items.FindByValue(lbl_district_id.Text).Selected = true;

            dt_outreach_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 10)").CopyToDataTable();
        }
        else
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_districts = (System.Web.UI.HtmlControls.HtmlTableRow)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("rowDistricts"));
            row_districts.Visible = false;
            dt_outreach_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 9)").CopyToDataTable();
        }

        ddl_outreach_status.DataSource = dt_outreach_status;
        ddl_outreach_status.DataTextField = dt_outreach_status.Columns[1].ToString();
        ddl_outreach_status.DataValueField = dt_outreach_status.Columns[0].ToString();
        ddl_outreach_status.DataBind();

        if (!string.IsNullOrEmpty(lbl_outreach_status.Text))
        {
            if (ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text) != null)
                ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text).Selected = true;
        }

        var date_control = (PickerAndCalendar)(this.grdAssignedToDistrictLocalLeads.Rows[this.grdAssignedToDistrictLocalLeads.EditIndex].FindControl("PickerAndCalendarFrom"));
        if (date_control != null)
        {
            ((PickerAndCalendar)date_control).MaxDate = DateTime.Now.AddDays(1);

            lbl_last_contact.Text = DateTime.Now.ToString("MM/dd/yyyy");
            ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Now;
        }

        this.bindUserStores(ddl_stores, lbl_storeid.Text, lbl_district_id.Text, lbl_business_pk.Text);
    }

    protected void grdAssignedToDistrictLocalLeads_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdAssignedToDistrictLocalLeads.EditIndex = -1;
        this.bindLocalLeads();
    }

    protected void grdAssignedToStoreLocalLeads_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdAssignedToDistrictLocalLeads.EditIndex = -1;
        this.grdUnassignedLocalLeads.EditIndex = -1;
        this.grdAssignedToStoreLocalLeads.PageIndex = e.NewPageIndex;
        this.bindLocalLeads();
    }

    protected void grdAssignedToStoreLocalLeads_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindLocalLeads();
    }

    protected void ddlDistrictsAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl_districts = (DropDownList)sender;
        GridViewRow business_row = (GridViewRow)ddl_districts.NamingContainer;
        DropDownList ddl_available_districts = (DropDownList)business_row.FindControl("ddlDistrictsAvailable");
        DropDownList ddl_available_stores = (DropDownList)business_row.FindControl("ddlStoresAvailable");
        Label lbl_business_pk = (Label)business_row.FindControl("lblBusinessPk");

        //binding date picker calendar
        var date_control = (PickerAndCalendar)(business_row.FindControl("PickerAndCalendarFrom"));
        if (date_control != null)
            ((PickerAndCalendar)date_control).MaxDate = DateTime.Now.AddDays(1);

        this.bindUserStores(ddl_available_stores, string.Empty, ddl_available_districts.SelectedValue.ToString(), lbl_business_pk.Text);
    }

    protected void grdAssignedToStoreLocalLeads_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contact_phone = (Label)e.Row.FindControl("lblBusinessPhone");
            if (lbl_contact_phone.Text.Trim().Length == 10)
                lbl_contact_phone.Text = "(" + lbl_contact_phone.Text.Substring(0, 3) + ") " + lbl_contact_phone.Text.Substring(3, 3) + "-" + lbl_contact_phone.Text.Substring(6, 4);

            Label lbl_can_delete = (Label)e.Row.FindControl("lblCanDelete");
            CheckBox chk_select_assigned_business = (CheckBox)e.Row.FindControl("chkSelectAssignedToStoreLocalLeads");

            if (!string.IsNullOrEmpty(lbl_can_delete.Text) && (Convert.ToInt32(lbl_can_delete.Text) == 0))
                chk_select_assigned_business.Enabled = false;
        }
    }
    #endregion

    #region ------------------- PRIVATE METHODS -------------
    /// <summary>
    /// Displayes local leads assignment tabs
    /// </summary>
    private void displayTabs()
    {
        //string business_type = string.Empty;
        StringBuilder sb = new StringBuilder();

        sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='UnassignedLocalLeads' style='width: 180px;' href='#idUnassignedLocalLeads' runat='server' onclick=setSelectedTab('UnassignedLocalLeads')><span style='font-weight:bold'>Unassigned <br />Local Leads</span></a></li>");
        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
            sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='AssignedToDistrictLocalLeads' style='width: 180px;' href='#idAssignedToDistrictLocalLeads' runat='server' onclick=setSelectedTab('AssignedToDistrictLocalLeads')><span style='font-weight:bold'>Local Leads <br />Assigned to District</span></a></li>");
        sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='AssignedToStoresLocalLeads' style='width: 180px;' href='#idAssignedToStoresLocalLeads' runat='server' onclick=setSelectedTab('AssignedToStoresLocalLeads')><span style='font-weight:bold'>Assigned <br />Local Leads</span></a></li>");

        this.lblLocalLeadsTabs.Text = sb.ToString();
    }

    /// <summary>
    /// Gets Local lead businesses
    /// </summary>
    /// <param name="assigned_to"></param>
    /// <param name="search_value"></param>
    /// <param name="location_filter_type"></param>
    /// <param name="location_filter_value"></param>
    private void getLocalLeadsBusinesses(int assigned_to, string search_value = "", string location_filter_type = "", string location_filter_value = "")
    {
        this.logger.Info("Method {0} accessed from tab {1} of {2} page by user {3} - START", "getLocalLeadsBusinesses", assigned_to, System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        this.dtlocalLeadsRecord = this.dbOperations.getLocalLeadsBusiness(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole, assigned_to, search_value, location_filter_type, location_filter_value);
        this.logger.Info("Method {0} accessed from tab {1} of {2} page by user {3} - END", "getLocalLeadsBusinesses", assigned_to, System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds local leads to grid controls
    /// </summary>
    private void bindLocalLeads()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "bindLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);

        this.rowUnassignedLocalLeadsNoRecords.Visible = false;
        this.rowAssignedDistrictLocalLeadsNoRecords.Visible = false;
        this.rowAssignedStoreLocalLeadsNoRecords.Visible = false;
        this.rowUnassignedLocalLeads.Visible = true;
        this.rowAssignedDistrictLocalLeads.Visible = true;
        this.rowAssignedStoreLocalLeads.Visible = true;

        this.ltlUnassignedLocalLeadsNote.Text = this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager" ? string.Format((string)GetGlobalResourceObject("errorMessages", "localLeadsUnassignedNoteToDistricts"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower()) : (string)GetGlobalResourceObject("errorMessages", "localLeadsUnassignedNote");
        this.ltlAssignedDistrictLocalLeadsNote.Text = (string)GetGlobalResourceObject("errorMessages", "localLeadsAssignedtoDistrictNote");
        this.ltlAssignedStoreLocalLeadsNote.Text = (string)GetGlobalResourceObject("errorMessages", "localLeadsAssignedtoStoreNote");

        this.ltlUnassignedLocalLeadsNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoUnassignedLocalLeadsAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoUnassignedLocalLeads"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlAssignedDistrictLocalLeadsNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoAssignedDistrictLocalLeadsAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoAssignedDistrictLocalLeads"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlAssignedStoreLocalLeadsNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoAssignedStoreLocalLeadsAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoAssignedStoreLocalLeads"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());

        //if (this.dtlocalLeadsRecord.Rows.Count == 0)
        //{
        //    this.getLocalLeadsBusinesses();
        //}
        this.dtLocalLeads = this.dtlocalLeadsRecord;

        if (this.dtLocalLeads.Rows.Count == 0)
        {
            this.rowUnassignedLocalLeadsNoRecords.Visible = true;
            this.rowAssignedDistrictLocalLeadsNoRecords.Visible = true;
            this.rowAssignedStoreLocalLeadsNoRecords.Visible = true;
            this.rowUnassignedLocalLeads.Visible = false;
            this.rowAssignedDistrictLocalLeads.Visible = false;
            this.rowAssignedStoreLocalLeads.Visible = false;
            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                this.rowAssignedDistrictLocalLeadsNoRecords.Visible = false;
        }
        else
        {
            DataRow[] dr_unassigned_local_leads = this.dtlocalLeadsRecord.Select(this.getFilterExpression(false, "UnAssigned"));
            DataTable dt_unassigned_local_leads = new DataTable();
            if (dr_unassigned_local_leads.Count() > 0)
            {
                this.rowUnassignedLocalLeads.Visible = true;
                this.rowUnassignedLocalLeadsNoRecords.Visible = false;
                //dt_unassigned_local_leads = !Convert.ToBoolean(hdnUADB2BShowMoreThan10.Value) ? dr_unassigned_local_leads.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : dr_unassigned_local_leads.CopyToDataTable();
                dt_unassigned_local_leads = dr_unassigned_local_leads.CopyToDataTable();
                if ((ViewState["sortOrder"] != null) && (dt_unassigned_local_leads.Columns.Contains(GridSortExpression)))
                    dt_unassigned_local_leads.DefaultView.Sort = ViewState["sortOrder"].ToString();
                else
                    dt_unassigned_local_leads.DefaultView.Sort = this.defaultSortExp;
                this.grdUnassignedLocalLeads.DataSource = dt_unassigned_local_leads;
                this.grdUnassignedLocalLeads.DataBind();

                this.logger.Info("Bound {0} Grid", "grdUnassignedLocalLeads");

                if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                {
                    this.imgBtnApproveUnassignedLocalLeadsDistrictAssignment.Visible = false;
                    this.rowAssignedDistrictLocalLeads.Visible = false;
                }
            }
            else
            {
                this.rowUnassignedLocalLeadsNoRecords.Visible = true;
                this.rowUnassignedLocalLeads.Visible = false;

                if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                {
                    this.rowAssignedDistrictLocalLeadsNoRecords.Visible = false;
                    this.rowAssignedDistrictLocalLeads.Visible = false;
                }
            }

            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
            {
                DataRow[] dr_assigned_distict_leads = this.dtlocalLeadsRecord.Select(this.getFilterExpression(true, "AssignedToDistrict"));
                DataTable dt_assigned_distict_leads = new DataTable();
                if (dr_assigned_distict_leads.Count() > 0)
                {
                    this.rowAssignedDistrictLocalLeadsNoRecords.Visible = false;
                    this.rowAssignedDistrictLocalLeads.Visible = true;
                    //dt_assigned_distict_leads = !Convert.ToBoolean(hdnADDB2BShowMoreThan10.Value) ? dr_assigned_distict_leads.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : dr_assigned_distict_leads.CopyToDataTable();
                    dt_assigned_distict_leads = dr_assigned_distict_leads.CopyToDataTable();
                    if ((ViewState["sortOrder"] != null) && (dt_assigned_distict_leads.Columns.Contains(GridSortExpression)))
                        dt_assigned_distict_leads.DefaultView.Sort = ViewState["sortOrder"].ToString();
                    else
                        dt_assigned_distict_leads.DefaultView.Sort = this.defaultSortExp;
                    this.grdAssignedToDistrictLocalLeads.DataSource = dt_assigned_distict_leads;
                    this.grdAssignedToDistrictLocalLeads.DataBind();
                    this.logger.Info("Bound {0} Grid", "grdAssignedToDistrictLocalLeads");
                }
                else
                {
                    this.rowAssignedDistrictLocalLeads.Visible = false;
                    this.rowAssignedDistrictLocalLeadsNoRecords.Visible = true;
                }
            }

            DataRow[] dr_assigned_store_leads = this.dtlocalLeadsRecord.Select(this.getFilterExpression(true, "Assigned"));
            if (dr_assigned_store_leads.Count() > 0)
            {
                this.rowAssignedStoreLocalLeads.Visible = true;
                this.rowAssignedStoreLocalLeadsNoRecords.Visible = false;
                //DataTable dt_assigned_store_leads = !Convert.ToBoolean(hdnADB2BShowMoreThan10.Value) ? dr_assigned_store_leads.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : dr_assigned_store_leads.CopyToDataTable();
                DataTable dt_assigned_store_leads = dr_assigned_store_leads.CopyToDataTable();
                if ((ViewState["sortOrder"] != null) && (dt_assigned_store_leads.Columns.Contains(GridSortExpression)))
                    dt_assigned_store_leads.DefaultView.Sort = ViewState["sortOrder"].ToString();
                else
                    dt_assigned_store_leads.DefaultView.Sort = this.defaultSortExp;
                this.grdAssignedToStoreLocalLeads.DataSource = dt_assigned_store_leads;
                this.grdAssignedToStoreLocalLeads.DataBind();
                this.logger.Info("Bound {0} Grid", "grdAssignedToStoreLocalLeads");
            }
            else
            {
                this.btnResetToHCS.Visible = false;
                this.rowAssignedStoreLocalLeads.Visible = false;
                this.rowAssignedStoreLocalLeadsNoRecords.Visible = true;
            }

        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "bindLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds unassigned local leads
    /// </summary>
    private void bindUnassignedLocalLeads()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "bindUnassignedLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);

        DataRow[] dr_unassigned_local_leads = this.dtlocalLeadsRecord.Select(this.getFilterExpression(false, "UnAssigned"));
        DataTable dt_unassigned_local_leads = new DataTable();
        if (dr_unassigned_local_leads.Count() > 0)
        {
            this.grdUnassignedLocalLeads.PageIndex = 0;
            this.imgBtnApproveUnassignedLocalLeadsStoreAssignment.Visible = true;
            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
                this.imgBtnApproveUnassignedLocalLeadsDistrictAssignment.Visible = true;
            dt_unassigned_local_leads = dr_unassigned_local_leads.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && (dt_unassigned_local_leads.Columns.Contains(GridSortExpression)))
                dt_unassigned_local_leads.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_unassigned_local_leads.DefaultView.Sort = this.defaultSortExp;
        }
        else
        {
            this.imgBtnApproveUnassignedLocalLeadsStoreAssignment.Visible = false;
            this.imgBtnApproveUnassignedLocalLeadsDistrictAssignment.Visible = false;
        }
        this.grdUnassignedLocalLeads.EditIndex = -1;
        this.grdUnassignedLocalLeads.DataSource = dt_unassigned_local_leads;
        this.grdUnassignedLocalLeads.DataBind();
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "bindUnassignedLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds local leads assigned to districts
    /// </summary>
    private void bindAssignedToDistrictLocalLeads()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "bindAssignedToDistrictLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);

        DataRow[] dr_assigned_distict_leads = this.dtlocalLeadsRecord.Select(this.getFilterExpression(true, "AssignedToDistrict"));
        DataTable dt_assigned_distict_leads = new DataTable();
        if (dr_assigned_distict_leads.Count() > 0)
        {
            this.grdAssignedToDistrictLocalLeads.PageIndex = 0;
            this.imgBtnAssignedDistrictStoreAssignment.Visible = true;
            this.imgBtnAssignedDistrictDistrictAssignment.Visible = true;
            dt_assigned_distict_leads = dr_assigned_distict_leads.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && (dt_assigned_distict_leads.Columns.Contains(GridSortExpression)))
                dt_assigned_distict_leads.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_assigned_distict_leads.DefaultView.Sort = this.defaultSortExp;
        }
        else
        {
            this.imgBtnAssignedDistrictStoreAssignment.Visible = false;
            this.imgBtnAssignedDistrictDistrictAssignment.Visible = false;
        }
        this.grdAssignedToDistrictLocalLeads.EditIndex = -1;
        this.grdAssignedToDistrictLocalLeads.DataSource = dt_assigned_distict_leads;
        this.grdAssignedToDistrictLocalLeads.DataBind();
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "bindAssignedToDistrictLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds local leads assigned to stores
    /// </summary>
    private void bindAssignedStoreLocalLeads()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "bindAssignedStoreLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);

        DataRow[] dr_assigned_store_leads = this.dtlocalLeadsRecord.Select(this.getFilterExpression(true, "Assigned"));
        DataTable dt_assigned_store_leads = new DataTable();
        if (dr_assigned_store_leads.Count() > 0)
        {
            this.btnResetToHCS.Visible = true;
            this.grdAssignedToStoreLocalLeads.PageIndex = 0;
            dt_assigned_store_leads = dr_assigned_store_leads.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && (dt_assigned_store_leads.Columns.Contains(GridSortExpression)))
                dt_assigned_store_leads.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_assigned_store_leads.DefaultView.Sort = this.defaultSortExp;
            //this.grdAssignedToStoreLocalLeads.DataSource = !Convert.ToBoolean(hdnUADB2BShowMoreThan10.Value) ? dr_assigned_store_leads.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : dr_assigned_store_leads.CopyToDataTable();
            this.grdAssignedToStoreLocalLeads.DataSource = dr_assigned_store_leads.CopyToDataTable();
            this.grdAssignedToStoreLocalLeads.DataBind();
        }
        else
        {
            this.btnResetToHCS.Visible = false;
        }
        this.grdAssignedToStoreLocalLeads.EditIndex = -1;
        this.grdAssignedToStoreLocalLeads.DataSource = dt_assigned_store_leads;
        this.grdAssignedToStoreLocalLeads.DataBind();

        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "bindAssignedStoreLocalLeads", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds user stores to local leads
    /// </summary>
    /// <param name="ddl_stores"></param>
    /// <param name="business_storeid"></param>
    /// <param name="business_districtid"></param>
    /// <param name="business_pk"></param>
    private void bindUserStores(DropDownList ddl_stores, string business_storeid, string business_districtid, string business_pk)
    {
        DataTable dt_stores = new DataTable();
        if (!string.IsNullOrEmpty(business_districtid))
            dt_stores = this.dbOperations.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, Convert.ToInt32(business_districtid), Convert.ToInt32(business_pk), "Local");
        else
            dt_stores = this.dbOperations.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);

        if (dt_stores != null && dt_stores.Rows.Count > 0)
        {
            ddl_stores.DataSource = dt_stores;
            ddl_stores.DataTextField = dt_stores.Columns[1].ToString();
            ddl_stores.DataValueField = dt_stores.Columns[0].ToString();
            ddl_stores.DataBind();

            if (ddl_stores.Items.FindByValue(business_storeid) != null)
                ddl_stores.Items.FindByValue(business_storeid).Selected = true;
            else
            {
                string closest_store = ((DataRow)dt_stores.Select("closestClinicRank = 1")[0])["storeId"].ToString();
                if (ddl_stores.Items.FindByValue(closest_store) != null)
                    ddl_stores.Items.FindByValue(closest_store).Selected = true;
            }
        }
    }

    /// <summary>
    /// Sets local leads as selected
    /// </summary>
    /// <param name="assigned_to"></param>
    private void setLocalBusinessChecked(string assigned_to)
    {
        this.dtLocalLeads = null;
        var is_select_all_district = assigned_to == "AssignedStore" ? false : (assigned_to == "UnassignedLocalLeads" ? this.chkSelectAllUnassignedLocalLeadsInDistrict.Checked : this.chkSelectAllAssignedDistrictLocalLeadsInDistrict.Checked);

        if (is_select_all_district)
        {
            DataRow[] dr_local_leads = assigned_to == "UnassignedLocalLeads" ? this.dtlocalLeadsRecord.Select(this.getFilterExpression(false, "Unassigned")) : this.dtlocalLeadsRecord.Select(this.getFilterExpression(true, "AssignedToDistrict"));
            if (dr_local_leads.Count() > 0)
            {
                this.dtLocalLeads = dr_local_leads.CopyToDataTable();
                foreach (DataRow dr_local_lead in dtLocalLeads.Rows)
                {
                    if (dr_local_lead["outreachStatusId"].ToString() != "4" && dr_local_lead["outreachStatusId"].ToString() != "5")
                        dr_local_lead["isChecked"] = 1;
                }
            }
        }
        else
        {
            if (this.dtlocalLeadsRecord != null && this.dtlocalLeadsRecord.Rows.Count > 0)
            {
                this.dtLocalLeads = assigned_to == "AssignedStore" ? this.dtlocalLeadsRecord.Select("isStoreConfirmed = 1").CopyToDataTable() : this.dtlocalLeadsRecord.Select("isStoreConfirmed = 0").CopyToDataTable();
                GridView grd_local_leads = assigned_to == "AssignedStore" ? this.grdAssignedToStoreLocalLeads : (assigned_to == "UnassignedLocalLeads" ? this.grdUnassignedLocalLeads : this.grdAssignedToDistrictLocalLeads);

                foreach (GridViewRow row in grd_local_leads.Rows)
                {
                    DataRow[] dr_local_lead = this.dtLocalLeads.Select("businessPk = " + Convert.ToInt32(((Label)(row.FindControl("lblBusinessPk"))).Text.Trim()));
                    if (dr_local_lead.Count() > 0)
                    {
                        CheckBox chk_selected_business = assigned_to == "AssignedStore" ? (CheckBox)(row.FindControl("chkSelectAssignedToStoreLocalLeads")) : (assigned_to == "UnassignedLocalLeads" ? (CheckBox)(row.FindControl("chkSelectUnassignedLocalLeads")) : (CheckBox)(row.FindControl("chkSelectDistrictLocalLeads")));
                        dr_local_lead.ElementAt(0)["isChecked"] = (chk_selected_business.Checked ? 1 : 0);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Updates store assignment to the selected local leads
    /// </summary>
    /// <param name="grd_local_leads"></param>
    private void updateStoreAssignment(GridView grd_local_leads)
    {
        int business_pk = 0, updated_storeid = 0, updated_districtid = 0;
        DropDownList ddl_selected_store = (DropDownList)(grd_local_leads.Rows[grd_local_leads.EditIndex].FindControl("ddlStoresAvailable"));
        DropDownList ddl_selected_district = (DropDownList)(grd_local_leads.Rows[grd_local_leads.EditIndex].FindControl("ddlDistrictsAvailable"));
        DropDownList ddl_selected_status = (DropDownList)(grd_local_leads.Rows[grd_local_leads.EditIndex].FindControl("ddlStatus"));

        Label lbl_business_pk = (Label)(grd_local_leads.Rows[grd_local_leads.EditIndex].FindControl("lblBusinessPk"));
        Label lbl_last_outreach_status = (Label)(grd_local_leads.Rows[grd_local_leads.EditIndex].FindControl("lblOutreachStatusId"));
        DateTime selected_contact_date = ((PickerAndCalendar)(grd_local_leads.Rows[grd_local_leads.EditIndex].FindControl("PickerAndCalendarFrom"))).getSelectedDate;

        Int32.TryParse(lbl_business_pk.Text, out business_pk);
        Int32.TryParse(ddl_selected_store.SelectedValue, out updated_storeid);
        Int32.TryParse(ddl_selected_district.SelectedValue, out updated_districtid);

        if (this.dtLocalLeads.Rows.Count > 0)
        {
            DataRow[] update_row = this.dtLocalLeads.Select("businessPk = '" + business_pk + "'");

            if (Convert.ToInt32(update_row[0]["storeId"]) != updated_storeid)
            {
                string business_state = update_row[0]["businessAddress"].ToString().Split(',').Last().Trim().Substring(0, 2);
                string store_state = ddl_selected_store.SelectedItem.Text.Split(',').Last().Trim().Substring(0, 2);

                update_row[0]["storeId"] = updated_storeid;
                update_row[0]["districtId"] = updated_districtid;
                update_row[0]["isStoreUpdated"] = "1";
                if (business_state != store_state)
                {
                    this.isStoreUpdated = false;
                }
            }

            if (ddl_selected_status.SelectedValue.Length > 0 && (Convert.ToInt32(ddl_selected_status.SelectedValue) != Convert.ToInt32(lbl_last_outreach_status.Text)))
            {
                update_row[0]["lastContactDate"] = selected_contact_date.ToString("MM/dd/yyyy");
                update_row[0]["outreachStatusId"] = ddl_selected_status.SelectedValue;
                update_row[0]["outreachStatus"] = ddl_selected_status.SelectedItem.Text;
                update_row[0]["isStatusUpdated"] = "1";
            }
        }
    }

    /// <summary>
    /// Saves/Assigns/Resets local lead businesses
    /// </summary>
    /// <param name="update_type"></param>
    private void saveLocalLeadsUpdates(string update_type)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} with type {3} - START", "saveLocalLeadsUpdates", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName, update_type);

        XmlDocument local_businesses = new XmlDocument();
        local_businesses = this.createUpdateLocalBusinessesXML(update_type);

        if (local_businesses.SelectSingleNode("//localBusiness") != null)
        {
            int return_value = 0;
            return_value = this.dbOperations.updateLocalBusinessesAssignment(local_businesses, update_type);

            if (return_value == 0)
            {
                this.logger.Info("Database Operation {0} done successfully for type {1}", "updateLocalBusinessesAssignment", update_type);

                commonAppSession.dtLocalLeads = new DataTable();
                this.dtlocalLeadsRecord = new DataTable();

                if (update_type == "Update")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('LocalLeads'); alert('" + (string)GetGlobalResourceObject("errorMessages", "LocalLeadsStoreChanged") + "');", true);
                else if (update_type == "AssignDistrict")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('LocalLeads'); alert('" + (string)GetGlobalResourceObject("errorMessages", "LocalLeadsUpdateDistrictAssignment") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
                else if (update_type == "AssignStore")
                {
                    this.emailOperations.sendLocalLeadsStoreAssignmentEmail(this.dtLocalLeads, this.commonAppSession.LoginUserInfoSession.UserID, "store");

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('LocalLeads'); alert('" + (string)GetGlobalResourceObject("errorMessages", "LocalLeadsUpdateStoreAssignment") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
                }
                else if (update_type == "Revert")
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('LocalLeads'); alert('" + GetGlobalResourceObject("errorMessages", "resetToHCSConfirmation") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
            }
        }
        else
        {
            commonAppSession.dtLocalLeads = new DataTable();
            this.dtlocalLeadsRecord = new DataTable();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('LocalLeads'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} with type {3} - END", "saveLocalLeadsUpdates", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName, update_type);
    }

    /// <summary>
    /// Creates local leads xml document for updating or assignment
    /// </summary>
    /// <param name="update_type"></param>
    /// <returns></returns>
    private XmlDocument createUpdateLocalBusinessesXML(string update_type)
    {
        XmlDocument local_businesses = new XmlDocument();
        XmlElement local_business_ele = local_businesses.CreateElement("localBusinesses");
        XmlElement local_business = null;

        if (this.dtLocalLeads != null && this.dtLocalLeads.Rows.Count > 0)
        {
            DataRow[] dr_local_businesses = null;

            if (update_type == "Update")
                dr_local_businesses = this.dtLocalLeads.Select("isStoreConfirmed = 0 AND (ISNULL(isStoreUpdated, 0) = '1' OR ISNULL(isStatusUpdated, 0) = '1')");
            else if (update_type == "AssignDistrict" || update_type == "AssignStore")
                dr_local_businesses = this.dtLocalLeads.Select("isStoreConfirmed = 0 AND ISNULL(isChecked, 0) = '1'");
            else if (update_type == "Revert")
                dr_local_businesses = this.dtLocalLeads.Select("isStoreConfirmed = 1 AND ISNULL(isChecked, 0) = '1'");

            if (dr_local_businesses.Length > 0)
            {
                foreach (DataRow row in dr_local_businesses)
                {
                    local_business = local_businesses.CreateElement("localBusiness");

                    local_business.SetAttribute("businessPk", row["businessPk"].ToString());
                    local_business.SetAttribute("outreachBusinessPk", row["outreachBusinessPk"].ToString());
                    local_business.SetAttribute("storeId", row["storeId"].ToString());
                    local_business.SetAttribute("lastContactDate", row["lastContactDate"].ToString());
                    local_business.SetAttribute("outreachStatusId", row["outreachStatusId"].ToString());
                    local_business.SetAttribute("isStatusUpdated", row["isStatusUpdated"].ToString());
                    local_business.SetAttribute("createdBy", this.commonAppSession.LoginUserInfoSession.UserID.ToString());
                    local_business_ele.AppendChild(local_business);
                    local_businesses.AppendChild(local_business_ele);
                }
            }
        }

        return local_businesses;
    }

    /// <summary>
    /// Get data filter query for Unassigned and Assigned local leads
    /// </summary>
    /// <param name="is_store_confirmed"></param>
    /// <param name="assignment_type"></param>
    /// <returns></returns>
    private string getFilterExpression(bool is_store_confirmed, string assignment_type)
    {
        string str_filter = string.Empty;
        string business_search_key = string.Empty;

        string filter_query;
        if (assignment_type.ToLower() == "unassigned")
        {
            filter_query = "isStoreConfirmed = " + Convert.ToInt32(is_store_confirmed).ToString() + (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager" ? " AND isDistrictConfirmed = 0" : " AND isDistrictConfirmed = 1");
            if (this.ddlUnassignedLocalLeadsFilters.SelectedItem != null && this.ddlUnassignedLocalLeadsFilters.SelectedItem.Value.Trim().Length > 0)
            {
                if (this.txtUnassignedLocalLeadsFilter.Text.Trim().Length > 0)
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlUnassignedLocalLeadsFilters, this.txtUnassignedLocalLeadsFilter);
                }
            }
            str_filter = this.txtUnassignedLocalLeadsSearch.Text.Trim();
            business_search_key = this.txtUnassignedLocalLeadsSearch.Text.Trim().Replace("'", "''");
        }
        else if (assignment_type.ToLower() == "assignedtodistrict")
        {
            filter_query = "isDistrictConfirmed = 1 AND isStoreConfirmed = 0";
            if (this.ddlAssignedDistrictLocalLeadsFilters.SelectedItem != null && this.ddlAssignedDistrictLocalLeadsFilters.SelectedItem.Value.Trim().Length > 0)
            {
                if (this.txtAssignedDistrictLocalLeadsFilter.Text.Trim().Length > 0)
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlAssignedDistrictLocalLeadsFilters, this.txtAssignedDistrictLocalLeadsFilter);
                }
            }
            str_filter = this.txtAssignedDistrictLocalLeadsSearch.Text.Trim();
            business_search_key = this.txtAssignedDistrictLocalLeadsSearch.Text.Trim().Replace("'", "''");
        }
        else
        {
            filter_query = "isDistrictConfirmed = 1 AND isStoreConfirmed = 1";
            if (ddlAssignedStoreLocalLeadsFilters.SelectedItem != null && ddlAssignedStoreLocalLeadsFilters.SelectedItem.Value.Trim().Length > 0)
            {
                if (txtAssignedStoreLocalLeadsFilter.Text.Trim().Length > 0)
                {
                    filter_query += appSettings.getLocationFilterExpression(ddlAssignedStoreLocalLeadsFilters, txtAssignedStoreLocalLeadsFilter);
                }
            }
            str_filter = this.txtAssignedStoreLocalLeadsSearch.Text.Trim();
            business_search_key = this.txtAssignedStoreLocalLeadsSearch.Text.Trim().Replace("'", "''");
        }

        business_search_key = business_search_key.Replace("[", "[[]");
        business_search_key = business_search_key.Replace("*", "[*]");
        str_filter = str_filter.Replace("'", "''");
        str_filter = str_filter.Replace("[", "[[]");
        str_filter = str_filter.Replace("*", "[*]");

        if (!string.IsNullOrEmpty(str_filter))
        {
            filter_query += " AND (businessName like '%" + str_filter + "%'" +
                            " OR businessAddress like '%" + business_search_key + "%'" +
                            " OR CONVERT(actualLocationEmploymentSize, System.String) like '%" + business_search_key + "%'" +
                //" OR lastContact like '%" + clinic_search_key + "%'" +
                            " OR  CONVERT(districtId, System.String) like '%" + business_search_key + "%'" +
                            " OR  CONVERT(storeId, System.String) like '%" + business_search_key + "%'" +
                            ")";
        }

        return filter_query;
    }

    /// <summary>
    /// Sorting grid
    /// </summary>
    private string GridSortExpression
    {
        get
        {
            string sort_expression = string.Empty;
            string[] sort_by_type = new string[1];

            if (ViewState["sortOrder"] != null)
            {
                sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
                sort_expression = sort_by_type[0].Trim();
            }

            return sort_expression;
        }
    }

    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }
    #endregion

    #region ------------------- PRIVATE VARIABLES ------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperations = null;
    private DataTable dtLocalLeads = null;
    private EmailOperations emailOperations = null;
    private ApplicationSettings appSettings = null;
    private Logger logger = LogManager.GetCurrentClassLogger();

    private DataTable dtlocalLeadsRecord
    {
        get
        {
            return this.commonAppSession.dtLocalLeads;
        }
        set
        {
            //if (value.Rows.Count == 0)
            //{
            //    this.commonAppSession.dtLocalLeads = this.dbOperations.getLocalLeadsBusiness(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
            //}
            //else
            //{
            this.commonAppSession.dtLocalLeads = value;
            //}
        }
    }

    private bool isStoreUpdated
    {
        get
        {
            if (ViewState["isStoreUpdated"] != null)
            {
                return Convert.ToBoolean(ViewState["isStoreUpdated"].ToString());

            }
            return true;
        }
        set
        {
            ViewState["isStoreUpdated"] = value;
        }
    }

    private string defaultSortExp = "[businessRank],[businessPk],[districtId],[storeId]";
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperations = new DBOperations();
        this.dtLocalLeads = new DataTable();
        this.emailOperations = new EmailOperations();
        this.appSettings = new ApplicationSettings();
    }
    #endregion
}