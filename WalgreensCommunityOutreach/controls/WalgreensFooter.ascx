﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WalgreensFooter.ascx.cs" Inherits="controls_WalgreensFooter" %>
<table  id="tblfooter" runat="server" border="0" cellspacing="0" cellpadding="0" align="center" width="935">
    <tr>
        <td align="center" valign="top" style="padding: 18px 12px 36px 12px;" class="footerText">
            <p>
                All scheduled corporate, charity (HHS Voucher), community outreach and local clinics will be monitored on a weekly
                basis by the Purchasing Team to ensure that the hosting store is shipped the proper
                Flu vaccine. For status of shipment please go to Storenet > Patient Care > Immunization
                Services > Immunization Reports</p>
            <p>
                For Questions or Concerns, please open a ticket with the <span class="class24"><a
                    href="https://helpctr.walgreens.com/docugen/viewmenu/immunization_services" class="class24"
                    target="_blank">Immunization Service Desk</a></span>.</p>
            <p>
                The <span class="class24"><a href="https://helpctr.walgreens.com/docugen/viewmenu/immunization_services"
                    class="class24" target="_blank">Immunization Service Desk</a></span> can be
                accessed directly through the <span class="class24"><a href="http://snetapp.walgreens.com/SNETGatewayLBWeb/walgreens/snet/utl/StoreNetMain.html"
                    class="class24" target="_blank">Immunization Services Storenet page</a></span>
                (Storenet > Patient Care > Immunization Services), by clicking the Immunization
                Service Desk hyperlink under the “Questions” header located on the main page.</p>
        </td>
    </tr>
</table>
