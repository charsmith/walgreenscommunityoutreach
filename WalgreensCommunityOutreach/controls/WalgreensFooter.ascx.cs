﻿using System;
using TdApplicationLib;

public partial class controls_WalgreensFooter : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AppCommonSession common_app_session = AppCommonSession.initCommonAppSession();
            if (common_app_session.SelectedStoreSession.OutreachProgramSelectedId == "1" || common_app_session.SelectedStoreSession.OutreachProgramSelectedId == null)
            {
                this.tblfooter.Visible = false;
            }
        }

    }
}