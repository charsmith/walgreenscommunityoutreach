﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using System.Data;
using System.Xml;
using System.Text;
using System.Web.Services;
using TdWalgreens;
using NLog;


public partial class CorporateClinics : System.Web.UI.UserControl
{
    #region ------------------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ddlUnassignedClinicsFilters.bindDataFilterTypes(commonAppSession.LoginUserInfoSession.UserRole);
            this.ddlAssignedDistrictClinicsFilters.bindDataFilterTypes(commonAppSession.LoginUserInfoSession.UserRole);
            this.ddlAssignedStoreClinicsFilters.bindDataFilterTypes(commonAppSession.LoginUserInfoSession.UserRole);

            if (this.commonAppSession.LoginUserInfoSession != null && (this.commonAppSession.LoginUserInfoSession.IsAdmin || this.commonAppSession.LoginUserInfoSession.IsPowerUser))
            {
                this.commonAppSession.SelectedStoreSession.SelectedCorporateClinicsTabId = "UnassignedClinics";
                this.bindNationalAccounts();

                if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    this.bindCancelledClinics();
            }
            else
                Response.Redirect("walgreensLandingPage.aspx");
        }
        else
        {
            string event_args = Request["__EVENTTARGET"];
            if (event_args.ToLower() == "rescheduleappts")
            {
                //TODO - Vishnu
                //bool reschedule_appts = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                //if (reschedule_appts)
                //{
                //    //string error_message = string.Empty;
                //    //int return_value = 0;
                //}
            }
        }

        this.displayTabs();
    }

    protected void btnApproveAssignment_Click(object sender, CommandEventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "btnApproveAssignment_Click", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);

        this.setStoreChecked(e.CommandArgument.ToString());
        this.updateStoreAssignment(false, this.grdNcStoreAssignment, false);
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;

        int return_value = 0;
        string date_time_stamp_message = "";
        XmlDocument national_accounts = this.createUpdatedUnAssignedClinicsXml("send");
        if (national_accounts.SelectSingleNode("//nationalAccount") != null)
        {
            return_value = this.dbOperation.updateConfirmNaStoreAssignment(national_accounts, "send", out date_time_stamp_message, this.commonAppSession.LoginUserInfoSession.UserID);
            this.logger.Info("Database operation {0} done with return value : {1}", "updateConfirmNaStoreAssignment", return_value.ToString());

            if (return_value == 0)
            {
                this.emailOperations.sendCorporateClinicStoreAssignmentEmail(this.dtCorporateClinics, this.commonAppSession.LoginUserInfoSession.UserID, "store");

                this.commonAppSession.dtCorporateClinics = new DataTable();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('CorporateClinics'); alert('" + (string)GetGlobalResourceObject("errorMessages", "updateStoreAssignment") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
            }
            else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
            {
                this.logger.Info("Date Time Overlap issue occured with date_time_stamp_message : {0}", date_time_stamp_message);
                this.commonAppSession.dtCorporateClinics = new DataTable();
                //ViewState["corporateClinics"] = this.dbOperation.getCorporateClinics(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
                string validation_message = (string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictMessageDetails");
                List<string> failed_locations = date_time_stamp_message.Split('|').ToList();
                string title_text = "Multiple Conflicts Alert";
                if (failed_locations.Count == 1)
                {
                    title_text = "Single Conflict Alert";
                    string failed_location = failed_locations.First();
                    validation_message = String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapSingleConflictDetails"), failed_location.Split('<')[1], Convert.ToDateTime(failed_location.Split('<')[2]).ToShortDateString(), failed_location.Split('<')[3], failed_location.Split('<')[4], failed_location.Split('<')[0]);
                }
                else
                {
                    failed_locations.ForEach(x =>
                    {
                        validation_message += String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictLocations"), x.Split('<')[1], Convert.ToDateTime(x.Split('<')[2]).ToShortDateString(), x.Split('<')[3], x.Split('<')[4], x.Split('<')[0]) + "\\n";
                    });
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDateTimeStampValidationMessage('" + validation_message + "','" + title_text + "');", true);
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "btnApproveAssignment_Click", System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    protected void btnApproveDistrictAssignment_Click(object sender, CommandEventArgs e)
    {
        this.setStoreChecked(e.CommandArgument.ToString());
        this.updateStoreAssignment(false, this.grdNcStoreAssignment, false);
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;
        XmlDocument national_accounts = this.createUpdatedUnAssignedClinicsXml("sendDistrict");

        if (!string.IsNullOrEmpty(national_accounts.InnerXml))
        {
            int return_value = 0;
            string date_time_stamp_message = "";

            return_value = this.dbOperation.updateConfirmNaStoreAssignment(national_accounts, "sendDistrict", out date_time_stamp_message);
            if (return_value == 0)
            {
                this.emailOperations.sendCorporateClinicStoreAssignmentEmail(this.dtCorporateClinics, this.commonAppSession.LoginUserInfoSession.UserID, "district");

                this.commonAppSession.dtCorporateClinics = new DataTable();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('CorporateClinics'); alert('" + (string)GetGlobalResourceObject("errorMessages", "updateDistrictAssignment") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
            }
            else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
            {

                this.commonAppSession.dtCorporateClinics = new DataTable();
                //ViewState["corporateClinics"] = this.dbOperation.getCorporateClinics(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
                string validation_message = (string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictMessageDetails");
                List<string> failed_locations = date_time_stamp_message.Split('|').Select(x => x.Trim()).ToList();
                string title_text = "Multiple Conflicts Alert";
                if (failed_locations.Count == 1)
                {
                    title_text = "Single Conflict Alert";
                    string failed_location = failed_locations.First();
                    validation_message = String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapSingleConflictDetails"), failed_location.Split('<')[1], Convert.ToDateTime(failed_location.Split('<')[2]).ToShortDateString(), failed_location.Split('<')[3], failed_location.Split('<')[4], failed_location.Split('<')[0]);
                }
                else
                {
                    failed_locations.ForEach(x =>
                    {
                        validation_message += String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictLocations"), x.Split('<')[1], Convert.ToDateTime(x.Split('<')[2]).ToShortDateString(), x.Split('<')[3], x.Split('<')[4], x.Split('<')[0]) + "\\n";
                    });
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDateTimeStampValidationMessage('" + validation_message + "','" + title_text + "');", true);
            }
        }
    }

    protected void btnReviseAssignment_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        this.updateStoreAssignment(false, this.grdAssignedClinics, true);
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;

        int return_value = 0;
        string date_time_stamp_message = "";
        XmlDocument assigned_clinics = this.createUpdatedAssignedClinicsXml();
        if (assigned_clinics.SelectSingleNode("//assignedClinic") != null)
        {
            return_value = this.dbOperation.updateClinicStoreReAssignment(assigned_clinics, "ClinicStore", out date_time_stamp_message);

            if (return_value == 0)
            {
                //sending email to the users
                if (this.commonAppSession.dtCorporateClinics.Rows.Count != 0)
                {
                    this.emailOperations.sendRevisedStoreAssignments(corporateClinics, "newStore", this.commonAppSession.LoginUserInfoSession.UserID);
                    this.emailOperations.sendRevisedStoreAssignments(corporateClinics, "oldStore", this.commonAppSession.LoginUserInfoSession.UserID);
                }

                this.commonAppSession.dtCorporateClinics = new DataTable();
                //ViewState["corporateClinics"] = this.dbOperation.getCorporateClinics(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('CorporateClinics'); alert('" + (string)GetGlobalResourceObject("errorMessages", "updateStoreReAssignment") + "');", true);
            }
            else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
            {

                this.commonAppSession.dtCorporateClinics = new DataTable();
                //ViewState["corporateClinics"] = this.dbOperation.getCorporateClinics(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
                string validation_message = (string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictMessageDetails");
                List<string> failed_locations = date_time_stamp_message.Split('|').Select(x => x.Trim()).ToList();
                string title_text = "Multiple Conflicts Alert";
                if (failed_locations.Count == 1)
                {
                    title_text = "Single Conflict Alert";
                    string failed_location = failed_locations.First();
                    validation_message = String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapSingleConflictDetails"), failed_location.Split('<')[1], Convert.ToDateTime(failed_location.Split('<')[2]).ToShortDateString(), failed_location.Split('<')[3], failed_location.Split('<')[4], failed_location.Split('<')[0]);
                }
                else
                {
                    failed_locations.ForEach(x =>
                    {
                        validation_message += String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictLocations"), x.Split('<')[1], Convert.ToDateTime(x.Split('<')[2]).ToShortDateString(), x.Split('<')[3], x.Split('<')[4], x.Split('<')[0]) + "\\n";
                    });
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDateTimeStampValidationMessage('" + validation_message + "','" + title_text + "');", true);
            }
        }

        this.bindNationalAccounts();
    }

    //protected void btnResolveDuplicates_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    //{
    //    StringBuilder remove_businesses_list = new StringBuilder();
    //    StringBuilder keep_businesses_list = new StringBuilder();
    //    RadioButtonList rbl_resolution;

    //    foreach (GridViewRow row in this.grdDuplicateBusiness.Rows)
    //    {
    //        rbl_resolution = (RadioButtonList)row.FindControl("rblResolution");

    //        if (!string.IsNullOrEmpty(rbl_resolution.SelectedValue) && Convert.ToInt32(rbl_resolution.SelectedValue) == 0)
    //            remove_businesses_list.Append(((Label)row.FindControl("lblBusinessPk")).Text + ",");
    //        if (!string.IsNullOrEmpty(rbl_resolution.SelectedValue) && Convert.ToInt32(rbl_resolution.SelectedValue) == 1)
    //            keep_businesses_list.Append(((Label)row.FindControl("lblBusinessPk")).Text + ",");
    //    }

    //    int return_value = this.dbOperation.processDuplicateBusiness(remove_businesses_list.ToString().TrimEnd(','), keep_businesses_list.ToString().TrimEnd(','));
    //    this.bindDuplicateBusiness();
    //}

    protected void btnCancelScheduledAppts_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        StringBuilder cancelled_clinics_list = new StringBuilder();
        Label cancelled_clinic;
        foreach (GridViewRow row in this.grdCancelledClinics.Rows)
        {
            cancelled_clinic = (Label)row.FindControl("lblClinicPk");
            cancelled_clinics_list.Append(((Label)row.FindControl("lblClinicPk")).Text + ",");
        }

        DataTable dt_block_appts = new DataTable();
        int return_value = this.dbOperation.SendClinicCancelledEmail(cancelled_clinics_list.ToString().TrimEnd(','), out dt_block_appts);

        if (dt_block_appts != null && dt_block_appts.Rows.Count > 0)
            this.emailOperations.sendCancelClinicApptsEmail(dt_block_appts);

        this.bindCancelledClinics();
    }

    protected void imgBtnUnassignedClinicsSearch_Click(object sender, ImageClickEventArgs e)
    {
        //hdnUACorpShowMoreThan10.Value = "true";
        this.bindUnassignedSearchResults();
    }

    protected void imgBtnAssignedDistrictClinicsSearch_Click(object sender, ImageClickEventArgs e)
    {
        //hdnADCorpShowMoreThan10.Value = "true";
        this.bindAssignedToDistrctSearchResults();
    }

    protected void imgBtnAssignedClinicsSearch_Click(object sender, ImageClickEventArgs e)
    {
        //hdnACorpShowMoreThan10.Value = "true";
        this.bindAssignedSearchResults();
    }

    protected void ddlDistrictsAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl_districts = (DropDownList)sender;
        GridViewRow clinic_row = (GridViewRow)ddl_districts.NamingContainer;
        DropDownList ddl_clinic_district = (DropDownList)clinic_row.FindControl("ddlDistrictsAvailable");
        DropDownList ddl_clinic_store = (DropDownList)clinic_row.FindControl("ddlStoreAvailable");
        Label lbl_clinic_pk = (Label)clinic_row.FindControl("lblClinicPk");

        this.bindUserStores(ddl_clinic_store, string.Empty, ddl_clinic_district.SelectedValue.ToString(), lbl_clinic_pk.Text);
    }

    protected void grdNcStoreAssignment_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.updateStoreAssignment(false, this.grdNcStoreAssignment, false);
        this.grdNcStoreAssignment.EditIndex = e.NewEditIndex;
        this.grdAssignedClinics.EditIndex = -1;
        this.grdAssignedDistrictClinics.EditIndex = -1;
        this.saveClinicStoreAssignment();

        if (this.grdCancelledClinics != null && this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.grdCancelledClinics.EditIndex = -1;
            this.bindCancelledClinics();
        }

        this.bindNationalAccounts();

        DropDownList ddl_stores = (DropDownList)(this.grdNcStoreAssignment.Rows[this.grdNcStoreAssignment.EditIndex].FindControl("ddlStoreAvailable"));
        Label lbl_storeid = (Label)(this.grdNcStoreAssignment.Rows[this.grdNcStoreAssignment.EditIndex].FindControl("lblStoreId"));
        Label lbl_district_id = (Label)(this.grdNcStoreAssignment.Rows[this.grdNcStoreAssignment.EditIndex].FindControl("lblDistrictId"));
        //Label lbl_business_name = (Label)(this.grdNcStoreAssignment.Rows[this.grdNcStoreAssignment.EditIndex].FindControl("lblClientName"));
        Label lbl_clinic_pk = (Label)(this.grdNcStoreAssignment.Rows[this.grdNcStoreAssignment.EditIndex].FindControl("lblClinicPk"));

        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
        {
            DropDownList ddl_districts = (DropDownList)(this.grdNcStoreAssignment.Rows[this.grdNcStoreAssignment.EditIndex].FindControl("ddlDistrictsAvailable"));
            DataTable dt_districts = this.dbOperation.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, null, null, null);
            ddl_districts.DataSource = dt_districts;
            ddl_districts.DataTextField = dt_districts.Columns[1].ToString();
            ddl_districts.DataValueField = dt_districts.Columns[0].ToString();
            ddl_districts.DataBind();

            if (ddl_districts.Items.FindByValue(lbl_district_id.Text) != null)
                ddl_districts.Items.FindByValue(lbl_district_id.Text).Selected = true;
        }
        else
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_districts = (System.Web.UI.HtmlControls.HtmlTableRow)(this.grdNcStoreAssignment.Rows[this.grdNcStoreAssignment.EditIndex].FindControl("rowClinicDistricts"));
            row_districts.Visible = false;
        }

        this.bindUserStores(ddl_stores, lbl_storeid.Text, lbl_district_id.Text, lbl_clinic_pk.Text);
    }

    protected void grdNcStoreAssignment_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.dtCorporateClinics = corporateClinics;
        this.updateStoreAssignment(true, this.grdNcStoreAssignment, false);
        this.grdNcStoreAssignment.EditIndex = -1;
        this.saveClinicStoreAssignment();

        this.bindNationalAccounts();
    }

    protected void grdNcStoreAssignment_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdNcStoreAssignment.PageIndex = e.NewPageIndex;
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedDistrictClinics.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;

        if (this.grdCancelledClinics != null && this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.grdCancelledClinics.EditIndex = -1;
            this.bindCancelledClinics();
        }

        this.bindNationalAccounts();
    }

    protected void grdNcStoreAssignment_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindNationalAccounts();
    }

    protected void grdNcStoreAssignment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox chk_selectAllUnassignedRows = (CheckBox)e.Row.FindControl("chkSelectAllUnassignedClinics");
            //CheckBox chk_selectedRow = (CheckBox)e.Row.FindControl("chkSelectUnassignedClinic");
            if (chkSelectAllUnassignedClinicsInDistrict.Checked)
                chk_selectAllUnassignedRows.Checked = true;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk_selectedRow = (CheckBox)e.Row.FindControl("chkSelectUnassignedClinic");
            if (chkSelectAllUnassignedClinicsInDistrict.Checked)
                chk_selectedRow.Checked = true;

            Label lbl_clinic_type = (Label)e.Row.FindControl("lblClinicType");
            if (lbl_clinic_type != null && lbl_clinic_type.Text == "Uploaded Local")
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#006699");
            }
            if (lbl_clinic_type != null && lbl_clinic_type.Text == "Uploaded Charity")
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#418200");
            }
        }
    }

    protected void grdAssignedDistrictClinics_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.updateStoreAssignment(false, this.grdAssignedDistrictClinics, false);
        this.grdAssignedDistrictClinics.EditIndex = e.NewEditIndex;
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;
        this.saveClinicStoreAssignment();

        if (this.grdCancelledClinics != null && this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.grdCancelledClinics.EditIndex = -1;
            this.bindCancelledClinics();
        }

        this.bindNationalAccounts();

        DropDownList ddl_stores = (DropDownList)(this.grdAssignedDistrictClinics.Rows[this.grdAssignedDistrictClinics.EditIndex].FindControl("ddlStoreAvailable"));
        Label lbl_storeid = (Label)(this.grdAssignedDistrictClinics.Rows[this.grdAssignedDistrictClinics.EditIndex].FindControl("lblStoreId"));
        Label lbl_district_id = (Label)(this.grdAssignedDistrictClinics.Rows[this.grdAssignedDistrictClinics.EditIndex].FindControl("lblDistrictId"));
        //Label lbl_business_name = (Label)(this.grdAssignedDistrictClinics.Rows[this.grdAssignedDistrictClinics.EditIndex].FindControl("lblClientName"));
        Label lbl_clinic_pk = (Label)(this.grdAssignedDistrictClinics.Rows[this.grdAssignedDistrictClinics.EditIndex].FindControl("lblClinicPk"));

        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
        {
            DropDownList ddl_districts = (DropDownList)(this.grdAssignedDistrictClinics.Rows[this.grdAssignedDistrictClinics.EditIndex].FindControl("ddlDistrictsAvailable"));
            DataTable dt_districts = this.dbOperation.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, null, null, null);
            ddl_districts.DataSource = dt_districts;
            ddl_districts.DataTextField = dt_districts.Columns[1].ToString();
            ddl_districts.DataValueField = dt_districts.Columns[0].ToString();
            ddl_districts.DataBind();

            if (ddl_districts.Items.FindByValue(lbl_district_id.Text) != null)
                ddl_districts.Items.FindByValue(lbl_district_id.Text).Selected = true;
        }
        else
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_districts = (System.Web.UI.HtmlControls.HtmlTableRow)(this.grdAssignedDistrictClinics.Rows[this.grdAssignedDistrictClinics.EditIndex].FindControl("rowClinicDistricts"));
            row_districts.Visible = false;
        }

        this.bindUserStores(ddl_stores, lbl_storeid.Text, lbl_district_id.Text, lbl_clinic_pk.Text);
    }

    protected void grdAssignedDistrictClinics_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.dtCorporateClinics = corporateClinics;
        this.updateStoreAssignment(true, this.grdAssignedDistrictClinics, false);
        this.grdAssignedDistrictClinics.EditIndex = -1;
        this.saveClinicStoreAssignment();

        this.bindNationalAccounts();
    }

    protected void grdAssignedDistrictClinics_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdAssignedDistrictClinics.PageIndex = e.NewPageIndex;
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;
        this.grdAssignedDistrictClinics.EditIndex = -1;

        if (this.grdCancelledClinics != null && this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.grdCancelledClinics.EditIndex = -1;
            this.bindCancelledClinics();
        }

        this.bindNationalAccounts();
    }

    protected void grdAssignedDistrictClinics_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindNationalAccounts();
    }

    protected void grdAssignedDistrictClinics_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox chk_selectAllUnassignedRows = (CheckBox)e.Row.FindControl("chkSelectAllAssignedDistrictClinics");
            if (chkSelectAllAssignedDistrictClinicsInDistrict.Checked)
                chk_selectAllUnassignedRows.Checked = true;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk_selectedRow = (CheckBox)e.Row.FindControl("chkSelectAssignedDistrictClinic");
            if (chkSelectAllAssignedDistrictClinicsInDistrict.Checked)
                chk_selectedRow.Checked = true;

            Label lbl_clinic_type = (Label)e.Row.FindControl("lblClinicType");
            if (lbl_clinic_type != null && lbl_clinic_type.Text == "Uploaded Local")
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#006699");
            }
            if (lbl_clinic_type != null && lbl_clinic_type.Text == "Uploaded Charity")
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#418200");
            }
        }
    }

    protected void grdAssignedClinics_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string clinic_type = "";
        this.updateStoreAssignment(false, this.grdAssignedClinics, true);
        this.grdAssignedClinics.EditIndex = e.NewEditIndex;
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedDistrictClinics.EditIndex = -1;
        if (this.grdCancelledClinics != null && this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.grdCancelledClinics.EditIndex = -1;
            this.bindCancelledClinics();
        }

        this.bindNationalAccounts();

        DropDownList ddl_stores = (DropDownList)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl("ddlStoreAvailable"));
        Label lbl_storeid = (Label)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl("lblStoreId"));
        Label lbl_business_name = (Label)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl("lblClientName"));

        Label lbl_clinic_type = (Label)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl(("lblClinicType")));
        clinic_type = lbl_clinic_type != null ? lbl_clinic_type.Text : "";

        this.bindUserStores(ddl_stores, lbl_storeid.Text, string.Empty, string.Empty);

        //bind lead store id's
        DataTable dt_client_stores = this.dbOperation.getCorporateClientStores(lbl_business_name.Text.Substring(0, lbl_business_name.Text.LastIndexOf('-')));
        if (dt_client_stores != null && dt_client_stores.Rows.Count > 1 && clinic_type != "Uploaded Local")
        {
            DropDownList ddl_lead_stores = (DropDownList)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl("ddlLeadStores"));
            Label lbl_lead_storeid = (Label)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl("lblLeadStoreId"));
            CheckBox chk_lead_store = (CheckBox)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl("chkLeadStore"));
            System.Web.UI.HtmlControls.HtmlTable tbl_lead_store_address = (System.Web.UI.HtmlControls.HtmlTable)(this.grdAssignedClinics.Rows[this.grdAssignedClinics.EditIndex].FindControl("tblLeadStoreAssignment"));
            tbl_lead_store_address.Visible = true;
            ddl_lead_stores.DataSource = dt_client_stores;
            ddl_lead_stores.DataTextField = dt_client_stores.Columns[1].ToString();
            ddl_lead_stores.DataValueField = dt_client_stores.Columns[0].ToString();
            ddl_lead_stores.DataBind();

            ddl_lead_stores.Items.Insert(0, new ListItem("-- Select Lead Store --", "0"));
            if (ddl_lead_stores.Items.FindByValue(lbl_lead_storeid.Text) != null)
                ddl_lead_stores.Items.FindByValue(lbl_lead_storeid.Text).Selected = true;

            if (!string.IsNullOrEmpty(lbl_lead_storeid.Text))
                chk_lead_store.Checked = true;
        }
    }

    protected void grdAssignedClinics_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.dtCorporateClinics = corporateClinics;
        this.updateStoreAssignment(true, this.grdAssignedClinics, true);
        this.grdAssignedClinics.EditIndex = -1;
        XmlDocument assigned_clinics = this.createUpdatedAssignedClinicsXml();

        if (assigned_clinics.SelectNodes("//assignedClinic[@isLeadStoreUpdated='1']").Count > 0)
        {
            int return_value = 0;
            string date_time_stamp_error_message = "";
            return_value = this.dbOperation.updateClinicStoreReAssignment(assigned_clinics, "LeadStore", out date_time_stamp_error_message);

            if (return_value == 0)
            {
                //sending email to the users
                //if (ViewState["corporateClinics"] != null)
                //{
                this.emailOperations.sendRevisedStoreAssignments(corporateClinics, "newLeadStore", this.commonAppSession.LoginUserInfoSession.UserID);
                //this.emailOperations.sendRevisedStoreAssignments((DataTable)ViewState["corporateClinics"], "oldStore", this.commonAppSession.LoginUserInfoSession.UserID);

                DataTable dt_national_accounts = corporateClinics;
                DataRow dr_lead_store = dt_national_accounts.Select("isStoreConfirmed = 1 AND (ISNULL(isLeadStoreUpdated, 0) = '1')")[0];
                dr_lead_store["oldLeadStoreId"] = dr_lead_store["leadStoreId"].ToString();
                dr_lead_store["isLeadStoreUpdated"] = "0";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('CorporateClinics'); alert('" + (string)GetGlobalResourceObject("errorMessages", "worksiteClinicLeadStoreAssignment") + "');", true);
                //}
            }
            else if (return_value == -4 && !string.IsNullOrEmpty(date_time_stamp_error_message))
            {
                this.commonAppSession.dtCorporateClinics = new DataTable();
                string validation_message = (string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictMessageDetails");
                List<string> failed_locations = date_time_stamp_error_message.Split(',').ToList();
                string title_text = "Multiple Conflicts Alert";
                if (failed_locations.Count == 1)
                {
                    title_text = "Single Conflict Alert";
                    string failed_location = failed_locations.First();
                    validation_message = String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapSingleConflictDetails"), failed_location.Split('<')[1], Convert.ToDateTime(failed_location.Split('<')[2]).ToShortDateString(), failed_location.Split('<')[3], failed_location.Split('<')[4], failed_location.Split('<')[0]);
                }
                else
                {
                    failed_locations.ForEach(x =>
                    {
                        validation_message += String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictLocations"), x.Split('<')[1], Convert.ToDateTime(x.Split('<')[2]).ToShortDateString(), x.Split('<')[3], x.Split('<')[4], x.Split('<')[0]) + "\\n";
                    });
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDateTimeStampValidationMessage('" + validation_message + "','" + title_text + "');", true);
            }
        }

        this.bindNationalAccounts();
    }

    protected void grdAssignedClinics_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdAssignedClinics.PageIndex = e.NewPageIndex;
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;
        if (this.grdCancelledClinics != null && this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.grdCancelledClinics.EditIndex = -1;
            this.bindCancelledClinics();
        }

        this.bindNationalAccounts();
    }

    protected void grdAssignedClinics_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindNationalAccounts();
    }

    protected void grdAssignedClinics_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton img_btn_edit = (ImageButton)e.Row.FindControl("imgBtnEdit");
            Label lbl_assigned_store_address = (Label)e.Row.FindControl("lblAssignedStore");
            Label lbl_iscurrent = (Label)e.Row.FindControl("lblIsCurrent");
            Label lbl_lead_storeid = (Label)e.Row.FindControl("lblLeadStoreId");
            Label lbl_clinic_type = (Label)e.Row.FindControl("lblClinicType");
            if (lbl_clinic_type != null && lbl_clinic_type.Text == "Uploaded Local")
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#006699");
            }
            if (lbl_clinic_type != null && lbl_clinic_type.Text == "Uploaded Charity")
            {
                e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#418200 ");
            }
            if (!string.IsNullOrEmpty(lbl_iscurrent.Text) && Convert.ToInt32(lbl_iscurrent.Text) == 0)
            {
                img_btn_edit.Visible = false;
                lbl_assigned_store_address.Text = "Cancelled Clinic";
            }

            if (!string.IsNullOrEmpty(lbl_lead_storeid.Text))
            {
                lbl_lead_storeid.Text = (lbl_lead_storeid.Text == "0" ? "" : lbl_lead_storeid.Text);
                System.Web.UI.HtmlControls.HtmlTable tbl_lead_store = (System.Web.UI.HtmlControls.HtmlTable)e.Row.FindControl("tblLeadStoreId");
                System.Web.UI.HtmlControls.HtmlTable tbl_lead_store_address = (System.Web.UI.HtmlControls.HtmlTable)e.Row.FindControl("tblLeadStoreAddress");
                if (tbl_lead_store != null && tbl_lead_store_address != null)
                {
                    tbl_lead_store.Visible = true;
                    tbl_lead_store_address.Visible = true;
                }
            }
        }
    }

    protected void grdCancelledClinics_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdCancelledClinics.PageIndex = e.NewPageIndex;
        this.grdCancelledClinics.EditIndex = -1;
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;
        this.bindCancelledClinics();
    }

    protected void grdCancelledClinics_sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindCancelledClinics();
    }

    protected void grdCancelledClinics_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.grdCancelledClinics.EditIndex = e.NewEditIndex;
        this.grdNcStoreAssignment.EditIndex = -1;
        this.grdAssignedClinics.EditIndex = -1;
        this.grdAssignedDistrictClinics.EditIndex = -1;
        this.bindCancelledClinics();

        DropDownList ddl_outreach_statuses = (DropDownList)(this.grdCancelledClinics.Rows[this.grdCancelledClinics.EditIndex].FindControl("ddlOutreachStatuses"));
        this.bindOutreachStatuses(ddl_outreach_statuses);
    }

    protected void grdCancelledClinics_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DropDownList ddl_outreach_statuses = (DropDownList)(this.grdCancelledClinics.Rows[this.grdCancelledClinics.EditIndex].FindControl("ddlOutreachStatuses"));
        Label lbl_clinic_pk = (Label)(this.grdCancelledClinics.Rows[this.grdCancelledClinics.EditIndex].FindControl("lblClinicPk"));
        Label lbl_outreach_business_pk = (Label)(this.grdCancelledClinics.Rows[this.grdCancelledClinics.EditIndex].FindControl("lblOutreachBusinessPk"));

        int return_value = 0;
        if (Convert.ToInt32(ddl_outreach_statuses.SelectedValue) != 0)
            return_value = this.dbOperation.UpdateCancelledClinicsContactStatus(Convert.ToInt32(lbl_clinic_pk.Text), Convert.ToInt32(lbl_outreach_business_pk.Text), Convert.ToInt32(ddl_outreach_statuses.SelectedValue), this.commonAppSession.LoginUserInfoSession.UserID, DateTime.Now);

        this.grdCancelledClinics.EditIndex = -1;
        this.bindCancelledClinics();
    }

    /*
    protected void grdDuplicateBusiness_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contacts = (Label)e.Row.FindControl("lblContacts");
            Label lbl_keep_business = (Label)e.Row.FindControl("lblKeepBusiness");
            RadioButtonList rbl_resolution = (RadioButtonList)e.Row.FindControl("rblResolution");
            if ((!(string.IsNullOrEmpty(lbl_contacts.Text)) && Convert.ToInt32(lbl_contacts.Text) > 0) || (!string.IsNullOrEmpty(lbl_keep_business.Text) && Convert.ToBoolean(lbl_keep_business.Text)))
            {
                foreach (ListItem item in rbl_resolution.Items)
                {
                    if (item.Text.ToLower() == "keep")
                        item.Selected = true;

                    if (!(string.IsNullOrEmpty(lbl_contacts.Text)) && Convert.ToInt32(lbl_contacts.Text) > 0)
                        item.Enabled = false;
                }
            }
            Label lbl_duplicate_type = (Label)e.Row.FindControl("lblDuplication");
            if (lbl_duplicate_type.Text.Substring(0, 2) == ", ")
                lbl_duplicate_type.Text = lbl_duplicate_type.Text.Replace(lbl_duplicate_type.Text.Substring(0, 2), "");
            if (lbl_duplicate_type.Text.EndsWith(", "))
                lbl_duplicate_type.Text = lbl_duplicate_type.Text.Replace(", ", "");
        }
    }

    protected void grdDuplicateBusiness_sorting(object sender, GridViewSortEventArgs e)
    {
        //DataTable dt_businesses = this.dbOperation.getDuplicateBusinesses(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
        //if (dt_businesses.Rows.Count > 0)
        //{
        ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
        this.bindDuplicateBusiness();
        this.setDuplicateRowsBackColor();
        //}
    }
    */

    //protected void grdDuplicateBusiness_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    this.grdDuplicateBusiness.PageIndex = e.NewPageIndex;
    //    this.bindDuplicateBusiness();
    //}

    //protected void btnRefresh_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    //{

    //}

    protected void grdAssignedClinics_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdAssignedClinics.EditIndex = -1;
        this.bindAllGrids();
    }

    protected void grdCancelledClinics_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdCancelledClinics.EditIndex = -1;
        this.bindAllGrids();
    }

    protected void grdAssignedDistrictClinics_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdAssignedDistrictClinics.EditIndex = -1;
        this.bindAllGrids();
    }

    protected void grdNcStoreAssignment_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdNcStoreAssignment.EditIndex = -1;
        this.bindAllGrids();
    }

    protected void btnCorporateClinics_Command(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLower())
        {
            case "unassigned":
                //hdnUACorpShowMoreThan10.Value = "true";
                this.bindUnassignedSearchResults();
                break;
            case "assignedtodist":
                //hdnADCorpShowMoreThan10.Value = "true";
                this.bindAssignedToDistrctSearchResults();
                break;
            case "assigned":
                //hdnACorpShowMoreThan10.Value = "true";
                this.bindAssignedSearchResults();
                break;
        }
    }

    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList comboBox = (DropDownList)sender;
        CheckBox checkBox;
        switch (comboBox.ID)
        {
            case "ddlAssignedDistrictClinicsFilters": checkBox = this.chkSelectAllAssignedDistrictClinicsInDistrict;
                break;
            case "ddlUnassignedClinicsFilters": checkBox = this.chkSelectAllUnassignedClinicsInDistrict;
                break;
            default: checkBox = this.chkSelectAllUnassignedClinicsInDistrict;
                break;
        }
        if (comboBox.SelectedItem.Value == "District")
        {
            checkBox.Visible = true;
        }
        else
        {
            checkBox.Visible = false;
        }
    }
    #endregion

    #region ------------------- PRIVATE METHODS -------------
    /// <summary>
    /// Binding all the grids
    /// </summary>
    private void bindAllGrids()
    {
        this.bindNationalAccounts();
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
            this.bindCancelledClinics();
    }

    /// <summary>
    /// Displays corporate worksite clinics tabs
    /// </summary>
    private void displayTabs()
    {
        StringBuilder sb = new StringBuilder();
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
        {
            sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='UnassignedClinics' style='width: 180px;' href='#idUnassignedClinics' runat='server' onclick=setSelectedTab('UnassignedClinics')><span style='font-weight:bold'>Worksite Clinic <br />Assignments Needed</span></a></li>");
            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
                sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='AssignedDistrictClinics' style='width: 180px;' href='#idAssignedDistrictClinics' runat='server' onclick=setSelectedTab('AssignedDistrictClinics')><span style='font-weight:bold'>Worksite Clinics<br />Assigned to District</span></a></li>");

            sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='AssignedStoreClinics' style='width: 180px;' href='#idAssignedStoreClinics' runat='server' onclick=setSelectedTab('AssignedStoreClinics')><span style='font-weight:bold'>Assigned <br />Worksite Clinics</span></a></li>");

            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='CancelledClinics' style='width: 180px;' href='#idCancelledClinics' runat='server' onclick=setSelectedTab('CancelledClinics')><span style='font-weight:bold; padding-top:3px;'>Cancelled Clinic <br />Approvals Needed</span></a></li>");
        }

        if (this.commonAppSession.LoginUserInfoSession.DuplicateBusinessCount > 0)
            sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='ResolveDupBusiness' href='#idResolveDupBusiness' runat='server' onclick=setSelectedTab('ResolveDupBusiness');><span style='font-weight:bold'>Resolve <br />Duplicate Businesses</span></a></li>");

        this.lblTabLinks.Text = sb.ToString();
    }

    /// <summary>
    /// Binds corporate worksite clinics
    /// </summary>
    private void bindNationalAccounts()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "bindNationalAccounts",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);

        DataTable dt_corporate_clinics_copy;
        if (this.commonAppSession.dtCorporateClinics.Rows.Count == 0)
        {
            this.dtCorporateClinics = this.dbOperation.getCorporateClinics(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
            corporateClinics = this.dtCorporateClinics;
        }
        else
            this.dtCorporateClinics = corporateClinics;

        this.ltlUnassignedClinicsNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoWorksiteClinicsAssignmentAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoWorksiteClinicsAssignment"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlAssignedDistrictClinicsNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoAssignedDistrictWorksiteClinicsAssignmentAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoAssignedDistrictWorksiteClinicsAssignment"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlAssignedStoreClinicsNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoWorksiteClinicsAssignedAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoWorksiteClinicsAssigned"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlUnassignedClinicsNote.Text = this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager" ? string.Format((string)GetGlobalResourceObject("errorMessages", "worksiteClinicsAssignmentNote"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower()) : (string)GetGlobalResourceObject("errorMessages", "worksiteClinicsAssignmentNoteHCS"); ;
        this.ltlAssignedDistrictClinicsNote.Text = (string)GetGlobalResourceObject("errorMessages", "WorksiteClinicsAssignedtoDistrictNote");

        if (this.dtCorporateClinics.Rows.Count == 0)
        {
            this.rowUnassignedClinics.Visible = false;
            this.rowAssignedStoreClinics.Visible = false;
            this.rowAssignedDistrictClinics.Visible = false;
            this.rowUnassignedClinicsNoRecords.Visible = true;
            this.rowAssignedStoreClinicsNoRecords.Visible = true;
            this.rowAssignedDistrictClinicsNoRecords.Visible = true;

            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                this.rowAssignedDistrictClinicsNoRecords.Visible = false;
        }
        else
        {
            //DataRow[] unassigned_clinics = this.dtCorporateClinics.Select(this.getFilterExpression(false, "UnAssigned"));
            DataRow[] unassigned_clinics = this.commonAppSession.dtCorporateClinics.Select(this.getFilterExpression(false, "UnAssigned"));
            if (unassigned_clinics.Count() > 0)
            {
                this.rowUnassignedClinics.Visible = true;
                this.rowUnassignedClinicsNoRecords.Visible = false;
                dt_corporate_clinics_copy = new DataTable();
                //dt_corporate_clinics_copy = !Convert.ToBoolean(hdnUACorpShowMoreThan10.Value) ? unassigned_clinics.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : unassigned_clinics.CopyToDataTable();
                dt_corporate_clinics_copy = unassigned_clinics.CopyToDataTable();
                if ((ViewState["sortOrder"] != null) && (dt_corporate_clinics_copy.Columns.Contains(GridSortExpression)))
                    dt_corporate_clinics_copy.DefaultView.Sort = ViewState["sortOrder"].ToString();
                else
                    dt_corporate_clinics_copy.DefaultView.Sort = defaultSortExp;

                this.grdNcStoreAssignment.DataSource = dt_corporate_clinics_copy;
                this.grdNcStoreAssignment.DataBind();

                if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                    this.btnApproveDistrictAssignment.Visible = false;
            }
            else
            {
                this.rowUnassignedClinics.Visible = false;
                this.rowUnassignedClinicsNoRecords.Visible = true;
            }

            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
            {
                //DataRow[] assigned_district_clinics = this.dtCorporateClinics.Select(this.getFilterExpression(false, "AssignedDistrict"));
                DataRow[] assigned_district_clinics = this.commonAppSession.dtCorporateClinics.Select(this.getFilterExpression(false, "AssignedDistrict"));
                if (assigned_district_clinics.Count() > 0)
                {
                    this.rowAssignedDistrictClinics.Visible = true;
                    this.rowAssignedDistrictClinicsNoRecords.Visible = false;
                    dt_corporate_clinics_copy = new DataTable();
                    //dt_corporate_clinics_copy = !Convert.ToBoolean(hdnADCorpShowMoreThan10.Value) ? assigned_district_clinics.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : assigned_district_clinics.CopyToDataTable();
                    dt_corporate_clinics_copy = assigned_district_clinics.CopyToDataTable();
                    if ((ViewState["sortOrder"] != null) && (dt_corporate_clinics_copy.Columns.Contains(GridSortExpression)))
                        dt_corporate_clinics_copy.DefaultView.Sort = ViewState["sortOrder"].ToString();
                    else
                        dt_corporate_clinics_copy.DefaultView.Sort = defaultSortExp;

                    this.grdAssignedDistrictClinics.DataSource = dt_corporate_clinics_copy;
                    this.grdAssignedDistrictClinics.DataBind();
                }
                else
                {
                    this.rowAssignedDistrictClinics.Visible = false;
                    this.rowAssignedDistrictClinicsNoRecords.Visible = true;
                }
            }

            //DataRow[] assigned_clinics = this.dtCorporateClinics.Select(this.getFilterExpression(true, "AssignedStore"));
            DataRow[] assigned_clinics = this.commonAppSession.dtCorporateClinics.Select(this.getFilterExpression(true, "AssignedStore"));
            if (assigned_clinics.Count() > 0)
            {
                this.rowAssignedStoreClinics.Visible = true;
                this.rowAssignedStoreClinicsNoRecords.Visible = false;
                dt_corporate_clinics_copy = new DataTable();
                //dt_corporate_clinics_copy = !Convert.ToBoolean(hdnACorpShowMoreThan10.Value) ? assigned_clinics.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : assigned_clinics.CopyToDataTable();
                dt_corporate_clinics_copy = assigned_clinics.CopyToDataTable();
                if ((ViewState["sortOrder"] != null) && (dt_corporate_clinics_copy.Columns.Contains(GridSortExpression)))
                    dt_corporate_clinics_copy.DefaultView.Sort = ViewState["sortOrder"].ToString();
                else
                    dt_corporate_clinics_copy.DefaultView.Sort = defaultSortExp;

                this.grdAssignedClinics.DataSource = dt_corporate_clinics_copy;
                this.grdAssignedClinics.DataBind();
            }
            else
            {
                this.rowAssignedStoreClinics.Visible = false;
                this.rowAssignedStoreClinicsNoRecords.Visible = true;
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "bindNationalAccounts",
                  System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds cancelled clinics
    /// </summary>
    private void bindCancelledClinics()
    {
        DataTable dt_cancelled_clinics = new DataTable();
        dt_cancelled_clinics = this.dbOperation.getCancelledClinicsForApproval;

        this.grdCancelledClinics.DataSource = dt_cancelled_clinics;
        this.grdCancelledClinics.DataBind();
        if (dt_cancelled_clinics != null && dt_cancelled_clinics.Rows.Count > 0)
        {
            this.rowCancelledClinics.Visible = true;
            this.rowCancelledClinicsNoRecords.Visible = false;
        }
        else
        {
            this.rowCancelledClinicsNoRecords.Visible = true;
            this.rowCancelledClinics.Visible = false;
        }
    }

    private void bindUnassignedSearchResults()
    {
        this.dtCorporateClinics = corporateClinics;
        DataRow[] dr_filtered_clinics = null;
        DataTable dt_filtered_clinics = new DataTable();
        dt_filtered_clinics = this.dtCorporateClinics.Clone();

        dr_filtered_clinics = this.dtCorporateClinics.Select(this.getFilterExpression(false, "UnAssigned"));

        if (dr_filtered_clinics.Count() > 0)
        {
            this.grdNcStoreAssignment.PageIndex = 0;
            this.btnApproveAssignment.Visible = true;
            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
                this.btnApproveDistrictAssignment.Visible = true;
            dt_filtered_clinics = dr_filtered_clinics.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && dt_filtered_clinics.Columns.Contains(GridSortExpression))
                dt_filtered_clinics.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_filtered_clinics.DefaultView.Sort = defaultSortExp;
        }
        else
        {
            this.btnApproveAssignment.Visible = false;
            this.btnApproveDistrictAssignment.Visible = false;
        }

        this.grdNcStoreAssignment.EditIndex = -1;

        this.grdNcStoreAssignment.DataSource = dt_filtered_clinics;
        this.grdNcStoreAssignment.DataBind();
    }

    private void bindAssignedToDistrctSearchResults()
    {
        this.dtCorporateClinics = corporateClinics;
        DataRow[] dr_filtered_clinics = null;
        DataTable dt_filtered_clinics = new DataTable();
        dt_filtered_clinics = this.dtCorporateClinics.Clone();

        dr_filtered_clinics = this.dtCorporateClinics.Select(this.getFilterExpression(false, "AssignedDistrict"));

        if (dr_filtered_clinics.Count() > 0)
        {
            this.grdAssignedDistrictClinics.PageIndex = 0;
            this.imgBtnAssignSendDistrictAssignment.Visible = true;
            this.imgBtnAssignSentStoreAssignment.Visible = true;
            dt_filtered_clinics = dr_filtered_clinics.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && dt_filtered_clinics.Columns.Contains(GridSortExpression))
                dt_filtered_clinics.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_filtered_clinics.DefaultView.Sort = defaultSortExp;
        }
        else
        {
            this.imgBtnAssignSendDistrictAssignment.Visible = false;
            this.imgBtnAssignSentStoreAssignment.Visible = false;
        }

        this.grdAssignedDistrictClinics.EditIndex = -1;

        this.grdAssignedDistrictClinics.DataSource = dt_filtered_clinics;
        this.grdAssignedDistrictClinics.DataBind();
    }

    private void bindAssignedSearchResults()
    {
        this.dtCorporateClinics = corporateClinics;
        DataRow[] dr_filtered_clinics = null;
        DataTable dt_filtered_clinics = new DataTable();
        dt_filtered_clinics = this.dtCorporateClinics.Clone();

        dr_filtered_clinics = this.dtCorporateClinics.Select(this.getFilterExpression(true, "AssignedStore"));

        if (dr_filtered_clinics.Count() > 0)
        {
            this.grdAssignedClinics.PageIndex = 0;
            dt_filtered_clinics = dr_filtered_clinics.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && (dt_filtered_clinics.Columns.Contains(GridSortExpression)))
                dt_filtered_clinics.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_filtered_clinics.DefaultView.Sort = defaultSortExp;

            //showing ReviseAssignment button id data present 
            this.btnReviseAssignment.Visible = true;
        }
        else
        {
            this.btnReviseAssignment.Visible = false;
        }
        this.grdAssignedClinics.EditIndex = -1;

        this.grdAssignedClinics.DataSource = dt_filtered_clinics;
        this.grdAssignedClinics.DataBind();
    }

    /// <summary>
    /// Binds scheduled clinic outreach statuses
    /// </summary>
    /// <param name="ddl_outreach_status"></param>
    private void bindOutreachStatuses(DropDownList ddl_outreach_status)
    {
        ddl_outreach_status.DataSource = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='AC' AND isActive = 'True'").CopyToDataTable();
        ddl_outreach_status.DataTextField = "outreachStatus";
        ddl_outreach_status.DataValueField = "pk";
        ddl_outreach_status.DataBind();
        ddl_outreach_status.Items.RemoveAt(0);
        ddl_outreach_status.Items.RemoveAt(1);
        ddl_outreach_status.Items.Insert(0, new ListItem("-- Select Outreach Status --", "0"));
    }

    /// <summary>
    /// Binds user stores to worksite clinics
    /// </summary>
    /// <param name="ddl_stores"></param>
    /// <param name="clinic_storeid"></param>
    /// <param name="clinic_district_id"></param>
    /// <param name="clinic_pk"></param>
    private void bindUserStores(DropDownList ddl_stores, string clinic_storeid, string clinic_district_id, string clinic_pk)
    {
        DataTable dt_stores = new DataTable();
        if (!string.IsNullOrEmpty(clinic_district_id))
            dt_stores = this.dbOperation.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, Convert.ToInt32(clinic_district_id), Convert.ToInt32(clinic_pk), "Corporate");
        else
            dt_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);

        if (dt_stores != null && dt_stores.Rows.Count > 0)
        {
            ddl_stores.DataSource = dt_stores;
            ddl_stores.DataTextField = dt_stores.Columns[1].ToString();
            ddl_stores.DataValueField = dt_stores.Columns[0].ToString();
            ddl_stores.DataBind();

            if (this.commonAppSession.SelectedStoreSession.SelectedCorporateClinicsTabId == "AssignedStoreClinics")
                ddl_stores.Items.Insert(dt_stores.Rows.Count, new ListItem("Cancel Clinic", "-1"));

            if (ddl_stores.Items.FindByValue(clinic_storeid) != null)
                ddl_stores.Items.FindByValue(clinic_storeid).Selected = true;
            else
            {
                if (this.commonAppSession.SelectedStoreSession.SelectedCorporateClinicsTabId != "AssignedStoreClinics")
                {
                    string closest_store = ((DataRow)dt_stores.Select("closestClinicRank = 1")[0])["storeId"].ToString();
                    if (ddl_stores.Items.FindByValue(closest_store) != null)
                        ddl_stores.Items.FindByValue(closest_store).Selected = true;
                }
            }
        }
    }

    /// <summary>
    /// Sets worksite clinics as selected
    /// </summary>
    private void setStoreChecked(string grd_source_key)
    {
        this.dtCorporateClinics = corporateClinics.Select("isStoreConfirmed = 0").CopyToDataTable();
        int clinic_pk = 0;
        GridView grd_source = grd_source_key == "AssignedDistrict" ? grdAssignedDistrictClinics : grdNcStoreAssignment;
        var is_all_districts_checked = grd_source_key == "AssignedDistrict" ? this.chkSelectAllAssignedDistrictClinicsInDistrict.Checked : this.chkSelectAllUnassignedClinicsInDistrict.Checked;
        foreach (GridViewRow row in grd_source.Rows)
        {
            Int32.TryParse(((Label)(row.FindControl("lblClinicPk"))).Text.Trim(), out clinic_pk);

            if (clinic_pk > 0)
            {
                DataRow row_clinic = this.dtCorporateClinics.Select("clinicPk = " + clinic_pk).ElementAt(0);
                CheckBox chk_box_selected_row = grd_source_key == "AssignedDistrict" ? (CheckBox)(row.FindControl("chkSelectAssignedDistrictClinic")) : (CheckBox)(row.FindControl("chkSelectUnassignedClinic"));
                row_clinic["isChecked"] = is_all_districts_checked ? 1 : (chk_box_selected_row.Checked ? 1 : 0);
            }
        }
    }

    /// <summary>
    /// Updates store assignment to the selected store
    /// </summary>
    /// <param name="is_edited"></param>
    /// <param name="grd_corporate_clinics"></param>
    /// <param name="is_approved_clinics"></param>
    private void updateStoreAssignment(bool is_edited, GridView grd_corporate_clinics, bool is_approved_clinics)
    {
        int clinic_pk = 0, updated_storeid = 0, new_lead_storeid = 0, old_lead_storeid = 0;
        this.clientId = 0;
        DropDownList ddl_selected_store = new DropDownList();
        DropDownList ddl_selected_lead_store = new DropDownList();

        Label lbl_current_lead_storeid = new Label();
        Label lbl_clinic_pk = new Label();
        Label lbl_client_id = new Label();

        CheckBox chk_lead_store = new CheckBox();

        if (is_edited)
        {
            ddl_selected_store = (DropDownList)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("ddlStoreAvailable"));

            if (is_approved_clinics)
            {
                chk_lead_store = (CheckBox)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("chkLeadStore"));
                ddl_selected_lead_store = (DropDownList)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("ddlLeadStores"));
                lbl_current_lead_storeid = (Label)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("lblLeadStoreId"));
            }

            lbl_clinic_pk = (Label)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("lblClinicPk"));
            lbl_client_id = (Label)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("lblClientId"));
        }
        else
        {
            foreach (GridViewRow row in grd_corporate_clinics.Rows)
            {
                if (row.RowState.ToString().Contains("Edit"))
                {
                    ddl_selected_store = (DropDownList)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("ddlStoreAvailable"));

                    if (is_approved_clinics)
                    {
                        chk_lead_store = (CheckBox)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("chkLeadStore"));
                        ddl_selected_lead_store = (DropDownList)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("ddlLeadStores"));
                        lbl_current_lead_storeid = (Label)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("lblLeadStoreId"));
                    }

                    lbl_clinic_pk = (Label)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("lblClinicPk"));
                    lbl_client_id = (Label)(grd_corporate_clinics.Rows[grd_corporate_clinics.EditIndex].FindControl("lblClientId"));
                }
            }
        }

        Int32.TryParse(lbl_clinic_pk.Text, out clinic_pk);
        Int32.TryParse(lbl_client_id.Text, out this.clientId);
        Int32.TryParse(ddl_selected_store.SelectedValue, out updated_storeid);
        if (is_approved_clinics)
        {
            Int32.TryParse(ddl_selected_lead_store.SelectedValue, out new_lead_storeid);
            Int32.TryParse(lbl_current_lead_storeid.Text, out old_lead_storeid);
        }

        if (this.dtCorporateClinics.Rows.Count > 0)
        {
            DataRow[] update_row = this.dtCorporateClinics.Select("clinicPk = '" + clinic_pk + "'");
            if (updated_storeid != 0 && Convert.ToInt32(update_row[0]["storeId"].ToString()) != updated_storeid)
            {
                if (Convert.ToInt32(update_row[0]["storeId"]) != updated_storeid)
                {
                    update_row[0]["storeId"] = updated_storeid;
                    update_row[0]["storeAddress"] = (updated_storeid != -1 ? ddl_selected_store.SelectedItem.Text.getStoreAddress() : "");
                    update_row[0]["isStoreUpdated"] = "1";
                    update_row[0]["isDistrictUpdated"] = "1";
                }
            }

            if (is_approved_clinics)
            {
                if ((old_lead_storeid != new_lead_storeid && chk_lead_store.Checked) || (old_lead_storeid > 0 && !chk_lead_store.Checked))
                {
                    update_row[0]["leadStoreId"] = chk_lead_store.Checked ? new_lead_storeid : 0;
                    update_row[0]["leadStoreAddress"] = chk_lead_store.Checked ? ("<b>LEAD STORE:</b><br /> " + ddl_selected_lead_store.SelectedItem.Text.getStoreAddress()) : "";
                    update_row[0]["isLeadStoreUpdated"] = "1";

                    //DataRow[] client_clinic_stores = this.dtCorporateClinics.Select("clientId = '" + this.clientId + "'");
                    //if (client_clinic_stores.Count() > 0)
                    //{
                    //    client_clinic_stores.ToList<DataRow>().ForEach(row =>
                    //    {
                    //        row["leadStoreId"] = chk_lead_store.Checked ? new_lead_storeid : 0;
                    //        row["leadStoreAddress"] = chk_lead_store.Checked ? ("<b>LEAD STORE:</b><br /> " + ddl_selected_lead_store.SelectedItem.Text.getStoreAddress()) : "";
                    //        row["isLeadStoreUpdated"] = "1";
                    //    });
                    //}
                }
            }
        }
    }

    /// <summary>
    /// Creates corporate unassigned clinics xml document for updating and sending store assignment
    /// </summary>
    /// <param name="update_type"></param>
    /// <returns></returns>
    private XmlDocument createUpdatedUnAssignedClinicsXml(string update_type)
    {
        XmlDocument national_accounts = new XmlDocument();
        XmlElement national_account_ele = national_accounts.CreateElement("nationalAccounts");
        XmlElement national_account = null;

        if (this.dtCorporateClinics.Rows.Count > 0)
        {
            DataRow[] dr_clinics;
            if (update_type == "update")
                dr_clinics = this.dtCorporateClinics.Select("(isStoreConfirmed = 0 AND ISNULL(isStoreUpdated, 0) = '1')");
            else
                dr_clinics = this.dtCorporateClinics.Select("isStoreConfirmed = 0 AND ISNULL(isChecked, 0) = '1'");

            if (dr_clinics.Length > 0)
            {
                foreach (DataRow row in dr_clinics)
                {
                    national_account = national_accounts.CreateElement("nationalAccount");

                    national_account.SetAttribute("clinicPk", row["clinicPk"].ToString());
                    national_account.SetAttribute("storeId", row["storeId"].ToString());
                    national_account.SetAttribute("isStoreUpdated", row["isStoreUpdated"].ToString());

                    national_account_ele.AppendChild(national_account);
                    national_accounts.AppendChild(national_account_ele);
                }
            }
        }

        return national_accounts;
    }

    /// <summary>
    /// Creates assigned clinics xml document for updating and sending store assignment
    /// </summary>
    /// <returns></returns>
    private XmlDocument createUpdatedAssignedClinicsXml()
    {
        XmlDocument assigned_clinics = new XmlDocument();
        XmlElement assigned_clinics_ele = assigned_clinics.CreateElement("assignedClinics");
        XmlElement assigned_clinic = null;

        //if (ViewState["corporateClinics"] != null)
        //{
        DataTable dt_national_accounts = corporateClinics;
        if (dt_national_accounts.Rows.Count > 0)
        {
            DataRow[] rows = dt_national_accounts.Select("isStoreConfirmed = 1 AND (ISNULL(isStoreUpdated, 0) = '1' OR ISNULL(isLeadStoreUpdated, 0) = '1')");

            if (rows.Length > 0)
            {
                foreach (DataRow row in rows)
                {
                    if ((!string.IsNullOrEmpty(row["isStoreUpdated"].ToString()) && Convert.ToInt32(row["isStoreUpdated"].ToString()) == 1) || (!string.IsNullOrEmpty(row["isLeadStoreUpdated"].ToString()) && Convert.ToInt32(row["isLeadStoreUpdated"].ToString()) == 1))
                    {
                        assigned_clinic = assigned_clinics.CreateElement("assignedClinic");

                        assigned_clinic.SetAttribute("clinicPk", row["clinicPk"].ToString());
                        assigned_clinic.SetAttribute("clinicBusinessName", row["businessName"].ToString());
                        assigned_clinic.SetAttribute("clinicStoreId", row["storeId"].ToString());
                        assigned_clinic.SetAttribute("leadStoreId", row["leadStoreId"].ToString());
                        assigned_clinic.SetAttribute("isLeadStoreUpdated", (!string.IsNullOrEmpty(row["isLeadStoreUpdated"].ToString()) && Convert.ToInt32(row["isLeadStoreUpdated"].ToString()) == 1) ? "1" : "0");

                        assigned_clinics_ele.AppendChild(assigned_clinic);
                        assigned_clinics.AppendChild(assigned_clinics_ele);
                    }
                }
            }
        }
        //}

        return assigned_clinics;
    }

    /// <summary>
    /// Saves clinic store assignment
    /// </summary>
    private void saveClinicStoreAssignment()
    {
        XmlDocument national_accounts = this.createUpdatedUnAssignedClinicsXml("update");

        if (national_accounts.SelectSingleNode("//nationalAccount") != null)
        {
            int return_value = 0;
            string date_time_stamp_message = "";
            return_value = this.dbOperation.updateConfirmNaStoreAssignment(national_accounts, "update", out date_time_stamp_message);

            if (return_value == 0)
            {
                this.commonAppSession.dtCorporateClinics = new DataTable();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('CorporateClinics'); alert('" + (string)GetGlobalResourceObject("errorMessages", "worksiteClinicStoreChanged") + "');", true);
            }
            else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
            {
                this.commonAppSession.dtCorporateClinics = new DataTable();
                string validation_message = (string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictMessageDetails");
                List<string> failed_locations = date_time_stamp_message.Split('|').ToList();
                string title_text = "Multiple Conflicts Alert";
                if (failed_locations.Count == 1)
                {
                    title_text = "Single Conflict Alert";
                    string failed_location = failed_locations.First();
                    validation_message = String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapSingleConflictDetails"), failed_location.Split('<')[1], Convert.ToDateTime(failed_location.Split('<')[2]).ToShortDateString(), failed_location.Split('<')[3], failed_location.Split('<')[4], failed_location.Split('<')[0]);
                }
                else
                {
                    failed_locations.ForEach(x =>
                    {
                        validation_message += String.Format((string)GetGlobalResourceObject("errorMessages", "datetimeOverlapMultipleConflictLocations"), x.Split('<')[1], Convert.ToDateTime(x.Split('<')[2]).ToShortDateString(), x.Split('<')[3], x.Split('<')[4], x.Split('<')[0]) + "\\n";
                    });
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDateTimeStampValidationMessage('" + validation_message + "','" + title_text + "');", true);
            }
        }
    }

    /// <summary>
    /// Sends email to Store manager and Pharmacy Manager, updates store assignment
    /// </summary>
    /// <param name="dt_corporate"></param>
    //    private void sendMailAndUpdateStoreAssignment(DataTable dt_corporate)
    //    {
    //        var lst_store_ids = (from r in ((DataTable)dt_corporate).AsEnumerable()
    //                             where r["isStoreConfirmed"].ToString().Equals("0")
    //                             select r["storeId"]).Distinct().ToList();

    //        foreach (int store_id in lst_store_ids)
    //        {
    //            DataRow[] store_rows = ((DataTable)ViewState["corporateClinics"]).Select("storeId = '" + store_id + "' And isStoreConfirmed = 0 AND ISNULL(isChecked, 0) = '1'");
    //            string store_manager_email, pharmacy_manager_email, district_manager_email, sales_contact_email, email_body, subject_line, email_cc;

    //            if (this.walgreensEmail != null)
    //            {
    //                DataTable dt_user_emails = dbOperation.getStoreUserEmails(this.commonAppSession.LoginUserInfoSession.UserID, store_id);
    //                store_manager_email = ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_user_emails.Rows[0]["storeManagerEmail"].ToString();
    //                pharmacy_manager_email = ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_user_emails.Rows[0]["pharmacyManagerEmail"].ToString();
    //                district_manager_email = ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_user_emails.Rows[0]["districtManagerEmail"].ToString();

    //                email_cc = (!string.IsNullOrEmpty(store_manager_email)) ? "," + store_manager_email : "";
    //                email_cc = (!string.IsNullOrEmpty(district_manager_email)) ? email_cc + "," + district_manager_email : email_cc;

    //                foreach (DataRow row in store_rows)
    //                {
    //                    //sales_contact_email = row["naSalesContactEmail"].ToString();

    //                    //email_body = "<table width='100%' style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #000000;' cellspacing='0' cellpadding='0'><tr><td style='border: 1px solid black;'><b> Business Account Name </b></td> <td style='border: 1px solid black;'><b> Full Address </b></td><td style='border: 1px solid black;'><b> Clinic Date </b></td><td style='border: 1px solid black;'><b> Clinic Time </b></td></tr>";
    //                    //email_body += "<tr><td style='border: 1px solid black;'> " + row["businessName"].ToString() + "</td> <td style='border: 1px solid black;'>" + row["accountAddress"].ToString() + " </td><td style='border: 1px solid black;'> " + row["naClinicDate"].ToString() + " </td><td style='border: 1px solid black;'> " + row["naClinicStartTime"].ToString() + " </td></tr>";
    //                    //email_body += "</tabel>";

    //                    email_body = "<p class='assignment' align='left'><b>Client: </b>" + row["businessName"].ToString() + "</p>";
    //                    email_body += "<p class='assignment' align='left'><b>Clinic Date: </b>" + row["naClinicDate"].ToString() + "</p>";
    //                    email_body += "<p class='assignment' align='left'><b>Plan ID: </b>" + ((row.Table.Columns["naClinicPlanId"] != null) ? row["naClinicPlanId"].ToString() : "") + "</p>";
    //                    email_body += "<p class='assignment' align='left'><b>Group ID: </b>" + ((row.Table.Columns["naClinicGroupId"] != null) ? row["naClinicGroupId"].ToString() : "") + "</p>";

    //                    email_body += @"</td></tr></table></td><td class='wrapper last fields' style='position: relative; background: #ebebeb;  padding: 10px 20px 0px 0px;' align='left' bgcolor='#ebebeb' valign='top'>
    //                                    <table class='six columns' style='border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 280px; margin: 0 auto; padding: 0;'>
    //                                    <tr style='vertical-align: top; text-align: left; padding: 0;' align='left'>
    //                                        <td class='last right-text-pad fields' style='padding: 0px 0px 10px;' align='left' valign='top'>";

    //                    email_body += "<p class='assignment' align='left'><b>Clinic Time: </b>" + row["naClinicStartTime"].ToString() + "</p>";
    //                    email_body += "<p class='assignment' align='left'><b>Clinic Location: </b>" + row["accountAddress"].ToString() + "</p>";

    //                    subject_line = String.Format((string)GetGlobalResourceObject("errorMessages", "nlaSubjectLine"), this.appSettings.removeLineBreaks(row["businessName"].ToString()));

    //                    if (!string.IsNullOrEmpty(pharmacy_manager_email))
    //                        this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb("Pharmacy Manager", ApplicationSettings.encryptedLinkWithThreeArgs(pharmacy_manager_email, "walgreensLandingPage.aspx", "walgreensHome.aspx"), store_id.ToString(), String.Format((string)GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), Server.MapPath("~/emailTemplates/nationalAssignmentsTemplate.htm"), subject_line, email_body, ConfigurationManager.AppSettings["emailSendTo"].ToString().Trim().Length != 0 ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : pharmacy_manager_email, email_cc.TrimStart(','), true);

    //                    //if (!string.IsNullOrEmpty(sales_contact_email))
    //                    //    this.walgreensEmail.sendNationalLargeAccountAssignmentEmailFromWeb("Pharmacy Manager", "", store_id.ToString(), String.Format((string)GetGlobalResourceObject("errorMessages", "nlaThankYouLine")), Server.MapPath("nationalAssignmentsSalesTemplate.htm"), subject_line, email_body, ConfigurationManager.AppSettings["emailSendTo"].ToString() != "" ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : sales_contact_email, "", true);
    //                }
    //            }
    //        }
    //    }    

    /// <summary>
    /// Get filtered query for Assigned and Unassigned clinics
    /// </summary>
    /// <param name="is_store_confirmed"></param>
    /// <returns></returns>
    private string getFilterExpression(bool is_store_confirmed, string assignment_type)
    {
        string str_filter;
        string clinic_search_key;
        string filter_query;
        if (assignment_type.ToLower() == "unassigned")
        {
            filter_query = "isStoreConfirmed = " + Convert.ToInt32(is_store_confirmed) + (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager" ? " AND isDistrictConfirmed = 0" : " AND isDistrictConfirmed = 1");
            if (this.ddlUnassignedClinicsFilters.SelectedItem != null && this.ddlUnassignedClinicsFilters.SelectedItem.Value.Trim().Length > 0)
            {
                if (this.txtUnassignedClinicsFilter.Text.Trim().Length > 0)
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlUnassignedClinicsFilters, this.txtUnassignedClinicsFilter);
                }
            }
            str_filter = this.txtUnassignedClinicsSearch.Text.Trim();
            clinic_search_key = this.txtUnassignedClinicsSearch.Text.Trim().Replace("'", "''");
        }
        else if (assignment_type.ToLower() == "assigneddistrict")
        {
            filter_query = "isDistrictConfirmed = 1 AND isStoreConfirmed = 0";
            if (this.ddlAssignedDistrictClinicsFilters.SelectedItem != null && this.ddlAssignedDistrictClinicsFilters.SelectedItem.Value.Trim().Length > 0)
            {
                if (this.txtAssignedDistrictClinicsFilter.Text.Trim().Length > 0)
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlAssignedDistrictClinicsFilters, this.txtAssignedDistrictClinicsFilter);
                }
            }
            str_filter = this.txtAssignedDistrictClinicsSearch.Text.Trim();
            clinic_search_key = this.txtAssignedDistrictClinicsSearch.Text.Trim().Replace("'", "''");
        }
        else
        {
            filter_query = "isDistrictConfirmed = 1 AND isStoreConfirmed = 1";
            if (this.ddlAssignedStoreClinicsFilters.SelectedItem != null && this.ddlAssignedStoreClinicsFilters.SelectedItem.Value.Trim().Length > 0)
            {
                if (this.txtAssignedStoreClinicsFilter.Text.Trim().Length > 0)
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlAssignedStoreClinicsFilters, this.txtAssignedStoreClinicsFilter);
                }
            }
            str_filter = this.txtAssignedClinicsSearch.Text.Trim();
            clinic_search_key = this.txtAssignedClinicsSearch.Text.Trim().Replace("'", "''");
        }

        clinic_search_key = clinic_search_key.Replace("[", "[[]");
        clinic_search_key = clinic_search_key.Replace("*", "[*]");
        str_filter = str_filter.Replace("'", "''");
        str_filter = str_filter.Replace("[", "[[]");
        str_filter = str_filter.Replace("*", "[*]");

        if (!string.IsNullOrEmpty(str_filter))
        {
            filter_query += " AND (businessName like '%" + str_filter + "%'" +
                                            " OR naClinicLocation like '%" + clinic_search_key + "%'" +
                                            " OR accountAddress like '%" + clinic_search_key + "%'" +
                                            " OR naClinicDate like '%" + clinic_search_key + "%'" +
                                            " OR storeAddress like '%" + clinic_search_key + "%'" +
                                            " OR CONVERT(storeId, System.String) like '%" + clinic_search_key + "%'" +
                                            ")";
        }

        return filter_query;
    }

    /// <summary>
    /// Prepares grid sort expression
    /// </summary>
    private string GridSortExpression
    {
        get
        {
            string sort_expression = string.Empty;
            string[] sort_by_type = new string[1];

            if (ViewState["sortOrder"] != null)
            {
                sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
                sort_expression = sort_by_type[0].Trim();
            }

            return sort_expression;
        }
    }

    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }

    #endregion

    #region ------------------- PRIVATE VARIABLES ------------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private EmailOperations emailOperations = null;
    private ApplicationSettings appSettings = null;
    private DataTable dtCorporateClinics = null;
    private int clientId;
    private DataTable corporateClinics
    {
        get
        {
            if (this.commonAppSession.dtCorporateClinics.Rows.Count == 0)
            {
                this.commonAppSession.dtCorporateClinics = this.dbOperation.getCorporateClinics(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
            }
            return this.commonAppSession.dtCorporateClinics;
        }
        set
        {
            this.commonAppSession.dtCorporateClinics = value;
        }
    }
    private string defaultSortExp = "[districtId],[storeId]";
    private Logger logger = LogManager.GetCurrentClassLogger();
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.emailOperations = new EmailOperations();
        this.appSettings = new ApplicationSettings();
        this.dtCorporateClinics = new DataTable();
    }
    #endregion
}