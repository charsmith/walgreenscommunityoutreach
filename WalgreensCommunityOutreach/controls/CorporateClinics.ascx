﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorporateClinics.ascx.cs" Inherits="CorporateClinics" %>
<input type="hidden" id="hfUnassignedClinicsflag" value="false" runat="server" />
<input type="hidden" id="hfAssignedDistrictClinicsflag" value="false" runat="server" />
<input type="hidden" id="hfAssignedStoreClinicsflag" value="false" runat="server" />
<%--<asp:HiddenField ID="hdnUACorpShowMoreThan10" runat="server" Value="false" />
<asp:HiddenField ID="hdnADCorpShowMoreThan10" runat="server" Value="false" />
<asp:HiddenField ID="hdnACorpShowMoreThan10" runat="server" Value="false" />--%>
<table width="97%" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <div id="tabsCorporateClinics" style="min-height: 200px; vertical-align: top;">
                <ul style="font-size: 70%">
                    <asp:Literal ID="lblTabLinks" runat="server" Text=""></asp:Literal>
                </ul>
                <div id="idUnassignedClinics">
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowUnassignedClinicsNoRecords" visible="false">
                        <tr>
                            <td style="text-align: center; vertical-align: top;"><span class="formFields"><asp:Literal ID="ltlUnassignedClinicsNoRecords" runat="server"></asp:Literal></span></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowUnassignedClinics" visible="false">
                        <tr>
                            <td colspan="2" style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">
                                <span class="formFields"><asp:Label ID="ltlUnassignedClinicsNote" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="right">
                                        <td style="float: left">
                                            <table>
                                                <tr>
                                                    <td>Filter:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlUnassignedClinicsFilters" CssClass="ddlFilter" runat="server" ></asp:DropDownList>
                                                    </td>
                                                    <td>ID #:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtUnassignedClinicsFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                        <asp:Button ID="btnUnassignedCoporateClinics" runat="server" Style="display: none;" OnCommand="btnCorporateClinics_Command" CommandArgument="UnAssigned" />
                                                    </td>
                                                    <td style="display:none;">
                                                        <asp:CheckBox ID="chkSelectAllUnassignedClinicsInDistrict" Text="Select All in District" runat="server" style="display:none;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:Panel runat="server" DefaultButton="hfImgBtnUnassignedClinicsSearch">
                                                <asp:TextBox ID="txtUnassignedClinicsSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" Height="18px"></asp:TextBox>
                                                <asp:ImageButton ID="hfImgBtnUnassignedClinicsSearch" runat="server" OnClick="imgBtnUnassignedClinicsSearch_Click" Style="display: none;" />
                                            </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="ImgBtnUnassignClinicsSearch" runat="server" AlternateText="Unassigned Clinics Search" ImageUrl="../images/btn_search.png" 
                                            OnClick="imgBtnUnassignedClinicsSearch_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" 
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgBtnUnassignedClinicsInstr" runat="server" AlternateText="Unassigned Corporate Worksite Clinics instructions" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="vertical-align: top; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 10px; text-align: left;" class="formFields2">
                                <asp:GridView ID="grdNcStoreAssignment" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10" GridLines="None" 
                                Width="100%" OnRowDataBound="grdNcStoreAssignment_RowDataBound" OnPageIndexChanging="grdNcStoreAssignment_PageIndexChanging" OnRowCancelingEdit="grdNcStoreAssignment_OnRowCancelingEdit" OnSorting="grdNcStoreAssignment_sorting" 
                                OnRowEditing="grdNcStoreAssignment_RowEditing" OnRowUpdating="grdNcStoreAssignment_RowUpdating" DataKeyNames="clinicPk,clientId,districtId" AlternatingRowStyle-BackColor="#E9E9E9">
                                    <FooterStyle CssClass="footerGrey" />
                                    <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                    <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="selectUnassignedClinics" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAllUnassignedClinics" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelectUnassignedClinic" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Id" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactName" runat="server" Text='<%# Bind("ContactName") %>' Visible="true"></asp:Label>
                                                <asp:Label ID="lblContactPhone" runat="server" Text='<%# Bind("ContactPhone") %>' Visible="true"></asp:Label>
                                                <%--<asp:Label ID="lblEmail" runat="server" Text='<%# Bind("naSalesContactEmail") %>' Visible="true"></asp:Label>--%>
                                                <%--<asp:Label ID="lblClinicDate" runat="server" Text='<%# Bind("naClinicDate") %>' Visible="true"></asp:Label>--%>
                                                <asp:Label ID="lblClinicStartTime" runat="server" Text='<%# Bind("naClinicStartTime") %>' Visible="true"></asp:Label>

                                                <%--<asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>--%>
                                                <asp:Label ID="lblClinicPk" runat="server" Text='<%# Bind("clinicPk") %>'></asp:Label>
                                                <asp:Label ID="lblClientId" runat="server" Text='<%# Bind("clientId") %>'></asp:Label>
                                                <asp:Label ID="lblDistrictId" runat="server" Text='<%# Bind("districtId") %>'></asp:Label>
                                                <asp:Label ID="lblClinicType" runat="server" Text='<%# Bind("clinicType") %>'  Visible="false"></asp:Label>
                                                <%--<asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Address" HeaderStyle-HorizontalAlign="Center" SortExpression="accountAddress" HeaderStyle-BorderColor="#336699" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicAddress" runat="server" Text='<%# Bind("accountAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Date" HeaderStyle-HorizontalAlign="Center" SortExpression="naClinicDate" HeaderStyle-BorderColor="#336699" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicDate" runat="server" Text='<%# Bind("naClinicDate") %>' Width="55px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Closest<br /> Store #" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>' class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store Address" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAssignedStore" runat="server" Text='<%# Bind("storeAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <table id="tblClinicStores" cellpadding="0" cellspacing="0" border="0">
                                                    <tr id="rowClinicDistricts" runat="server">
                                                        <td>
                                                            <b class="formFields">District:</b><br />
                                                            <asp:DropDownList ID="ddlDistrictsAvailable" runat="server" Width="250" CssClass="formFields" DataTextField="" DataValueField="" 
                                                            OnSelectedIndexChanged="ddlDistrictsAvailable_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="formFields">Store:</b><br />
                                                            <asp:DropDownList ID="ddlStoreAvailable" runat="server" Width="250" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList><br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" SortExpression="phone" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update"
                                                    runat="server" AlternateText="Save" /><br />
                                                <br />
                                                <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit"
                                                    runat="server" AlternateText="Edit" />
                                            </ItemTemplate>
                                           </asp:TemplateField>
                                       
                                    </Columns>
                                       
                                    <EmptyDataTemplate>
                                        <center>
                                    <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                </center>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="LightBlue" />
                                </asp:GridView>
                            </td>                        
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">Use the Search and Filter functions for more results.</td>
                            <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: right;"><img id="imgClrKeyUnassigned" runat="server" src="~/images/outreach_color_key_uploaded.gif" /></td>                   
                            
                           
                        </tr>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan="2" style="text-align: right; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 12px;">
                                <asp:ImageButton ID="btnApproveDistrictAssignment" ImageUrl="~/images/btn_assign_send_dm.png" runat="server" AlternateText="Assign &amp; Send Selected to District Manager" 
                                CommandArgument="AssignmentNeeded" OnCommand="btnApproveDistrictAssignment_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_assign_send_dm.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_assign_send_dm_lit.png');" CausesValidation="true" />&nbsp;&nbsp;
                                <asp:ImageButton ID="btnApproveAssignment" ImageUrl="~/images/btn_approve_send.png" runat="server" AlternateText="Approve &amp; Send Selected Store Assignments" 
                                CommandArgument="AssignmentNeeded" OnCommand="btnApproveAssignment_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_approve_send.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_approve_send_lit.png');" CausesValidation="true" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="idAssignedDistrictClinics">
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowAssignedDistrictClinicsNoRecords" visible="false">
                        <tr>
                            <td style="text-align: center; vertical-align: top;">
                                <span class="formFields"><asp:Literal ID="ltlAssignedDistrictClinicsNoRecords" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowAssignedDistrictClinics" visible="false">
                        <tr>
                            <td colspan="2" style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">
                                <span class="formFields"><asp:Label ID="ltlAssignedDistrictClinicsNote" runat="server" Style="display: none;"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="right">
                                         <td style="float: left">
                                            <table>
                                                <tr>
                                                    <td>Filter:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlAssignedDistrictClinicsFilters" runat="server" CssClass="ddlFilter"></asp:DropDownList>
                                                    </td>
                                                    <td>ID #:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtAssignedDistrictClinicsFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                        <asp:Button ID="btnAssignedToDistCoporateClinics" runat="server" OnCommand="btnCorporateClinics_Command" style="display:none;" CommandArgument="AssignedToDist" />                                                    </td>
                                                    <td style="display:none;">
                                                        <asp:CheckBox ID="chkSelectAllAssignedDistrictClinicsInDistrict" Text="Select All in District" runat="server" style="display:none" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:Panel runat="server" DefaultButton="hfImgBtnAssignedDistrictClinicsSearch">
                                                <asp:TextBox ID="txtAssignedDistrictClinicsSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" Height="18px"></asp:TextBox>
                                                <asp:ImageButton ID="hfImgBtnAssignedDistrictClinicsSearch" runat="server" OnClick="imgBtnAssignedDistrictClinicsSearch_Click" Style="display: none;" />
                                            </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgBtnAssignedDistrictClinicsSearch" runat="server" AlternateText="Assigned District Clinics Search" ImageUrl="../images/btn_search.png" 
                                            OnClick="imgBtnAssignedDistrictClinicsSearch_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" 
                                            onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgBtnAssignedDistrictClinicsInstr" runat="server" AlternateText="Corporate Worksite Clinics assigned to district" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="vertical-align: top; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 10px; text-align: left;" class="formFields2">
                                <asp:GridView ID="grdAssignedDistrictClinics" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10" GridLines="None" Width="100%"
                                    OnRowDataBound ="grdAssignedDistrictClinics_RowDataBound" OnPageIndexChanging="grdAssignedDistrictClinics_PageIndexChanging" OnRowCancelingEdit="grdAssignedDistrictClinics_OnRowCancelingEdit" OnSorting="grdAssignedDistrictClinics_sorting" 
                                    OnRowEditing="grdAssignedDistrictClinics_RowEditing" OnRowUpdating="grdAssignedDistrictClinics_RowUpdating" DataKeyNames="clinicPk,clientId,districtId" AlternatingRowStyle-BackColor="#E9E9E9">
                                    <FooterStyle CssClass="footerGrey" />
                                    <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                    <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="selectAssignedDistrictClinics" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAllAssignedDistrictClinics" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelectAssignedDistrictClinic" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Id" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactName" runat="server" Text='<%# Bind("ContactName") %>' Visible="true"></asp:Label>
                                                <asp:Label ID="lblContactPhone" runat="server" Text='<%# Bind("ContactPhone") %>' Visible="true"></asp:Label>
                                                <asp:Label ID="lblClinicStartTime" runat="server" Text='<%# Bind("naClinicStartTime") %>' Visible="true"></asp:Label>
                                                <asp:Label ID="lblClinicPk" runat="server" Text='<%# Bind("clinicPk") %>'></asp:Label>
                                                <asp:Label ID="lblClientId" runat="server" Text='<%# Bind("clientId") %>'></asp:Label>
                                                <asp:Label ID="lblDistrictId" runat="server" Text='<%# Bind("districtId") %>'></asp:Label>
                                                <asp:Label ID="lblClinicType" runat="server" Text='<%# Bind("clinicType") %>'  Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Address" HeaderStyle-HorizontalAlign="Center" SortExpression="accountAddress" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicAddress" runat="server" Text='<%# Bind("accountAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Date" HeaderStyle-HorizontalAlign="Center" SortExpression="naClinicDate" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicDate" runat="server" Text='<%# Bind("naClinicDate") %>' Width="55px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Closest<br /> Store #" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>' class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store Address" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAssignedStore" runat="server" Text='<%# Bind("storeAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <table id="tblClinicStores" cellpadding="0" cellspacing="0" border="0">
                                                    <tr id="rowClinicDistricts" runat="server">
                                                        <td>
                                                            <b class="formFields">District:</b><br />
                                                            <asp:DropDownList ID="ddlDistrictsAvailable" runat="server" Width="250" CssClass="formFields" DataTextField="" DataValueField="" OnSelectedIndexChanged="ddlDistrictsAvailable_SelectedIndexChanged" 
                                                            AutoPostBack="true"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b class="formFields">Store:</b><br />
                                                            <asp:DropDownList ID="ddlStoreAvailable" runat="server" Width="250" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList><br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" SortExpression="phone" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update" runat="server" AlternateText="Save" />
                                                <br />
                                                <br />
                                                <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel" runat="server" AlternateText="Cancel" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');" CausesValidation="true" 
                                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit" runat="server" AlternateText="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <center>
                                    <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                </center>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="LightBlue" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">
                                <asp:Label ID="lblFilterMessageassignedToDist" runat="server" Text="Use the Search and Filter functions for more results."></asp:Label>
                            </td>
							<td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: right;"><img id="imgClrKeyUnassignedDist" runat="server" src="~/images/outreach_color_key_uploaded.gif" /></td>                   
                        </tr>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan="2" style="text-align: right; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 12px;">
                                <asp:ImageButton ID="imgBtnAssignSendDistrictAssignment" ImageUrl="~/images/btn_assign_send_dm.png" runat="server" AlternateText="Approve &amp; Send Selected to District Manager" 
                                CommandArgument="AssignedDistrict" OnCommand="btnApproveDistrictAssignment_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_assign_send_dm.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_assign_send_dm_lit.png');" CausesValidation="true" />&nbsp;&nbsp;
                                <asp:ImageButton ID="imgBtnAssignSentStoreAssignment" ImageUrl="~/images/btn_approve_send.png" runat="server" AlternateText="Approve &amp; Send Selected Store Assignments" 
                                CommandArgument="AssignedDistrict" OnCommand="btnApproveAssignment_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_approve_send.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_approve_send_lit.png');" CausesValidation="true" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="idAssignedStoreClinics">
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowAssignedStoreClinicsNoRecords" visible="false">
                        <tr>
                            <td valign="top" align="center"><span class="formFields">
                                <asp:Literal ID="ltlAssignedStoreClinicsNoRecords" runat="server"></asp:Literal></span></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowAssignedStoreClinics" visible="false">
                        <tr>
                            <td colspan="2" style="text-align: left; vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; padding-bottom: 5px; text-align: left;">
                                <span class="formFields" id="ltlAssignedClinicsNote" runat="server" style="display: none">The clinics below have been assigned to a store and the assigned store 
                                has been notified by email. No further action is required unless it becomes necessary to change the assignment or cancel the clinic.</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right; padding-top: 12px; padding-left: 12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="right">
                                        <td style="float: left">
                                            <table>
                                                <tr>
                                                    <td>Filter:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlAssignedStoreClinicsFilters" runat="server"></asp:DropDownList>
                                                    </td>
                                                    <td>ID #:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtAssignedStoreClinicsFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                        <asp:Button ID="btnAssignedCoporateClinics" runat="server" OnCommand="btnCorporateClinics_Command" style="display:none;" CommandArgument="Assigned"  />
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkSelectAllAssignedStoreClinicsInDistrict" Text="Select All in District" runat="server" Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:Panel runat="server" DefaultButton="hfImgBtnAssignedClinicsSearch">
                                                <asp:TextBox ID="txtAssignedClinicsSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" Height="18px"></asp:TextBox>
                                                <asp:ImageButton ID="hfImgBtnAssignedClinicsSearch" runat="server" OnClick="imgBtnAssignedClinicsSearch_Click" Style="display: none;" />
                                            </asp:Panel>
                                        </td>
                                        <td >
                                            <asp:ImageButton ID="imgBtnAssignedClinicsSearch" runat="server" AlternateText="Assigned Clinics Search" ImageUrl="../images/btn_search.png" OnClick="imgBtnAssignedClinicsSearch_Click"
                                                onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" />
                                        </td>
                                        <td >
                                            <asp:ImageButton ID="imgBtnAssignedClinicsInstr" runat="server" AlternateText="Corporate Worksite Clinics assigned to store" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="vertical-align: top; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 5px; text-align: left;" class="formFields2">
                                <asp:GridView ID="grdAssignedClinics" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10" GridLines="None" Width="100%" 
                                    OnPageIndexChanging="grdAssignedClinics_PageIndexChanging" OnSorting="grdAssignedClinics_sorting" OnRowEditing="grdAssignedClinics_RowEditing" OnRowUpdating="grdAssignedClinics_RowUpdating"
                                    OnRowDataBound="grdAssignedClinics_OnRowDataBound" OnRowCancelingEdit="grdAssignedClinics_OnRowCancelingEdit" DataKeyNames="clinicPk, clientId" AlternatingRowStyle-BackColor="#E9E9E9">
                                    <FooterStyle CssClass="footerGrey" />
                                    <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                    <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="selectAssignedClinics" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAllAssignedClinics" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelectAssignedClinics" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="businessPk" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactName" runat="server" Text='<%# Bind("ContactName") %>' Visible="true"></asp:Label>
                                                <asp:Label ID="lblContactPhone" runat="server" Text='<%# Bind("ContactPhone") %>' Visible="true"></asp:Label>
                                                <%--<asp:Label ID="lblEmail" runat="server" Text='<%# Bind("naSalesContactEmail") %>' Visible="true"></asp:Label>--%>
                                                <asp:Label ID="lblClinicStartTime" runat="server" Text='<%# Bind("naClinicStartTime") %>' Visible="true"></asp:Label>
                                                <asp:Label ID="lblClinicPk" runat="server" Text='<%# Bind("clinicPk") %>'></asp:Label>
                                                <asp:Label ID="lblClientId" runat="server" Text='<%# Bind("clientId") %>'></asp:Label>
                                                <asp:Label ID="lblIsCurrent" runat="server" Text='<%# Bind("isCurrent") %>'></asp:Label>
                                                <asp:Label ID="lblClinicType" runat="server" Text='<%# Bind("clinicType") %>'  Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Location" HeaderStyle-HorizontalAlign="Center" SortExpression="naClinicLocation"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicLocation" runat="server" Text='<%# Bind("naClinicLocation") %>' Width="100px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Address" HeaderStyle-HorizontalAlign="Center" SortExpression="accountAddress"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicAddress" runat="server" Text='<%# Bind("accountAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Date" HeaderStyle-HorizontalAlign="Center" SortExpression="naClinicDate" HeaderStyle-Width="85px"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicDate" runat="server" Text='<%# Bind("naClinicDate") %>' Width="85px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store #" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-Width="65px"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>' Width="65px" class="wrapword"></asp:Label>
                                                <table id="tblLeadStoreId" runat="server" border="0" cellpadding="0" cellspacing="0" visible="false">
                                                    <tr>
                                                        <td><br />
                                                            <asp:Label ID="lblLeadStoreId" runat="server" Text='<%# Bind("leadStoreId") %>' ForeColor="#508fcf" Font-Bold="true" Width="65px" class="wrapword"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned Store" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlStoreAvailable" runat="server" Width="250" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList>
                                                <table id="tblLeadStoreAssignment" runat="server" border="0" cellpadding="0" cellspacing="0" visible="false">
                                                    <tr>
                                                        <td>
                                                            <br />
                                                            <asp:CheckBox ID="chkLeadStore" runat="server" CssClass="formFields" ForeColor="#508fcf" Font-Bold="true" Text="LEAD STORE:" /><br />
                                                            <asp:Label ID="lblLeadStoreAddress" runat="server" Width="65px" class="wrapword"></asp:Label><br />
                                                            <asp:DropDownList ID="ddlLeadStores" runat="server" Width="250" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAssignedStore" runat="server" Text='<%# Bind("storeAddress") %>'></asp:Label>
                                                <table id="tblLeadStoreAddress" runat="server" border="0" cellpadding="0" cellspacing="0" visible="false">
                                                    <tr>
                                                        <td style="color: #508fcf; padding-top: 5px;">
                                                            <asp:Label ID="lblLeadStoreAddress" runat="server" ForeColor="#508fcf" Text='<%# Bind("leadStoreAddress") %>'></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" SortExpression="phone" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update"
                                                    runat="server" AlternateText="Save" /><br />
                                                <br />
                                                <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit"
                                                    runat="server" AlternateText="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <center><b><asp:Label ID="lblNote" Text="No assigned worksite clinics found for given search criteria." CssClass="formFields" runat="server"></asp:Label></center>
                                        </b>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="LightBlue" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">Use the Search and Filter functions for more results.</td>
                            <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: right;"><img id="imgClrKeyAssigned" runat="server" src="~/images/outreach_color_key_uploaded.gif" /></td>                   
                        </tr>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan="2" style="text-align: right; padding-top: 5px; padding-left: 12px; padding-right: 12px; padding-bottom: 12px;">
                                <asp:ImageButton ID="btnReviseAssignment" ImageUrl="~/images/btn_send_revised_assignments.png" runat="server" AlternateText="Send Revised Assignment" OnClick="btnReviseAssignment_Click"
                                    onmouseout="javascript:MouseOutImage(this.id,'images/btn_send_revised_assignments.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_send_revised_assignments_lit.png');"
                                    CausesValidation="true" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="idCancelledClinics">
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowCancelledClinicsNoRecords" visible="false">
                        <tr>
                            <td valign="top" align="center"><span class="formFields">There are no Cancelled Clinics requiring your attention at this time.</span></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowCancelledClinics" visible="false">
                        <tr>
                            <td colspan="2" valign="top">
                                <span class="formFields">The clinics below have had a "Cancelled" status logged. Either click the "Send Clinic Cancelled Email to Scheduled Patients" button or change the status of 
                                the clinic by clicking the "Edit" button.</span></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;" align="center" valign="top" class="formFields2">
                                <asp:GridView ID="grdCancelledClinics" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10" GridLines="None" Width="100%" 
                                    OnPageIndexChanging="grdCancelledClinics_PageIndexChanging" OnRowCancelingEdit="grdCancelledClinics_OnRowCancelingEdit" OnSorting="grdCancelledClinics_sorting" 
                                    OnRowEditing="grdCancelledClinics_RowEditing" OnRowUpdating="grdCancelledClinics_RowUpdating" DataKeyNames="clinicPk" AlternatingRowStyle-BackColor="#E9E9E9">
                                    <FooterStyle CssClass="footerGrey" />
                                    <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" Height="25px" BorderColor="#336699" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" />
                                    <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="clinicPk" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicPk" runat="server" Text='<%# Bind("clinicPk") %>'></asp:Label>
                                                <asp:Label ID="lblOutreachBusinessPk" runat="server" Text='<%# Bind("outreachBusinessPk") %>'></asp:Label>
                                                <asp:Label ID="lblDesignPk" runat="server" Text='<%# Bind("designPk") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Width="200px"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-CssClass="wrapword">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>' Width="200px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Location" HeaderStyle-HorizontalAlign="Center" SortExpression="naClinicLocation" HeaderStyle-Width="150px"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-CssClass="wrapword">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicLocation" runat="server" Text='<%# Bind("naClinicLocation") %>' Width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Clinic Address" HeaderStyle-HorizontalAlign="Center" SortExpression="accountAddress" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicAddress" runat="server" Text='<%# Bind("accountAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Date" HeaderStyle-HorizontalAlign="Center" SortExpression="naClinicDate" HeaderStyle-Width="85px"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-CssClass="wrapword">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNaClinicDate" runat="server" Text='<%# Bind("naClinicDate") %>' Width="85px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store #" HeaderStyle-HorizontalAlign="Center" SortExpression="clinicStoreId" HeaderStyle-Width="65px"
                                            HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-CssClass="wrapword">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClinicStoreId" runat="server" Text='<%# Bind("clinicStoreId") %>' Width="65px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlOutreachStatuses" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField=""></asp:DropDownList>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblOutreachStatus" runat="server" Text="Cancelled"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update"
                                                    runat="server" AlternateText="Save" />
                                                <br />
                                                <br />
                                                <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel"
                                                    runat="server" AlternateText="Cancel" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');"
                                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit"
                                                    runat="server" AlternateText="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <center>
                                    <b>
                                    <asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label>
                                </center>
                                        </b>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="LightBlue" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" valign="top"><span style="padding-top: 0px">
                                <asp:ImageButton ID="imgBtnCancelClinic"
                                    ImageUrl="~/images/btn_send_cancel_email.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_send_cancel_email.png');"
                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_send_cancel_email_lit.png');"
                                    runat="server" AlternateText="Send Clinic Cancelled Email to Scheduled Patients" OnClick="btnCancelScheduledAppts_Click" />
                            </span></td>
                        </tr>
                    </table>
                </div>
                <%--<div id="idResolveDupBusiness">
                        <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowResolveDupBusiness" visible="false" >
                            <tr><td align="center" valign="top" class="formFields2">
                            <asp:GridView ID="grdDuplicateBusiness" runat="server"  AutoGenerateColumns="False" CellPadding="1" GridLines="None"  Width="100%" CellSpacing="0" AllowPaging="true" AllowSorting="true" 
                            PageSize="10" OnSorting="grdDuplicateBusiness_sorting" OnPageIndexChanging ="grdDuplicateBusiness_PageIndexChanging" OnRowDataBound="grdDuplicateBusiness_RowDataBound" 
                            DataKeyNames="businessAddress,phone,duplicateType">
                                    <FooterStyle CssClass="footerGrey" />
                                    <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true"  />
                                    <RowStyle BorderWidth="1px" BorderColor= "#CCCCCC" />                                               
                                    <Columns>
                                        <asp:TemplateField HeaderText="businessPk" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Store Id" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Height="25px" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Name" SortExpression="businessName" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                            <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>' width="100px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                    
                                        <asp:TemplateField HeaderText="Address" SortExpression="businessAddress" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("businessAddress") %>' width="150px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                    
                                        <asp:TemplateField HeaderText="Phone" SortExpression="phone" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                            <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("phone") %>' width="85px" class="wrapword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duplication" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                            <asp:Label ID="lblDuplication" runat="server" Text='<%# Bind("duplicateType") %>' Width="100" class="wrapword" CssClass="wrapword" ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                 
                                            <asp:TemplateField HeaderText="Contacts" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                            <asp:Label ID="lblContacts" runat="server" Text='<%# Bind("contacts") %>' Width="50" class="wrapword" CssClass="wrapword" ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                   
                                        <asp:TemplateField HeaderText="Resolution" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblKeepBusiness" runat="server" Text='<%# Bind("keepBusiness") %>' Visible="false"></asp:Label>
                                                <asp:RadioButtonList ID="rblResolution" runat="server" CssClass="formFields" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2">
                                                <asp:ListItem Value="1" Text="Keep"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Remove"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                    
                                    </Columns>
                                    <EmptyDataTemplate>
						            <center>
						                <b>
						                <asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields"  runat="server"></asp:Label>
						                </b>
						            </center>
						            </EmptyDataTemplate>
						            <SelectedRowStyle BackColor="LightBlue" />
						        </asp:GridView></td></tr>
                                <tr>
						        <td align="right" valign="top" ><span style="padding-top:0px">
						            <asp:ImageButton ID="btnResolveDuplicates" ImageUrl="~/images/btn_submit_event.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_event.png');" 
                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_event_lit.png');" runat="server" onclick="btnResolveDuplicates_Click" 
                                    AlternateText="Process duplicate business" />                      
						        </span></td>
							    </tr>
                        </table>
                    </div>--%>
            </div>

        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>
