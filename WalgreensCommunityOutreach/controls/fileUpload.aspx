﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fileUpload.aspx.cs" Inherits="controls_fileUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
  <link href="../css/wagsSched.css" rel="stylesheet" type="text/css" />
  <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>  
  <script type="text/javascript">
      function displayClientLogo() {
          parent.displayClientLogo();
      }

      function validateUploadedFile(obj) {
          var value = $("#uploadLogoFile").val();
          var file_ext = value.substring(value.lastIndexOf('.') + 1, value.length);
          if (file_ext.toUpperCase() != "JPG" && file_ext.toUpperCase() != "PNG" && file_ext.toUpperCase() != "GIF" && file_ext.toUpperCase() != "JPEG") {
              alert("Only image files with a '.png', '.gif', '.jpg' or '.jpeg' file extension are allowed.");
              return;
          }
          else {
              obj.form.submit();
          }
      }
  </script>
</head>
<body>
    <form id="form1" runat="server" >
    <div>
        <span style="color:White;">
        <asp:FileUpload ID="uploadLogoFile" runat="server" onchange="javascript:validateUploadedFile(this);" />
        </span>
    </div>
    </form>
</body>
</html>
