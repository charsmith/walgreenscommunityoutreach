﻿<%@ WebHandler Language="C#" Class="ResourceFileHandler" %>

using System;
using System.Web;
using System.IO;

public class ResourceFileHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        //required fields
        string file_name = context.Request.QueryString["path"];
        string rootPath = string.Empty;
        
        if (context.Request.QueryString["FileType"] != null)
        {
            rootPath = (TdWalgreens.ApplicationSettings.GetContractVoucherVARPdfFilesPath.Substring(TdWalgreens.ApplicationSettings.GetContractVoucherVARPdfFilesPath.Length - 1)) == "\\" ? TdWalgreens.ApplicationSettings.GetContractVoucherVARPdfFilesPath : TdWalgreens.ApplicationSettings.GetContractVoucherVARPdfFilesPath + "\\";
        }
        else
        {
            rootPath = (TdWalgreens.ApplicationSettings.resourcesPath.Substring(TdWalgreens.ApplicationSettings.resourcesPath.Length - 1)) == "\\" ? TdWalgreens.ApplicationSettings.resourcesPath : TdWalgreens.ApplicationSettings.resourcesPath + "\\";
        }
        //setting based on file type
        switch (file_name.Substring(file_name.LastIndexOf('.') + 1))
        {
            case "pdf":
                context.Response.ClearHeaders();
                context.Response.Cache.SetNoServerCaching();
                context.Response.Cache.SetNoStore();
                context.Response.ContentType = "application/pdf";
                context.Response.AddHeader("content-disposition", "inline; filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "docx":
                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "doc":
                context.Response.ContentType = "application/msword";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "pptx":
                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "ppt":
                context.Response.ContentType = " application/vnd.ms-powerpoint";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "xlsx":
                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "xls":
                context.Response.ContentType = "application/vnd.ms-excel";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;            
			case "wmv":
                context.Response.ContentType = "video/x-ms-wmv";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "mp4":
                context.Response.ContentType = "video/mp4";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "ogg":
                context.Response.ContentType = "video/ogg";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
            case "mov":
                context.Response.ContentType = "video/quicktime";
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (file_name.Split('/').Length > 1 ? file_name.Split('/')[(file_name.Split('/').Length) - 1] : file_name));
                break;
        }

        if (File.Exists(rootPath + file_name))
        {
            context.Response.WriteFile(rootPath + file_name);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}