﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DirectB2BCampaigns.ascx.cs" Inherits="DirectB2BCampaigns" %>
<%@ Register Src="PickerAndCalendar.ascx" TagName="PickerAndCalendar" TagPrefix="uc1" %>
<script type="text/javascript">
   
</script>
<input type="hidden" id="_UnAssignB2Bflag" value="false" runat="server" />
<input type="hidden" id="_AssignDistrictB2Bflag" value="false" runat="server" />
<input type="hidden" id="_AssignedB2Bflag" value="false" runat="server" />
<%--<asp:HiddenField ID="hdnUADB2BShowMoreThan10" runat="server" Value="false"/>
<asp:HiddenField ID="hdnADDB2BShowMoreThan10" runat="server" Value="false"/>
<asp:HiddenField ID="hdnADB2BShowMoreThan10" runat="server" Value="false"/>--%>

    <table width="97%" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div id="tabsDirectB2BMailBusiness" style="min-height: 200px; vertical-align: top;">
                    <ul style="font-size: 70%">
                        <asp:Literal ID="lblDirectB2BMailBusinessTabs" runat="server" Text=""></asp:Literal>
                    </ul>
                    <div id="idUnassignedDirectB2BMail">
                        <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowUnassignedDirectB2BMailNoRecords">
                          <tr>
                            <td style="text-align: center; vertical-align: top;">
                                <span class="formFields"><asp:Literal ID="ltlUnassignedDirectB2BMailNoRecords" runat="server"></asp:Literal></span>
                            </td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowUnassignedDirectB2BMailBusiness">
                          <tr>
                            <td style="vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; text-align:left;" >
                              <span class="formFields"><asp:Label ID="ltlUnassignedDirectB2BMailBusinessNote" runat="server"></asp:Label></span>
                            </td>
                          </tr>
                          <tr>
                            <td style="text-align: right; padding-top:12px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="right">
                                      <td style="padding-bottom: 5px; float: left"> 
                                        <table>
                                          <tr>
                                            <td>Filter:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlUnassignedDirectB2BMailBusinessFilters" runat="server" CssClass="ddlFilter"></asp:DropDownList>
                                            </td>
                                            <td>ID #:</td>
                                            <td>
                                                <asp:TextBox ID="txtUnassignedDirectB2BMailBusinessFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                <asp:Button ID="btnUnassignedDirectB2BMailBusiness" runat="server" style="display:none;" OnCommand="btnDirectB2BMailBusiness_Command" CommandArgument="Unassigned" />
                                            </td>
                                            <td>
                                              <div>
                                                <asp:CheckBox ID="chkSelectAllUnassignedDirectB2BMailBusinessInDistrict" Text="Select All in District" runat="server"  />
                                              </div>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                      <td>
                                        <asp:Panel runat="server" DefaultButton="hfImgBtnUnassignedDirectB2BMailBusinessSearch">
                                            <asp:TextBox ID="txtUnassignedDirectB2BMailBusinessSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" Height="18px"></asp:TextBox>
                                            <asp:ImageButton ID="hfImgBtnUnassignedDirectB2BMailBusinessSearch" runat="server" CommandArgument="UnassignedDirectB2BMail" OnCommand="btnSearchRecords_Click" Style="display:none;" />
                                        </asp:Panel>
                                      </td>
                                      <td>
                                        <asp:ImageButton ID="ImgBtnUnassignedDirectB2BMailBusinessSearch" runat="server" AlternateText="Unassigned Direct B2B Mail Businesses" ImageUrl="../images/btn_search.png" 
                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" 
                                        CommandArgument="UnassignedDirectB2BMail" OnCommand="btnSearchRecords_Click" />
                                      </td>
                                      <td><asp:ImageButton ID="imgBtnUnassignedDirectB2BMailInstr" runat="server" AlternateText="Unassigned Direct B2B Mail Businesses" /></td>
                                    </tr>
                                </table>
                            </td>
                          </tr>
                          <tr>
                            <td style="vertical-align: top; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 10px; text-align:left;" class="formFields2" >
                            <asp:GridView ID="grdUnassignedDirectB2BMailBusiness" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10" 
                            GridLines="None" Width="100%" OnPageIndexChanging="grdUnassignedDirectB2BMailBusiness_PageIndexChanging" OnRowCancelingEdit="grdUnassignedDirectB2BMailBusiness_OnRowCancelingEdit" 
                            OnRowDataBound="grdUnassignedDirectB2BMailBusiness_RowDataBind" OnSorting="grdUnassignedDirectB2BMailBusiness_sorting" OnRowEditing="grdUnassignedDirectB2BMailBusiness_RowEditing" 
                            OnRowUpdating="grdUnassignedDirectB2BMailBusiness_RowUpdating" AlternatingRowStyle-BackColor="#E9E9E9" >
                                <FooterStyle CssClass="footerGrey" />
                                <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />
                                <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                <Columns>
                                <asp:templatefield headertext="selectUnassignedDirectB2BMailBusiness" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
				                    <headertemplate>
					                    <asp:checkbox id="chkAllUnassignedDirectB2BMailBusiness" runat="server" />
				                    </headertemplate>
				                    <itemtemplate>
					                    <asp:checkbox id="chkUnassignedDirectB2BMailBusiness" runat="server" />
				                    </itemtemplate>
			                    </asp:templatefield>
                               <asp:TemplateField HeaderText="StoreId" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                        <asp:Label ID="lblOutreachBusinessPk" runat="server" Text='<%# Bind("outreachBusinessPk") %>'></asp:Label>
                                        <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>
                                        <asp:Label ID="lblLastContactDate" runat="server" Text=''></asp:Label>
                                        <asp:Label ID="lblOutreachStatusId" runat="server" Text='<%# Bind("outreachStatusId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Business Name & Number" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true"
                                HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label><br />
                                        <asp:Label ID="lblBusinessPhone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Business Address" HeaderStyle-HorizontalAlign="Center" SortExpression="businessAddress" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusinessAddress" runat="server" Text='<%# Bind("businessAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emp" HeaderStyle-HorizontalAlign="Center" SortExpression="actualLocationEmploymentSize" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                HeaderStyle-BorderWidth="1px">
                                  <ItemTemplate>
                                  <asp:Label ID="lblActualLocationEmploymentSize" runat="server" Text='<%# Bind("actualLocationEmploymentSize") %>' Width="50px" class="wrapword" style="text-align:center"></asp:Label>
                                  </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="District" HeaderStyle-HorizontalAlign="Center" SortExpression="districtId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblDistrictId" runat="server" Text='<%# Bind("districtId") %>' Width="50px" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Closest Store" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblClosestStoreId" runat="server" Text='<%# Bind("storeId") %>' Width="50px" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <table id="tblStores" cellpadding="0" cellspacing="0" border="0">
                                          <tr id="rowDistricts" runat="server">
                                            <td>
                                              <b class="formFields" >District:</b><br />
                                              <asp:DropDownList ID="ddlDistrictsAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField="" 
                                              OnSelectedIndexChanged="ddlDistrictsAvailable_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b class="formFields" >Store:</b><br />
                                              <asp:DropDownList ID="ddlStoreAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField="" ></asp:DropDownList><br />
                                            </td>
                                          </tr>                                        
                                        </table>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Last Contact" HeaderStyle-HorizontalAlign="Center" SortExpression="LastContact" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                 HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastContact" runat="server" Text='<%# Eval("lastContactDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                          <uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" /><asp:TextBox ID="txtCalenderFrom" runat="server" Visible="False" CssClass="contractBodyText" Width="100px" 
                                          meta:resourcekey="txtCalenderFromResource1"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Center" SortExpression="Status" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                 HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                     <asp:Label ID="lblOutreachStatus" runat="server" Text='<%# Bind("outreachStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlOutreachStatus" runat="server" Width="100" CssClass="formFields" DataTextField="" DataValueField="" ></asp:DropDownList><br />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <EditItemTemplate>
                                    <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');" 
                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update" runat="server" AlternateText="Save" />
                                    <br /><br />
                                    <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');" 
                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel" runat="server" AlternateText="Cancel" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');" 
                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit" runat="server" AlternateText="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                <center>
                                    <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                </center>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="LightBlue" />
                            </asp:GridView>
                            </td>
                          </tr>
                          <tr>
                            <td style="vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; text-align:left;" >Use the Search and Filter functions for more results.</td>
                          </tr>
                          <tr>
                            <td style="text-align: right; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 12px;">
                                <asp:ImageButton ID="btnApproveDistrictAssignment" ImageUrl="~/images/btn_assign_send_dm.png" runat="server" AlternateText="Assign &amp; Send Selected to District Manager" 
                                CommandArgument="AssignmentNeeded" OnCommand="btnApproveDistrictAssignment_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_assign_send_dm.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_assign_send_dm_lit.png');" CausesValidation="true" />&nbsp;&nbsp;
                                <asp:ImageButton ID="btnApproveAssignment" ImageUrl="~/images/btn_approve_send.png" runat="server" AlternateText="Approve &amp; Send Selected Store Assignments" 
                                CommandArgument="AssignmentNeeded" OnCommand="btnApproveAssignment_Click" onmouseout="javascript:MouseOutImage(this.id,'images/btn_approve_send.png');" 
                                onmouseover="javascript:MouseOverImage(this.id,'images/btn_approve_send_lit.png');" CausesValidation="true" />
                            </td>
                          </tr>
                      </table>
                    </div>
                    <div id="idAssignedToDistrictDirectB2BMail">
                        <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowAssignedDistrictDirectB2BMailNoRecords">
                            <tr>
                            <td valign="top" align="center" ><span class="formFields"><asp:Literal ID="ltlAssignedDistrictDirectB2BMailNoRecords" runat="server"></asp:Literal></span></td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowAssignedDistrictDirectB2BMailBusiness">
                            <tr>
                                <td style="text-align: left; vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" >
                                  <span class="formFields">
                                    <asp:Label ID="ltlAssignedDistrictDirectB2BMailNote" runat="server" Style="display: none;"></asp:Label>
                                  </span></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; padding-top:12px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr align="right">
                                      <td style="padding-bottom: 5px; float: left"> 
                                        <table>
                                            <tr>
                                                <td>Filter:</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAssignedDistrictDirectB2BMailFilters" runat="server" CssClass="ddlFilter"></asp:DropDownList>
                                                </td>
                                                <td>ID #:</td>
                                                <td>
                                                    <asp:TextBox ID="txtAssignedDistrictDirectB2BMailFilter" runat="server" onpaste="return false"></asp:TextBox>
                                                    <asp:Button ID="btnAssignedToDistrictDirectB2BMail" runat="server" style="display:none;" OnCommand="btnDirectB2BMailBusiness_Command" CommandArgument="AssignedToDist" />
                                                </td>
                                                <td>
                                                    <div>
                                                    <asp:CheckBox ID="chkSelectAllAssignedDistrictDirectB2BMailInDistrict" Text="Select All in District" runat="server" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                      </td>
                                      <td><asp:Panel ID="Panel1" runat="server" DefaultButton="hfImgBtnAssignedDistrictDirectB2BMailSearch">
                                            <asp:TextBox ID="txtAssignedDistrictDirectB2BMailSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" Height="18px"></asp:TextBox>
                                            <asp:ImageButton ID="hfImgBtnAssignedDistrictDirectB2BMailSearch" runat="server" CommandArgument="AssignedDistrictDirectB2BMail" OnCommand="btnSearchRecords_Click" Style="display:none;" />
                                        </asp:Panel>
                                      </td>
                                      <td>
                                        <asp:ImageButton ID="ImgBtnAssignedDistrictDirectB2BMailSearch" runat="server" AlternateText="Assigned District Direct B2B Mail Business Search" ImageUrl="../images/btn_search.png" 
                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" 
                                        CommandArgument="AssignedDistrictDirectB2BMail" OnCommand="btnSearchRecords_Click" />
                                      </td>
                                      <td><asp:ImageButton ID="imgBtnAssignedDistrictDirectB2BMailInstr" runat="server" AlternateText="Direct B2B Mail assigned to district instructions" /></td>
                                    </tr>
                                </table>
                                </td>                                         
                            </tr>
                            <tr>
                              <td style="vertical-align: top; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2">
                              <asp:GridView ID="grdAssignedDistrictDirectB2BMail" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10"  GridLines="None" Width="100%" 
                              OnPageIndexChanging="grdAssignedDistrictDirectB2BMail_PageIndexChanging" OnRowCancelingEdit="grdAssignedDistrictDirectB2BMail_RowCancelingEdit" OnSorting="grdAssignedDistrictDirectB2BMail_sorting" 
                              OnRowEditing="grdAssignedDistrictDirectB2BMail_RowEditing" OnRowUpdating="grdAssignedDistrictDirectB2BMail_RowUpdating" AlternatingRowStyle-BackColor="#E9E9E9" OnRowDataBound="grdAssignedDistrictDirectB2BMail_RowDataBound" >
                                <FooterStyle CssClass="footerGrey" />                                
                                <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />                                
                                <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                <Columns>
                                <asp:templatefield headertext="selectAssignedDistrictDirectB2BMail" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
				                    <headertemplate>
					                    <asp:checkbox id="chkSelectAllDirectB2BBusiness" runat="server" />
				                    </headertemplate>
				                    <itemtemplate>
					                    <asp:checkbox id="chkSelectDirectB2BBusiness" runat="server" />
				                    </itemtemplate>
			                    </asp:templatefield>
                               <asp:TemplateField HeaderText="StoreId" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                        <asp:Label ID="lblOutreachBusinessPk" runat="server" Text='<%# Bind("outreachBusinessPk") %>'></asp:Label>
                                        <asp:Label ID="lblStoreId" runat="server" Text='<%# Bind("storeId") %>'></asp:Label>
                                        <asp:Label ID="lblLastContactDate" runat="server" Text=''></asp:Label>
                                        <asp:Label ID="lblOutreachStatusId" runat="server" Text='<%# Bind("outreachStatusId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Business Name & Number" HeaderStyle-HorizontalAlign="Center" SortExpression="ContactName" HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true"
                                                HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label><br />
                                    <asp:Label ID="lblBusinessPhone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Business Address" HeaderStyle-HorizontalAlign="Center" SortExpression="businessAddress" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblBusinessAddress" runat="server" Text='<%# Bind("businessAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emp" HeaderStyle-HorizontalAlign="Center" SortExpression="actualLocationEmploymentSize" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblActualLocationEmploymentSize" runat="server" Text='<%# Bind("actualLocationEmploymentSize") %>' Width="50px" class="wrapword" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="District" HeaderStyle-HorizontalAlign="Center" SortExpression="districtId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblDistrictId" runat="server" Text='<%# Bind("districtId") %>' Width="50px" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Closest Store" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblClosestStoreId" runat="server" Text='<%# Bind("storeId") %>' Width="40px" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <table id="tblStores" cellpadding="0" cellspacing="0" border="0">
                                          <tr id="rowDistricts" runat="server">
                                            <td>
                                              <b class="formFields" >District:</b><br />
                                              <asp:DropDownList ID="ddlDistrictsAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField="" OnSelectedIndexChanged="ddlDistrictsAvailable_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b class="formFields" >Store:</b><br />
                                              <asp:DropDownList ID="ddlStoreAvailable" runat="server" Width="150" CssClass="formFields" DataTextField="" DataValueField="" ></asp:DropDownList><br />
                                            </td>
                                          </tr>                                        
                                        </table>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Last Contact" HeaderStyle-HorizontalAlign="Center" SortExpression="LastContact" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblLastContact" runat="server" Text='<%# Eval("lastContactDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                          <uc1:PickerAndCalendar ID="PickerAndCalendarFrom" runat="server" /><asp:TextBox ID="txtCalenderFrom" runat="server" Visible="False" CssClass="contractBodyText" Width="100px" 
                                          meta:resourcekey="txtCalenderFromResource1"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Center" SortExpression="Status" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                 HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                     <asp:Label ID="lblOutreachStatus" runat="server" Text='<%# Bind("outreachStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlOutreachStatus" runat="server" Width="100" CssClass="formFields" DataTextField="" DataValueField="" ></asp:DropDownList><br />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText=""  HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <EditItemTemplate>
                                    <asp:ImageButton ID="imgBtnDone" ImageUrl="~/images/btn_save_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_save_assignment.png');"
                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_save_assignment_lit.png');" CommandName="Update" runat="server" AlternateText="Save" /><br /><br />
                                     <asp:ImageButton ID="imgBtnCancel" ImageUrl="~/images/btn_cancel_assignment.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_assignment.png');" 
                                     CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_assignment_lit.png');" CommandName="Cancel" runat="server" AlternateText="Cancel" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnEdit" ImageUrl="~/images/btn_edit.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_edit.png');" 
                                    CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_edit_lit.png');" CommandName="Edit" runat="server" AlternateText="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                <center>
                                    <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                </center>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="LightBlue" />
                            </asp:GridView>
                              </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; text-align:left;" >
                                    <asp:Label ID="lblFilterMessageassignedToDist" runat="server" Text="Use the Search and Filter functions for more results."></asp:Label>
                                </td>
                            </tr>
                            <tr>
                              <td style="text-align: right; padding-top:5px; padding-left:22px; padding-right: 22px; padding-bottom: 22px;" >
                               <asp:ImageButton ID="btnDistrictAssigned" ImageUrl="~/images/btn_assign_send_dm.png" runat="server" AlternateText="Assign &amp; Send Selected to District Manager" CommandArgument="AssignedDistrict" OnCommand="btnApproveDistrictAssignment_Click"
                                    onmouseout="javascript:MouseOutImage(this.id,'images/btn_assign_send_dm.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_assign_send_dm_lit.png');" CausesValidation="true" />&nbsp;&nbsp;
                                <asp:ImageButton ID="btnDistrictApproveAssignment" ImageUrl="~/images/btn_approve_send.png" runat="server" AlternateText="Approve &amp; Send Selected Store Assignments"  CommandArgument="AssignedDistrict" OnCommand="btnApproveAssignment_Click"
                                    onmouseout="javascript:MouseOutImage(this.id,'images/btn_approve_send.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_approve_send_lit.png');" CausesValidation="true" />                                                                                                                                                                                                                           
                              </td>
                            </tr>
                        </table>
                    </div>

                    <div id="idAssignedToStoresDirectB2BMail">
                        <table width="100%" border="0" cellspacing="22" cellpadding="0" runat="server" id="rowAssignedStoreDirectB2BMailNoRecords">
                          <tr>
                            <td valign="top" align="center" ><span class="formFields"><asp:Literal ID="ltlAssignedStoreDirectB2BMailNoRecords" runat="server"></asp:Literal></span></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" runat="server" id="rowAssignedStoreDirectB2BMailBusiness">
                          <tr>
                            <td style="text-align: left; vertical-align: top; padding-top:12px; padding-left:12px; padding-right: 12px; padding-bottom: 5px; text-align:left;" >
                                  <span class="formFields b2bInfoLabel">
                                    <asp:Label ID="ltlAssignedB2BNote" runat="server" Style="display: none;"></asp:Label>
                                  </span>
                            </td>
                          </tr>
                          <tr>
                            <td style="text-align: right; padding-top:12px; padding-left:12px; padding-right: 12px; padding-bottom: 5px;" class="formFields2" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr align="right">
                                    <td style="padding-bottom: 5px; float: left"> 
                                      <table>
                                        <tr>
                                            <td>Filter:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlAssignedStoreDirectB2BMailFilters" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>ID #:</td>
                                            <td>
                                                <asp:TextBox ID="txtAssignedStoreDirectB2BMailFilters" runat="server" onpaste="return false"></asp:TextBox>
                                                <asp:Button ID="btnAssignedStoreDirectB2BMail" runat="server" OnCommand="btnDirectB2BMailBusiness_Command" style="display:none;" CommandArgument="Assigned" />
                                            </td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td>
                                        <asp:Panel runat="server" DefaultButton="hfImgBtnAssignedStoreDirectB2BMailSearch">
                                          <asp:TextBox ID="txtAssignedStoreDirectB2BMailSearch" runat="server" Width="200px" CssClass="formFields clearable" MaxLength="50" Height="18px"></asp:TextBox>
                                          <asp:ImageButton ID="hfImgBtnAssignedStoreDirectB2BMailSearch" runat="server" CommandArgument="AssignedStoreDirectB2BMail" OnCommand="btnSearchRecords_Click" Style="display:none;" />
                                        </asp:Panel>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgBtnAssignedStoreDirectB2BMailSearch" runat="server" AlternateText="Assigned Store Direct B2B Mail Businesses Search" ImageUrl="../images/btn_search.png" 
                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_search.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_lit.png');" 
                                        CommandArgument="AssignedStoreDirectB2BMail" OnCommand="btnSearchRecords_Click" />
                                    </td>
                                    <td><asp:ImageButton ID="imgBtnAssignedStoreDirectB2BMailInstr" runat="server" AlternateText="Assigned to Store Direct B2B Mail Business instructions" /></td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                            <tr>
                            <td style="vertical-align: top; padding-top:5px; padding-left:12px; padding-right: 12px; padding-bottom: 5px; text-align:left;" class="formFields2">
                            <asp:GridView ID="grdAssignedStoreDirectB2BMail" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10" GridLines="None" 
                            Width="100%" OnPageIndexChanging="grdAssignedStoreDirectB2BMail_PageIndexChanging"  OnRowDataBound="grdAssignedStoreDirectB2BMail_RowDataBound" OnSorting="grdAssignedStoreDirectB2BMail_sorting" AlternatingRowStyle-BackColor="#E9E9E9" >
                                <FooterStyle CssClass="footerGrey" />                                
                                <HeaderStyle BackColor="#3096D8" ForeColor="White" Font-Bold="true" />                                
                                <RowStyle BorderWidth="1px" BorderColor="#CCCCCC" />
                                <Columns>
                                 <asp:TemplateField HeaderText="selectAssignedBusinesses" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
				                    <HeaderTemplate>
					                    <asp:CheckBox id="chkSelectAllAssignedBusiness" runat="server" />
				                    </HeaderTemplate>
				                    <ItemTemplate>
					                    <asp:CheckBox id="chkSelectAssignedBusiness" runat="server" />
				                    </ItemTemplate>
			                    </asp:TemplateField>
                                 <asp:TemplateField HeaderText="StoreId" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusinessPk" runat="server" Text='<%# Bind("businessPk") %>'></asp:Label>
                                        <asp:Label ID="lblOutreachBusinessPk" runat="server" Text='<%# Bind("outreachBusinessPk") %>'></asp:Label>
                                        <asp:Label ID="lblCanDelete" runat="server" Text='<%# Bind("canDelete") %>'></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Business Name & Number" HeaderStyle-HorizontalAlign="Center" SortExpression="businessName" HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true"
                                                HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                      <asp:Label ID="lblBusinessName" runat="server" Text='<%# Bind("businessName") %>'></asp:Label><br />
                                      <asp:Label ID="lblBusinessPhone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Business Address" HeaderStyle-HorizontalAlign="Center" SortExpression="businessAddress" HeaderStyle-BorderColor="#336699" 
                                  HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                      <asp:Label ID="lblBusinessAddress" runat="server" Text='<%# Bind("businessAddress") %>' Width="150px" class="wrapword"></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Emp" HeaderStyle-HorizontalAlign="Center" SortExpression="actualLocationEmploymentSize" HeaderStyle-BorderColor="#336699" 
                                  HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                      <asp:Label ID="lblActualLocationEmploymentSize" runat="server" Text='<%# Bind("actualLocationEmploymentSize") %>' Width="50px" class="wrapword" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="District" HeaderStyle-HorizontalAlign="Center" SortExpression="districtId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                  HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                      <asp:Label ID="lblDistrict" runat="server" Text='<%# Bind("districtId") %>' Width="50px" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Assigned Store" HeaderStyle-HorizontalAlign="Center" SortExpression="storeId" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                  HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                      <asp:Label ID="lblB2BStoreId" runat="server" Text='<%# Bind("storeId") %>' Width="40px" style="text-align:center"></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Assigned Date" HeaderStyle-HorizontalAlign="Center" SortExpression="LastContact" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                  HeaderStyle-BorderWidth="1px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssignedDate" runat="server" Text='<%# Eval("dateAdded", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Center" SortExpression="OutreachStatus" HeaderStyle-BorderColor="#336699" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOutreachStatus" runat="server" Text='<%# Bind("outreachStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                  </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                  <center>
                                    <b><asp:Label ID="lblNote" Text="No record exists for given search criteria." CssClass="formFields" runat="server"></asp:Label></b>
                                  </center>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="LightBlue" />
                            </asp:GridView>                            
                            </td></tr>
                            <tr>
                                <td style="vertical-align: top; padding-top: 12px; padding-left: 12px; padding-right: 12px; text-align: left;">Use the Search and Filter functions for more results.</td>
                           </tr>
                            <tr>
                              <td style="text-align: right; padding-top:5px; padding-left:22px; padding-right: 22px; padding-bottom: 22px;" >
                               <asp:ImageButton ID="btnResetToHCS" ImageUrl="~/images/btn_reset_to_HCS.png" runat="server" AlternateText="Reset To HCS" OnCommand="btnResetToHCS_Command"
                                    CommandArgument="AssignedStore" onmouseout="javascript:MouseOutImage(this.id,'images/btn_reset_to_HCS.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_reset_to_HCS_lit.png');" CausesValidation="true" />                                                                                                                                                                                                                           
                              </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>