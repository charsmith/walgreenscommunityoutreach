﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MetricsReports.ascx.cs" Inherits="MetricsReports" %>
<table width="100%" border="0" cellpadding="0" cellspacing="6" class="matricsreportMonitor" runat="server" id="rowMetricsReports">
    <tr class="matricsreportMonitorInterior">
        <td width="100%" valign="top" class="matricsreportHead">Metrics</td>
    </tr>
    <tr>
        <td width="100%" valign="top">
            <asp:GridView ID="grdMetricsReports" runat="server" GridLines="None" AutoGenerateColumns="false" Width="100%" AllowPaging="false" AllowSorting="false" 
                Font-Names="Arial, Helvetica, sans-serif" Font-Size="12px" BackColor="#ffffff" OnRowDataBound="grdMetrics_OnRowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Reports" HeaderStyle-CssClass="matricsreportTextHead" HeaderStyle-Width="75%" HeaderStyle-HorizontalAlign="Left" 
                        ItemStyle-CssClass="matricsreportText" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnMetricReportId" runat="server" CssClass="matricsreportMonitorItems" Text='<%# Bind("MetricName") %>' 
                                CommandArgument='<%# Bind("MetricId") %>' OnCommand="doProcess_Click" CommandName="report" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Qty" HeaderStyle-CssClass="matricsreportTextHead" HeaderStyle-Width="10%"  HeaderStyle-HorizontalAlign="right" 
                        ItemStyle-CssClass="matricsreportText" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            <asp:Label ID="lblMetricCount" runat="server" CssClass="matricsreportMonitorItems" Text='<%# Bind("MetricCount") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
