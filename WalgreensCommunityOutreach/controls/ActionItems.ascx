﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActionItems.ascx.cs" Inherits="ActionItems" %>
<table width="100%" border="0" cellpadding="0" cellspacing="6" class="actionitemMonitor" runat="server" id="rowActionReports">
    <tr class="matricsreportMonitorInterior">
        <td width="100%" valign="top" class="actionitemHead">HCS Action Items</td>
    </tr>
    <tr>
        <td width="100%" valign="top">
            <asp:GridView ID="grdActionItems" runat="server" GridLines="None" AutoGenerateColumns="false" Width="100%" AllowPaging="false" AllowSorting="false" 
                Font-Names="Arial, Helvetica, sans-serif" Font-Size="12px" BackColor="#ffffff" OnRowDataBound="grdActionItems_OnRowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="actionitemTextHead" HeaderStyle-Width="75%" HeaderStyle-HorizontalAlign="Left" 
                        ItemStyle-CssClass="actionitemText" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblActionItemId" runat="server" Text='<%# Bind("actionItemId") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblActionItem" runat="server" CssClass="actionitemMonitorItems" Text='<%# Bind("actionItem") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HCS" HeaderStyle-CssClass="actionitemTextHead" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="center" 
                        ItemStyle-CssClass="actionitemText" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnHCSCounts" CommandArgument='<%# Bind("actionItemId") %>' runat="server" CssClass="actionitemMonitorItems" 
                                Text='<%# Bind("HCSCount") %>' OnCommand="doProcess_Click" CommandName="report" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Store" HeaderStyle-CssClass="actionitemTextHead" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="center" 
                        ItemStyle-CssClass="workflowText" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBtnStoreCounts" runat="server" CssClass="actionitemMonitorItems" Text='<%# Bind("StoreCount") %>' 
                                OnCommand="doProcess_Click" CommandName="store" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
