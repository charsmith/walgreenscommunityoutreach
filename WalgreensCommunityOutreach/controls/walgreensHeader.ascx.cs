﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using System.Data;
using System.Web.Security;
using TdWalgreens;
using System.Web.Script.Serialization;

public partial class controls_walgreensHeader : System.Web.UI.UserControl
{
    public delegate void OnButtonClick();
    public event OnButtonClick btnRefreshHandler;
    public delegate void onRefreshButtonClick();
    public event onRefreshButtonClick btnStoreIdRefreshHandler;

    #region -------------------- PROTECTED EVENTS ---------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserName))
        {
            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "admin":
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                case "regional healthcare director":
                case "regional vice president":
                    this.btnStore.Visible = true;
                    this.btnHome.PostBackUrl = "~/walgreensDistrictUsersHome.aspx";
                    //if (this.commonAppSession.LoginUserInfoSession.DuplicateBusinessCount == 0 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                    if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                        this.btnHome.Visible = false;
                    break;
            }
        }
        else
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return;
        }

        if (!IsPostBack)
        {
            if (Session["clientDetails"] != null)
                Session.Remove("clientDetails");
            if (Request.Url.ToString().Substring(Request.Url.ToString().LastIndexOf("/") + 1).ToLower() != "usereditor.aspx")
                Session.Remove("UserProfiles");

            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
        }

        string[] folder = Request.Url.ToString().Split('/');

        this.imgLogo.Src = (folder.Count() > 1) ? "../images/wags_logo.png" : "images/wags_logo.png";
        if (folder.Contains("Admin") || folder.Contains("corporateUploader"))
            this.MouseOverImages();

        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
        {
            this.lblOutreachProgram.InnerText = "Senior Outreach";
        }
        else
        {
            this.lblOutreachProgram.InnerText = "Immunization Program";
        };

        this.LastContact();

        if (Request.Url.ToString().Substring(Request.Url.ToString().LastIndexOf("/") + 1).ToLower() != "walgreensdistrictusershome.aspx")
        {
            this.commonAppSession.dtCorporateClinics = null;
            this.commonAppSession.dtDirectB2BBusiness = null;
            this.commonAppSession.dtLocalLeads = null;
        }

        this.UserId = this.commonAppSession.LoginUserInfoSession.UserID;
        this.displayStoreSelectionToUser();
    }

    protected void lnkBtnRefreshWorkflow_Click(object sender, EventArgs e)
    {
        if (Request.Url.AbsoluteUri.Contains("walgreensHome.aspx"))
            btnRefreshHandler();
    }

    protected void showContactLog_click(object sender, CommandEventArgs e)
    {
        string[] folder = Request.Url.ToString().Split('/');

        this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = Convert.ToInt32(e.CommandArgument.ToString());
        if (folder.Contains("Admin") || folder.Contains("reports") || folder.Contains("corporateUploader"))
            Response.Redirect("../walgreensContactLog.aspx", false);
        else
            Response.Redirect("walgreensContactLog.aspx", false);
    }

    protected void imgSignOut_Click(object sender, ImageClickEventArgs e)
    {
        Dictionary<int, string> wag_user = (Dictionary<int, string>)Application["WagUsers"];
        bool user_exists = false;
        if (wag_user.Count > 0)
        {
            AppCommonSession common_app_session = new AppCommonSession();            
            if (wag_user.ContainsKey(common_app_session.LoginUserInfoSession.UserID))
                user_exists = true;

            if (user_exists)
                wag_user.Remove(common_app_session.LoginUserInfoSession.UserID);

            Application["WagUsers"] = wag_user;
        }
        Session.RemoveAll();
        FormsAuthentication.SignOut();
        string[] folder = Request.Url.ToString().Split('/');
        if (folder.Contains("Admin") || folder.Contains("reports") || folder.Contains("corporateUploader"))
            Response.Redirect("../thankYou.aspx");
        else
            Response.Redirect("thankYou.aspx");
    }

    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        if (this.btnStoreIdRefreshHandler != null)
        {
            this.btnStoreIdRefreshHandler.Invoke();
        }

        if (this.ddlStoreList.SelectedItem != null)
            this.txtStoreProfiles.Text = this.ddlStoreList.SelectedItem.Text;
        this.commonAppSession.SelectedStoreSession.storeName = this.strStoreName;
        this.commonAppSession.SelectedStoreSession.lastStatusProvided = this.strLastStatusProvided;
        this.commonAppSession.SelectedStoreSession.storeName = this.txtStoreProfiles.Text;
    }
    #endregion

    #region -------------------- PRIVATE METHODS ----------------------
    private void LastContact()
    {
        string last_access = string.Empty;
        string to_goal = string.Empty;
        string to_complete = string.Empty;
        string store_state;
        this.commonAppSession.SelectedStoreSession.storeID = string.IsNullOrEmpty(this.txtStoreId.Text) ? this.commonAppSession.SelectedStoreSession.storeID : Convert.ToInt32(this.txtStoreId.Text);
        this.dbOperation.getHeaderValues(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected, ApplicationSettings.getOutreachStartDate, ApplicationSettings.getCurrentQuarter.ToString(), this.commonAppSession.LoginUserInfoSession.UserID, out last_access, out to_goal, out to_complete); //set the goal message here   

        if (!string.IsNullOrEmpty(txtStoreProfiles.Text) && !this.txtStoreProfiles.Text.Contains("Nothing"))
        {
            this.commonAppSession.SelectedStoreSession.storeName = txtStoreProfiles.Text;
            this.commonAppSession.SelectedStoreSession.storeID = Convert.ToInt32(txtStoreId.Text);
        }

        this.commonAppSession.SelectedStoreSession.getAssignedBusinesses = dbOperation.getAssignedBusinesses(this.commonAppSession.SelectedStoreSession.storeID, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), out store_state);

        this.commonAppSession.SelectedStoreSession.storeState = store_state;

        this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses = this.dbOperation.getAssistedClinicBusinesses(this.commonAppSession.SelectedStoreSession.storeID);

        this.assignedBusiness();

        if (!string.IsNullOrEmpty(last_access) && !this.txtStoreProfiles.Text.Contains("Nothing"))
            this.commonAppSession.SelectedStoreSession.lastStatusProvided = (string)GetGlobalResourceObject("errorMessages", "lastStatusProvided") + " " + last_access;
        else
            this.commonAppSession.SelectedStoreSession.lastStatusProvided = "";

        this.strStoreName = this.commonAppSession.SelectedStoreSession.storeName;
        this.strLastStatusProvided = this.commonAppSession.SelectedStoreSession.lastStatusProvided;

        if (string.IsNullOrEmpty(to_goal)) to_goal = "0";
        if (string.IsNullOrEmpty(to_complete)) to_complete = "0";
        Literal lblHeaderDetails = ((Literal)this.FindControl("lblHeaderDetails"));
        Literal lblLastStatusProvided = ((Literal)this.FindControl("lblLastStatusProvided"));

        if (!this.txtStoreProfiles.Text.Contains("Nothing"))
        {
            if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
            {
                this.strStoreGoalMsg += String.Format((string)GetGlobalResourceObject("errorMessages", "goalMessage"), to_goal, to_complete, "Q" + ApplicationSettings.getCurrentQuarter.ToString());
            }

            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                case "regional vice president":
                case "regional healthcare director":
                    lblHeaderDetails.Text = "<span class='wagsAddress'> " + this.commonAppSession.LoginUserInfoSession.LocationType + " " + this.commonAppSession.LoginUserInfoSession.AssignedLocations + "</span><br />";
                    if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                        lblLastStatusProvided.Text = (!string.IsNullOrEmpty(this.strStoreGoalMsg) ? "<span class='statusLine'>" + this.strStoreGoalMsg + "</span>" : "");
                    break;
                default:
                    lblHeaderDetails.Text = "<span class='wagsAddress'>" + this.strStoreName + "</span><br />";
                    lblLastStatusProvided.Text = "";
                    lblLastStatusProvided.Text = "<span class='statusLine' width='200'>" + (!string.IsNullOrEmpty(this.strStoreGoalMsg) ? this.strStoreGoalMsg + "<br />" : "") + (!string.IsNullOrEmpty(this.strLastStatusProvided) ? "<b>" + this.strLastStatusProvided + "</b>" : (string.IsNullOrEmpty(this.strStoreGoalMsg)) ? lblLastStatusProvided.Text : "") + "</span>";
                    break;
            }
        }
        else
        {
            //this.commonAppSession.SelectedStoreSession.storeGoalMsg = "";
            this.strStoreGoalMsg = "";
            lblHeaderDetails.Text = "<span class='wagsAddress'>" + this.strStoreName + "</span><br />";
        }
        this.commonAppSession.SelectedStoreSession.HeaderDetails = "";
        this.commonAppSession.SelectedStoreSession.HeaderDetails = lblHeaderDetails.Text + "|" + lblLastStatusProvided.Text;

        if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId))
        {
            this.strStoreName = this.commonAppSession.SelectedStoreSession.storeName;
            this.strLastStatusProvided = this.commonAppSession.SelectedStoreSession.lastStatusProvided != null ? this.commonAppSession.SelectedStoreSession.lastStatusProvided.Trim() : "";
            this.strStoreGoalMsg = this.commonAppSession.SelectedStoreSession.storeGoalMsg;
            if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
                this.commonAppSession.SelectedStoreSession.HeaderDetails = string.Empty;

            if (string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.HeaderDetails))
            {
                switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
                {
                    case "district manager":
                    case "healthcare supervisor":
                    case "director – rx & retail ops":
                    case "regional vice president":
                    case "regional healthcare director":
                        lblHeaderDetails.Text = "<span class='wagsAddress'> " + this.commonAppSession.LoginUserInfoSession.LocationType + " " + this.commonAppSession.LoginUserInfoSession.AssignedLocations + "</span><br />";
                        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                            this.lblHeaderDetails.Text += "<span class='statusLine' width='200'>" + this.commonAppSession.SelectedStoreSession.storeGoalMsg + "</span>";
                        break;
                    default:
                        this.lblHeaderDetails.Text = "<span class='wagsAddress'>" + this.strStoreName + "</span>";
                        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
                            this.lblHeaderDetails.Text = this.lblHeaderDetails.Text + (!string.IsNullOrEmpty(this.strStoreGoalMsg) ? "<br /><span class='statusLine'>" + this.strStoreGoalMsg + "</span>" : "");

                        if (this.strLastStatusProvided.Length > 0 && this.strLastStatusProvided.Split(':')[1].ToString().Length > 0)
                            this.lblLastStatusProvided.Text = (!string.IsNullOrEmpty(this.strLastStatusProvided) && (this.strLastStatusProvided.ToLower() != "last status provided:") ? "<span class='statusLine' width='200'><b>" + this.strLastStatusProvided + "</b></span>" : lblLastStatusProvided.Text);
                        break;
                }
            }
            else
            {
                string[] delim = { "<br />" };
                string[] spltd = this.commonAppSession.SelectedStoreSession.HeaderDetails.Split(delim, StringSplitOptions.None);

                this.lblHeaderDetails.Text = spltd[0];
                if (spltd[1] != null && spltd[1].Length > 0)
                    this.lblLastStatusProvided.Text = spltd[1].ToString().Substring(1, spltd[1].ToString().Length - 1);

                if (spltd.Count() > 2)
                {
                    if (spltd[2] != null && spltd[2].Length > 0)
                        this.lblLastStatusProvided.Text += "<br />" + spltd[2].ToString();
                }
            }

            switch (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId)
            {
                case "3": //Immunization Progaram
                    this.lnkStoreBusinessContactReport.Visible = true;
                    this.lnkStoreBusinessReport.Visible = false;
                    this.lnkContactLogReport.Visible = false;
                    this.lnkJobAids.Visible = true;
                    this.lnkClinicContactLog.Visible = true;
                    this.lnkFaqIP.Visible = true;
                    this.lnkLocationComplianceReport.HRef = "~/reports/reportStoreBusinessFeedbackPer.aspx";
                    //this.lnkHighLevelReport.HRef = "~/reports/reportHighLevel.aspx";
                    //this.lnkDistrictCompliance.HRef = "~/reports/reportDistrictCompliance.aspx";
                    this.lnkDistrictCompletion.Visible = false;
                    this.lnkEventScheduleReport.Visible = false;
                    this.lnkLocationComplianceReport.Visible = true;
                    this.lnkClinicDetailsReport.Visible = true;
                    this.lnkActivityReport.Visible = false;
                    this.lnkScheduledEventsReport.Visible = false;
                    this.lnkVaccinePurchasingReport.Visible = false;
                    this.lnkVaccinePurchasingPage.Visible = false;
                    this.lnkDirectB2BMailCampaignReport.Visible = false;
                    this.lnkGrpIdAssignmentReport.Visible = false;
                    this.lnkGrpIdAssignmentPage.Visible = false;
                    this.lnkUploadCorporateData.Visible = false;
                    this.lnkStoreProfileUpdate.Visible = false;
                    this.lnkScheduledAppointmentsReport.Visible = false;
                    this.lnkContractAgreementsReport.Visible = false;
                    this.lnkExceptionShotsPage.Visible = false;
                    this.lnkVaccineExceptionPage.Visible = false;
                    //this.lnkHHSVoucherReport.Visible = true;
                    if (ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "StoreProfileUpdate"))
                    {
                        this.lnkStoreProfileUpdate.Visible = true;
                    }
                    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    {
                        this.imgAdmin.Visible = true;
                        this.lnkAccessLogReport.Visible = true;
                        this.lnkFollowupReminderReport.Visible = false;
                        this.lnkPendingEmailReminderReport.Visible = false;
                        this.lnkHighLevelStatusReport.Visible = true;
                        this.lnkDistrictCompliance.Visible = false;
                        this.lnkHighLevelReport.Visible = false;
                        this.lnkHighLevelStatusReport.InnerHtml = "High Level Status Report";
                        //this.lnkAssignedNationalAccountsReport.Visible = true;
                        this.lnkContractAgreementsReport.Visible = true;
                        this.lnkWorkflowMonitorReport.Visible = true;
                        this.lnkScheduledAppointmentsReport.Visible = true;
                        this.lnkVaccinePurchasingReport.Visible = true;
                        this.lnkVaccinePurchasingPage.Visible = true;
                        this.lnkDirectB2BMailCampaignReport.Visible = true;
                        this.lnkGrpIdAssignmentReport.Visible = true;
                        this.lnkGrpIdAssignmentPage.Visible = true;
                        this.lnkUploadCorporateData.Visible = true;
                        this.lnkExceptionShotsPage.Visible = true;
						this.lnkCustomMessage.Visible = true;
                        this.lnkVaccineExceptionPage.Visible = true;
                        this.lnkImmunizationFinancePage.Visible = true;
                    }
                    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
                    {
                        this.imgAdmin.Visible = false;
                        this.lnkAccessLogReport.Visible = true;
                        this.lnkHighLevelStatusReport.Visible = true;
                        this.lnkDistrictCompliance.Visible = false;
                        this.lnkHighLevelReport.Visible = false;
                        this.lnkWorkflowMonitorReport.Visible = true;
                        this.lnkLocationComplianceReport.Visible = true;
                        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                        {
                            this.lnkHighLevelStatusReport.InnerHtml = "District High Level Status Report";
                            this.lnkLocationComplianceReport.Visible = false;
                            //lnkAssignedNationalAccountsReport.Visible = true;
                            //this.lnkWorkflowMonitorReport.Visible = true;
                        }
                        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "director – rx & retail ops" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "healthcare supervisor")
                            this.lnkHighLevelStatusReport.InnerHtml = "Area High Level Status Report";

                        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional vice president" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional healthcare director")
                        {
                            this.lnkHighLevelStatusReport.InnerHtml = "Region High Level Status Report";
                            //this.lnkWorkflowMonitorReport.Visible = true;
                        }
                    }
                    else
                    {
                        this.imgAdmin.Visible = false;
                        this.lnkAccessLogReport.Visible = false;
                        this.lnkHighLevelReport.Visible = false;
                        this.lnkDistrictCompliance.Visible = false;
                        this.lnkFollowupReminderReport.Visible = false;
                        this.lnkWorkflowMonitorReport.Visible = false;
                    }
                    break;
                case "1": // Senior Outreach
                    this.lnkStoreBusinessContactReport.Visible = false;
                    this.lnkStoreBusinessReport.Visible = true;
                    this.lnkContactLogReport.Visible = true;
                    this.lnkResources.Visible = true;
                    this.lnkClinicContactLog.Visible = false;
                    this.lnkFaqSO.Visible = true;
                    this.lnkLocationComplianceReport.HRef = "~/reports/reportLocationComplianceSO.aspx";
                    this.lnkHighLevelReport.HRef = "~/reports/reportHighLevelSO.aspx";
                    this.lnkDistrictCompliance.HRef = "~/reports/reportDistrictComplianceSO.aspx";
                    this.lnkDistrictCompletion.Visible = false;
                    this.lnkLocationComplianceReport.Visible = true;
                    this.lnkClinicDetailsReport.Visible = false;
                    //this.lnkHHSVoucherReport.Visible = false;
                    //lnkAssignedNationalAccountsReport.Visible = false;
                    this.lnkWorkflowMonitorReport.Visible = false;
                    this.lnkCorporateScheduler.Visible = false;
                    this.lnkScheduledAppointmentsReport.Visible = false;
                    this.lnkActivityReport.Visible = true;
                    this.lnkScheduledEventsReport.Visible = true;
                    this.lnkUploadCorporateData.Visible = false;
                    this.lnkStoreProfileUpdate.Visible = false;
                    this.lnkCustomMessage.Visible = false;
                    if (ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "StoreProfileUpdate"))
                        this.lnkStoreProfileUpdate.Visible = true;
                    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    {
                        this.imgAdmin.Visible = true;
                        this.lnkAccessLogReport.Visible = true;
                        this.lnkEventScheduleReport.Visible = false;
                        this.lnkFollowupReminderReport.Visible = true;
                        this.lnkPendingEmailReminderReport.Visible = true;
                        this.lnkHighLevelStatusReport.Visible = true;
                        this.lnkHighLevelReport.Visible = true;
                        this.lnkDistrictCompliance.Visible = true;
                        this.lnkHighLevelStatusReport.InnerHtml = "High Level Status Report";
                        this.lnkCustomMessage.Visible = true;
                    }
                    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
                    {
                        this.imgAdmin.Visible = false;
                        this.lnkAccessLogReport.Visible = true;
                        this.lnkEventScheduleReport.Visible = false;
                        this.lnkHighLevelReport.Visible = false;
                        this.lnkDistrictCompliance.Visible = true;
                        this.lnkHighLevelStatusReport.Visible = true;
                        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                        {
                            this.lnkLocationComplianceReport.Visible = false;
                            this.lnkHighLevelStatusReport.InnerHtml = "District High Level Status Report";
                        }
                        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "director – rx & retail ops" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "healthcare supervisor")
                            this.lnkHighLevelStatusReport.InnerHtml = "Area High Level Status Report";

                        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional vice president" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "regional healthcare director")
                            this.lnkHighLevelStatusReport.InnerHtml = "Region High Level Status Report";
                    }
                    else
                    {
                        this.imgAdmin.Visible = false;
                        this.lnkAccessLogReport.Visible = false;
                        this.lnkEventScheduleReport.Visible = false;
                        this.lnkHighLevelReport.Visible = false;
                        this.lnkDistrictCompliance.Visible = false;
                        this.lnkFollowupReminderReport.Visible = true;
                    }
                    break;
            }
        }

        DataRow[] dt_rows;
        if (this.commonAppSession.SelectedStoreSession.getAssignedBusinesses != null && this.commonAppSession.SelectedStoreSession.getAssignedBusinesses.Rows.Count > 0)
        {
            dt_rows = this.commonAppSession.SelectedStoreSession.getAssignedBusinesses.Select("businessType='2'");
            if (dt_rows.Count() > 0 && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
                this.lnkScheduledBusinesses.Visible = true;
            else
                this.lnkScheduledBusinesses.Visible = false;
            if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses != null && this.commonAppSession.SelectedStoreSession.getAssistedClinicBusinesses.Rows.Count > 0)
                this.lnkAssistedClinics.Visible = true;
            else
                this.lnkAssistedClinics.Visible = false;
        }

    }

    /// <summary>
    /// Binding businesses assigned to store to dropdown    
    /// </summary>
    private void assignedBusiness()
    {
        //string store_state;
        int store_id = 0;
        DataTable dt_filtered_business = new DataTable();
        //DataTable dt_businesses = this.dbOperation.getAssignedBusinesses(this.commonAppSession.SelectedStoreSession.storeID, Convert.ToInt32(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId), out store_state);

        DataTable dt_businesses = this.commonAppSession.SelectedStoreSession.getAssignedBusinesses;

        DataRow[] businesses = dt_businesses.Select("outreachEffort LIKE '%" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected + "%'");
        if (businesses.Count() > 0)
            dt_businesses = businesses.CopyToDataTable();
        else
            dt_businesses.Clear();

        DataView dv_businesses = dt_businesses.DefaultView;

        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
            dv_businesses.Sort = "isExistingBusiness DESC";
        else
            dv_businesses.Sort = "sicPriority DESC";
        //dv_businesses.Sort = "isStandardized DESC";

        dv_businesses.RowFilter = "outreachStatusTitle NOT LIKE '%Business Closed%'";
        dt_filtered_business = dv_businesses.Table.Clone();
        foreach (DataRowView drowview in dv_businesses)
            dt_filtered_business.ImportRow(drowview.Row);

        DataView dv_business_closed = dt_businesses.DefaultView;
        dv_business_closed.RowFilter = "outreachStatusTitle LIKE '%Business Closed%'";

        foreach (DataRowView drowview in dv_business_closed)
            dt_filtered_business.ImportRow(drowview.Row);

        //Create Outreach Business JSON object serialization
        var json_serializer = new JavaScriptSerializer();
        List<Dictionary<string, object>> outreach_rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> outreach_row = null;

        List<Dictionary<string, object>> clinic_rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> clinic_row = null;

        int count = 0;
        string outreach_status_id = "";
        string contacts = "";
        string last_contact_grid = "";
        string str_contact_output = "";
        string outreach_status_id_grid = "";
        string outreach_statusTitle_grid = "";
        string outreach_status_title = "";
        string contacts_tool_tip = "";
        string last_contact_tool_tip = "";
        string outreach_status_grid_tool_tip = "";
        string outreach_contact_date = "";
        string account_Type = "";

        foreach (DataRow dt_row in dt_businesses.Rows)
        {
            if (Convert.ToInt32(dt_row["businessType"].ToString()) == 1)
            {
                outreach_row = new Dictionary<string, object>();

                foreach (DataColumn dt_column in dt_businesses.Columns)
                {
                    if (dt_column.ColumnName.ToLower() == "businesspk")
                        outreach_row.Add("businessId", dt_row["businessPk"].ToString().Trim());
                    if (dt_column.ColumnName.ToLower() == "businessname")
                        outreach_row.Add(dt_column.ColumnName, "<u style='cursor:hand'>" + ((Convert.ToInt32(dt_row["isStandardized"]) == 0) ? this.appSettings.removeLineBreaks(dt_row[dt_column].ToString().Replace("\"", "&quot;")) + " *" : this.appSettings.removeLineBreaks(dt_row[dt_column].ToString().Replace("\"", "&quot;"))) + "</u>".Trim());
                    else if (dt_column.ColumnName.ToLower() == "employmentsize")
                        outreach_row.Add(dt_column.ColumnName, (dt_row["employmentSize"] is DBNull || dt_row["employmentSize"].ToString().Equals("0")) ? "N/A" : dt_row["employmentSize"].ToString().Trim());
                    else if (dt_column.ColumnName.ToLower() == "outreacheffort")
                    {
                        count = 0;
                        outreach_status_id = "";
                        contacts = "";
                        last_contact_grid = "";
                        outreach_status_id_grid = "";
                        outreach_statusTitle_grid = "";
                        outreach_status_title = "";
                        contacts_tool_tip = "";
                        last_contact_tool_tip = "";
                        outreach_status_grid_tool_tip = "";
                        foreach (string str_outreach_program in dt_row["outreachEffort"].ToString().TrimStart(',').Split(','))
                        {
                            str_contact_output = dt_row["lastContact"].ToString().Split(',').Count() > 1 ? dt_row["lastContact"].ToString().Split(',')[count + 1].ToString() : "";

                            if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelected == str_outreach_program)
                            {
                                //contacts = contacts + "<a href='walgreensContactLog.aspx?businessId=" + dt_row["businessPk"] + "&outreachStatus=" + dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString() + "&outreachEffort=" + str_outreach_program + "'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";

                                last_contact_grid = last_contact_grid + "<span>" + str_contact_output + "</span><br/>";
                                outreach_status_id_grid = dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString();

                                outreach_status_id = dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString();
                                outreach_status_title = dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString();
                                contacts = contacts + "<a onClick='javascript:showBusinessContactLog(" + dt_row["businessPk"] + "," + outreach_status_id + ",3)' href='#'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Replace("IP", "IMZ") + "</a><br/>";

                                if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" && outreach_status_id == "3")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(1, " + contact_log_pk + ")' href='#' >" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (outreach_status_id == "2")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(2, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "8")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(4, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "3")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(3, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "11")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(5, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" && outreach_status_id == "12")
                                {
                                    string contact_log_pk = dt_row["contactLogPk"].ToString().Split(',')[count + 1].ToString();
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<a onClick='javascript:showContactLogDetails(6, " + contact_log_pk + ")' href='#'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                }
                                else
                                    outreach_statusTitle_grid = outreach_statusTitle_grid + "<span>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</span><br/>";

                                outreach_contact_date = str_contact_output;
                            }
                            else
                            {
                                outreach_status_id = dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString();
                                //contacts = contacts + "<a href='walgreensContactLog.aspx?businessId=" + dt_row["businessPk"] + "&outreachStatus=" + dt_row["outreachStatus"].ToString().TrimStart(',').Split(',')[count].ToString() + "&outreachEffort=" + str_outreach_program + "'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString() + "</a><br/>";
                                contacts = contacts + "<a onClick='javascript:showBusinessContactLog(" + dt_row["businessPk"] + "," + outreach_status_id + ",1)' href='#'>" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Replace("IP", "IMZ") + "</a><br/>";
                                last_contact_grid = last_contact_grid + "<span style='color:#999999'>" + str_contact_output + "</span><br/>";
                                outreach_statusTitle_grid = outreach_statusTitle_grid + "<span style='color:#999999'>" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString() + "</span><br/>";
                            }

                            if (str_outreach_program.ToLower() == "ip")
                            {
                                contacts_tool_tip = "Immunization Program-" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Split(' ')[1];
                                if (str_contact_output.Trim().Length != 0) last_contact_tool_tip = "Immunization Program-" + str_contact_output;
                                outreach_status_grid_tool_tip = "Immunization Program-" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString();
                            }
                            else if (str_outreach_program.ToLower() == "so")
                            {
                                if (contacts_tool_tip.Trim().Length != 0) contacts_tool_tip += "|";
                                contacts_tool_tip = contacts_tool_tip + "Senior Outreach-" + dt_row["contacts"].ToString().TrimStart(',').Split(',')[count].ToString().Split(' ')[1];

                                if (last_contact_tool_tip.Trim().Length != 0) last_contact_tool_tip += "|";
                                if (str_contact_output.Trim().Length != 0) last_contact_tool_tip = last_contact_tool_tip + "Senior Outreach-" + str_contact_output;

                                if (outreach_status_grid_tool_tip.Trim().Length != 0) outreach_status_grid_tool_tip += "|";
                                outreach_status_grid_tool_tip = outreach_status_grid_tool_tip + "Senior Outreach-" + dt_row["outreachStatusTitle"].ToString().TrimStart(',').Split(',')[count].ToString();
                            }
                            count++;
                        }
                        outreach_row.Add("contacts", contacts.Trim());
                        outreach_row.Add("lastContactGrid", last_contact_grid.Trim());
                        outreach_row.Add("lastContact", str_contact_output.Trim());
                        outreach_row.Add("outreachStatus", outreach_status_title.Trim());
                        outreach_row.Add("outreachStatusIdGrid", outreach_status_id_grid.Trim());
                        outreach_row.Add("outreachStatusTitleGrid", outreach_statusTitle_grid.Trim());
                        outreach_row.Add("outreachStatusId", outreach_status_id_grid.Trim());
                        outreach_row.Add("contactsToolTip", contacts_tool_tip.Trim());
                        outreach_row.Add("lastContactToolTip", last_contact_tool_tip.Trim());
                        outreach_row.Add("statusToolTip", outreach_status_grid_tool_tip.Trim());
                        outreach_row.Add("outreachContactDate", outreach_contact_date.Trim());
                    }
                    else if (dt_column.ColumnName.ToLower() != "contacts" && dt_column.ColumnName.ToLower() != "lastcontact" && dt_column.ColumnName.ToLower() != "outreachstatus" && dt_column.ColumnName.ToLower() != "businessPk")
                        outreach_row.Add(dt_column.ColumnName, dt_row[dt_column].ToString().Trim().Replace("\\", "&quot;"));
                }

                outreach_rows.Add(outreach_row);
            }
            else if (Convert.ToInt32(dt_row["businessType"].ToString()) == 2)
            {
                clinic_row = new Dictionary<string, object>();
                foreach (DataColumn dt_column in dt_businesses.Columns)
                {
                    if (dt_column.ColumnName.ToLower() == "businessname")
                        clinic_row.Add(dt_column.ColumnName, this.appSettings.removeLineBreaks(dt_row[dt_column].ToString().Replace("\"", "&quot;")).Trim());
                    else if (dt_column.ColumnName.ToLower() == "phone")
                        clinic_row.Add("contactPhone", dt_row["phone"]);
                    else if (dt_column.ColumnName.ToLower() == "contacts")
                        clinic_row.Add(dt_column.ColumnName, dt_row["contacts"].ToString().TrimStart(','));
                    else if (dt_column.ColumnName.ToLower() == "lastcontact")
                        clinic_row.Add(dt_column.ColumnName, dt_row["lastContact"].ToString().TrimStart(','));
                    else if (dt_column.ColumnName.ToLower() == "outreachstatustitle")
                    {
                        account_Type = dt_row["accountType"].ToString();
                        if (!string.IsNullOrEmpty(dt_row["leadStoreId"].ToString()))
                        {
                            if ((dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0].ToLower() == "contact client") && Convert.ToInt32(dt_row["leadStoreId"].ToString()) != Convert.ToInt32(dt_row["storeId"].ToString()))
                            {
                                if (Convert.ToInt32(dt_row["leadStoreId"].ToString()) != store_id)
                                {
                                    clinic_row.Add("contactStatus", "<span style='color:#5B9EC6'><b>*" + dt_row["leadStoreId"].ToString() + ":</b> </span>" + dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                                    // assigned_clinic_row.Visible = true;
                                }
                                else
                                {
                                    clinic_row.Add("contactStatus", "<span style='color:#5B9EC6'><b>*" + dt_row["storeId"].ToString() + ":</b> </span>" + dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                                    //clinic_lead_row.Visible = true;
                                }
                            }
                            else
                            {
                                clinic_row.Add("contactStatus", dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                            }
                        }
                        else
                        {
                            clinic_row.Add("contactStatus", dt_row["outreachstatustitle"].ToString().TrimStart(',').Split(',')[0]);
                        }
                    }
                    else if (dt_column.ColumnName.ToLower() == "storeid" && Convert.ToInt32(dt_row["storeid"]) != this.commonAppSession.SelectedStoreSession.storeID && Convert.ToInt32(dt_row["leadStoreId"]) == this.commonAppSession.SelectedStoreSession.storeID)
                        clinic_row.Add("isClinicUnderLeadStore", "true");
                    else if (dt_column.ColumnName.ToLower() == "outreachstatus")
                        clinic_row.Add("outreachStatusId", dt_row["outreachstatus"].ToString().TrimStart(','));
                    //else if (dt_column.ColumnName.ToLower() == "naclinictotalimmadministered")
                    //    clinic_row.Add("totalImmAdministered", dt_row["naClinicTotalImmAdministered"].ToString().TrimStart(','));
                    else
                        clinic_row.Add(dt_column.ColumnName, dt_row[dt_column]);
                }
                clinic_rows.Add(clinic_row);

            }
        }
        this.commonAppSession.SelectedStoreSession.OutreachBusinessesJson = json_serializer.Serialize(outreach_rows).Replace(@"\""", @"&quot;").Replace("\''", "&#39;");
        this.commonAppSession.SelectedStoreSession.AssignedClinicsJson = json_serializer.Serialize(clinic_rows).Replace("\r\n", "").Replace("\n", "").Replace("\r", "").Replace(@"\""", @"&quot;").Replace("\''", "&#39;");
    }

    /// <summary>
    /// Binding Store dropdown without view state
    /// </summary>
    private void storeDropDown()
    {
        DBOperations dbOperation = new DBOperations();
        DataTable user_stores = dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
        this.ddlStoreList.DataSource = user_stores;
        this.ddlStoreList.DataTextField = "address";
        this.ddlStoreList.DataValueField = "storeid";
        this.ddlStoreList.DataBind();
    }

    /// <summary>
    /// Check for admin or power user/normal user to disable Admin menu item
    /// </summary>
    private void displayStoreSelectionToUser()
    {
        this.tdStoreSelectionBlock.Visible = false;
        this.tdStoreSelectionSearch.Visible = false;
        this.tdStoreSelectionDropDown.Visible = false;

        if (this.isStoreSearchVisible)
        {
            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
            {
                this.tdStoreSelectionBlock.Visible = true;
                this.tdStoreSelectionSearch.Visible = true;
                this.tdStoreSelectionDropDown.Visible = false;
            }
            else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
            {
                this.tdStoreSelectionBlock.Visible = true;
                this.tdStoreSelectionSearch.Visible = false;
                this.tdStoreSelectionDropDown.Visible = true;
                this.storeDropDown();
                if (this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text) != null)
                    this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text).Selected = true;
            }
        }
    }

    /// <summary>
    /// Shows/Hides menu items based on user logins
    /// </summary>
    private void MouseOverImages()
    {
        this.btnHome.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_home.gif');");
        this.btnHome.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_home_lit.gif');");
        this.btnStore.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_store.gif');");
        this.btnStore.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_store_lit.gif');");
        this.btnContact.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_contact_log.gif');");
        this.btnContact.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_contact_log_lit.gif');");
        this.btnMap.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_map.gif');");
        this.btnMap.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_map_lit.gif');");
        this.btnJobAids.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_Resources.gif');");
        this.btnJobAids.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_Resources_lit.gif');");
        this.btnResources.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_Resources.gif');");
        this.btnResources.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_Resources_lit.gif');");
        this.btnFaqIP.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_faq.gif');");
        this.btnFaqIP.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_faq_lit.gif');");
        this.btnFaqSO.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_faq.gif');");
        this.btnFaqSO.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_faq_lit.gif');");
        this.btnReports.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_reports.gif');");
        this.btnReports.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_reports_lit.gif');");
        this.imgAdmin.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_admin.gif');");
        this.imgAdmin.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_admin_lit.gif');");
        this.imgOutreachPortal.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_outreach_portal.gif');");
        this.imgOutreachPortal.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_outreach_portal_lit.gif');");
        this.imgSignOut.Attributes.Add("onmouseout", "javascript:MouseOutImage(this.id,'../images/btn_sign_out.gif');");
        this.imgSignOut.Attributes.Add("onmouseover", "javascript:MouseOverImage(this.id,'../images/btn_sign_out_lit.gif');");
    }
    #endregion

    #region -------------------- PRIVATE VARIABLES --------------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private ApplicationSettings appSettings = null;
    #endregion

    #region -------------------- PUBLIC PROPERTIES --------------------
    public string strStoreName { get; set; }
    public string strLastStatusProvided { get; set; }
    public string strStoreGoalMsg { get; set; }
    public string menuImagePath { get; set; }
    public bool isStoreSearchVisible { get; set; }
    public int UserId { get; set; }
    public string storeId
    {
        get { return txtStoreId.Text; }
        set { txtStoreId.Text = value; }
    }
    #endregion

    #region -------------------- Web Form Designer generated code --------------------
    protected void Page_Init(object sender, EventArgs e)
    {
        this.appSettings = new ApplicationSettings();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.isStoreSearchVisible = true;
    }
    #endregion
}