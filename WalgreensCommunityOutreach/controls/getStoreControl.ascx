﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="getStoreControl.ascx.cs" Inherits="getStoreControl" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    
    <style type="text/css">
    	.ui-autocomplete-loading { background: white url('images/ui-anim_basic_16x16.gif') right center no-repeat; }
	</style>
<script type="text/javascript" language="javascript">
    var baseURL;
    $(document).ready(function () {
        baseURL = '<%=FilePath%>';
    });

    $(function () {
        function log(selected, storeidvalue) {

            $("#txtStoreId").val(storeidvalue);
            $("#txtStoreProfiles").val(selected);


            $("#btnRefresh").click();

        }
        $("#storeId").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: baseURL, //"search.aspx",
                    dataType: "json",
                    data: {
                        parameter: request.term,
                        type: "storeId"
                    },
                    success: function (data) {
                        $("#txtStoreProfiles").val("Nothing selected, input was " + $('#storeId').val());
                        $("#txtStoreId").val(0);
                        response($.map(data, function (item) {
                            if (item.name == "Number$format" || item.name == "Number$localeFormat" || item.name == "Number$_toFormattedString")
                                return;
                            return {
                                label: item.name,
                                value: item.id
                            }

                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function (event, ui) {
                log(ui.item.label, ui.item.value);
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                if (window.location.pathname.indexOf("walgreensHome.aspx") == -1)
                    disabledropdown();
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                if (window.location.pathname.indexOf("walgreensHome.aspx") == -1)
                    enabledropdown();
            }
        });
    });
</script>
</head>
<body>  
    <div>
    	<input id="storeId" class="formFields" style="width:440px" value="" /><br  /> 
        <span  class="formFields" style="font-size:8pt; color:#57a1d3;">Search by store id, city or state to select a Walgreens store location.</span>          
    </div>   
    </body>
</html>
