﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TdApplicationLib;
using System.Data;
using System.Xml;
using System.Text;
using TdWalgreens;
using NLog;

public partial class DirectB2BCampaigns : System.Web.UI.UserControl
{
    #region ------------------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ddlAssignedStoreDirectB2BMailFilters.bindDataFilterTypes(this.commonAppSession.LoginUserInfoSession.UserRole);
            this.ddlAssignedDistrictDirectB2BMailFilters.bindDataFilterTypes(this.commonAppSession.LoginUserInfoSession.UserRole);
            this.ddlUnassignedDirectB2BMailBusinessFilters.bindDataFilterTypes(this.commonAppSession.LoginUserInfoSession.UserRole);

            this.commonAppSession.SelectedStoreSession.SelectedDirectB2BMailTabId = "UnassignedDirectB2BMail";
            this.commonAppSession.dtDirectB2BBusiness = new DataTable();
            this.getDirectB2BMailBusiness(0);
            this.bindDirectB2BMailBusiness();
        }
        else
        {
            string event_target = Request["__EVENTTARGET"];
            string event_args = Request["__EVENTARGUMENT"];
            if (event_target.ToLower() == "b2bresettohcs")
            {
                this.setStoreChecked("AssignedStore");
                b2bResetToHCS(true);
            }
            else if (event_target.ToLower() == "assignbusinesstootherstate" && event_args.Contains("DirectB2B"))
            {
                string[] args = event_args.Split(',');
                this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;
                this.isStoreUpdated = true;
                this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
                this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;
                this.saveClinicStoreAssignment(args[0].Trim(), Convert.ToInt32(args[1].Trim()));
                this.bindDirectB2BMailBusiness();
            }
        }

        this.displayTabs();
    }

    protected void btnSearchRecords_Click(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString())
        {
            case "UnassignedDirectB2BMail":
                this.getDirectB2BMailBusiness(1, this.txtUnassignedDirectB2BMailBusinessSearch.Text, this.ddlUnassignedDirectB2BMailBusinessFilters.SelectedItem.Value, this.txtUnassignedDirectB2BMailBusinessFilter.Text);
                this.bindUnassignedSearchResult();
                break;
            case "AssignedDistrictDirectB2BMail":
                this.getDirectB2BMailBusiness(2, this.txtAssignedDistrictDirectB2BMailSearch.Text, this.ddlAssignedDistrictDirectB2BMailFilters.SelectedItem.Value, this.txtAssignedDistrictDirectB2BMailFilter.Text);
                this.bindAssignedToDistrictSearchResult();
                break;
            case "AssignedStoreDirectB2BMail":
                this.getDirectB2BMailBusiness(3, this.txtAssignedStoreDirectB2BMailSearch.Text, this.ddlAssignedStoreDirectB2BMailFilters.SelectedItem.Value, this.txtAssignedStoreDirectB2BMailFilters.Text);
                this.bindAssignedSearchResult();
                break;
        }
    }

    protected void btnDirectB2BMailBusiness_Command(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLower())
        {
            case "unassigned":
                this.getDirectB2BMailBusiness(1, this.txtUnassignedDirectB2BMailBusinessSearch.Text, this.ddlUnassignedDirectB2BMailBusinessFilters.SelectedItem.Value, this.txtUnassignedDirectB2BMailBusinessFilter.Text);
                this.bindUnassignedSearchResult();
                break;
            case "assignedtodist":
                this.getDirectB2BMailBusiness(2, this.txtAssignedDistrictDirectB2BMailSearch.Text, this.ddlAssignedDistrictDirectB2BMailFilters.SelectedItem.Value, this.txtAssignedDistrictDirectB2BMailFilter.Text);
                this.bindAssignedToDistrictSearchResult();
                break;
            case "assigned":
                this.getDirectB2BMailBusiness(3, this.txtAssignedStoreDirectB2BMailSearch.Text, this.ddlAssignedStoreDirectB2BMailFilters.SelectedItem.Value, this.txtAssignedStoreDirectB2BMailFilters.Text);
                this.bindAssignedSearchResult();
                break;
        }
    }

    protected void btnApproveAssignment_Click(object sender, CommandEventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "btnApproveAssignment_Click",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        
        this.setStoreChecked(e.CommandArgument.ToString());
        this.updateStoreAssignment(false, this.grdUnassignedDirectB2BMailBusiness, false);

        int return_value = 0;
        string date_time_stamp_message = "";
        XmlDocument national_accounts = this.createUpdatedUnAssignedClinicsXml("send");
        if (national_accounts.SelectSingleNode("//nationalAccount") != null)
        {
            return_value = this.dbOperation.updateB2BStoreAssignment(national_accounts, "send", out date_time_stamp_message);
            if (return_value == 0)
            {
                this.logger.Info("Businesses were successfully assigned to store(s). Sending Emails..");        
                this.emailOperations.sendDirectB2BStoreAssignmentEmail(this.dtDirectB2BMailBusinesses, this.commonAppSession.LoginUserInfoSession.UserID, "store");
                this.logger.Info("Direct B2B Store assignment emails were sent.");
                commonAppSession.dtDirectB2BBusiness = new DataTable();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('DirectB2BMailBusiness'); alert('" + (string)GetGlobalResourceObject("errorMessages", "B2BUpdateStoreAssignment") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "btnApproveAssignment_Click",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        
    }

    protected void btnApproveDistrictAssignment_Click(object sender, CommandEventArgs e)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "btnApproveDistrictAssignment_Click",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        
        this.setStoreChecked(e.CommandArgument.ToString());
        this.updateStoreAssignment(false, this.grdUnassignedDirectB2BMailBusiness, false);
        XmlDocument national_accounts = this.createUpdatedUnAssignedClinicsXml("sendDistrict");

        if (!string.IsNullOrEmpty(national_accounts.InnerXml))
        {
            int return_value = 0;
            string date_time_stamp_message = "";

            return_value = this.dbOperation.updateB2BStoreAssignment(national_accounts, "sendDistrict", out date_time_stamp_message);
            if (return_value == 0)
            {
                this.logger.Info("Businesses were successfully assigned to district(s).");  
                commonAppSession.dtDirectB2BBusiness = new DataTable();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('DirectB2BMailBusiness'); alert('" + (string)GetGlobalResourceObject("errorMessages", "DirectB2BMailUpdateDistrictAssignment") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "btnApproveDistrictAssignment_Click",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    protected void grdUnassignedDirectB2BMailBusiness_RowDataBind(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox chk_all_unassigned = (CheckBox)e.Row.FindControl("chkAllUnassignedDirectB2BMailBusiness");
            if (this.chkSelectAllUnassignedDirectB2BMailBusinessInDistrict.Checked)
                chk_all_unassigned.Checked = true;
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contact_phone = (Label)e.Row.FindControl("lblBusinessPhone");
            if (lbl_contact_phone.Text.Trim().Length == 10)
                lbl_contact_phone.Text = "(" + lbl_contact_phone.Text.Substring(0, 3) + ") " + lbl_contact_phone.Text.Substring(3, 3) + "-" + lbl_contact_phone.Text.Substring(6, 4);

            Label lbl_outreachstatus_id = (Label)e.Row.FindControl("lblOutreachStatusId");
            CheckBox chk_unassigned_business = (CheckBox)e.Row.FindControl("chkUnassignedDirectB2BMailBusiness");
            if (lbl_outreachstatus_id.Text.Trim() == "4" || lbl_outreachstatus_id.Text.Trim() == "5")
                chk_unassigned_business.Enabled = false;

            CheckBox chk_selected_business = (CheckBox)e.Row.FindControl("chkUnassignedDirectB2BMailBusiness");
            if (this.chkSelectAllUnassignedDirectB2BMailBusinessInDistrict.Checked)
                chk_selected_business.Checked = true;
        }
    }

    protected void grdUnassignedDirectB2BMailBusiness_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int status = this.updateStoreAssignment(false, this.grdUnassignedDirectB2BMailBusiness, false);
        this.grdUnassignedDirectB2BMailBusiness.EditIndex = e.NewEditIndex;
        this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;
        this.bindDirectB2BMailBusiness();

        DropDownList ddl_stores = (DropDownList)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("ddlStoreAvailable"));
        Label lbl_storeid = (Label)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("lblStoreId"));
        Label lbl_district_id = (Label)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("lblDistrictId"));
        Label lbl_last_contact = (Label)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("lblLastContactDate"));
        Label lbl_outreach_status = (Label)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("lblOutreachStatusId"));
        Label lbl_business_pk = (Label)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("lblBusinessPk"));
        DropDownList ddl_outreach_status = (DropDownList)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("ddlOutreachStatus"));
        DataTable dt_outreach_status = new DataTable();

        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
        {
            DropDownList ddl_districts = (DropDownList)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("ddlDistrictsAvailable"));
            DataTable dt_districts = this.dbOperation.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, null, null, null);
            ddl_districts.DataSource = dt_districts;
            ddl_districts.DataTextField = dt_districts.Columns[1].ToString();
            ddl_districts.DataValueField = dt_districts.Columns[0].ToString();
            ddl_districts.DataBind();

            if (ddl_districts.Items.FindByValue(lbl_district_id.Text) != null)
                ddl_districts.Items.FindByValue(lbl_district_id.Text).Selected = true;

            dt_outreach_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 10)").CopyToDataTable();
        }
        else
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_districts = (System.Web.UI.HtmlControls.HtmlTableRow)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("rowDistricts"));
            row_districts.Visible = false;
            dt_outreach_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 9)").CopyToDataTable();
        }

        ddl_outreach_status.DataSource = dt_outreach_status;
        ddl_outreach_status.DataTextField = dt_outreach_status.Columns[1].ToString();
        ddl_outreach_status.DataValueField = dt_outreach_status.Columns[0].ToString();
        ddl_outreach_status.DataBind();

        if (!string.IsNullOrEmpty(lbl_outreach_status.Text))
        {
            if (ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text) != null)
                ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text).Selected = true;
        }

        var date_control = (PickerAndCalendar)(this.grdUnassignedDirectB2BMailBusiness.Rows[this.grdUnassignedDirectB2BMailBusiness.EditIndex].FindControl("PickerAndCalendarFrom"));
        if (date_control != null)
        {
            ((PickerAndCalendar)date_control).MaxDate = DateTime.Now.AddDays(1);

            lbl_last_contact.Text = DateTime.Now.ToString("MM/dd/yyyy");
            ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Now;
        }

        this.bindUserStores(ddl_stores, lbl_storeid.Text, lbl_district_id.Text, lbl_business_pk.Text);
    }

    protected void grdUnassignedDirectB2BMailBusiness_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;
        int status = this.updateStoreAssignment(true, this.grdUnassignedDirectB2BMailBusiness, false);
        if (this.isStoreUpdated)
        {
            this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
            this.saveClinicStoreAssignment("unassigned", status);
            this.bindDirectB2BMailBusiness();
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showConfirmationDialog('Store Assignment','assignBusinessToOtherState', ['unassigned', '" + status + "', 'DirectB2B'], '" + (string)GetGlobalResourceObject("errorMessages", "BusinessAssignmentToOtherStateAlert") + "');", true);
    }

    protected void grdUnassignedDirectB2BMailBusiness_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
        this.bindDirectB2BMailBusiness();
    }

    protected void grdUnassignedDirectB2BMailBusiness_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdUnassignedDirectB2BMailBusiness.PageIndex = e.NewPageIndex;
        this.grdAssignedStoreDirectB2BMail.EditIndex = -1;
        this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
        this.bindDirectB2BMailBusiness();
    }

    protected void grdUnassignedDirectB2BMailBusiness_sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.grdUnassignedDirectB2BMailBusiness.EditIndex == -1)
        {
            ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
            this.bindDirectB2BMailBusiness();
        }
        else
        {
            this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
            this.bindDirectB2BMailBusiness();
        }
    }

    protected void grdAssignedDistrictDirectB2BMail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox chk_all_assigned_district_busineses = (CheckBox)e.Row.FindControl("chkSelectAllDirectB2BBusiness");
            if (this.chkSelectAllAssignedDistrictDirectB2BMailInDistrict.Checked)
                chk_all_assigned_district_busineses.Checked = true;
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contact_phone = (Label)e.Row.FindControl("lblBusinessPhone");
            if (lbl_contact_phone.Text.Trim().Length == 10)
                lbl_contact_phone.Text = "(" + lbl_contact_phone.Text.Substring(0, 3) + ") " + lbl_contact_phone.Text.Substring(3, 3) + "-" + lbl_contact_phone.Text.Substring(6, 4);

            Label lbl_outreachstatus_id = (Label)e.Row.FindControl("lblOutreachStatusId");
            CheckBox chk_business_assigned_to_district = (CheckBox)e.Row.FindControl("chkSelectDirectB2BBusiness");
            if (lbl_outreachstatus_id.Text.Trim() == "4" || lbl_outreachstatus_id.Text.Trim() == "5")
                chk_business_assigned_to_district.Enabled = false;

            CheckBox chk_selected_business = (CheckBox)e.Row.FindControl("chkSelectDirectB2BBusiness");
            if (this.chkSelectAllAssignedDistrictDirectB2BMailInDistrict.Checked)
                chk_selected_business.Checked = true;
        }
    }

    protected void grdAssignedDistrictDirectB2BMail_RowEditing(object sender, GridViewEditEventArgs e)
    {
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;
        int status = this.updateStoreAssignment(false, this.grdAssignedDistrictDirectB2BMail, false);
        this.grdAssignedDistrictDirectB2BMail.EditIndex = e.NewEditIndex;
        this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
        this.saveClinicStoreAssignment("direct", status);
        this.bindDirectB2BMailBusiness();

        DropDownList ddl_stores = (DropDownList)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("ddlStoreAvailable"));
        Label lbl_storeid = (Label)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("lblStoreId"));
        Label lbl_district_id = (Label)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("lblDistrictId"));
        Label lbl_last_contact = (Label)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("lblLastContactDate"));
        Label lbl_outreach_status = (Label)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("lblOutreachStatusId"));
        Label lbl_business_pk = (Label)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("lblBusinessPk"));
        DropDownList ddl_outreach_status = (DropDownList)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("ddlOutreachStatus"));
        DataTable dt_status = new DataTable();

        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
        {
            DropDownList ddl_districts = (DropDownList)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("ddlDistrictsAvailable"));
            DataTable dt_districts = this.dbOperation.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, null, null, null);
            ddl_districts.DataSource = dt_districts;
            ddl_districts.DataTextField = dt_districts.Columns[1].ToString();
            ddl_districts.DataValueField = dt_districts.Columns[0].ToString();
            ddl_districts.DataBind();

            if (ddl_districts.Items.FindByValue(lbl_district_id.Text) != null)
                ddl_districts.Items.FindByValue(lbl_district_id.Text).Selected = true;

            dt_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 10)").CopyToDataTable();
        }
        else
        {
            System.Web.UI.HtmlControls.HtmlTableRow row_districts = (System.Web.UI.HtmlControls.HtmlTableRow)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("rowDistricts"));
            row_districts.Visible = false;
            dt_status = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='IP' AND isActive = 'True' AND pk IN (0, 4, 5, 9)").CopyToDataTable();
        }

        ddl_outreach_status.DataSource = dt_status;
        ddl_outreach_status.DataTextField = dt_status.Columns[1].ToString();
        ddl_outreach_status.DataValueField = dt_status.Columns[0].ToString();
        ddl_outreach_status.DataBind();

        if (!string.IsNullOrEmpty(lbl_outreach_status.Text))
        {
            if (ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text) != null)
                ddl_outreach_status.Items.FindByValue(lbl_outreach_status.Text).Selected = true;
        }

        var date_control = (PickerAndCalendar)(this.grdAssignedDistrictDirectB2BMail.Rows[this.grdAssignedDistrictDirectB2BMail.EditIndex].FindControl("PickerAndCalendarFrom"));
        if (date_control != null)
        {
            ((PickerAndCalendar)date_control).MaxDate = DateTime.Now.AddDays(1);

            lbl_last_contact.Text = DateTime.Now.ToString("MM/dd/yyyy");
            ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Now;
        }

        this.bindUserStores(ddl_stores, lbl_storeid.Text, lbl_district_id.Text, lbl_business_pk.Text);
    }

    protected void grdAssignedDistrictDirectB2BMail_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;
        int status = this.updateStoreAssignment(true, this.grdAssignedDistrictDirectB2BMail, false);
        if (this.isStoreUpdated)
        {
            this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;
            this.saveClinicStoreAssignment("direct", status);
            this.bindDirectB2BMailBusiness();
        }
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showConfirmationDialog('Store Assignment','assignBusinessToOtherState', ['direct', '" + status + "', 'DirectB2B'], '" + (string)GetGlobalResourceObject("errorMessages", "BusinessAssignmentToOtherStateAlert") + "');", true);

    }

    protected void grdAssignedDistrictDirectB2BMail_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;
        this.bindDirectB2BMailBusiness();
    }

    protected void grdAssignedStoreDirectB2BMail_sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.grdAssignedStoreDirectB2BMail.EditIndex == -1)
        {
            ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
            this.bindDirectB2BMailBusiness();
        }
        else
        {
            this.grdAssignedStoreDirectB2BMail.EditIndex = -1;
            this.bindDirectB2BMailBusiness();
        }
    }

    protected void grdAssignedStoreDirectB2BMail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdAssignedStoreDirectB2BMail.PageIndex = e.NewPageIndex;
        this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
        this.grdAssignedStoreDirectB2BMail.EditIndex = -1;
        this.bindDirectB2BMailBusiness();
    }

    protected void ddlDistrictsAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl_districts = (DropDownList)sender;
        GridViewRow business_row = (GridViewRow)ddl_districts.NamingContainer;
        DropDownList ddl_available_districts = (DropDownList)business_row.FindControl("ddlDistrictsAvailable");
        DropDownList ddl_available_stores = (DropDownList)business_row.FindControl("ddlStoreAvailable");
        Label lbl_business_pk = (Label)business_row.FindControl("lblBusinessPk");

        //binding date picker calendar
        var date_control = (PickerAndCalendar)(business_row.FindControl("PickerAndCalendarFrom"));
        if (date_control != null)
            ((PickerAndCalendar)date_control).MaxDate = DateTime.Now.AddDays(1);

        this.bindUserStores(ddl_available_stores, string.Empty, ddl_available_districts.SelectedValue.ToString(), lbl_business_pk.Text);
    }

    protected void grdAssignedDistrictDirectB2BMail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grdAssignedDistrictDirectB2BMail.PageIndex = e.NewPageIndex;
        this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;
        this.bindDirectB2BMailBusiness();
    }

    protected void grdAssignedDistrictDirectB2BMail_sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.grdAssignedDistrictDirectB2BMail.EditIndex == -1)
        {
            ViewState["sortOrder"] = e.SortExpression + "" + this.getGridSortDirection(e);
            this.bindDirectB2BMailBusiness();
        }
        else
        {
            this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;
            this.bindDirectB2BMailBusiness();
        }
    }

    private void bindAssignedToDistrictSearchResult()
    {
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;
        DataRow[] dr_filtered_clinics = null;
        DataTable dt_filtered_clinics = new DataTable();
        dt_filtered_clinics = this.dtDirectB2BMailBusinesses.Clone();

        dr_filtered_clinics = this.dtDirectB2BMailBusinesses.Select(this.getFilterExpression(false, "assignedtodistrict"));
        if (dr_filtered_clinics.Count() > 0)
        {
            this.grdAssignedDistrictDirectB2BMail.PageIndex = 0;
            this.btnDistrictAssigned.Visible = true;
            this.btnDistrictApproveAssignment.Visible = true;
            dt_filtered_clinics = dr_filtered_clinics.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && dt_filtered_clinics.Columns.Contains(GridSortExpression))
                dt_filtered_clinics.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_filtered_clinics.DefaultView.Sort = defaultSortExp;
        }
        else
        {
            this.btnDistrictAssigned.Visible = false;
            this.btnDistrictApproveAssignment.Visible = false;

        }

        this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;

        this.grdAssignedDistrictDirectB2BMail.DataSource = dt_filtered_clinics;
        this.grdAssignedDistrictDirectB2BMail.DataBind();
    }

    protected void btnB2BDirectB2BBusinessReset_Click(object sender, ImageClickEventArgs e)
    {
        this.txtAssignedDistrictDirectB2BMailSearch.Text = "";
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness.Select(this.getFilterExpression(false, "assignedtodistrict")).CopyToDataTable();
        if (this.dtDirectB2BMailBusinesses.Rows.Count > 0)
        {
            this.grdAssignedDistrictDirectB2BMail.EditIndex = -1;

            this.grdAssignedDistrictDirectB2BMail.DataSource = this.dtDirectB2BMailBusinesses;
            this.grdAssignedDistrictDirectB2BMail.DataBind();
            this.btnApproveAssignment.Visible = true;
        }
    }

    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList comboBox = (DropDownList)sender;
        CheckBox checkBox;
        switch (comboBox.ID)
        {
            case "ddlAssignedDistrictDirectB2BMailFilters": checkBox = this.chkSelectAllAssignedDistrictDirectB2BMailInDistrict;
                break;
            case "ddlUnassignedDirectB2BMailBusinessFilters": checkBox = this.chkSelectAllUnassignedDirectB2BMailBusinessInDistrict;
                break;
            default: checkBox = this.chkSelectAllUnassignedDirectB2BMailBusinessInDistrict;
                break;
        }
        if (comboBox.SelectedItem.Value == "District")
        {
            checkBox.Visible = true;
        }
        else
        {
            checkBox.Visible = false;
        }
    }

    protected void btnResetToHCS_Command(object sender, CommandEventArgs e)
    {
        this.setStoreChecked(e.CommandArgument.ToString());
        var isHavingAdminOverrideAccess = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "ResetContactedStoreStatus");
        DataRow[] dr_businesses = this.dtDirectB2BMailBusinesses.Select("isStoreConfirmed = 1 AND ISNULL(isChecked, 0) = '1' AND outreachStatusId <> 0");
        if (dr_businesses.Length > 0)
        {
            StringBuilder contacted_Stores = new StringBuilder();
            foreach (DataRow row in dr_businesses)
            {
                contacted_Stores.Append(row["businessName"].ToString() + "(" + row["outreachStatus"].ToString() + "), ");
            }

            if (isHavingAdminOverrideAccess)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showConfirmationDialog('Reset To HCS','b2bResetToHCS','','" + GetGlobalResourceObject("errorMessages", "resetcontactedBusinessAdmin") + "\\n" + contacted_Stores.ToString().Replace("'", "\\'").Trim().TrimEnd(',') + "');", true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + GetGlobalResourceObject("errorMessages", "resetcontactedBusiness").ToString() + "\\n" + contacted_Stores.ToString().Replace("'", "\\'").Trim().TrimEnd(',') + "');", true);
        }
        else
            this.b2bResetToHCS(isHavingAdminOverrideAccess);
    }

    protected void grdAssignedStoreDirectB2BMail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_contact_phone = (Label)e.Row.FindControl("lblBusinessPhone");
            if (lbl_contact_phone.Text.Trim().Length == 10)
                lbl_contact_phone.Text = "(" + lbl_contact_phone.Text.Substring(0, 3) + ") " + lbl_contact_phone.Text.Substring(3, 3) + "-" + lbl_contact_phone.Text.Substring(6, 4);

            Label lbl_can_delete = (Label)e.Row.FindControl("lblCanDelete");
            CheckBox chk_select_assigned_business = (CheckBox)e.Row.FindControl("chkSelectAssignedBusiness");

            if (!string.IsNullOrEmpty(lbl_can_delete.Text) && (Convert.ToInt32(lbl_can_delete.Text) == 0))
                chk_select_assigned_business.Enabled = false;
        }
    }
    #endregion

    #region ------------------- PRIVATE METHODS -------------
    /// <summary>
    /// Displays Direct B2B Mail businesses tabs
    /// </summary>
    private void displayTabs()
    {
        //string business_type = string.Empty;
        StringBuilder sb = new StringBuilder();

        sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='UnassignedDirectB2BMail' style='width: 180px;' href='#idUnassignedDirectB2BMail' runat='server' onclick=setSelectedTab('UnassignedDirectB2BMail')><span style='font-weight:bold'>Unassigned <br />Businesses</span></a></li>");
        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
            sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='AssignedToDistrictDirectB2BMail' style='width: 180px;' href='#idAssignedToDistrictDirectB2BMail' runat='server' onclick=setSelectedTab('AssignedToDistrictDirectB2BMail')><span style='font-weight:bold'>Businesses <br />Assigned to District</span></a></li>");
        sb.Append("<li class='profileTabs' style='width: 200px; height: 38px; '><a id='AssignedToStoresDirectB2BMail' style='width: 180px;' href='#idAssignedToStoresDirectB2BMail' runat='server' onclick=setSelectedTab('AssignedToStoresDirectB2BMail')><span style='font-weight:bold'>Assigned <br />Businesses</span></a></li>");

        this.lblDirectB2BMailBusinessTabs.Text = sb.ToString();
    }

    /// <summary>
    /// Binds user stores to direct B2B mail business
    /// </summary>
    /// <param name="ddl_stores"></param>
    /// <param name="business_storeid"></param>
    /// <param name="business_districtid"></param>
    /// <param name="business_pk"></param>
    private void bindUserStores(DropDownList ddl_stores, string business_storeid, string business_districtid, string business_pk)
    {
        DataTable dt_stores = new DataTable();
        if (!string.IsNullOrEmpty(business_districtid))
            dt_stores = this.dbOperation.getUserAllDistricts(this.commonAppSession.LoginUserInfoSession.UserID, Convert.ToInt32(business_districtid), Convert.ToInt32(business_pk), "Local");
        else
            dt_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);

        if (dt_stores != null && dt_stores.Rows.Count > 0)
        {
            ddl_stores.DataSource = dt_stores;
            ddl_stores.DataTextField = dt_stores.Columns[1].ToString();
            ddl_stores.DataValueField = dt_stores.Columns[0].ToString();
            ddl_stores.DataBind();

            if (ddl_stores.Items.FindByValue(business_storeid) != null)
                ddl_stores.Items.FindByValue(business_storeid).Selected = true;
            else
            {
                string closest_store = ((DataRow)dt_stores.Select("closestClinicRank = 1")[0])["storeId"].ToString();
                if (ddl_stores.Items.FindByValue(closest_store) != null)
                    ddl_stores.Items.FindByValue(closest_store).Selected = true;
            }
        }
    }

    /// <summary>
    /// Gets Direct B2B Mail Campaign businesses
    /// </summary>
    /// <param name="assigned_to"></param>
    /// <param name="search_value"></param>
    /// <param name="location_filter_type"></param>
    /// <param name="location_filter_value"></param>
    private void getDirectB2BMailBusiness(int assigned_to, string search_value = "", string location_filter_type = "", string location_filter_value = "")
    {
        this.logger.Info("Method {0} accessed from tab {1} of {2} page by user {3} - START", "getDirectB2BMailBusiness", assigned_to,
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        this.directB2BMailBusiness = this.dbOperation.getDirectB2BMailBusiness(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole, assigned_to, search_value, location_filter_type, location_filter_value);
        this.logger.Info("Method {0} accessed from tab {1} of {2} page by user {3} - END", "getDirectB2BMailBusiness", assigned_to,
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Binds Direct B2B Mail Campaign Businesses
    /// </summary>
    private void bindDirectB2BMailBusiness()
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "bindDirectB2BMailBusiness",
            System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        
        this.rowUnassignedDirectB2BMailNoRecords.Visible = false;
        this.rowAssignedDistrictDirectB2BMailNoRecords.Visible = false;
        this.rowAssignedStoreDirectB2BMailNoRecords.Visible = false;
        this.rowUnassignedDirectB2BMailBusiness.Visible = true;
        this.rowAssignedDistrictDirectB2BMailBusiness.Visible = true;
        this.rowAssignedStoreDirectB2BMailBusiness.Visible = true;

        this.ltlUnassignedDirectB2BMailNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoUnassignedDirectB2BMailBusinessesAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoUnassignedDirectB2BMailBusinesses"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlAssignedDistrictDirectB2BMailNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoAssignedDistrictDirectB2BMailBusinessAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoAssignedDistrictDirectB2BMailBusiness"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlAssignedStoreDirectB2BMailNoRecords.Text = this.commonAppSession.LoginUserInfoSession.IsAdmin ? (string)GetGlobalResourceObject("errorMessages", "NoAssignedStoreDirectB2BMailBusinessAdmin") : string.Format((string)GetGlobalResourceObject("errorMessages", "NoAssignedStoreDirectB2BMailBusiness"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());

        this.ltlUnassignedDirectB2BMailBusinessNote.Text = string.Format((string)GetGlobalResourceObject("errorMessages", "directB2BMailBusinessUnassignedNote"), this.commonAppSession.LoginUserInfoSession.LocationType.ToLower());
        this.ltlAssignedDistrictDirectB2BMailNote.Text = (string)GetGlobalResourceObject("errorMessages", "directB2BMailBusinessAssignedDistrictNote");
        this.ltlAssignedB2BNote.Text = (string)GetGlobalResourceObject("errorMessages", "directB2BMailBusinessAssignedStoreNote");

        //if (this.directB2BMailBusiness.Rows.Count == 0)
        //{
        //    this.directB2BMailBusiness = this.dbOperation.getDirectB2BMailBusiness(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);
        //}
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;

        DataTable dt_businesses_filtered;
        if (this.dtDirectB2BMailBusinesses.Rows.Count == 0)
        {
            this.rowUnassignedDirectB2BMailNoRecords.Visible = true;
            this.rowAssignedDistrictDirectB2BMailNoRecords.Visible = true;
            this.rowAssignedStoreDirectB2BMailNoRecords.Visible = true;
            this.rowUnassignedDirectB2BMailBusiness.Visible = false;
            this.rowAssignedDistrictDirectB2BMailBusiness.Visible = false;
            this.rowAssignedStoreDirectB2BMailBusiness.Visible = false;
            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                this.rowAssignedDistrictDirectB2BMailNoRecords.Visible = false;
        }
        else
        {
            DataRow[] unassigned_business = this.dtDirectB2BMailBusinesses.Select(this.getFilterExpression(false, "Unassigned"));
            if (unassigned_business.Count() > 0)
            {
                dt_businesses_filtered = new DataTable();
                //dt_businesses_filtered = !Convert.ToBoolean(hdnUADB2BShowMoreThan10.Value) ? unassigned_business.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : unassigned_business.CopyToDataTable();
                dt_businesses_filtered = unassigned_business.CopyToDataTable();
                if ((ViewState["sortOrder"] != null) && (dt_businesses_filtered.Columns.Contains(GridSortExpression)))
                    dt_businesses_filtered.DefaultView.Sort = ViewState["sortOrder"].ToString();
                else
                    dt_businesses_filtered.DefaultView.Sort = defaultSortExp;

                this.grdUnassignedDirectB2BMailBusiness.DataSource = dt_businesses_filtered;
                this.grdUnassignedDirectB2BMailBusiness.DataBind();
                this.logger.Info("Bound {0} Grid", "grdUnassignedDirectB2BMailBusiness");
                if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                {
                    this.btnApproveDistrictAssignment.Visible = false;
                    this.rowAssignedDistrictDirectB2BMailBusiness.Visible = false;
                }
            }
            else
            {
                this.rowUnassignedDirectB2BMailNoRecords.Visible = true;
                this.rowUnassignedDirectB2BMailBusiness.Visible = false;

                if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "district manager")
                {
                    this.rowAssignedDistrictDirectB2BMailNoRecords.Visible = false; 
                    this.rowAssignedDistrictDirectB2BMailBusiness.Visible = false;
                }
            }

            if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager")
            {
                DataRow[] dr_assigned_district_business = this.dtDirectB2BMailBusinesses.Select(this.getFilterExpression(true, "AssignedToDistrict"));
                if (dr_assigned_district_business.Count() > 0)
                {
                    dt_businesses_filtered = new DataTable();
                    //dt_businesses_filtered = !Convert.ToBoolean(hdnADDB2BShowMoreThan10.Value) ? dr_assigned_district_business.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : dr_assigned_district_business.CopyToDataTable();
                    dt_businesses_filtered = dr_assigned_district_business.CopyToDataTable();
                    if ((ViewState["sortOrder"] != null) && (dt_businesses_filtered.Columns.Contains(GridSortExpression)))
                        dt_businesses_filtered.DefaultView.Sort = ViewState["sortOrder"].ToString();
                    else
                        dt_businesses_filtered.DefaultView.Sort = defaultSortExp;

                    this.grdAssignedDistrictDirectB2BMail.DataSource = dt_businesses_filtered;
                    this.grdAssignedDistrictDirectB2BMail.DataBind();
                    this.logger.Info("Bound {0} Grid", "grdAssignedDistrictDirectB2BMail");
                }
                else
                {
                    this.rowAssignedDistrictDirectB2BMailNoRecords.Visible = true;
                    this.rowAssignedDistrictDirectB2BMailBusiness.Visible = false;
                }
            }

            DataRow[] dr_assigned_store_business = this.dtDirectB2BMailBusinesses.Select(this.getFilterExpression(true, "AssignedStore"));
            if (dr_assigned_store_business.Count() > 0)
            {
                this.rowAssignedStoreDirectB2BMailBusiness.Visible = true;
                this.rowAssignedStoreDirectB2BMailNoRecords.Visible = false;
                dt_businesses_filtered = new DataTable();
                //dt_businesses_filtered = !Convert.ToBoolean(hdnADB2BShowMoreThan10.Value) ? dr_assigned_store_business.CopyToDataTable().Rows.Cast<DataRow>().Take(10).CopyToDataTable() : dr_assigned_store_business.CopyToDataTable();
                dt_businesses_filtered = dr_assigned_store_business.CopyToDataTable();
                if ((ViewState["sortOrder"] != null) && (dt_businesses_filtered.Columns.Contains(GridSortExpression)))
                    dt_businesses_filtered.DefaultView.Sort = ViewState["sortOrder"].ToString();
                else
                    dt_businesses_filtered.DefaultView.Sort = defaultSortExp;

                this.grdAssignedStoreDirectB2BMail.DataSource = dt_businesses_filtered;
                this.grdAssignedStoreDirectB2BMail.DataBind();
                this.logger.Info("Bound {0} Grid", "grdAssignedStoreDirectB2BMail");
            }
            else
            {
                btnResetToHCS.Visible = false;
                this.rowAssignedStoreDirectB2BMailBusiness.Visible = false;
                this.rowAssignedStoreDirectB2BMailNoRecords.Visible = true;
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "bindDirectB2BMailBusiness",
            System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Searches unassigned direct B2B mail businesses
    /// </summary>
    private void bindUnassignedSearchResult()
    {
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;
        DataRow[] dr_filtered_clinics = null;
        DataTable dt_filtered_clinics = new DataTable();
        dt_filtered_clinics = this.dtDirectB2BMailBusinesses.Clone();
        dr_filtered_clinics = this.dtDirectB2BMailBusinesses.Select(this.getFilterExpression(false, "Unassigned"));
        
        if (dr_filtered_clinics.Count() > 0)
        {
            this.grdUnassignedDirectB2BMailBusiness.PageIndex = 0;
            this.btnApproveAssignment.Visible = true;
            this.btnApproveDistrictAssignment.Visible = true;

            dt_filtered_clinics = dr_filtered_clinics.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && dt_filtered_clinics.Columns.Contains(GridSortExpression))
                dt_filtered_clinics.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_filtered_clinics.DefaultView.Sort = defaultSortExp;
        }
        else
        {
            this.btnApproveAssignment.Visible = false;
            this.btnApproveDistrictAssignment.Visible = false;
        }

        this.grdUnassignedDirectB2BMailBusiness.EditIndex = -1;
        this.grdUnassignedDirectB2BMailBusiness.DataSource = dt_filtered_clinics;
        this.grdUnassignedDirectB2BMailBusiness.DataBind();
    }

    /// <summary>
    /// Searches assigned direct B2B mail businesses
    /// </summary>
    private void bindAssignedSearchResult()
    {
        this.dtDirectB2BMailBusinesses = this.directB2BMailBusiness;
        DataRow[] dr_filtered_clinics = null;
        DataTable dt_filtered_clinics = new DataTable();
        dt_filtered_clinics = this.dtDirectB2BMailBusinesses.Clone();
        dr_filtered_clinics = this.dtDirectB2BMailBusinesses.Select(this.getFilterExpression(true, "AssignedStore"));

        if (dr_filtered_clinics.Count() > 0)
        {
            btnResetToHCS.Visible = true;
            this.grdAssignedStoreDirectB2BMail.PageIndex = 0;
            dt_filtered_clinics = dr_filtered_clinics.CopyToDataTable();
            if ((ViewState["sortOrder"] != null) && (dt_filtered_clinics.Columns.Contains(GridSortExpression)))
                dt_filtered_clinics.DefaultView.Sort = ViewState["sortOrder"].ToString();
            else
                dt_filtered_clinics.DefaultView.Sort = defaultSortExp;
        }
        else
        {
            btnResetToHCS.Visible = false;
        }

        this.grdAssignedStoreDirectB2BMail.EditIndex = -1;
        this.grdAssignedStoreDirectB2BMail.DataSource = dt_filtered_clinics;
        this.grdAssignedStoreDirectB2BMail.DataBind();
    }

    /// <summary>
    /// Binds scheduled clinic outreach statuses
    /// </summary>
    /// <param name="ddl_outreach_status"></param>
    private void bindOutreachStatuses(DropDownList ddl_outreach_status)
    {
        ddl_outreach_status.DataSource = this.commonAppSession.LoginUserInfoSession.OutreachStatuses.Select("category='AC' AND isActive = 'True'").CopyToDataTable();
        ddl_outreach_status.DataTextField = "outreachStatus";
        ddl_outreach_status.DataValueField = "pk";
        ddl_outreach_status.DataBind();
        ddl_outreach_status.Items.RemoveAt(0);
        ddl_outreach_status.Items.RemoveAt(1);
        ddl_outreach_status.Items.Insert(0, new ListItem("-- Select Outreach Status --", "0"));
    }

    /// <summary>
    /// Sets worksite clinics as selected
    /// </summary>
    /// <param name="grd_source_key"></param>
    private void setStoreChecked(string grd_source_key)
    {
        this.dtDirectB2BMailBusinesses = null;
        this.dtDirectB2BMailBusinesses = grd_source_key == "AssignedStore" ? this.directB2BMailBusiness.Select("isStoreConfirmed = 1").CopyToDataTable() : this.directB2BMailBusiness.Select("isStoreConfirmed = 0").CopyToDataTable();
        int business_pk = 0;
        GridView grd_source = grd_source_key == "AssignedStore" ? this.grdAssignedStoreDirectB2BMail : (grd_source_key == "AssignedDistrict" ? this.grdAssignedDistrictDirectB2BMail : this.grdUnassignedDirectB2BMailBusiness);
        var is_all_districts_checked = grd_source_key == "AssignedStore" ? false : (grd_source_key == "AssignedDistrict" ? this.chkSelectAllAssignedDistrictDirectB2BMailInDistrict.Checked : this.chkSelectAllUnassignedDirectB2BMailBusinessInDistrict.Checked);
        if (!is_all_districts_checked)
        {
            foreach (GridViewRow row in grd_source.Rows)
            {
                Int32.TryParse(((Label)(row.FindControl("lblBusinessPk"))).Text.Trim(), out business_pk);

                if (business_pk > 0)
                {
                    DataRow[] row_clinic = this.dtDirectB2BMailBusinesses.Select("businessPk = " + business_pk);
                    if (row_clinic.Count() > 0)
                    {
                        CheckBox chk_box_selected_row = grd_source_key == "AssignedStore" ? (CheckBox)(row.FindControl("chkSelectAssignedBusiness")) : (grd_source_key == "AssignedDistrict" ? (CheckBox)(row.FindControl("chkSelectDirectB2BBusiness")) : (CheckBox)(row.FindControl("chkUnassignedDirectB2BMailBusiness")));
                        row_clinic.ElementAt(0)["isChecked"] = is_all_districts_checked ? 1 : (chk_box_selected_row.Checked ? 1 : 0);
                    }
                }
            }
        }
        else
        {
            this.dtDirectB2BMailBusinesses = null;
            DataRow[] drDirectB2BMailBusinesses = grd_source_key == "AssignedDistrict" ? this.directB2BMailBusiness.Select(this.getFilterExpression(true, "AssignedToDistrict")) : this.directB2BMailBusiness.Select(this.getFilterExpression(false, "UnAssigned"));
            if (drDirectB2BMailBusinesses.Count() > 0)
            {
                this.dtDirectB2BMailBusinesses = drDirectB2BMailBusinesses.CopyToDataTable();
                foreach (DataRow dr_direct_b2b in dtDirectB2BMailBusinesses.Rows)
                {
                    if (dr_direct_b2b["outreachStatusId"].ToString() != "4" && dr_direct_b2b["outreachStatusId"].ToString() != "5")
                        dr_direct_b2b["isChecked"] = 1;
                }
            }
        }
    }

    /// <summary>
    /// Updates store assignment to the selected store
    /// </summary>
    /// <param name="is_edited"></param>
    /// <param name="grd_corporate_clinics"></param>
    /// <param name="is_approved_clinics"></param>
    /// <returns></returns>
    private int updateStoreAssignment(bool is_edited, GridView grd_b2b_business, bool is_approved_clinics)
    {
        int business_pk = 0, updated_storeid = 0, updated_districtid;
        DropDownList ddl_selected_store = new DropDownList();
        DropDownList ddl_selected_district = new DropDownList();
        DropDownList ddl_selected_status = new DropDownList();

        //Label lbl_current_lead_storeid = new Label();
        Label lbl_business_pk = new Label();
        Label lbl_last_contact_date = new Label();
        Label lbl_last_outreach_status = new Label();

        if (is_edited)
        {
            ddl_selected_store = (DropDownList)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("ddlStoreAvailable"));
            ddl_selected_district = (DropDownList)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("ddlDistrictsAvailable"));
            ddl_selected_status = (DropDownList)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("ddlOutreachStatus"));
            DateTime seleted_date = ((PickerAndCalendar)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("PickerAndCalendarFrom"))).getSelectedDate;
            lbl_last_contact_date.Text = seleted_date.ToString("MM/dd/yyyy");

            lbl_business_pk = (Label)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("lblBusinessPk"));
            lbl_last_outreach_status = (Label)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("lblOutreachStatusId"));
        }
        else
        {
            foreach (GridViewRow row in grd_b2b_business.Rows)
            {
                if (row.RowState.ToString().Contains("Edit"))
                {
                    ddl_selected_store = (DropDownList)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("ddlStoreAvailable"));
                    lbl_business_pk = (Label)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("lblBusinessPk"));
                    lbl_last_contact_date = (Label)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("lblLastContactDate"));
                    lbl_last_outreach_status = (Label)(grd_b2b_business.Rows[grd_b2b_business.EditIndex].FindControl("lblOutreachStatusId"));
                }
            }
        }

        Int32.TryParse(lbl_business_pk.Text, out business_pk);
        Int32.TryParse(ddl_selected_store.SelectedValue, out updated_storeid);
        Int32.TryParse(ddl_selected_district.SelectedValue, out updated_districtid);

        if (this.dtDirectB2BMailBusinesses.Rows.Count > 0)
        {
            DataRow[] update_row = this.dtDirectB2BMailBusinesses.Select("businessPk = '" + business_pk + "'");
            if (updated_storeid != 0)
            {
                
                update_row[0]["businessPk"] = business_pk;
                

                DataTable dt_stores = new DataTable();
                if (ViewState["dtStores"] != null && !this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    dt_stores = (DataTable)ViewState["dtStores"];
                else
                    dt_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);

                string business_state = update_row[0]["businessAddress"].ToString().Split(',').Last().Trim().Substring(0, 2);
                string store_state = ddl_selected_store.SelectedItem.Text.Split(',').Last().Trim().Substring(0, 2);
                if (Convert.ToInt32(update_row[0]["storeId"]) != updated_storeid && business_state != store_state)
                {
                    this.isStoreUpdated = false;
                }
                if (ddl_selected_status.SelectedValue.Length > 0 && (Convert.ToInt32(ddl_selected_status.SelectedValue) != Convert.ToInt32(lbl_last_outreach_status.Text)))
                {
                    update_row[0]["lastContactDate"] = (lbl_last_contact_date.Text);
                    update_row[0]["isStoreUpdated"] = "1";
                    update_row[0]["storeId"] = updated_storeid;
                    update_row[0]["districtId"] = (updated_districtid);
                }
                else if (Convert.ToInt32(update_row[0]["storeId"]) != updated_storeid)
                {
                    
                    update_row[0]["storeId"] = updated_storeid;
                    update_row[0]["districtId"] = (updated_districtid);
                    update_row[0]["isStoreUpdated"] = "1";
                   
                }                
            }

        }
        return (ddl_selected_status.SelectedValue.Length > 0 ? Convert.ToInt32(ddl_selected_status.SelectedValue) : 0);
    }

    /// <summary>
    /// Creates corporate unassigned clinics xml document for updating and sending store assignment
    /// </summary>
    /// <param name="update_type"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private XmlDocument createUpdatedUnAssignedClinicsXml(string update_type, int status = 0)
    {
        XmlDocument national_accounts = new XmlDocument();
        XmlElement national_account_ele = national_accounts.CreateElement("nationalAccounts");
        XmlElement national_account = null;

        if (this.dtDirectB2BMailBusinesses.Rows.Count > 0)
        {
            DataRow[] dr_clinics = null;
            if (update_type == "update")
                dr_clinics = this.dtDirectB2BMailBusinesses.Select("(isStoreConfirmed = 0 AND ISNULL(isStoreUpdated, 0) = '1')");
            else
                dr_clinics = this.dtDirectB2BMailBusinesses.Select("isStoreConfirmed = 0 AND ISNULL(isChecked, 0) = '1'");

            if (dr_clinics.Length > 0)
            {
                foreach (DataRow row in dr_clinics)
                {
                    national_account = national_accounts.CreateElement("nationalAccount");

                    national_account.SetAttribute("businessPk", row["businessPk"].ToString());
                    national_account.SetAttribute("storeId", row["storeId"].ToString());
                    national_account.SetAttribute("isStoreUpdated", row["isStoreUpdated"].ToString());
                    national_account.SetAttribute("lastContactDate", row["lastContactDate"].ToString());
                    national_account.SetAttribute("status", status.ToString());
                    national_account.SetAttribute("createdBy", this.commonAppSession.LoginUserInfoSession.UserID.ToString());
                    national_account_ele.AppendChild(national_account);
                    national_accounts.AppendChild(national_account_ele);
                }
            }
        }

        return national_accounts;
    }

    /// <summary>
    /// Creates corporate unassigned clinics xml document for updating and sending store assignment
    /// </summary>
    /// <param name="update_type"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    private XmlDocument createUpdatedAssignedClinicsXml(string update_type, int status = 0)
    {
        XmlDocument national_accounts = new XmlDocument();
        XmlElement national_account_ele = national_accounts.CreateElement("nationalAccounts");
        XmlElement national_account = null;

        if (this.dtDirectB2BMailBusinesses.Rows.Count > 0)
        {
            DataRow[] dr_clinics = null;
            if (update_type == "assigned")
                dr_clinics = this.dtDirectB2BMailBusinesses.Select("(isStoreConfirmed = 1 AND ISNULL(isStoreUpdated, 0) = '1')");
            else if (update_type == "direct")
                dr_clinics = this.dtDirectB2BMailBusinesses.Select("(isDistrictConfirmed = 1 AND ISNULL(isStoreUpdated, 0) = '1')");
            else if (update_type == "revert")
                dr_clinics = this.dtDirectB2BMailBusinesses.Select("isStoreConfirmed = 1 AND ISNULL(isChecked, 0) = '1' AND outreachStatusId=0");
            else
                dr_clinics = this.dtDirectB2BMailBusinesses.Select("isStoreConfirmed = 1 AND ISNULL(isChecked, 0) = '1'");

            if (dr_clinics.Length > 0)
            {
                foreach (DataRow row in dr_clinics)
                {
                    national_account = national_accounts.CreateElement("nationalAccount");

                    national_account.SetAttribute("businessPk", row["businessPk"].ToString());
                    national_account.SetAttribute("storeId", row["storeId"].ToString());
                    national_account.SetAttribute("isStoreUpdated", row["isStoreUpdated"].ToString());
                    national_account.SetAttribute("lastContactDate", ((update_type == "direct") ? row["lastContactDate"].ToString() : ""));
                    national_account.SetAttribute("status", ((update_type == "direct" || update_type.Contains("revert")) ? status.ToString() : ""));
                    national_account.SetAttribute("createdBy", this.commonAppSession.LoginUserInfoSession.UserID.ToString());

                    national_account_ele.AppendChild(national_account);
                    national_accounts.AppendChild(national_account_ele);
                }
            }
        }

        return national_accounts;
    }

    /// <summary>
    /// Saves clinic store assignment
    /// </summary>
    /// <param name="target"></param>
    /// <param name="status"></param>
    private void saveClinicStoreAssignment(string target, int status = 0)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "saveClinicStoreAssignment",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        
        XmlDocument national_accounts = null;
        if (target.Trim().ToLower() == "unassigned")
            national_accounts = this.createUpdatedUnAssignedClinicsXml("update", status);
        else
            national_accounts = this.createUpdatedAssignedClinicsXml(target, status);

        if (national_accounts.SelectSingleNode("//nationalAccount") != null)
        {
            int return_value = 0;
            string date_time_stamp_message = "";
            return_value = this.dbOperation.updateB2BStoreAssignment(national_accounts, "update", out date_time_stamp_message);

            if (return_value == 0)
            {
                this.logger.Info("Store Assignment saved successfully.");
                this.directB2BMailBusiness = new DataTable();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('DirectB2BMailBusiness'); alert('" + (string)GetGlobalResourceObject("errorMessages", "DirectB2BMailBusinessStoreChanged") + "');", true);
            }
            else if (return_value == -4 && !String.IsNullOrEmpty(date_time_stamp_message))
                this.directB2BMailBusiness = new DataTable();
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "saveClinicStoreAssignment",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }

    /// <summary>
    /// Get data filter query for Unassigned and Assigned B2B businesses
    /// </summary>
    /// <param name="is_store_confirmed"></param>
    /// <param name="assignment_type"></param>
    /// <returns></returns>
    private string getFilterExpression(bool is_store_confirmed, string assignment_type)
    {
        string str_filter = string.Empty;
        string business_search_key = string.Empty;

        string filter_query;
        if (assignment_type.ToLower() == "unassigned")
        {
            filter_query = "isStoreConfirmed = " + Convert.ToInt32(is_store_confirmed).ToString() + (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() != "district manager" ? " AND isDistrictConfirmed = 0" : " AND isDistrictConfirmed = 1");
            if (this.ddlUnassignedDirectB2BMailBusinessFilters.SelectedItem != null && this.ddlUnassignedDirectB2BMailBusinessFilters.SelectedItem.Value != "")
            {
                if (this.txtUnassignedDirectB2BMailBusinessFilter.Text != "")
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlUnassignedDirectB2BMailBusinessFilters, this.txtUnassignedDirectB2BMailBusinessFilter);
                }
            }
            str_filter = this.txtUnassignedDirectB2BMailBusinessSearch.Text;
            business_search_key = this.txtUnassignedDirectB2BMailBusinessSearch.Text.Replace("'", "''");
        }
        else if (assignment_type.ToLower() == "assignedtodistrict")
        {
            filter_query = "isDistrictConfirmed = 1 AND isStoreConfirmed = 0";
            if (this.ddlAssignedDistrictDirectB2BMailFilters.SelectedItem != null && this.ddlAssignedDistrictDirectB2BMailFilters.SelectedItem.Value != "")
            {
                if (this.txtAssignedDistrictDirectB2BMailFilter.Text != "")
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlAssignedDistrictDirectB2BMailFilters, this.txtAssignedDistrictDirectB2BMailFilter);
                }
            }
            str_filter = this.txtAssignedDistrictDirectB2BMailSearch.Text;
            business_search_key = this.txtAssignedDistrictDirectB2BMailSearch.Text.Replace("'", "''");
        }
        else
        {
            filter_query = "isDistrictConfirmed = 1 AND isStoreConfirmed = 1";
            if (this.ddlAssignedStoreDirectB2BMailFilters.SelectedItem != null && this.ddlAssignedStoreDirectB2BMailFilters.SelectedItem.Value != "")
            {
                if (this.txtAssignedStoreDirectB2BMailFilters.Text != "")
                {
                    filter_query += appSettings.getLocationFilterExpression(this.ddlAssignedStoreDirectB2BMailFilters, this.txtAssignedStoreDirectB2BMailFilters);
                }
            }
            str_filter = this.txtAssignedStoreDirectB2BMailSearch.Text;
            business_search_key = this.txtAssignedStoreDirectB2BMailSearch.Text.Replace("'", "''");
        }

        business_search_key = business_search_key.Replace("[", "[[]");
        business_search_key = business_search_key.Replace("*", "[*]");
        str_filter = str_filter.Replace("'", "''");
        str_filter = str_filter.Replace("[", "[[]");
        str_filter = str_filter.Replace("*", "[*]");

        if (!string.IsNullOrEmpty(str_filter))
        {
            filter_query += " AND (businessName like '%" + str_filter + "%'" +
                            " OR businessAddress like '%" + business_search_key + "%'" +
                            " OR CONVERT(actualLocationEmploymentSize, System.String) like '%" + business_search_key + "%'" +
                //" OR lastContact like '%" + clinic_search_key + "%'" +
                            " OR  CONVERT(districtId, System.String) like '%" + business_search_key + "%'" +
                            " OR  CONVERT(storeId, System.String) like '%" + business_search_key + "%'" +
                            ")";
        }

        return filter_query;
    }

    /// <summary>
    /// Grid data sorting expression
    /// </summary>
    private string GridSortExpression
    {
        get
        {
            string sort_expression = string.Empty;
            string[] sort_by_type = new string[1];

            if (ViewState["sortOrder"] != null)
            {
                sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
                sort_expression = sort_by_type[0].Trim();
            }

            return sort_expression;
        }
    }

    /// <summary>
    /// Sorting the grid
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private string getGridSortDirection(GridViewSortEventArgs e)
    {
        string sort_direction = string.Empty;
        string[] sort_by_type = new string[1];

        if (ViewState["sortOrder"] != null)
        {
            sort_by_type = ViewState["sortOrder"].ToString().Replace(" ", "-").Split('-');
            if (sort_by_type[1].ToLower() == "desc")
                sort_direction = " ASC";
            else
                sort_direction = " DESC";
        }
        else
            sort_direction = " ASC";

        return sort_direction;
    }

    /// <summary>
    /// Reset B2B Mailing business assigend to Store to HCS
    /// </summary>
    /// <param name="isHavingAdminOverrideAccess"></param>
    private void b2bResetToHCS(bool isHavingAdminOverrideAccess)
    {
        this.logger.Info("Method {0} accessed from {1} page by user {2} - START", "b2bResetToHCS",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
        int return_value = 0;
        string date_time_stamp_message = "";
        XmlDocument national_accounts = isHavingAdminOverrideAccess ? this.createUpdatedAssignedClinicsXml("revert_with_status", 0) : this.createUpdatedAssignedClinicsXml("revert", 0);
        if (national_accounts.SelectSingleNode("//nationalAccount") != null)
        {
            return_value = this.dbOperation.updateB2BStoreAssignment(national_accounts, "revert", out date_time_stamp_message);
            if (return_value == 0)
            {
                this.logger.Info("Assigned business was reset to HCS succesfully");
                commonAppSession.dtDirectB2BBusiness = new DataTable();
                this.directB2BMailBusiness = new DataTable();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "setTabs('DirectB2BMailBusiness'); alert('" + GetGlobalResourceObject("errorMessages", "resetToHCSConfirmation") + "'); window.location.href = 'walgreensDistrictUsersHome.aspx';", true);
            }
        }
        this.logger.Info("Method {0} accessed from {1} page by user {2} - END", "b2bResetToHCS",
                    System.IO.Path.GetFileNameWithoutExtension(Request.Path), this.commonAppSession.LoginUserInfoSession.UserName);
    }
    #endregion

    #region ------------------- PRIVATE VARIABLES -----------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private EmailOperations emailOperations = null;
    private ApplicationSettings appSettings = null;
    private DataTable dtDirectB2BMailBusinesses = null;
    private Logger logger = LogManager.GetCurrentClassLogger();

    private DataTable directB2BMailBusiness
    {
        get
        {
            //if (this.commonAppSession.dtDirectB2BBusiness.Rows.Count == 0)
            //{
            //    this.commonAppSession.dtDirectB2BBusiness = this.dbOperation.getDirectB2BMailBusiness(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.LoginUserInfoSession.UserRole);

            //}
            return this.commonAppSession.dtDirectB2BBusiness;
        }
        set
        {
            this.commonAppSession.dtDirectB2BBusiness = value;
        }
    }
    private bool isStoreUpdated
    {
        get
        {
            if (ViewState["isStoreUpdated"] != null)
            {
                return Convert.ToBoolean(ViewState["isStoreUpdated"].ToString());

            }
            return true;
        }
        set
        {
            ViewState["isStoreUpdated"] = value;
        }
    }

    private string defaultSortExp = "[businessRank],[businessPk],[districtId],[storeId]";
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        emailOperations = new EmailOperations();
        this.appSettings = new ApplicationSettings();
        this.dtDirectB2BMailBusinesses = new DataTable();
    }
    #endregion
}