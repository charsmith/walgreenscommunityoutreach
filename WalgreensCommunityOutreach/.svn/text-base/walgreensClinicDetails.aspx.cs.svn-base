﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using TdApplicationLib;
using tdEmailLib;

public partial class walgreensClinicDetails : System.Web.UI.Page
{
    #region ----------------- PROTECTED EVENTS -----------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bindStates();
            if (this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk > 0)
            {
                this.bindCoverageTypes();
                this.bindClinicDetails(this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk);
                this.displayControlsBasedOnUserTypes();
            }
            else
                Response.Redirect("walgreensHome.aspx", false);
        }
        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
    }

    protected void doProcess(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLower())
        {
            case "submit":
                string clinic_date = (this.dtClinicDate.getSelectedDate != DateTime.MinValue) ? this.dtClinicDate.getSelectedDate.ToShortDateString() : string.Empty;
                Page.Validate();
                if (Page.IsValid && !string.IsNullOrEmpty(clinic_date) && this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk > 0)
                {
                    XmlDocument xml_doc = this.prepareXmlDocument(this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk);

                    XDocument new_clinic_details_xdoc = new XDocument();
                    XDocument old_clinic_details_xdoc = new XDocument();
                    new_clinic_details_xdoc = XDocument.Parse(xml_doc.OuterXml);
                    old_clinic_details_xdoc = XDocument.Parse(((XmlDocument)Session["prevClinicDetails"]).OuterXml);

                    if (!(XDocument.DeepEquals(new_clinic_details_xdoc, old_clinic_details_xdoc)))
                    {
                        string email_body = this.compareClinicDetails(new_clinic_details_xdoc, old_clinic_details_xdoc);
                        string pharmacy_manager_email, store_manager_email, district_lead_email, subject_line, email_to, email_cc, salutation;

                        this.dbOperation.updateClinicDetails(this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk, xml_doc);
                        DataTable dt_users1 = dbOperation.getStoreUserEmails(this.commonAppSession.LoginUserInfoSession.UserID, this.commonAppSession.SelectedStoreSession.storeID);
                        
                        if (dt_users1.Rows.Count > 0)
                        {
                            store_manager_email = ConfigurationManager.AppSettings["emailSendTo"].ToString() != "" ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_users1.Rows[0]["storeManagerEmail"].ToString();
                            pharmacy_manager_email = ConfigurationManager.AppSettings["emailSendTo"].ToString() != "" ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_users1.Rows[0]["pharmacyManagerEmail"].ToString();
                            district_lead_email = ConfigurationManager.AppSettings["emailSendTo"].ToString() != "" ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : dt_users1.Rows[0]["districtLead"].ToString();

                            email_to = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? pharmacy_manager_email : store_manager_email;
                            salutation = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "Pharmacy Manager" : "Store Manager";
                            email_cc = (!string.IsNullOrEmpty(pharmacy_manager_email)) ? "," + pharmacy_manager_email : "";
                            email_cc = (!string.IsNullOrEmpty(district_lead_email)) ? email_cc + "," + district_lead_email : email_cc;

                            if (this.walgreensEmail != null && !string.IsNullOrEmpty(email_to))
                            {
                                ApplicationSettings app_settings = new ApplicationSettings();
                                subject_line = "Clinic Details Have Changed for the " + app_settings.removeLineBreaks(this.lblClientName.Text) + " Clinic";
                                this.walgreensEmail.sendClinicDetailsChangesEmail(salutation, ApplicationSettings.encryptedLinkWithFourArgs(pharmacy_manager_email, "walgreensLandingPage.aspx", "walgreensClinicDetails.aspx", this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk.ToString()), email_body, app_settings.removeLineBreaks(this.lblClientName.Text), Server.MapPath("clinicDetailsChangesTemplate.htm"), subject_line, email_to, email_cc.TrimStart(','), true);
                            }
                        }
                        this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
                        Session.Remove("prevClinicDetails");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                    }
                }
                break;
            case "cancel":
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
                Session.Remove("prevClinicDetails");
                Response.Redirect("~/walgreensHome.aspx");
                break;
        }
    }

    protected void ValidateCompanyPhoneFax(object sender, ServerValidateEventArgs e)
    {
        string value = e.Value.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();

        string regex_pattern_1 = @"^[0-9]+$";
        e.IsValid = false;
        if (!Regex.IsMatch(value, regex_pattern_1)) return;
        if (string.IsNullOrEmpty(value)) return;
        e.IsValid = Convert.ToInt64(value) > 0 && value.Length == 10;
    }

    protected void validateZipCode(object sender, ServerValidateEventArgs e)
    {
        string value = e.Value;

        string regex_pattern_1 = @"^[0-9]+$";
        e.IsValid = false;
        if (!Regex.IsMatch(value, regex_pattern_1)) return;
        if (string.IsNullOrEmpty(value)) return;
        e.IsValid = (Convert.ToInt32(value) > 0 && value.Length == 5);
    }
    #endregion

    #region --------- PRIVATE METHODS--------------
    /// <summary>
    /// This method will be used to prepare xml document
    /// </summary>
    /// <param name="business_pk"></param>
    /// <returns></returns>
    private XmlDocument prepareXmlDocument(int business_pk)
    {
        string phoneNum = string.Empty;
        if (!string.IsNullOrEmpty(this.txtContactPhone.Text))
            phoneNum = this.txtContactPhone.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
        bool is_admin_or_poweruser;
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin || this.commonAppSession.LoginUserInfoSession.IsPowerUser)
            is_admin_or_poweruser = true;
        else
            is_admin_or_poweruser = false;

        XmlDocument clinic_details = new XmlDocument();
        XmlElement clinic_details_ele = clinic_details.CreateElement("clinicDetails");
        XmlElement client_info = clinic_details.CreateElement("clientInformation");
        client_info.SetAttribute("clientName", this.lblClientName.Text.Trim());
        client_info.SetAttribute("contactPhone", (is_admin_or_poweruser ? phoneNum : this.lblContactPhone.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim()));
        client_info.SetAttribute("contactFirstName", (is_admin_or_poweruser ? this.txtFirstContactName.Text.Trim() : this.lblFirstContactName.Text.Trim()));
        client_info.SetAttribute("contactLastName", (is_admin_or_poweruser ? this.txtLastContactName.Text.Trim() : this.lblLastContactName.Text.Trim()));
        client_info.SetAttribute("contactEmail", (is_admin_or_poweruser ? this.txtContactEmail.Text.Trim() : this.lblContactEmail.Text.Trim()));
        //client_info.SetAttribute("totalEmployeeCount", this.txtTotalEmpCount.Text.Trim());
        client_info.SetAttribute("salesContactName", (is_admin_or_poweruser ? this.txtSalesContactName.Text.Trim() : this.lblSalesContactName.Text.Trim()));
        client_info.SetAttribute("salesContactEmail", (is_admin_or_poweruser ? this.txtSalesContactEmail.Text.Trim() : this.lblSalesContactEmail.Text.Trim()));
        client_info.SetAttribute("canPharmacyReachout", this.ddlCanReachSites.SelectedValue);
        clinic_details_ele.AppendChild(client_info);

        XmlElement clinic_info = clinic_details.CreateElement("clinicInformation");
        clinic_info.SetAttribute("naClinicLocation", this.txtClinicLocation.Text.Trim());
        clinic_info.SetAttribute("naClinicDate", this.dtClinicDate.getSelectedDate.ToString("MM/dd/yyyy"));
        clinic_info.SetAttribute("address", this.txtClinicAddress1.Text.Trim());
        clinic_info.SetAttribute("naClinicStartTime", this.txtClinicStartTime.Text.Trim());
        clinic_info.SetAttribute("address2", this.txtClinicAddress2.Text.Trim());
        clinic_info.SetAttribute("naClinicEndTime", this.txtClinicEndTime.Text.Trim());
        clinic_info.SetAttribute("city", this.txtClinicCity.Text.Trim());
        if (this.txtEstimateVolume.Text.IndexOf('-') == 0) { this.txtEstimateVolume.Text=this.txtEstimateVolume.Text.Replace("-", ""); }
        clinic_info.SetAttribute("naClinicEstimatedVolume", this.txtEstimateVolume.Text.Trim().Replace("'", "''"));
        clinic_info.SetAttribute("state", ddlClinicState.SelectedValue.Trim());
        clinic_info.SetAttribute("naClinicVaccinesCovered", (is_admin_or_poweruser ? this.txtVaccinesCovered.Text.Trim() : this.lblVaccinesCovered.Text.Trim()));
        clinic_info.SetAttribute("zip", txtClinicZip.Text.Trim());
        clinic_info.SetAttribute("naClinicCoverageType", this.ddlCoverageType.SelectedValue.Trim());
        clinic_info.SetAttribute("naClinicPlanId", (is_admin_or_poweruser ? this.txtPlanId.Text.Trim() : this.lblPlanId.Text.Trim()));
        this.lblCopay.Text = this.lblCopay.Text.Trim() == string.Empty ? "0" : this.lblCopay.Text;
        clinic_info.SetAttribute("naClinicCopay", (is_admin_or_poweruser ? this.txtCopay.Text.Trim().Replace("'", "''") : this.lblCopay.Text.Trim()));
        clinic_info.SetAttribute("naClinicGroupId", (is_admin_or_poweruser ? this.txtGroupId.Text.Trim() : this.lblGroupId.Text.Trim()));
        clinic_info.SetAttribute("naClinicAddlComments", txtAddlComments.Text.Trim().Replace("'", "''"));
        clinic_info.SetAttribute("naClinicSplBillingInstr", this.txtSpecialInstr.Text.Trim().Replace("'", "''"));
        clinic_details_ele.AppendChild(clinic_info);

        XmlElement post_clinic_info = clinic_details.CreateElement("postClinicInformation");
        post_clinic_info.SetAttribute("naClinicTotalImmAdministered", (is_admin_or_poweruser ? this.txtTotalImm.Text.Trim() : this.lblTotalImm.Text.Trim()));
        clinic_details_ele.AppendChild(post_clinic_info);

        clinic_details.AppendChild(clinic_details_ele);
        return clinic_details;
    }

    /// <summary>
    /// Compares previous and current clinic details for updates
    /// </summary>
    /// <param name="new_clinic_details_xml"></param>
    /// <param name="old_clinic_details_xml"></param>
    /// <returns></returns>
    private string compareClinicDetails(XDocument new_clinic_details_xml, XDocument old_clinic_details_xml)
    {
        Dictionary<string, string> dic_labels = getClinicDetails();
        XmlDocument xdoc = new XmlDocument();
        XmlDocument xdoc1 = new XmlDocument();

        string str_output = "<table style='font-family: Arial, Helvetica, sans-serif;font-size: 12px;color: #000000;'>";

        xdoc1.LoadXml(new_clinic_details_xml.ToString());
        xdoc.LoadXml(old_clinic_details_xml.ToString());


        foreach (XmlNode xn in xdoc)
        {
            XmlNodeList x = ((XmlElement)xn).ChildNodes;

            for (int j = 0; j < x.Count; j++)
            {
                XmlNodeList xNodeOld = xdoc.SelectNodes("clinicDetails/" + x[j].Name);
                XmlNodeList xNodeNew = xdoc1.SelectNodes("clinicDetails/" + x[j].Name);

                for (int k = 0; k < xNodeOld.Count; k++)
                {
                    XmlAttributeCollection attrColl = xNodeOld[k].Attributes;
                    XmlAttributeCollection attrCol2 = xNodeNew[k].Attributes;
                    for (int i = 0; i < attrColl.Count; i++)
                    {
                        if (!(attrColl[i].Value.Equals(attrCol2[i].Value.ToString()) && attrColl[i].Name.Equals(attrCol2[i].Name.ToString())))
                        {
                            switch (attrCol2[i].Name)
                            {
                                case "canPharmacyReachout":
                                    str_output += "<tr><td><b>" + dic_labels[attrCol2[i].Name] + ":</b></td>";
                                    str_output += "<td>" + this.ddlCanReachSites.SelectedItem.Text + "</td></tr>";
                                    break;
                                case "naClinicCoverageType":
                                    str_output += "<tr><td><b>" + dic_labels[attrCol2[i].Name] + ":</b></td>";
                                    str_output += "<td>" + this.ddlCoverageType.SelectedItem.Text + "</td></tr>";
                                    break;
                            }
                            if (attrCol2[i].Name != "canPharmacyReachout" && attrCol2[i].Name != "naClinicCoverageType")
                            {
                                str_output += "<tr><td><b>" + dic_labels[attrCol2[i].Name] + ":</b></td>";
                                str_output += "<td>" + attrCol2[i].InnerText + "</td></tr>";
                            }
                        }
                    }
                }
            }
        }
        str_output += "</table>";
        return str_output;
    }

    /// <summary>
    /// Gets field proper name for sending email
    /// </summary>
    /// <returns></returns>
    private Dictionary<string, string> getClinicDetails()
    {
        Dictionary<string, string> idic = new Dictionary<string, string>();
        idic.Add("clientName", "Client Name");
        idic.Add("contactPhone", "Contact Phone");
        idic.Add("contactFirstName", "Contact First Name");
        idic.Add("contactLastName", "Contact Last Name");
        idic.Add("contactEmail", "Contact Email");
        idic.Add("salesContactName", "Sales Contact Name");
        idic.Add("salesContactEmail", "Sales Contact Email");
        idic.Add("totalEmployeeCount", "Total Employee Count");
        idic.Add("canPharmacyReachout", "Can Pharmacy Reach out to </br> Local Sites to Promote Pull Through");
        idic.Add("naClinicLocation", "Clinic Location");
        idic.Add("naClinicDate", "Clinic Date");
        idic.Add("naClinicStartTime", "Clinic Start Time");
        idic.Add("address", "Clinic Address1");
        idic.Add("address2", "Clinic Address2");
        idic.Add("naClinicEndTime", "Clinic End Time");
        idic.Add("city", "Clinic City");
        idic.Add("naClinicEstimatedVolume", "Estimated Volume");
        idic.Add("state", "Clinic State");
        idic.Add("zip", "Clinic Zip");
        idic.Add("naClinicAddlComments", "Additional Comments/Instructions");
        idic.Add("naClinicSplBillingInstr", "Special Billing Instructions");
        idic.Add("naClinicPlanId", "Plan ID");
        idic.Add("naClinicCopay", "Copay");
        idic.Add("naClinicGroupId", "Group ID");
        idic.Add("naClinicTotalImmAdministered", "Total immunizations Administered");
        idic.Add("naClinicCoverageType", "Coverage Type");
        idic.Add("naClinicVaccinesCovered", "Vaccines Covered");
        return idic;
    }

    /// <summary>
    /// This method will be used to bind clinic details
    /// </summary>
    /// <param name="business_pk"></param>
    private void bindClinicDetails(int business_pk)
    {
        DataTable dt_clinicdetails = this.dbOperation.getScheduledClinics(business_pk);
        if (dt_clinicdetails.Rows.Count > 0)
        {
            this.lblClientName.Text = dt_clinicdetails.Rows[0]["businessName"].ToString();

            if (this.commonAppSession.LoginUserInfoSession.IsAdmin || this.commonAppSession.LoginUserInfoSession.IsPowerUser)
            {                
                if (dt_clinicdetails.Rows[0]["phone"].ToString().Length == 10)
                    this.txtContactPhone.Text = dt_clinicdetails.Rows[0]["phone"].ToString().Substring(0, 3) + "-" + dt_clinicdetails.Rows[0]["phone"].ToString().Substring(3, 3) + "-" + dt_clinicdetails.Rows[0]["phone"].ToString().Substring(6, 4);
                else
                    this.txtContactPhone.Text = dt_clinicdetails.Rows[0]["phone"].ToString();
                this.txtFirstContactName.Text = dt_clinicdetails.Rows[0]["firstName"].ToString();
                this.txtLastContactName.Text = dt_clinicdetails.Rows[0]["lastName"].ToString();
                this.txtContactEmail.Text = dt_clinicdetails.Rows[0]["businessContactEmail"].ToString();
                this.txtSalesContactName.Text = dt_clinicdetails.Rows[0]["naSalesContactName"].ToString();
                this.txtSalesContactEmail.Text = dt_clinicdetails.Rows[0]["naSalesContactEmail"].ToString();
                this.txtVaccinesCovered.Text = dt_clinicdetails.Rows[0]["naClinicVaccinesCovered"].ToString();
                this.txtPlanId.Text = dt_clinicdetails.Rows[0]["naClinicPlanId"].ToString();
                this.txtCopay.Text = dt_clinicdetails.Rows[0]["naClinicCopay"].ToString().Replace("''", "'");
                this.txtGroupId.Text = dt_clinicdetails.Rows[0]["naClinicGroupId"].ToString();
                this.txtTotalImm.Text = dt_clinicdetails.Rows[0]["naClinicTotalImmAdministered"].ToString();
                if (dt_clinicdetails.Rows[0]["naClinicDate"] != DBNull.Value && dt_clinicdetails.Rows[0]["naClinicDate"].ToString() != string.Empty)
                    this.txtTotalImm.Enabled = (Convert.ToDateTime(dt_clinicdetails.Rows[0]["naClinicDate"].ToString()) <= DateTime.Now);

                if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    this.txtSpecialInstr.Text = dt_clinicdetails.Rows[0]["naClinicSplBillingInstr"].ToString().Replace("''", "'");
                else
                    this.lblSpecialInstr.Text = dt_clinicdetails.Rows[0]["naClinicSplBillingInstr"].ToString().Replace("''", "'");
            }
            else
            {                
                if (dt_clinicdetails.Rows[0]["phone"].ToString().Length == 10)
                    this.lblContactPhone.Text = dt_clinicdetails.Rows[0]["phone"].ToString().Substring(0, 3) + "-" + dt_clinicdetails.Rows[0]["phone"].ToString().Substring(3, 3) + "-" + dt_clinicdetails.Rows[0]["phone"].ToString().Substring(6, 4);
                else
                    this.lblContactPhone.Text = dt_clinicdetails.Rows[0]["phone"].ToString();
                this.lblFirstContactName.Text = dt_clinicdetails.Rows[0]["firstName"].ToString();
                this.lblLastContactName.Text = dt_clinicdetails.Rows[0]["lastName"].ToString();
                this.lblContactEmail.Text = dt_clinicdetails.Rows[0]["businessContactEmail"].ToString();
                this.lblSalesContactName.Text = dt_clinicdetails.Rows[0]["naSalesContactName"].ToString();
                this.lblSalesContactEmail.Text = dt_clinicdetails.Rows[0]["naSalesContactEmail"].ToString();
                this.lblVaccinesCovered.Text = dt_clinicdetails.Rows[0]["naClinicVaccinesCovered"].ToString();
                if (Convert.ToInt32(dt_clinicdetails.Rows[0]["naClinicCoverageType"].ToString()) != 0 && dt_clinicdetails.Rows[0]["naClinicCoverageType"] != DBNull.Value)
                    this.lblCoverageType.Text = this.ddlCoverageType.Items[Convert.ToInt32(dt_clinicdetails.Rows[0]["naClinicCoverageType"].ToString())].Text;
                this.lblPlanId.Text = dt_clinicdetails.Rows[0]["naClinicPlanId"].ToString();
                this.lblCopay.Text = dt_clinicdetails.Rows[0]["naClinicCopay"].ToString().Replace("''", "'");
                this.lblGroupId.Text = dt_clinicdetails.Rows[0]["naClinicGroupId"].ToString();
                this.lblTotalImm.Text = dt_clinicdetails.Rows[0]["naClinicTotalImmAdministered"].ToString();
                this.lblCanReachSites.Text = (dt_clinicdetails.Rows[0]["naCanPharmacyReachout"] != DBNull.Value) ? (((bool)dt_clinicdetails.Rows[0]["naCanPharmacyReachout"]) ? "Yes" : "No") : "";
                this.lblSpecialInstr.Text = dt_clinicdetails.Rows[0]["naClinicSplBillingInstr"].ToString().Replace("''", "'");
            }

            if (dt_clinicdetails.Rows[0]["naCanPharmacyReachout"] != DBNull.Value)
                this.ddlCanReachSites.SelectedValue = ((bool)dt_clinicdetails.Rows[0]["naCanPharmacyReachout"]) ? "1" : "0";
            this.ddlCoverageType.SelectedValue = dt_clinicdetails.Rows[0]["naClinicCoverageType"].ToString();

            this.txtClinicLocation.Text = dt_clinicdetails.Rows[0]["naClinicLocation"].ToString();
            this.txtClinicAddress1.Text = dt_clinicdetails.Rows[0]["address"].ToString();
            if (dt_clinicdetails.Rows[0]["naClinicDate"] != DBNull.Value && dt_clinicdetails.Rows[0]["naClinicDate"].ToString() != string.Empty)
            {
                this.dtClinicDate.getSelectedDate = Convert.ToDateTime(dt_clinicdetails.Rows[0]["naClinicDate"].ToString());
                if (Convert.ToDateTime(dt_clinicdetails.Rows[0]["naClinicDate"].ToString()) <= DateTime.Now)
                    this.dtClinicDate.MinDate = Convert.ToDateTime(dt_clinicdetails.Rows[0]["naClinicDate"].ToString()).AddDays(-1);
                else
                    this.dtClinicDate.MinDate = DateTime.Now;
            }

            this.txtClinicStartTime.Text = dt_clinicdetails.Rows[0]["naClinicStartTime"].ToString();
            this.txtClinicAddress2.Text = dt_clinicdetails.Rows[0]["address2"].ToString();
            this.txtClinicEndTime.Text = dt_clinicdetails.Rows[0]["naClinicEndTime"].ToString();
            this.txtClinicCity.Text = dt_clinicdetails.Rows[0]["city"].ToString();
            this.txtEstimateVolume.Text = dt_clinicdetails.Rows[0]["naClinicEstimatedVolume"].ToString().Replace("''", "'");
            this.ddlClinicState.SelectedValue = dt_clinicdetails.Rows[0]["state"].ToString();
            this.txtClinicZip.Text = dt_clinicdetails.Rows[0]["zip"].ToString();
            this.txtAddlComments.Text = dt_clinicdetails.Rows[0]["naClinicAddlComments"].ToString().Replace("''", "'");

            Session["prevClinicDetails"] = this.prepareXmlDocument(business_pk);
        }
    }

    /// <summary>
    /// This method will be used to display controls based on login user
    /// </summary>
    private void displayControlsBasedOnUserTypes()
    {
        this.txtSpecialInstr.Visible = false;
        this.lblSpecialInstr.Visible = true;
        this.txtSpecialInstrRegEV.Enabled = false;
        if (!this.commonAppSession.LoginUserInfoSession.IsAdmin && !this.commonAppSession.LoginUserInfoSession.IsPowerUser)
        {
            this.txtContactPhone.Visible = false;
            this.txtContactPhoneCV.Enabled = false;
            this.txtFirstContactName.Visible = false;
            this.txtFirstContactNameReqFV.Enabled = false;
            this.txtFirstContactNameRegEV.Enabled = false;
            this.txtLastContactName.Visible = false;
            this.txtLastContactNameReqFV.Enabled = false;
            this.txtLastContactNameRegEV.Enabled = false;
            this.txtContactEmail.Visible = false;
            this.txtContactEmailReqFV.Enabled = false;
            this.txtContactEmailRegEV.Enabled = false;
            this.txtSalesContactName.Visible = false;
            this.txtSalesContactNameReqFV.Enabled = false;
            this.txtSalesContactNameRegEV.Enabled = false;
            this.txtSalesContactEmail.Visible = false;
            this.txtSalesContactEmailReqFV.Enabled = false;
            this.txtSalesContactEmailRegEV.Enabled = false;
            this.ddlCanReachSites.Visible = false;
            this.ddlCanReachSitesReqFV.Enabled = false;
            this.txtVaccinesCovered.Visible = false;
            this.txtVaccinesCoveredReqFV.Enabled = false;
            this.txtVaccinesCoveredRegEV.Enabled = false;
            this.ddlCoverageType.Visible = false;
            this.ddlCoverageTypeReqFV.Enabled = false;
            this.txtPlanId.Visible = false;
            this.txtPlanIdReqFV.Enabled = false;
            this.txtPlanIdRegEV.Enabled = false;
            this.txtCopay.Visible = false;
            this.txtCopayReqFV.Enabled = false;
            this.txtGroupId.Visible = false;
            this.txtGroupIdReqFV.Enabled = false;
            this.txtGroupIdRegEV.Enabled = false;
            this.txtTotalImm.Visible = false;
            this.txtTotalImmRegEV.Enabled = false;
            this.txtContactPhoneRegEV.Enabled = false;
            this.txtContactPhoneCV.Enabled = false;
            this.txtCopayRegEV.Enabled = false;
            this.txtTotalImmRegEV.Enabled = false;

            this.lblClientName.Visible = true;
            this.lblContactPhone.Visible = true;
            this.lblFirstContactName.Visible = true;
            this.lblLastContactName.Visible = true;
            this.lblContactEmail.Visible = true;
            this.lblSalesContactName.Visible = true;
            this.lblSalesContactEmail.Visible = true;
            this.lblVaccinesCovered.Visible = true;
            this.lblCoverageType.Visible = true;
            this.lblPlanId.Visible = true;
            this.lblCopay.Visible = true;
            this.lblGroupId.Visible = true;
            this.lblTotalImm.Visible = true;
            this.lblCanReachSites.Visible = true;
        }

        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.txtSpecialInstr.Visible = true;
            this.txtSpecialInstrRegEV.Enabled = true;
            this.lblSpecialInstr.Visible = false;
        }
    }

    /// <summary>
    /// This function is binding states to state drop down list
    /// </summary>
    private void bindStates()
    {
        DataSet ds_states = ApplicationSettings.getStates();

        if (ds_states.Tables.Count > 0)
        {
            this.ddlClinicState.DataSource = ds_states;
            this.ddlClinicState.DataValueField = ds_states.Tables[0].Columns["Value"].ToString();
            this.ddlClinicState.DataTextField = ds_states.Tables[0].Columns["state_text"].ToString();
            this.ddlClinicState.DataBind();
            this.ddlClinicState.Items.Insert(0, new ListItem("-- Select --", ""));
        }
    }

    /// <summary>
    /// Binds clinic coverage types to dropdown
    /// </summary>
    private void bindCoverageTypes()
    {
        DataTable dt_coverage_types = this.dbOperation.getClinicCoverageTypes();

        this.ddlCoverageType.DataSource = dt_coverage_types;
        this.ddlCoverageType.DataValueField = dt_coverage_types.Columns["fieldValue"].ToString();
        this.ddlCoverageType.DataTextField = dt_coverage_types.Columns["CoverageType"].ToString();
        this.ddlCoverageType.DataBind();
        this.ddlCoverageType.Items.Insert(0, new ListItem("Select", ""));
    }
    #endregion

    #region ---------- PRIVATE VARIABLES-----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private WalgreenEmail walgreensEmail = null;
    #endregion

    #region ------- Web Form Designer generated code -------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.walgreensEmail = ApplicationSettings.emailSettings();
    }
    #endregion
}