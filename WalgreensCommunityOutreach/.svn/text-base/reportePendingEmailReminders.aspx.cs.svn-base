﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Web.UI;

public partial class reportePendingEmailReminders : Page
{
    #region -------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.PickerAndCalendarFrom.getSelectedDate = ApplicationSettings.getDeploymentDate();
            this.PickerAndCalendarFrom.MinDate = ApplicationSettings.getDeploymentDate();            
            this.PickerAndCalendarTo.getSelectedDate = DateTime.Now;
            this.reportBind();
        }
    }    
    /// <summary>
    /// Assign some filter conditon to get the filterd output report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGoUser_Click(object sender, EventArgs e)
    {  
        this.reportBind();
    }

    /// <summary>
    /// Reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click1(object sender, EventArgs e)
    {
        this.clearFields();
    }

    /// <summary>
    /// Event to bind the report
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        this.reportBind();
    }
    #endregion

    #region ------------------PRIVATE METHODS------------------
    /// <summary>
    /// Clearing all the fields
    /// </summary>
    private void clearFields()
    {
        this.PickerAndCalendarFrom.getSelectedDate = ApplicationSettings.getDeploymentDate();
        this.PickerAndCalendarTo.getSelectedDate = DateTime.Now;
        this.reportBind();
    }

    /// <summary>
    /// Generates report for the selected store
    /// </summary>
    private void reportBind()
    {
        DataTable data_tbl;
        string from_date = (this.PickerAndCalendarFrom.getSelectedDate != DateTime.MinValue) ? this.PickerAndCalendarFrom.getSelectedDate.ToShortDateString() : string.Empty;
        string to_date = (this.PickerAndCalendarTo.getSelectedDate != DateTime.MinValue) ? this.PickerAndCalendarTo.getSelectedDate.ToShortDateString() : string.Empty;

        this.pendingEmailRemindersReport.ProcessingMode = ProcessingMode.Local;
        data_tbl = this.dbOperation.getPendingEmailReminders(this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId, from_date == "" ? "1900-01-01" : from_date, to_date == "" ? "9999-12-31" : to_date);

        if (data_tbl.Rows.Count > 0)
        {
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "tdWalgreensDataSet_tblStoreBusinessFeedbacks";
            rds.Value = data_tbl;

            this.pendingEmailRemindersReport.LocalReport.DataSources.Clear();
            this.pendingEmailRemindersReport.LocalReport.DataSources.Add(rds);
            this.pendingEmailRemindersReport.ShowPrintButton = false;
            if (data_tbl.Rows.Count > 0)
                this.pendingEmailRemindersReport.LocalReport.ReportPath = Server.MapPath("reports/pendingEmailReminders.rdlc");
        }
        data_tbl = null;
    }
    #endregion


    #region --------- PRIVATE VARIABLES----------
    protected AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private int storeId = 0;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion

}
