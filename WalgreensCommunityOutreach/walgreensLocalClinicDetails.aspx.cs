﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using TdApplicationLib;
using tdEmailLib;
using TdWalgreens;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using NLog;
using System.Web.Services;
using System.Web;

public partial class walgreensLocalClinicDetails : System.Web.UI.Page
{
    #region ------------ PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserName))
        {
            switch (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower())
            {
                case "admin":
                case "district manager":
                case "healthcare supervisor":
                case "director – rx & retail ops":
                    //Enable store assignment access to Admin,DM,HCS,DPR only
                    this.isStoreEditable = true;
                    break;
                default: this.isStoreEditable = false;
                    break;
            }
        }
        if (!Page.IsPostBack)
        {
            if (this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk > 0)
            {
                this.hfBusinessClinicPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk.ToString();
                this.hfBusinessStoreId.Value = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.txtDefaultClinicStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
                this.isDisableEditClinicDetails = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails");
                if (this.isDisableEditClinicDetails)
                    this.imgbtnRemoveClinicAgreement.Visible = false;
                if (!ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "ModifyApprovedAgreement"))
                    this.imgbtnModifyAgreement.Visible = false;
                this.bindLocalClinicDetails();
                this.controlAccess();
                if (this.isMOPreviousSeasonBusiness)
                {
                    this.bindLocalClinicPreviousLocationToDropdown();
                }
                this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = 0;
            }
            else
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                Response.Redirect("walgreensHome.aspx");
            }
        }
        else
        {
            string event_args = Request["__EVENTTARGET"];
            if (event_args.ToLower() == "maintaincontactstatus")
            {
                bool maintain_log = Convert.ToBoolean(Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"])));
                if (!maintain_log)
                {
                    int business_clinic_pk;
                    Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);

                    if (business_clinic_pk > 0)
                        this.dbOperation.removeClinicContactLogs(business_clinic_pk, "Local");
                }

                Session.Remove(this.hfBusinessClinicPk.Value);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
            else if (event_args.ToLower() == "deleteclinicagreement")
            {
                int business_clinic_pk;
                int return_value = 0;
                Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);

                if (business_clinic_pk > 0)
                {
                    return_value = this.dbOperation.deleteAgreement(business_clinic_pk, this.commonAppSession.LoginUserInfoSession.UserID, "Local");

                    if (return_value == 0)
                    {
                        this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId = 1;

                        //Send delete contract agreement notification email to admin users
                        string to_email = string.Empty;
                        to_email += ApplicationSettings.getEmailInfoFromGroup("deleteContractAgreementNotificationGroup", true).Trim();

                        this.walgreensEmail.sendCommunityOffsiteAgreementDeletedNotificationEmail("Admin", this.commonAppSession.LoginUserInfoSession.UserName, "Clinic " + ((Label)this.grdLocations.Rows[0].FindControl("lblClinicLocation")).Text, Server.MapPath("~/emailTemplates/communityOffsiteAgreementDeletionTemplate.htm"), String.Format((string)GetGlobalResourceObject("errorMessages", "deleteContractAgreementNotifyEmailSubject"), this.lblClientName.Text), this.hfBusinessStoreId.Value, this.lblClientName.Text, this.hfContractedStoreId.Value, (string)GetGlobalResourceObject("errorMessages", "nlaThankYouLine"), (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailSendTo"].ToString()) ? ConfigurationManager.AppSettings["emailSendTo"].ToString() : to_email), "", true);

                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "deletedClinicAgreement") + "'); window.location.href = 'walgreensHome.aspx';", true);
                    }
                }
            }
            else if (event_args.ToLower() == "clinicdetailsupdated")
            {
                bool save_changes = Convert.ToBoolean(Convert.ToInt32(Request["__EVENTARGUMENT"]));
                if (save_changes)
                {
                    this.updatedAction = "Submit";
                    if (this.ValidateLocations("UpdateClinic"))
                    {
                        this.doProcess(true, true);

                        if (!this.showAlertMessage && this.isValid)
                        {
                            Session.Remove(this.hfBusinessClinicPk.Value);
                            Response.Redirect("walgreensHome.aspx");
                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
                        return;
                    }
                }
                else
                {
                    Session.Remove(this.hfBusinessClinicPk.Value);
                    Response.Redirect("walgreensHome.aspx");
                }
            }
            else if (event_args.ToLower().Contains("continuesaving"))//With Override
            {
                if (this.infoMsgList.Count != 0 && (Request["__EVENTARGUMENT"] == null || Request["__EVENTARGUMENT"].ToString() == "isValid"))
                {
                    this.showValidationSummary(false);
                }
                else
                {
                    this.doProcess(false, false);
                }
            }
            else if (event_args.ToLower().Contains("continuesubmitting"))//With Continue
            {
                this.doProcess(false, false, true);
            }
            this.setMinMaxDates();
        }

        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
        this.ddlClinicLocations = (this.grdLocations.Rows.Count > 0) ? (DropDownList)this.grdLocations.Rows[0].FindControl("ddlClinicLocations") : null;
        this.imgBtnAddLocation = (this.grdLocations.Rows.Count > 0) ? (ImageButton)this.grdLocations.Rows[0].FindControl("imgBtnAddLocation") : null;
    }

    protected void grdLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Web.UI.HtmlControls.HtmlControl row_store_assign = (System.Web.UI.HtmlControls.HtmlControl)e.Row.FindControl("rowStoreAssignment");
            System.Web.UI.HtmlControls.HtmlControl row_reAssign = (System.Web.UI.HtmlControls.HtmlControl)e.Row.FindControl("rowReAssignment");
            System.Web.UI.HtmlControls.HtmlTableRow row_add_clinic = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowAddClinicLocation");


            GridView grd_clinic_immunizations = (GridView)e.Row.FindControl("grdClinicImmunizations");
            row_store_assign.Visible = false;

            DropDownList ddl_states = (DropDownList)e.Row.FindControl("ddlState");
            string restriction_start_date = ApplicationSettings.getStoreStateRestrictions("restrictionStartDate");
            string restriction_end_date = ApplicationSettings.getStoreStateRestrictions("restrictionEndDate");
            if (Convert.ToDateTime(restriction_start_date) < DateTime.Now.Date && DateTime.Now.Date <= Convert.ToDateTime(restriction_end_date))
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString(), ApplicationSettings.getStoreStateRestrictions("storeState"));
            else
                ddl_states.bindStatesWithRestriction(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString(), "");

            if (ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString()) != null)
                ddl_states.Items.FindByValue(grdLocations.DataKeys[e.Row.RowIndex].Values["naClinicState"].ToString()).Selected = true;

            string clinic_date = "";
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
                clinic_date = grdLocations.DataKeys[e.Row.RowIndex].Values["clinicDate"].ToString();

            if (e.Row.RowIndex == 0)
            {
                ImageButton btn_remove_clinic = (ImageButton)e.Row.FindControl("imgBtnRemoveLocation");
                System.Web.UI.HtmlControls.HtmlTableRow row_clinic_immunizations = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowClinicImmunizations");

                row_reAssign.Visible = false;
                btn_remove_clinic.Visible = false;
                row_clinic_immunizations.Visible = false;
                grd_clinic_immunizations.Visible = false;

                if (this.isDisableEditClinicDetails)
                {
                    row_add_clinic.Visible = false;
                    ddl_states.Enabled = false;
                }

                //Show clinic date reminder above Billing & Vaccine Information for default clinic location
                DateTime clinic_scheduled_on = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["clinicScheduledOn"].ToString());
                if (!string.IsNullOrEmpty(clinic_date) && Convert.ToDateTime(clinic_date).Date < clinic_scheduled_on.Date.AddDays(14))
                {
                    this.lblClinicDateAlert.Text = (string)GetGlobalResourceObject("errorMessages", "clinicApprovedBefore2Weeks");
                    this.lblClinicDateAlert.Visible = true;
                }
            }
            else
            {
                row_add_clinic.Visible = false;

                System.Web.UI.HtmlControls.HtmlTable tbl_confirmed_client_name = (System.Web.UI.HtmlControls.HtmlTable)e.Row.FindControl("tblConfirmedClientName");
                tbl_confirmed_client_name.Visible = false;

                //bind immunizations to new clinic locations
                this.bindClinicImmunizations(grd_clinic_immunizations, e.Row.RowIndex);

                //Displays store assignment to specific users only
                if (this.isStoreEditable)
                    row_store_assign.Visible = true;

                if (clinic_date.Length > 0 && Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14))
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row_clinic_date_alert = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowClinicDateAlert");
                    Label lbl_clinic_date_alert = (Label)e.Row.FindControl("lblClinicDateAlert");
                    row_clinic_date_alert.Visible = true;
                    lbl_clinic_date_alert.Text = (string)GetGlobalResourceObject("errorMessages", "clinicCreatedBefore2Weeks");
                }
            }

            //Enable Default store assignment to specific users only
            if (this.isStoreEditable)
                this.txtDefaultClinicStoreId.Enabled = true;

            CheckBox chk_reassign_clinics = (CheckBox)e.Row.FindControl("chkReassignClinic");
            Label lbl_reassign_clinics = (Label)e.Row.FindControl("lblIsReassignClinic");
            int is_reassign = 0;
            Int32.TryParse(lbl_reassign_clinics.Text, out is_reassign);
            chk_reassign_clinics.Checked = (is_reassign == 0) ? false : true;
            chk_reassign_clinics.Visible = chkReassignClinicPrimary.Visible;

            this.setClinicDates(e.Row, true);

            //disabling address fields if state is MO and for Previous clinics 
            //disabling address fields if state is DC and for Previous clinics 
            if (this.isAddressDisabled)
            {
                ((TextBox)e.Row.FindControl("txtAddress1")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtAddress2")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtCity")).Enabled = false;
                ((TextBox)e.Row.FindControl("txtZipCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlState")).Enabled = false;
            }

            //Voucher Expiry Dates logic
            PickerAndCalendar flu_exp_date = (PickerAndCalendar)e.Row.FindControl("pcFluExpiryDate");
            PickerAndCalendar routine_exp_date = (PickerAndCalendar)e.Row.FindControl("pcRoutineExpiryDate");
            TextBox txt_flu_expiry_date = ((TextBox)e.Row.FindControl("txtFluExpiryDate"));
            TextBox txt_routine_expiry_date = ((TextBox)e.Row.FindControl("txtRoutineExpiryDate"));
            DateTime dt_flu_exp_date = DateTime.Now;
            DateTime dt_routine_exp_date = DateTime.Now;
            if (grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"] != null && !string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString()))
            {
                dt_flu_exp_date = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["fluExpiryDate"].ToString());
                flu_exp_date.getSelectedDate = dt_flu_exp_date;
                txt_flu_expiry_date.Text = dt_flu_exp_date.ToString("MM/dd/yyyy");
            }
            else
            {
                flu_exp_date.getSelectedDate = ApplicationSettings.getVoucherMaxExpDate;
                txt_flu_expiry_date.Text = ApplicationSettings.getVoucherMaxExpDate.ToString("MM/dd/yyyy");
            }
            if (dt_flu_exp_date.Date >= DateTime.Today.Date)
                flu_exp_date.MinDate = DateTime.Now.AddDays(-1);
            else
                flu_exp_date.MinDate = dt_flu_exp_date.AddDays(-1);
            flu_exp_date.MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);

            if (grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"] != null && !string.IsNullOrEmpty(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString()))
            {
                dt_routine_exp_date = Convert.ToDateTime(grdLocations.DataKeys[e.Row.RowIndex].Values["routineExpiryDate"].ToString());
                routine_exp_date.getSelectedDate = dt_routine_exp_date;
                txt_routine_expiry_date.Text = dt_routine_exp_date.ToString("MM/dd/yyyy");
            }
            else
            {
                routine_exp_date.getSelectedDate = DateTime.Today.AddYears(1);
                txt_routine_expiry_date.Text = DateTime.Today.AddYears(1).ToString("MM/dd/yyyy");
            }
            if (dt_routine_exp_date.Date >= DateTime.Today.Date)
                routine_exp_date.MinDate = DateTime.Now.AddDays(-1);
            else
                routine_exp_date.MinDate = dt_routine_exp_date.AddDays(-1);
            routine_exp_date.MaxDate = DateTime.Today.AddYears(1).AddDays(1);

            this.showHideVoucherDetails(e.Row);
        }
    }

    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            Label lbl_immunization_id = (Label)e.Row.FindControl("lblImmunizationPk");
            Label lbl_payment_type_id = (Label)e.Row.FindControl("lblPaymentTypeId");
            Label lbl_immunization_name = (Label)e.Row.FindControl("lblImmunizationCheck");

            if (Convert.ToInt32(lbl_payment_type_id.Text) == 6)
            {
                if (lbl_immunization_name.Text.ToLower().Contains("influ"))
                    this.isFluImmunizationExists = true;
                else
                    this.isRoutineImmunizationExists = true;

                System.Web.UI.HtmlControls.HtmlTableRow row_send_invoice_to = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowSendInvoiceTo");
                System.Web.UI.HtmlControls.HtmlTableRow row_voucher_needed = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowVoucherNeeded");
                System.Web.UI.HtmlControls.HtmlTableRow row_voucher_exp_date = (System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("rowExpirationDate");
                DropDownList ddl_tax_exempt = (DropDownList)e.Row.FindControl("ddlTaxExempt");
                DropDownList ddl_iscopay = (DropDownList)e.Row.FindControl("ddlIsCopay");
                DropDownList ddl_voucher = (DropDownList)e.Row.FindControl("ddlVoucher");

                row_send_invoice_to.Visible = true;
                row_voucher_needed.Visible = true;

                DataRow dr_immunization = this.dtClinicImmunizations.Select("immunizationPk=" + Convert.ToInt32(lbl_immunization_id.Text) + " AND paymentTypeId=" + Convert.ToInt32(lbl_payment_type_id.Text))[0];
                if (ddl_tax_exempt.Items.FindByValue("" + dr_immunization["tax"].ToString() + "") != null)
                    ddl_tax_exempt.Items.FindByValue("" + dr_immunization["tax"].ToString() + "").Selected = true;

                if (ddl_iscopay.Items.FindByValue("" + dr_immunization["isCoPay"].ToString() + "") != null)
                    ddl_iscopay.Items.FindByValue("" + dr_immunization["isCoPay"].ToString() + "").Selected = true;

                if (ddl_voucher.Items.FindByValue("" + dr_immunization["isVoucherNeeded"].ToString() + "") != null)
                    ddl_voucher.Items.FindByValue("" + dr_immunization["isVoucherNeeded"].ToString() + "").Selected = true;

                if (ddl_voucher.SelectedValue == "Yes")
                {
                    row_voucher_exp_date.Attributes.CssStyle.Add("display", "block");
                    ((TextBox)e.Row.FindControl("txtVaccineExpirationDate")).Text = Convert.ToDateTime(dr_immunization["voucherExpirationDate"].ToString()).ToString("MM/dd/yyyy");
                }
                else
                    row_voucher_exp_date.Attributes.CssStyle.Add("display", "none");

                lbl_price.Text = "$ " + lbl_price.Text;

                //Updating copay groupId related Values 
                if (ddl_iscopay.SelectedItem.Value.ToLower() == "yes")
                {
                    this.hfCopayGroupIdFlu.Value = this.hfCopayGroupIdFlu.Value == "" ? ((this.blockedOutImmunizations.Contains(((Label)e.Row.FindControl("lblImmunizationCheck")).Text.ToLower().Trim()) && string.IsNullOrEmpty(this.txtCopyGroupIdFlu.Text)) ? "true" : "") : this.hfCopayGroupIdFlu.Value;
                    this.hfCopayGroupIdRoutine.Value = this.hfCopayGroupIdRoutine.Value == "" ? ((this.blockedOutImmunizations.Contains(((Label)e.Row.FindControl("lblImmunizationCheck")).Text.ToLower().Trim()) && string.IsNullOrEmpty(this.txtCopyGroupIdRoutine.Text)) ? "" : "true") : this.hfCopayGroupIdRoutine.Value;
                }
            }
            else
                lbl_price.Text = (string.IsNullOrEmpty(lbl_price.Text)) ? "N/A" : "$ " + lbl_price.Text;
        }
    }

    protected void grdClinicUpdatesHistory_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_update_action = (Label)e.Row.FindControl("lblAction");
            if (lbl_update_action.Text.ToLower() == "updated")
            {
                System.Web.UI.HtmlControls.HtmlTableCell update_field = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateField");
                update_field.ColSpan = 2;
                System.Web.UI.HtmlControls.HtmlTableCell update_value = (System.Web.UI.HtmlControls.HtmlTableCell)e.Row.FindControl("tdClinicUpdateValue");
                update_value.Visible = false;
            }
        }
    }

    protected void btnAddLocation_Click(object sender, EventArgs e)
    {
        this.prepareClinicLocationDetailsXml(-1);

        XmlNode clinic_locations = this.xmlClinicDetails.SelectSingleNode("//clinicLocations");
        XmlElement clinic_location = this.xmlClinicDetails.CreateElement("clinicLocation");

        int max_clinic_number;
        double locations_count = 0;

        Int32.TryParse(this.hdMaxClinicNumber.Value, out max_clinic_number);
        locations_count = max_clinic_number + (this.grdLocations.Rows.Count - 1);

        clinic_location.SetAttribute("naClinicLocation", ((locations_count >= 26) ? ((Char)(65 + (locations_count % 26 == 0 ? Math.Ceiling(locations_count / 26) - 1 : Math.Ceiling(locations_count / 26) - 2))).ToString() + "" + ((Char)(65 + locations_count % 26)).ToString() : ((Char)(65 + locations_count % 26)).ToString()));
        clinic_location.SetAttribute("naContactFirstName", string.Empty);
        clinic_location.SetAttribute("naContactLastName", string.Empty);
        clinic_location.SetAttribute("naClinicContactPhone", string.Empty);
        clinic_location.SetAttribute("naContactEmail", string.Empty);
        if (this.isRestrictedStoreState && this.ddlClinicLocations.Visible && this.ddlClinicLocations.SelectedItem.Value != "0")
        {
            DataRow added_row = this.previousClinicLocation.Select("rowId='" + this.ddlClinicLocations.SelectedItem.Value + "'").FirstOrDefault();
            clinic_location.SetAttribute("naClinicAddress1", added_row["naClinicAddress1"].ToString());
            clinic_location.SetAttribute("naClinicAddress2", added_row["naClinicAddress2"].ToString());
            clinic_location.SetAttribute("naClinicCity", added_row["naClinicCity"].ToString());
            clinic_location.SetAttribute("naClinicState", added_row["naClinicState"].ToString());
            clinic_location.SetAttribute("naClinicZip", added_row["naClinicZip"].ToString());
        }
        else if (this.isRestrictedStoreState)
        {
            DataTable dt_business_contact = dbOperation.getBusinessContactDetails(Convert.ToInt32(this.hfContactLogPk.Value));
            clinic_location.SetAttribute("naClinicAddress1", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicAddress2", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["address2"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicCity", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["city"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicState", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["state"].ToString() : string.Empty);
            clinic_location.SetAttribute("naClinicZip", (dt_business_contact.Rows.Count > 0) ? dt_business_contact.Rows[0]["zip"].ToString() : string.Empty);
        }
        else
        {
            clinic_location.SetAttribute("naClinicAddress1", string.Empty);
            clinic_location.SetAttribute("naClinicAddress2", string.Empty);
            clinic_location.SetAttribute("naClinicCity", string.Empty);
            clinic_location.SetAttribute("naClinicState", string.Empty);
            clinic_location.SetAttribute("naClinicZip", string.Empty);
        }
        clinic_location.SetAttribute("clinicDate", string.Empty);
        clinic_location.SetAttribute("naClinicStartTime", string.Empty);
        clinic_location.SetAttribute("naClinicEndTime", string.Empty);
        clinic_location.SetAttribute("clinicScheduledOn", string.Empty);

        //Add re-assign attribute
        clinic_location.SetAttribute("isReassign", "0");
        clinic_location.SetAttribute("isNoClinic", "0");
        clinic_location.SetAttribute("clinicStoreId", this.hfBusinessStoreId.Value);
        //Add clinic latitude and longitude attributes
        clinic_location.SetAttribute("clinicLatitude", string.Empty);
        clinic_location.SetAttribute("clinicLongitude", string.Empty);
        clinic_location.SetAttribute("confirmedClientName", string.Empty);

        //Add selected immunizations to the new clinic location
        XmlNodeList default_clinic_immunizations = clinic_locations.FirstChild.FirstChild.ChildNodes;
        XmlElement clinic_immunizations = this.xmlClinicDetails.CreateElement("Immunizations");
        foreach (XmlElement immunization in default_clinic_immunizations)
        {
            XmlElement estshots_node = this.xmlClinicDetails.CreateElement("Immunization");
            estshots_node.SetAttribute("pk", immunization.Attributes["pk"].Value);
            estshots_node.SetAttribute("immunizationName", immunization.Attributes["immunizationName"].Value);
            estshots_node.SetAttribute("paymentTypeId", immunization.Attributes["paymentTypeId"].Value);
            estshots_node.SetAttribute("paymentTypeName", immunization.Attributes["paymentTypeName"].Value);
            estshots_node.SetAttribute("estimatedQuantity", string.Empty);
            estshots_node.SetAttribute("totalImmAdministered", string.Empty);
            clinic_immunizations.AppendChild(estshots_node);
        }

        clinic_location.AppendChild(clinic_immunizations);
        clinic_locations.AppendChild(clinic_location);
        this.bindClinicLocations();
        if (this.isRestrictedStoreState)
            this.bindLocalClinicPreviousLocationToDropdown();
    }

    protected void imgBtnRemoveLocation_Click(object sender, EventArgs e)
    {
        ImageButton img_btn_remove = (ImageButton)sender;
        GridViewRow grd_row = (GridViewRow)img_btn_remove.NamingContainer;

        this.prepareClinicLocationDetailsXml(grd_row.RowIndex);
        this.bindClinicLocations();
        if (this.isMOPreviousSeasonBusiness)
            this.bindLocalClinicPreviousLocationToDropdown();
    }

    /// <summary>
    /// Time picker at server side
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void displayTime_Picker(object sender, EventArgs e)
    {
        StringBuilder script_text = new StringBuilder();
        GridViewRow gvr = (GridViewRow)((sender as TextBox).NamingContainer);
        TextBox txt_end_time = new TextBox();
        txt_end_time = (TextBox)gvr.FindControl("txtEndTime");

        script_text = ApplicationSettings.displayTimePicker((sender as TextBox).ClientID, txt_end_time.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DateScript" + gvr.RowIndex, script_text.ToString(), true);
    }

    protected void lnkContractPageNav_Click(object sender, EventArgs e)
    {
        this.commonAppSession.SelectedStoreSession.isModifyAgreement = false;
        this.navigateToAgreement();
    }

    protected void doProcess_Click(object sender, CommandEventArgs e)
    {
        this.updatedAction = e.CommandArgument.ToString();
        bool is_valid = true;
        if (this.updatedAction != "Cancel")
        {
            if (this.updatedAction == "Confirmed")
                is_valid = this.ValidateLocations("ConfirmClinic");
            else if (this.updatedAction == "Completed")
                is_valid = this.ValidateLocations("CompleteClinic");
            else if (this.updatedAction == "Cancelled")
                is_valid = this.ValidateLocations("CancelledClinic");
            else
                is_valid = this.ValidateLocations("UpdateClinic");

            if (is_valid)
            {
                this.doProcess(false, true);
            }
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Highlighted input fields are required/invalid. Please update and submit.');", true);
        }
        else
        {
            if (!ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "DisableEditClinicDetails"))
                this.doProcess(false, true);
            else
            {
                Session.Remove(this.hfBusinessClinicPk.Value);
                Response.Redirect("~/walgreensHome.aspx");
            }
        }
    }

    protected void ValidateCompanyPhoneFax(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validatePhone();
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        // to clear the date in picker if we select MO or DC state by non admin user
        //calling bind clinic to set min dates dates for date pickers
        DropDownList ddl_state_clinic_location = (DropDownList)sender;
        if (ddl_state_clinic_location != null)
        {
            GridViewRow row = (GridViewRow)ddl_state_clinic_location.Parent.Parent.Parent;
            if (row != null)
            {
                var date_control = row.FindControl("PickerAndCalendarFrom");
                DateTime seleted_date = ((PickerAndCalendar)date_control).getSelectedDate;

                if ((ddl_state_clinic_location.SelectedValue == "MO" || ddl_state_clinic_location.SelectedValue == "DC") && !commonAppSession.LoginUserInfoSession.IsAdmin && seleted_date < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")) && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    ((PickerAndCalendar)date_control).getSelectedDate = DateTime.Parse("01/01/0001");
                }
            }
            this.prepareClinicLocationDetailsXml(-1);
            this.bindClinicLocations();
        }
    }

    protected void imgbtnModifyAgreement_Click(object sender, ImageClickEventArgs e)
    {
        this.commonAppSession.SelectedStoreSession.isModifyAgreement = true;
        this.navigateToAgreement();
    }

    protected void download_Click(object sender, CommandEventArgs e)
    {
        Session.Remove("downloadType");
        Session.Remove("clinicPk");
        Session["downloadType"] = e.CommandArgument.ToString();
        Session["clinicPk"] = this.hfBusinessClinicPk.Value;
        Session["outReachBusinessPk"] = this.hfOutReachBusinessPk.Value;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "navigateForm('" + Session["downloadType"] + "');", true);
    }

    protected void chkNoClinic_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk_no_clinic = (CheckBox)sender;
        GridViewRow grd_clinic_location_row = (GridViewRow)chk_no_clinic.NamingContainer;
        this.showHideVoucherDetails(grd_clinic_location_row);
    }
    #endregion

    #region ------------ PRIVATE METHODS -------------
    /// <summary>
    /// Validates clinic data and checks mandatory fields
    /// </summary>
    /// <param name="validation_group"></param>
    /// <returns></returns>
    private bool ValidateLocations(string validation_group)
    {
        bool is_page_valid = true;
        string value = string.Empty;

        //Validate default clinic store Id
        is_page_valid = this.txtDefaultClinicStoreId.validateControls("textbox", "number", true, string.Empty, "Please enter valid Store Id", this.Page);

        //Validate client business information
        is_page_valid = this.txtFirstContactName.validateControls("textbox", "string", false, "Contact First Name is required", "Contact First Name: , < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtLocalContactPhonePrimary.validateControls("textbox", "phone", false, "Contact Phone number is required", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        is_page_valid = this.txtLastContactName.validateControls("textbox", "string", false, "Contact Last Name is required", "Contact Last Name: , < > characters are not allowed", this.Page) && is_page_valid;
        is_page_valid = this.txtLocalContactEmailPrimary.validateControls("textbox", "email", true, string.Empty, "Please enter a valid Email address", this.Page) && is_page_valid;
        is_page_valid = this.txtContactJobTitle.validateControls("textbox", "string", true, string.Empty, "Contact Job Title: , < > characters are not allowed", this.Page) && is_page_valid;

        //Validate clinic billing & vaccine information
        if (this.txtPlanId.Visible)
            is_page_valid = this.txtPlanId.validateControls("textbox", "string", false, "Plan ID is required", "Plan ID: < > characters are not allowed", this.Page) && is_page_valid;
        if (this.txtGroupId.Visible)
            is_page_valid = this.txtGroupId.validateControls("textbox", "string", false, "Group ID is required", "Group ID: < > characters are not allowed", this.Page) && is_page_valid;
        if (this.txtCopyGroupIdFlu.Visible)
            is_page_valid = this.txtCopyGroupIdFlu.validateControls("textbox", "string", false, "Copay group id for Flu is required", "Group ID: < > characters are not allowed", this.Page) && is_page_valid;
        if (txtCopyGroupIdRoutine.Visible)
            is_page_valid = this.txtCopyGroupIdRoutine.validateControls("textbox", "string", false, "Copay group id for Routine is required", "Group ID: < > characters are not allowed", this.Page) && is_page_valid;
        if (this.txtIdRecipient.Visible)
            is_page_valid = this.txtIdRecipient.validateControls("textbox", "string", false, "ID Recipient is required", "ID Recipient: < > characters are not allowed", this.Page) && is_page_valid;

        foreach (GridViewRow row in grdImmunizationChecks.Rows)
        {
            is_page_valid = ((TextBox)row.FindControl("txtEstimatedQuantity")).validateControls("textbox", "zero", false, "Please enter the number of Estimated Vol", "Please enter valid number of Estimated Vol (ex: #####)", this.Page) && is_page_valid;

            if (validation_group == "CompleteClinic")
                is_page_valid = ((TextBox)row.FindControl("txtTotalImmAdministered")).validateControls("textbox", "zero", false, "Please enter number of Total Immunizations Administered", "Please enter valid number of Total Immunizations Administered (ex: #####)", this.Page) && is_page_valid;
            else
                is_page_valid = ((TextBox)row.FindControl("txtTotalImmAdministered")).validateControls("textbox", "zero", true, string.Empty, "Please enter valid number of Total Immunizations Administered (ex: #####)", this.Page) && is_page_valid;
        }

        //Validate clinic location information
        foreach (GridViewRow row in grdLocations.Rows)
        {
            bool is_required = !((CheckBox)row.FindControl("chkNoClinic")).Checked;
            is_page_valid = ((TextBox)row.FindControl("txtContactFirstName")).validateControls("textbox", "string", false, "Contact First Name is required", "Contact First Name: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtContactLastName")).validateControls("textbox", "string", false, "Contact Last Name is required", "Contact Last Name: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtAddress1")).Enabled ? ((TextBox)row.FindControl("txtAddress1")).validateControls("textbox", "address", false, "Address1 is required", "Clinic Address1: < > characters are not allowed", this.Page) && is_page_valid : is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtAddress2")).Enabled ? ((TextBox)row.FindControl("txtAddress2")).validateControls("textbox", "address", true, string.Empty, "Clinic Address2: < > characters are not allowed", this.Page) && is_page_valid : is_page_valid;

            value = ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy");
            HtmlControl cont = (HtmlControl)((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).Parent;
            if (value == "01/01/0001" && is_required)
            {
                cont.Style.Add("border", "1px solid red");
                cont.Attributes.Add("title", "Clinic Date is required");
                is_page_valid = false;
            }
            else
            {
                cont.Style.Add("border", "0px gray");
                cont.Attributes.Add("title", "");
                is_page_valid = true && is_page_valid;
            }

            is_page_valid = ((TextBox)row.FindControl("txtCity")).validateControls("textbox", "string", false, "City is required", "Clinic City: < > characters are not allowed", this.Page) && is_page_valid;

            if ((validation_group == "ConfirmClinic" || validation_group == "CompleteClinic") && (row.RowIndex == 0))
                is_page_valid = ((TextBox)row.FindControl("txtConfirmedClientName")).validateControls("textbox", "string", false, "This clinic cannot be logged as Confirmed/Completed until you have entered the 'Name of Client Individual Who Confirmed the Clinic'", "Name of Client Individual Who Confirmed the Clinic: < > characters are not allowed", this.Page) && is_page_valid;
            else
                is_page_valid = ((TextBox)row.FindControl("txtConfirmedClientName")).validateControls("textbox", "string", true, string.Empty, "Name of Client Individual Who Confirmed the Clinic: < > characters are not allowed", this.Page) && is_page_valid;

            is_page_valid = ((TextBox)row.FindControl("txtLocalContactPhone")).validateControls("textbox", "phone", false, "Contact Phone is required", "Valid phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtZipCode")).validateControls("textbox", "zip", false, "Zip Code is required", "Please enter a valid Zip Code (ex: #####)", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtLocalContactEmail")).validateControls("textbox", "email", false, "Contact Email is required", "Please enter a valid Email address", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtStartTime")).validateControls("textbox", "string", !is_required, "Start Time is required", "Clinic Start Time: < > Characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((TextBox)row.FindControl("txtEndTime")).validateControls("textbox", "string", !is_required, "End Time is required", "Clinic End Time: < > Characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = ((DropDownList)row.FindControl("ddlState")).validateControls("dropdownlist", "select", false, "State is required", string.Empty, this.Page) && is_page_valid;

            if (row.RowIndex != 0)
            {
                GridView grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
                if (grd_clinic_immunizations != null)
                {
                    foreach (GridViewRow row_clinic_immunization in grd_clinic_immunizations.Rows)
                    {
                        is_page_valid = ((TextBox)row_clinic_immunization.FindControl("txtEstimatedQuantity")).validateControls("textbox", "zero", false, "Please enter the number of Estimated Vol", "Please enter valid number of Estimated Vol (ex: #####)", this.Page) && is_page_valid;
                    }
                }
            }
        }

        //Validate pharmacist and post clinic information
        if (validation_group == "CompleteClinic")
        {
            is_page_valid = this.txtRxHost.validateControls("textbox", "string", false, "This clinic cannot be logged as Completed until you have entered 'Name of Rx Host'", "Name Of Rx Host: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = this.txtTotalHoursClinicHeld.validateControls("textbox", "decimal", false, "This clinic cannot be logged as Completed until you have entered 'Total Hours Clinic Held'", "Please enter the valid Total Hours Clinic Held", this.Page) && is_page_valid;
            is_page_valid = this.txtRxHostPhone.validateControls("textbox", "phone", false, "This clinic cannot be logged as Completed until you have entered 'Phone #'", "Valid Phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        }
        else
        {
            is_page_valid = this.txtRxHost.validateControls("textbox", "string", true, string.Empty, "Name Of Rx Host: < > characters are not allowed", this.Page) && is_page_valid;
            is_page_valid = this.txtTotalHoursClinicHeld.validateControls("textbox", "decimal", true, string.Empty, "Please enter the valid Total Hours Clinic Held", this.Page) && is_page_valid;
            is_page_valid = this.txtRxHostPhone.validateControls("textbox", "phone", true, string.Empty, "Valid Phone number is required(ex: ###-###-####)", this.Page) && is_page_valid;
        }

        if (validation_group == "CancelledClinic")
            is_page_valid = this.txtFeedBack.validateControls("textbox", "string", false, "This clinic cannot be logged as Cancelled until you have entered 'Feedback Notes'", "Feedback: < > characters are not allowed", this.Page) && is_page_valid;
        else
            is_page_valid = this.txtFeedBack.validateControls("textbox", "string", true, string.Empty, "Feedback: < > characters are not allowed", this.Page) && is_page_valid;

        return is_page_valid;
    }

    /// <summary>
    /// Binds clinic details and locations information
    /// </summary>
    private void bindLocalClinicDetails()
    {
        int business_clinic_pk;
        Int32.TryParse(this.hfBusinessClinicPk.Value, out business_clinic_pk);
        if (!string.IsNullOrEmpty(this.commonAppSession.LoginUserInfoSession.UserRole))
        {
            DataSet ds_clinic_details = dbOperation.getClinicLocationDetails(business_clinic_pk, 2015, 1, this.commonAppSession.LoginUserInfoSession.UserRole);
            if (ds_clinic_details != null && ds_clinic_details.Tables.Count > 0)
            {
                if (ds_clinic_details.Tables[0].Rows.Count > 0)
                {
                    this.hfOutReachBusinessPk.Value = ds_clinic_details.Tables[0].Rows[0]["outreachBusinessPk"].ToString();
                    this.hfContactLogPk.Value = ds_clinic_details.Tables[0].Rows[0]["contactLogPk"].ToString();
                    this.hfIsPreviousSeasonLog.Value = ds_clinic_details.Tables[0].Rows[0]["isPreviousSeasonLog"].ToString();
                    this.hdMaxClinicNumber.Value = ds_clinic_details.Tables[0].Rows[0]["maxClinicLocationId"].ToString();
                    this.hfContractedStoreId.Value = ds_clinic_details.Tables[0].Rows[0]["contractedStoreId"].ToString();
                    this.hfRegionNumber.Value = ds_clinic_details.Tables[0].Rows[0]["marketNumber"].ToString();

                    //Business Information
                    this.lblClientName.Text = ds_clinic_details.Tables[0].Rows[0]["businessName"].ToString();
                    this.txtFirstContactName.Text = ds_clinic_details.Tables[0].Rows[0]["firstName"].ToString();
                    this.txtLastContactName.Text = ds_clinic_details.Tables[0].Rows[0]["lastName"].ToString();
                    this.txtContactJobTitle.Text = ds_clinic_details.Tables[0].Rows[0]["jobTitle"].ToString();
                    this.txtLocalContactEmailPrimary.Text = ds_clinic_details.Tables[0].Rows[0]["businessContactEmail"].ToString();
                    this.txtLocalContactPhonePrimary.Text = ds_clinic_details.Tables[0].Rows[0]["phone"].ToString();

                    //Billing & Vaccine Information
                    this.txtPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                    this.lblPlanId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicPlanId"].ToString();
                    this.txtGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();
                    this.lblGroupId.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicGroupId"].ToString();

                    this.txtCopyGroupIdFlu.Text = ds_clinic_details.Tables[0].Rows[0]["coPayFLUGroupId"].ToString();
                    this.txtCopyGroupIdRoutine.Text = ds_clinic_details.Tables[0].Rows[0]["coPayROUTINEGroupId"].ToString();

                    this.txtIdRecipient.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();
                    this.lblIdRecipient.Text = ds_clinic_details.Tables[0].Rows[0]["recipientId"].ToString();

                    this.txtComments.Text = ds_clinic_details.Tables[0].Rows[0]["naClinicAddlComments"].ToString();
                    this.dtClinicImmunizations = ds_clinic_details.Tables[1];
                    this.isCTIExists = (dtClinicImmunizations.Select("paymentTypeId = 6").Count() > 0);
                    this.isFluVoucherNeeded = (dtClinicImmunizations.Select("isVoucherNeeded = 'Yes' AND immunizationName LIKE '%flu%'").Count() > 0);
                    this.isRoutineVoucherNeeded = (dtClinicImmunizations.Select("isVoucherNeeded = 'Yes' AND immunizationName NOT LIKE '%flu%'").Count() > 0);
                    this.grdImmunizationChecks.DataSource = this.dtClinicImmunizations;
                    this.grdImmunizationChecks.DataBind();

                    if (ApplicationSettings.isRestrictedStoreState(ds_clinic_details.Tables[0].Rows[0]["storeState"].ToString(), this.commonAppSession.LoginUserInfoSession.UserRole))
                    {
                        this.previousClinicLocation = dbOperation.GetAllPreviousClinicLocations(Convert.ToInt32(ds_clinic_details.Tables[0].Rows[0]["businessPk"].ToString()));
                        if (this.previousClinicLocation.Rows.Count > 0)
                            this.isMOPreviousSeasonBusiness = true;

                        chkReassignClinicPrimary.Visible = false;
                        this.isAddressDisabled = true;
                        this.isRestrictedStoreState = true;

                    }
                    this.grdLocations.DataSource = ds_clinic_details.Tables[0];
                    this.grdLocations.DataBind();

                    //Bind Pharmacist & Post Clinic Information
                    this.txtRxHost.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistName"].ToString();
                    this.txtRxHostPhone.Text = ds_clinic_details.Tables[0].Rows[0]["pharmacistPhone"].ToString();
                    this.txtTotalHoursClinicHeld.Text = ds_clinic_details.Tables[0].Rows[0]["totalHours"].ToString();

                    //Show/hide outreach contact status buttons
                    this.btnCancelClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnCancelClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnConfirmedClinic.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) && !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnConfirmedClinicDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isConfirmed"].ToString()) || Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnClinicCompleted.Visible = !Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());
                    this.btnClinicCompletedDim.Visible = Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isCompleted"].ToString());

                    this.hfIsCancelled.Value = ds_clinic_details.Tables[0].Rows[0]["isCancelled"].ToString();

                    if (Convert.ToBoolean(ds_clinic_details.Tables[0].Rows[0]["isNoClinic"].ToString()))
                    {
                        this.btnConfirmedClinic.Text = "Vouchers Provided";
                        this.btnConfirmedClinicDim.Text = "Vouchers Provided";
                    }

                    //disable edit clinic details facility to specific clients
                    if (this.isDisableEditClinicDetails)
                    {
                        this.btnCancelClinic.Visible = false;
                        this.btnConfirmedClinic.Visible = false;
                        this.btnClinicCompleted.Visible = false;
                        this.btnSubmit.Visible = false;
                        this.btnCancelClinicDim.Visible = true;
                        this.btnConfirmedClinicDim.Visible = true;
                        this.btnClinicCompletedDim.Visible = true;
                        this.btnSubmitDim.Visible = true;
                    }

                    //Bind clinic updates history log
                    this.grdClinicUpdatesHistory.DataSource = ApplicationSettings.getClinicUpdateHistory(ds_clinic_details.Tables[2], "Local");
                    this.grdClinicUpdatesHistory.DataBind();
                    //System.Web.HttpBrowserCapabilities browser = Request.Browser;
                    //setting hfScheduledDate value to store scheduled on date 
                    DataRow[] row = ds_clinic_details.Tables[2].Select("updateAction = 'Scheduled'");
                    if (row.Count() > 0)
                    {
                        hfScheduledDate.Value = row[0]["updatedOn"].ToString();
                    }
                    //this.Logger.Trace("Client browser(Server-side)::" + browser.Browser + " " + browser.Version + "; Action::Page Load; Username::" + this.commonAppSession.LoginUserInfoSession.UserName + "; StoreId::" + this.hfBusinessStoreId.Value + "; ClinicPk::" + this.hfBusinessClinicPk.Value + "; Clinic Location::" + this.lblClientName.Text + "-Clinic " + ((Label)this.grdLocations.Rows[0].FindControl("lblClinicLocation")).Text);
                }

                this.prepareClinicLocationDetailsXml(-1);
                if (this.isMOPreviousSeasonBusiness)
                    this.bindLocalClinicPreviousLocationToDropdown();

                Session[this.hfBusinessClinicPk.Value] = this.xmlClinicDetails;
            }
        }
        else
            Response.Redirect("auth/sessionTimeout.aspx");
    }

    /// <summary>
    /// Binds clinic locations to Grid
    /// </summary>
    private void bindClinicLocations()
    {
        StringReader location_reader = new StringReader(this.xmlClinicDetails.SelectSingleNode("//clinicLocations").OuterXml.ToString());
        DataSet location_dataset = new DataSet();
        location_dataset.ReadXml(location_reader);

        grdLocations.DataSource = location_dataset.Tables[0];
        grdLocations.DataBind();
        if (this.isMOPreviousSeasonBusiness)
            this.bindLocalClinicPreviousLocationToDropdown();
    }

    /// <summary>
    /// Binds Immunizations to new clinic locations
    /// </summary>
    /// <param name="grd_clinic_immunizations"></param>
    /// <param name="clinic_number"></param>
    private void bindClinicImmunizations(GridView grd_clinic_immunizations, int clinic_number)
    {
        StringReader clinic_immunizations_reader = new StringReader(this.xmlClinicDetails.SelectSingleNode("//clinicLocations").ChildNodes[clinic_number].SelectSingleNode("Immunizations").OuterXml);
        DataSet ds_clinic_immunizations = new DataSet();
        ds_clinic_immunizations.ReadXml(clinic_immunizations_reader);

        if (ds_clinic_immunizations.Tables.Count > 0 && ds_clinic_immunizations.Tables[0].Rows.Count > 0)
        {
            grd_clinic_immunizations.DataSource = ds_clinic_immunizations.Tables[0];
            grd_clinic_immunizations.DataBind();
        }
    }

    /// <summary>
    /// Displays controls based on user login
    /// </summary>
    private void controlAccess()
    {
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            this.txtPlanId.Visible = true;
            //disabling GroupId for all users 
            if (this.txtGroupId.Text.Contains("pending copay group id"))
            {
                this.lblGroupId.Visible = true;

                this.tblCopayGroupId.Visible = true;
                this.trCoPayFlu.Visible = (this.hfCopayGroupIdFlu.Value == "true");
                this.trCoPayRoutine.Visible = (this.hfCopayGroupIdRoutine.Value == "true");
            }
            this.lblPlanId.Visible = false;
            this.txtIdRecipient.Visible = true;
            this.lblIdRecipient.Visible = false;
        }
    }

    /// <summary>
    /// Prepares clinic locations XML document
    /// </summary>
    /// <param name="remove_location_id"></param>
    private void prepareClinicLocationDetailsXml(int remove_location_id)
    {
        this.xmlClinicDetails = new XmlDocument();
        XmlElement clinic_details_ele = this.xmlClinicDetails.CreateElement("clinicDetails");
        int new_store_id = 0;
        int max_clinic_number = 0;
        Int32.TryParse(this.hdMaxClinicNumber.Value, out max_clinic_number);
        double location_count = max_clinic_number - 1;

        GridView grd_clinic_immunizations = null;

        //Clinic client details
        XmlElement client_info = this.xmlClinicDetails.CreateElement("clientInformation");
        client_info.SetAttribute("clientName", this.lblClientName.Text.Trim());
        client_info.SetAttribute("contactFirstName", this.txtFirstContactName.Text.Trim());
        client_info.SetAttribute("contactLastName", this.txtLastContactName.Text.Trim());
        client_info.SetAttribute("contactPhone", this.txtLocalContactPhonePrimary.Text.Trim());
        client_info.SetAttribute("contactEmail", this.txtLocalContactEmailPrimary.Text.Trim());
        client_info.SetAttribute("jobTitle", this.txtContactJobTitle.Text.Trim());
        client_info.SetAttribute("naClinicPlanId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtPlanId.Text.Trim() : this.lblPlanId.Text.Trim()));
        client_info.SetAttribute("naClinicGroupId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtGroupId.Text.Trim() : this.lblGroupId.Text.Trim()));
        //Setting Copay GroupId
        if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
        {
            if (this.hfCopayGroupIdFlu.Value == "true")
                client_info.SetAttribute("coPayFluGroupId", this.txtCopyGroupIdFlu.Text.Trim());
            if (this.hfCopayGroupIdRoutine.Value == "true")
                client_info.SetAttribute("coPayRoutineGroupId", this.txtCopyGroupIdRoutine.Text.Trim());
        }
        client_info.SetAttribute("recipientId", ((this.commonAppSession.LoginUserInfoSession.IsAdmin) ? this.txtIdRecipient.Text.Trim() : this.lblIdRecipient.Text.Trim()));
        client_info.SetAttribute("comments", this.txtComments.Text.Trim());
        client_info.SetAttribute("feedback", this.txtFeedBack.Text);
        clinic_details_ele.AppendChild(client_info);

        //Clinic locations details
        XmlElement clinic_locations = this.xmlClinicDetails.CreateElement("clinicLocations");
        foreach (GridViewRow row in grdLocations.Rows)
        {
            if (row.RowIndex != remove_location_id)
            {
                XmlElement location_node = this.xmlClinicDetails.CreateElement("clinicLocation");
                location_node.SetAttribute("naClinicLocation", (row.RowIndex == 0) ? ((Label)row.FindControl("lblClinicLocation")).Text : ((location_count >= 26) ? ((Char)(65 + (location_count % 26 == 0 ? Math.Ceiling(location_count / 26) - 1 : Math.Ceiling(location_count / 26) - 2))).ToString() + "" + ((Char)(65 + location_count % 26)).ToString() : ((Char)(65 + location_count % 26)).ToString()));

                location_node.SetAttribute("naContactFirstName", ((TextBox)row.FindControl("txtContactFirstName")).Text);
                location_node.SetAttribute("naContactLastName", ((TextBox)row.FindControl("txtContactLastName")).Text);
                location_node.SetAttribute("naClinicContactPhone", ((TextBox)row.FindControl("txtLocalContactPhone")).Text);
                location_node.SetAttribute("naContactEmail", ((TextBox)row.FindControl("txtLocalContactEmail")).Text);
                location_node.SetAttribute("naClinicAddress1", ((TextBox)row.FindControl("txtAddress1")).Text);
                location_node.SetAttribute("naClinicAddress2", ((TextBox)row.FindControl("txtAddress2")).Text);
                location_node.SetAttribute("naClinicCity", ((TextBox)row.FindControl("txtCity")).Text);
                location_node.SetAttribute("naClinicState", ((DropDownList)row.FindControl("ddlState")).SelectedValue);
                location_node.SetAttribute("naClinicZip", ((TextBox)row.FindControl("txtZipCode")).Text);

                location_node.SetAttribute("clinicScheduledOn", grdLocations.DataKeys[row.RowIndex].Values["clinicScheduledOn"].ToString());
                location_node.SetAttribute("isNoClinic", (((CheckBox)row.FindControl("chkNoClinic")).Checked ? "1" : "0"));

                if (((CheckBox)row.FindControl("chkNoClinic")).Checked)
                {
                    if (((PickerAndCalendar)row.FindControl("pcFluExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001" && this.isFluImmunizationExists)
                        location_node.SetAttribute("fluExpiryDate", ((PickerAndCalendar)row.FindControl("pcFluExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy"));
                    else if (((TextBox)row.FindControl("txtFluExpiryDate")).Text != "01/01/0001" && this.isFluImmunizationExists)
                        location_node.SetAttribute("fluExpiryDate", ((TextBox)row.FindControl("txtFluExpiryDate")).Text);
                    else
                        location_node.SetAttribute("fluExpiryDate", "");

                    if (((PickerAndCalendar)row.FindControl("pcRoutineExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001" && this.isRoutineImmunizationExists)
                        location_node.SetAttribute("routineExpiryDate", ((PickerAndCalendar)row.FindControl("pcRoutineExpiryDate")).getSelectedDate.ToString("MM/dd/yyyy"));
                    else if (((TextBox)row.FindControl("txtRoutineExpiryDate")).Text != "01/01/0001" && this.isRoutineImmunizationExists)
                        location_node.SetAttribute("routineExpiryDate", ((TextBox)row.FindControl("txtRoutineExpiryDate")).Text);
                    else
                        location_node.SetAttribute("routineExpiryDate", "");

                    location_node.SetAttribute("clinicDate", "");
                    location_node.SetAttribute("naClinicStartTime", "");
                    location_node.SetAttribute("naClinicEndTime", "");
                }
                else
                {
                    if (((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString("MM/dd/yyyy") != "01/01/0001")
                        location_node.SetAttribute("clinicDate", ((PickerAndCalendar)row.FindControl("PickerAndCalendarFrom")).getSelectedDate.ToString());
                    else
                        location_node.SetAttribute("clinicDate", "");

                    location_node.SetAttribute("naClinicStartTime", ((TextBox)row.FindControl("txtStartTime")).Text);
                    location_node.SetAttribute("naClinicEndTime", ((TextBox)row.FindControl("txtEndTime")).Text);
                    location_node.SetAttribute("fluExpiryDate", "");
                    location_node.SetAttribute("routineExpiryDate", "");
                }

                //Add clinic store Id & re-assign attribute
                if (row.RowIndex == 0)
                {
                    grd_clinic_immunizations = this.grdImmunizationChecks;
                    Int32.TryParse(txtDefaultClinicStoreId.Text, out new_store_id);
                    location_node.SetAttribute("isReassign", (chkReassignClinicPrimary.Checked ? "1" : "0"));
                }
                else
                {
                    grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
                    Int32.TryParse(((TextBox)row.FindControl("txtClinicStore")).Text, out new_store_id);
                    location_node.SetAttribute("isReassign", (((CheckBox)row.FindControl("chkReassignClinic")).Checked ? "1" : "0"));
                }

                if (this.isStoreEditable)
                    location_node.SetAttribute("clinicStoreId", (new_store_id > 0) ? new_store_id.ToString() : "");
                else
                    location_node.SetAttribute("clinicStoreId", "");

                //Add clinic latitude and longitude attributes
                location_node.SetAttribute("clinicLatitude", ((HiddenField)row.FindControl("hfClinicLatitude")).Value);
                location_node.SetAttribute("clinicLongitude", ((HiddenField)row.FindControl("hfClinicLongitude")).Value);
                location_node.SetAttribute("confirmedClientName", ((TextBox)row.FindControl("txtConfirmedClientName")).Text);

                clinic_locations.AppendChild(location_node);
                location_count++;

                //Add clinic immunizations
                XmlElement clinic_immunizations = this.xmlClinicDetails.CreateElement("Immunizations");
                if (grd_clinic_immunizations != null)
                {
                    foreach (GridViewRow immunization in grd_clinic_immunizations.Rows)
                    {
                        XmlElement immunization_ele = this.xmlClinicDetails.CreateElement("Immunization");
                        immunization_ele.SetAttribute("pk", ((Label)immunization.FindControl("lblImmunizationPk")).Text);
                        immunization_ele.SetAttribute("immunizationName", ((Label)immunization.FindControl("lblImmunizationCheck")).Text);
                        immunization_ele.SetAttribute("paymentTypeId", ((Label)immunization.FindControl("lblPaymentTypeId")).Text);
                        immunization_ele.SetAttribute("paymentTypeName", ((Label)immunization.FindControl("lblPaymentType")).Text);
                        immunization_ele.SetAttribute("estimatedQuantity", ((TextBox)immunization.FindControl("txtEstimatedQuantity")).Text);
                        immunization_ele.SetAttribute("totalImmAdministered", ((TextBox)immunization.FindControl("txtTotalImmAdministered")).Text);

                        clinic_immunizations.AppendChild(immunization_ele);
                    }
                    location_node.AppendChild(clinic_immunizations);
                }
            }
        }
        clinic_details_ele.AppendChild(clinic_locations);

        XmlElement post_clinic_info = this.xmlClinicDetails.CreateElement("postClinicInformation");
        post_clinic_info.SetAttribute("pharmacistName", this.txtRxHost.Text);
        post_clinic_info.SetAttribute("totalHours", this.txtTotalHoursClinicHeld.Text);
        post_clinic_info.SetAttribute("pharmacistPhone", this.txtRxHostPhone.Text);
        clinic_details_ele.AppendChild(post_clinic_info);

        this.xmlClinicDetails.AppendChild(clinic_details_ele);
    }

    /// <summary>
    /// Prepare CoPay Group Id
    /// </summary>
    private void applyCoPayGroupId()
    {
        if (commonAppSession.LoginUserInfoSession.IsAdmin && (this.hfCopayGroupIdFlu.Value != "" || this.hfCopayGroupIdRoutine.Value != ""))
        {
            if (this.trCoPayFlu.Visible == true || this.trCoPayRoutine.Visible == true)
            {
                string new_copay = this.hfCopayGroupIdFlu.Value != "" && this.hfCopayGroupIdRoutine.Value != "" ? (this.txtCopyGroupIdFlu.Text.Trim() != "" ? (this.txtCopyGroupIdFlu.Text.Trim() + "(flu)" + ", ") : "") + (this.txtCopyGroupIdRoutine.Text.Trim() != "" ? (this.txtCopyGroupIdRoutine.Text.Trim() + "(routine)") : "") : (this.hfCopayGroupIdFlu.Value != "" ? (this.txtCopyGroupIdFlu.Text.Trim() != "" ? (this.txtCopyGroupIdFlu.Text.Trim() + "(flu)") : "") : (this.txtCopyGroupIdRoutine.Text.Trim() != "" ? (this.txtCopyGroupIdRoutine.Text.Trim() + "(routine)") : ""));
                if (!string.IsNullOrEmpty(new_copay))
                    this.txtGroupId.Text = this.txtGroupId.Text.Replace("pending copay group id", new_copay);
            }
        }
    }

    /// <summary>
    /// Submits clinic details and clinic outreach contact status
    /// </summary>
    /// <param name="is_return_home"></param>
    /// <param name="is_no_override"></param>
    private void doProcess(bool is_return_home, bool is_no_override, bool is_continue = false)
    {
        int business_clinic_pk, store_id, new_store_id;
        Int32.TryParse(this.hfBusinessClinicPk.Value.Trim(), out business_clinic_pk);
        Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);
        Int32.TryParse(this.txtDefaultClinicStoreId.Text, out new_store_id);
        int return_value = 0;
        bool is_valid = true;

        if (new_store_id == 0 || new_store_id == store_id)
            new_store_id = 0;

        if (Session[this.hfBusinessClinicPk.Value] != null && business_clinic_pk > 0 && this.commonAppSession.LoginUserInfoSession != null)
        {
            string error_message = string.Empty;
            string email_body = string.Empty;
            //string billing_email;
            //bool send_emailto_clinical_contract = false;
            XmlDocument old_clinic_details_xml = ((XmlDocument)Session[this.hfBusinessClinicPk.Value]);
            this.applyCoPayGroupId();
            this.prepareClinicLocationDetailsXml(-1);
            bool is_equal = ApplicationSettings.compareXMLDocuments(this.xmlClinicDetails, old_clinic_details_xml);

            //this.Logger.Trace("Action: " + this.updatedAction + "; Username::" + this.commonAppSession.LoginUserInfoSession.UserName + "; StoreId::" + this.hfBusinessStoreId.Value + "; ClinicPk::" + this.hfBusinessClinicPk.Value + "; Clinic Location::" + this.lblClientName.Text + "-Clinic " + ((Label)this.grdLocations.Rows[0].FindControl("lblClinicLocation")).Text);
            //Warning to save changes before returning to home page
            switch (this.updatedAction.ToLower())
            {
                case "cancel":
                    if (!is_equal)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showUpdateClinicWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "clinicDetailsChanged")) + "');", true);
                        return;
                    }
                    else
                    {
                        Session.Remove(this.hfBusinessClinicPk.Value);
                        Response.Redirect("~/walgreensHome.aspx");
                    }
                    break;
                case "submit":
                    if (is_equal)
                    {
                        //calling this method to set min date css for clinic date picker
                        this.bindClinicLocations();
                        return;
                    }
                    break;
                case "confirmed":
                case "completed":
                case "cancelled":
                    if (new_store_id > 0)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showStoreChangeWithContactStatusWarning('" + string.Format((string)GetGlobalResourceObject("errorMessages", "storeChangedWithContactStatusUpdate")) + "');", true);
                        return;
                    }
                    break;
            }

            if (is_no_override && this.updatedAction.ToLower() != "cancelled")
            {
                is_valid = this.ValidationsHandler();
            }
            if (is_valid)
            {
                if (!is_continue && this.updatedAction.ToLower() != "completed")
                {
                    //Show Confirmation Alert
                    bool has_max_qty = false;
                    int loc_count = 1;
                    string max_qty_error = string.Empty;
                    var loc_list = (from locations in XDocument.Parse(this.xmlClinicDetails.InnerXml).Descendants("clinicLocation")
                                    where locations.Attribute("isNoClinic").Value != "1"
                                    select locations).ToList();
                    foreach (var clinic_location in loc_list)
                    {

                        var new_imm_list = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation[@isNoClinic='0'][" + loc_count + "]/Immunizations/Immunization/@estimatedQuantity").Cast<XmlNode>().ToList();
                        var imm_name_list = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation[@isNoClinic='0'][" + loc_count + "]/Immunizations/Immunization/@immunizationName").Cast<XmlNode>().ToList();
                        for (int i = 0; i < new_imm_list.Count; i++)
                        {
                            int new_est_shots;
                            int old_est_shots;
                            int.TryParse(new_imm_list[i].Value, out new_est_shots);

                            if (loc_count == 1)
                            {
                                var old_imm_list = old_clinic_details_xml.SelectNodes("/clinicDetails/clinicLocations/clinicLocation/Immunizations/Immunization/@estimatedQuantity").Cast<XmlNode>().ToList();
                                int.TryParse(old_imm_list[i].Value, out old_est_shots);
                                if (new_est_shots != old_est_shots && new_est_shots > 250)
                                {
                                    has_max_qty = true;
                                    max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), new_est_shots, imm_name_list[i].Value.ToString(), clinic_location.Attribute("naClinicLocation").Value.Replace("CLINIC LOCATION ", ""));
                                }
                            }
                            else if (new_est_shots > 250)
                            {
                                has_max_qty = true;
                                max_qty_error += "<br />" + string.Format((string)GetGlobalResourceObject("errorMessages", "maxImmQtyWarning"), new_est_shots, imm_name_list[i].Value, clinic_location.Attribute("naClinicLocation").Value.Replace("CLINIC LOCATION ", ""));

                            }
                        }
                        loc_count++;
                    }
                    if (has_max_qty)
                    {
                        this.showAlertMessage = true;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showWarning('WARNING','" + max_qty_error.Substring(6) + "','continuesubmitting');", true);
                        return;
                    }
                }
                XmlNodeList clinic_location_nodes = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation[@isReassign='0']");
                DataTable dt_reassigned_clinics = new DataTable();
                Dictionary<string, string> updated_values = ApplicationSettings.getUpdatedClinicDetails(this.xmlClinicDetails, ((XmlDocument)Session[this.hfBusinessClinicPk.Value]), "Local");
                email_body = updated_values["emailBody"];
                //billing_email = updated_values["billingEmail"];

                if (!string.IsNullOrEmpty(updated_values["historyLog"]))
                {
                    //Prepare updated fields xml and append to clinic details xml document
                    XmlDocumentFragment xml_frag = this.xmlClinicDetails.CreateDocumentFragment();
                    xml_frag.InnerXml = updated_values["historyLog"];
                    this.xmlClinicDetails.DocumentElement.AppendChild(xml_frag);

                    //send_emailto_clinical_contract = CommonExtensionsMethods.sendEmailToClinicalContract(updated_values["historyLog"]);
                }

                //reassign clinic if store state is not restricted.
                //bool can_reassign_clinic = (!ApplicationSettings.isRestrictedStoreState("MO", this.commonAppSession.LoginUserInfoSession.UserRole));
                return_value = this.dbOperation.updateClinicLocationDetails(business_clinic_pk, "Local", this.commonAppSession.LoginUserInfoSession.UserID, this.updatedAction, this.xmlClinicDetails.InnerXml, false, out error_message, out dt_reassigned_clinics);

                if (return_value == -2 && !string.IsNullOrEmpty(error_message))
                {
                    //calling this methos due to set min date css for picker 
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message.Substring(4) + "');", true);
                    return;
                }
                if (return_value == -4 && !String.IsNullOrEmpty(error_message))
                {
                    string validation_message = string.Empty;
                    ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out validation_message, "localdetails", this.lblClientName.Text, error_message);
                    this.showAlertMessage = true;
                    //calling this methos due to set min date css for picker 
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", validation_message, true);
                    return;
                }
                if (return_value == -5 && !string.IsNullOrEmpty(error_message))//Assigned to Restricted store state
                {
                    //calling this method due to set min date css for picker 
                    this.showAlertMessage = true;
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
                    return;
                }
                if (return_value == -10 || return_value == -11 || return_value == -12)
                {
                    this.showAlertMessage = true;
                    txtGroupId.Text = lblGroupId.Text;
                    string alert_message = return_value == -10 ? (string)GetGlobalResourceObject("errorMessages", "copayInvalidGroupIds") : (return_value == -11 ? (string)GetGlobalResourceObject("errorMessages", "copayInvalidFluGroupId") : (string)GetGlobalResourceObject("errorMessages", "copayInvalidRoutineGroupId"));
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + alert_message + "');", true);
                    return;
                }
                DataTable dt_default_clinic_user = dbOperation.getStoreUsersEmails(new_store_id > 0 ? new_store_id : store_id);
                EmailOperations email_operations = new EmailOperations();

                //Sending clinic details changed email 'Business Information'...
                //if ((!string.IsNullOrEmpty(email_body) || !string.IsNullOrEmpty(billing_email)) && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
                if (!string.IsNullOrEmpty(email_body) && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
                {
                    //email_operations.sendClinicDetailsChangedEmail(dt_default_clinic_user, "Local", email_body, billing_email, this.lblClientName.Text, business_clinic_pk.ToString(), this.hfContactLogPk.Value.Trim(), (new_store_id > 0 ? new_store_id : store_id).ToString(), send_emailto_clinical_contract);
                    email_operations.sendClinicDetailsChangedEmail(dt_default_clinic_user, "Local", email_body, string.Empty, this.lblClientName.Text, business_clinic_pk.ToString(), this.hfContactLogPk.Value.Trim(), (new_store_id > 0 ? new_store_id : store_id).ToString(), false);
                    //if (!string.IsNullOrEmpty(updated_values["billingEmailToAllClinics"]))
                    //    email_operations.sendUpdateBillingInfoEmailToAllClinics("Local", email_body, updated_values["billingEmailToAllClinics"], this.lblClientName.Text, business_clinic_pk, this.hfContactLogPk.Value.Trim(), send_emailto_clinical_contract);
                }

                if (dt_reassigned_clinics != null && dt_reassigned_clinics.Rows.Count > 0 && dt_default_clinic_user != null && dt_default_clinic_user.Rows.Count > 0)
                {
                    //Sending store re-assignment email...
                    email_operations.sendLocalClinicStoreReassignmentEmail(dt_default_clinic_user, dt_reassigned_clinics);

                    //Sending store assignment notification email to assigned store
                    email_operations.sendLocalClinicStoreAssignmentEmail(dt_reassigned_clinics, this.hfContactLogPk.Value);
                }

                if (return_value == -3 && !string.IsNullOrEmpty(error_message))
                {
                    //calling this methos due to set min date css for picker 
                    this.bindClinicLocations();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showMaintainContactLogWarning('" + error_message + "');", true);
                    return;
                }
                else if (!is_return_home)
                {
                    Session.Remove(this.hfBusinessClinicPk.Value);
                    this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;

                    if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "confirmed")
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "confirmedClinic") + "'); window.location.href = 'walgreensLocalClinicDetails.aspx';", true);
                    else if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "completed")
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicCompleted") + "'); window.location.href = 'walgreensLocalClinicDetails.aspx';", true);
                    else if (!string.IsNullOrEmpty(this.updatedAction) && this.updatedAction.ToLower() == "cancelled")
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicCancelled") + "'); window.location.href = 'walgreensLocalClinicDetails.aspx';", true);
                    else
                    {
                        if (new_store_id > 0)
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensHome.aspx';", true);
                        else
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "clinicDetailsUpdated") + "'); window.location.href = 'walgreensLocalClinicDetails.aspx';", true);
                    }
                }
            }
            else
                this.isValid = false;
        }
        else
        {
            Session.Remove(this.hfBusinessClinicPk.Value);
            Response.Redirect("~/walgreensHome.aspx");
        }
    }

    /// <summary>
    /// Binds the previous season clinic locations to dropdown
    /// </summary>
    private void bindLocalClinicPreviousLocationToDropdown()
    {
        this.ddlClinicLocations = (this.grdLocations.Rows.Count > 0) ? (DropDownList)this.grdLocations.Rows[0].FindControl("ddlClinicLocations") : null;
        this.imgBtnAddLocation = (this.grdLocations.Rows.Count > 0) ? (ImageButton)this.grdLocations.Rows[0].FindControl("imgBtnAddLocation") : null;
        if (this.previousClinicLocation.Rows.Count > 0 && this.ddlClinicLocations != null)
        {
            this.ddlClinicLocations.DataSource = this.previousClinicLocation;
            this.ddlClinicLocations.DataTextField = "naClinicAddress";
            this.ddlClinicLocations.DataValueField = "rowId";
            this.ddlClinicLocations.DataBind();
            this.ddlClinicLocations.Items.Insert(0, new ListItem("Select Location", "0"));
            this.ddlClinicLocations.Visible = true;
        }
        else
            this.ddlClinicLocations.Visible = false;
    }

    /// <summary>
    /// Validates clinic duration between clinic date and scheduled date
    /// </summary>
    /// <returns></returns>
    private bool validateClinicDates()
    {
        bool is_date_valid = true;
        int clinic_count = 1;
        var clinic_details_xml = XDocument.Parse(this.xmlClinicDetails.InnerXml);

        var clinic_dates = from dates in clinic_details_xml.Descendants("clinicLocation")
                           select dates.Attribute("clinicDate").Value;

        foreach (var clinic_date in clinic_dates)
        {
            if (clinic_count == 1)
            {
                DateTime previous_clinic_date = Convert.ToDateTime(((XmlDocument)Session[this.hfBusinessClinicPk.Value]).SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicDate").Value);
                DateTime clinic_scheduled_date = Convert.ToDateTime(((XmlDocument)Session[this.hfBusinessClinicPk.Value]).SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicScheduledOn").Value);

                if (previous_clinic_date.Date > clinic_scheduled_date.Date.AddDays(14) && Convert.ToDateTime(clinic_date).Date < clinic_scheduled_date.Date.AddDays(14))
                {
                    is_date_valid = false;
                    break;
                }
            }
            else
            {
                if (Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14))
                {
                    is_date_valid = false;
                    break;
                }
            }

            clinic_count++;
        }

        return is_date_valid;
    }
    /// <summary>
    /// Opens agreement page from details page
    /// </summary>
    /// <returns></returns>
    private void navigateToAgreement()
    {
        int business_clinic_pk, store_id;

        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        Int32.TryParse(this.hfBusinessClinicPk.Value.Trim(), out business_clinic_pk);
        Int32.TryParse(this.hfBusinessStoreId.Value, out store_id);

        this.commonAppSession.SelectedStoreSession.SelectedContactBusinessPk = business_clinic_pk;
        this.commonAppSession.SelectedStoreSession.storeID = store_id;
        this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = this.contactLogPk;
        this.commonAppSession.SelectedStoreSession.referrerPath = "walgreensLocalClinicDetails.aspx";
        Session.Remove(this.hfBusinessClinicPk.Value);

        if (!string.IsNullOrEmpty(this.hfIsPreviousSeasonLog.Value) && Convert.ToBoolean(this.hfIsPreviousSeasonLog.Value) && Convert.ToDateTime(this.hfScheduledDate.Value) < Convert.ToDateTime("05/01/2015"))
            Response.Redirect("walgreensClinicAgreementPrevSeason.aspx");
        else
            Response.Redirect("walgreensClinicAgreement.aspx");
    }

    /// <summary>
    /// Handler to check server side validations
    /// </summary>
    private bool ValidationsHandler()
    {
        bool is_valid = true;
        string failed_clinic_locations = string.Empty;
        bool has_min_est_shots = false, has_min_adm_shots = false;
        bool has_clinic_before_2weeks = false, clinic_date_changed_2weeks = false;
        List<string> lst_flu_expiry_dates = new List<string>();
        List<string> lst_routine_expiry_dates = new List<string>();
        bool is_having_override = true, est_qnt_increased = false;
        TextBox txt_clinic_immunization_shots, txt_clinic_administered_shots;
        CheckBox chk_no_clinic;
        GridView grd_clinic_immunizations;
        //string max_qty_error = string.Empty;
        int clinic_count = 1;
        bool has_voucher = false, voucher_alert_needed = false;

        var old_clinic_details_xml = ((XmlDocument)Session[this.hfBusinessClinicPk.Value]);
        var new_clinic_details_xml = XDocument.Parse(this.xmlClinicDetails.InnerXml);
        var old_imm_list = old_clinic_details_xml.SelectNodes("/clinicDetails/clinicLocations/clinicLocation/Immunizations/Immunization/@estimatedQuantity").Cast<XmlNode>().ToList();
        var new_imm_list = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation/Immunizations/Immunization/@estimatedQuantity").Cast<XmlNode>().ToList();

        var clinic_dates = from dates in new_clinic_details_xml.Descendants("clinicLocation")
                           where dates.Attribute("isNoClinic").Value != "1"
                           select dates.Attribute("clinicDate").Value;

        //var imm_names_list = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation/Immunizations/Immunization/@immunizationName").Cast<XmlNode>().ToList();
        //var loc_names_list = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation/@naClinicLocation").Cast<XmlNode>().ToList();
        var is_no_clinic = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation/@isNoClinic").Cast<XmlNode>().ToList();
        //Compare with previous data and then validate
        if (old_imm_list != new_imm_list && is_no_clinic[0].Value.ToString() != "1")
        {
            for (int i = 0; i < old_imm_list.Count; i++)
            {
                int est_shots_count;
                int old_est_shots;

                int.TryParse(new_imm_list[i].Value, out est_shots_count);
                int.TryParse(old_imm_list[i].Value, out old_est_shots);

                //priority will be for est qty increased below 2weeks
                if ((est_shots_count > old_est_shots) && Convert.ToDateTime(clinic_dates.FirstOrDefault()).Date < DateTime.Now.Date.AddDays(14))
                    est_qnt_increased = true;
            }
        }
        foreach (GridViewRow imm_row in grdImmunizationChecks.Rows)
        {
            var ctl_date_picker = imm_row.FindControl("txtVaccineExpirationDate");
            bool is_voucher_needed = (((DropDownList)imm_row.FindControl("ddlVoucher")).SelectedItem.Value.ToString() == "Yes");
            string vaccine_expiry_date = (ctl_date_picker != null) ? ((TextBox)ctl_date_picker).Text.ToString() : string.Empty;
            Label lbl_Immunization = (Label)imm_row.FindControl("lblImmunizationCheck");
            if (!string.IsNullOrEmpty(vaccine_expiry_date) && Convert.ToDateTime(vaccine_expiry_date) != Convert.ToDateTime("1/1/0001"))
            {
                if (lbl_Immunization.Text.ToLower().Contains("influ") && !lst_flu_expiry_dates.Contains(vaccine_expiry_date))
                    lst_flu_expiry_dates.Add(vaccine_expiry_date);
                else if (!lbl_Immunization.Text.ToLower().Contains("influ") && !lst_routine_expiry_dates.Contains(vaccine_expiry_date))
                    lst_routine_expiry_dates.Add(vaccine_expiry_date);
            }
            if (is_voucher_needed)
                has_voucher = true;
        }
        foreach (GridViewRow row in grdLocations.Rows)
        {
            chk_no_clinic = (CheckBox)row.FindControl("chkNoClinic");
            bool is_estimated_qnt_exists = false;
            var date_control = row.FindControl("PickerAndCalendarFrom");
            DateTime clinic_date = ((PickerAndCalendar)date_control).getSelectedDate;
            int total_est_shots_count = 0, adm_shots_count = 0;
            var flu_date_picker = row.FindControl("pcFluExpiryDate");
            string flu_expiry_date = (flu_date_picker != null && chk_no_clinic.Checked && this.isFluImmunizationExists) ? ((PickerAndCalendar)flu_date_picker).getSelectedDate.ToString("MM/dd/yyyy") : string.Empty;
            var routine_date_picker = row.FindControl("pcRoutineExpiryDate");
            string routine_expiry_date = (routine_date_picker != null && chk_no_clinic.Checked && this.isRoutineImmunizationExists) ? ((PickerAndCalendar)routine_date_picker).getSelectedDate.ToString("MM/dd/yyyy") : string.Empty;

            if (!string.IsNullOrEmpty(flu_expiry_date) && this.isFluImmunizationExists && Convert.ToDateTime(flu_expiry_date) != Convert.ToDateTime("1/1/0001") && !lst_flu_expiry_dates.Contains(flu_expiry_date))
                lst_flu_expiry_dates.Add(flu_expiry_date);

            if (!string.IsNullOrEmpty(routine_expiry_date) && this.isRoutineImmunizationExists && Convert.ToDateTime(routine_expiry_date) != Convert.ToDateTime("1/1/0001") && !lst_routine_expiry_dates.Contains(routine_expiry_date))
                lst_routine_expiry_dates.Add(routine_expiry_date);


            if (!chk_no_clinic.Checked) //If not voucher only clinic
            {
                string clinic_location_name = ((Label)row.FindControl("lblClinicLocation")).Text;
                if (row.RowIndex != 0)
                    grd_clinic_immunizations = (GridView)row.FindControl("grdClinicImmunizations");
                else
                    grd_clinic_immunizations = this.grdImmunizationChecks;

                foreach (GridViewRow imm_row in grd_clinic_immunizations.Rows)
                {
                    txt_clinic_immunization_shots = (TextBox)imm_row.FindControl("txtEstimatedQuantity");
                    txt_clinic_administered_shots = (TextBox)imm_row.FindControl("txtTotalImmAdministered");
                    string immunization_name = ((Label)imm_row.FindControl("lblImmunizationCheck")).Text;
                    if (!string.IsNullOrEmpty(txt_clinic_immunization_shots.Text) && this.updatedAction.ToLower() != "completed")
                    {
                        int est_shots_count = 0;
                        Int32.TryParse(txt_clinic_immunization_shots.Text, out est_shots_count);
                        if (this.blockedOutImmunizations.Contains(immunization_name.ToLower().Trim()) && est_shots_count > 0)
                            is_estimated_qnt_exists = true;

                        total_est_shots_count += est_shots_count;
                    }
                    if (txt_clinic_administered_shots != null && !string.IsNullOrEmpty(txt_clinic_administered_shots.Text))
                        adm_shots_count += Convert.ToInt32(txt_clinic_administered_shots.Text);
                }

                if (clinic_date != Convert.ToDateTime("1/1/0001") && is_estimated_qnt_exists)
                {
                    if (clinic_date >= Convert.ToDateTime("04/15/2017") && clinic_date <= Convert.ToDateTime("07/15/2017"))
                    {
                        failed_clinic_locations += "\\n" + "Clinic Location" + clinic_location_name;
                    }
                }
                if (total_est_shots_count < 25)
                    has_min_est_shots = true;
                if (adm_shots_count < 25)
                    has_min_adm_shots = true;

            }
            else
            {
                if (!has_voucher)
                    voucher_alert_needed = true;
            }
        }
        //Check duration between clinic scheduled on and clinic date
        foreach (var clinic_date in clinic_dates)
        {
            if (clinic_count == 1)
            {
                if (old_clinic_details_xml != null && !string.IsNullOrEmpty(old_clinic_details_xml.SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicDate").Value))
                {
                    DateTime previous_clinic_date = Convert.ToDateTime(old_clinic_details_xml.SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicDate").Value);
                    //DateTime clinic_scheduled_date = Convert.ToDateTime(old_clinic_details_xml.SelectSingleNode("/clinicDetails/clinicLocations/clinicLocation/@clinicScheduledOn").Value);

                    if (previous_clinic_date.Date != Convert.ToDateTime(clinic_date).Date && Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14))
                    {
                        clinic_date_changed_2weeks = true;
                    }
                }
            }
            else
            {
                if (Convert.ToDateTime(clinic_date).Date < DateTime.Now.Date.AddDays(14))
                {
                    has_clinic_before_2weeks = true;
                }
            }

            clinic_count++;
        }
        //Fire Alerts

        if (!string.IsNullOrEmpty(failed_clinic_locations))
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "blackoutImmunizationValidationMessageForDetails") + failed_clinic_locations);
            is_having_override = false;
        }

        if (this.updatedAction.ToLower() == "completed")
        {
            if (has_min_adm_shots)
                this.infoMsgList.Add((string)GetGlobalResourceObject("errorMessages", "minimumShotsRequiredInfo"));
        }
        else
        {
            if (has_min_est_shots)
            {
                this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "minimumShotsRequiredAlert"));
                if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    is_having_override = false;
            }
            if (clinic_date_changed_2weeks)
            {
                this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "clinicChangedBefore2Weeks"));
                if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    is_having_override = false;

            }
            if (est_qnt_increased)
            {
                this.validationMsgList.Add("<b style=\"color: red;\">" + (string)GetGlobalResourceObject("errorMessages", "immQtyIncreasedBefore2Weeks") + "</b>");
                if (!this.commonAppSession.LoginUserInfoSession.IsAdmin)
                    is_having_override = false;
            }
        }

        if (has_clinic_before_2weeks)
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "clinicCreatedBefore2Weeks"));
            if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                is_having_override = false;
        }
        if (voucher_alert_needed)
        {
            this.validationMsgList.Add("At least one vaccine with <b>Voucher needed</b> should be available to conduct Voucher Distribution");
            is_having_override = false;
        }
        //Check clinic date and time overlap
        XmlNodeList clinic_location_nodes = this.xmlClinicDetails.SelectNodes("/clinicDetails/clinicLocations/clinicLocation[@isReassign='0']");
        string failed_location = "";
        if (!Convert.ToBoolean(this.hfIsCancelled.Value))
        {
            ApplicationSettings.checkDateTimeStampValidation(clinic_location_nodes, out failed_location, "localdetails", this.lblClientName.Text, string.Empty);
            if (!String.IsNullOrEmpty(failed_location))
            {
                this.showAlertMessage = true;
                //calling this method to set min date css for clinic date picker
                this.bindClinicLocations();
                is_having_override = false;
                this.validationMsgList.Add(failed_location);
            }
        }

        if (lst_flu_expiry_dates.Count > 1)
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "fluExpiryDateMismatchAlert"));
            is_having_override = false;
        }

        if (lst_routine_expiry_dates.Count > 1)
        {
            this.validationMsgList.Add((string)GetGlobalResourceObject("errorMessages", "routineExpiryDateMismatchAlert"));
            is_having_override = false;
        }
        if (this.validationMsgList.Count > 0 || this.infoMsgList.Count > 0)
        {
            this.showValidationSummary(is_having_override);
            is_valid = false;
        }
        return is_valid;
    }

    /// <summary>
    /// Shows Validation summary to the user before sending email/saving agreement.
    /// </summary>
    /// <param name="isSendEmail"></param>
    /// <param name="is_confirmation_alert"></param>
    private void showValidationSummary(bool is_confirmation_alert)
    {
        string validation_summary = string.Empty;
        if (this.validationMsgList.Count > 0)
        // if (!string.IsNullOrEmpty(validation_summary) && isConfirmationAlert)
        {
            validation_summary = "<ul>";
            foreach (var item in this.validationMsgList)
            {
                validation_summary += "<li style=\"text-align:left\">" + item.Replace("\n", "<br />").Trim() + "</li><br/>";
            }
            validation_summary += "</ul>";
            if (is_confirmation_alert)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryWarning('" + validation_summary + "','continuesaving');", true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showValidationSummaryAlert('" + validation_summary + "','continuesaving');", true);
            validation_summary = "";
            this.validationMsgList.Clear();
        }
        else if (this.infoMsgList.Count > 0)
        {
            this.infoMsgList = this.infoMsgList.Distinct().ToList();
            validation_summary = "<ul>";
            foreach (var item in this.infoMsgList)
            {
                validation_summary += "<li style=\"text-align:left\">" + item.Replace("\n", "<br />").Trim() + "</li><br/>";
            }
            validation_summary += "</ul>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "showInformationMessage('" + validation_summary + "','continuesaving');", true);
            validation_summary = "";
            this.infoMsgList.Clear();
        }
    }

    /// <summary>
    /// Set minimum and maximum dates for date controls.
    /// </summary>
    private void setMinMaxDates()
    {
        foreach (GridViewRow row in this.grdLocations.Rows)
        {
            var flu_exp_date = row.FindControl("pcFluExpiryDate");
            var routine_exp_date = row.FindControl("pcRoutineExpiryDate");
            var clinic_date = row.FindControl("PickerAndCalendarFrom");
            if (flu_exp_date != null)
            {
                if (DateTime.Now < Convert.ToDateTime(((PickerAndCalendar)flu_exp_date).getSelectedDate.ToString()))
                {
                    ((PickerAndCalendar)flu_exp_date).MinDate = DateTime.Now.AddDays(-1);
                    ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
                }
                else
                {
                    ((PickerAndCalendar)flu_exp_date).MinDate = Convert.ToDateTime(((PickerAndCalendar)flu_exp_date).getSelectedDate.ToString()).AddDays(-1);
                    ((PickerAndCalendar)flu_exp_date).MaxDate = ApplicationSettings.getVoucherMaxExpDate.AddDays(1);
                }
            }
            if (routine_exp_date != null)
            {
                if (DateTime.Now < Convert.ToDateTime(((PickerAndCalendar)routine_exp_date).getSelectedDate.ToString()))
                {
                    ((PickerAndCalendar)routine_exp_date).MinDate = DateTime.Now.AddDays(-1);
                    ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
                }
                else
                {
                    ((PickerAndCalendar)routine_exp_date).MinDate = Convert.ToDateTime(((PickerAndCalendar)routine_exp_date).getSelectedDate.ToString()).AddDays(-1);
                    ((PickerAndCalendar)routine_exp_date).MaxDate = DateTime.Today.AddYears(1).AddDays(1);
                }
            }
            if (clinic_date != null)
            {
                this.setClinicDates(row, false);
            }
        }
    }

    /// <summary>
    /// Sets min/max dates for clinics
    /// </summary>
    /// <param name="row"></param>
    private void setClinicDates(GridViewRow row, bool is_from_grid_load)
    {
        DropDownList ddl_states = (DropDownList)row.FindControl("ddlState");
        var date_control = row.FindControl("PickerAndCalendarFrom");

        if (grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString().Trim().Length != 0)
        {
            string clinic_date = grdLocations.DataKeys[row.RowIndex].Values["clinicDate"].ToString();
            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                ((PickerAndCalendar)date_control).MinDate = ApplicationSettings.getOutreachStartDate;
            else
            {
                if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                {
                    if (Convert.ToDateTime(clinic_date) >= DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                        ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
                    else if (Convert.ToDateTime(clinic_date) < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                        ((PickerAndCalendar)date_control).SetMinDate = Convert.ToDateTime(clinic_date);
                }
                else if (Convert.ToDateTime(clinic_date) > DateTime.Today.Date)
                {
                    if (DateTime.Now.AddDays(14) < Convert.ToDateTime(clinic_date)
                        && !ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                        ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(13);
                    else if (ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                        ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
                    else
                        ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
                }
                else
                    ((PickerAndCalendar)date_control).MinDate = Convert.ToDateTime(clinic_date).AddDays(-1);
            }
            if (is_from_grid_load)
            {
                ((PickerAndCalendar)date_control).getSelectedDate = Convert.ToDateTime(clinic_date);
                ((TextBox)row.FindControl("txtCalenderFrom")).Text = Convert.ToDateTime(clinic_date).ToString("MM/dd/yyyy");
            }
        }
        else
        {
            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                ((PickerAndCalendar)date_control).MinDate = ApplicationSettings.getOutreachStartDate;
            else if ((ddl_states.SelectedValue == "MO" || ddl_states.SelectedValue == "DC") && DateTime.Now < DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate")))
                ((PickerAndCalendar)date_control).SetMinDate = DateTime.Parse(ApplicationSettings.getStoreStateRestrictions("minClinicDate"));
            else
            {
                if (!ApplicationSettings.isAuthorisedForBelow2Weeks(this.commonAppSession.LoginUserInfoSession.UserRole))
                    ((PickerAndCalendar)date_control).SetMinDate = DateTime.Now.AddDays(14);
                else
                    ((PickerAndCalendar)date_control).MinDate = DateTime.Now.AddDays(-1);
            }
        }
    }

    /// <summary>
    /// Shows or hides voucher details based on clinic/no clinic.
    /// </summary>
    /// <param name="row"></param>
    private void showHideVoucherDetails(GridViewRow row)
    {
        CheckBox chk_no_clinic = (CheckBox)row.FindControl("chkNoClinic");
        ImageButton img_btn_flu_voucher = (ImageButton)row.FindControl("imgBtnFluVoucher");
        ImageButton img_btn_routine_voucher = (ImageButton)row.FindControl("imgBtnRoutineVoucher");
        ImageButton img_btn_flu_voucher_spa = (ImageButton)row.FindControl("imgBtnFluVoucherSpa");
        ImageButton img_btn_routine_voucher_spa = (ImageButton)row.FindControl("imgBtnRoutineVoucherSpa");
        ImageButton img_btn_var_eng = (ImageButton)row.FindControl("imgBtnVarEng");
        ImageButton img_btn_var_spa = (ImageButton)row.FindControl("imgBtnVarSpa");
        PickerAndCalendar flu_exp_date = (PickerAndCalendar)row.FindControl("pcFluExpiryDate");
        PickerAndCalendar routine_exp_date = (PickerAndCalendar)row.FindControl("pcRoutineExpiryDate");
        TextBox txt_flu_expiry_date = ((TextBox)row.FindControl("txtFluExpiryDate"));
        TextBox txt_routine_expiry_date = ((TextBox)row.FindControl("txtRoutineExpiryDate"));
        System.Web.UI.HtmlControls.HtmlTableCell cell_download_btns = (System.Web.UI.HtmlControls.HtmlTableCell)row.FindControl("cellDownloadBtns");

        bool is_voucher = true;
        XmlDocument clinic_details = ((XmlDocument)Session[this.hfBusinessClinicPk.Value]);
        if (clinic_details != null)
        {
            XmlNodeList clinic_location_nodes = clinic_details.SelectNodes("/clinicDetails/clinicLocations/clinicLocation");
            is_voucher = (clinic_location_nodes[0].Attributes["isNoClinic"].Value.ToString() == "1");
        }

        if (row.RowIndex == 0)
        {
            if (is_voucher && chk_no_clinic.Checked)
            {
                flu_exp_date.Visible = false;
                routine_exp_date.Visible = false;
                txt_flu_expiry_date.Enabled = false;
                txt_routine_expiry_date.Enabled = false;
                txt_flu_expiry_date.Visible = this.isFluImmunizationExists;
                txt_routine_expiry_date.Visible = this.isRoutineImmunizationExists;
                cell_download_btns.Visible = true;

                this.btnConfirmedClinic.Text = "Vouchers Provided";
                this.btnConfirmedClinicDim.Text = "Vouchers Provided";
            }
            else if (!is_voucher && !chk_no_clinic.Checked)
            {
                cell_download_btns.Visible = true;
            }
            else if (clinic_details != null)
            {
                cell_download_btns.Visible = false;
                this.btnConfirmedClinic.Text = "Confirmed Clinic";
                this.btnConfirmedClinicDim.Text = "Confirmed Clinic";
            }
        }
        else if (chk_no_clinic.Checked)
        {
            flu_exp_date.Visible = this.isFluImmunizationExists;
            routine_exp_date.Visible = this.isRoutineImmunizationExists;
            txt_flu_expiry_date.Visible = false;
            txt_routine_expiry_date.Visible = false;
            cell_download_btns.Visible = false;
        }
        else
        {
            cell_download_btns.Visible = false;
            //this.btnConfirmedClinic.Text = "Confirmed Clinic";
            //this.btnConfirmedClinicDim.Text = "Confirmed Clinic";
        }

        //img_btn_flu_voucher.Visible = (chk_no_clinic.Checked || this.isFluVoucherNeeded) && this.isFluImmunizationExists;
        //img_btn_flu_voucher_spa.Visible = (chk_no_clinic.Checked || this.isFluVoucherNeeded) && this.isFluImmunizationExists;
        //img_btn_routine_voucher.Visible = (chk_no_clinic.Checked || this.isRoutineVoucherNeeded) && this.isRoutineImmunizationExists;
        //img_btn_routine_voucher_spa.Visible = (chk_no_clinic.Checked || this.isRoutineVoucherNeeded) && this.isRoutineImmunizationExists;
        img_btn_flu_voucher.Visible = this.isFluVoucherNeeded;
        img_btn_flu_voucher_spa.Visible = this.isFluVoucherNeeded;
        img_btn_routine_voucher.Visible = this.isRoutineVoucherNeeded;
        img_btn_routine_voucher_spa.Visible = this.isRoutineVoucherNeeded;

        img_btn_var_eng.Visible = !chk_no_clinic.Checked;
        img_btn_var_spa.Visible = !chk_no_clinic.Checked;

        if (cell_download_btns.Visible)
            cell_download_btns.Visible = this.isDownloadButtonsVisible(row);
    }

    /// <summary>
    /// Checks if download buttons should be displayed or not
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private bool isDownloadButtonsVisible(GridViewRow row)
    {
        if (this.txtGroupId.Text.Contains("pending copay group id"))
        {
            return false;
        }
        return true;
    }
    #endregion

    #region ------------ PRIVATE VARIABLES -----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private WalgreenEmail walgreensEmail = null;
    private int contactLogPk;
    private string updatedAction
    {
        get
        {
            string result = string.Empty;

            if (ViewState["updatedAction"] != null)
            {
                result = (string)ViewState["updatedAction"];
            }
            return result;
        }
        set
        {
            ViewState["updatedAction"] = value;
        }
    }
    private DataTable dtClinicImmunizations;
    private XmlDocument xmlClinicDetails;
    private bool showAlertMessage = false;
    //Logger Logger = LogManager.GetCurrentClassLogger();

    //Clinic dates will be blocked out if one of the immunizations has seleted "Influenza - Standard/PF Injectable (trivalent)", "Influenza - Standard injectable Quadrivalent", "Influenza - High Dose"
    private string[] blockedOutImmunizations = { "influenza - standard/pf injectable (trivalent)", "influenza - standard injectable quadrivalent", "influenza - high dose" };
    private bool isDisableEditClinicDetails = false;
    private bool isStoreEditable = false;
    private bool isMOPreviousSeasonBusiness
    {
        get
        {
            return Convert.ToBoolean(hfIsMoPrevious.Value);
        }
        set
        {
            hfIsMoPrevious.Value = value.ToString();
        }
    }

    private DataTable previousClinicLocation
    {
        get
        {
            DataTable result = new DataTable();

            if (ViewState["previousClinicLocation"] != null)
            {
                result = (DataTable)ViewState["previousClinicLocation"];
            }
            return result;
        }
        set
        {
            ViewState["previousClinicLocation"] = value;
        }
    }

    public DropDownList ddlClinicLocations { get; set; }

    public ImageButton imgBtnAddLocation { get; set; }

    private bool isAddressDisabled
    {
        get
        {
            bool value = false;
            if (ViewState["isAddressDisabled"] != null)
                value = (bool)ViewState["isAddressDisabled"];
            return value;
        }
        set
        {
            ViewState["isAddressDisabled"] = value;
        }
    }

    private bool isRestrictedStoreState
    {
        get
        {
            bool value = false;
            if (ViewState["isRestrictedStoreState"] != null)
                value = (bool)ViewState["isRestrictedStoreState"];
            return value;
        }
        set
        {
            ViewState["isRestrictedStoreState"] = value;
        }
    }
    protected bool isFluImmunizationExists
    {
        get
        {
            bool result = false;

            if (ViewState["isFluImmunizationExists"] != null)
            {
                result = (bool)ViewState["isFluImmunizationExists"];
            }
            return result;
        }
        set
        {
            ViewState["isFluImmunizationExists"] = value;
        }
    }
    protected bool isRoutineImmunizationExists
    {
        get
        {
            bool result = false;

            if (ViewState["isRoutineImmunizationExists"] != null)
            {
                result = (bool)ViewState["isRoutineImmunizationExists"];
            }
            return result;
        }
        set
        {
            ViewState["isRoutineImmunizationExists"] = value;
        }
    }
    protected bool isCTIExists
    {
        get
        {
            bool result = false;

            if (ViewState["isCTIExists"] != null)
            {
                result = (bool)ViewState["isCTIExists"];
            }
            return result;
        }
        set
        {
            ViewState["isCTIExists"] = value;
        }
    }
    protected bool isFluVoucherNeeded
    {
        get
        {
            bool result = false;

            if (ViewState["isFluVoucherNeeded"] != null)
            {
                result = (bool)ViewState["isFluVoucherNeeded"];
            }
            return result;
        }
        set
        {
            ViewState["isFluVoucherNeeded"] = value;
        }
    }
    protected bool isRoutineVoucherNeeded
    {
        get
        {
            bool result = false;

            if (ViewState["isRoutineVoucherNeeded"] != null)
            {
                result = (bool)ViewState["isRoutineVoucherNeeded"];
            }
            return result;
        }
        set
        {
            ViewState["isRoutineVoucherNeeded"] = value;
        }
    }
    private List<string> validationMsgList
    {
        get
        {
            if (!(ViewState["validationSummary"] is List<string>))
            {
                ViewState["validationSummary"] = new List<string>();
            }

            return (List<string>)ViewState["validationSummary"];
        }
        set
        {
            ViewState["validationSummary"] = value;
        }
    }
    private List<string> infoMsgList
    {
        get
        {
            if (!(ViewState["infoMsgList"] is List<string>))
            {
                ViewState["infoMsgList"] = new List<string>();
            }

            return (List<string>)ViewState["infoMsgList"];
        }
        set
        {
            ViewState["infoMsgList"] = value;
        }
    }
    private bool isValid = true;
    #endregion

    #region ------------ PUBLIC FUNCTIONS ------------
    [WebMethod]
    public static void logJSerrors(string log_details)
    {
        AppCommonSession common_app_session = new AppCommonSession();
        Logger logger = LogManager.GetLogger("ClinicDetailsLogger");
        logger.Info("Clinic Details Update - Local ; Username::" + common_app_session.LoginUserInfoSession.UserName + "; " + log_details);
    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        this.walgreensEmail = ApplicationSettings.emailSettings();
        this.updatedAction = string.Empty;
        this.dtClinicImmunizations = new DataTable();
        this.xmlClinicDetails = new XmlDocument();
    }
}