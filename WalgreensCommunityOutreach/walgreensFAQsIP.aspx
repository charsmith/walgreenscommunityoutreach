﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensFAQsIP.aspx.cs"
    Inherits="walgreensFAQsIP" %>

<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript" src="javaScript/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <style type="text/css">
        .linkColor a:link
        {
            color: #3096D8;
        }
        .linkColor a:visited
        {
            color: #3096D8;
        }
        .linkColor a:hover
        {
            color: #3096D8;
        }
        .linkColor a:active
        {
            color: #3096D8;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0" class="linkColor">
                    <tr>
                        <td valign="top" class="pageTitle">
                            FAQs
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table width="890" border="0" cellspacing="20" cellpadding="0">
                                <tr>
                                    <td width="562" class="bestPracticesText">
                                        <span class="class4"><a href="#businessGrid" class="class4">Which businesses am I responsible
                                            for contacting?</a></span><br />
                                        <span class="class4"><a href="#potentialLocalBusinesses" class="class4">What are Potential
                                            local Businesses?</a></span><br />
                                        <span class="class4"><a href="#scheduledClinics" class="class4">What are Scheduled Clinics?</a></span><br />
                                        <span class="class4"><a href="#doNotCall" class="class4">What are Do Not Call - National
                                            Contracts?</a></span><br />
                                        <span class="class4"><a href="#contactLogger" class="class3">How do I log a contact
                                            I've made?</a></span><br />
                                        <span class="class4"><a href="#contractForm">Where can I find the Community Off-Site
                                            Agreement form (Contract)?</a></span><br />
                                        <span class="class4"><a href="#contractNext">What happens after I email the Community
                                            Off-Site Agreement form (Contract) to the business?</a></span><br />
                                        <span class="class4"><a href="#contactLog" class="class3">How do I view past contacts
                                            I've logged?</a></span><br />
                                        <span class="class4"><a href="#noBusinesses" class="class4">What do I do if no businesses
                                            have been assigned to my store for an outreach program?</a></span><br />
                                        <span class="class4"><a href="#viewStores" class="class4">How do I view other Walgreens
                                            stores in my District or Community?</a></span><br />
                                        <span class="class4"><a href="#addBusinesses" class="class4">How do I add a business
                                            that I want to contact that is not one of my assigned businesses?</a></span><br />
                                        <span class="class4"><a href="#addOutreach" class="class4">How do I add a business from
                                            one Outreach Campaign to another of my Outreach Campaigns?</a></span><br />
                                        <span class="class4"><a href="#multipleEfforts" class="class4">Why do some of my businesses
                                            have more than one contact listed in the grid?</a></span><br />
                                        <span class="class4"><a href="#hhsVouchers" class="class4">How do I setup a business
                                            for the Charity Program (Charity Clinic)?</a></span><br />
                                        <span class="class4"><a href="#postClinic" class="class4">After I perform a clinic,
                                            where do I log the number of immunizations administered?</a></span><br />
                                    </td>
                                    <td width="268" align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/outreachPortal.jpg" width="268" height="202" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="businessGrid" id="businessGrid"></a>Which businesses am I responsible for
                                                contacting?</b></p>
                                        <p>
                                            Your store is responsible for contacting businesses listed under Potential Local
                                            Business tab on the <a href="walgreensHome.aspx">Home</a> page of the outreach program.
                                            These businesses appear in a grid along with all of the businesses pertinent contact
                                            information. Clicking the <a href="walgreensMap.aspx">Map</a> button in the navigation
                                            bar will allow you to view the businesses locations in reference to your store.
                                            You may also get a listing of your businesses by running the <a href="reports/walgreensReport.aspx">
                                                Store Business Report</a> located under Reports in the navigation bar.</p>
                                        <p>
                                            You will also be notified by email if you have been assigned a Corporate Offsite
                                            clinic. These Corporate Clients will appear under the Scheduled Clinics tab. If
                                            your store is assigned one of these clinics you will be required to confirm and
                                            perform the clinic. Any contacts made should be logged to the site, as well as post
                                            clinic information such as immunizations administered.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/potentialLocalBusinesses.jpg" width="268" height="122" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="potentialLocalBusinesses" id="potentialLocalBusinesses"></a>What are Potential
                                                Local Businesses?</b></p>
                                        <p>
                                            Potential Local Businesses are businesses selected with targeted categories/segments
                                            that have a greater potential for contracting an immunization solution. Each store
                                            is assigned up to 40 businesses to contact within an immunization season. Dialog
                                            is provided in the <a href="walgreensJobAids.aspx">Job Aids</a> section
                                            of this site to assist with these calls. Every contact with these businesses should
                                            be logged in the site to track your progress and to assist with future calls. Once
                                            a contract has been initiated and approved, the business will move from the Potential
                                            Local Businesses tab to the Scheduled Clinics tab.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/potentialLocalBusinesses.jpg" width="268" height="122" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="scheduledClinics" id="scheduledClinics"></a>What are Scheduled Clinics?</b></p>
                                        <p>
                                            There are two Clinic Types that make up Scheduled Clinics: Corporate Clinics and
                                            Local Clinics.</p>
                                        <p>
                                            Corporate Clinics are scheduled clinics assigned to a store by a District Manager from
                                            a Walgreens Corporate Sales Representative. These clinics need to be confirmed and
                                            performed by the store they are assigned to. Contacts and Clinic Completion with
                                            these Corporate Clients need to be logged in the site. The details of these clinics
                                            can be viewed by clicking on the &quot;View/Edit&quot; link in the Clinic details
                                            column of the grid. The number of immunizations administered is required in this
                                            section as well, after the clinic has been performed.</p>
                                        <p>
                                            Local Clinics are clinics that the store has contracted. Once a business approves
                                            an initiated contract, the business moves from &quot;Potential Local Businesses&quot;
                                            to &quot;Scheduled Clinics&quot;. Contacts and Clinic Completion with these Local
                                            Businesses need to be logged in the site as well. The details of these clinics can
                                            be viewed by clicking on the &quot;View/Edit&quot; link in the Clinic details column
                                            of the grid. The number of immunizations administered is required in this section
                                            as well, after the clinic has been performed.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/scheduledClinics.jpg" width="268" height="106" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="doNotCall" id="doNotCall"></a>What are Do Not Call - National Contracts?</b></p>
                                        <p>
                                            The Do Not Call - National Contracts is a list of National Clients that should not
                                            be contacted by stores for a sales type call. These National Businesses are handled
                                            by a Walgreens Corporate Sales team. The only time a store should contact these
                                            National Clients is if they are assigned an Off-Site Clinic to setup and perform.
                                            There is no action that is required by the stores under this tab, it is simply to
                                            provide awareness of National Clients.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/doNotCall.jpg" width="268" height="145" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="contactLogger" id="contactLogger"></a>How do I log a contact I've made?</b></p>
                                        <p>
                                            To log a recent contact, use the Log Business Contact form on the right of the <a
                                                href="walgreensHome.aspx">Home</a> or <a href="walgreensContactLog.aspx">Contact Log</a>
                                            pages. Select the business you contacted from the Business Contacted drop down menu.
                                            This will populate the Contact Name fields with any available information. You may
                                            update the Contact Name if the listed name is different than the appropriate contact
                                            at that business. Select the correct Contact Date if it is different than the current
                                            date. Select an Outreach Status based on the results of your outreach call. Enter
                                            any Feedback or Notes that may aid you or your associates in future calls. Finally
                                            click the Submit Entry button to complete your log. A success message will appear
                                            to let you know that your log has been added to the Contact Log. From the <a href="walgreensContactLog.aspx">
                                                Contact Log</a> page, you may view the entire log for each business or remove
                                            an individual log if you have submitted a log incorrectly.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/contactLogger.jpg" width="145" height="236" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="contractForm" id="contractForm"></a>Where can I find the Community Off-Site
                                                Agreement form (Contract)?</b></p>
                                        <p>
                                            After Submitting a "Contract Initiated" status in the Business Log, a Community
                                            Off-Site Agreement form will pop up. This contract form may only be edited by a
                                            Walgreens user. The contract may be filled out immediately after submitting the
                                            "Contract Initiated" status or the user can return to the contract later by clicking
                                            the "Contract Initiated" link from the potential local Businesses grid or Contact
                                            log entry.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/contractForm.jpg" width="268" height="238"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="contractNext" id="contractNext"></a>What happens after I email the Community
                                                Off-Site Agreement form (Contract) to the business?</b></p>
                                        <p>
                                            After emailing the Community Off-Site Agreement, the business will receive an email
                                            with the Subject line &quot;Walgreens Community Offsite Immunization Clinic Agreement&quot;.
                                            This email will contain a link to the Agreement that you have setup. The Agreement
                                            the business receives contains highlighted fields that the business contact will need to complete. The client also has the
                                            ability to Approve or Reject the Agreement. If they Reject the Agreement, a notes
                                            section is provided for the client to provide required changes for their Approval.
                                            You will receive an email notifying you of the rejection. You can then return to
                                            the Contract by clicking the &quot;Contract Initiated&quot; link in the Status column
                                            of the business, make the requested changes and send another email to the business.
                                            Once the client Approves the Agreement and enters their name in the Electronic Signature field, the business will move under the Scheduled
                                            Clinics tab and you will be required to confirm, perform and complete the clinic.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/contractNext.jpg" width="268" height="286" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="contactLog" id="contactLog"></a>How do I view past contacts I've logged?</b></p>
                                        <p>
                                            Past log entries can be viewed on the <a href="walgreensContactLog.aspx">Contact Log</a>
                                            page. The screen will remain blank, until you select a business from the drop down
                                            menu in the header. Once you've selected a business the dates for each log entry
                                            will appear below the business name. Each date can be toggled open to show the contact
                                            information from that log entry. Some status may contain a link to open a secondary
                                            form if applicable to the status. If a log entry was made incorrectly they can be
                                            removed by clicking the Remove Entry link below the log entry.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/contactLog.jpg" width="268" height="138" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="noBusinesses" id="noBusinesses"></a>What do I do if no businesses have been
                                                assigned to my store for an outreach program?</b></p>
                                        <p>
                                            If you have relationships with local businesses that you want to contact as part
                                            of the outreach campaign, you will still have access to the site. If you have relationships
                                            with local businesses that you contact, you may attempt to add those businesses
                                            to the site. On the <a href="walgreensHome.aspx">Home</a> page there is a link above
                                            the business grid to Search/Add A Business. The link opens a form to allow you to
                                            enter the business information for the business you want to add. Fill out the form
                                            and click Submit. The system will do a quick search through existing businesses
                                            in the site and either add the business to your list or deny the add if the business
                                            is already assigned to another Walgreens store. If added the business will now appear
                                            in the business grid of the home page.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/addBusiness.jpg" width="268" height="193" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="viewStores" id="viewStores"></a>How do I view other Walgreens stores in
                                                my Region, District or Community?</b></p>
                                        <p>
                                            Region, District and Community users who have multiple stores associated with their
                                            login link can view other Walgreens stores associated to them by using the drop
                                            down menu labeled Store Name below the navigation bar. When you've selected a new
                                            store the store id number and address will appear below the outreach name in the
                                            header of the page. The same drop down menu of store locations will be found within
                                            some of the reports that apply to individual store locations.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/storeNameMenuIMZ.jpg" width="268" height="102" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="addBusinesses" id="addBusinesses"></a>How do I add a business that I want
                                                to contact that is not one of my assigned businesses?</b></p>
                                        <p>
                                            If you have relationships with local businesses that you want to contact as part
                                            of the outreach campaign, you may attempt to add those businesses to the site. On
                                            the <a href="walgreensHome.aspx">Home</a> page there is a link above the business
                                            grid to <a href="walgreensStoreBusiness.aspx">Add A Business</a>. The link opens
                                            a form to allow you to enter the business information for the business you want
                                            to add. Fill out the form and click Submit. The system will do a quick search through
                                            existing businesses in the site and either add the business to your list or deny
                                            the add if the business is already assigned to another Walgreens store. If added
                                            the business will now appear in the business grid of the home page.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/addBusiness.jpg" alt="" width="268" height="193" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="addOutreach" id="addOutreach"></a>How do I add a business from one Outreach
                                                Campaign to another of my Outreach Campaigns?</b></p>
                                        <p>
                                            If you have a relationship with a local business from another Outreach Campaign
                                            and would like to extend that relationship to other Outreach Campaigns, you can
                                            add the other Outreach Program through the <a href="walgreensEditBusinessProfile.aspx">
                                                Edit Business Profiles</a> screen. Edit the business profile in the Outreach
                                            Campaign in which the business is already assigned to your store. Once you have
                                            navigated to the View/Edit Business Profiles screen, select the business you want
                                            to add from the &quot;Local Business&quot; dropdown menu and select &quot;Outreach
                                            Campaigns&quot; from the &quot;Edit&quot; menu. Once selected, the form below will
                                            fill with all of the data about that business. The Outreach Campaigns will be displayed
                                            in the &ldquo;Outreach Campaigns&quot; section at the bottom of the page. If a campaign
                                            is checked, it is already assigned to the &quot;Store&quot; that is shown to the
                                            right of the campaign name. If it is already checked, you will not be able to add
                                            this business to your list of assigned businesses. If it is unchecked, you may check
                                            the Outreach Campaign. The campaign will be assigned to your store when you click
                                            the &quot;Submit&quot; button to complete the update.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/addOutreach.jpg" alt="" width="268" height="219" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="multipleEfforts" id="multipleEfforts"></a>Why do some of my businesses have
                                                more than one contact listed in the grid?</b></p>
                                        <p>
                                            You may notice that some businesses have multiple contacts, contact dates and statuses.
                                            These occur when a business is associated with more than one outreach campaign.
                                            If you see that a business has recently been contacted for another outreach campaign,
                                            you may want to postpone contacting them immediately about a different outreach
                                            campaign. Only the logs for the current outreach campaign may be affected, which
                                            is why the other contacts are grayed out. You will however be able to view all past
                                            logs for all outreach campaigns in the <a href="walgreensContactLog.aspx">Contact Log</a>.
                                            FI is the acronym for Flu Immunization and SO is the acronym for Senior Outreach.
                                            If you hold the cursor over the link it will also provide the full label.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/multipleEfforts.jpg" width="268" height="109" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="hhsVouchers" id="hhsVouchers"></a>How do I setup a business for the Charity
                                                Program (Charity Clinic)?</b></p>
                                        <p>
                                            If the business is an existing business in your Potential Local Businesses queue,
                                            select the business and change the status to &quot;Charity Program&quot; in the
                                            Contact Log. When you submit the log entry, you will be taken to the Charity Program
                                            screen. If you are performing a Charity Clinic, fill in all of the fields in the
                                            form. If you are only distributing vouchers, enter the number of &quot;Vouchers
                                            Distributed&quot;. When you &quot;Submit&quot; the form, the business will be moved
                                            under the &quot;Scheduled Clinics&quot; tab.</p>
                                        <p>
                                            If you are adding a new business that is part of the Charity Program, &quot;<a href="#addBusiness">Add
                                                A Business</a>&quot; as usual and check the &quot;Charity Program&quot; checkbox
                                            in the <a href="walgreensStoreBusiness.aspx">Add A Business form</a>. When you &quot;Submit&quot;
                                            the form, you will be taken to the &quot;Charity Program&quot; screen. If you are
                                            performing a Charity Clinic, fill in all of the fields in the form. If you are only
                                            distributing vouchers, enter the number of &quot;Vouchers Distributed&quot;. When
                                            you &quot;Submit&quot; the form, the business will be moved under the &quot;Scheduled
                                            Clinics&quot; tab.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/charityClinic.jpg" width="269" height="130" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" class="bestPracticesText">
                                        <p>
                                            <b><a name="postClinic" id="postClinic"></a>After I perform a clinic, how do I complete the clinic?</b></p>
                                        <p>
                                            After you have performed a clinic, additional details will need to be entered on the Clinic Details screen for the clinic. This page can be accesses by navigating to the Scheduled Clinics tab of the Store page and clicking the &quot;Open&quot; button in the &quot;Clinic Details&quot; column of the performed clinic. In the gold Billing &amp; Vaccine Information section, enter the number of vaccines administered for each immunization. Then in the red Pharmacist &amp; Post Clinic Information section, enter the name of the Rx host, their phone number, the total hours the clinic was held and any feedback. Once you have completed these sections, click the blue &quot;Clinic Completed&quot; button at the bottom of the screen. You will get a message that reads &quot;This clinic has been successfully logged as Completed.&quot;</p>
                                        <p>Even though you completed the clinic, you can still return to Clinic Details and update any of this information by making your edits and clicking &quot;Submit Changes&quot; at the bottom of the screen.</p>
                                    </td>
                                    <td align="left" valign="top" style="padding-top: 10px">
                                        <img src="images/faqs/postClinic.jpg" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
