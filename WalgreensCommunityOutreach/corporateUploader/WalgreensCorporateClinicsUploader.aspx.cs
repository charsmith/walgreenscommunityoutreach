﻿using System;
using System.Linq;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Data.OleDb;
using TdWalgreens;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using TdApplicationLib;
using System.Text;
using System.Xml.Linq;
using System.Web;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using NLog;

public partial class WalgreensCorporateClinicsUploader : Page
{
    #region --------------- PROTECTED EVENTS ---------------

    protected void Page_Load(object sender, EventArgs e)
    {
        this.rowUploadClinicType = (System.Web.UI.HtmlControls.HtmlTableRow)this.FindControl("rowClinicType");
        this.rowUploadClinicType.Visible = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "UploadLocalClinics") ? true : false;
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (this.corporateClinicUploadControl.HasFile)
        {
            this.resetValues();
            this.labelUploadedfile.Text = corporateClinicUploadControl.FileName;
            this.saveFileOnServerfolder(labelUploadedfile.Text);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string upload_timestamp = DateTime.Now.ToString("MM-dd-yyyy").Replace("-", "") + DateTime.Now.ToString("hh-mm-ss").Replace("-", "");

        try
        {
            if (this.rowUploadClinicType != null && this.rowUploadClinicType.Visible && this.ddlClinicType.SelectedValue == "select")
            {
                this.userMessage.Text = (string)GetGlobalResourceObject("errorMessages", "selectClinicType");
                this.labelUploadedfile.Text = "";
            }
            if (String.IsNullOrEmpty(this.userMessage.Text))
            {
                if (!String.IsNullOrEmpty(this.hfUploadedFile.Value))
                {
                    DataTable data_table = (new ExcelDataUpload(upload_timestamp)).readExcelFile(this.hfUploadedFile.Value, out this.validationError);

                    if (data_table == null || data_table.Rows.Count == 0)
                    {
                        if (File.Exists(this.hfUploadedFile.Value)) File.Delete(this.hfUploadedFile.Value);
                        this.userMessage.Text = (string.IsNullOrEmpty(this.validationError) ? (string)GetGlobalResourceObject("errorMessages", "invalidFile") : this.validationError.TrimEnd(';').Trim());
                        this.labelUploadedfile.Text = "";
                    }
                    else if (this.rowUploadClinicType != null && this.rowUploadClinicType.Visible && this.ddlClinicType.SelectedValue == "select")
                    {
                        this.userMessage.Text = (string)GetGlobalResourceObject("errorMessages", "selectClinicType");
                        this.labelUploadedfile.Text = "";
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(this.labelUploadedfile.Text))
                        {
                            int no_store_clinics;
                            no_store_clinics = data_table.Select("[By Pass to Hosting Store #] = ''").Count();
                            if (no_store_clinics <= 20)
                            {
                                int upload_type = 3; //Default to Corporate Upload
                                DataTable uploaded_records;
                                if (this.rowUploadClinicType != null && this.rowUploadClinicType.Visible)
                                    Int32.TryParse(this.ddlClinicType.SelectedValue, out upload_type);

                                uploaded_records = new DataTable();
                                bool is_override_user = ApplicationSettings.isdisableClientServices(this.commonAppSession.LoginUserInfoSession.UserName, "AccessOverlapDBForCorporateClinics");
                                uploaded_records = this.dbOperation.dumpCorporateDataToDatabase(data_table, "tblCorporateClinicsUploadData", upload_timestamp, this.commonAppSession.LoginUserInfoSession.UserID, upload_type, is_override_user);
                                this.lblRecordInserted.Text = uploaded_records.Select("isValidRecord = 'True'").Count().ToString() + "/" + uploaded_records.Rows.Count;
                                this.lblRecordFailed.Text = uploaded_records.Select("isValidRecord = 'False'").Count().ToString() + "/" + uploaded_records.Rows.Count;
                                this.lblErrorNote.Visible = uploaded_records.Select("isValidRecord = 'False'").Count() > 0;
                                this.reportBind(uploaded_records);
                            }
                            else
                                this.userMessage.Text = "Uploaded file contains 20 or more clinics without [By Pass to Hosting Store #]. Please split the file into multiple files with 20 or less records and upload.";
                        }
                        else
                        {
                            this.userMessage.Text = (string)GetGlobalResourceObject("errorMessages", "uploadValidFile");
                        }
                    }
                }
                else
                    this.userMessage.Text = (string)GetGlobalResourceObject("errorMessages", "uploadValidFile");
            }
        }
        catch (Exception ex)
        {
            this.labelUploadedfile.Text = "";
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Error((Session["logString"] != null ? "User Log:" + Session["logString"].ToString() : "") + "|Error message: " + ex.Message + " Stack Trace: " + ex.StackTrace);
            dbOperation.logError(-1, ex.Message + " " + (Session["logString"] != null ? "User Log:" + Session["logString"].ToString() : "") + "; Location: " + System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString(), ex.StackTrace);

            string error_message = "";
            error_message = (string)(System.Web.HttpContext.GetGlobalResourceObject("systemExceptionErrors", ex.GetType().Name));
            if (!string.IsNullOrEmpty(error_message))
            {
                HttpContext.Current.Session["displayErrorMessage"] = error_message;
                Response.Redirect("../ErrorPage.aspx");
            }
        }

        this.hfUploadedFile.Value = "";
    }

    #endregion

    #region --------------- PRIVATE METHODS ----------------
    /// <summary>
    /// Reset all values to initial state
    /// </summary>
    private void resetValues()
    {
        this.lblRecordInserted.Text = "0";
        this.lblRecordFailed.Text = "0";
        this.lblErrorNote.Visible = false;
        this.userMessage.Text = "";

        this.reportBind(new DataTable());
    }

    /// <summary>
    /// Save uploaded file on server
    /// </summary>
    /// <param name="file_name"></param>
    /// <returns></returns>
    private void saveFileOnServerfolder(string file_name)
    {
        string[] file_extensions = { ".xlsx", ".xlsm" };
        string file_path = Path.Combine(ApplicationSettings.GetCorporateClinicsUploadFilesPath, file_name);

        GC.Collect();
        GC.WaitForPendingFinalizers();
        if (file_extensions.Contains(Path.GetExtension(file_path)))
        {
            file_path = file_path.Replace(' ', '_').Substring(0, file_path.LastIndexOf('.')) + "_"
                                    + DateTime.Now.ToString("MM-dd-yyyy").Replace("-", "")
                                    + DateTime.Now.ToString("hh-mm-ss").Replace("-", "")
                                    + Path.GetExtension(file_path);

            if (!File.Exists(file_path))
            {
                this.corporateClinicUploadControl.SaveAs(file_path);
                this.hfUploadedFile.Value = file_path;
            }
        }
        else
            this.userMessage.Text = (string)GetGlobalResourceObject("errorMessages", "uploadValidFile");
    }

    /// <summary>
    /// Binds report data to the report viewer
    /// </summary>
    /// <param name="report_data"></param>
    private void reportBind(DataTable report_data)
    {
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "tdWalgreensDataSet_tblUploadCorporteData";
        rds.Value = report_data;

        this.corporateUploadRecordReport.LocalReport.DataSources.Clear();
        this.corporateUploadRecordReport.LocalReport.DataSources.Add(rds);
        this.corporateUploadRecordReport.ShowPrintButton = false;
        if (report_data.Rows.Count > 0)
            this.corporateUploadRecordReport.LocalReport.ReportPath = Server.MapPath("reportInvalidCorporateRecords.rdlc");
        report_data = null;
    }

    #endregion

    #region --------------- PRIVATE VARIABLES --------------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    private string validationError = string.Empty;
    System.Web.UI.HtmlControls.HtmlTableRow rowUploadClinicType = null;

    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}