﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WalgreensCorporateClinicsUploader.aspx.cs" Inherits="WalgreensCorporateClinicsUploader" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Src="~/controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="~/controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Scheduled Clinics Uploader</title>
    <link href="../css/wags.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/commonFunctions.js" type="text/javascript"></script>
    <script src="../javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="../css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="../css/gridstyles.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="../themes/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
    <script src="../javaScript/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        function validateClinicType() {
            if ($('#rowClinicType').is(":visible") && $('#ddlClinicType').val() == "select") {
                alert("Please select clinic type.");
                return false;
            }
            return true;
        }
        function validateUploadFile() {
            //Validate File type
            var fileExtension = ['xlsx', 'xlsm'];
            if ($.inArray($('#corporateClinicUploadControl').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Please select only excel files.");
                return false;
            }
            else {
                var fileContent = $('#corporateClinicUploadControl').val();
                if (fileContent != null && fileContent != undefined)
                    document.getElementById("btnUpload").click();
            }
        }
        $(document).ready(function () {
            $("#corporateUploadRecordReport_ctl10").css("width", "900px");
            $("#corporateUploadRecordReport_ctl10").css("height", "400px");
            $("#corporateUploadRecordReport_ctl09").css("width", "900px");
            $("#corporateUploadRecordReport_ctl09").css("height", "400px");
        });
    </script>
    <style type="text/css">
        .profileFormObject {
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hfUploadedFile" runat="server" />
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
            <tr>
                <td colspan="2">
                    <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <table width="935" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" style="background-color: #FFFFFF;">
                                <table width="900" border="0" cellspacing="8" style="border: 1px solid #999;">
                                    <tr>
                                        <td align="left" class="subTitle">Scheduled Clinics Uploader</td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" class="logSubTitles">
                                            <asp:Label ID="userMessage" runat="server" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" nowrap="nowrap" class="logSubTitles">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr id="rowClinicType" runat="server" visible="false" style="height: 50px;">
                                                    <td align="left" class="logSubTitles">Clinic Type:&nbsp;&nbsp;
                                                        <asp:DropDownList ID="ddlClinicType" class="formFields" runat="server" Width="133px">
                                                            <asp:ListItem Value="select">-- Select --</asp:ListItem>
                                                            <asp:ListItem Value="3">Corporate</asp:ListItem>
                                                            <asp:ListItem Value="5">Uploaded Local</asp:ListItem>
                                                            <asp:ListItem Value="7">Uploaded Charity</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">Select a file to upload:<asp:FileUpload ID="corporateClinicUploadControl" runat="server" Style="color: transparent;" onchange="validateUploadFile();" CssClass="profileFormObject"></asp:FileUpload>
                                                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Style="display: none;" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnSumbit" ImageUrl="~/images/btn_submit_img.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_img.png');"
                                                            CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_img_lit.png');"
                                                            runat="server" AlternateText="Submit" OnClientClick="return validateClinicType();" OnClick="btnSubmit_Click" />
                                                    </td>
                                                </tr>
                                                <tr style="height: 50px;">
                                                    <td colspan="2">Selected File: &nbsp;&nbsp;<asp:Label ID="labelUploadedfile" class="fileInfo" runat="server" Width="350px"></asp:Label></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td width="125" style="text-align: left; vertical-align: top;" nowrap="nowrap" class="logSubTitles">
                                                        <asp:Label ID="labelTotalRecordsInserted" Text="Total Records Inserted:" runat="server" Width="150px"></asp:Label>
                                                        <asp:Label ID="lblRecordInserted" class="profileTitles" runat="server" Width="80px"></asp:Label><br />
                                                        <asp:Label ID="labelTotalRecordsFailed" ForeColor="Red" Text="Total Records Failed:" runat="server" Width="150px"></asp:Label>
                                                        <asp:Label ID="lblRecordFailed" ForeColor="Red" class="profileTitles" runat="server" Width="80px"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top; padding-top: 10px;" nowrap="nowrap" class="logSubTitles">
                                                        <asp:Label ID="lblErrorNote" ForeColor="Red" Text="Check the 'Remarks' column of the report below for upload errors." Visible="false" runat="server" Width="150px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="width: 900px; height: 420px">
                                                            <rsweb:ReportViewer ID="corporateUploadRecordReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="900px" Height="400px" TabIndex="3"></rsweb:ReportViewer>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <img src="../images/spacer.gif" width="1" height="10" alt="spacer" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
