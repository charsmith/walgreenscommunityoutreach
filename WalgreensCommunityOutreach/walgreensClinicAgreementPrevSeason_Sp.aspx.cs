﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using tdEmailLib;
using System.Net.Configuration;
using System.Configuration;
using System.Web.Configuration;
using System.IO;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Text;
using TdWalgreens;

public partial class walgreensClinicAgreementPrevSeason_Sp : Page
{
    #region ------------ PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["args"] != null && Request.QueryString.Count > 0)
            {
                EncryptedQueryString args = new EncryptedQueryString(Request.QueryString["args"]);
                if (args.Count() > 1)
                {
                    Int32.TryParse(args["arg2"].ToString(), out this.contactLogPk);
                    this.hfUserEmail.Value = args["arg1"].ToString();
                    this.hfContactLogPk.Value = args["arg2"].ToString();
                    string url_referrer = "";
                    if (Request.UrlReferrer != null)
                    {
                        if (Request.UrlReferrer.ToString().IndexOf('?') > 0)
                            url_referrer = Request.UrlReferrer.ToString().Substring(0, Request.UrlReferrer.ToString().IndexOf('?')).Substring(Request.UrlReferrer.ToString().LastIndexOf('/') + 1);
                        else
                            url_referrer = Request.UrlReferrer.ToString().Substring(Request.UrlReferrer.ToString().LastIndexOf('/') + 1);
                    }

                    if (url_referrer == "walgreensClinicAgreementPrevSeason.aspx")
                        this.hfUserType.Value = args["arg3"].ToString();
                    else
                        this.hfUserType.Value = "BusinessUser";

                    this.bindContractAgreement();
                }
            }

            if (this.contactLogPk == 0)
            {
                this.redirectUser();
            }
        }
    }

    protected void btnSendMail_Click(object sender, ImageClickEventArgs e)
    {
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);

        if (Page.IsValid)
        {
            EmailOperations email_operations = new EmailOperations();
            if (email_operations.sendClinicAgreementToBusinessUsers(this.txtEmails.Text, this.contactLogPk, false))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "walgreensCommunityAgreementMail") + "'); window.location.href = 'walgreensHome.aspx';", true);
            }
        }
    }

    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        this.redirectUser();
    }

    protected void grdImmunizationChecks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_price = (Label)e.Row.FindControl("lblValue");
            if (this.dtPaymentTypes.Select("paymentId IN (4,5) AND isSelected = 'True'").Count() > 0)
                lbl_price.Visible = false;
            else
            {
                lbl_price.Visible = true;
                lbl_price.Text = "$ " + lbl_price.Text;
            }

            if (this.dtImmunizationChecks.Select("paymentOption = 1 And isSelected='True'").Count() > 0)
            {
                this.lblMinimumInvoiced.Visible = true;
                string result = new String(this.dtImmunizationChecks.Select("paymentOption = 1 And isSelected='True'")[0]["spanishName"].ToString().Where(x => Char.IsDigit(x) && x != 0).ToArray());
                this.lblMinimumInvoiced.Text = "Minimum # to be invoiced:  " + result;
            }
            else
                this.lblMinimumInvoiced.Visible = false;
        }
    }

    protected void grdPaymentTypes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl_name = (Label)e.Row.FindControl("lblPmtName");
            if (lbl_name.Text.Trim().Length != 0)
            {
                System.Web.UI.HtmlControls.HtmlTable tbl_paymentInfo = ((System.Web.UI.HtmlControls.HtmlTable)e.Row.FindControl("tblPaymentInfo"));
                Label lbl_Dollar = (Label)e.Row.FindControl("lblDollar");
                if (((Label)e.Row.FindControl("lblIsCopay")).Text == "Yes")
                    lbl_Dollar.Visible = true;
                else
                    lbl_Dollar.Visible = false;

                tbl_paymentInfo.Visible = true;
            }
        }
    }

    protected void lnkChangeCulture_Click(object sender, EventArgs e)
    {
        Int32.TryParse(this.hfContactLogPk.Value.Trim(), out this.contactLogPk);
        if (this.contactLogPk > 0)
        {
            EncryptedQueryString encryped_link = new EncryptedQueryString();
            encryped_link["arg1"] = this.hfUserEmail.Value;
            encryped_link["arg2"] = this.contactLogPk.ToString();
            encryped_link["arg3"] = this.hfUserType.Value;

            Response.Redirect("walgreensClinicAgreementPrevSeason.aspx?args=" + encryped_link.ToString());
        }
        else
            Response.Redirect("walgreensLandingPage.aspx");
    }

    #endregion

    #region ------------ PRIVATE METHODS -------------
    /// <summary>
    /// Gets and binds contract agreement
    /// </summary>
    private void bindContractAgreement()
    {
        string is_contract_approved = string.Empty;
        DataSet ds_contract_agreement = new DataSet();
        DataTable dt_contract_agreement = new DataTable();
        ds_contract_agreement = dbOperation.getClinicAgreementPrevSeason(this.contactLogPk.ToString());
        dt_contract_agreement = ds_contract_agreement.Tables[0]; //Contract agreement details

        if ((dt_contract_agreement == null) || (dt_contract_agreement != null && dt_contract_agreement.Rows.Count == 0))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('No clinic location has been added to this business'); window.location.href = 'walgreensHome.aspx';", true);
            return;
        }
        else
        {
            //Check if contract agreement is new
            if (string.IsNullOrEmpty(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString()))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('No clinic location has been added to this business'); window.location.href = 'walgreensHome.aspx';", true);
                return;
            }
            else
            {
                is_contract_approved = dt_contract_agreement.Rows[0]["isApproved"].ToString();

                XmlDocument xml_contract_agreement = new XmlDocument();
                xml_contract_agreement.LoadXml(dt_contract_agreement.Rows[0]["clinicAgreementXml"].ToString());

                DataSet ds_bind_data = new DataSet();
                StringReader sr_location_xml = new StringReader(xml_contract_agreement.SelectSingleNode("//Businesses").OuterXml);
                ds_bind_data.ReadXml(sr_location_xml);
                grdLocations.DataSource = ds_bind_data.Tables[0];
                grdLocations.DataBind();

                //Bind payment types
                if (xml_contract_agreement.SelectSingleNode("//PaymentTypes") != null)
                {
                    ds_bind_data = new DataSet();
                    StringReader sr_payments_xml = new StringReader(xml_contract_agreement.SelectSingleNode("//PaymentTypes").OuterXml);
                    ds_bind_data.ReadXml(sr_payments_xml);
                    if (ds_bind_data.Tables.Count > 0)
                    {
                        this.dtPaymentTypes = ds_bind_data.Tables[0];
                        this.grdPaymentTypes.DataSource = this.dtPaymentTypes;
                        this.grdPaymentTypes.DataBind();
                    }
                }

                //Bind immunization checks
                if (xml_contract_agreement.SelectSingleNode("//ImmunizationChecks") != null)
                {
                    ds_bind_data = new DataSet();
                    StringReader sr_immunization_xml = new StringReader(xml_contract_agreement.SelectSingleNode("//ImmunizationChecks").OuterXml);
                    ds_bind_data.ReadXml(sr_immunization_xml);
                    if (ds_bind_data.Tables.Count > 0)
                    {
                        this.dtImmunizationChecks = ds_bind_data.Tables[0];
                        this.grdImmunizationChecks.DataSource = this.dtImmunizationChecks;
                        this.grdImmunizationChecks.DataBind();

                        if (this.dtImmunizationChecks.Select("pk = 2 and isSelected = 'True'").Count() > 0)
                            this.lblImmunizationDisclaimer.Text = "*Walgreens facturará al plan de seguro de la tarifa contratada. El tarifa contratada incluye la vacuna y el administrado.";
                        else
                            this.lblImmunizationDisclaimer.Text = "*El costo incluye la vacuna y el administrado.";
                    }
                }

                //Bind Client details
                this.lblClient.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/Client/@header").Value.ToString();
                this.lblClientName.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/Client/@name").Value.ToString();
                this.lblClientTitle.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/Client/@title").Value.ToString();
                this.lblClientDate.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/Client/@date").Value.ToString();

                //Binds Walgreen CO. details
                this.lblWalgreenName.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@name").Value.ToString();
                this.lblWalgreenTitle.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@title").Value.ToString();
                this.lblWalgreenDate.Text = Convert.ToDateTime(xml_contract_agreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@date").Value.ToString()).ToString("MM/dd/yyyy");
                this.lblDistrictNum.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/WalgreenCo/@districtNumber").Value.ToString();

                //Binds Legal Notice details
                this.lblLegalAttentionTo.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@attentionTo").Value.ToString();
                this.lblLegalAddress1.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address1").Value.ToString();
                this.lblLegalAddress2.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@address2").Value.ToString();
                this.lblLegalCity.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@city").Value.ToString();
                this.lblLegalState.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@state").Value.ToString();
                this.lblLegalZipCode.Text = xml_contract_agreement.SelectSingleNode("ClinicAgreement/LegalNoticeAddress/@zipCode").Value.ToString();
            }

            this.txtElectronicSign.Text = dt_contract_agreement.Rows[0]["signature"].ToString();

            //Set controls visiblity based on Contract Agreement Email Sent or Approved/Rejected status
            if (!string.IsNullOrEmpty(is_contract_approved))
            {
                //Contract Agreement approved
                if (Convert.ToBoolean(is_contract_approved))
                {
                    this.disableControls(true);
                    this.rbtnApprove.Checked = true;
                    this.lblErrorMessage.Text = "The Walgreens Community Off-Site Agreement has been submitted by " + dt_contract_agreement.Rows[0]["approvedOrRejectedBy"].ToString();
                    this.lblErrorMessage.Visible = true;
                }
                else
                {
                    this.rbtnReject.Checked = true;
                    this.trNotes.Visible = true;
                    this.txtNotes.Text = dt_contract_agreement.Rows[0]["notes"].ToString();
                    this.txtNotes.Enabled = false;
                }

                this.txtElectronicSign.Enabled = false;
                this.tblApproval.Visible = true;
                this.tblApproval.Disabled = true;
                this.rbtnApprove.Enabled = false;
                this.rbtnReject.Enabled = false;
            }
            else
            {
                this.btnSendMail.Visible = false;
                this.disableControls(true);
            }

            if (this.hfUserType.Value == "WalgreensUser")
            {
                this.tblBusinessUserMsg.Visible = false;
                this.tblEmails.Visible = true;
                this.btnSendMail.Visible = true;
                this.btnCancel.Visible = true;
                this.trNoteText.Visible = true;
            }
            else
            {
                this.tblApproval.Visible = true;
            }
        }
    }

    /// <summary>
    /// Disables controls
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableControls(bool is_disable)
    {
        this.disableCommunityAgreement(is_disable, this.tblApproval.Controls);
        this.disableCommunityAgreement(is_disable, this.tblAgreement.Controls);
        this.disableGridControls(is_disable, this.grdLocations);
        this.disableGridControls(is_disable, this.grdImmunizationChecks);
        this.disableGridControls(is_disable, this.grdPaymentTypes);

        this.grdImmunizationChecks.Enabled = !is_disable;
        this.grdPaymentTypes.Enabled = !is_disable;
    }

    /// <summary>
    /// Disables all input controls in the page
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableCommunityAgreement(bool disable, ControlCollection ctrl_main)
    {
        foreach (Control page_ctrl in ctrl_main)
        {
            foreach (Control ctrl1 in page_ctrl.Controls)
            {
                foreach (Control ctrl in ctrl1.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        ((TextBox)ctrl).Enabled = !disable;
                        if (((TextBox)ctrl).ID == "txtGroupDate" || ((TextBox)ctrl).ID == "txtWalgreensDate")
                        {
                            ((TextBox)ctrl).Visible = true;
                            this.lnkPrintContract.Visible = true;
                        }
                    }

                    if (ctrl is RadioButton)
                        ((RadioButton)ctrl).Enabled = !disable;

                    if (ctrl is DropDownList)
                        ((DropDownList)ctrl).Enabled = !disable;
                }
            }
        }
    }

    /// <summary>
    /// disable immunization grid controls.
    /// </summary>
    /// <param name="disable"></param>
    /// <param name="grd_ctrl"></param>
    private void disableGridControls(bool disable, GridView grd_ctrl)
    {
        foreach (GridViewRow row in grd_ctrl.Rows)
        {
            foreach (Control ctrl in row.Controls)
            {
                foreach (Control ctrl1 in ctrl.Controls)
                {
                    if (ctrl1 is TextBox)
                    {
                        ((TextBox)ctrl1).Enabled = !disable;
                        if (((TextBox)ctrl1).ID.Contains("txtCalenderFrom"))
                            ((TextBox)ctrl1).Visible = true;
                    }

                    if (ctrl1 is DropDownList)
                        ((DropDownList)ctrl1).Enabled = !disable;

                    if (ctrl1 is CheckBox)
                        ((CheckBox)ctrl1).Enabled = !disable;

                    if (ctrl1 is ImageButton)
                        ((ImageButton)ctrl1).Visible = !disable;
                }
            }
        }
    }

    /// <summary>
    /// Redirects user based on login type
    /// </summary>
    private void redirectUser()
    {
        if (this.hfUserType.Value == "WalgreensUser")
            Response.Redirect("walgreensContactLog.aspx", false);
        else
        {
            Session.RemoveAll();
            Session.Abandon();
            Response.Redirect("walgreensNoAccess.htm");
        }
    }
    #endregion

    #region ------------ PRIVATE VARIABLES -----------
    private DBOperations dbOperation = null;
    private int contactLogPk = 0;
    private DataTable dtPaymentTypes;
    private DataTable dtImmunizationChecks;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        //ApplicationSettings.validateSession();
        this.dbOperation = new DBOperations();
        this.dtPaymentTypes = new DataTable();
        this.dtImmunizationChecks = new DataTable();
        if (Session.IsNewSession)
        {
            FormsAuthentication.SignOut();
            string str = Request.Url.ToString();
            Response.Redirect(str);
        }
    }
}