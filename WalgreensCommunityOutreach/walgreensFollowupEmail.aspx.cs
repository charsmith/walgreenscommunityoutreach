﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TdApplicationLib;
using System.Data;
using TdWalgreens;

public partial class walgreensFollowupEmail : Page
{
    #region -------------------- PROTECTED EVENTS --------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.hfContactLogPk.Value = this.commonAppSession.SelectedStoreSession.SelectedContactLogPk.ToString();
            this.dtFollowUpEmail.getSelectedDate = DateTime.Now.Date.AddDays(7);
            this.dtFollowUpEmail.MinDate = DateTime.Now.Date.AddDays(0);
            this.followUpDependOnUserRoles();
            this.getFollowUpEvent();
            this.lblOutreachEffort.Text = (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1" ? " Senior Outreach " : (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" ? " Immunization Program " : ""));
        }
        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.walgreensHeaderCtrl.FindControl("menuTab")).InnerHtml = "&nbsp;";
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {
        int return_value = 0;
        return_value = this.dbOperation.createFollowUpEvent(this.prepareStoreProfileXml());
        if (Page.IsValid)
        {
            if (return_value != 2)
            {
                if (!this.chkEmailReminder.Checked)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "followUpReminderScheduled") + "'); window.location.href = '" + this.commonAppSession.SelectedStoreSession.referrerPath + "';", true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "followUpReminderAdded") + "'); window.location.href = '" + this.commonAppSession.SelectedStoreSession.referrerPath + "';", true);
            }
        }
    }

    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        if (!string.IsNullOrEmpty(this.commonAppSession.SelectedStoreSession.referrerPath))
        {
            Response.Redirect(this.commonAppSession.SelectedStoreSession.referrerPath);
        }
    }
    #endregion

    #region --------- PRIVATE METHODS--------------
    //Enable or disable check box (do not send email) depending on user roles.
    private void followUpDependOnUserRoles()
    {
        if (this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "store manager" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "pharmacy manager" || this.commonAppSession.LoginUserInfoSession.UserRole.ToLower() == "admin")
        {
            this.chkEmailReminder.Enabled = true;
            this.chkEmailReminder.Checked = false;
        }
        else
        {
            this.chkEmailReminder.Checked = true;
            this.chkEmailReminder.Enabled = false;
        }
    }

    /// <summary>
    /// This method will be used to get followup event data
    /// </summary>
    private void getFollowUpEvent()
    {
        DataTable schedule_event;
        int contactLog_pk;
        Int32.TryParse(this.hfContactLogPk.Value, out contactLog_pk);

        schedule_event = this.dbOperation.getFollowUpEvent(Convert.ToInt32(contactLog_pk.ToString()));
        this.txtEmails.Text = this.commonAppSession.LoginUserInfoSession.Email;
        if (schedule_event.Rows.Count > 0)
        {
            this.dtFollowUpEmail.getSelectedDate = ((schedule_event.Rows[0]["followUpDate"] is DBNull) ? DateTime.Today : (Convert.ToDateTime(schedule_event.Rows[0]["followUpDate"].ToString()) < DateTime.Today) ? DateTime.Now.AddDays(6) : Convert.ToDateTime(schedule_event.Rows[0]["followUpDate"].ToString()));
            if (!(string.IsNullOrEmpty(schedule_event.Rows[0]["emailReminder"].ToString())) && Convert.ToBoolean(schedule_event.Rows[0]["emailReminder"].ToString()))
            {
                this.chkEmailReminder.Checked = true;
            }
            else
                this.chkEmailReminder.Checked = false;

            if (schedule_event.Rows[0]["emailTo"] != DBNull.Value)
                this.txtEmails.Text = schedule_event.Rows[0]["emailTo"].ToString();
            this.btnCancel.Visible = true;
        }
    }

    /// <summary>
    /// This function returns the xml structure to DB for creating the Store Profile
    /// </summary>
    /// <returns></returns>
    private XmlDocument prepareStoreProfileXml()
    {
        int contactLog_pk;
        Int32.TryParse(this.hfContactLogPk.Value, out contactLog_pk);

        XmlDocument profile_doc = new XmlDocument();
        XmlElement store_profile_element = store_profile_element = profile_doc.CreateElement("storeprofile");
        XmlElement fields_element = profile_doc.CreateElement("fields");

        XmlElement create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "followUpDate");
        create_store.SetAttribute("value", this.dtFollowUpEmail.getSelectedDate.ToString());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "contactLogPk");
        create_store.SetAttribute("value", contactLog_pk.ToString());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "emailReminder");
        create_store.SetAttribute("value", this.chkEmailReminder.Checked ? "true" : "false");
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "emailTo");
        create_store.SetAttribute("value", this.txtEmails.Text);
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "updatedBy");
        create_store.SetAttribute("value", this.commonAppSession.LoginUserInfoSession.UserID.ToString());
        fields_element.AppendChild(create_store);

        create_store = profile_doc.CreateElement("field");
        create_store.SetAttribute("name", "updatedDate");
        create_store.SetAttribute("value", DateTime.Now.ToString());
        fields_element.AppendChild(create_store);

        store_profile_element.AppendChild(fields_element);
        profile_doc.AppendChild(store_profile_element);

        return profile_doc;
    }

    #endregion

    #region --------- PRIVATE VARIABLES----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region -------------------- Web Form Designer generated code --------------------
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
    }
    #endregion
}