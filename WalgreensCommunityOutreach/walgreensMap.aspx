﻿<%@ Page Language="C#" AutoEventWireup="false" CodeFile="walgreensMap.aspx.cs" Inherits="walgreensMap" %>

<%@ Register Assembly="Trirand.Web" TagPrefix="trirand" Namespace="Trirand.Web.UI.WebControls" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <link href="css/ddcolortabs.css" rel="stylesheet" type="text/css" />
    <link href="css/wags.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&v=3"></script>
    <link rel="stylesheet" type="text/css" href="themes/jquery-ui-1.8.17.custom.css" />
    <link rel="stylesheet" type="text/css" href="themes/ui.jqgrid.css" />
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javaScript/grid.locale-en.js" type="text/javascript"></script>
    <script src="javaScript/jquery.jqGrid.min.js?a=1" type="text/javascript"></script>
    <script src="javaScript/commonFunctions.js" type="text/javascript"></script>
    <script type="text/javascript"  src="javaScript/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
             var columnIndex = 3;   
             $('#prospectLeftGrid').find("td:nth-child(" + columnIndex + ")").live("mouseover", function () {
                $(this).attr("title", $(this).html() + " miles from Walgreens");
            });
        });
    
   

        var map;
        var prospectMarkersA = [];
        var infoWindowsA = []
        <% = this.prospectsA %>

        function initialize() {
//           $("#map_canvas").css("height",  $("#prospectLeftGrid").height() +"px");
           
            var latlng = new google.maps.LatLng(<%= this.storeLatitude %>,<%= this.storeLongitude %>);
            var myOptions = {
                zoom: 13,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);


       var contentString = "<%= this.storeDescription %>"

       var infowindow = new google.maps.InfoWindow({            
                content: contentString,
                maxWidth: 100
       });



        
        //var image = new google.maps.MarkerImage('images/map_markers/walgreens_map_marker.png',
        //// This marker is 59 pixels wide by 24 pixels tall.
        //new google.maps.Size(87, 32),
        //// The origin for this image is 0,0.
        //new google.maps.Point(0,0),
        //// The anchor for this image is the point of the marker at 29,24.
        //new google.maps.Point(44, 32));
        //var shadow = new google.maps.MarkerImage('images/map_markers/walgreens_map_marker_shadow.png',


        //new google.maps.Size(97, 32),
        //new google.maps.Point(0,0),
        //new google.maps.Point(44, 32));
        //// Shapes define the clickable region of the icon.
        //// The type defines an HTML <area> element 'poly' which
        //// traces out a polygon as a series of X,Y points. The final
        //// coordinate closes the poly by connecting to the first
        //// coordinate.
        //var shape = {
        //coord: [0,8,7,0,79,0,87,6,87,15,80,22,50,22,46,31,43,30,39,22,8,22,0,15 ],
            //type: 'poly'};
       var image = new google.maps.MarkerImage('images/map_markers/wag_map_marker.png',
            // This marker is 25 pixels wide by 28 pixels tall.
       new google.maps.Size(25, 28),
            // The origin for this image is 0,0.
       new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 8,27.
       new google.maps.Point(8, 27));
       var shadow = new google.maps.MarkerImage('',
       // The shadow image is larger in the horizontal dimension
       // while the position and offset are the same as for the main image.
       new google.maps.Size(0, 0),
       new google.maps.Point(0, 0),
       new google.maps.Point(00, 0));
       var shape = {
           coord: [0, 0, 25, 0, 24, 20, 9, 20, 9, 28, 0, 20],
           type: 'poly'
       };





        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            shadow: shadow,
            icon: image,
            shape: shape,
            draggable: false,
            zIndexProcess: function() { return 1; },
            title: 'Walgreens'
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });

        addProspectMarkers(map, gProspectsA);

        for (i in prospectMarkersA){
            prospectMarkersA[i].setMap(map);
        }            

        }          
         function addProspectMarkers(map, locations){
          // Add markers to the map
          // Marker sizes are expressed as a Size of X,Y
          // where the origin of the image (0,0) is located
          // in the top left of the image.

          // Origins, anchor positions and coordinates of the marker
          // increase in the X direction to the right and in
          // the Y direction down.
          var shape = {
              //coord: [0, 0, 34, 0, 34, 14, 18, 15, 18, 20, 16, 20, 15, 14, 0, 14],
              coord: [9, 34, 7, 25, 3, 17, 0, 2, 0, 7, 6, 0, 14, 0,20, 7, 20, 12, 16, 17, 12, 25,11,34],
              type: 'poly'
          };
          for (var i = 0; i < locations.length; i++) {
            var prospect = locations[i];
            if(prospect!=undefined){
            var image = new google.maps.MarkerImage('images/map_markers/' + prospect[3],
                // This marker is 20 pixels wide by 34 pixels tall.
                new google.maps.Size(20, 34),
                // The origin for this image is 0,0.
                new google.maps.Point(0,0),
                // The anchor for this image is the base of the flagpole at 0,32.
                new google.maps.Point(10, 34));

                var shadow = new google.maps.MarkerImage('images/map_markers/shadow50.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(37, 34),
                new google.maps.Point(0,0),
                new google.maps.Point(10,34));
                
            // Shapes define the clickable region of the icon.
            // The type defines an HTML <area> element 'poly' which
            // traces out a polygon as a series of X,Y points. The final
            // coordinate closes the poly by connecting to the first
            // coordinate.
            var myLatLng = new google.maps.LatLng(prospect[1], prospect[2]);
            var myTitle =  prospect[0]; 
            var prospect_marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                shadow: shadow,
                icon: image,
                zIndexProcess: function() { return 1000; },
                shape: shape,
                title: myTitle
            });

           var prospect_contentString = prospect[4]
           var infowindow = new google.maps.InfoWindow({            
                    content: prospect_contentString,
                    maxWidth: 100
                });
                infoWindowsA.push(infowindow);
            bindInfoW(map,prospect_marker, prospect_contentString, infowindow);  
          }          
       }
    }        
    function bindInfoW(map,marker, contentString, infowindow) 
    { 
        prospectMarkersA.push(marker)
            google.maps.event.addListener(marker, 'click', function() { 
            for (i=0;i<infoWindowsA.length;i++) { 
                    infoWindowsA[i].close(); 
            } 
            for (i=0;i<prospectMarkersA.length;i++) { 
                    prospectMarkersA[i].setZIndex(1000); 
            } 
            var infowindow = new google.maps.InfoWindow({            
                content: contentString,
                maxWidth: 100
            });
            infoWindowsA.push(infowindow);
            infowindow.open(map, marker); 
            marker.setZIndex(10000)
            var latlng = marker.position;
            map.setCenter(latlng)
            map.setZoom(16)
            }); 
    }         
    function formatImage(cellValue, options, rowObject) {
        var the_business = ("" + rowObject[1]);

           while(the_business.indexOf("'") > 0 || the_business.indexOf("\"") > 0){
               the_business = ("" + the_business.replace("'","`").replace("\"", "&quot;"));
            }
        
           var imageHtml = '<a href="javascript:iconClick(\''+ the_business + '\',\''+ rowObject[3] + '\')"><img border="0" src="images/map_markers/' + cellValue + '" originalValue="' + cellValue + '" /></a>';
            return imageHtml;
        }
    function iconClick(contentString,prospect_marker_index) {
        var marker = prospectMarkersA[prospect_marker_index]
		var marker_lat = marker.position.lat();
		var marker_lng = marker.position.lng();

         if(isNaN(marker_lat) || isNaN(marker_lng))
            return;

        for (i=0;i<infoWindowsA.length;i++) { 
            infoWindowsA[i].close(); 
        } 
        for (i=0;i<prospectMarkersA.length;i++) { 
            prospectMarkersA[i].setZIndex(1000); 
        } 
        infoWindowsA[prospect_marker_index].content=contentString;
        infoWindowsA[prospect_marker_index].maxWidth=100;

        infoWindowsA[prospect_marker_index].open(map,marker); 
        marker.setZIndex(10000)
        var latlng = marker.position;
        map.setCenter(latlng);
        map.setZoom(16);
    }        
    </script>
    <style type="text/css">
        body, html
        {
            font-size: 80%;
        }
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient">
    <form id="form1" runat="server">
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF">
                <table width="935" border="0" cellspacing="22" cellpadding="0" height="600">
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" class="pageTitle">
                                <tr>
                                    <td align="left" width="30%">
                                        Business Locations Map
                                    </td>
                                    <td id="rowBusinessType" runat="server">
                                        <span class="logSubTitles">Log Type:</span>
                                        <asp:DropDownList ID="ddlBussinessType" runat="server" CssClass="formFields" AutoPostBack="true"
                                            OnSelectedIndexChanged="rebuildMap" CausesValidation="false">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right">
                                        <img id="markerKey" runat="server" alt="Marker Key" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="268" valign="top">
                            <trirand:JQGrid runat="server" ID="prospectLeftGrid" Width="268" Height="502px" PagerSettings-PageSize="100">
                                <Columns>
                                    <trirand:JQGridColumn DataField="icon" Width="30" HeaderText=" " TextAlign="Center">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="formatImage" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="fulladdress" Width="208" HeaderText="Address" />
                                    <trirand:JQGridColumn DataField="distanceToStore" Width="45" DataFormatString="{0:n2}"
                                        HeaderText="Dist." TextAlign="Right" />
                                    <trirand:JQGridColumn DataField="prospectArrayPosition" Visible="false" />
                                </Columns>
                                <ClientSideEvents BeforeAjaxRequest="clearGrid('Left')" LoadComplete="initialize" />
                                <ToolBarSettings ToolBarPosition="Hidden" />
                                <PagerSettings PageSize="100"></PagerSettings>
                            </trirand:JQGrid>
                        </td>
                        <td width="598" valign="top">
                            <div id="map_canvas" style="width: 598px; height: 525px; border-color: #999; border-style: solid;
                                border-width: 1px">
                            </div>
                            <script type="text/javascript">
                                function clearGrid(grid) {
                                    var grid_object = $("prospect" + grid + "Grid")
                                    grid_object.clearGridData();
                                }
                            </script>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter id="walgreensFooter" runat="server" />
    </form>
</body>
</html>
