﻿using System;
using TdApplicationLib;
using System.Data;
using System.Text;
using Trirand.Web.UI.WebControls;
using System.Web.UI;
using System.Linq;
using TdWalgreens;

public partial class walgreensMap : Page
{
    #region -------------- PROTECTED EVENTS ---------------
    public static int storeId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.bindPage();
    }

    protected void rebuildMap(object sender, EventArgs e)
    {
        Session["businessType"] = this.ddlBussinessType.SelectedValue;
        this.getStoreDataSet();
    }
    #endregion

    #region --------- PRIVATE METHODS --------------
    private void bindPage()
    {
        bool is_same_store = storeId == this.commonAppSession.SelectedStoreSession.storeID ? true : false;
        if ((!this.Page.IsPostBack || !is_same_store) && this.prospectLeftGrid.AjaxCallBackMode != AjaxCallBackMode.RequestData)
        {
            this.bindBusinessType();
            storeId = this.commonAppSession.SelectedStoreSession.storeID;
        }
        
        this.getStoreDataSet();
        if (this.dataset.Tables["prospect"] != null && this.dataset.Tables["prospect"].Rows.Count > 0)
        {
            this.dataset.Tables["prospect"].DefaultView.RowFilter = "side='left'";
            DataTable left_table = this.dataset.Tables["prospect"].DefaultView.ToTable();
            this.prospectLeftGrid.DataSource = left_table;
            this.prospectLeftGrid.DataBind();
        }
    }
    /// <summary>
    /// Binds Outreach Program business types to dropdown
    /// </summary>
    private void bindBusinessType()
    {
        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3" || this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
        {
            this.ddlBussinessType.DataSource = ApplicationSettings.getOutreachBusinessTypesForContactLog(this.commonAppSession.SelectedStoreSession.OutreachProgramSelected);
            this.ddlBussinessType.DataTextField = this.commonAppSession.LoginUserInfoSession.BusinessTypes.Columns["name"].ToString();
            this.ddlBussinessType.DataValueField = this.commonAppSession.LoginUserInfoSession.BusinessTypes.Columns["fieldValue"].ToString();
            this.ddlBussinessType.DataBind();

            if (this.ddlBussinessType.Items.FindByValue(this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId.ToString()) != null)
                this.ddlBussinessType.Items.FindByValue(this.commonAppSession.SelectedStoreSession.SelectedBusinessTypeId.ToString()).Selected = true;

            Session["businessType"] = this.ddlBussinessType.SelectedValue;

        }
    }

    /// <summary>
    /// Binds store businesses to grid
    /// </summary>
    /// <param name="business_type"></param>
    private void getStoreDataSet()
    {
       
        this.rowBusinessType.Visible = true;

        if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "1")
            this.rowBusinessType.Visible = false;
        else
        {
            DataRow[] dt_rows;
            if (this.commonAppSession.SelectedStoreSession.getAssignedBusinesses != null && this.commonAppSession.SelectedStoreSession.getAssignedBusinesses.Rows.Count > 0)
            {

                dt_rows = this.commonAppSession.SelectedStoreSession.getAssignedBusinesses.Select("businessType='2'");
                if (dt_rows.Count() == 0 || this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId != "3")
                {
                    if (this.ddlBussinessType.Items.FindByText("Scheduled Clinics") != null)
                    {
                        this.ddlBussinessType.Items.Remove(ddlBussinessType.Items.FindByText("Scheduled Clinics"));
                        this.ddlBussinessType.SelectedValue = this.ddlBussinessType.Items[0].Value;
                        Session["businessType"] = this.ddlBussinessType.SelectedValue;
                        this.rowBusinessType.Visible = false;
                    }
                   
                }
            }
            
        }

        this.dataset = this.dbOperation.getStoreCustDist(this.commonAppSession.SelectedStoreSession.storeID, this.commonAppSession.SelectedStoreSession.OutreachProgramSelected);
        if (Session["businessType"].ToString() == "2") this.markerKey.Src = "images/map_markers/marker_key_fi_clinic.gif";
        else this.markerKey.Src = "images/map_markers/marker_key_" + this.commonAppSession.SelectedStoreSession.OutreachProgramSelected.ToLower() + ".gif";

        bool has_rows = true;
        foreach (DataTable dt in dataset.Tables)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.TableName = dt.Rows[0]["tablename"].ToString();
            }
            else
            {
                has_rows = false;
            }
        }
        if (has_rows)
        {
            DataColumn icon = new DataColumn("Icon");
            DataColumn distance = new DataColumn("Distance");
            DataColumn side = new DataColumn("side");
            DataColumn prospectArrayPosition = new DataColumn("prospectArrayPosition");
            dataset.Tables["prospect"].Columns.Add(icon);
            dataset.Tables["prospect"].Columns.Add(distance);
            dataset.Tables["prospect"].Columns.Add(side);
            dataset.Tables["prospect"].Columns.Add(prospectArrayPosition);

            StringBuilder sb = new StringBuilder();
            sb.Append("var gProspectsA =[");

            int counter = 1;
            foreach (DataRow drow in dataset.Tables["prospect"].Rows)
            {
                if (drow["businessType"].ToString() == Session["businessType"].ToString())
                {
                    string info_bubble = "<b>"
                        + drow["BusinessName"].ToString().Replace("'", "`").Replace("\"", "&quot;")
                        + "</b>" + "<br />status:" + drow["outreachStatus"].ToString()
                        + "<br />Last Contact:" + drow["ContactDate"].ToString();

                    string status_color;
                    if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId.Equals("3") && Session["businessType"].ToString() == "1")
                        status_color = (drow["outReachStatusId"] == null || drow["outReachStatusId"].ToString() == "0") ? "_red" : (drow["outReachStatusId"].ToString() == "1" || drow["outReachStatusId"].ToString() == "2") ? "_yellow" : (drow["outreachStatusId"].ToString() == "3" || drow["outReachStatusId"].ToString() == "11") ? "_green" : (drow["outreachStatusId"].ToString() == "4" || drow["outreachStatusId"].ToString() == "5") ? "_blue" : (drow["outreachStatusId"].ToString() == "12") ? "_purple" : "_orange";
                    else if (this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId.Equals("3") && Session["businessType"].ToString() == "2")
                        status_color = (drow["outReachStatusId"] == null || drow["outReachStatusId"].ToString() == "0") ? "_yellow" : (drow["outReachStatusId"].ToString() == "1") ? "_green" : (drow["outreachStatusId"].ToString() == "2") ? "_red" : "_blue";
                    else
                        status_color = (drow["outReachStatusId"] == null || drow["outReachStatusId"].ToString() == "0") ? "_red" : (drow["outReachStatusId"].ToString() == "1" || drow["outReachStatusId"].ToString() == "2") ? "_yellow" : (drow["outreachStatusId"].ToString() == "4" || drow["outreachStatusId"].ToString() == "5") ? "_blue" : "_green";

                    string marker = "marker" + status_color + counter + ".png";

                    sb.Append(
                        "['" + drow["BusinessName"].ToString().Replace("'", "`").Replace("\"", "&quot;") + "'," +
                        drow["Latitude"].ToString() + "," +
                        drow["Longitude"].ToString() + ",'" + marker + "',"
                        + "'" + info_bubble + "'],"
                        );
                    drow["icon"] = marker;
                    drow["prospectArrayPosition"] = counter - 1;
                    drow["side"] = "left";
                    counter++;
                }
            }
            string my_string_array = sb.ToString();
            my_string_array = my_string_array.Substring(0, my_string_array.Length - 1);
            my_string_array = my_string_array + "]";

            this.prospectsA = my_string_array;

            this.storeLatitude = float.Parse(dataset.Tables["store"].Rows[0]["Latitude"].ToString());
            this.storeLongitude = float.Parse(dataset.Tables["store"].Rows[0]["Longitude"].ToString());
            this.storeDescription = "<font face='arial,helvetica'>" + dataset.Tables["store"].Rows[0]["fullAddress"].ToString() + "</font>";
        }
        else
        {
            this.prospectsA = "var gProspectsA =[]";
        }

        if (!string.IsNullOrEmpty(this.prospectsA) && this.prospectsA == "var gProspectsA =]")
            this.prospectsA = "var gProspectsA =[]";

    }
    #endregion


    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }
    private void InitializeComponent()
    {
        this.Init += new System.EventHandler(this.Page_Init);
        this.Load += new System.EventHandler(this.Page_Load);
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        //commonAppSession.SelectedStoreSession.storeFullName = this.walgreensHeaderCtrl.selectedStoreOnNonAdmin;
        //commonAppSession.SelectedStoreSession.storeID = Convert.ToInt32(this.walgreensHeaderCtrl.storeId);
        //if (!this.walgreensHeaderCtrl.selectedStoreOnNonAdmin.Contains("Nothing"))
        //    commonAppSession.SelectedStoreSession.storeName = this.walgreensHeaderCtrl.selectedStoreOnNonAdmin;
        this.bindPage();
    }

    #endregion

    #region PRIVATE VARIABLES
    private AppCommonSession commonAppSession = null;
    protected DataSet dataset;
    protected float storeLatitude;
    protected float storeLongitude;
    protected string storeDescription;
    protected string prospectsA;
    private DBOperations dbOperation = null;
    #endregion
}
