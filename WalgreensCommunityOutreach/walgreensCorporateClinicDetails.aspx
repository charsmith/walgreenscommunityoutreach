﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="walgreensCorporateClinicDetails.aspx.cs" Inherits="walgreensCorporateClinicDetails" %>
<%@ Register src="controls/PickerAndCalendar.ascx" tagname="PickerAndCalendar" tagprefix="uc1" %>
<%@ Register Src="controls/WalgreensFooter.ascx" TagName="walgreensFooter" TagPrefix="ucWFooter" %>
<%@ Register Src="controls/walgreensHeader.ascx" TagName="walgreensHeader" TagPrefix="ucWHeader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta content="IE=8" http-equiv="X-UA-Compatible" />
    <title>Walgreens Community Outreach</title>
    <script src="javaScript/dropdowntabs.js" type="text/javascript"></script>
    <script src="javaScript/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="javascript/commonFunctions.js?v=10202015" type="text/javascript"></script>
    <link rel="stylesheet" href="css/ddcolortabs.css" type="text/css" />
    <link rel="stylesheet" href="css/wags.css?v=10202015" type="text/css" />
    <link rel="stylesheet" href="css/theme.css" type="text/css" />
    <link rel="stylesheet" href="css/calendarStyle.css" type="text/css" />
    <%--<link rel="stylesheet" href="themes/jquery-ui-1.8.17.custom.css" type="text/css" />--%>
    <link rel="stylesheet" href="css/jquery.timepicker.css" type="text/css" />
    <script src="javaScript/jquery.timepicker.js" type="text/javascript"></script>
    <script type="text/javascript" src="javaScript/jquery-ui.js"></script>
    <link rel="stylesheet"  href="css/jquery-ui.css" type="text/css" />
    <style type="text/css">
        .wagButtonDim {
	        -moz-box-shadow: 0px 0px 0px 0px #ffffff;
	        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;
	        box-shadow: 0px 0px 0px 0px #ffffff;
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #CFECEC), color-stop(1, #B7CEEC));
	        background:-moz-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:-webkit-linear-gradient(top,#CFECEC 5%, #B7CEEC 100%);
	        background:-o-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:-ms-linear-gradient(top, #CFECEC 5%, #B7CEEC 100%);
	        background:linear-gradient(to bottom, #CFECEC 5%, #B7CEEC 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#CFECEC', endColorstr='#B7CEEC',GradientType=0);
	        background-color:#CFECEC;
	        -moz-border-radius:3px;
	        -webkit-border-radius:3px;
	        border-radius:3px;
	        border:1px solid #B7CEEC;
	        display:inline-block;
	        cursor:pointer;
	        color:#ffffff;
	        font-family:Arial;
	        font-size:12px;
	        padding:6px 12px;
	        text-decoration:none;
	        text-shadow:0px -1px 0px #B7CEEC;
	        behavior:url(PIE.htc);
        }
        
        .wagButton {
	        -moz-box-shadow: 0px 0px 0px 0px #ffffff;
	        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;
	        box-shadow: 0px 0px 0px 0px #ffffff;
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #57a1d3), color-stop(1, #2f6fa7));
	        background:-moz-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-webkit-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-o-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:-ms-linear-gradient(top, #57a1d3 5%, #2f6fa7 100%);
	        background:linear-gradient(to bottom, #57a1d3 5%, #2f6fa7 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#57a1d3', endColorstr='#2f6fa7',GradientType=0);
	        background-color:#57a1d3;
	        -moz-border-radius:3px;
	        -webkit-border-radius:3px;
	        border-radius:3px;
	        border:1px solid #2f6fa7;
	        display:inline-block;
	        cursor:pointer;
	        color:#ffffff;
	        font-family:Arial;
	        font-size:12px;
	        padding:6px 12px;
	        text-decoration:none;
	        text-shadow:0px -1px 0px #2f6fa7;
	        behavior:url(PIE.htc);
        }
        
        .wagButton:hover {
	        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #2f6fa7), color-stop(1, #57a1d3));
	        background:-moz-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-webkit-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-o-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:-ms-linear-gradient(top, #2f6fa7 5%, #57a1d3 100%);
	        background:linear-gradient(to bottom, #2f6fa7 5%, #57a1d3 100%);
	        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#2f6fa7', endColorstr='#57a1d3',GradientType=0);
	        background-color:#2f6fa7;
        }
        .wagButton:active {
	        position:relative;
	        top:1px;
        }
        .ui-widget-content A
        {
            color:#ffffff;
        }
        
        .tooltip {
            display:none;
            font-size:12px;
            height:70px;
            color:#fff;
        }
        
        .ui-widget
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
        }
        .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
        }
        .ui-widget-header
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 13px;
            font-weight: bold;
        }
        .tooltipText
        {
            font-weight:bold;
            font-size:11px;
            color:red;
            position:absolute;
            white-space: pre-wrap;
            max-width:180px;
            padding:2px;
            border-radius:0px;
            -webkit-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            -moz-box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
            box-shadow: 9px 10px 0px -5px rgba(211,211,211,1);
        }
    </style> 

    <script type="text/javascript">
        var logMessage = "";
        var clinicDetailsInfo = "";
        
        function fnGetBrowserVersion() {
            var N = navigator.appName, ua = navigator.userAgent, tem;
            var M = ua.match(/(chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
            if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
            M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
            return M[0] + ' ' + M[1] + ' ';
        }
        $(document).ready(function () {
            $('.formFields').keypress(function (key) {
                if (key.charCode == 63) return false;
            });
        });

        $("[title]").tooltip();

        function AlertMessage(alert_message, clinic_location, message) {
            if (alert_message != "")
                alert_message = "\r\n" + clinic_location + ": " + message;
            else
                alert_message = clinic_location + ": " + message;

            return alert_message;
        }

        //setting control css
        var setInvalidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "background-color": "yellow" });

            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.tooltip({
                tooltipClass: "tooltipText"
            });
            ctrl.focus(function (evt) {
                $(evt.currentTarget).tooltip("close");
            });
            ctrl.tooltip("enable");

        }
        var setValidControlCss = function (control, is_from_code_behind) {
            var ctrl = is_from_code_behind ? ctrl = $(document.getElementById(control)) : ctrl = control;
            ctrl.css({ "border": "1px solid gray" });
            ctrl.css({ "background-color": "" });
            if (ctrl[0].id.indexOf('Picker') > -1) {
                ctrl = ctrl.parent();
            }
            ctrl.removeAttr("title");
            ctrl.tooltip({
                disabled: true
            });
            ctrl.on("click", function () {
                ctrl.data("title", ctrl.attr("title")).removeAttr("title");
            }, function () {
                var title = ctrl.data("title");
                ctrl.tooltip("option", "content", title || ctrl.attr("title"));
            });

        }

        function CustomValidatorForLocations(validationGroup, source_ctrl) {            
            try {
                clinicDetailsInfo = '';
                logMessage = "; In try block; Client browser::" + fnGetBrowserVersion();
                logMessage += "; Source Control ID::" + (typeof soure_ctrl === 'string' ? source_ctrl : source_ctrl.id);
                logMessage += "; StoreId::" + $("#hfBusinessClinicStoreId").val() + "; ClinicPk::" + $("#hfBusinessClinicPk").val();
                //var validating_fields = $('input[type=text],textarea,select');
                var validating_fields = $("form :text, textarea, select");
                var validating_field_names = {
                    'txtClinicStore': { 'isRequired': true, 'when': 'always', 'name': 'Store Id', 'type': 'numeric', 'requiredMessage': 'Store Id is required', 'invalidMessage': 'Please enter valid Store Id' },
                    'txtFirstContactName': { 'isRequired': true, 'when': 'always', 'name': 'Contact First Name', 'type': 'junk', 'requiredMessage': 'Contact First Name is required', 'invalidMessage': 'Local Client Contact Name: , < > Characters are not allowed' },
                    'txtContactPhone': { 'isRequired': true, 'when': 'always', 'name': 'Contact Phone', 'type': 'phone', 'requiredMessage': 'Contact Phone is required', 'invalidMessage': 'Valid phone number is required(ex: ###-###-####)' },
                    'txtContactPhoneExt': { 'isRequired': false, 'when': 'always', 'name': 'Contact Phone Extension', 'type': 'numeric', 'requiredMessage': '', 'invalidMessage': 'Please enter valid phone number extension(ex: #####)' },
                    'txtContactEmail': { 'isRequired': true, 'when': 'always', 'name': 'Contact Email', 'type': 'email', 'requiredMessage': 'Contact Email is required', 'invalidMessage': 'Invalid contact email' },
                    'txtPlanId': { 'isRequired': true, 'when': 'always', 'name': 'Plan ID', 'type': 'junk', 'requiredMessage': 'Plan ID is required', 'invalidMessage': 'Plan ID: < > Characters are not allowed' },
                    'txtGroupId': { 'isRequired': true, 'when': 'always', 'name': 'Group ID', 'type': 'junk', 'requiredMessage': 'Group ID is required', 'invalidMessage': 'Group ID: < > Characters are not allowed' },
                    'txtCoverageType': { 'isRequired': true, 'when': 'always', 'name': 'Coverage Type', 'type': 'junk', 'requiredMessage': 'Coverage Type is required', 'invalidMessage': 'Coverage Type: < > Characters are not allowed' },
                    'txtCopay': { 'isRequired': true, 'when': 'always', 'name': 'Copay', 'type': 'junk', 'requiredMessage': 'Copay is required', 'invalidMessage': 'Employee Copay: < > Characters are not allowed' },
                    'txtRecipientId': { 'isRequired': true, 'when': 'always', 'name': 'ID Recipient', 'type': 'junk', 'requiredMessage': 'ID Recipient is required', 'invalidMessage': 'ID Recipient: < > Characters are not allowed' },
                    'txtClinicAddress1': { 'isRequired': true, 'when': 'always', 'name': 'Clinic Address1', 'type': 'address', 'requiredMessage': 'Clinic Address1 is required', 'invalidMessage': 'Clinic Address1: < > Characters are not allowed' },
                    'txtClinicAddress2': { 'isRequired': false, 'when': 'always', 'name': 'Clinic Address2', 'type': 'address', 'requiredMessage': '', 'invalidMessage': 'Clinic Address2: < > Characters are not allowed' },
                    'txtEstimateVolume': { 'isRequired': true, 'when': 'always', 'name': 'Estimated Volume', 'type': 'numeric', 'requiredMessage': 'Estimated Volume is required', 'invalidMessage': 'Estimated Volume: Please enter a number' },
                    'txtTotalImm': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Total Administered', 'type': 'numeric', 'requiredMessage': 'Total Administered is required', 'invalidMessage': 'Total Administered: Please enter a number' },
                    'txtAddlComments': { 'isRequired': false, 'when': 'always', 'name': 'Additional Comments', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': 'Additional Comments Instructions: < > Characters are not allowed' },
                    'txtSpecialInstr': { 'isRequired': false, 'when': 'always', 'name': 'Special Instructions', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': 'Special Billing Instructions: < > Characters are not allowed' },
                    'txtClinicLocation': { 'isRequired': false, 'when': 'always', 'name': 'Clinic Location', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': 'Clinic Location Name: < > Characters are not allowed' },
                    'txtClinicDate': { 'isRequired': true, 'when': 'always', 'name': 'Clinic Date', 'type': '', 'requiredMessage': 'Select the Clinic Date.', 'invalidMessage': '' },
                    'txtClinicStartTime': { 'isRequired': true, 'when': 'always', 'name': 'Clinic Start Time', 'type': '', 'requiredMessage': 'Clinic Start Time is required', 'invalidMessage': '' },
                    'txtClinicEndTime': { 'isRequired': true, 'when': 'always', 'name': 'Clinic End Time', 'type': '', 'requiredMessage': 'Clinic End Time is required', 'invalidMessage': '' },
                    'txtClinicCity': { 'isRequired': true, 'when': 'always', 'name': 'Clinic City', 'type': 'junk', 'requiredMessage': 'Clinic City is required', 'invalidMessage': '' },
                    'txtClinicZip': { 'isRequired': true, 'when': 'always', 'name': 'Clinic Zip', 'type': 'zipcode', 'requiredMessage': 'ZipCode is required', 'invalidMessage': 'Please enter a valid Zip Code (ex: #####)' },
                    'txtRoom': { 'isRequired': false, 'when': 'always', 'name': 'Clinic Room', 'type': 'junk', 'requiredMessage': '', 'invalidMessage': 'Clinic Room: < > Characters are not allowed' },
                    'txtClientIndividual': { 'isRequired': true, 'when': 'confirmclinic', 'name': 'Confired Client', 'type': 'junk', 'requiredMessage': 'This clinic cannot be logged as Confirmed until you have entered the \'Name of Client Individual Who Confirmed the Clinic\'.', 'invalidMessage': 'Client Individual: < > Characters are not allowed' },
                    'ddlClinicState': { 'isRequired': true, 'when': 'always', 'name': 'Clinic State', 'type': 'select', 'requiredMessage': 'Clinic State is required', 'invalidMessage': '' },
                    //**********************  Pharmacist Section Start ********************
                    'txtNameOfRxHost': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Name Of Rx Host', 'type': 'junk', 'requiredMessage': 'This clinic cannot be logged as Completed until you have entered \'Name of Rx Host\'.', 'invalidMessage': 'Name Of Rx Host: < > Characters are not allowed' },
                    'txtPharmacistPhone': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Pharmacist Phone number', 'type': 'phone', 'requiredMessage': 'This clinic cannot be logged as Completed until you have entered \'Phone #\'.', 'invalidMessage': 'Valid phone number is required(ex: ###-###-####)' },
                    'txtTotalHoursClinicHeld': { 'isRequired': true, 'when': 'completeclinic', 'name': 'Total Hours', 'type': 'decimal', 'requiredMessage': 'This clinic cannot be logged as Completed until you have entered \'Total Hours Clinic Held\'.', 'invalidMessage': 'Please enter the Total Hours Clinic Held' },
                    'txtFeedBack': { 'isRequired': true, 'when': 'cancelclinic', 'name': 'Feedback', 'type': 'junk', 'requiredMessage': 'This clinic cannot be logged as Cancelled until you have entered \'Feedback/Notes\'.', 'invalidMessage': 'Feed Back: < > Characters are not allowed' }
                    //**********************  Pharmacist Section End ********************
                };

                var first_identified_control = "";
                var is_valid = true;
                var ctrl_Id;
                var indexOf_Underscore;

                $.each(validating_fields, function (index, ctrl) {                    
                    setValidControlCss($(ctrl), false);
					$(ctrl).attr({ "title": "" });
                    $(ctrl).removeAttr("title");
                    ctrl_Id = $(ctrl).attr('id');
                    clinicDetailsInfo += (clinicDetailsInfo != '') ? ' ' + ctrl_Id : ctrl_Id;
                    if ((ctrl_Id.indexOf('grdVaccineInformation') > -1) && (ctrl_Id.lastIndexOf('_') > -1)) {
                        indexOf_Underscore = ctrl_Id.lastIndexOf('_');
                        if (ctrl_Id.length > indexOf_Underscore + 1) {
                            ctrl_Id = ctrl_Id.substring(indexOf_Underscore + 1);
                        }
                    }
                    clinicDetailsInfo += ':' + $(ctrl).val() + '; ';
                    //throw new Error('Error in Try');
                    if ((validating_field_names[ctrl_Id] != undefined) && validating_field_names[ctrl_Id].isRequired &&
                    (validating_field_names[ctrl_Id].when == 'always' || validating_field_names[ctrl_Id].when == validationGroup.toLowerCase()) &&
                    $(ctrl).val() == "") {
                        if (validating_field_names[ctrl_Id].type == 'select') {
                            if ((navigator.appVersion.indexOf("MSIE 7.") != -1) || (navigator.appVersion.indexOf("MSIE 6.") != -1) || (navigator.userAgent.indexOf("Trident") != -1)) {
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).parent().attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            }
                            else {
                        setInvalidControlCss($(ctrl), false);
                        $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                            }
                        }
                        else {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].requiredMessage });
                        }
                        if (first_identified_control == '')
                            first_identified_control = $(ctrl);
                        is_valid = false;
                    }
                    else if ((validating_field_names[ctrl_Id] != undefined) && $(ctrl).val() != "") {
                        if (!validateData(validating_field_names[ctrl_Id].type, $(ctrl).val(), ((validating_field_names[ctrl_Id].name == 'Estimated Volume' || validating_field_names[ctrl_Id].name == 'Total Administered') ? false : true))) {
                            setInvalidControlCss($(ctrl), false);
                            $(ctrl).attr({ "title": validating_field_names[ctrl_Id].invalidMessage });
                            if (first_identified_control == '')
                                first_identified_control = $(ctrl);
                            is_valid = false;
                        }
                        else if (validating_field_names[ctrl_Id].type == 'address') {
                            if (!(validatePO($(ctrl).val()))) {
                                setInvalidControlCss($(ctrl), false);
                                $(ctrl).attr({ "title": '<%= HttpContext.GetGlobalResourceObject("errorMessages", "ClincLocationPOBoxAlert") %>' });
                                if (first_identified_control == '')
                                    first_identified_control = $(ctrl);
                                is_valid = false;
                            }
                        }
                    }
                });
                if (!is_valid) {
                    alert("Highlighted input fields are required/invalid. Please update and submit.");
                    first_identified_control[0].focus();
                    return false;
                }
                else {
                    logMessage += "; Clinic Location::" + $("#lblClientName").text() + "; Clinic Details::" + clinicDetailsInfo;
                    //logJSerrors(logMessage.replace(/'/g, ""));
                    return true;
                }
            }
            catch (err) {
                var errorMessage = typeof (err.message) == 'undefined' ? err : err.message;
                logMessage += "; In catch block; StoreId::" + $("#hfBusinessClinicStoreId").val() + "; ClinicPk::" + $("#hfBusinessClinicPk").val() + "; Clinic Location::" + $("#lblClientName").text() + "; Error Message::" + errorMessage + "; Error Number::" + err.number + "; Error Name::" + err.name + "; Clinic Details:: " + clinicDetailsInfo;
                //alert('Log Details::' + logMessage.replace(/'/g, "") + '; Error Name::' + err.name + '; Stack::' + err.stack + '; Error Line::' + err.lineNumber + '; Error Column::' + err.columnNumber);
                logJSerrors(logMessage.replace(/'/g, ""));

                alert('An error occurred, please contact administrator');
                return false;
            }
        }

        window.onerror = function (error_msg, url, line_number, error_obj) {
            logMessage += "StoreId::" + $("#hfBusinessClinicStoreId").val() + "; ClinicPk::" + $("#hfBusinessClinicPk").val() + "; Clinic Location::" + $("#lblClientName").text() + "; Error Message::" + error_msg + "; Line Number::" + line_number;
            logJSerrors(logMessage.replace(/'/g, ""));
            return false;
        }
         
        function logJSerrors(log_details) {

            $.ajax({
                url: 'walgreensCorporateClinicDetails.aspx/logJSerrors',
                type: 'POST',
                data: "{'log_details':'" + log_details + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                }
            });
        }

        var tabId = '<%= commonAppSession.SelectedStoreSession.SelectedCorporateClinicsTabId %>';
        function setTab() {
            var selected_tab;
            switch (tabId) {
                case "ClinicInfo":
                    selected_tab = 0;
                    break;
                case "ScheduleAppointment":
                    selected_tab = 1;
                    break;
                default:
                    selected_tab = 0;
                    break;
            }
            var $tabs = $('#tabs').tabs();
            $tabs.tabs('select', selected_tab);
        }

        function setSelectedTab(home_tab_id) {
            $.ajax({
                url: "walgreensCorporateClinicDetails.aspx/setSelectedTabId",
                type: "POST",
                data: "{'home_tab_id':'" + home_tab_id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                }
            });
        }

        $(document).ready(function () {

            if ($.browser.msie) {
                $('#txtClinicStartTime').keydown(function (event) { event.preventDefault(); });
                $('#txtClinicEndTime').keydown(function (event) { event.preventDefault(); });
            }
            else {
                $('#txtClinicStartTime').keypress(function (event) { event.preventDefault(); });
                $('#txtClinicEndTime').keypress(function (event) { event.preventDefault(); });
            }

            var maxLength = 140;
            var returnValue = false;
            $("#idCount").text(maxLength);
            $("#idLabel").text(maxLength);
            $("#txtFeedBack").keypress(function (e) {
                if (($("#txtFeedBack").val().length < maxLength) || (e.keyCode == 8 || e.keyCode == 46 || (e.keyCode >= 35 && e.keyCode <= 40))) {
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                    returnValue = true;
                }
                else {
                    $("#idCount").text(0);
                    returnValue = false;
                }
                return returnValue;
            });

            $("#txtFeedBack").focusout(function () {
                if ($("#txtFeedBack").val().length > maxLength) {
                    $("#txtFeedBack").val($("#txtFeedBack").val().substring(0, maxLength));
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                }
                else {
                    $("#idCount").text(maxLength - $("#txtFeedBack").val().length);
                }

                $("#txtFeedBack").val($("#txtFeedBack").val().replace("<", "&lt;").replace(">", "&gt;"));
            });

            $('#txtClinicStartTime').timepicker({ step: 30, minTime: '00:00am', maxTime: '11:30pm' });
            var clinic_start_time = $('#hfClinicStartTime').val();
            $('#txtClinicEndTime').timepicker({ step: 30, minTime: '00:00am', maxTime: '11:30pm' });
            var dateToday = new Date();
            dateToday = '<%=minClinicDate %>';

            $("#txtClinicDate").datepicker({
                showOn: "button",
                buttonImage: "images/btn_calendar.gif",
                buttonImageOnly: true,
                minDate: dateToday,
                onSelect: function (dateText) {
                    $("#hfClinicDate").val(dateText);
                }
            });

            $("#txtClinicDate").val($("#hfClinicDate").val());

            //Check all/Uncheck all scheduled appointments
            var chk_appt_clinics = $("input[id$='chkAllScheduledAppts']");
            if (chk_appt_clinics != undefined) {
                if ($("#grdScheduledAppt INPUT[id*='chkScheduledAppt']").length == $("#grdScheduledAppt INPUT[id*='chkScheduledAppt']:disabled").length) {
                    chk_appt_clinics.attr('disabled', 'disabled')
                }
            }

            chk_appt_clinics.click(function () {
                $("#grdScheduledAppt INPUT[type='checkbox']").each(function () {
                    if (!$(this)[0].disabled) {
                        $(this).attr('checked', chk_appt_clinics.is(':checked'));
                    }
                });
            });

            $("#grdScheduledAppt INPUT[type='checkbox']").click(
            function (e) {
                if ($("#grdScheduledAppt INPUT[id*='chkScheduledAppt']").length == $("#grdScheduledAppt INPUT[id*='chkScheduledAppt']:checked").length) {
                    chk_appt_clinics.attr('checked', true);
                }
                else {
                    if (!$(this)[0].checked) {
                        chk_appt_clinics.attr('checked', false);
                    }
                }
            });
        });

        function checkSelected(action) {
            var is_selected = false;

            if (action == 'block') {
                $("#grdScheduledAppt INPUT[type='checkbox']:checked").parent().parent().each(function () {
                    if ($(this).find('td')[4] != undefined && $(this).find('td')[4] != null && ($(this).find('td')[4].innerHTML != 'BLOCKED OUT' && $(this).find('td')[4].innerHTML != 'PHARMACIST BREAK'))
                        is_selected = true;
                });
            }
            else if (action == 'unblock') {
                $("#grdScheduledAppt INPUT[type='checkbox']:checked").parent().parent().each(function () {
                    if ($(this).find('td')[4] != undefined && $(this).find('td')[4] != null && ($(this).find('td')[4].innerHTML == 'BLOCKED OUT' || $(this).find('td')[4].innerHTML == 'PHARMACIST BREAK' || $(this).find('td')[4].innerHTML == ""))
                        is_selected = true;
                });
            }
            if (is_selected) {
                return true;
            }
            else {
                alert('Please select an appointment to ' + action);
                return false;
            }
        }

        function addTime(oldTime) {
            var time = oldTime.split(":");
            var hours = time[0];
            var ampm = time[1].substring(2, 4);
            var minutes = time[1].substring(0, 2);
            var time_new='';
            if (+minutes >= 30) {
                hours = (+hours + 1) % 24;
            }
            minutes = (+minutes + 30) % 60;
            if (hours >= 12) {
                time_new = hours - 12 + ':' + minutes + 'pm';
            }
            else
                time_new = hours + ':' + minutes + ampm;
            return time_new;
        }

        function ValidateStoreId(source, arguments) {
            var num_regEx = /^[0-9]+$/;
            var store_id = $('#txtClinicStore').val();

            if(store_id != "")
            {
                if (!num_regEx.test(store_id)) {
                    arguments.IsValid = false;
                }
                else if (store_id == 0) {
                    arguments.IsValid = false;
                }
            }
            else
                arguments.IsValid = false;
        }

        function validateSearchText() {
            var value;
            value = $('#txtSearchAppointments').val();
            $('#txtSearchAppointments').val(value.replace(/</g, "").replace(/>/g, ""));            
        }

        function showStoreChangeWithContactStatusWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "Store change with contact status warning",
                    buttons: {
                        'Ok': function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function showCancelClinicWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 220,
                    width: 450,
                    title: "Cancel clinic confirmation",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("cancelAppts", 1);
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            __doPostBack("cancelAppts", 0);
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function showMaintainContactLogWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 200,
                    width: 375,
                    title: "Reset contact status",
                    buttons: {
                        'Maintain Status': function () {
                            __doPostBack("maintainContactStatus", 1);
                            $(this).dialog('close');
                        },
                        'Contact Client': function () {
                            __doPostBack("maintainContactStatus", 0);
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function showChangeClinicDetailsWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 170,
                    width: 450,
                    title: "Reschedule Appointment confirmation",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("rescheduleAppts", 1);
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            __doPostBack("rescheduleAppts", 0);
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function showBlockApptsWarning(alert) {
            $(function () {
                $("#divConfirmDialog").html(alert);
                $("#divConfirmDialog").dialog({
                    closeOnEscape: false,
                    beforeclose: function (event, ui) { return false; },
                    dialogClass: "noclose",
                    height: 170,
                    width: 450,
                    title: "Block Out Time",
                    buttons: {
                        'Continue': function () {
                            __doPostBack("blockSelectedAppts");
                            $(this).dialog('close');
                        },
                        'Cancel': function () {
                            $("#grdScheduledAppt INPUT[type='checkbox']").each(function () {
                                $(this).removeAttr('checked');
                            });
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        }

        function doBlockUnscheduledAppts() {
            setTab();
            __doPostBack("blockSelectedAppts");
        }

        function showBlockUnblockApptsSuccessMesg(msg) {
            setTab();
            alert(msg);
            __doPostBack("reloadAppointments");
        }
        
    </script>

    <style type="text/css">
        .ui-timepicker-list li
        {
            color: #333333;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 12px;
        }
        .noclose .ui-dialog-titlebar-close { display:none; }
        .ui-icon ui-icon-closethick { display:none; }
          
        .ui-widget
        {
            font-size:11px;
        }
        
        .ui-widget-header
        {
            border-bottom:0px none #FFFFFF;           
        }
        
        .ui-tabs .ui-tabs-panel
        {
            padding:0px;           
        }
        
        .ui-state-active, .ui-state-active, .ui-widget-header .ui-state-active        
        {            
            background:url("images/tab_gradient.jpg") repeat-x scroll 50% 50%;
	        background-repeat: repeat-x;
	        background-position:bottom; 
        }
	    .ui-tabs .ui-tabs-nav li a, .ui-tabs.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-selected a {
		    cursor: pointer;
		    text-align: left;
	    }
	    .ui-tabs .ui-tabs-nav li a 
	    {
		    padding-top:5px;
		    padding-left:15px;
	    }

	    .gridStyles
        {
            border-bottom-width:1px;
            border-bottom-style:solid;
            border-bottom-color:#d6d6d6;
        }
        .gridHeaderStyle
        {
            border-bottom-width:2px;
            border-bottom-style:solid;
            border-bottom-color:#646464;
        }
        .ui-datepicker-trigger
        {
            vertical-align:bottom;
            padding-left:5px;
        }
       
       .WordWrap { width:100%;word-break : break-all }
       .WordBreak { width:100px; OVERFLOW:hidden; TEXT-OVERFLOW:ellipsis}
    </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="backgroundGradient" onload="javascript:setTab()">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfBusinessClinicPk" runat="server" />
    <asp:HiddenField ID="hfBusinessClinicStoreId" runat="server" />
    <asp:HiddenField ID="hfClinicLeadStoreId" runat="server" />
    <asp:HiddenField ID="hfDesignPk" runat="server" />
    <asp:HiddenField ID="hfClinicDate" runat="server" />
    <asp:HiddenField ID="hfClinicStartTime" runat="server" />
    <asp:HiddenField ID="hfClinicUpdateAction" runat="server" />
    <asp:HiddenField ID="hfIsOverlapAllowed" runat="server" Value="false" />
    <asp:HiddenField ID="hfClinicType" runat="server" Value="false" />
    <table width="935" border="0" align="center" cellpadding="0" cellspacing="0" class="dropShadow">
        <tr>
            <td colspan="2">
                <ucWHeader:walgreensHeader ID="walgreensHeaderCtrl" runat="server" />
            </td>
        </tr>
        <tr>
        <td colspan="2" bgcolor="#FFFFFF" align="center" style="padding-top: 24px; padding-bottom: 24px">
            <table width="888" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left">
                <%--<asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="UpdateClinic" />
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="CompleteClinic" />
                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="ConfirmClinic" />
                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" DisplayMode="BulletList" HeaderText="Error: " ShowMessageBox="true" ShowSummary="false" ValidationGroup="CancelClinic" />--%>
                    <div id="tabs">
                        <ul style="font-size: 70%">
                            <asp:Literal ID="lblTabLinks" runat="server" Text="" ></asp:Literal>
                        </ul>
                        <div id="idClinicInfo" style="text-align:center;">
                            <table width="888" border="0" cellspacing="8" cellpadding="0">
                                <tr>
                                    <td colspan="4" class="logTitle" style="text-align: left; vertical-align: middle">Clinic Details</td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                    <table width="870" border="0" cellspacing="12" cellpadding="0" bgcolor="#d7ecc1" style="border: 1px solid #999;">
                                        <tr><td colspan="2" align="left" class="subTitle">Store Information</td></tr>
                                        <tr>
                                            <td align="left">
                                                <table  border="0" cellspacing="0" cellpadding="0" bgcolor="#d7ecc1">
                                                <tr>
                                                    <td width="127" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Assigned Store:</td>
                                                    <td width="50" valign="middle" align="left">
                                                        <asp:TextBox ID="txtClinicStore" runat="server" MaxLength="5" TabIndex="1" Width="100" CssClass="formFields" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr><td colspan="4" align="center">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <table width="870" border="0" cellspacing="12" cellpadding="0" bgcolor="#c1dfec" style="border: 1px solid #999;">
                                            <tr>
                                            <td colspan="4" align="left" class="subTitle">Business Information</td>
                                            </tr>
                                            <tr>
                                            <td align="left">
                                                <table  border="0" cellspacing="0" cellpadding="0" bgcolor="#c1dfec">
                                                <tr>
                                                    <td width="127" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Client Name:</td>
                                                    <td width="208" valign="middle" class="formFields" align="left"><b>
                                                        <asp:Label ID="lblClientName" Width="623px" runat="server" CssClass="formFields" ></asp:Label></b>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td colspan="4" align="center">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <table width="870" border="0" cellspacing="12" cellpadding="0" bgcolor="#f2eebb" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="4" align="left" class="subTitle">Billing &amp; Vaccine Information</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" nowrap="nowrap" class="logSubTitles">Plan ID:</td>
                                                <td align="left" valign="top" class="formFields">
                                                    <asp:TextBox ID="txtPlanId" MaxLength="256" runat="server" TabIndex="2" TextMode="MultiLine" Rows="4" Columns="45" Width="200px" CssClass="formFields"></asp:TextBox>
                                                    <asp:Label ID="lblPlanId" runat="server" CssClass="formFields" visible="false"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" nowrap="nowrap" class="logSubTitles">Coverage Type:</td>
                                                <td align="left" valign="top"><label for="textarea"></label>
                                                    <asp:TextBox ID="txtCoverageType" MaxLength="500" runat="server" TabIndex="3" TextMode="MultiLine" Rows="4" Columns="45" Width="200px" CssClass="formFields"></asp:TextBox>
                                                    <asp:Label ID="lblCoverageType" runat="server" CssClass="formFields" visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="logSubTitles">ID Recipient:<br />
                                                    <span style="font-weight:normal; font-size:11px;">(Cash and Corporate<br />to Invoice Claims)</span></td>
                                                <td valign="top" align="left">
                                                    <asp:TextBox ID="txtRecipientId" MaxLength="256" runat="server" TabIndex="4" Width="200px" CssClass="formFields" TextMode="MultiLine" Rows="4" Columns="45"></asp:TextBox>
                                                    <asp:Label ID="lblRecipientId" runat="server" CssClass="formFields" visible="false"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" nowrap="nowrap" class="logSubTitles">Group ID:</td>
                                                <td align="left" valign="top"><label for="textarea"></label>
                                                    <asp:TextBox ID="txtGroupId"  MaxLength="256" runat="server" TabIndex="5" TextMode="MultiLine" Rows="4" Columns="45" Width="200px" CssClass="formFields"></asp:TextBox>
                                                    <asp:Label ID="lblGroupId" runat="server" CssClass="formFields" visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" valign="middle" class="logSubTitles">
                                                <table width="100%" border="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; background: #fff; border-collapse: collapse; text-align: left;">
                                                    <tr>
                                                    <td>
                                                    <asp:GridView ID="grdVaccineInformation" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" BorderColor="#000000">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Vaccines Covered" HeaderStyle-CssClass="clinicDetailsHeader" HeaderStyle-HorizontalAlign="left" ItemStyle-CssClass="clinicDetailsRow" ItemStyle-Height="20px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblImmunizationPk" runat="server" CssClass="formFields" Text='<%#Bind("immunizationPk") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblVaccinesCovered" runat="server" CssClass="formFields" Text='<%#Bind("immunizationName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Estimated Volume" HeaderStyle-CssClass="clinicDetailsHeader" HeaderStyle-HorizontalAlign="right" ItemStyle-CssClass="clinicDetailsRow" ItemStyle-HorizontalAlign="Right" >
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtEstimateVolume" runat="server" align="right" Text='<%#Bind("estimatedQuantity") %>' TabIndex="5" Width="50" MaxLength="5" CssClass="inputFieldRightAlign"></asp:TextBox>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total Administered" HeaderStyle-CssClass="clinicDetailsHeader" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="right" ItemStyle-CssClass="clinicDetailsRow">
                                                            <ItemTemplate>
                                                                <asp:TextBox TabIndex="6" ID="txtTotalImm" runat="server" align="right" MaxLength="4" Text='<%#Bind("totalImmAdministered") %>' Width="50" CssClass="inputFieldRightAlign"></asp:TextBox>
                                                                <asp:Label ID="lblTotalImm" runat="server" CssClass="formFields" visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <table style="background-color:#debd3a;color:#ffffff;height:25px;padding-left:10px;" width="100%">
                                                                <tr class="clinicDetailsHeader">
                                                                    <td align="left">Vaccines Covered</td>
                                                                    <td align="right">Estimted Volume</td>
                                                                    <td align="right">Total Administered</td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" style="height:25px;padding-left:10px;">
                                                            <tr>  
                                                                <td colspan="3" align="left" class="clinicDetailsRow">No Data Found</td>
                                                            </tr>
                                                            </table>
                                                        </EmptyDataTemplate>  
                                                    </asp:GridView>
                                                    </td>
                                                </tr>
                                                </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" valign="middle" class="logSubTitles">Employee Copay:<br />
                                                    <asp:TextBox ID="txtCopay" runat="server" CssClass="formFields" TabIndex="6" TextMode="MultiLine" MaxLength="500" Rows="3" Columns="100"></asp:TextBox>
                                                    <asp:Label ID="lblCopay" runat="server" CssClass="formFields" style="font-weight:normal;" visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" valign="middle" class="logSubTitles">Additional Comments/Instructions:<br />
                                                    <asp:TextBox ID="txtAddlComments" runat="server" TabIndex="7" CssClass="formFields" TextMode="MultiLine" MaxLength="500" Rows="3" Columns="100"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="left" valign="middle" class="logSubTitles">Special Billing Instructions:<br />
                                                    <asp:TextBox ID="txtSpecialInstr" runat="server" TabIndex="8" CssClass="formFields" TextMode="MultiLine" MaxLength="500" Rows="3" Columns="100" Visible="false"></asp:TextBox>
                                                    <asp:Label ID="lblSpecialInstr" runat="server" CssClass="formFields" style="font-weight:normal;" visible="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td colspan="4" align="center">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <table width="870" border="0" cellspacing="12" cellpadding="0" bgcolor="#f0debc" style="border: 1px solid #999;">
                                            <tr>
                                                <td colspan="4" align="left" class="subTitle">Clinic Information</td>
                                            </tr>
                                            <tr>
                                                <td width="162" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Local Client Contact Name:</td>
                                                <td width="234" valign="middle" align="left">
                                                    <asp:TextBox ID="txtFirstContactName" runat="server" TabIndex="9" Width="200" CssClass="formFields" MaxLength="50"></asp:TextBox>
                                                    <asp:Label ID="lblFirstContactName" runat="server" CssClass="formFields" Visible="false"></asp:Label>
                                                </td>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Local Client Contact Phone #:</td>
                                                <td valign="middle" align="left">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                      <tr>
                                                        <td valign="middle" align="left" style="width:115px;">
                                                            <asp:TextBox ID="txtContactPhone" runat="server" TabIndex="10" Width="100" MaxLength="17" CssClass="formFields" onblur="textBoxOnBlur(this);"></asp:TextBox>
                                                            <asp:Label ID="lblContactPhone" runat="server" CssClass="formFields" Visible="false"></asp:Label>
                                                        </td>
                                                        <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Ext.:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtContactPhoneExt" runat="server" TabIndex="10" Width="50" MaxLength="5" CssClass="formFields" ></asp:TextBox>
                                                            <asp:Label ID="lblContactPhoneExt" runat="server" CssClass="formFields" Visible="false"></asp:Label>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                    
                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Clinic Location:</td>
                                                <td width="234" valign="middle" class="formFields" align="left">
                                                    <asp:TextBox ID="txtClinicLocation" runat="server" TabIndex="11" Width="200" MaxLength="256" CssClass="formFields"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Local Client Contact Email:</td>
                                                <td align="left" valign="middle" nowrap="nowrap">
                                                    <asp:TextBox ID="txtContactEmail" runat="server" TabIndex="12" Width="200" CssClass="formFields" MaxLength="256"></asp:TextBox>
                                                    <asp:Label ID="lblContactEmail" runat="server" CssClass="formFields" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Clinic Address 1:</td>
                                                <td width="234" valign="middle" align="left">
                                                    <asp:TextBox ID="txtClinicAddress1" runat="server" TabIndex="13" Width="200" CssClass="formFields" MaxLength="500"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Clinic Date:</td>
                                                <td align="left" valign="top" nowrap="nowrap" >
                                                    <asp:TextBox ID="txtClinicDate" runat="server" ReadOnly="true" Width="100px" tabindex="14" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Clinic Address 2:</td>
                                                <td width="234" valign="middle" align="left">
                                                    <asp:TextBox ID="txtClinicAddress2" runat="server" TabIndex="15" Width="200" CssClass="formFields" MaxLength="40"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Clinic Start Time:</td>
                                                <td valign="middle" align="left">
                                                    <asp:TextBox ID="txtClinicStartTime" onpaste="return false" runat="server" TabIndex="16" MaxLength="7" Width="100" CssClass="formFields"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Room:</td>
                                                <td valign="middle" align="left">
                                                    <asp:TextBox ID="txtRoom" runat="server" TabIndex="17" Width="200" CssClass="formFields" MaxLength="256"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">Clinic End Time:</td>
                                                <td valign="middle" align="left">
                                                    <asp:TextBox ID="txtClinicEndTime" onpaste="return false" runat="server" TabIndex="18" Width="100" MaxLength="7" CssClass="formFields" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">City:</td>
                                                <td width="234" valign="middle" align="left">
                                                    <asp:TextBox ID="txtClinicCity" runat="server" TabIndex="19" Width="200" CssClass="formFields" MaxLength="100"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="middle" nowrap="nowrap" class="logSubTitles">&nbsp;</td>
                                                <td valign="middle">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="middle" nowrap="nowrap" class="logSubTitles">
                                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                     <tr>
                                                         <td style="text-align:left; vertical-align: top;" width="42%" >
                                                         <table border="0" cellpadding="0" cellspacing="0"><tr><td>State:&nbsp;&nbsp;</td>
                                                             <td id="td1"  runat="server"><asp:DropDownList ID="ddlClinicState" runat="server" Width="100px" CssClass="profileFormObject" TabIndex="20"></asp:DropDownList>
                                                         </td></tr></table></td>
                                                         <td style="text-align:left; vertical-align: top;" width="58%">Zip Code:&nbsp;&nbsp;
                                                             <asp:TextBox ID="txtClinicZip" runat="server" TabIndex="21" MaxLength="5" CssClass="formFields"></asp:TextBox>
                                                         </td>
                                                     </tr>
                                                  </table>
                                                </td>
                                                <td id="logSubTitle" runat="server" align="left" valign="middle" class="logSubTitles">Name of Client Individual <br />Who Confirmed the Clinic:</td>
                                                <td valign="middle" align="left">
                                                    <asp:TextBox ID="txtClientIndividual" runat="server" TabIndex="22" Width="200px" MaxLength="100" CssClass="formFields"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td colspan="4" align="center">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <table width="870" border="0" cellspacing="12" cellpadding="0" bgcolor="#f3d8d9" style="border: 1px solid #999;">
                                            <tr>
                                            <td colspan="4" align="left" class="subTitle">Pharmacist &amp; Post Clinic Information</td>
                                            </tr>
                                            <tr>
                                                <td width="125" align="left" valign="top" nowrap="nowrap" class="logSubTitles">Name of Rx Host:</td>
                                                <td width="262" valign="top" class="formFields" align="left">
                                                    <asp:TextBox ID="txtNameOfRxHost" runat="server" TabIndex="23" Width="200" CssClass="formFields" MaxLength="100"></asp:TextBox>
                                                    <asp:Label ID="lblNameOfRxHost" runat="server" CssClass="formFields" Visible="false"></asp:Label>
                                                </td>
                                                <td width="125" align="left" valign="top" nowrap="nowrap" class="logSubTitles">Total Hours Clinic Held:</td>
                                                <td valign="top" align="left">
                                                    <asp:TextBox ID="txtTotalHoursClinicHeld" runat="server" CssClass="formFields" TabIndex="24" MaxLength="5"></asp:TextBox>
                                                </td>                                                  
                                            </tr>
                                            <tr>
                                            <td align="left" valign="top" nowrap="nowrap" class="logSubTitles">Phone #:</td>
                                            <td valign="top" class="formFields" align="left">
                                                <asp:TextBox ID="txtPharmacistPhone" ValidationGroup="ClinicCompleted" runat="server" TabIndex="25" Width="200" MaxLength="15" CssClass="formFields" onblur="textBoxOnBlur(this);"></asp:TextBox>
                                                <asp:Label ID="lblPharmacistPhone" runat="server" CssClass="formFields" Visible="false"></asp:Label>
                                            </td>
                                            <td width="125" align="left" valign="top" nowrap="nowrap" class="logSubTitles">Feedback/Notes:</td>
                                            <td width="275" valign="middle" class="formFields" align="left">
                                                <asp:TextBox ID="txtFeedBack" runat="server" TextMode="MultiLine" Columns="30" Rows="5" TabIndex="26"></asp:TextBox>
                                                <table align="left" width="85%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left"><span style="font-size: x-small">*<asp:Label ID="idLabel" runat="server"></asp:Label> 
                                                            characters allowed</span></td>
                                                        <td align="right" class="logSubTitles"><asp:Label ID="idCount" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            </tr>
                                        </table>
                                   </td>
                                </tr>
                                <tr><td colspan="4" align="center">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <%--<asp:ImageButton ID="btnConfirmedClinicDim" ImageUrl="~/images/btn_confirmed_clinic_dim.png" CausesValidation="false" runat="server" AlternateText="Confirmed Clinic" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnConfirmedClinicDim" Text="Confirmed Clinic" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Confirmed Clinic" OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnConfirmedClinic" ImageUrl="images/btn_confirmed_clinic.png" CausesValidation="true" runat="server" AlternateText="Confirmed Clinic" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_confirmed_clinic.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_confirmed_clinic_lit.png');" 
                                            TabIndex="28" CommandArgument="Confirmed" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('ConfirmClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnConfirmedClinic" Text="Confirmed Clinic" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Confirmed Clinic" 
                                            TabIndex="27" CommandArgument="Confirmed" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('ConfirmClinic',this);"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnClinicCompletedDim" ImageUrl="~/images/btn_clinic_completed_dim.png" CausesValidation="false" runat="server" AlternateText="Clinic Completed" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnClinicCompletedDim" Text="Clinic Completed" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Clinic Completed" OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnClinicCompleted" ImageUrl="images/btn_clinic_completed.png" CausesValidation="true" runat="server" AlternateText="Clinic Completed" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_clinic_completed.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_clinic_completed_lit.png');" 
                                            TabIndex="29" CommandArgument="Completed" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('CompleteClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnClinicCompleted" Text="Clinic Completed" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Clinic Completed" 
                                            TabIndex="28" CommandArgument="Completed" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('CompleteClinic',this);"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnCancelClinicDim" ImageUrl="~/images/btn_cancel_clinic_dim.png" CausesValidation="false" runat="server" AlternateText="Cancelled Clinic" OnClientClick="javascript: return false;"/>--%>
                                        <asp:LinkButton ID="btnCancelClinicDim" Text="Cancel Clinic" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Cancelled Clinic" OnClientClick="javascript: return false;"/>
                                        <%--<asp:ImageButton ID="btnCancelClinic" ImageUrl="images/btn_cancel_clinic.png" CausesValidation="true" runat="server" AlternateText="Cancelled Clinic" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_cancel_clinic.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_cancel_clinic_lit.png');" 
                                            TabIndex="30" CommandArgument="Cancelled" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('CancelClinic');"/>&nbsp;--%>
                                        <asp:LinkButton ID="btnCancelClinic" Text="Cancel Clinic" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Cancelled Clinic" 
                                            TabIndex="29" CommandArgument="Cancelled" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('CancelClinic',this);"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnSubmitDetails" ImageUrl="images/btn_submit_changes.png" CausesValidation="true" runat="server" AlternateText="Submit"
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_changes.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_changes_lit.png');" 
                                            TabIndex="31" CommandArgument="Submit" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('UpdateClinic');" />&nbsp;--%>
                                        <asp:LinkButton ID="btnSubmitDetails" Text="Submit Changes" CssClass="wagButton" CausesValidation="true" runat="server" AlternateText="Submit" 
                                            TabIndex="30" CommandArgument="Submit" OnCommand="doProcess" OnClientClick="return CustomValidatorForLocations('UpdateClinic',this);" />
                                        <asp:LinkButton ID="btnSubmitDetailsDim" Text="Submit Changes" CssClass="wagButtonDim" CausesValidation="false" runat="server" AlternateText="Submit" 
                                            OnClientClick="javascript: return false;" Visible="false"/>&nbsp;
                                        <%--<asp:ImageButton ID="btnCancelChanges" ImageUrl="images/btn_return_home.png" CausesValidation="false" runat="server" AlternateText="Cancel" 
                                            onmouseout="javascript:MouseOutImage(this.id,'images/btn_return_home.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_return_home_lit.png');" 
                                            TabIndex="32" CommandArgument="Cancel" OnCommand="doProcess" />--%>
                                        <asp:LinkButton ID="btnCancelChanges" Text="Return To Home" CssClass="wagButton" CausesValidation="false" runat="server" AlternateText="Cancel" 
                                            TabIndex="31" CommandArgument="Cancel" OnCommand="doProcess" />
                                    </td>
                                </tr>
                                <tr><td colspan="4" align="center">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <table width="870" border="0" cellspacing="12" cellpadding="0" bgcolor="#eeeeee" style="border: 1px solid #999;">
                                            <tr>
                                                <td width="400" colspan="4" class="subTitle" align="left">History Log</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="grdClinicUpdatesHistory" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="grdClinicUpdatesHistory_OnRowDataBound">
                                                         <Columns>
                                                             <asp:TemplateField HeaderText="">
                                                                <HeaderTemplate>
                                                                <tr>
                                                                    <td width="12%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Date</td>
                                                                    <td width="13%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Action</td>
                                                                    <td width="30%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Updated Field</td>
                                                                    <td width="30%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Update</td>
                                                                    <td width="15%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">User</td>
                                                                </tr>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                <tr>  
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblHistorydate" runat="server" Text='<% #Bind("updatedOn", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblAction" runat="server" Text='<%#Bind("updateAction") %>'></asp:Label>
                                                                    </td>
                                                                    <td id="tdClinicUpdateField" runat="server" class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdatedField" runat="server" Text='<%#Bind("updatedField") %>'></asp:Label>
                                                                    </td>
                                                                    <td id="tdClinicUpdateValue" runat="server" class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdate" runat="server" Text='<%#Bind("updatedValue") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="clinicDetailsRow" style="text-align: left; vertical-align: top; height:25px; ">
                                                                        <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Bind("updatedBy") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                </ItemTemplate>
                                                             </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>  
                                                           <table border="0" width="100%" >
                                                                <tr>
                                                                    <td width="12%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Date</td>
                                                                    <td width="13%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Action</td>
                                                                    <td id="tdClinicUpdateField" runat="server" width="18%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Updated Field</td>
                                                                    <td id="tdClinicUpdateValue" runat="server" width="36%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">Update</td>
                                                                    <td width="21%" class="clinicUpdatesHistoryHeader" style="text-align: left; vertical-align: middle;" scope="col">User</td>
                                                                </tr>
                                                                <tr>  
                                                                    <td colspan="5" class="clinicDetailsRow" style="text-align: center; vertical-align: middle; height:25px; ">No Data Found</td>
                                                                </tr>
                                                            </table> 
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>  
                                <tr><td>&nbsp;</td></tr>                          
                            </table>
                        </div>
                        <div id="idScheduleAppointment" runat="server">
                            <table width="888" border="0" cellspacing="8" cellpadding="0">
                                <tr><td class="logTitle" align="left">Clinic Details</td></tr>
                                <tr>
                                    <td align="center" colspan="4">
                                      <table width="100%" border="0" cellspacing="12" cellpadding="0" bgcolor="#eeeeee" style="border: 1px solid #999;">
                                        <tr><td class="subTitle" style="text-align: left; vertical-align: middle" >Scheduled Appointments</td></tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="35%" valign="top" align="left">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr><td class="profileTitles" align="left" style="height:25px;">Number Of Immunizers:&nbsp;&nbsp;<asp:Label class="formFields2" ID="lblTotalImmunizers" runat="server"></asp:Label></td></tr>
                                                                <tr><td class="profileTitles" align="left" style="height:25px;"><asp:Label ID="lblTotalApptScheduledText" runat="server"></asp:Label><asp:Label class="formFields2" ID="lblTotalScheduledAppts" runat="server" ></asp:Label></td></tr>
                                                                <tr><td align="left">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="padding-bottom:8px; padding-top: 4px;">
                                                                                <asp:ImageButton ID="btnBlockCheckedAppts" ImageUrl="~/images/block_out_scheduled_appt.png" 
                                                                                onmouseout="javascript:MouseOutImage(this.id,'images/block_out_scheduled_appt.png');" onmouseover="javascript:MouseOverImage(this.id,'images/block_out_scheduled_appt_lit.png');" 
                                                                                runat="server" AlternateText="Block Out Selected Appointments" CausesValidation="false" CommandArgument="block" OnCommand="doApptsProcess_Click" OnClientClick="javascript:return checkSelected('block');" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-bottom:8px;">
                                                                                <asp:ImageButton ID="btnUnBlockCheckedAppts" ImageUrl="~/images/unblock_out_scheduled_appt.png" 
                                                                                onmouseout="javascript:MouseOutImage(this.id,'images/unblock_out_scheduled_appt.png');" onmouseover="javascript:MouseOverImage(this.id,'images/unblock_out_scheduled_appt_lit.png');" 
                                                                                runat="server" AlternateText="Block Out Selected Appointments" CausesValidation="false" CommandArgument="Unblock" OnCommand="doApptsProcess_Click" OnClientClick="javascript:return checkSelected('unblock');" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td></tr>
                                                            </table>
                                                        </td>
                                                        <td width="65%" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>                                    
                                                                <td align="left" valign="middle" class="formFields" style="height: 35px; padding-top: 0px; padding-left:0px; padding-right:0px;">
                                                                    <asp:TextBox ID="txtSearchAppointments" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td valign="middle">
                                                                    <asp:ImageButton ID="btnSearchAppts" ImageUrl="~/images/btn_search_appts.png" 
                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_search_appts.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_search_appts_lit.png');" 
                                                                        runat="server" AlternateText="Search Appointments" CausesValidation="false" CommandArgument="Search" OnCommand="doApptsProcess_Click" OnClientClick="javascript:validateSearchText();" />&nbsp;
                                                                    <asp:ImageButton ID="btnShowAllAppts" ImageUrl="~/images/btn_show_all_appts.png" 
                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_show_all_appts.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_show_all_appts_lit.png');" 
                                                                        runat="server" AlternateText="Show All Appointments" CausesValidation="false" CommandArgument="Show All" OnCommand="doApptsProcess_Click" OnClientClick="javascript:validateSearchText();" />&nbsp;
                                                                    <asp:ImageButton ID="btnShowUnScheduledAppts" ImageUrl="~/images/btn_show_unscheduled.png" 
                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_show_unscheduled.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_show_unscheduled_lit.png');" 
                                                                        runat="server" AlternateText="Show Unscheduled" CausesValidation="false" CommandArgument="Show Unscheduled" OnCommand="doApptsProcess_Click" OnClientClick="javascript:validateSearchText();" />
                                                                    <asp:ImageButton ID="btnHideUnScheduledAppts" ImageUrl="~/images/btn_hide_unscheduled.png" 
                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_hide_unscheduled.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_hide_unscheduled_lit.png');" 
                                                                        runat="server" AlternateText="Hide Unscheduled" CausesValidation="false" CommandArgument="Hide Unscheduled" OnCommand="doApptsProcess_Click" OnClientClick="javascript:validateSearchText();" />&nbsp;
                                                                    <asp:ImageButton ID="btnViewScheduledApptReport" ImageUrl="~/images/btn_view_appt_report.png" 
                                                                        onmouseout="javascript:MouseOutImage(this.id,'images/btn_view_appt_report.png');" onmouseover="javascript:MouseOverImage(this.id,'images/btn_view_appt_report_lit.png');" 
                                                                        runat="server" AlternateText="View Report" CausesValidation="false" CommandArgument="View Report" OnCommand="doApptsProcess_Click" OnClientClick="javascript:validateSearchText();" />                                                        
                                                                </td>
                                                            </tr>
                                                                <tr>
                                                                <td class="formFields2" colspan="2">
                                                                    <asp:GridView ID="grdScheduledAppt" runat="server" AutoGenerateColumns="False" CellPadding="3" CellSpacing="0" AllowPaging="true" AllowSorting="true" PageSize="10"
                                                                    OnPageIndexChanging ="grdScheduledAppt_PageIndexChanging" OnRowDataBound="grdScheduledAppt_RowDataBound" OnSorting="grdScheduledAppts_OnSorting" GridLines="None" Width="100%" DataKeyNames="apptPk, apptTimePk, clinicPk">
                                                                        <FooterStyle CssClass="footerGrey" />                                
                                                                        <HeaderStyle BackColor="#999999" ForeColor="White" Font-Size="12px" Font-Bold="true" Height="30px" CssClass="gridHeaderStyle" />
                                                                        <RowStyle Height="35px" BackColor="White" CssClass="gridStyles" />
                                                                        <Columns>
                                                                            <asp:templatefield HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20px" ItemStyle-Width="20px">
				                                                            <headertemplate>
					                                                            <asp:CheckBox ID="chkAllScheduledAppts" runat="server" />
				                                                            </headertemplate>
				                                                            <itemtemplate>
					                                                            <asp:CheckBox ID="chkScheduledAppt" runat="server" />
                                                                                <asp:Label ID="lblIsBlocked" runat="server" Text='<%# Bind("isBlocked") %>' Visible="false"></asp:Label>                                                                    
				                                                            </itemtemplate>
			                                                            </asp:templatefield>
                                                                        <asp:BoundField DataField="apptPk" Visible="false" />
                                                                        <asp:BoundField DataField="apptTimePk" Visible="false" />
                                                                        <asp:BoundField DataField="clinicPk" Visible="false" />                                                            
                                                                        <asp:BoundField HeaderText="Appt Time" HeaderStyle-HorizontalAlign="Center" DataField="apptAvlTimes" SortExpression="apptTimeSortId" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="wrapword" ItemStyle-Width="120px" />                                                            
                                                                        <asp:BoundField HeaderText="Time Slot" HeaderStyle-HorizontalAlign="Center" DataField="apptTimeSlot" SortExpression="apptTimeSlot" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="wrapword" ItemStyle-Width="90px" />
                                                                        <asp:BoundField HeaderText="ID" HeaderStyle-HorizontalAlign="Center" DataField="confirmationId" SortExpression="confirmationId" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="wrapword" />
                                                                        <asp:BoundField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" DataField="PatientName" SortExpression="PatientName" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="wrapword" />                                                            
                                                                        </Columns>
                                                                        <EmptyDataTemplate>
                                                                        <center>
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <thead class="gridHeaderStyle">
                                                                                <tr style="background-color:#999999; color:White; font-weight:bold; height:25px; ">
                                                                                <td class="wrapword" style="width:120px;">Appt Time</td>
                                                                                <td class="wrapword" style="width:90px;">Time Slot</td>
                                                                                <td class="wrapword">ID</td>
                                                                                <td class="wrapword">Name</td>
                                                                                </tr>    
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr style="height:25px;"><td colspan="4" class="formFields" align="center"><b>No appointments scheduled</b></td></tr>
                                                                            </tbody>                                                                                                                                
                                                                            </table>
                                                              
                                                                        </center>
                                                                        </EmptyDataTemplate>
                                                                        <SelectedRowStyle BackColor="LightBlue" />
                                                                    </asp:GridView>
                                                                </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                      </table>
                                    </td>
                                </tr>
                                <tr>
                                <td colspan="4" align="center" valign="bottom" style="padding-top: 20px">
                                    <asp:ImageButton ID="btnSubmit" ImageUrl="images/btn_submit_changes.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_submit_changes.png');" CausesValidation="true" onmouseover="javascript:MouseOverImage(this.id,'images/btn_submit_changes_lit.png');" runat="server" AlternateText="Submit" CommandArgument="Submit" OnCommand="doProcess" />&nbsp;
                                    <asp:ImageButton ID="btnCancel" ImageUrl="images/btn_return_home.png" onmouseout="javascript:MouseOutImage(this.id,'images/btn_return_home.png');" CausesValidation="false" onmouseover="javascript:MouseOverImage(this.id,'images/btn_return_home_lit.png');" runat="server" AlternateText="Cancel" CommandArgument="Cancel" OnCommand="doProcess" />
                                </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    </td>
                </tr>
            </table>
            <div id="divConfirmDialog" style="display: none; font-family:Arial; font-size: 13px; text-align:left; font-weight: normal;"></div>
        </td>
        </tr>
    </table>
    <ucWFooter:walgreensFooter ID="walgreensFooter" runat="server" />
    </form>
</body>
</html>
