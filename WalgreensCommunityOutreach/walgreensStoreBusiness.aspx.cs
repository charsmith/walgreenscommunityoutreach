using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using TdApplicationLib;
using TdWalgreens;
using System.Xml.XPath;

public partial class walgreensStoreBusiness : Page
{
    #region ----------- PROTECTED EVENTS ------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
            this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
            //this.bindStates();
            this.ddlState.bindStates();

            this.makeUserOutreachEfforts();
        }
        else
        {
            int business_pk = 0;
            string error_message = "";
            string event_target = (this.Request["__EVENTTARGET"] == null) ? string.Empty : this.Request["__EVENTTARGET"];
            string event_argument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
            if (!string.IsNullOrEmpty(event_argument))
                Int32.TryParse(event_argument, out business_pk);

            if (!string.IsNullOrEmpty(event_target) && event_target.ToLower() == "deletecontactlog" && business_pk > 0)
            {
                int return_value = 0;
                return_value = this.dbOperation.assignOutreachTobusiness(Convert.ToInt32(this.txtStoreId.Text), business_pk, this.createOutreachEffortsList(), 1, out error_message);
                if (return_value >= 0)
                {
                    this.cancelButton.Text = " Return";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');", true);
                    this.disableAddBusinessControls(false);
                    this.rowAddBusiness.Visible = true;
                    this.rowDupBusinessesGrid.Visible = false;
                    this.doClearFields();
                }
            }
            else if (!string.IsNullOrEmpty(event_target) && event_target.ToLower() == "addbusiness" && business_pk > 0)
            {
                this.doSaveBusiness(true);
            }
            else if (!string.IsNullOrEmpty(event_target) && event_target.ToLower() == "canassigntootherstate")
            {
                this.doSaveBusiness(false);
            }
        }
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
        //this.getStoreControl1.FilePath = "search.aspx";
        //this.displayStoreSelectionToUser();
        this.walgreensHeaderCtrl.isStoreSearchVisible = false;
    }

    /// <summary>
    /// Store Selection refresh handler
    /// </summary>
    protected void walgreensHeaderCtrl_btnStoreIdRefreshHandler()
    {
        this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
        this.txtStoreProfiles.Text = this.commonAppSession.SelectedStoreSession.storeName;
        this.ddlState.bindStates();
        this.makeUserOutreachEfforts();
    }
    #endregion

    #region ----------- PROTECTED ACCESSORS ---------
    /// <summary>
    /// save user and associated store business information to the specified database tables.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void doSave(object sender, CommandEventArgs e)
    {
        if (this.txtStoreId.Text == "0")
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('Please Select a Store from Store Search');", true);
            return;
        }
        else
        {
            //Disabling "Add A Business" facility to restricted state stores
            string store_state = string.Empty;
            store_state = this.commonAppSession.SelectedStoreSession.storeState;
            if (ApplicationSettings.isRestrictedStoreState(store_state, commonAppSession.LoginUserInfoSession.UserRole))
            {
                for (int cnt = 0; cnt < this.chkLstOutreachEfforts.Items.Count; cnt++)
                {
                    if (this.chkLstOutreachEfforts.Items[cnt].Text.ToLower() == "immunization program" && this.chkLstOutreachEfforts.Items[cnt].Selected)
                    {
                        this.chkLstOutreachEfforts.Items[cnt].Selected = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + String.Format((string)GetGlobalResourceObject("errorMessages", "disableAddBusinessToStore"), (store_state == "MO" ? "Missouri" : "District of Columbia"), (store_state == "MO" ? "20" : "15")) + "');", true);
                        return;
                    }
                }
            }

            if (Page.IsValid)
            {
                if (this.ddlState.SelectedItem.Value != store_state)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showWarning('Store Assignment','" + (string)GetGlobalResourceObject("errorMessages", "BusinessAssignmentToOtherStateAlert") + "','CanAssignToOtherState');", true);
                else
                    this.doSaveBusiness(false);
            }
            else
            {
                string msg = "";
                // Loop through all validation controls to see which generated the errors.
                foreach (IValidator aValidator in this.Validators)
                {
                    if (!aValidator.IsValid)
                    {
                        msg += aValidator.ErrorMessage + "\\r\\n";
                    }
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + msg.Trim() + "');", true);
            }
        }
    }

    protected void doAssignBusiness(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLower())
        {
            case "add":
                RadioButton rb_business = new RadioButton();
                int business_pk = 0, return_value = 0;
                string error_message = "";
                foreach (GridViewRow row in this.grdDuplicateBusiness.Rows)
                {
                    rb_business = (RadioButton)row.FindControl("rbSelectBusiness");
                    if (rb_business.Checked)
                        business_pk = Convert.ToInt32(((Label)row.FindControl("lblBusinessPk")).Text);
                }

                if (business_pk > 0)
                {
                    return_value = this.dbOperation.assignOutreachTobusiness(Convert.ToInt32(this.txtStoreId.Text), business_pk, this.createOutreachEffortsList(), 0, out error_message);

                    if (return_value >= 0)
                    {
                        this.cancelButton.Text = " Return";

                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');", true);
                        this.disableAddBusinessControls(false);
                        this.rowAddBusiness.Visible = true;
                        this.rowDupBusinessesGrid.Visible = false;
                        this.doClearFields();
                    }
                    else if (return_value == -2)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "displayConfirmation('" + error_message + "','" + business_pk + "');", true);
                    else if (return_value == -3)
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message + "');", true);
                }
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "selectBusinessToAdd") + "');", true);

                break;
            case "cancel":
                this.disableAddBusinessControls(false);
                this.rowAddBusiness.Visible = true;
                this.rowDupBusinessesGrid.Visible = false;
                break;
        }
    }

    protected void doCancel(object sender, CommandEventArgs e)
    {
        Response.Redirect("walgreensHome.aspx", false);
    }

    //protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    //{
    //    if (this.ddlStoreList.SelectedItem != null)
    //        this.txtStoreProfiles.Text = this.ddlStoreList.SelectedItem.Text;
    //}
    #endregion

    #region ----------- PRIVATE METHODS -------------
    private void doSaveBusiness(bool is_ignore_dnc)
    {
        int return_value = 0;
        int contactlog_pk = 0;
        string error_message = string.Empty;
        DataTable dt_dup_business;

        return_value = this.dbOperation.createStoreBusiness(this.prepareBusinessAddressXml(), this.commonAppSession.LoginUserInfoSession.UserID, (is_ignore_dnc ? 0 : 1), out error_message, out dt_dup_business, out contactlog_pk);

        if (return_value == -1 || return_value == 0)
        {
            this.cancelButton.Text = " Return";
            this.doClearFields();
            if (contactlog_pk > 0)
            {
                this.commonAppSession.SelectedStoreSession.SelectedContactLogPk = contactlog_pk;
                if (hdSelectedBusinessType.Value == "charity" && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');window.location.href='walgreensCharityProgram.aspx';", true);
                    return;
                }
                else if (hdSelectedBusinessType.Value == "CommunityOutreach" && this.commonAppSession.SelectedStoreSession.OutreachProgramSelectedId == "3")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');window.location.href='walgreensCommunityOutreachProgram.aspx';", true);
                    return;
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');", true);
            }
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "businessAdded") + "');", true);

        }
        else if (return_value == -2)
        {
            string last_string = "";
            error_message = error_message.TrimStart(',').TrimEnd(',');
            if (error_message.LastIndexOf(',').ToString() != "-1")
            {
                last_string = error_message.Substring(error_message.LastIndexOf(','));
                error_message = error_message.Replace(last_string, last_string.Replace(",", " and"));
            }
            if (dt_dup_business.Rows.Count > 0)
            {
                this.disableAddBusinessControls(true);
                this.grdDuplicateBusiness.DataSource = dt_dup_business;
                this.grdDuplicateBusiness.DataBind();
                this.rowDupBusinessesGrid.Visible = true;
                this.rowAddBusiness.Visible = false;
                this.lblDuplicatebusinessAlert.Text = String.Format((string)GetGlobalResourceObject("errorMessages", "assignOutreachToDuplicateBusiness"), error_message.Replace("'", "`"), this.getSelectedOutreachEfforts());
            }
            else
            {
                error_message = String.Format((string)GetGlobalResourceObject("errorMessages", "duplicateBusiness"), error_message);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + error_message.Replace("'", "`") + "');", true);
            }
        }
        else if (return_value == -3)
        {
            if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "showDNCMatchWarning('" + (string)GetGlobalResourceObject("errorMessages", "doNotCallDupBusiness") + "');", true);
            else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Window", "alert('" + (string)GetGlobalResourceObject("errorMessages", "doNotCallDupBusiness") + "');", true);
        }
    }

    /// <summary>
    /// Disables all input controls in the page
    /// </summary>
    /// <param name="is_disable"></param>
    private void disableAddBusinessControls(bool is_disable)
    {
        foreach (Control page_ctrl in this.Page.Controls)
        {
            foreach (Control ctrl in page_ctrl.Controls)
            {
                if (ctrl is TextBox && ctrl.ID.ToLower() != "txtstoreid" && ctrl.ID.ToLower() != "txtstoreprofiles")
                    ((TextBox)ctrl).Enabled = !is_disable;

                if (ctrl is DropDownList)
                    ((DropDownList)ctrl).Enabled = !is_disable;

                if (ctrl is CheckBoxList)
                    ((CheckBoxList)ctrl).Enabled = !is_disable;
            }
        }
    }

    ///// <summary>
    ///// Binding Store dropdown with out view state
    ///// </summary>
    //private void storeDropDown()
    //{
    //    DataTable user_stores = this.dbOperation.getUserAllStores(this.commonAppSession.LoginUserInfoSession.UserID);
    //    this.ddlStoreList.DataSource = user_stores;
    //    this.ddlStoreList.DataTextField = "address";
    //    this.ddlStoreList.DataValueField = "storeid";
    //    this.ddlStoreList.DataBind();
    //}

    ///// <summary>
    ///// Display store selection drop down to users
    ///// </summary>
    //private void displayStoreSelectionToUser()
    //{
    //    if (this.commonAppSession.LoginUserInfoSession.IsAdmin)
    //    {
    //        //this.tblStores.Visible = true;
    //        this.tdStoreSelectionDropDown.Visible = false;

    //    }
    //    else if (this.commonAppSession.LoginUserInfoSession.IsPowerUser)
    //    {
    //       // this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = true;
    //        this.storeDropDown();
    //        if (this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text) != null)
    //            this.ddlStoreList.Items.FindByValue(this.txtStoreId.Text).Selected = true;
    //    }
    //    else
    //    {
    //        //this.tblStores.Visible = false;
    //        this.tdStoreSelectionDropDown.Visible = false;
    //    }
    //}

    /// <summary>
    /// Get Outreach Effort List
    /// </summary>
    private void makeUserOutreachEfforts()
    {
        DataTable dt_outreach_efforts = new DataTable();
        dt_outreach_efforts = this.dbOperation.getOutreachEffortsList;

        foreach (DataRow row in dt_outreach_efforts.Rows)
        {
            ListItem outreach_listitem = new ListItem();
            outreach_listitem.Value = row["fieldValue"].ToString();
            outreach_listitem.Text = row["outreachEffort"].ToString();

            this.chkLstOutreachEfforts.Items.Add(outreach_listitem);
        }
    }

    /// <summary>
    /// This function returns the xml structure to DB for creating the Store Profile
    /// </summary>
    /// <returns></returns>
    private XmlDocument prepareBusinessAddressXml()
    {
        XmlDocument business_address_doc = new XmlDocument();
        XmlElement store_profile_element = store_profile_element = business_address_doc.CreateElement("storeprofile");
        XmlElement fields_element = business_address_doc.CreateElement("fields");

        //XmlElement create_store = business_address_doc.CreateElement("field");

        this.businessInforElement("storeId", this.txtStoreId.Text, false, fields_element, business_address_doc, out business_address_doc, out fields_element);

        this.businessInforElement("firstName", this.txtFirstName.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("lastName", this.txtLastName.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("jobTitle", this.txtTitle.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("businessName", this.txtBusinessName.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("address", this.txtAddress1.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("address2", this.txtAddress2.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("county", this.txtCounty.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("city", this.txtCity.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("state", ddlState.SelectedValue.Trim(), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("zip", this.txtZip.Text.Trim(), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("zip4", this.txtZip4.Text.Trim(), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("phone", this.txtPhone.Text.Trim().Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", ""), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("tollfree", this.txtTollFree.Text.Trim(), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("industry", this.txtIndustry.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("website", this.txtWebUrl.Text.Trim().Replace("'", "`"), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("isCurrent", "1", true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("actualLocationEmploymentSize", this.txtEmpSize.Text.Trim(), true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("isStandardized", "0", true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("wave", "0", true, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("outreachEffort", this.createOutreachEffortsList(), false, fields_element, business_address_doc, out business_address_doc, out fields_element);

        //Removing Charity(HHS Voucher) program option until August 2018, since the only immunization "Standard Trivalent" is no longer available.
        //this.businessInforElement("isHHSVoucher", (this.chkHhsProgram.Checked) ? "1" : "0", false, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("isHHSVoucher", "0", false, fields_element, business_address_doc, out business_address_doc, out fields_element);
        this.businessInforElement("isCommunityOutReach", (this.chkCOProgram.Checked) ? "1" : "0", false, fields_element, business_address_doc, out business_address_doc, out fields_element);

        store_profile_element.AppendChild(fields_element);
        business_address_doc.AppendChild(store_profile_element);

        return business_address_doc;
    }

    /// <summary>
    /// Creates the business information field element for profile XML document
    /// </summary>
    /// <param name="field_name"></param>
    /// <param name="field_value"></param>
    /// <param name="is_table_column"></param>
    /// <param name="fields_element"></param>
    /// <param name="profile_doc"></param>
    protected void businessInforElement(string field_name, string field_value, bool is_table_column, XmlElement fields_element, IXPathNavigable profile_doc, out XmlDocument profile_doc_created, out XmlElement fields_element_created)
    {
        XmlElement create_store;

        create_store = ((XmlDocument)profile_doc).CreateElement("field");
        create_store.SetAttribute("name", field_name);
        create_store.SetAttribute("value", field_value);
        if (!is_table_column)
            create_store.SetAttribute("isTableColumn", "0");

        fields_element.AppendChild(create_store);

        profile_doc_created = (XmlDocument)profile_doc;
        fields_element_created = fields_element;
    }


    /// <summary>
    /// Creates selected outreach efforts list
    /// </summary>
    /// <returns></returns>
    private string createOutreachEffortsList()
    {
        string outreachefforts_list = string.Empty;
        for (int cnt = 0; cnt < this.chkLstOutreachEfforts.Items.Count; cnt++)
        {
            if (!this.chkLstOutreachEfforts.Items[cnt].Selected) continue;
            if (outreachefforts_list.Length > 0)
                outreachefforts_list += "," + this.chkLstOutreachEfforts.Items[cnt].Value.Trim();
            else
                outreachefforts_list = this.chkLstOutreachEfforts.Items[cnt].Value.Trim();
        }

        if (!string.IsNullOrEmpty(hdOutreachEffort.Value))
            outreachefforts_list = hdOutreachEffort.Value;
        return outreachefforts_list;
    }

    /// <summary>
    /// Gets selected outreach efforts
    /// </summary>
    /// <returns></returns>
    private string getSelectedOutreachEfforts()
    {
        string outreachefforts_list = string.Empty;
        for (int cnt = 0; cnt < this.chkLstOutreachEfforts.Items.Count; cnt++)
        {
            if (!this.chkLstOutreachEfforts.Items[cnt].Selected) continue;
            if (outreachefforts_list.Length > 0)
                outreachefforts_list += "," + this.chkLstOutreachEfforts.Items[cnt].Text.Trim();
            else
                outreachefforts_list = this.chkLstOutreachEfforts.Items[cnt].Text.Trim();
        }
        return outreachefforts_list;
    }

    /// <summary>
    /// Clear all the date existing in the controls.
    /// </summary>
    private void doClearFields()
    {
        foreach (Control ctrl in from Control c in Page.Controls from Control ctrl in c.Controls select ctrl)
            if (ctrl is TextBox && ctrl.ID.ToLower() != "txtstoreid" && ctrl.ID.ToLower() != "txtstoreprofiles")
                ((TextBox)ctrl).Text = "";
            else if (ctrl is DropDownList)
                ((DropDownList)ctrl).SelectedIndex = 0;
            else if (ctrl is CheckBox)
                ((CheckBox)ctrl).Checked = false;
            else if (ctrl is CheckBoxList)
                ((CheckBoxList)ctrl).ClearSelection();

        this.txtStoreId.Text = this.commonAppSession.SelectedStoreSession.storeID.ToString();
    }

    /// <summary>
    /// Validatiing phone number
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void validatePhone(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validatePhone();
    }

    /// <summary>
    /// Validates zipCode to check 00000 & non-numeric values
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void validateZipCode(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = e.Value.validateZipCode();
    }
    /// <summary>
    /// Validates Address to check if it has PO Box
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void validateAddress(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = (this.txtAddress1.Text.Trim() + this.txtAddress2.Text.Trim()).validateAddress();
    }
    #endregion

    #region ----------- PRIVATE VARIABLES -----------
    private AppCommonSession commonAppSession = null;
    private DBOperations dbOperation = null;
    #endregion

    #region Web Form Designer generated code
    protected void Page_Init(object sender, EventArgs e)
    {
        //Validate session and redirect to session timeout if session expires
        ApplicationSettings.validateSession();

        this.commonAppSession = AppCommonSession.initCommonAppSession();
        this.dbOperation = new DBOperations();
        walgreensHeaderCtrl.btnStoreIdRefreshHandler += walgreensHeaderCtrl_btnStoreIdRefreshHandler;
    }
    #endregion
}
